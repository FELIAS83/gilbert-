/*---------------------
	:: <%- name %> 
	-> model
---------------------*/
<%- name %> = Model.extend({
	
	// Attributes/Schema
	// 
	// ex.
	// attrName: {
	//	type: STRING,
	//	validate: {
	//	is: ['someValue','someOtherValue']
	//	}
	// },
	
	// Associations
	// 
	// ex.
	// belongsTo: [ 'SomeModel', 'SomeOtherModel']
	
});