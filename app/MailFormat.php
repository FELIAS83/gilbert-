<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailFormat extends Model
{
    protected $fillable = ['id','name','subject','title', 'content', 'status','user_id_update', 'user_id_creation' ];
}
