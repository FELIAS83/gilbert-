<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricalRenovation extends Model
{
     protected $fillable = ['id','id_policy','id_plan','id_deductibles', 'id_coverage','status','user_id_creation'];
}
