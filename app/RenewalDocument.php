<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenewalDocument extends Model
{
    protected $table = 'renewal_documents';
    protected $fillable = ['id_policy', 'id_doc_type','filename','description','directory','status','user_id_creation','user_id_update'];
}
