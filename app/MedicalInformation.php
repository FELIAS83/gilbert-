<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalInformation extends Model
{
    //
    protected $table = 'medical_informations';
    protected $fillable = ['id', 'policy_id','quiz_id', 'quiz_details_id', 'answer', 'repairable_id', 'observation','user_id_creation','user_id_update'];
}
