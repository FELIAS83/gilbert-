<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorMedicalSpeciality extends Model
{
    protected $table = 'doctor_medical_specialities';
    protected $fillable = ['doctor_id', 'speciality_id','status','user_id_creation','user_id_update'];
}
