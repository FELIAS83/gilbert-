<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimDocumentTypes extends Model
{
     protected $fillable = [
       'id','name','status','user_id_update', 'user_id_creation' 
    ];
}
