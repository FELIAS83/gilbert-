<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressCity extends Model
{

    protected $table = 'address_citys';
    protected $fillable = ['id','name'];
}
