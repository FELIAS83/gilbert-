<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annex extends Model
{
    protected $fillable = [
        'policy_id','affiliate_id','annex','effective_date','user_id_creation','user_id_update'
    ];
}
