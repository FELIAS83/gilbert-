<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeDocuments extends Model
{
     protected $fillable = [
        'id_policy','payment_id','filename','status','user_id_creation','user_id_update','type_id'
    ];
}
