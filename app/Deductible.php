<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deductible extends Model
{
    //
     protected $fillable = ['id','name','amount_in_usa','amount_out_usa', 'plan_id', 'status','user_id_creation','user_id_update'] ;
}
