<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $fillable = ['id','policy_id', 'dispatched', 'is_closed', 'status','user_id_creation','user_id_update'] ;
}
