<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCommision extends Model
{
    //
     protected $fillable = [
        'id','agent_id', 'policy_id', 'plan_id', 'deductible_id', 'coverage_id', 'commision', 'amount', 'status', 'user_id_creation', 'user_id_update'
    ];
}
