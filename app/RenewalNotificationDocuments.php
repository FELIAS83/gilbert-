<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenewalNotificationDocuments extends Model
{
     protected $table = 'renewal_notification_documents';
    protected $fillable = ['id_policy', 'id_doc_type','filename','directory','status','user_id_creation','user_id_update'];
}
