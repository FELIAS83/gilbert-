<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $fillable = ['id','name','company_id','commission_percentage', 'status','user_id_creation','user_id_update'];
}
