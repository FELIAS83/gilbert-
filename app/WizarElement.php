<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WizarElement extends Model
{
    protected $fillable = ['id', 'name', 'icon', 'order', 'status', 'user_id_creation', 'user_id_update'] ;
}
