<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messenger extends Model
{
    //
    protected $fillable = [
        'id','name', 'type', 'identity_document', 'status', 'user_id_creation'
    ];
}
