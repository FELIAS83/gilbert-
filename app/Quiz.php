<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    //
      protected $fillable = ['id','name','slug','status','user_id_creation','user_id_update'] ;
}
}
