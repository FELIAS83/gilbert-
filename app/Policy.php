<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $fillable = ['id','first_name','last_name','identity_document','email','mobile','phone','agent_id','plan_id','start_date_coverage','continuity_coverage','have_prev_safe','company_name','plan_name','customer_response', 'type_policy','is_international','status','user_id_creation','user_id_update'] ;
}
