<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnostic extends Model
{
    protected $fillable = ['id','claim_id','description','status','user_id_creation','user_id_update', 'id_person', 'person_type'] ;

    public function documents(){
        return $this->hasMany('App\DocumentDiagnostic','diagnostic_id','id');
    }

    public function claims(){
        return $this->hasOne('App\Claim','id','claim_id');
    }

    public function claimDocuments(){
        return $this->hasMany('App\ClaimDocument','id','claim_id');
    }

}
