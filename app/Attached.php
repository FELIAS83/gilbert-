<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attached extends Model
{
    protected $fillable = [
        'payment_id','description','filename','user_id_creation','user_id_update'
    ];
}
