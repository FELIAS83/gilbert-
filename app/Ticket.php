<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
  protected $fillable = [
       'id','id_ticket_type','id_policy','id_person','person_type','hour','doctor','medical_speciality','hospital','city','date_start','date_end','request_medical','confirmed_date','guarantee_letter','status', 'user_id_creation','user_id_update' 
    ];

    public function ticketType(){
        return $this->hasOne('App\TicketTypes','id','id_ticket_type');
    }
}

