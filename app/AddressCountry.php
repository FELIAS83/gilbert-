<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressCountry extends Model
{
    protected $table = 'address_countrys';
    protected $fillable = ['id','name'];
}
