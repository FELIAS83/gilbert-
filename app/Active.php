<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    //
    protected $table = 'actives';
    protected $fillable = ['id','variable_name','active'];
}
