<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policystep extends Model
{
    //
    protected $fillable = ['id','id_policy','step1','step2', 'step3', 'step4','step5','step6','step7','user_id_update', 'user_id_creation' ];
}