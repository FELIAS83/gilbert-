<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoverageDetail extends Model
{
    //
    protected $fillable = ['id','amount','coverage_id','deductible_id','status','user_id_creation','user_id_update'] ;
}
