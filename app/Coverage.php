<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coverage extends Model
{
    //
     protected $fillable = ['id','name','plan_id','status','user_id_creation','user_id_update'] ;

     public function detail(){//funcion para relacionar la tabla doctors con doctor_medical_specialities
        return $this->hasOne('App\CoverageDetail',//modelo a relacionar
                              'coverage_id',
                              'id');//campos a relacionar
    }

}
