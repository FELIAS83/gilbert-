<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizDetail extends Model
{
    //
    protected $table = 'quiz_details';
    protected $fillable = ['id','name','quiz_id','status','user_id_creation','user_id_update'] ;
}
