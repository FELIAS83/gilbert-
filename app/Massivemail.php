<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Massivemail extends Model
{
    protected $fillable = ['id', 'format', 'subject', 'title', 'content', 'recipient'];
    public $timestamps = false;
}
