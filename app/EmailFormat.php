<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailFormat extends Model
{
    protected $fillable = ['id','name','subject','title', 'content','recipient', 'cc', 'status','user_id_update', 'user_id_creation' ];
}
