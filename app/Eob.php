<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eob extends Model
{
    protected $fillable = ['id','id_claim','diagnostic_id','payment_type','observation','bank','amount','check_number','status','user_id_creation','user_id_update' 
    ];   
}
