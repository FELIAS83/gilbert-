<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['module', 'key_field', 'text','status','user_id_creation','user_id_update'];

    public function userInfo(){
        return $this->hasOne('App\User','id','user_id_creation');
    }
}
