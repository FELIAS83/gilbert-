<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyHistory extends Model
{
    //
     protected $table = 'policies_history';
    protected $fillable = ['id','policy_id','number','agent_id','plan_id','deductible_id', 'coverage_id','start_date_coverage','user_id_creation','user_id_update'] ;
}
