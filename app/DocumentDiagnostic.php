<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentDiagnostic extends Model
{
    protected $fillable = ['id','diagnostic_id','document_id','related','status','user_id_creation','user_id_update'] ;

    public function diagnostics(){
        return $this->hasOne('App\ClaimDocument','id','document_id');
    }

    public function diagnostics2(){
        return $this->hasOne('App\Diagnostic','id','diagnostic_id');
    }

}
