<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimDocumentTypeDescriptions extends Model
{
     protected $fillable = [
       'id','id_doc_type','description','status','user_id_update', 'user_id_creation' 
    ];
}
