<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    //
      protected $table = 'commissions';
      protected $fillable = [
        'id', 'agent_id', 'company_id', 'branch_id', 'plan_id', 'commission','commission_renewal', 'status','user_id_creation','user_id_update'
    ];
}
