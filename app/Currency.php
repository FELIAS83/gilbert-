<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
       'id','name','iso','status','user_id_update', 'user_id_creation' 
    ];
}
