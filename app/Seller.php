<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = ['id','company_id','agent_id','type','percentage_vn','percentage_rp','status','user_id_creation','user_id_update'] ;
}
