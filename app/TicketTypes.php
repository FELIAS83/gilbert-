<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketTypes extends Model
{
    //
    protected $fillable = ['id','name','status','user_id_creation','user_id_update' 
    ];    
}
