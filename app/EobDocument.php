<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EobDocument extends Model
{
    //
    protected $table = 'eob_documents';
    protected $fillable = ['id','eob_id','directory','filename','status','user_id_creation','user_id_update'] ;
}
