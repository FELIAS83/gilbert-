<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductibleDetail extends Model
{
    //
    protected $fillable = ['id','age_start','age_end','is_child', 'child_quantity', 'amount', 'deductible_id','user_id_creation','user_id_update'] ;
}
