<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable = [
        'id','policy_id', 'mode', 'method', 'discount', 'discount_percent', 'invoice_before','created_at', 'updated_at', 'user_id_creation', 'user_id_update',
    ];
}
