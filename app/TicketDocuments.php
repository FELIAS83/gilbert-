<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketDocuments extends Model

{
    	 protected $fillable = ['id','ticket_id','id_doc_type','directory','filename','status','user_id_creation','user_id_update' 
    ];    
}
