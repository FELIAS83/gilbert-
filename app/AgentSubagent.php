<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentSubagent extends Model
{
    //
     protected $fillable = [
        'agent_id', 'subagent_id', 'status', 'user_id_creation', 'user_id_update'
    ];
}
