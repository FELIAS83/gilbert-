<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimDocument extends Model
{
    protected $fillable = ['id_claim', 'id_doc_type', 'provider_id', 'currency_id', 'amount', 'filename','description','directory','document_date','status','user_id_creation','user_id_update'];
    
    public function documents(){
        return $this->hasOne('App\DocumentDiagnostic','document_id','id');
    }
}
