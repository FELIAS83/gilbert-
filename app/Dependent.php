<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    protected $fillable = ['id','applicant_id','first_name','last_name','gender','birthday','height','height_unit_measurement','document_type','identity_document','relationship','weight','weight_unit_measurement','status','user_id_creation','user_id_update'] ;
}
