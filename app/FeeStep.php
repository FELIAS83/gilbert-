<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeStep extends Model
{
    //
    protected $fillable = ['id','fee_id','policy_id','step1','step2', 'step3', 'step4','step5','status','user_id_update', 'user_id_creation' ];
}
