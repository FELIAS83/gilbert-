<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressState extends Model
{
    //
    protected $fillable = ['id','name'];
}
