<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailConfiguration extends Model
{
    protected $fillable = ['host', 'username','name','password','protocol','port'];
}
