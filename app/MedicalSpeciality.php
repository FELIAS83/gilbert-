<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalSpeciality extends Model
{
    protected $table = 'medical_specialties';
    protected $fillable = ['abbreviation', 'name','status','user_id_creation','user_id_update'];
}
