<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $table = 'hospitals';
    protected $fillable = ['name', 'country_id','province_id','city','address','status','user_id_creation','user_id_update'];

    public function specialities(){//funcion para relacionar la tabla hospitals con hospital_medical_specialities
        return $this->hasMany('App\HospitalMedicalSpeciality',//modelo a relacionar
                              'hospital_id',
                              'id');//campos a relacionar
    }
}
