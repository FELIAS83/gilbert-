<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddressProvinceController extends Controller
{
    //

    public function getIndex($country_id)
    {
    	$states =  DB::table('address_provinces')->where('country_id', '=', $country_id)->get();

    	return response()->success($states);

    }
}
