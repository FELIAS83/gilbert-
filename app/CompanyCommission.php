<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCommission extends Model
{
    //
      protected $fillable = [
        'id','company_id','agent_id', 'policy_id', 'plan_id', 'deductible_id', 'coverage_id', 'commission', 'amount', 'status', 'user_id_creation', 'user_id_update'
    ];
}
