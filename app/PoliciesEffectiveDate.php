<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoliciesEffectiveDate extends Model
{
    //
    protected $fillable = [
        'id', 'start_date_coverage','policy_id','status','created_at', 'updated_at', 'user_id_creation', 'user_id_update',
    ];
}
