<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $fillable = ['id','policy_id','first_name','last_name','address','gender','civil_status','country_id','province_id','city_id','email','mobile','phone','place_of_birth','birthday','height','height_unit_measurement','document_type','identity_document','weight','weight_unit_measurement','occupation','status','user_id_creation','user_id_update'] ;
    public function country(){
        return $this->hasMany('App\AddressCountry','id','country_id');
    }
    public function province(){
        return $this->hasMany('App\AddressProvince','id','province_id');
    }
    public function city(){
        return $this->hasMany('App\AddressCity','id','city_id');
    }
    public function dependents(){
        return $this->hasMany('App\Dependent','applicant_id','id');
    }

}
