<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubAgent extends Model
{
    //
    protected $fillable = ['id','first_name','last_name','identity_document','birthday','email','skype','mobile','phone','country_id','province_id','city_id','address','commission','leader','status','user_id_creation','user_id_update'] ;
}
