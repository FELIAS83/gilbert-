<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name', 'status','branch_id','commission_percentage','max_dependent_age', 'created_at', 'updated_at', 'user_id_creation', 'user_id_update',
    ];
}
