<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amendment extends Model
{
    protected $fillable = [
    'policy_id','role_order','affiliate_id','type','description','user_id_creation','user_id_update'
    ];
}
