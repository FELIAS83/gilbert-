<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyApplicant extends Model
{
    //
    protected $fillable = [
        'id','policy_id_renewal','applicant_id', 'policy_id_old','user_id_creation','user_id_update'
    ];
}
