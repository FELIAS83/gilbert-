<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        'id','first_name','last_name','identity_document','brithday','email','skype','mobile','phone','country_id','province_id','city_id','address','sub_agent','commission','leader'
    ];
}
