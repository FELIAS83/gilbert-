<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\User;
use App\Hospital;
use App\HospitalMedicalSpeciality;
use App\MedicalSpeciality;
use App\AddressCountry;
use App\AddressProvince;
use App\AddressCity;
use Auth;
use Input;
use DB;

class HospitalController extends Controller
{
    public function getIndex()
    {
     $hospitals = Hospital::with('specialities')->where('status', '1')->get();//consulta tabla hospitals con specialities

        for ($i = 0; $i < count($hospitals) ; $i++ ){//recorro los hospitales

            $country = AddressCountry::find($hospitals[$i]->country_id);
            $hospitals[$i]->countryname=$country->name;

            $province = AddressProvince::find($hospitals[$i]->province_id);
            $hospitals[$i]->provincename=$province->name;

            //$city = AddressCity::find($hospitals[$i]->city_id);
            //$hospitals[$i]->cityname=$city->name;

            for ($j = 0; $j < count($hospitals[$i]->specialities) ; $j++ ){//recorro las especialidades de cada hospital
                $medical_speciality = MedicalSpeciality::find($hospitals[$i]->specialities[$j]->speciality_id);//consulto cual es el nombre de cada especialidad
                $hospitals[$i]->specialitiesnames.=$medical_speciality->name.',';//creo un elemento del array con los nombres concatenados
            }
            if ($hospitals[$i]->specialitiesnames==false){
                $hospitals[$i]->specialitiesnames=''    ;
            }
            else{
            $hospitals[$i]->specialitiesnames = substr($hospitals[$i]->specialitiesnames, 0, -1);//elimino la ultima coma
            }

        }

        return response()->success(compact('hospitals'));
    }


    public function postHospitals()
    {
        $usercreate = Auth::user();

        $hospital = Hospital::create([
            'name' => Input::get('name'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city' => Input::get('city'),
            'address' => Input::get('address'),
            'user_id_creation' => $usercreate->id
        ]);

        foreach (Input::get('specialties') as $speciality) {
            HospitalMedicalSpeciality::create(['hospital_id'=>$hospital->id,'speciality_id'=>$speciality]);
        }


        return response()->success('hospital');
    }

    public function getShow($id)
    {

        $data = Hospital::with('specialities')->find($id);//hago una consulta relacionada para traer los datos del hospital y las especialidades que tiene en la tabla
        $data->id = (string)$data->id;

        foreach ($data->specialities as $key => $value) {
            $activespecialities[] = $value->speciality_id;
        }


        if (isset($activespecialities))
        {
            $data->activespecialities = $activespecialities;
        }
        else
        {
            $data->activespecialities = [];
        }
        return response()->success($data);


    }

    public function putShow(Request $request)
    {
        $userupdate = Auth::user();
        $userForm = array_dot(
            app('request')->all()
        );

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required',
            'data.country_id' => 'required',
            'data.province_id' => 'required',
            'data.city' => 'required',
            'data.address' => 'required',
        ]);

        $data = [
            'name' => $userForm['data.name'],
            'country_id' => $userForm['data.country_id'],
            'province_id' => $userForm['data.province_id'],
            'city' => $userForm['data.city'],
            'address' => $userForm['data.address'],
            'user_id_update' => $userupdate->id
        ];

        $hospital = Hospital::where('id',$userForm['data.id'])->update($data);

        HospitalMedicalSpeciality::where('hospital_id',$userForm['data.id'])->delete();

        foreach (Input::get('data.activespecialities') as $speciality) {
            HospitalMedicalSpeciality::create(['hospital_id'=>$userForm['data.id'],'speciality_id'=>$speciality]);
        }

        return response()->success('success');
    }


    public function deleteHospitals($id)
    {
        $hospitalData = [
            'status' => 0,
        ];
        $affectedRows = Hospital::where('id', '=', $id)->update($hospitalData);
        return response()->success('success');
    }
}
