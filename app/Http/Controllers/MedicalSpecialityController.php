<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;


use App\User;
use App\MedicalSpeciality;
use Auth;
use Input;

class MedicalSpecialityController extends Controller
{
    public function getIndex()
    {
        $medicalspecialties = DB::table('medical_specialties')->where('status', '=', '1')
                    ->select(["medical_specialties.*"])
                    ->get();

        return response()->success(compact('medicalspecialties'));
    }

    public function postMedicalspecialties()
    {
        $usercreate = Auth::user();

        $medical_speciality = MedicalSpeciality::create([
            'abbreviation' => Input::get('abbreviation'),
            'name' => Input::get('name'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('medical_speciality');
    }

    public function getShow($id)
    {
        $medical_speciality = MedicalSpeciality::find($id);

        return response()->success($medical_speciality);
    }

    /**
     * Update company data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $medical_specialityForm = array_dot(
            app('request')->only(
                'data.id',
                'data.abbreviation',
                'data.name'
            )
        );

        $medical_specialityId = intval($medical_specialityForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $medical_specialityData = [
            'abbreviation' => $medical_specialityForm['data.abbreviation'],
            'name' => $medical_specialityForm['data.name'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = MedicalSpeciality::where('id', '=', $medical_specialityId)->update($medical_specialityData);

        return response()->success('success');
    }

    /**
     * Delete company Data.
     *
     * @return JSON success message
     */
    public function deleteMedicalspecialties($id)
    {
        $medical_specialityData = [
            'status' => 0,
        ];
        $affectedRows = MedicalSpeciality::where('id', '=', $id)->update($medical_specialityData);
        return response()->success('success');
    }
}
