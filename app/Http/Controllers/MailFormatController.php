<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\EmailFormat;

class MailFormatController extends Controller
{
    public function getIndex()
    {

      
        $mail_formats = EmailFormat::all();
        return response()->success(compact('email_formats'));
    }
    public function postShow(Request $request)
    {
        $mail_formatsForm = array_dot(
            app('request')->all()
        );

        $this->validate($request, [
            // 'data.id' => 'required|integer',
            'data.name' => 'required',
            'data.subject' => 'required',
            'data.title' => 'required',
            'data.content' => 'required'
          
        ]);

        foreach ($request->input('data') as $key => $value) {
            $mail_formatsData[$key] = $value;
        }

        $mail_formats = EmailFormat::create($mail_formatsData);

        return response()->success('success');
    }

    public function getShow($id)
    {
        $data = EmailFormat::find($id);
        return response()->success($data);
    }

    public function putShow(Request $request)
    {
        $mail_formatsForm = array_dot(
            app('request')->all()
        );

        $id = intval($mail_formatsForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required',
            'data.subject' => 'required',
            'data.title' => 'required',
            'data.content' => 'required'
            ]);

        $data = [
            'name' => $mail_formatsForm['data.name'],
            'subject' => $mail_formatsForm['data.subject'],
            'title' => $mail_formatsForm['data.title'],
            'content' => $mail_formatsForm['data.content'],
            'status' => $mail_formatsForm['data.status'],
            'user_id_update' => $userupdate->id
        ];

        $affectedRows = EmailFormat::where('id', '=', $id)->update($data);

        return response()->success('success');
    }

    public function deleteFormat($id)
    {
        $mail_formats = EmailFormat::find($id);
        $mail_formatsData = [
            'status' => 0,            
        ];
        $affectedRows = EmailFormat::where('id', '=', $id)->update($mail_formatsData);
        return response()->success('success');
    }
}
