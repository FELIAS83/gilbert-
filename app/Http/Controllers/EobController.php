<?php

namespace App\Http\Controllers;
Use Auth;
Use DB;
Use Input;
Use Illuminate\Http\Request;
Use App\Eob;
Use App\Claim;
Use App\Policy;
Use App\Diagnostic;
Use App\EmailFormat;
Use App\DocumentDiagnostic;
Use App\Applicant;
use App\EobDocument;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class EobController extends Controller
{

        /**
     * Get all eobs.
     *
     * @return JSON
     */

    public function getIndex()
    {
        $eob = Eob::all();
        $eob = DB::table('eobs')->where('status', '=', '1')
                    ->select(["eob.*"])
                    ->get();
       
        return response()->success(compact('eob'));
    }
    
    /**
     * Create new eob.
     *
     * @return JSON
    */ 
    public function postEob()
    {
        $usercreate = Auth::user();
        $file = Input::get('myFile');
        $folder = "/eob/";
        $diagnostic_id = Input::get('diagnostic_id');
        
        $eob = Eob::create([
            'id_claim'=>Input::get('id_claim'),
            'diagnostic_id'=>Input::get('diagnostic_id'),
            'observation' => Input::get('observation'),
            'check_number' => Input::get('check_number'),
            'bank' => Input::get('bank'),
            'amount' => Input::get('amount'),
            'payment_type' => Input::get('payment_type'),
            'user_id_creation' => $usercreate->id
        ]);

        foreach ($file as $value) {
            $eob_document = EobDocument::create([
                "eob_id" => $eob->id,
                "directory"  => $folder.$diagnostic_id,
                "filename"  => $value,
                'user_id_creation' => $usercreate->id     
            ]);
        }


        return response()->success('eob');
    }

    public function postPrintdoc(){



        $user = Auth::user();

        $diagnosticId = Input::get('diagnostic_id');
        $allClaims = Input::get('allClaims');
        $claim_id =$allClaims[0]['claim_id'];
        $payment_type_id=Input::get('payment_type_id');
        $eob = Input::get('eob');
        $dependents_name=Input::get('insured_name');
        $amount=Input::get('amount');
        $observation=Input::get('observation');
        $amountDeduc= Input::get('amountDeduc');
        $discount= Input::get('discount');
        $cover = Input::get('cover');
        $check_number= Input::get('check_number');
        $bank = Input::get('bank');
        $provider= Input::get('provider');

        $policy_id = Input::get('policy_id');
    
               
        if ($payment_type_id == 1)
                {
                    $payment_type='Cheque';
                }
                else{
                    $payment_type='Transferencia';
                }       
        
        
        $pages = array();

         if (!empty($observation)) {
            $observation = "<strong>Observación: </strong>".$observation."<br/><br/>" ;
        }

        $mailformat = EmailFormat::find(12);
        
        $policy = Policy::find($policy_id);
        $policy_number = $policy->number;
        //echo $policy_id;
        $applicant = Applicant::where ('policy_id', $policy_id)->first();
        //var_dump($applicant);
        $applicant_name = $applicant->first_name.' '.$applicant->last_name;
        //echo $applicant_name;
        $subject = $mailformat->subject;        
        $subject = str_replace("policy_number",$policy_number,$subject);
        $subject = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$subject);

        $title =  $mailformat->title; 
        //var_dump($title);       
        $content = $mailformat->content;
        $content = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$content);
        $content = str_replace("policy_number",$policy_number,$content);
        $content = str_replace("dependents_name",$dependents_name,$content);

        $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $current_date = "<p align='right'>Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y')."</p>";
        $title = str_replace("applicant_name",$applicant_name,$title);
        $title = str_replace("current_date",$current_date,$title);
        
        //var_dump($title);        
        $body_table = "";
        $totalCount = 0 ;

        foreach ($allClaims as  $key => $value) {
            if($value['checkInvoice'] == true){
                $body_table.= "<tr>
                                <th align='center'>".$eob[$key]['eob']."</th>
                                <th align='center'>".$value['amount']."</th>
                                <th align='center'>".$amountDeduc[$key]."</th>
                                <th align='center'>".$discount[$key]."</th>
                                <th align='center'>".$cover[$key]."</th>
                                <th align='center'>".$provider[$key]."</th>
                                <th align='center'>".$value['total']."</th>
                            </tr>";
                 
                $totalCount += $allClaims[$key]['total'];
            }
        }
        
        $content = str_replace("body_table",$body_table,$content);
        $content = str_replace("observation",$observation,$content);
        $content = str_replace("total_count",$totalCount,$content);
        $content = str_replace("bank",$bank,$content);
        $content = str_replace("check_number",$check_number,$content);
        $content = str_replace("amount",$amount,$content);
        $content = str_replace("payment_type",$payment_type,$content);

        //$content.= "-------------------------------------------------------------------------------------";         
    
        $pages[] = $title.$content;

     
        return response()->success($pages);


    }

    function postPrintdocpdf(){
    $user = Auth::user();
    $content = [];
    $final_content = [];
    $table = [];


        $diagnosticId = Input::get('diagnostic_id');
        $allClaims = Input::get('allClaims');
        $claim_id =$allClaims[0]['claim_id'];
        $payment_type_id=Input::get('payment_type_id');
        $eob = Input::get('eob');
        $dependents_name=Input::get('insured_name');
        $amount=Input::get('amount');
        $observation=Input::get('observation');
        $amountDeduc= Input::get('amountDeduc');
        $discount= Input::get('discount');
        $cover = Input::get('cover');
        $check_number= Input::get('check_number');
        $bank = Input::get('bank');
        $provider= Input::get('provider');

        $policy_id = Input::get('policy_id');
    
               
        if ($payment_type_id == 1)
        {
            $payment_type='Cheque';
        }
        else{
            $payment_type='Transferencia';
        }       
        /*if (!empty($observation)) {
            $observation = "Observación: ".$observation ;
        }*/

        $mailformat = EmailFormat::find(12);
        
        $policy = Policy::find($policy_id);
        $policy_number = $policy->number;
        
        $applicant = Applicant::where ('policy_id', $policy_id)->first();
        
        $applicant_name = $applicant->first_name.' '.$applicant->last_name;
        
        $subject = $mailformat->subject;        
        //$subject = str_replace("policy_number",$policy_number,$subject);
        //$subject = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$subject);

        $title =  $mailformat->title; 
        //var_dump($title);       
        
        
        $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $date = "Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y').""; 
        $totalCount = 0 ;

        foreach ($allClaims as  $key => $value) {
            $dataT = [];
            if($value['checkInvoice'] == true){
                $dataT = ["eob" => $eob[$key]['eob'], "amount" => $value['amount'], "deductible" => $amountDeduc[$key], "discount" =>   $discount[$key], "cover" =>  $cover[$key], "provider" => $provider[$key], "total" => $value['total']] ;
                                
                $totalCount += $allClaims[$key]['total'];
            }
            $totalCount = number_format((float)$totalCount,2,'.','');
            array_push($table, $dataT);
        }
        
        //$content = str_replace("body_table",$body_table,$content);
        $final_content = ["policy_number" => $policy->number, "applicant" => $applicant_name, "table" => $table , "observation" => $observation, "totalCount" => $totalCount, "affiliate" => $dependents_name, "date" => $date];
        /*$content = str_replace("observation",$observation,$content);
        $content = str_replace("total_count",$totalCount,$content);
        $content = str_replace("bank",$bank,$content);
        $content = str_replace("check_number",$check_number,$content);
        $content = str_replace("amount",$amount,$content);
        $content = str_replace("payment_type",$payment_type,$content);
        */

        //$content.= "-------------------------------------------------------------------------------------";         
    
     //   $pages[] = $title.$content;

     
        return response()->success($final_content);

 } 

}