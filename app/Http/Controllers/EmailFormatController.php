<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\EmailFormat;
use Input;
use DB;
use Auth;

class EmailFormatController extends Controller
{
     public function getIndex()
    {
    	$email_formats = DB::table('email_formats')->where('status', '=', '1')
                    ->get();
       
        return response()->success(compact('email_formats'));
    }

    public function postShow(Request $request)
    {
        $email_formatsForm = array_dot(
            app('request')->all()
        );

        $this->validate($request, [
            // 'data.id' => 'required|integer',
            'data.name' => 'required',
            'data.subject' => 'required',
            'data.title' => 'required',
            'data.content' => 'required'
          
        ]);

        foreach ($request->input('data') as $key => $value) {
            $email_formatsData[$key] = $value;
        }

        $email_formats = EmailFormat::create($email_formatsData);

        return response()->success('success');
    }

    public function getShow($id)
    {
        $data = EmailFormat::find($id);
        return response()->success($data);
    }

    public function putShow(Request $request)
    {
        $email_formatsForm = array_dot(
            app('request')->all()
        );

        $id = intval($email_formatsForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required',
            'data.subject' => 'required',
            'data.title' => 'required',
            'data.content' => 'required'
            ]);

        $data = [
            'name' => $email_formatsForm['data.name'],
            'subject' => $email_formatsForm['data.subject'],
            'title' => $email_formatsForm['data.title'],
            'content' => $email_formatsForm['data.content'],
            'status' => $email_formatsForm['data.status'],
            'recipient' => $email_formatsForm['data.recipient'],
            'cc' => $email_formatsForm['data.cc'],
           // 'user_id_update' => $userupdate->id
        ];

        $affectedRows = EmailFormat::where('id', '=', $id)->update($data);

        return response()->success('success');
    }

    public function deleteFormat($id)
    {
        $email_formats = EmailFormat::find($id);
        $email_formatsData = [
            'status' => 0,            
        ];
        $affectedRows = EmailFormat::where('id', '=', $id)->update($email_formatsData);
        return response()->success('success');
    }
}
