<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Annex;

class AnnexController extends Controller
{
    public function getAnnexes($policy_id)
    {
        $data = Annex::where('policy_id',$policy_id)->where('status',1)->get();

        foreach ($data as $value){
            $value->affiliate_id = (int) $value->affiliate_id;
        }
        return response()->success($data);
    }
}
