<?php

namespace App\Http\Controllers;

use App\User;
use App\SubAgent;
use Auth;
use Input;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

class SubagentController extends Controller
{
    //
     /**
     * Get all active Agent.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        $subagents = DB::table('sub_agents')->where('status', '1')->get();

      
        return response()->success(compact('subagents'));
    }

     /**
     * Post  Agent.
     *
     * @return JSON
     */

     public function postSubagents()
    {
        $usercreate = Auth::user();
        
        $subagents =  SubAgent::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'birthday' => date("Y-m-d h:i:s", strtotime(Input::get('birthday'))),
            'email' => Input::get('email'),
            'skype' => Input::get('skype'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city_id' => Input::get('city_id'),
            'address' => Input::get('address'),
            'commission' => Input::get('commission'),
            'user_id_creation' => $usercreate->id
        ]);
        return response()->success('subagents');
    }

    /**
     * Get Agents details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $subagents = DB::table('sub_agents')->find($id);
        
        return response()->success($subagents);
    }


    /**
     * Update company data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $subagentForm = array_dot(
            app('request')->only(
                'data.id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.birthday',
                'data.email',
                'data.skype',
                'data.mobile',
                'data.phone',
                'data.country_id',
                'data.province_id',
                'data.city_id',
                'data.address',
                'data.commission'
            )
        );


        $subagentId = intval($subagentForm['data.id']);
      
        $userupdate = Auth::user();

        $subagentData = [
            'first_name' => $subagentForm['data.first_name'],
            'last_name' => $subagentForm['data.last_name'],
            'identity_document' => $subagentForm['data.identity_document'],
            'birthday' => $subagentForm['data.birthday'],
            'email' => $subagentForm['data.email'],
            'skype' => $subagentForm['data.skype'],
            'mobile' => $subagentForm['data.mobile'],
            'phone' => $subagentForm['data.phone'],
            'country_id' => $subagentForm['data.country_id'],
            'province_id' => $subagentForm['data.province_id'],
            'city_id' => $subagentForm['data.city_id'],
            'address' => $subagentForm['data.address'],
            'commission' => $subagentForm['data.commission'],
            'user_id_update' => $userupdate->id,
        ];
        $affectedRows = DB::table('sub_agents')->where('id', '=',  $subagentId)->update($subagentData);

        return response()->success('success');
    }


    /**
     * Delete active Agent.
     *
     * @return JSON
     */

     public function deleteSubagent($id)
    {
        $subagentData = [
            'status' => 0,            
        ];
        $affectedRows = DB::table('sub_agents')->where('id', '=', $id)->update($subagentData);
        return response()->success('success');
    }
}
