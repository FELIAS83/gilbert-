<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Payment;
use Auth;
use Input;
use DB;

class PaymentController extends Controller
{
    //
    /**
     * Get payment details referenced by policy_id.
     *
     * @param int policy_id ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $payments = Payment::where('policy_id', '=', $id)
                    ->where('status', '=', 1)  
                    ->get();


        return response()->success($payments);
    }

    /**
     * Post payment details
     *
     *
     *
     * @return JSON
     */

    public function postPayments(){

         $usercreate = Auth::user();

        $payments = Payment::create([
            'policy_id' => Input::get('policyId'),
            'mode' => Input::get('mode'),
            'method' => Input::get('method'),
            'discount' => Input::get('discount'),
            'discount_percent' => Input::get('discount_percent'),
            'invoice_before' => Input::get('invoice_before'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success(Input::get('policyId'));


    }

    /**
     * Put payment details
     *
     *
     *
     * @return JSON
     */

    public function putShow(){

         $paymentForm = array_dot(
            app('request')->only(
                'data.id',
                'data.policy_id',
                'data.mode',
                'data.method',
                'data.discount',
                'data.discount_percent',
                'data.invoice_before'

            )
        );
         $id = intval($paymentForm['data.id']);
         $userupdate = Auth::user();

          $paymentData = [
            'policy_id' => $paymentForm['data.policy_id'],
            'mode' => $paymentForm['data.mode'],
            'method' => $paymentForm['data.method'],
            'discount' => $paymentForm['data.discount'],
            'discount_percent' => $paymentForm['data.discount_percent'],
            'invoice_before' => $paymentForm['data.invoice_before'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Payment::where('id', '=',  $id)->update($paymentData);

        return response()->success($paymentForm['data.policy_id']);



    }
}
