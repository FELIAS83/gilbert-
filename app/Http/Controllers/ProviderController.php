<?php

namespace App\Http\Controllers;

use App\User;
use App\Provider;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;


class ProviderController extends Controller
{
     /**
     * Get all providers.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $providers = Provider::all();
        $providers = DB::table('providers')->where('status', '=', '1')
                    ->select(["providers.*"])
                    ->get();

        return response()->success(compact('providers'));
    }

    /**
     * Create new provider.
     *
     * @return JSON
     */
    public function postProviders()
    {
        $usercreate = Auth::user();
        
        $provider = Provider::create([
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('provider');
    }

    /**
     * Get provider details referenced by id.
     *
     * @param int provider ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $provider = Provider::find($id);
        
        return response()->success($provider);
    }

    /**
     * Update provider data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $providerForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.description'
                )
        );

        $providerId = intval($providerForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3'
        ]);

        $userupdate = Auth::user();

        $providerData = [
            'name' => $providerForm['data.name'],
            'description' => $providerForm['data.description'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Provider::where('id', '=', $providerId)->update($providerData);

        return response()->success('success');
    }


 /**
     * Delete provider Data.
     *
     * @return JSON success message
     */
    public function deleteProvider($id)
    {
        $providerData = [
            'status' => 0,            
        ];
        $affectedRows = Provider::where('id', '=', $id)->update($providerData);
        return response()->success('success');
    }
}
