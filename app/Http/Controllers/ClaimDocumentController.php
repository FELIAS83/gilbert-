<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\Http\Requests;

class ClaimDocumentController extends Controller
{
    public function getShow($id)
    {
        //$policydocument = PolicyDocument::where('status', '1')->where('id_policy',$id)->get();
        $policydocument = DB::table('claim_documents')->where('claim_documents.status', '=', '1')
        			->where('id_policy',$id)
                    ->select(["claim_documents.*", "doc_types.name"])
                    ->join('doc_types', 'claim_documents.id_doc_type', '=', 'doc_types.id')
                    ->get();

        return response()->success($policydocument);
    }
}
