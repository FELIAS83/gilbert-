<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\AgentSubagent;
use app\SubAgent;

use Auth;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AgentSubagentController extends Controller
{
    //
     /**
     * Get all active Subagent by Agent id.
     *
     * @return JSON
     */
    public function getIndex($agentId){
        $subagents = DB::table('sub_agents')
                        ->where('sub_agents.status', '=', '1')
                        ->where('agent_subagents.status', '=', '1')
                        ->where('agent_subagents.agent_id', '=',  $agentId)
                        ->select(["sub_agents.id", "sub_agents.first_name","sub_agents.last_name", "sub_agents.identity_document","sub_agents.mobile","sub_agents.phone","sub_agents.email","agent_subagents.agent_id"])
                        ->join('agent_subagents', 'sub_agents.id', '=', 'agent_subagents.subagent_id')
                        //->join('agents', 'agents.id', '=', 'agent_subagents.agent_id')
                        ->get();
        return response()->success(compact('subagents'));
    }

    //
     /**
     * Get all active Subagent 
     *
     * @return JSON
     */

    public function getShow($agentId){
        $subagents = DB::table('sub_agents')
                        ->where('agent_subagents.agent_id', '<>', $agentId)
                        ->where('sub_agents.status', '=',  '1')
                        ->select(["sub_agents.id", "sub_agents.first_name","sub_agents.last_name", "sub_agents.identity_document"])
                        ->leftjoin('agent_subagents',  'agent_subagents.subagent_id', '<>', 'sub_agents.id')
                        ->get();
        return response()->success(compact('subagents'));
    }


     /**
     * Delete active Agent.
     *
     * @return JSON
     */

     public function deleteAgentsub($id)
    {
        $subagentData = [
            'status' => 0,            
        ];
        $affectedRows = DB::table('agent_subagents')->where('subagent_id', '=', $id)->update($subagentData);
        return response()->success('success');
    }
}
