<?php

namespace App\Http\Controllers;

use App\User;
use App\Plan;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class PlanController extends Controller
{
    /**
     * Get all plans.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $plans = DB::table('plans')->where('status', '=', '1')
                    ->select(["plans.*"])
                    ->get();

        return response()->success(compact('plans'));
    }
    /**
     * Create new Plan.
     *
     * @return JSON
     */
    public function postPlan()
    {
        $usercreate = Auth::user();
        
        $plans = Plan::create([
            'name' => Input::get('name'),
            'branch_id' => Input::get('branchId'),
            'commission_percentage' => Input::get('commission_percentage'),
            'max_dependent_age' => Input::get('max_age'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('plans');
    }

    /**
     * Get messenger details referenced by id.
     *
     * @param int messenger ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $plans = Plan::find($id);
        
        return response()->success($plans);
    }

    /**
     * Update messenger data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $planForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.commission_percentage',
                'data.max_dependent_age'
            )
        );

        $planId = intval($planForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $planData = [
            'name' => $planForm['data.name'],
            'commission_percentage' => $planForm['data.commission_percentage'],
            'max_dependent_age' => $planForm['data.max_dependent_age'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Plan::where('id', '=', $planId)->update($planData);

        return response()->success('success');
    }

     /**
     * Delete plan Data.
     *
     * @return JSON success message
     */
    public function deletePlan($id)
    {
        $planData = [
            'status' => 0,            
        ];
        $affectedRows = Plan::where('id', '=', $id)->update($planData);
        return response()->success('success');
    }

    /**
    * Get Plan associate with branches
    *
    **/
    public function getPlans(){

        $branchId = Input::get('branchId');
        $plans = DB::table('plans')->where('status', '=', '1')->where('branch_id', $branchId)
                    ->select(["plans.*"])
                    ->get();

        return response()->success(compact('plans'));
    
    }
}
