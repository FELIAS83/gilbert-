<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use DB;
use App\User;
use Illuminate\Http\Request;
use App\RenewalNotificationDocuments;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Configuration;

class RenewalNotificationDocumentsController extends Controller
{
    public function getShow($id){
    	$notif_documents=DB::table('renewal_notification_documents')->where('status','=', '1')
    	->where('id_policy',$id)
    	->get(); 
    	 return response()->success($notif_documents);
    }
 /**
     * Create new claimDocumentTypes.
     *
     * @return JSON
    */ 
    public function postRenotificationdocs(Request $request)
    {
    	$filename= $request['filename'];
    	$policy_id = Input::get('id_policy');
        $usercreate = Auth::user();
        $doctypes = $request['id_doc_types'];

        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->renewal_notifications_file_directory)) ? $directory[0]->renewal_notifications_file_directory : 0;

	    for ($i = 0; $i < count($filename) ; $i++ ){//creacion de documentos de Aviso de renovacion

	        $notif_documents = RenewalNotificationDocuments::create([
	            'id_policy' => $policy_id,
	            'filename' => $filename[$i],
	            'directory'=> 'renewal_notifications/'.$policy_id,
	            'id_doc_type'=> 1,
	            'user_id_creation' => $usercreate->id
	        ]);
	    }

	    $modified = $request['modified'];
        if (isset($modified)){
            foreach ($modified as $value) {
                $affectedRows = RenewalNotificationDocuments::where('id',$value['id'])->update([
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => $usercreate->id
                ]);//update
            }
        }

        $document_ids = $request['deleted_ids'];
        if (isset($document_ids)){
            foreach ($document_ids as $value) {
                $affectedRows = RenewalNotificationDocuments::where('id', $value['id'])->update([
                    'status' => 0,
                    'user_id_update' => $usercreate->id,
                ]);//delete
            }
        }   


        return response()->success('notif_documents');
    }
}
