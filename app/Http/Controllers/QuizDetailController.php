<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\QuizDetail;
use Auth;
use Input;
use DB;

class QuizDetailController extends Controller
{
    //
    public function getIndex()
	{
		$quizdet =  DB::table('quiz_details')->where('status', '1')->get();
		return response()->success(compact('quizdet'));


	}
}
