<?php

namespace App\Http\Controllers;

use App\User;
use App\Claim;
use App\Applicant;
use App\ClaimDocument;
use App\ClaimDocumentTypes;
use App\ClaimDocumentTypeDescriptions;
use App\Diagnostic;
use App\DocumentDiagnostics;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Policy;
use App\Http\Requests;
use App\Configuration;
use App\DocumentDiagnostic;
use App\Dependent;
use App\EmailFormat;
use App\Provider;



class ClaimController extends Controller
{
    /**
     * Get all claims.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $step = Input::get('step');
        if (isset($step)){//liquidaciones pendientes
            $step = Input::get('step');
            $claims = DB::table('claims')->where('claims.status', '=', '1')
                    ->where('claims.dispatched', Input::get('claimStatus'))
                    ->where('claims.is_closed', '0')
                    ->where('claims.step', '>', 0)
                    ->where('claims.step', '<', 2)
                    ->select(["claims.id", "claims.policy_id", "claims.created_at", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(claims.created_at, '%d/%m/%Y') as created_at"), "policies.number", DB::raw('sum(amount) AS amount'),'claims.step','diagnostics.*'])
                    ->join('applicants', 'claims.policy_id', '=', 'applicants.policy_id')
                    ->join('policies', 'policies.id', '=', 'applicants.policy_id')
                    ->join('claim_documents', 'claim_documents.id_claim', '=', 'claims.id')
                    ->join('diagnostics', 'claims.id', '=', 'diagnostics.claim_id')
                    ->groupby('claims.id')
                    ->get();
        }
        else{//reclamos pendientes
            $claims = DB::table('claims')->where('claims.status', '=', '1')
                    ->where('claims.dispatched', Input::get('claimStatus'))
                    ->where('claims.is_closed', '0')
                    ->where('claims.step','<', 1)
                    ->select(["claims.id", "claims.policy_id", "claims.created_at", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(claims.created_at, '%d/%m/%Y') as created_at"), "policies.number", DB::raw('sum(amount) AS amount'),'claims.step', DB::raw('DATEDIFF(CURDATE(), MAX(claims.created_at)) as days_passed')])
                    ->join('applicants', 'claims.policy_id', '=', 'applicants.policy_id')
                    ->join('policies', 'policies.id', '=', 'applicants.policy_id')
                    ->join('claim_documents', 'claim_documents.id_claim', '=', 'claims.id')
                    ->groupby('claims.id')
                    ->get();
         }
        foreach ($claims as $claim){

            $id = $claim->id;
            $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
            ->whereHas('diagnostics2', function ($query) use ($id) {
                $query->where('claim_id', $id);
            })
            ->whereHas('diagnostics', function ($query) use ($id) {
                $query->where('id_doc_type', 1);
            })    ->get();

            $allliquidated = (count($document)>0) ? 1 : 0;

            foreach($document as $value){
                if ($value->liquidated == 0){
                    $allliquidated = 0;
                }


            }
            $claim->allliquidated = $allliquidated;
        }

        foreach ($claims as $claim){
            $id = $claim->id;
            $diagnostic = Diagnostic::where('status',1)
                          ->where('claim_id', $id)  
                          ->get();   
                          
            $withDiagnostic = (count($diagnostic)>0) ? 1 : 0;

            $claim->withDiagnostic = $withDiagnostic;

        }
        $long="";
        foreach ($claims as $claim) {
            $id = $claim->id;
            $claim_documents = ClaimDocument::where('id_claim',$id)->where('status','1')->where('returned',0)->get();
            $warning_documents = [];

            foreach ($claim_documents as $value){
                $diagnostic =DocumentDiagnostic::where('document_id',$value->id)->where('status','1')->first();
                if ($diagnostic == null){
                  $warning_documents[] = $value;  
                }
            }
            $long = count($warning_documents);

            

            $claim->long=$long;
            $claim->warning_documents =$warning_documents;
        }
        


        return response()->success(compact('claims'));
    }

     /**
     * Create new clam.
     *
     * @return JSON
     */
    public function postClaims()
    {
        $usercreate = Auth::user();
        $showhidepregnant = 0;        
        if (Input::get('have_prev_safe') == true) {
            $showhidepregnant = 1;
        }
        
        $policy_id = Input::get('policy_id');
        if (isset($policy_id)){
            $claim= Claim::create([
            'policy_id' => Input::get('policy_id'),
            'user_id_creation' => $usercreate->id
        ]);
        }

        $id_claim = Input::get('id_claim');
        $doctypes = Input::get('id_doc_types');
        $currency_ids= Input::get('currency_id');
        $provider_ids = Input::get('provider_id');
        $amounts= Input::get('values');
        $descriptions = Input::get('descriptions');
        $id_claim_document_type_descriptions= Input::get('id_claim_document_type_descriptions');
        $filenames = Input::get('filenames');

        $document_date = Input::get('document_date');
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->policy_file_directory)) ? $directory[0]->policy_file_directory : 0;

        for ($i = 0; $i < count($doctypes) ; $i++ ){//creacion de reclamo
            
            $policydocument = ClaimDocument::create([
                'id_claim' => (isset($id_claim)) ? $id_claim : $claim->id,
                'id_doc_type' => $doctypes[$i],
                'id_claim_document_type_descriptions'=>$id_claim_document_type_descriptions[$i],
                'currency_id' => $currency_ids[$i],
                'provider_id' => $provider_ids[$i],
                'amount' => $amounts[$i],
                'description' => $descriptions[$i],
                'document_date' => $document_date[$i],
                'directory' => (isset($id_claim)) ? $directory.'/'.'claims/'.$id_claim : ' ',
                'filename' => (isset($id_claim)) ? $filenames[$i] : '',
                'user_id_creation' => $usercreate->id
            ]);
        }

        $modified = Input::get('modified');
        var_dump($modified);
        if (isset($modified)){
            foreach ($modified as $value) {
                $affectedRows = ClaimDocument::where('id', '=', $value['id'])->update([
                'id_doc_type' => $value['id_doc_type'],
                'id_claim_document_type_descriptions' => $value['id_claim_document_type_descriptions'],
                'currency_id' => $value['currency_id'],
                'provider_id' => $value['provider_id'],
                'amount' => $value['amount'],
                'description' => $value['description'],
                'document_date' => date("Y-m-d h:i:s", strtotime($value['document_date'])),
                'filename' => $value['filename'],
                'user_id_update' => $usercreate->id]
                );//update
            }
        }

        $document_ids = Input::get('deleted_ids');
        if (isset($document_ids)){
            foreach ($document_ids as $value) {
                $affectedRows = ClaimDocument::where('id', '=', $value)->update([
                    'status' => 0,
                    'user_id_update' => $usercreate->id,
                ]);//delete
            }
        }

        $returned = Input::get('returned');
        if (isset($document_ids)){
            foreach ($returned as $value){
                if ($value['return'] == false){
                    ClaimDocument::where('id', $value['id'])->update([
                    'returned' => 0,
                    'user_id_update' => $usercreate->id,
                ]);
                }
            }
        }


       return (isset($claim)) ? response()->success($claim->id) : response()->success('success');
       return response()->success($directory);
    }

    public function deleteClaim($id)
    {
        $claimData = [
            'dispatched' => 1,
        ];
        $affectedRows = Claim::where('id', '=', $id)->update($claimData);
        return response()->success('success');
    }

    public function closedClaim(){
        $userupdate = Auth::user();
        $claimData = [
            'is_closed' => 1,
            'step' => 3,
            'user_id_update' => $userupdate->id,
        ];
        $affectedRows = Claim::where('id', '=', Input::get('id_claim'))->update($claimData);
        return response()->success('success');
    }

    public function postDocuments(){
        $user = Auth::user();
        $id_claim = Input::get('id_claim');

        /*-------new files-------------*/
        $doctypes = Input::get('id_doc_types');
        $currency_ids= Input::get('currency_ids');
        $amounts= Input::get('amounts');
        $descriptions = Input::get('descriptions');
        $filenames = Input::get('filenames');
         /*---------------------------*/
        //$document_ids = Input::get('ids');//files to delete
        //$modified = Input::get('modified');//files to update


        /*-------------INSERT---------*/
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->policy_file_directory)) ? $directory[0]->policy_file_directory : 0;

        for ($i = 0; $i < count($doctypes) ; $i++ ){//Insert
            $policydocument = PolicyDocument::create([
                'id_claim' => $id_claim,
                'id_doc_type' => $doctypes[$i],
                'currency_id' => $currency_ids[$i],
                'amount' => $amounts[$i],
                'description' => $descriptions[$i],
                'directory' => $directory.'/'.$id_claim,
                'filename' => $filenames[$i],
                'user_id_creation' => $user->id
            ]);
        }
        /*---------------------------*/


        /*-------Delete-------------*/
        /*$documentData = [
            'status' => 0,
            'user_id_update' => $user->id,
        ];
        foreach ($document_ids as $value) {
            $affectedRows = PolicyDocument::where('id', '=', $value)->update($documentData);//delete
        }*/
         /*--------------------------*/

        /*-------Update-------------*/
        /*foreach ($modified as $value) {
            $affectedRows = PolicyDocument::where('id', '=', $value['id'])->update([
            'description' => $value['description'],
            'id_doc_type' => $value['id_doc_type'],
            'filename' => $value['filename'],
            'user_id_update' => $user->id]
            );//update
        }*/
        /*--------------------------*/


        return response()->success('success');
    }

    public function getShow($id)
    {
        $claimdocument = ClaimDocument::where('id_claim',$id)
                                        ->where('claim_documents.status','1')
                                        ->with('documents')                   
                                        ->get();


        foreach ($claimdocument as $key=> $value){
            $claimdocument[$key]->id_doc_type = (int)$claimdocument[$key]->id_doc_type;
            $claimdocument[$key]->id_claim_document_type_descriptions = (int)$claimdocument[$key]->id_claim_document_type_descriptions;
            //$claimdocument[$key]->amount = ($claimdocument[$key]->amount == NULL) ? $zero : (float)$claimdocument[$key]->amount;
            $claimdocument[$key]->amount = (float) $claimdocument[$key]->amount;
            $claimdocument[$key]->currency_id = (int)$claimdocument[$key]->currency_id;
            $claimdocument[$key]->provider_id = (int)$claimdocument[$key]->provider_id;
            //$claimdocument[$key]->documents->pending_doc = ($claimdocument[$key]->documents->pending_doc == NULL) ? null : (int)$claimdocument[$key]->documents->pending_doc;
            $claimdocument[$key]->documents == null ? $claimdocument[$key]->documents = null : $claimdocument[$key]->documents->pending_doc = (int)$claimdocument[$key]->documents->pending_doc;
        }

        return response()->success($claimdocument);
    }


    public function getShowdoc($id)

    {
     $claimdocumentuno= ClaimDocument::where('id_claim',$id)->where('claim_documents.status','1')->where('claim_documents.returned',0)
          ->select(["claim_documents.id","claim_documents.id_doc_type", "claim_documents.filename", "document_diagnostics.document_id", "claim_documents.directory", "document_diagnostics.diagnostic_id"])
          ->join('document_diagnostics', 'claim_documents.id','=','document_diagnostics.document_id')
          ->where('claim_documents.id_doc_type','=',1)->groupBy('id');

        //      ->get();

        $claimdocument = ClaimDocument::where('id_claim',$id)->where('claim_documents.status','1')->where('claim_documents.returned',0)
        ->select(["claim_documents.id","claim_documents.id_doc_type", "claim_documents.filename", "document_diagnostics.document_id", "claim_documents.directory", "document_diagnostics.diagnostic_id"])
        ->leftJoin('document_diagnostics','document_diagnostics.document_id','=','claim_documents.id')
        ->whereNull('document_diagnostics.document_id')
        ->union($claimdocumentuno)
        ->get();



        foreach ($claimdocument as $key=> $value){
            $claimdocument[$key]->id_doc_type = (int)$claimdocument[$key]->id_doc_type;
            //$claimdocument[$key]->amount = ($claimdocument[$key]->amount == NULL) ? $zero : (float)$claimdocument[$key]->amount;
            $claimdocument[$key]->amount = (float) $claimdocument[$key]->amount;
            $claimdocument[$key]->currency_id = (int)$claimdocument[$key]->currency_id;
            $claimdocument[$key]->provider_id = (int)$claimdocument[$key]->provider_id;
        }

        return response()->success($claimdocument);


    }

    public function getDocumentswarning($claim_id){
        $claim_documents = ClaimDocument::where('id_claim',$claim_id)->where('status','1')->where('returned',0)->get();
        $warning_documents = [];

        foreach ($claim_documents as $value){
            $diagnostic =DocumentDiagnostic::where('document_id',$value->id)->where('status','1')->first();
            if ($diagnostic == null){
              $warning_documents[] = $value;

            }
        }

        return response()->success($warning_documents);
    }

    public function getShowDocInfo(){
        $id = Input::get('claimId');
        $diagnosticId = Input::get('diagnosticId');
        $showdocinfo = [];

        $showdocinf = ClaimDocument::where('id_claim',$id)->where('status','1')->where('returned',0)->with('documents')->get();

        foreach ($showdocinf as $key=> $value){
            if (($showdocinf[$key]->documents != null)  && ($showdocinf[$key]->documents->status == '1') && ($showdocinf[$key]->documents->diagnostic_id == $diagnosticId))
                $value->assigned = true;
            else
                $value->assigned = false;

            if ($showdocinf[$key]->documents != null){
                $search = DocumentDiagnostic::where('diagnostic_id','<>',$diagnosticId)->where('document_id',$showdocinf[$key]->documents->document_id)->with('diagnostics')->whereHas(

                    'diagnostics', function ($query) {
                    $query->where('id_doc_type','<>',1);
                })
                ->first();

                if (!$search)
                    $showdocinfo[] = $value;
            }

        }

        return response()->success(compact('showdocinfo'));
    }

    public function getClaiminvoices($id){
        $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
        ->whereHas('diagnostics2', function ($query) use ($id) {
            $query->where('claim_id', $id);
        })
        ->whereHas('diagnostics', function ($query2)  {
            $query2->where('id_doc_type', 1);
        })->get();

        foreach($document as $value){
            if ($value->diagnostics2->person_type == 'A'){
                $apllicant = Applicant::where('id',$value->diagnostics2->id_person)->where('status',1)->first();
                $value->names = $apllicant->first_name. ' '.$apllicant->last_name;
            }
            else{
                $dependent = Dependent::where('id',$value->diagnostics2->id_person)->where('status',1)->first();
                $value->names = $dependent->first_name. ' '.$dependent->last_name;
            }
            $value->diagnostics->currency_id = (int) $value->diagnostics->currency_id;
            $value->diagnostics->amount = (float) $value->diagnostics->amount;
            ($value->liquidated == 1) ? $value->checked = true : $value->checked = false;
        }

        return response()->success($document);
    }

    public function postLiquidate(){
        $invoices = Input::get('invoices');

        foreach($invoices as $value){
            DocumentDiagnostic::where('id',$value['id'])->update(['liquidated' => ($value['checked'] == true) ? 1 : 0]);
        }

        return response()->success('success');
    }

    public function getEobdata($id){
         $diagnostic_id = $id;

         $diagnostics = Diagnostic::where('diagnostics.status',1)->where('diagnostics.id',$diagnostic_id)
                                    ->where('claim_documents.id_doc_type',1)
                                    ->where('document_diagnostics.diagnostic_id','=',$diagnostic_id)
                                    ->where('document_diagnostics.returned','=',0)
                                    //->join('claim_documents', 'document_diagnostics.document_id', '=', 'claim_documents.id')
                                    //->join('claims','claim_documents.id_claim', '=', 'claims.id')
                                    ->join('document_diagnostics','document_diagnostics.diagnostic_id', '=', 'diagnostics.id')
                                    ->join('claim_documents', 'document_diagnostics.document_id', '=', 'claim_documents.id')
                                    //->distinct()
                                    ->get(['claim_documents.id as claim_doc_id','claim_documents.id_claim as id','claim_documents.amount','document_diagnostics.eob_number','claim_documents.id_doc_type', 'claim_documents.currency_id','claim_documents.id_claim', 'claim_documents.liquidated','diagnostics.id as diagnostic_id', 'diagnostics.id_person as id_person', 'diagnostics.person_type as person_type','document_diagnostics.id as diagnostic_doc_id', 'document_diagnostics.liquidated as docDiagLiquidated', 'document_diagnostics.returned as returned']); 


        //['claim_documents.id as claim_doc_id','claim_documents.amount','claim_documents.eob_number','claim_documents.id_doc_type', 'claim_documents.currency_id','claim_documents.id_claim', 'claim_documents.liquidated','diagnostics.id as diagnostic_id']
          foreach ($diagnostics as  $value) {
                                        
                                    }                          
         return response()->success($diagnostics);
    }



    public function updateEob(Request $request)
    {
        $userupdate = Auth::user();
        $id = $request['docDiagnosticId'];
        $diagnosticId = $request['diagnosticId'];
        $dateData = [
            'eob_number'=>$request['eob_number'],
            'liquidated'=> 1,
            //'user_id_update'=>$userupdate->id
        ];

        $affectedRows = DocumentDiagnostic::where('id', '=', $id)->update($dateData);
        $claimId = Diagnostic::where('id', '=', $diagnosticId)->select(['id as claimId'])->first();
       // $this->postClaimstep($diagnosticId, $claimId);

        return response()->success(compact('affectedRows'));

    }

    public function postPending(){
        $id = Input::get('id');
        $affected_rows = DocumentDiagnostic::where('id',$id) ->update([
            'pending_doc' => 0
        ]);
        return response()->success('success');
    }




     public function postPrintdoc(){



        $user = Auth::user();

        $diagnosticId = Input::get('diagnosticId');
        $claim_id = Input::get('claim_id');
        $claim = Claim::find($claim_id);
        $policy_id = $claim->policy_id;
    

        $policy = Policy::find($policy_id);
        $policy_number = $policy->number;

        $applicant = Applicant::where ('policy_id', $policy_id)->first();
        $name = $applicant->first_name.' '.$applicant->last_name;

        $pages = array();

       
        foreach ($diagnosticId as $value) {
        $content = "<body>";
        $content .= "<font face='Arial'>";
        $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $content .= "<p align='right'>Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y')."</p>";
        $content.= "<p style='font-family=Arial'>Señores <BR> <strong>BEST DOCTORS</strong> <BR> Ciudad.- <BR> <BR> <BR> Atte. Dpto. de Reclamos <BR><BR>De mis consideraciones: <BR><BR>Por medio del presente adjunto el siguiente reclamo:
            <br/><br/>
                    <blockquote><strong>Asegurado:</strong>&nbsp;&nbsp;".$applicant->first_name.' '.$applicant->last_name."<BR>
                    <strong>Poliza:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$policy_number</blockquote>
                    <br>";
        $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                        <thead>
                            <tr>
                                <th align='center'>Fecha Enviado</th>
                                <th align='center'>#GB</th>
                                <th align='center'>Titular</th>
                                <th align='center'># Póliza</th>
                                <th align='center'>Paciente</th>
                                <th align='center'>Diagnóstico</th>
                                <th align='center'>Monto</th>
                            </tr>
                        </head>
                        <tbody>";
        
       
            $amount = 0;
            $document = DocumentDiagnostic::with('diagnostics')->where('status',1)->where('diagnostic_id',$value["id"])->get();

            foreach ($document as $value2){
                $amount+= $value2->diagnostics->amount;
            }

            $diagnostics = Diagnostic::where('status',1)->where('id',$value["id"])->first();
            //var_dump($diagnostics->person_type);
            //var_dump($diagnostics->id_person);

            if ($diagnostics->person_type == 'A'){
                $content.="<tr>
                    <td align='center'>".date('d-m-Y')."</td>
                    <td align='center'>".$claim_id."</td>
                    <td align='center'>".$name."</td>
                    <td align='center'>".$policy_number."</td>
                    <td align='center'>".$name."</td>
                    <td align='center'>".$diagnostics->description."</td>
                    <td align='center'>".number_format((float)$amount,2,'.','')."</td>
                </tr>";
            }
            else{
                $dependent = Dependent::where('id',$diagnostics->id_person)->first();
               // var_dump($dependent);
                $content.="<tr>
                    <td align='center'>".date('d-m-Y')."</td>
                    <td align='center'>".$claim_id."</td>
                    <td align='center'>".$name."</td>
                    <td align='center'>".$policy_number."</td>
                    <td align='center'>".$dependent->first_name.' '.$dependent->last_name."</td>
                    <td align='center'>".$diagnostics->description."</td>
                    <td align='center'>".number_format((float)$amount,2,'.','')."</td>
                </tr>";
            }
       

        $content.=" </tbody>
                    </table>";

        $content .= '<BR>Detalles de los Documentos<BR><BR>';
        $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                        <thead>
                            <tr>
                                <th align='center'>Facturas</th>
                                <th align='center'>Fecha</th>
                                <th align='center'>Proveedor</th>
                                <th align='center'>Valor</th>
                            </tr>
                        </head>
                        <tbody>";


        $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
        ->whereHas('diagnostics2', function ($query) use ($value) {
            $query->where('diagnostic_id', $value["id"]);
        })
        ->whereHas('diagnostics', function ($query2)  {
            $query2->where('id_doc_type', 1);
        })->groupBy('document_id')->get();

        $total = 0;
        foreach ($document as $value){
           $provider = Provider::where('id',$value->diagnostics->provider_id)->first();

           // var_dump("Id del provider ".$value->diagnostics->provider_id);

            $content.="<tr>
                    <td align='center'>".$value->diagnostics->description."</td>
                    <td align='center'>".date('d-m-Y', strtotime($value->diagnostics->document_date))."</td>
                    <td align='center'>".$provider->name."</td>
                    <td align='center'>".number_format((float)$value->diagnostics->amount,2,'.','')."</td>
                </tr>";

            $total += $value->diagnostics->amount;
        }
        $total = number_format((float)$total,2,'.','');
        $content.="<tr>
                    <td></td>
                    <td align='center'><strong>Total</strong></td>
                    <td align='center'></td>
                    <td align='center'>$total</td>
                    </tr>
                    </tbody>
                    </table><BR>";
        
       $document2 = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
        ->whereHas('diagnostics2', function ($query) use ($value) {
            $query->where('diagnostic_id',  $value["diagnostic_id"]);
        })
        ->whereHas('diagnostics', function ($query2)  {
            $query2->where('id_doc_type','<>',  1);
        })->groupBy('document_id')->get();
        
        if(count($document2) > 0 ){
            $content .= '<BR>Documentos Adicionales<BR><BR>';
            $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                        <thead>
                            <tr>
                                <th align='center'>Tipo de Documento</th>
                                <th align='center'>Fecha</th>
                                <th align='center'>Descripción</th>
                            </tr>
                        </head>
                        <tbody>";
            
            foreach ($document2 as $value){
            if ($value->diagnostics->id_doc_type == 1){
                $doc_type = 'Factura';
            }
            elseif ($value->diagnostics->id_doc_type == 2){
                $doc_type = 'Informe';
            }
            elseif ($value->diagnostics->id_doc_type == 3){
                $doc_type = 'Orden';
            }
            else{
                $doc_type = 'Resultado';
            }

            $content.="<tr>
                    <td align='center'>".$doc_type."</td>
                    <td align='center'>".date('d-m-Y',strtotime($value->diagnostics->document_date))."</td>
                    <td align='center'>".$value->diagnostics->description."</td>
                </tr>";
            }

            $content.="</tbody>
                        </table><BR>";

            
        }
        

        $content.= "Sin otro particular por el momento, sucribo con un cordial saludo. <BR>";

        $content.= '<BR> <BR> Atentamente, <BR> <BR><strong>'.$user->name.'</strong>';

        $content.= '<BR> <BR> <BR> <BR> <BR> <BR> <BR> <BR>';
        $content.= '</body>';
        //$content.= "-------------------------------------------------------------------------------------";         
        $pages[] = $content;

     } //fin foreach
        return response()->success($pages);


    }


    function postDispatcheddoc(){
        $diagnostic_id = Input::get('diagnostic_id');
        $response = Diagnostic::where('id',$diagnostic_id)
                                ->select('dispatched')
                                ->first();
        return response()->success($response);

    }

    public function postClaims2()
    {
        $usercreate = Auth::user();
        $showhidepregnant = 0;        
        $id_diagnostic = Input::get('id_diagnostic');
        $id_claim = Input::get('id_claim');
        $doctypes = Input::get('id_doc_types');
        $currency_ids= Input::get('currency_id');
        $provider_ids = Input::get('provider_id');
        $amounts= Input::get('values');
        $descriptions = Input::get('descriptions');
        $filenames = Input::get('filenames');

        $document_date = Input::get('document_date');
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->policy_file_directory)) ? $directory[0]->policy_file_directory : 0;

        
        
        for ($i = 0; $i < count($doctypes) ; $i++ ){
            if(Input::get('document_date') == null){
                $document_date[$i] =  date("d-m-Y h:i:s");
             }
            $claimdocument = ClaimDocument::create([
                'id_claim' =>  $id_claim,
                'id_doc_type' => $doctypes[$i],
                'currency_id' => $currency_ids[$i],
                'provider_id' => $provider_ids[$i],
                'amount' => $amounts[$i],
                'description' => $descriptions[$i],
                'document_date' => date("Y-m-d h:i:s", strtotime($document_date[$i])),
                'directory' => (isset($id_claim)) ? $directory.'/'.'claims/'.$id_claim : ' ',
                'filename' => (isset($id_claim)) ? $filenames[$i] : '',
                'user_id_creation' => $usercreate->id
            ]);
            $documentId = $claimdocument->id;
            $diagnosticdocument = DocumentDiagnostic::create([
                'diagnostic_id' => $id_diagnostic,
                'document_id' => $documentId,
                'user_id_creation' => $usercreate->id
            ]);
        }

        //var_dump($documentId);
    
       return response()->success('success');
       
    }

    public function getClaimdiagnostics(){
       



        $diagnostics = Diagnostic::where('diagnostics.status',1)
                       ->where('diagnostics.dispatched',1)
                       ->join('claims','diagnostics.claim_id', '=', 'claims.id')                       
                       ->join('policies','claims.policy_id', '=', 'policies.id')  
                       ->join('claim_documents', 'claim_documents.id_claim', '=', 'claims.id') 
                        ->select(['diagnostics.*', DB::raw('CONCAT(policies.first_name," ",policies.last_name) AS titular'),'policies.number as number','policies.id as policy_id'])                  
                       ->groupby('diagnostics.id')
                       ->get();
        
        foreach($diagnostics as $value){
            $diagnosticDoc = DocumentDiagnostic::where('document_diagnostics.diagnostic_id',$value->id)
                                                    ->where('document_diagnostics.liquidated','0')
                                                    ->where('document_diagnostics.returned','0')
                                                    ->where('claim_documents.id_doc_type','1')
                                                    ->join('claim_documents','document_diagnostics.document_id','=','claim_documents.id')
                                                    ->select(['document_diagnostics.*','claim_documents.filename','claim_documents.amount', 'claim_documents.id_doc_type'])
                                                    ->get();
                

                $liquidated = 0;
                $returned = 0;
                $count = 0;
                $value->amountTotal = 0;                   
                $value->show = false;
                //var_dump($diagnosticDoc);
                foreach($diagnosticDoc as $value2){
                    
                    
                    $value2->checked = false;
                    if($value2->amount == null)
                    {
                        $value->amountTotal = 0;
                    }else{
                        $value->amountTotal +=  $value2->amount;
                    }
                    
                    
                    if($value2->liquidated == 1){
                            $liquidated++;
                    } 

                    if($value2->returned == 1){
                            $returned++;
                    } 

                    $count++;
                    $totalDoc = $liquidated + $returned;
                

                    if( $count == $totalDoc ||  $value->amountTotal == 0 ){
                        $value->show = false;
                    }else{
                        $value->show = true;
                    }  
 
            }

                

          if($value->person_type == "D"){
                //var_dump("Entro porque es D");
                $dependent = Dependent::where('id',$value->id_person)->first();
                $value->name = $dependent->first_name.' '.$dependent->last_name;
                $value->person_type = "Dependiente";
            }else{
                $value->name = $value->titular;
                $value->person_type = "Titular";
            }

                       
        }

        
        return response()->success($diagnostics);
    }

    public function postClaimstep(){
        $userupdate = Auth::user();
        $diagnosticId = Input::get('diagnostic_id');
        $claimId = Input::get('claimId');
        $cantLiquidated = 0;
        $cantReturned = 0;
        $step = 0;
         $diagnostic_invoices = DocumentDiagnostic::where('document_diagnostics.diagnostic_id', '=', $diagnosticId)
                               ->where('claim_documents.id_doc_type','=',1) 
                               ->join('claim_documents', 'claim_documents.id', '=', 'document_diagnostics.document_id')  
                                ->select(["document_diagnostics.diagnostic_id as id_diagnostic","document_diagnostics.liquidated as liquidated", "document_diagnostics.returned as returned","claim_documents.id_claim as claimId","claim_documents.id_doc_type as doc_type"]) 
                               ->get();

        $docs =  count($diagnostic_invoices);                       
       
        foreach ($diagnostic_invoices as  $value) {
            if($value['liquidated'] == 1){
                $cantLiquidated++;
            }                      
            if($value['returned'] == 1){
                $cantReturned++;
            }                      
        }                      

        $totalDoc = $cantLiquidated + $cantReturned;
       
        if($docs == $totalDoc){
            if($cantReturned > 0){
                $step = 5; // Liquidacion parcial..al menos una factura fue retornada
            }else{
                $step = 4; // Liquidadas todas las facturas del Reclamo
            }

        $data = ['step' => $step, 'is_closed' => 1,  'user_id_update'=>$userupdate->id];    
        $affectedRows = Claim::where('id', '=', $claimId)->update($data);  
        }
         return response()->success('success');
        
    }


    public function postPrintdocpdf(){



        $user = Auth::user();
        $content = [];
        $final_content = [];
        $diagnosticId = Input::get('diagnosticId');
        $claim_id = Input::get('claim_id');
        $claim = Claim::find($claim_id);
        $policy_id = $claim->policy_id;
    

        $policy = Policy::find($policy_id);
        $policy_number = $policy->number;

        $applicant = Applicant::where ('policy_id', $policy_id)->first();
        $name = $applicant->first_name.' '.$applicant->last_name;

       $pages = array();
       $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
       $date = "Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y');

        foreach ($diagnosticId as $value) {
          $rowData1 = [];
          $rowData2 = [];
          
          $amount = 0;
            $document = DocumentDiagnostic::with('diagnostics')->where('status',1)->where('diagnostic_id',$value["id"])->get();

            foreach ($document as $value2){
                $amount+= $value2->diagnostics->amount;
            }

            $diagnostics = Diagnostic::where('status',1)->where('id',$value["id"])->first();  
            
            if ($diagnostics->person_type == 'A'){
                $affiliate = $name;
                $rowData = ["date"=>date('d-m-Y'), "claim_id"=>$value["id"],"applicant"=>$name,"policy_number"=> $policy_number, "affiliate"=>$name, "diagnostic"=>$diagnostics->description, "amount"=>number_format((float)$amount,2,'.','')];
            }
            else{
                $dependent = Dependent::where('id',$diagnostics->id_person)->first();
               // var_dump($dependent);
                $affiliate = $dependent->first_name.' '.$dependent->last_name; 
                $rowData = ["date"=>date('d-m-Y'), "claim_id"=>$value["id"],"applicant"=>$name,"policy_number"=> $policy_number, "affiliate"=>$affiliate, "diagnostic"=>$diagnostics->description, "amount"=>number_format((float)$amount,2,'.','')];
            }
       

       


        $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
        ->whereHas('diagnostics2', function ($query) use ($value) {
            $query->where('diagnostic_id', $value["id"]);
        })
        ->whereHas('diagnostics', function ($query2)  {
            $query2->where('id_doc_type', 1);
        })->groupBy('document_id')->get();
        /******************** FACTURAS **************************/
        $total = 0;
        foreach ($document as $value){
            $provider = Provider::where('id',$value->diagnostics->provider_id)->first();
            $Data1 = ["factura"=>$value->diagnostics->description, "fecha_factura"=>date('d-m-Y',strtotime($value->diagnostics->document_date)), "provider"=> $provider->name, "monto_factura"=>number_format((float)$value->diagnostics->amount,2,'.','')];
            
            //$total += $value->diagnostics->amount;
            array_push($rowData1, $Data1);

            $total += $value->diagnostics->amount;
        }
        $total = number_format((float)$total,2,'.','');
       /****************************************************************/
       /*****************OTROS DOCS ********************************/
        
       $document2 = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
        ->whereHas('diagnostics2', function ($query) use ($value) {
            $query->where('diagnostic_id',  $value["diagnostic_id"]);
        })
        ->whereHas('diagnostics', function ($query2)  {
            $query2->where('id_doc_type','<>',  1);
        })->groupBy('document_id')->get();
        
        if(count($document2) > 0 ){
            $Data2 = [];
            
            foreach ($document2 as $value2){
            if ($value2->diagnostics->id_doc_type == 1){
                $doc_type = 'Factura';
            }
            elseif ($value2->diagnostics->id_doc_type == 2){
                $doc_type = 'Informe';
            }
            elseif ($value2->diagnostics->id_doc_type == 3){
                $doc_type = 'Orden';
            }
            else{
                $doc_type = 'Resultado';
            }

             $Data2 = ["doc_type"=>$doc_type, "date"=>date('d-m-Y',strtotime($value2->diagnostics->document_date)), "description_other"=>$value2->diagnostics->description];
            array_push($rowData2, $Data2);
            }

            
        }else{
              $rowData2 = [];
        }
        

        $content = ["claim_id" => $claim_id, "date" => $date, "applicant" => $name, "policy_number" => $policy_number, "affiliate" => $affiliate, "total"=>$total, "user"=>$user->name, "rowData"=>$rowData, "rowData1"=>$rowData1, "rowData2"=>$rowData2];

        array_push($final_content, $content);
        //$content.= "-------------------------------------------------------------------------------------";         
        $pages[] = $content;

     } //fin foreach
        return response()->success($final_content);


    }



}
