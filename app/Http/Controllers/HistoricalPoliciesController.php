<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Policy;
use App\Applicant;
use Auth;
use Input;
use DB;

use App\Configuration;
use App\Amendment;
use App\Annex;
use App\Fee;
use App\Payment;
use App\Attached;
use App\Policystep;
use App\PolicyApplicant;
use App\Dependent;
use App\PoliciesEffectiveDate;
use App\DeductibleDetail;
use App\Coverage;

class HistoricalPoliciesController extends Controller
{
    //
    function postPolicies(){
       $configurations = Configuration::all(); 
       $usercreate = Auth::user();
       $current_year = date ('Y');
       $current_month = date ('m');
       $policy_year = date('Y', strtotime(Input::get('date_start_coverage')));
       $policy_month = date('m', strtotime(Input::get('date_start_coverage')));
         //var_dump($current_year) ;   
       $dependents = Input::get('dependents');
       $fees = Input::get('fees');
       $number  = Input::get('number');
       $deductible_id = Input::get('deductible_id');
         $coverage_id = Input::get('coverage_id');
         $mode = Input::get('mode');
         $diffFees =  Input::get('diffFee');
         //var_dump($diffFees) ;
      
        $policy = Policy::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'email' => Input::get('email'),
            'agent_id' => Input::get('agent_id'),
            'plan_id' => Input::get('plan_id'),
            'start_date_coverage' => Input::get('date_start_coverage'),
            'user_id_creation' => $usercreate->id
        ]);

        if ($policy->id) {
            $applicant = Applicant::create([
            'policy_id' => $policy->id,
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'address' => Input::get('address'),
            'civil_status' => Input::get('civil_status'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city_id' => Input::get('city_id'),
            'phone' => Input::get('phone'),
            'mobile' => Input::get('mobile'),
            'place_of_birth' => Input::get('place_of_birth'),
            'occupation'  => Input::get('occupation'),
            'identity_document' => Input::get('identity_document'),
            'email' => Input::get('email'),
            'gender' => Input::get('gender'),
            'birthday' => Input::get('birthday'),
            'height' => Input::get('height'),
            'height_unit_measurement' => (Input::get('height_unit_measurement') == 'M') ? '1' : '0' ,
            'weight' => Input::get('weight'),
            'weight_unit_measurement' => (Input::get('weight_unit_measurement') == 'KG') ? '1' : '0',
            'user_id_creation' => $usercreate->id
        ]);

            $policystep = Policystep::create([
              'id_policy' => $policy->id,
              'user_id_creation' => $usercreate->id
            ]);

             Policy::where('id',$policy->id)->update(['number' => $number, 
                              'deductible_id' => $deductible_id, 
                              'coverage_id' => $coverage_id,
                              'send_to_reception' => '1',
                              'customer_response' => '1',
                              'completed' => '1',
                              'user_id_update' => $usercreate->id]);

              Payment::create([
                'policy_id' => $policy->id,
                'mode' => Input::get('mode'),
                'method' => Input::get('method'),
                'payment_date' => Input::get('payment_date'),
                'total_value' => Input::get('amount'),
                'user_id_creation' => $usercreate->id
             ]);

             }

           

        $aplicantid = $applicant->id;
        $childs = 0;
        $ageh = 0;
        for ($i = 0; $i < count($dependents); $i++){

            if($dependents[$i]['role'] == "H"){
               list($Y,$m,$d) = explode("-", $dependents[$i]['dob']);
               $ageh =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ;
            }else{
              $childs = $childs + 1; 
            }


            $dependent = Dependent::create([
                'applicant_id' => $aplicantid,
                'first_name' => $dependents[$i]['name'],
                'last_name' => $dependents[$i]['lastname'],
                'document_type' => $dependents[$i]['pid_type'],
                'identity_document' => $dependents[$i]['pid_num'],
                'relationship' => $dependents[$i]['role'],
                'birthday' => $dependents[$i]['dob'],
                'height' => $dependents[$i]['height'],
                'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                'weight' => $dependents[$i]['weight'],
                'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                'gender' => $dependents[$i]['sex'],
                'user_id_creation' => $usercreate->id
            ]);
        }
 
        $monthDiff =  $policy_month - $current_month;
        $yearDif = $current_year - $policy_year;
       // var_dump($monthDiff);


       /* if($monthDiff <= 0 && $yearDif <= 1 && $mode == 0){
          $yearDiff =  ($current_year - 1) - $policy_year ;  
        }else{
          $yearDiff =  $current_year - $policy_year;
        }*/
         //var_dump($yearDiff);
        if($policy_month >= $current_month){
          $yearDiff =  ($current_year - 1) - $policy_year ;  
        }
        else{
          $yearDiff =  $current_year - $policy_year;
        }

        $effective_date = date ( 'Y-m-d' , strtotime ( '+'.$yearDiff.' year' , strtotime(Input::get('date_start_coverage')) ));
        //var_dump($effective_date);
          if($mode == 0){
            $nfee = 1;
            $period = '+1 year';
            $yearDiff =  ($current_year - 1) - $policy_year ;  
          }
          elseif($mode == 1){
            $nfee = 2;
            $period = '+6 month';
            $yearDiff =  $current_year - $policy_year;
          }
          elseif($mode == 2){
            $nfee = 4;
            $period = '+3 month';
            $yearDiff =  $current_year - $policy_year;
          }
          else{
            $nfee = 12;
            $period = '+1 month';
            $yearDiff =  $current_year - $policy_year;
          }
     
      

      $fee_payment_date[0] = $effective_date;
        for ($i=1; $i < $nfee ; $i++) { 
          $fee_payment_date[$i]= date ( 'Y-m-d' , strtotime ( $period , strtotime($fee_payment_date[$i-1]) ) );   
        }
        
     /************************ Calculo de Montos de Fees ************************************/
      list($Y,$m,$d) = explode("-", Input::get('birthday'));
      $age =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ;


      $amount = DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($age." between age_start and age_end")->first();
       if($ageh > 0){
        $amounth =  DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($ageh." between age_start and age_end")->first(); 
      }else{
        $amounth = 0;
      }

       if (($childs > 0) && ($childs <= 3)){
            $amountc = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',$childs)->first();
        }
        elseif ($childs > 3){
            $amountc = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',3)->first();
        }
        else{
            $amountc = 0;
        }

        $coverage = Coverage::with('detail')->where('id',$coverage_id)->first();
        $amountCoverage = (isset($coverage->name)) ? (float) $coverage->detail->amount : 0;
        $amountCoverage = $amountCoverage/$nfee;
        $amountTitular =  (float) $amount->amount/$nfee ;
        $amountSpouse = (isset($amounth->amount)) ? (float) $amounth->amount/$nfee : 0;
        $amountChild = (isset($amountc->amount)) ? (float) $amountc->amount/$nfee : 0;
        $totaldetails =  $amountTitular +  $amountSpouse + $amountChild +  $amountCoverage;
        $installment_fee = 0;
        if($mode == 1){
            $installment_fee = (float) $totaldetails * 0.06;            
          }
        elseif($mode == 2 || $mode == 3){
           $installment_fee = (float) $totaldetails * 0.1;            
          }
          

        $taxscc = (float) $configurations[0]->value;
        $iva = (float) $configurations[2]->value;
        $totalTaxscc = ($totaldetails+$installment_fee)*($taxscc/100);
        $amountFee =  $totaldetails +  $installment_fee + $totalTaxscc;

     /***************************************************************************************/
       if($diffFees > 0){          
        $paid_fee = Fee::create([
                'policy_id' => $policy->id,
                'fee_number' =>  $fees,
                'fee_payment_date' => $fee_payment_date[$fees-1],
                'total' => Input::get('amount'),
                'amount_titular' => $amountTitular,
                'amount_spouse' => $amountSpouse,
                'amount_childs' => $amountChild,
                'method' => Input::get('method'),
                'payment_date' => Input::get('payment_date'),
                'user_id_creation' => $usercreate->id
            ]);

        Fee::where('id',$paid_fee->id)->update(['paid_fee' => 1, 
                            'payment_date' => Input::get('payment_date'), 
                            'user_id_update' => $usercreate->id]);  

              

          for ($i = $fees; $i < $nfee; ++$i) { 
             Fee::create([
                  'policy_id' => $policy->id,
                  'fee_number' => $i+1,
                  'fee_payment_date' => $fee_payment_date[$i],
                  'total' =>  $amountFee,
                  'amount_titular' => $amountTitular,
                  'amount_spouse' => $amountSpouse,
                  'amount_childs' => $amountChild,
                  'method' => Input::get('method'),
                  'user_id_creation' => $usercreate->id
              ]); 
            
          }

          PoliciesEffectiveDate::create([
            'start_date_coverage'  => $effective_date,
            'policy_id' => $policy->id,
            'user_id_creation' => $usercreate->id
          ]);
        
       }else{
          $paid_fee = Fee::create([
                'policy_id' => $policy->id,
                'fee_number' =>  $fees,
                'fee_payment_date' => $fee_payment_date[$fees-1],
                'total' => Input::get('amount'),
                'amount_titular' => $amountTitular,
                'amount_spouse' => $amountSpouse,
                'amount_childs' => $amountChild,
                'method' => Input::get('method'),
                'payment_date' => Input::get('payment_date'),
                'user_id_creation' => $usercreate->id
            ]);

        Fee::where('id',$paid_fee->id)->update(['paid_fee' => 1, 
                            'payment_date' => Input::get('payment_date'), 
                            'user_id_update' => $usercreate->id]);  

        $fee_payment_date[0] = $fee_payment_date[$fees-1];
        for ($i=1; $i <= $nfee ; $i++) { 
          $fee_payment_date[$i]= date ( 'Y-m-d' , strtotime ( $period , strtotime($fee_payment_date[$i-1]) ) );   
        }     
        //var_dump($fee_payment_date);
          for ($i = 1; $i <= $nfee; ++$i) { 
             Fee::create([
                  'policy_id' => $policy->id,
                  'fee_number' => $i,
                  'fee_payment_date' => $fee_payment_date[$i],
                  'total' =>  $amountFee,
                  'amount_titular' => $amountTitular,
                  'amount_spouse' => $amountSpouse,
                  'amount_childs' => $amountChild,
                  'method' => Input::get('method'),
                  'user_id_creation' => $usercreate->id
              ]); 
            
          }
          $effective_date = date ( 'Y-m-d' , strtotime ( '+1 year' , strtotime($effective_date)));  
        PoliciesEffectiveDate::create([
        'start_date_coverage'  => $effective_date,
        'policy_id' => $policy->id,
        'user_id_creation' => $usercreate->id
            ]);
        


       }

        return response()->success($policy->id);
       
    }

      public function getShow($number)
    {

        $policy = Policy::where('number', $number)->where('status', 1)->count();

        return response()->success($policy);
    }

    public function postAmendment(Request $request){

       $user = Auth::user();
        $policyId = $request['policyId'];
        $number = $request['number'];
        $amendments = $request['amendments'];
        $annexes = $request['annexes'];
        $effective_date=$request['effective_date'];

         foreach ($amendments as $value){
            $amendment = Amendment::create([
                'policy_id' => $policyId,
                'role_order' => $value['role_order'],
                'affiliate_id' => $value['affiliate'],
                'type' => $value['type'],
                'description' => $value['description'],
                'user_id_creation' => $user->id
            ]);
        }

         foreach ($annexes as $value){
            if ($value['effective_date']==null){
                $effec_date=$effective_date;
            }
            else
            {
                $effec_date=$value['effective_date'];
            }

            $annex = Annex::create([
                'policy_id' => $policyId,
                'affiliate_id' => $value['affiliate'],
                'annex' => $value['annex'],
                'effective_date' => $effec_date,
                'user_id_creation' => $user->id
            ]);
        }

         return response()->success(compact('confirm'));

    }
}
