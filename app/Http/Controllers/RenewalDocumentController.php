<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Configuration;
use Auth;
use Input;

use App\RenewalDocument;
use App\Policy;
use App\PolicyDocument;
use App\FeeStep;


class RenewalDocumentController extends Controller
{
     public function getShow($id)
    {
        $renewaldocument = DB::table('renewal_documents')->where('renewal_documents.status', '=', '1')
        			->where('id_policy',$id)
                    ->select(["renewal_documents.*", "doc_types.name", DB::raw('0 AS "table"')])
                    ->join('doc_types', 'renewal_documents.id_doc_type', '=', 'doc_types.id');
                    //->get();


         $policydocument = DB::table('policy_documents')->where('policy_documents.status', '=', '1')
                    ->where('id_policy',$id)
                    ->select(["policy_documents.*", "doc_types.name", DB::raw('1 AS "table"')])
                    ->join('doc_types', 'policy_documents.id_doc_type', '=', 'doc_types.id');
                    
          $renewaldocument =    $renewaldocument->union($policydocument)->get();    
                
          //var_dump($renewaldocument);

        foreach ($renewaldocument as $key=> $value){
            $renewaldocument[$key]->id_doc_type = (int)$renewaldocument[$key]->id_doc_type;
        }

        return response()->success($renewaldocument);
    }

    public function postDocuments(){
        $userupdate = Auth::user();
        $policy_id = Input::get('policy_id');
        $document_ids = Input::get('ids');//files to delete
        $modified = Input::get('modified');//files to modify
        $feeId = Input::get('feeId');
        /*-------new files-------------*/
        $doctypes = Input::get('id_doc_types');
        $descriptions = Input::get('descriptions');
        $filenames = Input::get('filenames');
                
         /*---------------------------*/

        /*$documentData = [
            'status' => 0,
            'user_id_update' => $userupdate->id,
        ];
        
        foreach ($document_ids as $value) {
            $affectedRows = RenewalDocument::where('id', '=', $value)->update($documentData);//delete
        }
        */
        
        foreach ($modified as $value) {
            if($value['table'] == 0){  
                $affectedRows = RenewalDocument::where('id', '=', $value['id'])->update([
                'description' => $value['description'],
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => $userupdate->id]
                );//modify
        }else{
            $affectedRows = PolicyDocument::where('id', '=', $value['id'])->update([
                'description' => $value['description'],
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => $userupdate->id]
                );

        }

        }
        //borrar arcivos en la BD
        foreach ($document_ids as $value) {
          if($value['table'] == 0){  
                $affectedRows = RenewalDocument::where('id', '=', $value['id'])->update([
                'status' => 0,
                'user_id_update' => $userupdate->id]
                );//delete
        }else{
            $affectedRows = PolicyDocument::where('id', '=', $value['id'])->update([
                'status' => 0,
                'user_id_update' => $userupdate->id]
                );

        }

        }


        
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->policy_file_directory)) ? $directory[0]->policy_file_directory : 0;

        $cuenta= count($doctypes);

        if ($cuenta > 0){
            for ($i = 0; $i < $cuenta ; $i++ ){//Insert
                $renewaldocument = RenewalDocument::create([
                'id_policy' => $policy_id,
                'id_doc_type' => $doctypes[$i],
                'description' => $descriptions[$i],
                'directory' => 'renewals/'.$policy_id,
                'filename' => $filenames[$i],
                'user_id_creation' => $userupdate->id
            ]);
            }
        }

         $fee = FeeStep::where('fee_id',$feeId)->first();
        if($fee ==  NULL){
            $fee = FeeStep::create([
                'fee_id' => $feeId,
                'policy_id' => $policy_id,
                'user_id_creation' => $userupdate->id
            ]);
        }
        $affectedRows = FeeStep::where('id','=',$fee->id)->update([
            'step2' => 1,
            'user_id_update' => $userupdate->id
        ]);
        return response()->success('success');

    }
}
