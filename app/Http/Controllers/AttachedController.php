<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Attached;

class AttachedController extends Controller
{
    public function getIndex(Request $request){
        $payment_id = $request->input('payment_id');

        $attacheds = Attached::where('payment_id',$payment_id)->where('status', '1')->get();
        return response()->success(compact('attacheds'));
    }
}
