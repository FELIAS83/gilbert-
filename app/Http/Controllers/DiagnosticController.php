<?php

namespace App\Http\Controllers;

use App\User;
use App\Diagnostic;
use App\DocumentDiagnostic;
use App\ClaimDocument;
use App\Claim;
use App\Dependent;
use App\Policy;
use App\Applicant;
use App\Provider;
use Auth;
use Input;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\NotificationController;


class DiagnosticController extends Controller
{
    /**
     * Get all active Diagnostics.
     *
     * @return JSON
     */
    public function getIndex()
    {

        $diagnostic = Diagnostic::where('status', '1')->get();


        return response()->success(compact('diagnostic'));
    }


    /**
     * Get diagnostics referenced by claim_id.
     *
     * @param int plan ID
     *
     * @return JSON
     */
    public function getDiagnostic($id)
    {
        $diagnostic = Diagnostic::where('claim_id', '=', $id)
                        ->where('status', '=', '1')
                        ->get();

        return response()->success($diagnostic);

    }

    public function getDiagnosticid($id)
    {
        $diagnostic = Diagnostic::where('id', '=', $id)->where('status',1)->with('claims')->first();

        return response()->success($diagnostic);
    }



    public function postDiagnostics()
    {
        $usercreate = Auth::user();

        $diagnostic = Diagnostic::create([
            'claim_id' => Input::get('claim_id'),
            'description' => Input::get('description'),
            'id_person'=>Input::get('id_person'),
            'person_type'=>Input::get('person_type'),
            'user_id_creation' => $usercreate->id
        ]);



        $document_id = Input::get('document_id');
        $diagnostic->policyId = Input::get('policyId');

        if($diagnostic->id){
            /*for ($i = 0; $i < count($document_id) ; $i++ ){//creacion de documento diagnostico
                $DocumentDiagnostic = DocumentDiagnostic::create([


                    'diagnostic_id' => $diagnostic->id,
                    'document_id' => $document_id[$i],
                    'user_id_creation' => $usercreate->id
                ]);
            }*/
            foreach ($document_id as $value){
                $DocumentDiagnostic = DocumentDiagnostic::create([
                    'diagnostic_id' => $diagnostic->id,
                    'document_id' => $value['id'],
                    'related' => $value['related'],
                    'user_id_creation' => $usercreate->id
                ]);    
            }

        }
        return response()->success($diagnostic);
    }


    public function getDiagnosticdocument(){
        $claimId = Input::get('claimId');
        $id = Input::get('id');
        $relationship = Input::get('relationship');
        if ($relationship == 'T')
            $relationship = 'A';
        else
            $relationship = 'D';

        $diagnostic = Diagnostic::where('claim_id', '=', $claimId)->where('status', '=', '1')->where('id_person',$id)->where('person_type',$relationship)->with('documents')->get();
        $documents = [];
        foreach ($diagnostic as $key => $value){
            $documents[$key]['diagnosticId'] = $value->id;
            $documents[$key]['description'] = $value->description;

            foreach($value->documents as $value2){
                if ($value2->status == 1){
                    
                    $files = ClaimDocument::where('claim_documents.id',$value2->document_id)->where('claim_documents.status', '=', '1')
                            ->join('providers', 'claim_documents.provider_id','=','providers.id')
                            ->join('claim_document_types', 'claim_documents.id_doc_type','=','claim_document_types.id')
                            ->select(["claim_documents.id_doc_type as id_doc_type","claim_documents.filename as filename", "claim_documents.amount as amount","claim_documents.description as description","claim_documents.currency_id as currency_id", "providers.name as provider", "claim_document_types.name as type"
                             ,DB::raw("date_format(claim_documents.document_date, '%d/%m/%Y') as created")])
                            ->first();

                    $documents[$key]['documents'][]['filename'] = $files->filename;

                    if ($files->currency_id =="1"){
                        $currency_id='USD';
                    }
                    elseif($files->currency_id="2"){
                         $currency_id='CLP';
                    }
                    elseif($files->currency_id="3"){
                         $currency_id='EUR';
                    }elseif($files->currency_id="4"){
                         $currency_id='GBP';
                    }

                    
                    /*
                    if ($files->id_doc_type == "1"){
                        $document_type = 'Factura';
                    }
                    else if ($files->id_doc_type == "2"){
                        $document_type = 'Informe';
                        $currency_id='N/A';
                    }
                    else if ($files->id_doc_type == "3"){
                        $document_type = 'Orden';
                        $currency_id='N/A';
                    }
                    else if ($files->id_doc_type == "4"){
                        $document_type = 'Resultado';
                        $currency_id='N/A';
                    }
                    */
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['amount'] =  number_format((float)$files->amount,2,'.','');
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['description'] = $files->description;
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['currency_id'] = $currency_id;
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['created'] = $files->created;
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['id_doc_type'] = $files->type;
                    $documents[$key]['documents'][count($documents[$key]['documents'])-1]['provider'] = $files->provider;


                }
            }
        }
        $diagnosticdocument = $documents;
        //return response()->success($documents);
        return response()->success(compact('diagnosticdocument'));
    }



    public function postUpdatediagnostic(){
        $user = Auth::user();
        $diagnosticId = Input::get('diagnosticId');

        $diagnostic = Diagnostic::where('id',$diagnosticId)->update([
            'description' => Input::get('description'),
            'user_id_update' => $user->id
        ]);

        $files = Input::get('files');

        foreach ($files as $key => $value){
            if ($value['assigned'] == false){
                DocumentDiagnostic::where('diagnostic_id',$diagnosticId)->where('document_id',$value['id'])->update([
                    'status' => 0,
                    'user_id_update' => $user->id
                ]);
                $docs = DocumentDiagnostic::where('diagnostic_id',$diagnosticId)->where('document_id',$value['id'])->first();
                if ($docs != null) {
                    $docs->delete();
                }

            }
            else{
                $exists = DocumentDiagnostic::where('diagnostic_id',$diagnosticId)->where('document_id',$value['id'])->first();

                if (!$exists){
                    DocumentDiagnostic::create([
                        'diagnostic_id' => $diagnosticId,
                        'document_id' => $value['id'],
                        'user_id_creation' => $user->id
                    ]);
                }
                else{
                    DocumentDiagnostic::where('diagnostic_id',$diagnosticId)->where('document_id',$value['id'])->update([
                        'status' => 1,
                        'user_id_update' => $user->id
                    ]);
                }
            }
        }
        $diagnostic = ['claimId' => Input::get('claimId'), 'policyId' => Input::get('policyId')];
        //$diagnostic->claimId = Input::get('claimId');
        //$diagnostic->policyId = Input::get('policyId');

        return response()->success($diagnostic);
    }
    public function postDispatchpatient()
    {
        $user = Auth::user();
        $dispatch_type = Input::get('dispatch_type');
        $claimId = Input::get('claimId');

        $diagnosticId = Input::get('diagnosticId');

        if ($dispatch_type == 1){
            $dispatchpatient = Diagnostic::where('claim_id',$claimId)->where('id_person',Input::get('id_person'))->where('person_type',Input::get('person_type'))->update([
                'dispatched' => 1,
                'user_id_update' => $user->id
            ]);
        }
        else{
            $step = 1;
            /*$dispatchpatient = Diagnostic::where('claim_id',$claimId)->update([
                'dispatched' => 1,
                'user_id_update' => $user->id
            ]);
            */
            //preguntar si no hay documentos pendientes o sin asingar, en caso de que haya le asigno otro step
            $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
            ->whereHas('diagnostics2', function ($query) use ($claimId) {
                $query->where('claim_id', $claimId);
            })
            ->get();

            $pending = true;
            $asign = true;
            $pendingAsign =  false;
            foreach($document as $value){
                if ($value->pending_doc == 1){
                    $pending = false;
                }
            }

            $claim_documents = ClaimDocument::where('id_claim',$claimId)->where('status','1')->where('returned',0)->get();
            $warning_documents = [];

            foreach ($claim_documents as $value){
                $diagnostic =DocumentDiagnostic::where('document_id',$value->id)->where('status','1')->first();
                if ($diagnostic == null){
                  $warning_documents[] = $value;
                  $pendingAsign = true;
                }
            }
            
            ////////////////////////////////////////////////////////

            foreach ($diagnosticId as  $value) {
                //var_dump($value["id"]);
                $dispatchpatient = Diagnostic::where('id',$value["id"])->update([
                    'dispatched' => 1,
                    'user_id_update' => $user->id
                ]);
            
            }
            $countDiagnostic = Diagnostic::where('claim_id',$claimId)->count();
            $countDispatched = Diagnostic::where('claim_id',$claimId)->where('dispatched','=', 1)->count();
            
            if ($countDiagnostic == $countDispatched){
                    $asign = false;
            }


            if (($pendingAsign == true) || ($asign == true)){
                $step = 0.5;
            }else{
                $step = 1;
            }
                 
                Claim::where('id',$claimId)->update([
                    'step' => $step
                    ]);
            

        }
        /********************** Envio de alerta a emisiones **********************************/
                
                //$sala =  "recepcion";
                //$message = "Se ha despachado un nuevo Reclamo";
                //NotificationController::notificar($sala, $message);

        /*************************************************************************/

        return response()->success('dispatchpatient');
    }

    public function getDiagnosticsclaim($claimId){
        $diagnostics = Diagnostic::where('status',1)->where('claim_id',$claimId)->with('documents')->get();
        //var_dump($diagnostics);
       /* foreach($diagnostics as $value){
            foreach($value->documents as $value2){
                $filename = ClaimDocument::where('id',$value2->document_id)->first();
                $value2->filename = $filename->filename;
                $value2->checked = false;
            }

        }*/
         foreach($diagnostics as $value){
           foreach($value->documents as $value2){
               $filename = ClaimDocument::where('claim_documents.id',$value2->document_id)
                            ->join('claim_document_types','claim_documents.id_doc_type','=','claim_document_types.id')
                            ->join('providers','claim_documents.provider_id','=','providers.id')
                            ->select(["claim_documents.*", "claim_document_types.name","providers.name as provider"])
                            ->first();

                $value2->filename = $filename->filename;
                $value2->checked = false;   
                $value2->doc_type = $filename->name;
                $value2->provider = $filename->provider;


               /*if ($filename->id_doc_type == "1"){
                   $value2->doc_type = 'Factura';
               }
               else if ($filename->id_doc_type == "2"){
                   $value2->doc_type = 'Informe';
               }
               else if ($filename->id_doc_type == "3"){
                   $value2->doc_type = 'Orden';
               }
               else if ($filename->id_doc_type == "4"){
                   $value2->doc_type = 'Resultado';
               }
                */
               $value2->description = $filename->description;

               if ($filename->currency_id =="1"){
                   $value2->currency = 'USD';
               }
               elseif($filename->currency_id="2"){
                   $value2->currency = 'CLP';
               }
               elseif($filename->currency_id="3"){
                   $value2->currency = 'EUR';
               }elseif($filename->currency_id="4"){
                   $value2->currency = 'GBP';
               }

               $value2->amount = $filename->amount;
               $value2->created =date ('d-m-Y',strtotime($filename->created_at));

           }

       }
        return response()->success($diagnostics);
    }



    /*
    Solicitar documentos segun diagnosticos Liquidaciones pendientes
    */
    public function getDocumentdiagnostics($diagnosticId){

        $diagnostics = Diagnostic::where('status',1)->where('id',$diagnosticId)->with('documents')->get();
        //var_dump($diagnostics);

        foreach($diagnostics as $value){
            foreach($value->documents as $value2){
                $filename = ClaimDocument::where('claim_documents.id',$value2->document_id)
                            ->join('claim_document_types','claim_documents.id_doc_type','=','claim_document_types.id')
                            ->join('providers','claim_documents.provider_id','=','providers.id')
                            ->select(["claim_documents.*", "claim_document_types.name","providers.name as provider"])
                            ->first();
                $value2->filename = $filename->filename;
                $value2->checked = false;   
                $value2->doc_type = $filename->name;
                $value2->provider = $filename->provider;
                /*if ($filename->id_doc_type == "1"){

                    $value2->doc_type = 'Factura';
                }
                else if ($filename->id_doc_type == "2"){
                    $value2->doc_type = 'Informe';
                }
                else if ($filename->id_doc_type == "3"){
                    $value2->doc_type = 'Orden';
                }
                else if ($filename->id_doc_type == "4"){
                    $value2->doc_type = 'Resultado';
                }
                **/
                $value2->description = $filename->description;

                if ($filename->currency_id =="1"){
                    $value2->currency = 'USD';
                }
                elseif($filename->currency_id="2"){
                    $value2->currency = 'CLP';
                }
                elseif($filename->currency_id="3"){
                    $value2->currency = 'EUR';
                }elseif($filename->currency_id="4"){
                    $value2->currency = 'GBP';
                }

                $value2->amount = $filename->amount;
                $value2->created =date ('d-m-Y',strtotime($filename->created_at));

            }

        }
        return response()->success($diagnostics);
    }


    public function getReportsorders($claimId, $affiliateId){
        /*$documents = ClaimDocument::where('claim_documents.status',1)->where('claim_documents.id_claim','<>',$claimId)
                   ->join('claim_document_types', 'claim_documents.id_doc_type','=','claim_document_types.id')
                   ->select(["claim_documents.*", "claim_document_types.name as doc_type"])
                    ->where(function ($query) {
                    $query->where('claim_documents.id_doc_type', '=', 2)->orWhere('claim_documents.id_doc_type', '=', 3);
                    })->get();
         echo $documents;           
        foreach ($documents as $value){
          
        
            //preguntar si ya estan en diagnosticos con related field
            $related = DocumentDiagnostic::where('status',1)->where('diagnostic_id',$diagnosticId)->where('document_id', $value->id)->first();
            if ($related){
                $value->assigned = ($related->related == 0) ? false : true;    
            }
            else{
                $value->assigned =false; 
            } 

        }*/
        $diagnostics = Diagnostic::where('diagnostics.id_person','=',$affiliateId)
                        ->join('document_diagnostics','diagnostics.id','=','document_diagnostics.diagnostic_id')
                        ->select(["diagnostics.*", "document_diagnostics.document_id as document_id"])
                        ->get();
        //echo $diagnostics;


        foreach ($diagnostics as $value){
        
            $documents = ClaimDocument::where('claim_documents.id','=',$value['document_id'])
                        ->join('claim_document_types', 'claim_documents.id_doc_type','=','claim_document_types.id')
                        ->select(["claim_documents.*", "claim_document_types.name as doc_type"])
                        ->where(function ($query) {
                            $query->where('claim_documents.id_doc_type', '=', 2)->orWhere('claim_documents.id_doc_type', '=', 3);
                        })->first();
             //var_dump($documents["filename"]);             
             $value->filename =  $documents["filename"];  
             $value->doc_type = $documents["doc_type"];   
             $value->id_doc_type = $documents["id_doc_type"];      
        }

        foreach ($diagnostics as $key => $value) {

            if($value->id_doc_type == null){
                unset($diagnostics[$key]);
            }
        }
                
        return response()->success($diagnostics);
    }



public function getClaimdiagnostics($claimId){
        $diagnostics = Diagnostic::where('diagnostics.status',1)
                       ->where('diagnostics.claim_id',$claimId)
                       ->with('documents')
                       ->join('claims','diagnostics.claim_id', '=', 'claims.id')                       
                       ->join('policies','claims.policy_id', '=', 'policies.id')   
                        ->select(['diagnostics.*', 'policies.first_name','policies.last_name'])                  
                       ->get();

        foreach($diagnostics as $value){
            $liquidated = 0;
            $count = 0;
            foreach($value->documents as $value2){
                $filename = ClaimDocument::where('id',$value2->document_id)->first();
                $value2->filename = $filename->filename;
                $value2->checked = false;
                $count++;
                if($value2->liquidated == 1){
                    $liquidated++;
                } 
            }

                if($count == $liquidated){
                    $value->show = false;
                }else{
                    $value->show = true;
                }  

          if($value->person_type == "D"){
                //var_dump("Entro porque es D");
                $dependent = Dependent::where('id',$value->id_person)->first();
                $value->name = $dependent->first_name.' '.$dependent->last_name;
                $value->person_type = "Dependiente";
            }else{
                $value->name = $value->first_name.' '.$value->last_name;
                $value->person_type = "Titular";
            }

           if($value->dispatched == 1){
                $value->disabled = true;
           }else{
                 $value->disabled = false;
           } 
            
        }

        
        return response()->success($diagnostics);
    }

    public function getDispatchdiagnostic($claimId){
        $diagnostics = Diagnostic::where('diagnostics.status',1)
                       ->where('diagnostics.claim_id',$claimId)
                       ->with('documents')
                       ->join('claims','diagnostics.claim_id', '=', 'claims.id')                       
                       ->join('policies','claims.policy_id', '=', 'policies.id')   
                        ->select(['diagnostics.*', 'policies.first_name','policies.last_name'])                  
                       ->get();

        foreach($diagnostics as $value){
            foreach($value->documents as $value2){
                $filename = ClaimDocument::where('id',$value2->document_id)->first();
                $value2->filename = $filename->filename;
                $value2->checked = false;
           

            }


           
            
        }

        
        return response()->success($diagnostics);
    }




    public function getInvoices($diagnosticId){

       $diagnostics = Diagnostic::where('status',1)->where('id',$diagnosticId)->with('documents')->get();
        //echo $diagnostics;

        foreach($diagnostics as $value){
            foreach($value->documents as $value2){
                $filename = ClaimDocument::where('claim_documents.id',$value2->document_id)
                            ->where('claim_documents.liquidated','=',0)
                            ->join('claim_document_types','claim_documents.id_doc_type','=','claim_document_types.id')
                            ->join('providers','claim_documents.provider_id','=','providers.id')
                            ->select(["claim_documents.*","claim_documents.id_doc_type as id_doc_type", "claim_document_types.name","providers.name as provider"])
                           
                            ->first();
              // if($filename->id_doc_type == 1){             
                $value2->filename = $filename->filename;
                $value2->checked = false;   
                $value2->doc_type = $filename->name;
                
                $value2->description = $filename->description;
                $value2->provider = $filename->provider;
                $value2->id_doc_type = $filename->id_doc_type;

                if ($filename->currency_id =="1"){
                    $value2->currency = 'USD';
                }
                elseif($filename->currency_id="2"){
                    $value2->currency = 'CLP';
                }
                elseif($filename->currency_id="3"){
                    $value2->currency = 'EUR';
                }elseif($filename->currency_id="4"){
                    $value2->currency = 'GBP';
                }

                $value2->amount = $filename->amount;
                $value2->created =date ('d-m-Y',strtotime($filename->created_at));

            //}

            }

        }

        
        return response()->success($diagnostics);
    }



    public function postReturninvoice(){

        $user = Auth::user();
        $documents = Input::get('documents');
        foreach ($documents as  $value) {
            if($value['return'] ==  true){

                $dispatch = DocumentDiagnostic::where('id',$value["id"])->update([
                    'returned' => 1,
                    'user_id_update' => $user->id
                ]);

            }
        }

         return response()->success('dispatch');
    }

    public function postDispatchdiagnostics()
    {
        $user = Auth::user();
        $claimId = Input::get('claims');
        $content = [];
        $final_content = [];
        
          //var_dump($claimId);
        foreach ($claimId as $value) {
                

                $claim = Claim::find($value["id"]);
                $policy_id = $claim->policy_id;
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;
                $applicant = Applicant::where ('policy_id', $policy_id)->first();
                $name = $applicant->first_name.' '.$applicant->last_name;    
                

                $diagnostics = Diagnostic::where('claim_id', '=', $value["id"])
                                ->select(['diagnostics.id','diagnostics.claim_id','diagnostics.description','diagnostics.person_type','diagnostics.id_person'])
                                ->get();
                  
                foreach ($diagnostics as $value2) {
                    $rowData1 = [];
                    $rowData2 = [];

                    $amount = 0;
                    $document = DocumentDiagnostic::with('diagnostics')->where('status',1)->where('diagnostic_id',$value2["id"])->get();
                    
                    foreach ($document as $value3){
                        $amount+= $value3->diagnostics->amount;
                    }
                    if ($value2->person_type == 'A'){
                        $affiliate = $name;
                        $rowData = ["date"=>date('d-m-Y'), "claim_id"=>$value["id"],"applicant"=>$name,"policy_number"=> $policy_number, "affiliate"=>$name, "diagnostic"=>$value2->description, "amount"=>number_format((float)$amount,2,'.','')];

                    }else{
                         $dependent = Dependent::where('id',$value2->id_person)->first();
                         $affiliate = $dependent->first_name.' '.$dependent->last_name; 
                         $rowData = ["date"=>date('d-m-Y'), "claim_id"=>$value["id"],"applicant"=>$name,"policy_number"=> $policy_number, "affiliate"=>$affiliate, "diagnostic"=>$value2->description, "amount"=>number_format((float)$amount,2,'.','')];


                    }
                    
                   /*****************cuadro de facturas**************************/
                   $document_in = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
                                    ->whereHas('diagnostics2', function ($query) use ($value2) {
                                    $query->where('diagnostic_id', $value2["id"]);
                                    })
                                    ->whereHas('diagnostics', function ($query2)  {
                                        $query2->where('id_doc_type', 1);
                                    })->groupBy('document_id')->get();

                     $total = 0;                 
                     $Data1 = [];
                     foreach ($document_in as $value4){
                        //var_dump($value4->diagnostics->provider_id);               
                        $provider = Provider::where('id',$value4->diagnostics->provider_id)->first();
                        $Data1 = ["factura"=>$value4->diagnostics->description, "fecha_factura"=>date('d-m-Y',strtotime($value4->diagnostics->document_date)), "provider"=> $provider->name, "monto_factura"=>number_format((float)$value4->diagnostics->amount,2,'.','')];
                        $total += $value4->diagnostics->amount;
                        array_push($rowData1, $Data1);
                     }
                     
                     $total = number_format((float)$total,2,'.','');

                     /*************************************************************/

                    /********************cuadro otros documentos *********************/
                     $document_ot = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
                                ->whereHas('diagnostics2', function ($query) use ($value2) {
                                    $query->where('diagnostic_id',  $value2->id);
                                })
                                ->whereHas('diagnostics', function ($query2)  {
                                    $query2->where('id_doc_type','<>',  1);
                                })->groupBy('document_id')->get();

                      if(count($document_ot) > 0 ){
                         $Data2 = [];
                         foreach ($document_ot as $value5){
                             if ($value5->diagnostics->id_doc_type == 1){
                                    $doc_type = 'Factura';
                            }
                            elseif ($value5->diagnostics->id_doc_type == 2){
                                    $doc_type = 'Informe';
                            }
                            elseif ($value5->diagnostics->id_doc_type == 3){
                                    $doc_type = 'Orden';
                            }
                            else{
                                    $doc_type = 'Resultado';
                            }

                            $Data2 = ["doc_type"=>$doc_type, "date"=>date('d-m-Y',strtotime($value5->diagnostics->document_date)), "description_other"=>$value5->diagnostics->description];
                            array_push($rowData2, $Data2);
                         }            
                    }else{
                        $rowData2 = [];

                    }
                    
                   /******************************************************************/

                   $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                    $date = "Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y');
               
                    $content = ["claim_id" => $value["id"], "date" => $date, "applicant" => $name, "policy_number" => $policy_number, "affiliate" => $affiliate, "total"=>$total, "user"=>$user->name, "rowData"=>$rowData, "rowData1"=>$rowData1, "rowData2"=>$rowData2];

                    array_push($final_content, $content);

                    //Despacho de los Diagnosticos
                    $dispatchpatient = Diagnostic::where('id',$value2["id"])->update([
                            'dispatched' => 1,
                            'user_id_update' => $user->id
                        ]);
            
                
                }//foreach diagnostics


                $asign = true;
                $pendingAsign =  false;
                $claim_documents = ClaimDocument::where('id_claim',$value["id"])->where('status','1')->where('returned',0)->get();
                //var_dump($claim_documents);
                foreach ($claim_documents as $value6){
                    $diagnostic =DocumentDiagnostic::where('document_id',$value6->id)->where('status','1')->first();
                    if ($diagnostic == null){        
                        $pendingAsign = true;
                    }
                }
            
            ////////////////////////////////////////////////////////

            $countDiagnostic = Diagnostic::where('claim_id',$value["id"])->count();
            $countDispatched = Diagnostic::where('claim_id',$value["id"])->where('dispatched','=', 1)->count();
         
            if ($countDiagnostic == $countDispatched){
                    $asign = false;
            }


            if (($pendingAsign == true) || ($asign == true)){
                $step = 0.5;
            }else{
                $step = 1;
            }
             Claim::where('id',$value["id"])->update([
                    'step' => $step
                    ]);
             

            }// foreach claims

            /********************datos para PDFS ****************************/
              return response()->success($final_content); 
              //var_dump($final_content);
            /****************************************************************/ 
        }
               
             
               



}
