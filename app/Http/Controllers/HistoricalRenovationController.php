<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\HistoricalRenovation;
use DB;
use App\Policy;
use App\Plan;
use App\Deductible;
use App\Coverage;


class HistoricalRenovationController extends Controller
{
    public function getIndex(){

        $historical=DB::table('historical_renovations')->where('historical_renovations.status','=','1')
                    ->select(["historical_renovations.id as id", DB::raw("date_format(historical_renovations.created_at, '%d/%m/%Y') as created_at"),
                        DB::raw('CONCAT_WS("/",plans.name, deductibles.name, coverages.name) AS plan'),  DB::raw('CONCAT_WS("/",plan.name, deductible.name, coverage.name) AS planAct'), "policies.number", DB::raw('CONCAT(policies.first_name," ", policies.last_name) as client')])
                    ->join('policies','historical_renovations.id_policy','=','policies.id')
                    ->join('plans','historical_renovations.id_plan','=','plans.id')
                    ->leftjoin('deductibles','historical_renovations.id_deductibles','=','deductibles.id')
                    ->leftjoin('coverages','historical_renovations.id_coverage','=','coverages.id')
                    ->join('plans as plan','policies.plan_id','=','plan.id')
                    ->leftjoin('deductibles as deductible','policies.deductible_id','=','deductible.id')
                    ->leftjoin('coverages as coverage','policies.coverage_id','=','coverage.id')
                    ->get();
        return response()->success(compact('historical'));
    }
}
