<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HospitalMedicalSpeciality extends Model
{
    protected $table = 'hospital_medical_specialities';
    protected $fillable = ['hospital_id', 'speciality_id','status','user_id_creation','user_id_update'];
}
