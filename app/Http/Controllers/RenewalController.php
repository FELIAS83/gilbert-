<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Input;
use DB;
use App\Classes\PHPMailer\PHPMailer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Policy;
use App\Payment;
use App\Applicant;
use App\PolicyDocument;
use App\PolicyApplicant;
use App\Fee;
use App\PolicyHistory;
use App\Attached;
use App\Configuration;
use App\PoliciesEffectiveDate;
use App\Policystep;
use App\HistoricalRenovation;
use App\EmailFormat;
use App\Agent;
use App\MailConfiguration;
use App\Dependent;
use App\DeductibleDetail;
use App\Coverage;
use App\Amendment;
use App\FeeStep;
use App\Annex;
use App\Company;
use App\Commission;
use App\CompanyCommission;
use App\Active;
use App\Plan;
use App\AgentCommision;
use Mail;
use Excel;


class RenewalController extends Controller
{


    public function getFilterdates(){
                $currentdate = date("d-m-Y");
                $dia = date ('d');
                $startdate= $currentdate;
                $startdate = substr($startdate, 6, 4)."-".substr($startdate,3, 2)."-01";
                
                if( $dia >= 15){
                  
                  $finaldate= date ( 'd-m-Y' , strtotime ( '+1 month' , strtotime ($currentdate) ) );
                  $finaldate = substr($finaldate, 6, 4)."-".substr($finaldate,3, 2)."-15";
                }
                else{
                
                  $finaldate =  date ( 'd-m-Y' , strtotime ( '+1 month' , strtotime ($startdate) ) );
                  $finaldate = substr($finaldate, 6, 4)."-".substr($finaldate,3, 2)."-01";
                
                }
                $date1=date_create($startdate);
                $date2=date_create($finaldate);
                //var_dump($date1);
                $dates  = array('start_date' =>  $date1 , 'end_date' => $date2 );
                 return response()->success($dates);
    }
     /**
     * Get all policies expired.
     *
     * @return JSON
     */
    public function getExpiredPolicies()
    {

        $startdate = Input::get('start_date');  
        $finaldate = Input::get('end_date');


         if($startdate == null){
            $currentdate = date("d-m-Y");
            $dia = date ('d');
            $startdate= $currentdate;
            $startdate = substr($startdate, 6, 4)."-".substr($startdate,3, 2)."-01";

         }else{
            $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
         }

        if($finaldate == null){
                $currentdate = date("d-m-Y");
                $dia = date ('d');
                
                if( $dia >= 15){  
                  $finaldate= date ( 'd-m-Y' , strtotime ( '+1 month' , strtotime ($currentdate) ) );
                  $finaldate = substr($finaldate, 6, 4)."-".substr($finaldate,3, 2)."-15";
                }
                else{
                
                  $finaldate =  date ( 'd-m-Y' , strtotime ( '+1 month' , strtotime ($startdate) ) );
                  $finaldate = substr($finaldate, 6, 4)."-".substr($finaldate,3, 2)."-01";
                
                }
        }else{
            $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 

        }
       
                   

        $getExpiredPolicies = DB::table('fees')->where('fees.status', '=', '1')
                    ->where('fees.paid_fee', '=', '0')
                    ->where('payments.status', '=', '1')
                    ->where('policies.status','=', '1')
                    ->whereBetween('fees.fee_payment_date', [$startdate, $finaldate])
                    ->join('policies', 'fees.policy_id', '=', 'policies.id')
                    ->join('payments', 'fees.policy_id', '=', 'payments.policy_id')
                    ->join('applicants', 'fees.policy_id', '=', 'applicants.policy_id')
                    //->join('plans', 'plans.id', '=', 'policies.plan_id')
                    //->join('deductibles', 'deductibles.id', '=', 'policies.deductible_id')
                    ->join('agents', 'agents.id', '=', 'policies.agent_id')
                    ->leftjoin('fee_steps', 'fees.id', '=', 'fee_steps.fee_id')
                    ->select(['policies.id as id', 'policies.number as number', 'policies.first_name', 'policies.last_name', 'policies.plan_id', DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), "fees.id as fee_id","fees.total", "payments.mode",DB::raw("date_format(fees.fee_payment_date, '%m/%d/%Y') as fee_payment_date"),
                        DB::raw('DATEDIFF(CURDATE(), fees.fee_payment_date) as days_passed'), 'fees.fee_number', 'payments.mode as modenum', /*'plans.name as plan_description', 'deductibles.name as option',*/ 'agents.first_name as agent_first_name', 'agents.last_name as agent_last_name', DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), 'fee_steps.step1','fee_steps.step2','fee_steps.step3','fee_steps.step4','fee_steps.step5','fee_steps.step6'])
                    ->get();



        /*$getExpired = DB::table('policies')->where('policies.status','=', '1')
                    ->where('payments.status', '=', '1')
                    ->whereRaw('MONTH(policies.start_date_coverage) BETWEEN MONTH(DATE_SUB(CURDATE(), INTERVAL 2 MONTH)) AND MONTH(CURDATE())')
                    ->whereRaw('timestampdiff(year,policies.start_date_coverage,curdate()) > 1')
                    //->whereRaw('fees.paid_fee = 0')
                    //->where('fees.status','=','1')
                    //->whereRaw('timestampdiff(month,policies.start_date_coverage,curdate()) <= 12')
                    //->join('fees', 'policies.id', '=', 'fees.policy_id')
                    ->join('payments', 'policies.id', '=', 'payments.policy_id')
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('fees', 'policies.id', '=', 'fees.policy_id')
                    ->select(["policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), "payments.id as fee_id","payments.total_value as total", "payments.mode", DB::raw("date_format(DATE_ADD(policies.start_date_coverage, INTERVAL 1 YEAR ), '%d/%m/%Y') as fee_payment_date")]);
                    //->get();

        $getExpiredPolicies = $getExpiredPolicies->union($getExpired)->get();    
    */
       

        foreach ($getExpiredPolicies as $key => $value) {
            $value->start_date = date_create($startdate);
            $value->final_date = date_create($finaldate);
           
            if($value->mode == "0" ){
                $value->mode = "Anual";
            }
            elseif($value->mode == "1" ){
                $value->mode = "Semestral"; 
            }
            elseif($value->mode == "2" ){
                $value->mode = "Trimestral"; 
            }else{
              $value->mode = "Mensual"; 
            }
            /*Fecha max de Dependiente segun plan de Poliza*/
            $plan = DB::table('plans')->where('id', '=', $value->plan_id)->where('status', '=', 1)->first();
            $max_dependent_age = $plan->max_dependent_age;
            /*                                             */
            $value->forDisabled = false; 
            $dependent = DB::table('dependents')->where('applicant_id', '=', $value->id)->where('status', '=', 1)->get();

            foreach ($dependent as $value2) {

                    //$value->dependientes = $dependent;

                $dia=date("d");
                $mes=date("m");
                $ano=date("Y");


                $dianaz=date("d",strtotime($value2->birthday));
                $mesnaz=date("m",strtotime($value2->birthday));
                $anonaz=date("Y",strtotime($value2->birthday));


                //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual

                if (($mesnaz == $mes) && ($dianaz > $dia)) {
                        $ano=($ano-1); 
                }

                //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual

                if ($mesnaz > $mes) {
                    $ano=($ano-1);
                }

                //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad

                $edad=($ano-$anonaz);
                if(($edad >= $max_dependent_age) && ($value2->relationship == "D")){
                   
                    $value->forDisabled = true;    
                }
                


            }

           
        }            

        return response()->success(compact('getExpiredPolicies'));
        
    }

 
    //
      /**
    *
    *Create Policy renewal
    *
    *
    */
    public function postRenewal(Request $request)
    {
        $usercreate = Auth::user();
        $policyId = $request['policy_id'];
        
        $policy = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.id', '=', $policyId)
                    ->select(["policies.*", "applicants.id as applicantId", "applicants.*"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->first();
         
      /* $policyRen = Policy::create([
            'number' =>  (string)$policy->number,
            'first_name' => $policy->first_name,
            'last_name' => $policy->last_name,
            'identity_document' => $policy->identity_document,
            'email' => $policy->email,
            'mobile' => $policy->mobile,
            'phone' => $policy->phone,
            'agent_id' => $policy->agent_id,
            'plan_id' => $policy->plan_id,
            'have_prev_safe' => $policy->have_prev_safe,
            'type_policy' => "R",
            'user_id_creation' => $usercreate->id
        ]);
      */
       $policyApplicant = PolicyApplicant::create([

            'policy_id_renewal' =>  $policyRen->id,
            'applicant_id' => $policy->applicantId,
            'policy_id_old' => $policyId,
            'user_id_creation' => $usercreate->id
        
        ]);

       $policy = ['policyId' => $policyRen->id, 'policyIdOld' => $policyId, 'applicantId' => $policy->applicantId];

        $policyData = [
            'have_renewal' => 1,            
        ];
        $affectedRows = Policy::where('id', '=', $policyId)->update($policyData);

        /***************************************************************************/
        /***************************************************************************/        
       return response()->success($policy);

    }

    /**
     * Delete  Policy.
     *
     * @return JSON
     */

     public function MotiveRenewal(Request $request)
    {
        $motive=$request['motive'];
        $id=$request['policyId'];

        $policyData = [
            'status_renewal' => 0,
            'status' => 0,
            'motive' => $motive
        ];
        $affectedRows = Policy::where('id', '=', $id)->update($policyData);

        //var_dump($affectedRows);
        return response()->success('success');
    }


      /**
     * Delete  Policy.
     *
     * @return JSON
     */

     public function deleteRenewal($id)
    {
        $policyData = [
            'status_renewal' => 0,            
        ];
        $affectedRows = Policy::where('id', '=', $id)->update($policyData);
        return response()->success('success');
    }


  public function postPayments(){

        $usercreate = Auth::user();
        $policy_id = Input::get('policyId');
        $feeId = Input::get('feeId');
        $mode = Input::get('mode');
        $method = Input::get('method');

        $fee = Fee::find($feeId);
        //var_dump($fee);
        $payment = Payment::where('policy_id','=',$policy_id)->where('status',1)->get();

        $fee_number = $fee->fee_number;
        $actual_mode = $payment[0]['mode'];
        $actual_method =$payment[0]['method'];

        if(($actual_mode != 0) && ($fee_number < 12)){

            if( $mode != $actual_mode){

                 $policyData = [
                    'have_renewal' => 1,            
                ];
                $affectedRows = Policy::where('id', '=', $policy_id)->update($policyData);
                $paymentData = [
                    'status' => 0,
                    'user_id_update' => $usercreate->id,
                ];
                $affectedRows = Payment::where('policy_id', '=', Input::get('policyId'))->update($paymentData);
                $payments = Payment::create([
                    'policy_id' => Input::get('policyId'),
                    'mode' => Input::get('mode'),
                    'method' => Input::get('method'),
                    'discount' => Input::get('discount'),
                    'discount_percent' => Input::get('discount_percent'),
                    'invoice_before' => Input::get('invoice_before'),
                    'user_id_creation' => $usercreate->id
                ]);
            }else{
                $paymentData = [
                    'mode' => Input::get('mode'),
                    'method' => Input::get('method'),
                    'discount' => Input::get('discount'),
                    'discount_percent' => Input::get('discount_percent'),
                    'invoice_before' => Input::get('invoice_before'),
                    'user_id_update' => $usercreate->id,
                ];
                $affectedRows = Payment::where('policy_id', '=', Input::get('policyId'))->update($paymentData);

            }
            }else{
             if($mode != $actual_mode){
                 $policyData = [
                    'have_renewal' => 1,            
                ];
                $affectedRows = Policy::where('id', '=', $policy_id)->update($policyData);
                $paymentData = [
                    'status' => 0,
                    'user_id_update' => $usercreate->id,
                ];
                $affectedRows = Payment::where('policy_id', '=', Input::get('policyId'))->update($paymentData);
                $payments = Payment::create([
                    'policy_id' => Input::get('policyId'),
                    'mode' => Input::get('mode'),
                    'method' => Input::get('method'),
                    'discount' => Input::get('discount'),
                    'discount_percent' => Input::get('discount_percent'),
                    'invoice_before' => Input::get('invoice_before'),
                    'user_id_creation' => $usercreate->id
                ]);
                
            }else{
                $paymentData = [
                    'mode' => Input::get('mode'),
                    'method' => Input::get('method'),
                    'discount' => Input::get('discount'),
                    'discount_percent' => Input::get('discount_percent'),
                    'invoice_before' => Input::get('invoice_before'),
                    'user_id_update' => $usercreate->id,
                ];
                $affectedRows = Payment::where('policy_id', '=', Input::get('policyId'))->update($paymentData);

            }
            
        }
        // solo si hay un cambio en el modo (anual,semestral,trimestral,mensual) ocurrira el recalculo de las cuotas   
         if($actual_mode !== $mode){    
             $current_year = date ('Y');
             $current_month = date ('m');
             $effective_date = DB::table('policies_effective_dates')->where('status','=','1')->where('policy_id','=',$policy_id)
                            ->first();  

             $consulta = DB::table('policies')->where('status','=','1')->where('id','=',$policy_id)->select(['policies.id','policies.start_date_coverage'])
                        ->first();       
            
            if($effective_date == null){
                $start_date_coverage = $consulta->start_date_coverage;
                $policy_year = date('Y', strtotime( $start_date_coverage));
                $policy_month = date('m', strtotime( $start_date_coverage));
                $monthDiff =  $policy_month - $current_month;
                //$yearDiff = $current_year - $policy_year;
        
                if($policy_month >= $current_month){
                    $yearDiff =  ($current_year - 1) - $policy_year ;  
                }
                else{
                    $yearDiff =  $current_year - $policy_year;
                }  
                
            $effective_date = date ( 'Y-m-d' , strtotime ( '+'.$yearDiff.' year' ,  strtotime($start_date_coverage) ));           
            PoliciesEffectiveDate::create([
                'start_date_coverage'  => $effective_date,
                'policy_id' => $policy_id,
                'user_id_creation' =>  $usercreate->id
            ]);
                         
            }

        if($this->calculateFeeDates($policy_id)){

            return response()->success($policy_id);           
       
         }
        }    

        return response()->success($policy_id);


    }



    public function getRenewal($id){

        $renewal = PolicyApplicant::count();
        //var_dump($renewal);
    }
public function getPolicyInfoRen(){
        $policy_id = Input::get('policy_id');
       
        // Nota: condicional, en el siguiente cambio utilizar filtros para $provider_id        
        $getPolicyInfoRen = DB::table('policies') 
            ->where('policies.id', $policy_id)
            ->where('payments.status', 1)
            ->join('agents', 'policies.agent_id', '=', 'agents.id')
            ->join('plans', 'policies.plan_id', '=', 'plans.id')
            ->leftJoin('deductibles', 'policies.deductible_id', '=', 'deductibles.id')
            ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
            ->leftJoin('payments', 'policies.id', '=', 'payments.policy_id')
            ->select('policies.*', DB::raw("CONCAT(agents.first_name,' ',agents.last_name) as agent"), 'plans.name as plan', 'deductibles.name as deductible', 'applicants.id as applicant_id', 'applicants.birthday as birthday','applicants.civil_status','payments.mode','payments.discount','payments.discount_percent','payments.total_value','payments.method','payments.payment_date','payments.bank_name','payments.check_number','payments.transfer_number','payments.titular_name','payments.account_type','payments.account_number','payments.deposit_number','payments.titular_lastnames','payments.card_type','payments.card_mark','payments.card_bank','payments.payment_type','payments.id as payment_id')

            ->get();
        $getPolicyInfoRen[0]->start_date_coverage = date('d-m-Y',strtotime($getPolicyInfoRen[0]->start_date_coverage));
        $getPolicyInfoRen[0]->effective_date = date('Y-m-d',strtotime($getPolicyInfoRen[0]->start_date_coverage));
        $getPolicyInfoRen[0]->inclusion_date = date('Y-m-d',strtotime($getPolicyInfoRen[0]->created_at));
        
        $first_value = Fee::where('policy_id',$policy_id)->first();
        $getPolicyInfoRen[0]->first_value = (isset($first_value->total)) ? $first_value->total : 0;

        $getPolicyInfoRen[0]->method = (int) $getPolicyInfoRen[0]->method;
        $getPolicyInfoRen[0]->account_type = (int) $getPolicyInfoRen[0]->account_type;
        $getPolicyInfoRen[0]->payment_type = (int) $getPolicyInfoRen[0]->payment_type;
        $getPolicyInfoRen[0]->card_mark = (int) $getPolicyInfoRen[0]->card_mark;
        $getPolicyInfoRen[0]->card_type= (int) $getPolicyInfoRen[0]->card_type;


       return response()->success(compact('getPolicyInfoRen'));
    } 

public function getPolicyInfoPay(){
        $policy_id = Input::get('policy_id');
        $fee_id = Input::get('fee_id');
       
        $getPolicyInfoPay = DB::table('policies') 
            ->where('policies.id', $policy_id)
            ->where('payments.status', 1)
            ->join('agents', 'policies.agent_id', '=', 'agents.id')
            ->join('plans', 'policies.plan_id', '=', 'plans.id')
            ->leftJoin('deductibles', 'policies.deductible_id', '=', 'deductibles.id')
            ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
            ->leftJoin('payments', 'policies.id', '=', 'payments.policy_id')
            ->select('policies.*', DB::raw("CONCAT(agents.first_name,' ',agents.last_name) as agent"), 'plans.name as plan', 'deductibles.name as deductible', 'applicants.id as applicant_id', 'applicants.birthday as birthday','applicants.civil_status','payments.mode','payments.discount','payments.discount_percent','payments.total_value','payments.method','payments.payment_date','payments.bank_name','payments.check_number','payments.transfer_number','payments.titular_name','payments.account_type','payments.account_number','payments.deposit_number','payments.titular_lastnames','payments.card_type','payments.card_mark','payments.card_bank','payments.payment_type','payments.id as payment_id', 'payments.interest', 'payments.month')

            ->get();
        $getPolicyInfoPay[0]->start_date_coverage = date('d-m-Y',strtotime($getPolicyInfoPay[0]->start_date_coverage));
        $getPolicyInfoPay[0]->effective_date = date('Y-m-d',strtotime($getPolicyInfoPay[0]->start_date_coverage));
        
        $first_value = Fee::where('id',$fee_id)->first();
        $getPolicyInfoPay[0]->first_value = (isset($first_value->total)) ? $first_value->total : 0;
        $fee_payment_date = Fee::where('id',$fee_id)->first();
        $getPolicyInfoPay[0]->fee_payment_date = date('Y-m-d',strtotime($fee_payment_date->fee_payment_date));
        $getPolicyInfoPay[0]->method = (int) $getPolicyInfoPay[0]->method;
        $getPolicyInfoPay[0]->account_type = (int) $getPolicyInfoPay[0]->account_type;
        $getPolicyInfoPay[0]->payment_type = (int) $getPolicyInfoPay[0]->payment_type;
        $getPolicyInfoPay[0]->card_mark = (int) $getPolicyInfoPay[0]->card_mark;
        $getPolicyInfoPay[0]->card_type= (int) $getPolicyInfoPay[0]->card_type;
        $getPolicyInfoPay[0]->interest= (int) $getPolicyInfoPay[0]->interest;
        $getPolicyInfoPay[0]->month= (int) $getPolicyInfoPay[0]->month;


       return response()->success(compact('getPolicyInfoPay'));
    }     

    public function postConfirm(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $total_value = $request['total_value'];
        $fees = $request['fees'];
        $mode = $request['mode'];
        $effective_date = $request['effective_date'];
        $have_renewal = $request['have_renewal'];
        $actual_number = $request['actual_number'];
        $feeId = $request['feeId'];

        $amendments = $request['amendments'];
        $annexes = $request['annexes'];
        $deletedamendments = $request['deletedamendments'];
        $deletedannexes = $request['deletedannexes'];
        $updatedamendments = $request['updatedamendments'];
        $updatedannexes = $request['updatedannexes'];
        /*annexes and ammendent*/
         foreach ($amendments as $value){
            $amendment = Amendment::create([
                'policy_id' => $policyId,
                'role_order' => $value['role_order'],
                'affiliate_id' => $value['affiliate'],
                'type' => $value['type'],
                'description' => $value['description'],
                'user_id_creation' => $user->id
            ]);
        }

        foreach ($updatedamendments as $value){
            $amendment = Amendment::where('id',$value['id'])->update([
                'role_order' => $value['role_order'],
                'affiliate_id' => $value['affiliate_id'],
                'type' => $value['type'],
                'description' => $value['description'],
                'user_id_update' => $user->id
            ]);
        }

        foreach ($deletedamendments as $value){
            $amendment = Amendment::where('id',$value['id'])->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);
        }

        foreach ($annexes as $value){
            if ($value['effective_date']==null){
                $effec_date=$effective_date;
            }
            else
            {
                $effec_date=$value['effective_date'];
            }

            $annex = Annex::create([
                'policy_id' => $policyId,
                'affiliate_id' => $value['affiliate'],
                'annex' => $value['annex'],
                'effective_date' => $effec_date,
                'user_id_creation' => $user->id
            ]);
        }

        foreach ($updatedannexes as $value){
            $annex = Annex::where('id',$value['id'])->update([
                'affiliate_id' => $value['affiliate_id'],
                'annex' => $value['annex'],
                'effective_date' => $value['effective_date'],
                'user_id_update' => $user->id
            ]);
        }

        foreach ($deletedannexes as $value){
            $annex = Annex::where('id',$value['id'])->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);
        }
        /**************************/
        /*Fee::where('policy_id',$policyId)->where('paid_fee',0)->update(['status' => 0]);*/

        //$fees = Input::get('fees');
        //$mode = Input::get('mode');
        //$effective_date = Input::get('effective_date');
        foreach ($fees as $key => $fee){
            $data = ['total' => $fee['total'],
                     'tax_ssc' => Input::get('tax_ssc') ? 1 : 0,
                     'amount_tax_ssc' => $fee['taxes'][2]['value'],
                     'amount_titular' => $fee['details'][0]['value'],
                     'amount_spouse' => isset($fee['details'][1]['value']) ? $fee['details'][1]['value'] : 0,
                     'amount_childs' => isset($fee['details'][2]['value']) ? $fee['details'][2]['value'] : 0,
                     'amount_coverage' => isset($fee['details'][3]['value']) ? $fee['details'][3]['value'] : 0,
                     'adminfee' => ($fee['taxes'][0]['checkvalue'] == true) ? 1 : 0,
                     'user_id_update' => $user->id
                    ];
            $affectedRows = Fee::where('policy_id',$policyId)->where('id',$fee['id'])->where('status',1)->update($data);
        }

        if($have_renewal == 1){
            $policy = Policy::where('id',$policyId)->update([
                'number' => $request['number'],
                'have_renewal' => 0,
                'user_id_update' => $user->id
            ]);
        }


        $payment = Payment::where('policy_id',$policyId)->update([
            'total_value' => $total_value,
            'user_id_update' => $user->id
        ]);
        /************ fee steps *****/
        $fee = FeeStep::where('fee_id',$feeId)->first();
        if($fee ==  NULL){
            $fee = FeeStep::create([
                'fee_id' => $feeId,
                'policy_id' => $policyId,
                'user_id_creation' => $user->id
            ]);
        }
        $affectedRows = FeeStep::where('id','=',$fee->id)->update([
            'step3' => 1,
            'user_id_update' => $user->id
        ]);
         /******************************/


        return response()->success(compact('confirm'));
    }


    public function getExpired(){

        $getExpired = DB::table('policies')->where('policies.status','=', '1')
                    //->where('fees.paid_fee', '=', '1')
                    ->where('payments.status', '=', '1')
                    ->whereRaw('timestampdiff(month,policies.start_date_coverage,curdate()) >= 10')
                    ->whereRaw('timestampdiff(month,policies.start_date_coverage,curdate()) <= 12')
                    //->join('fees', 'policies.id', '=', 'fees.policy_id')
                    ->join('payments', 'policies.id', '=', 'payments.policy_id')
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->select(["policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), "payments.mode", DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage")])
                    ->get();
                    //var_dump($getExpired);

               foreach ($getExpired as $key => $value) {
           
                    if($value->mode == "0" ){
                        $value->mode = "Anual";
                    }
                    elseif($value->mode == "1" ){
                        $value->mode = "Semestral"; 
                    }
                    elseif($value->mode == "2" ){
                        $value->mode = "Trimestral"; 
                    }else{
                        $value->mode = "Mensual"; 
                    }
           
                }            
        return response()->success(compact('getExpired'));
        

    }


  public function postRenewalpolicy(){
        $fee_number =  array();
        $adminfee = array();
        $fee_payment_date = array();
        $total = array();
        $user = Auth::user();
        $policyId = Input::get('policy_id');
        $policy = DB::table('policies')->where('policies.id', '=', $policyId)
                    ->where('policies.status', '=', '1')
                    ->where('payments.status', '=', '1')
                    ->join('payments', 'policies.id', '=', 'payments.policy_id')
                    ->select(["policies.*", "payments.mode"])
                    ->first();
         $start_date_coverage = $policy->start_date_coverage;   
         $start_date_coverage= date ( 'Y-m-d' , strtotime ( '+1 year' , strtotime ($start_date_coverage) ) );   
       
        $data = [
            'start_date_coverage' => $start_date_coverage,
            'user_id_update' => $user->id,
        ];

        Policy::where('id', '=',  $policyId)->update($data);

        $fees = DB::table('fees')->where('policy_id','=', $policyId)
                                 ->where('paid_fee', '=', '1')
                                 ->where('status', '=', '1')
                                 ->get();
        $mode = $policy->mode;
        
        if($mode == 0){
            $amount_fee = 1;
        }                         
        elseif($mode == 1){
            $amount_fee = 2;
        }
        elseif($mode == 2){
            $amount_fee = 4;
        }
        elseif($mode == 3){
            $amount_fee = 12;
        }

        foreach ($fees as $key => $value) {
            
            // $fee_number[$key] = $value->fee_number;
            // $adminfee[$key] = $value->adminfee;
            $fee_payment_date[$key] = $value->fee_payment_date;
            // $total[$key] = $value->total;
            $fee_payment_date[$key]= date ( 'Y-m-d' , strtotime ( '+1 year' , strtotime ($fee_payment_date[$key]) ) );   
                $nfee = Fee::create([
                    'policy_id' => $policyId,
                    'fee_number' =>  $value->fee_number ,
                    'adminfee' => $value->adminfee,
                    'fee_payment_date' =>   $fee_payment_date[$key],
                    'total' => $value->total,
                    'user_id_creation' => $user->id
            ]);
        }     

            PolicyHistory::create([
                    //'id' => $policyId,
                    'number' =>   $policy->number ,
                    'agent_id' =>  $policy->agent_id,
                    'plan_id' =>    $policy->plan_id,
                    'deductible_id' =>  $policy->deductible_id,
                    'coverage_id' =>  $policy->coverage_id,
                    'start_date_coverage' => $policy->start_date_coverage,
                    'user_id_creation' => $user->id
            ]);        
  }
   public function postConfirmrenewalpaymentfee(Request $request){
        $user = Auth::user();
        $mail = new PHPMailer(true);
        $mailconfiguration = MailConfiguration::find('1');
        $host = $mailconfiguration->host;
        $username = $mailconfiguration->username;
        $name = $user->name;
        $password = $mailconfiguration->password;
        $protocol = $mailconfiguration->protocol;
        $port = $mailconfiguration->port;
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = $protocol;
        $mail->Port = $port;
        $mail->CharSet = "UTF-8";
        $mail->setFrom($username, $name);

        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $filename = $request['filename'];
        $feeId = $request['feeId'];
        $mode = $request['mode'];
        $fee_payment_date = array();

        $dataFee = Fee::where('id',$feeId)->where('status',1)->first();

        

        if ($request['form.method'] == 0){
            $dateData = [
                
                'payment_date' => $request['form.payment_date'],
                'check_number' => $request['form.check_number'],
                'bank_name' => $request['form.bank_name'],
                'receipt_filename' => $filename,
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 1){
            $dateData = [
                
                'payment_date' => $request['form.payment_date'],
                'transfer_number' => $request['form.transfer_number'],
                'bank_name' => $request['form.bank_name'],
                'titular_name' => $request['form.titular_name'],
                'account_type' => $request['form.account_type'],
                'account_number' => $request['form.account_number'],
                'receipt_filename' => $filename,
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 2){
            $dateData = [
                
                'payment_date' => $request['form.payment_date'],
                'bank_name' => $request['form.bank_name'],
                'deposit_number' => $request['form.deposit_number'],
                'account_number' => $request['form.account_number'],
                'receipt_filename' => $filename,
                'user_id_update' => $user->id
            ];
        }
        else{
            $dateData = [
                
                'payment_date' => $request['form.payment_date'],
                'titular_name' => $request['form.titular_name'],
                'titular_lastnames' => $request['form.titular_lastnames'],
                'bank_name' => $request['form.bank_name'],
                'card_type' => $request['form.card_type'],
                'card_mark' => $request['form.card_mark'],
                'card_bank' => $request['form.card_bank'],
                'payment_type' => $request['form.payment_type'],
                'receipt_filename' => $filename,
                'user_id_update' => $user->id
            ];
        }
        Fee::where('id',$feeId)->update($dateData);

         if($mode == 0){
            $amount_fee = 1;
        }                         
        elseif($mode == 1){
            $amount_fee = 2;
        }
        elseif($mode == 2){
            $amount_fee = 4;
        }
        elseif($mode == 3){
            $amount_fee = 12;
        }
       

        $fee_payment_date[] = $dataFee->fee_payment_date;
        
       // si es la ultima cuota a pagar entonces se generaran las cuotas del proximo año, junto a un registro de la nueva fecha de
       // inicio de cobertura para ese nuevo año
        if($dataFee->fee_number == $amount_fee){

            for($i=0; $i < $amount_fee; $i++){
                //echo "Entro en ".$i ;
                if($mode == 0){ 
                    $fee_payment_date[$i+1]= date ( 'Y-m-d' , strtotime ( '+1 year' , strtotime ($fee_payment_date[$i]) ) );   
                }elseif ($mode == 1) {
                
                    $fee_payment_date[$i+1]= date ( 'Y-m-d' , strtotime ( '+6 month' , strtotime ($fee_payment_date[$i]) ) );

                }elseif ($mode == 2) {
                    $fee_payment_date[$i+1]= date ( 'Y-m-d' , strtotime ( '+3 month' , strtotime ($fee_payment_date[$i]) ) );   
                }elseif ($mode == 3) {
                    $fee_payment_date[$i+1]= date ( 'Y-m-d' , strtotime ( '+1 month' , strtotime ($fee_payment_date[$i]) ) );   
                }

            
                    Fee::create([
                        'policy_id' => $policyId,
                        'fee_number' =>  $i+1 ,
                        'adminfee' => 0,
                        'fee_payment_date' =>   $fee_payment_date[$i+1],
                        'total' => 0,
                        'user_id_creation' => $user->id
                    ]);

            }

                    $policyData = [
                        'status' => 0,    
                        'user_id_update' =>  $user->id      
                    ];
                    PoliciesEffectiveDate::where('policy_id', '=', $policyId)->update($policyData);
                    
                    PoliciesEffectiveDate::create([
                        'start_date_coverage' =>   $fee_payment_date[1] ,
                        'policy_id' => $policyId,
                        'status' => 1,
                        'user_id_creation' => $user->id
                    ]);
            
        }
        /************************envio de correo ******************************/
        $mailformat = EmailFormat::find(5);
        $subject = $mailformat->subject;
        $title = $mailformat->title;
        $content = $mailformat->content;

        $payment_method_name = $request->input('payment_method_name');
        $payment_method = $request->input('payment_method');
        $card_mark = $request->input('card_mark');
        $card_type= $request->input('card_type');

        $payment_detail = "<br>";

         $policy = Policy::find($policyId);

                $policy_number = $policy->number;

                $agents = Agent::find($policy->agent_id);
                //$to = $mailformat->recipient;
                $to = $agents->email;
                //$mail->addCC($user->email);

                $toCC = $mailformat->cc;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }
        $applicant = Applicant::where ('policy_id', $policyId)->get();

        $subject = str_replace("policy_id",$policyId,$subject);
        $subject = str_replace("policy_number",$policy_number,$subject);
        $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
        $content = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$content);
        $content = str_replace("policy_number",$policy_number,$content);
        $content = str_replace("payment_method",$payment_method_name,$content);        
        $content = str_replace("payment_detail",$payment_detail,$content);
        //$filename = Attached::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
        $mail->addAttachment('authorization_form/'.$policyId.'/'.$filename);

          try {
                $mail->addAddress($to);
                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = $title.$content;
                //$mail->Body    = $title.'<br>'.$content_form.'<br>'.$data;
                $mail->send();
                $message = 'Successfully event send';
            }
            catch (Exception $e) {
                $message = 'error to send,'.$mail->ErrorInfo;
            }

        /**********************************************************************/
        //$affectedRows = Fee::where('id',$feeId)->update($dateData);  
        /************ fee steps *****/
        $fee = FeeStep::where('fee_id',$feeId)->first();
        if($fee ==  NULL){
            $fee = FeeStep::create([
                'fee_id' => $feeId,
                'policy_id' => $policyId,
                'user_id_creation' => $user->id
            ]);
        }
        $affectedRows = FeeStep::where('id','=',$fee->id)->update([
            'step6' => 1,
            'user_id_update' => $user->id
        ]);
        $fees_step = FeeStep::where('policy_id','=',$policyId)->where('step5', 1)->first();
      
        if($fees_step){
            $affectedRows = Fee::where('id',$feeId)->update(['paid_fee' => 1]);
            $fee = Fee::where('id',$feeId)->first();
                $number_fee = $fee->fee_number;
                if($number_fee == 1){
                    $configuration = Active::where('id',1)->first();
                    $flag = $configuration->active;
                    if($flag == 1)
                    {
                        $amount_neto = $fee->amount_titular+$fee->amount_spouse+$fee->amount_childs+$fee->amount_coverage;
                        $this->calculateCommission($policyId,$amount_neto);
                    }
                }
        }
         /******************************/
      

        return response()->success(compact('affectedRows'));
    }


function getEffectivedateactual($policy_id){

      $effective_date = DB::table('policies_effective_dates')->where('policy_id','=',$policy_id)->where('status', '=', '1')
                    ->OrderBy('created_at','DESC')
                    ->select(["policies_effective_dates.start_date_coverage"])
                    ->take(1)
                    ->get();
      
      if($effective_date ==  null){
            $effective_date = DB::table('policies')->where('id','=',$policy_id)->where('status', '=', '1')
                    ->select(["policies.start_date_coverage"])
                    ->get();        
      }              
      return response()->success($effective_date);              



    }

 function getDependentsforpolicy(){

        /*$getDependentsforpolicy = DB::table('dependents')->where('dependents.relationship','D')
                                    ->where('dependents.status','0')
                                    ->where('dependents.motive','1')
                                    ->whereRaw('TIMESTAMPDIFF(YEAR,`dependents`.`birthday`,CURDATE()) >= 24')
                                    ->select([DB::raw('CONCAT(dependents.first_name, " ", dependents.last_name) AS name'), 'dependents.*'])
                                    ->get();
        */
        //var_dump($getDependentsforpolicy);    
        $getDependentsforpolicy =  DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', '0')
                    ->select([DB::raw('policies.id as policy_id'), "policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(policies.created_at, '%d/%m/%Y') as created"), DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), "policysteps.id_policy", "policysteps.step1", "policysteps.step2", "policysteps.step3", "policysteps.step4", "policysteps.step5", "policysteps.step6", "policysteps.step7"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->leftjoin('agents', 'policies.agent_id', '=', 'agents.id')
                    ->join('policysteps', 'policies.id', '=', 'policysteps.id_policy')
                    //->join('dependents', '')
                    ->get();                            
        return response()->success(compact('getDependentsforpolicy')); 


                                    
 }   

public function postPolicies2()
    {
        $usercreate = Auth::user();
        /*$showhidepregnant = 0;        
        if (Input::get('have_prev_safe') == true) {
            $showhidepregnant = 1;
        }
        */
       $policyId = Input::get('policyId');
       
        if($policyId == "0"){
        $policy = Policy::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'email' => Input::get('email'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'identity_document' => Input::get('identity_document'),
            'user_id_creation' => $usercreate->id
        ]);

        if ($policy->id) {
            $applicant = Applicant::create([
            'policy_id' => $policy->id,
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'address' => Input::get('address'),
            'gender' => Input::get('gender'),
            'email' => Input::get('email'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'civil_status' => Input::get('civil_status'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city_id' => Input::get('city_id'),
            'place_of_birth' => Input::get('place_of_birth'),
            'birthday' => Input::get('birthday'),
            'height' => Input::get('height'),
            'height_unit_measurement' => Input::get('height_unit_measurement'),
            'weight' => Input::get('weight'),
            'weight_unit_measurement' => Input::get('weight_unit_measurement'),
            'identity_document' => Input::get('identity_document'),
            'occupation'  => Input::get('occupation'),
            'user_id_creation' => $usercreate->id
        ]);
        }

        if ($policy->id) {
                $steps = Policystep::create([
                    'id_policy' => $policy->id,
                    'user_id_creation' => $usercreate->id
                ]);
            }
        $policyId =  $policy->id;  

        }else{
            $policyData = [
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
                'identity_document' => Input::get('identity_document'),
                'email' => Input::get('email'),
                'mobile' => Input::get('mobile'),
                'phone' => Input::get('phone'),
                'identity_document' => Input::get('identity_document'),
                'user_id_update' => $usercreate->id
            ];     
       

        /********************** Envio de alerta a emisiones **********************************/
                
                //$sala =  "emisiones";
                //$message = "Se ha ingresado una nueva Póliza ID: POL"+$policy->id;
                //NotificationController::notificar($sala, $message);

        /*************************************************************************/
        $affectedRows = Policy::where('id', '=',  $policyId)->update($policyData);

            $applicantData = [
                'first_name' => Input::get('first_name'),
                'last_name' => Input::get('last_name'),
                'address' => Input::get('address'),
                'gender' => Input::get('gender'),
                'email' => Input::get('email'),
                'mobile' => Input::get('mobile'),
                'phone' => Input::get('phone'),
                'civil_status' => Input::get('civil_status'),
                'country_id' => Input::get('country_id'),
                'province_id' => Input::get('province_id'),
                'city_id' => Input::get('city_id'),
                'place_of_birth' => Input::get('place_of_birth'),
                'birthday' => Input::get('birthday'),
                'height' => Input::get('height'),
                'height_unit_measurement' => Input::get('height_unit_measurement'),
                'weight' => Input::get('weight'),
                'weight_unit_measurement' => Input::get('weight_unit_measurement'),
                'identity_document' => Input::get('identity_document'),
                'occupation'  => Input::get('occupation'),
                'user_id_update' => $usercreate->id
            ];
            $affectedRows = Applicant::where('id', '=',  $policyId)->update($applicantData);
        }


        return response()->success($policyId);


    } 


     public function postPlandata(Request $request)
    {
        $policyId = Input::get('policy_id');
        $userupdate = Auth::user();
    
        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $planData = [
            'plan_id' => Input::get('plan_id'),
            'deductible_id' => Input::get('deductible_id'),
            'coverage_id' => Input::get('coverage_id'),
            'agent_id' => Input::get('seller_id'),
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Policy::where('id', '=',  $policyId)->update($planData);

        /*if ($planForm['data.is_renewal'] == true && ($consulta[0]->plan_id !== $planData['plan_id'] || $consulta[0]->deductible_id !== $planData['deductible_id'] || $consulta[0]->coverage_id !== $planData['coverage_id'])){//

            $HistoricalRenovation=HistoricalRenovation::create([
            'id_policy' => $policyId,
            'id_plan'=> $planData['plan_id'],
            'id_deductibles'=>$planData['deductible_id'],
            'id_coverage'=>$planData['coverage_id'],
            'user_id_creation'=>$userupdate->id,

        ]);
        }*/
        //return response()->success($affectedRows);
        return response()->success($policyId);

    }
    function getLastpaymentfee($policy_id){

         $last_fee = Fee::where('policy_id','=',$policy_id)
                        ->where('status','=',1)
                        ->where('paid_fee','=',1)
                        ->select(['fees.*'])
                        //->join('payments','fees.policy_id','=','payments.policy_id')
                       ->OrderBy('created_at','ASC')
                        ->take(1)
                        ->get();
        //var_dump($last_fee[0]['fee_number']);   
        
        $number_fee =  $last_fee[0]['fee_number'];            

        /*if (($number_fee == 1) && ($last_fee[0]['method'] != $last_fee[0]['method_f'])){
            $last_fee[0]['method'] = $last_fee[0]['method_f'];
        } 

        if (($number_fee == 1) && ($last_fee[0]['payment_date'] != $last_fee[0]['payment_date_f'])){
            $last_fee[0]['payment_date'] = $last_fee[0]['payment_date_f'];
        } */

        if( $last_fee[0]['method'] == 0){
            $last_fee[0]['method'] = "Cheque";
        }               
        else if( $last_fee[0]['method'] == 1){
            $last_fee[0]['method'] = "Transferencia";
        }
        else if( $last_fee[0]['method'] == 2){
            $last_fee[0]['method'] = "Depósito Bancario";
        }
        else{
            $last_fee[0]['method'] = "Tarjeta de Crédito";
        }

         return response()->success(compact('last_fee'));                 

             


    }

    public function postInvoice(Request $request){

        $user = Auth::user();
        $mail = new PHPMailer(true);
        $mailconfiguration = MailConfiguration::find('1');
        $host = $mailconfiguration->host;
        $username = $mailconfiguration->username;
        $name = $user->name;
        $password = $mailconfiguration->password;
        $protocol = $mailconfiguration->protocol;
        $port = $mailconfiguration->port;
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = $protocol;
        $mail->Port = $port;
        $mail->CharSet = "UTF-8";
        $mail->setFrom($username, $name);
        $policyId = $request['policyId'];
        //$payment_id = $request['payment_id'];
        $fee_id = $request['fee_id'];
        $filename = $request['filename'];

        $affectedRows = Fee::where('id',$fee_id)->update([
            'invoice_filename' => $filename,
            'user_id_update' => $user->id
        ]);
        
                $mailformat = EmailFormat::find(9);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                $toCC = $mailformat->cc;

                //$policy_id = $request->input('policy_id');

                $policy = Policy::find($policyId);
                $policy_number = $policy->number;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;
                //$cc = $user->email;
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policyId)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                $content = str_replace("policy_number",$policy_number,$content);
                $content = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$content);
                
                
                //$filename = Attached::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
                $mail->addAttachment('authorization_form/'.$policyId.'/'.$filename);
                try {
                    $mail->addAddress($to);
                    $mail->isHTML(true);
                    $mail->Subject = $subject;
                    $mail->Body    = $title.$content;
                    $mail->send();
                    $message = 'Successfully event send';
                }
                catch (Exception $e) {
                    $message = 'error to send,'.$mail->ErrorInfo;
            }
            /************ fee steps *****/
              $fee = FeeStep::where('fee_id',$fee_id)->first();
                if($fee ==  NULL){
                    $fee = FeeStep::create([
                        'fee_id' => $fee_id,
                        'policy_id' => $policyId,
                        'user_id_creation' => $user->id
                ]);
                }
            $affectedRows = FeeStep::where('id','=',$fee->id)->update([
                'step5' => 1,
                'user_id_update' => $user->id
            ]);
            $fees_step = FeeStep::where('policy_id','=',$policyId)->where('step6', 1)->first();
            if($fees_step){
                $affectedRows = Fee::where('id',$fee_id)->update(['paid_fee' => 1]);
                $fee = Fee::where('id',$fee_id)->first();
                $number_fee = $fee->fee_number;
                if($number_fee == 1){
                    $configuration = Active::where('id',1)->first();
                    $flag = $configuration->active;

                    if($flag == 1) //si está activo el pago de comisiones
                    {
                        $amount_neto = $fee->amount_titular+$fee->amount_spouse+$fee->amount_childs+$fee->amount_coverage;
                        $this->calculateCommission($policyId,$amount_neto);
                    }

                }
            }
         /******************************/


        return response()->success(compact('affectedRows'));
    }
    public function postCoverage(){
        $userupdate = Auth::user();
        $policyId = Input::get('policy_id');
        $have_renewal = Input::get('have_renewal');

        $startdate = date('Y-m-d',strtotime(Input::get('start_date_coverage')));
        $actual_date = PoliciesEffectiveDate::where('policy_id','=',$policyId)->where('status',1)->first();
        $actual_coverage_start_date = $actual_date['start_date_coverage'];
        //var_dump($actual_coverage_start_date);

        $policy = Policy::find($policyId);
               
        if($actual_coverage_start_date !=  $startdate){

            $policyData = [
                    'have_renewal' => 1,  
                    'continuity_coverage' => (Input::get('continuity_coverage') == true) ? 1 : 0,
                    'have_prev_safe' => Input::get('have_prev_safe'),
                    'company_name' => (Input::get('have_prev_safe') == true) ?  Input::get('company_name') : ' ',
                    'plan_name' => (Input::get('have_prev_safe') == true) ?  Input::get('plan_name') : ' ' ,
                    'is_international' => (Input::get('is_international') == true) ? 1 : 0,
                    'user_id_update' => $userupdate->id         
                ];
            $affectedRows = Policy::where('id', '=', $policyId)->update($policyData);

            $effectiveData = [
                'status' => 0,   
                'user_id_update' => $userupdate->id         

            ];
            $effective_date = PoliciesEffectiveDate::where('policy_id','=',$policyId)->where('status',1)->update($effectiveData);

            PoliciesEffectiveDate::create([
                'start_date_coverage' => $startdate,
                'policy_id' => $policyId,
                'user_id_creation' => $userupdate->id
            ]);

         
            PolicyHistory::create([
                'number' =>  $policy->number,
                'policy_id' =>  $policyId,
                'applicant_id' =>  $policyId,
                'agent_id' =>  $policy->agent_id,
                'plan_id' =>  $policy->plan_id,
                'deductible_id' =>  $policy->deductible_id,
                'coverage_id' =>  $policy->coverage_id,
                'start_date_coverage' =>  $actual_coverage_start_date,
                 'user_id_creation' => $userupdate->id
            ]);

            $this->calculateFeeWithNewStartDate($policyId);
        }else{
             $dateData = [
            'user_id_update' => $userupdate->id,
            'continuity_coverage' => (Input::get('continuity_coverage') == true) ? 1 : 0,
            'have_prev_safe' => Input::get('have_prev_safe'),
            'company_name' => (Input::get('have_prev_safe') == true) ?  Input::get('company_name') : ' ',
            'plan_name' => (Input::get('have_prev_safe') == true) ?  Input::get('plan_name') : ' ',
            'is_international' => (Input::get('is_international') == true) ? 1 : 0,
            'user_id_update' => $userupdate->id
        ];

             $affectedRows = Policy::where('id', '=',  $policyId)->update($dateData);

        }
        return response()->success($policyId);
    }



    public function postInvoiceemissionrenewal(Request $request){
         $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $filename = $request['filename'];

        $affectedRows = Attached::where('payment_id',$payment_id)->update([
            'invoice_filename' => $filename,
            'user_id_update' => $user->id
        ]);


        $fee = Fee::where('policy_id',$policyId)->where('fee_number',1)->OrderBy('created_at','DESC')->first();
        $feeId = $fee->id; 
        $affectedRows = Fee::where('id',$feeId)->update([
            'invoice_filename' => $filename,
            'user_id_update' => $user->id
        ]);

        return response()->success(compact('affectedRows'));
    }

    /********************** new dependet policy *****************/
public function postCreatepolicydependents()
    {
        $usercreate = Auth::user();
        /*$showhidepregnant = 0;        
        if (Input::get('have_prev_safe') == true) {
            $showhidepregnant = 1;
        }
        */
        $dependent = Input::get('dependent');
        //$dependentData = Dependent::find($dependentId);
        //var_dump($dependent["name"]);

        $policy = Policy::create([
            'first_name' => $dependent["name"],
            'last_name' => $dependent["lastname"],
            'identity_document' => $dependent["pid_num"],
            'user_id_creation' => $usercreate->id
        ]);
        
        if ($policy->id) {
            $applicant = Applicant::create([
            'policy_id' => $policy->id,
            'first_name' => $dependent["name"],
            'last_name' => $dependent["lastname"],
            //'address' => Input::get('address'),
            'gender' => $dependent["sex"],
            //'email' => Input::get('email'),
            //'mobile' => Input::get('mobile'),
            //'phone' => Input::get('phone'),
            //'civil_status' => Input::get('civil_status'),
            //'country_id' => Input::get('country_id'),
            //'province_id' => Input::get('province_id'),
            //'city_id' => Input::get('city_id'),
            //'place_of_birth' => Input::get('place_of_birth'),
            'birthday' => $dependent["dob"],
            'height' => $dependent["height"],
            'height_unit_measurement' => $dependent["height_unit_measurement"],
            'weight' => $dependent["weight"],
            'weight_unit_measurement' => $dependent["weight_unit_measurement"],
            'identity_document' => $dependent["pid_num"],
            'document_type'=> $dependent["pid_type"],
            //'occupation'  => Input::get('occupation'),
            'user_id_creation' => $usercreate->id
        ]);
        }

         if ($policy->id) {
                $steps = Policystep::create([
                    'id_policy' => $policy->id,
                    'user_id_creation' => $usercreate->id
                ]);
            }
         $policyId =  $policy->id;  
       
        
           


        return response()->success($policyId);


    }

    public function exportExcel(Request $request){
        $header = $request['header'];
        $values = $request['values'];
        $file = \Excel::create('Excel', function($excel) use ($header, $values) {
            $excel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $excel->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $excel->sheet('Renovaciones', function($sheet) use($header, $values) {
                $sheet->cell('A1:K1', function($cell) {
                    $cell->setFont(array(
                        'family'     => 'Tahoma',
                        'size'       => '10',
                        'bold'       =>  true
                    ));
                    $cell->setBackground('#c6591100');
                });

                $sheet->row(1, $header);

                foreach($values as $key => $value){
                    if ($value['mode']=='Mensual')
                        $option = 'Montly';
                    if ($value['mode']=='Trimestral')
                        $option = 'Quarterly';
                    if ($value['mode']=='Semestral')
                        $option = 'Semiannual';
                    if ($value['mode']=='Anual')
                        $option = 'Annual';
                    $sheet->row($key+2, [
                        $value['number'], $value['number'], $value['first_name'].' '.$value['last_name'], $value['plan_description'], $value['option'], $value['fee_payment_date'] , $option, $value['agent_first_name'].' '.$value['agent_last_name'],'GILBERT & BOLOÑA S. A. AGENCIA ASESORA PRODUCTORA DE SEGUROS SEGUGILBO', 'Próximo Pago', $value['total']
                    ]);
                }
            });
        })->export('xls');
        return Response::download($file, 'output.xls', ['Content-Type: text/xls']);
    }

    /**
     * Get Policy details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        //$policy = Policy::find($id);
        $policy = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.id', '=', $id)
                    ->select(["policies.*", "applicants.*"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->get();
        return response()->success($policy);
    }

    /**
     * Update applicant data plan-deductible-coverage .
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $userupdate = Auth::user();    
        //$renewal = new RenewalController;
        $planForm = array_dot(
            app('request')->only(
                'data.policy_id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.email',
                'data.phone',
                'data.mobile',
                'data.plan_id',
                'data.deductible_id',
                'data.coverage_id',
                'data.is_renewal',
                'data.seller_id'
            )
        );

        $policyId = intval($planForm['data.policy_id']);

        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $userupdate = Auth::user();

        $planData = [
            'plan_id' => $planForm['data.plan_id'],
            'deductible_id' => $planForm['data.deductible_id'],
            'coverage_id' => $planForm['data.coverage_id'],
            'agent_id' => $planForm['data.seller_id'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Policy::where('id', '=',  $policyId)->update($planData);

        

        /*if ($planForm['data.is_renewal'] == true && ($consulta[0]->plan_id !== $planData['plan_id'] || $consulta[0]->deductible_id !== $planData['deductible_id'] || $consulta[0]->coverage_id !== $planData['coverage_id'])){//

            $HistoricalRenovation=HistoricalRenovation::create([
            'id_policy' => $policyId,
            'id_plan'=> $planData['plan_id'],
            'id_deductibles'=>$planData['deductible_id'],
            'id_coverage'=>$planData['coverage_id'],
            'user_id_creation'=>$userupdate->id,

        ]);
        }*/


         $this->calculateFeeAmount($policyId);
        //return response()->success($affectedRows);
        return response()->success($planForm['data.policy_id']);

    }




public function postUpdatedependents(Request $request)
    {
        $userupdate = Auth::user();
        $aplicantid = $request['aplicantid'];
        $dependents = Input::get('dependents');

        
        /*$this->validate($request, [
            'dependents' => 'required',
        ]);*/

        for ($i = 0; $i < count($dependents); $i++){
            if(isset($dependents[$i]['id'])){
                $id = $dependents[$i]['id'];
                $dependentData = [
                    'applicant_id' => $aplicantid,
                    'first_name' => $dependents[$i]['name'],
                    'last_name' => $dependents[$i]['lastname'],
                    'document_type' => $dependents[$i]['pid_type'],
                    'identity_document' => $dependents[$i]['pid_num'],
                    'relationship' => $dependents[$i]['role'],
                    'birthday' => $dependents[$i]['dob'],
                    'height' => $dependents[$i]['height'],
                    'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                    'weight' => $dependents[$i]['weight'],
                    'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                    'gender' => $dependents[$i]['sex'],
                    'user_id_creation' => $userupdate->id
                ];    

                //var_dump($dependentData);
                $dependent = Dependent::where('id', '=', $id)->update($dependentData);
            }else{
            $dependent = Dependent::create([
                'applicant_id' => $aplicantid,
                'first_name' => $dependents[$i]['name'],
                'last_name' => $dependents[$i]['lastname'],
                'document_type' => $dependents[$i]['pid_type'],
                'identity_document' => $dependents[$i]['pid_num'],
                'relationship' => $dependents[$i]['role'],
                'birthday' => $dependents[$i]['dob'],
                'height' => $dependents[$i]['height'],
                'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                'weight' => $dependents[$i]['weight'],
                'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                'gender' => $dependents[$i]['sex'],
                'user_id_creation' => $userupdate->id
            ]);
            
        }

        }
        $this->calculateFeeAmount($aplicantid);
        return response()->success($aplicantid);
    }


    public function calculateFeeAmount($policyId){
        $userupdate = Auth::user();    
        $configurations = Configuration::all();

        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $applicant = DB::table('applicants')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();

        $dependents = DB::table('dependents')->where('status','=','1')->where('applicant_id','=',$policyId)
                    ->get();
        $payment =  DB::table('payments')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first(); 


        if($payment->mode == 0){
            $nfee = 1;
        }else if($payment->mode == 1){
            $nfee = 2;
        }else if($payment->mode == 2){
            $nfee = 4;
        }else{
            $nfee = 12;
        }


        $childs = 0;
        $ageh = 0;
        
          foreach ($dependents as $key => $value) {
            
            if($dependents[$key]->relationship == "H"){
               list($Y,$m,$d) = explode("-", $dependents[$key]->birthday);
               $ageh =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ;
            }else{
              $childs = $childs + 1; 
            }            
           
        }     



      list($Y,$m,$d) = explode("-",  $applicant->birthday);
      $age =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ; 

      $amount = DeductibleDetail::where('deductible_id',$consulta[0]->deductible_id)->whereRaw($age." between age_start and age_end")->first();
      if( $ageh > 0){
        $amounth =  DeductibleDetail::where('deductible_id',$consulta[0]->deductible_id)->whereRaw($ageh." between age_start and age_end")->first();
      }

       if (($childs > 0) && ($childs <= 3)){
            $amountc = DeductibleDetail::where('deductible_id',$consulta[0]->deductible_id)->where('child_quantity',$childs)->first();
        }
        elseif ($childs > 3){
            $amountc = DeductibleDetail::where('deductible_id',$consulta[0]->deductible_id)->where('child_quantity',3)->first();
        }
        else{
            $amountc = 0;
        }

        $coverage = Coverage::with('detail')->where('id', $consulta[0]->coverage_id)->first();
        $amountCoverage = (isset($coverage->name)) ? (float) $coverage->detail->amount : 0;
        $amountCoverageT = $amountCoverage/$nfee;
        $amountTitular =  (float) $amount->amount/$nfee ;
        $amountSpouse = (isset($amounth->amount)) ? (float) $amounth->amount/$nfee : 0;
        $amountChild = (isset($amountc->amount)) ? (float) $amountc->amount/$nfee : 0;
        $totaldetails =  $amountTitular +  $amountSpouse + $amountChild +  $amountCoverageT;
        $installment_fee = 0;
        if($payment->mode == 1){
            $installment_fee = (float) $totaldetails * 0.06;            
          }
        else if($payment->mode == 2 || $payment->mode == 3){
           $installment_fee = (float) $totaldetails * 0.1;            
          }
        $totalfee = $totaldetails +  $installment_fee;
        /*descuento*/
        $discount = 0;
        if($payment->discount == 1){
            $discount =  ($totalfee-($amountCoverageT))*$payment->discount_percent/100;
        }


         /*impuestos*/ 
        $taxscc = (float) $configurations[0]->value;
        $iva = (float) $configurations[2]->value;
        $totalTaxscc = ($totaldetails+$installment_fee)*($taxscc/100);
        $amountFee =  ($totaldetails +  $installment_fee + $totalTaxscc) - $discount;

        $affectedRows = Fee::where('policy_id','=', $policyId)->where('paid_fee','=',0)->where('status','=',1)->update([
            'total' =>  $amountFee,
            'amount_titular' => $amountTitular,
            'amount_spouse'  => $amountSpouse,
            'amount_childs'  => $amountChild,
            'amount_coverage' => $amountCoverageT,
            'user_id_update' => $userupdate->id
        ]);

        return true;


    }//


    public function calculateFeeDates($policyId){
        $userupdate = Auth::user();    
        $configurations = Configuration::all();
        $current_year = date ('Y');
        $current_month = date ('m');


        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->first();




        $applicant = DB::table('applicants')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();

        $dependents = DB::table('dependents')->where('status','=','1')->where('applicant_id','=',$policyId)
                    ->get();
        $payment =  DB::table('payments')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first(); 
        $effective_date = DB::table('policies_effective_dates')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();    
        
        //print_r($effective_date);
        
        /*if(!isset($effective_date)){
            $start_date_coverage = $consulta->start_date_coverage;
            $policy_year = date('Y', strtotime( $start_date_coverage));
            $policy_month = date('m', strtotime( $start_date_coverage));
            $monthDiff =  $policy_month - $current_month;
            //$yearDiff = $current_year - $policy_year;
        
            if($policy_month >= $current_month){
                $yearDiff =  ($current_year - 1) - $policy_year ;  
            }
            else{
                $yearDiff =  $current_year - $policy_year;
            }  
            var_dump($start_date_coverage);
            $effective_date_1 = date ( 'Y-m-d' , strtotime ( '+'.$yearDiff.' year' ,  strtotime($start_date_coverage) ));
            echo "valor----- ".$effective_date_1;            
            PoliciesEffectiveDate::create([
                'start_date_coverage'  => $effective_date_1,
                'policy_id' => $policyId,
                'user_id_creation' => $userupdate->id
            ]);
            $effective_date = DB::table('policies_effective_dates')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();     

                    
        }*/ 



        if($payment->mode == 0){
            $nfee = 1;
            $period = '+1 year';
        }else if($payment->mode == 1){
            $nfee = 2;
            $period = '+6 month';
        }else if($payment->mode == 2){
            $nfee = 4;
            $period = '+3 month';
        }else{
            $nfee = 12;
            $period = '+1 month';
        }


        $childs = 0;
        $ageh = 0;
        
          foreach ($dependents as $key => $value) {
            
            if($dependents[$key]->relationship == "H"){
               list($Y,$m,$d) = explode("-", $dependents[$key]->birthday);
               $ageh =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ;
            }else{
              $childs = $childs + 1; 
            }            
           
        }     



      list($Y,$m,$d) = explode("-",  $applicant->birthday);
      $age =  date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y ; 

      $amount = DeductibleDetail::where('deductible_id',$consulta->deductible_id)->whereRaw($age." between age_start and age_end")->first();
      if( $ageh > 0){
        $amounth =  DeductibleDetail::where('deductible_id',$consulta->deductible_id)->whereRaw($ageh." between age_start and age_end")->first(); 
      }

       if (($childs > 0) && ($childs <= 3)){
            $amountc = DeductibleDetail::where('deductible_id',$consulta->deductible_id)->where('child_quantity',$childs)->first();
        }
        elseif ($childs > 3){
            $amountc = DeductibleDetail::where('deductible_id',$consulta->deductible_id)->where('child_quantity',3)->first();
        }
        else{
            $amountc = 0;
        }

        $coverage = Coverage::with('detail')->where('id', $consulta->coverage_id)->first();
        $amountCoverage = (isset($coverage->name)) ? (float) $coverage->detail->amount : 0;
        $amountCoverageT = $amountCoverage/$nfee;
        $amountTitular =  (float) $amount->amount/$nfee ;
        $amountSpouse = (isset($amounth->amount)) ? (float) $amounth->amount/$nfee : 0;
        $amountChild = (isset($amountc->amount)) ? (float) $amountc->amount/$nfee : 0;
        $totaldetails =  $amountTitular +  $amountSpouse + $amountChild +  $amountCoverageT;
        $installment_fee = 0;
        if($payment->mode == 1){
            $installment_fee = (float) $totaldetails * 0.06;            
          }
        else if($payment->mode == 2 || $payment->mode == 3){
           $installment_fee = (float) $totaldetails * 0.1;            
          }
        $totalfee = $totaldetails +  $installment_fee;   
        /*descuento*/
        $discount = 0;
        if($payment->discount == 1){
            $discount =  ($totalfee-($amountCoverageT))*$payment->discount_percent/100;
        }


         /*impuestos*/ 
        $taxscc = (float) $configurations[0]->value;
        $iva = (float) $configurations[2]->value;
        $totalTaxscc = ($totaldetails+$installment_fee)*($taxscc/100);
        $amountFee =  ($totaldetails +  $installment_fee + $totalTaxscc) - $discount;

        $affectedRows = Fee::where('policy_id','=', $policyId)->where('paid_fee','=',0)->where('status','=',1)->update([
           'status' => 0,
           'user_id_update' => $userupdate->id
        ]);

        $fee_payment_date[0] = $effective_date->start_date_coverage;
        for ($i=1; $i < $nfee ; $i++) { 
          $fee_payment_date[$i]= date ( 'Y-m-d' , strtotime ( $period , strtotime($fee_payment_date[$i-1]) ) );   
        }


        if($payment->mode == 0){
            Fee::create([
                  'policy_id' => $policyId,
                  'fee_number' => 1,
                  'fee_payment_date' => $fee_payment_date[0],
                  'total' =>  $amountFee,
                  'amount_titular' => $amountTitular,
                  'amount_spouse'  => $amountSpouse,
                  'amount_childs'  => $amountChild,
                  'amount_coverage' => $amountCoverageT, 
                  'method' => $payment->method,
                  'user_id_creation' => $userupdate->id
              ]); 

        }else{
            for ($i=0; $i < $nfee ; $i++) { 
                Fee::create([
                  'policy_id' => $policyId,
                  'fee_number' => $i+1,
                  'fee_payment_date' => $fee_payment_date[$i],
                  'total' =>  $amountFee,
                  'amount_titular' => $amountTitular,
                  'amount_spouse'  => $amountSpouse,
                  'amount_childs'  => $amountChild,
                  'amount_coverage' => $amountCoverageT,
                  'method' => $payment->method,
                  'user_id_creation' => $userupdate->id
              ]); 
            }
        }
        return true;


    }

    public function calculateFeeWithNewStartDate($policyId){
        $userupdate = Auth::user();
        $current_year = date ('Y');
        $current_month = date ('m');

        $configurations = Configuration::all();

        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $applicant = DB::table('applicants')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();

        $dependents = DB::table('dependents')->where('status','=','1')->where('applicant_id','=',$policyId)
                    ->get();
        $payment =  DB::table('payments')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first(); 
        $effective_date = DB::table('policies_effective_dates')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();  
        $fees = DB::table('fees')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->get();   

        if($effective_date == null){
            $start_date_coverage = $consulta->start_date_coverage;
            $policy_year = date('Y', strtotime( $start_date_coverage));
            $policy_month = date('m', strtotime( $start_date_coverage));
            $monthDiff =  $policy_month - $current_month;
            //$yearDiff = $current_year - $policy_year;
        
            if($policy_month >= $current_month){
                $yearDiff =  ($current_year - 1) - $policy_year ;  
            }
            else{
                $yearDiff =  $current_year - $policy_year;
            }  
            $effective_date_1 = date ( 'Y-m-d' , strtotime ( '+'.$yearDiff.' year' ,  strtotime($start_date_coverage) ));
             
            PoliciesEffectiveDate::create([
                'start_date_coverage'  => $effective_date_1,
                'policy_id' => $policyId,
                'user_id_creation' => $userupdate->id
            ]);
         $effective_date = DB::table('policies_effective_dates')->where('status','=','1')->where('policy_id','=',$policyId)
                    ->first();    

                    
        }                                 



        if($payment->mode == 0){
            $nfee = 1;
            $period = '+1 year';
        }else if($payment->mode == 1){
            $nfee = 2;
            $period = '+6 month';
        }else if($payment->mode == 2){
            $nfee = 4;
            $period = '+3 month';
        }else{
            $nfee = 12;
            $period = '+1 month';
        }


        $fee_payment_date[0] = $effective_date->start_date_coverage;
        for ($i=1; $i < $nfee ; $i++) { 
          $fee_payment_date[$i]= date ( 'Y-m-d' , strtotime ( $period , strtotime($fee_payment_date[$i-1]) ) );   
        }


        /*for ($i=0; $i < $nfee ; $i++) { 
            

        }*/

        foreach ($fees as $key => $value) {
           $affectedRows = Fee::where('id', '=',  $fees[$key]->id)->update([
                'fee_payment_date' => $fee_payment_date[$key],
                'user_id_update' => $userupdate->id
            ]);
        }
        return true;


    }

/**
     * Update applicant data plan-deductible-coverage .
     *
     * @return JSON success message
     */
    public function postApplicant(Request $request)
    {
        $planForm = array_dot(
            app('request')->only(
                'data.policy_id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.email',
                'data.phone',
                'data.mobile',
                'data.birthday',
                'data.place_of_birth',
                'data.address',
                'data.country_id',
                'data.gender',
                'data.province_id',
                'data.civil_status',
                'data.city_id',
                'data.occupation',
                'data.height',
                'data.height_unit_measurement',
                'data.weight',
                'data.weight_unit_measurement'
            )
        );

        $policyId = intval($planForm['data.policy_id']);
        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $userupdate = Auth::user();

        $applicantData = [
            'first_name' => $planForm['data.first_name'],
            'last_name' => $planForm['data.last_name'],
            'identity_document' => $planForm['data.identity_document'],
            'email' => $planForm['data.email'],
            'phone' => $planForm['data.phone'],
            'mobile' => $planForm['data.mobile'],
            'birthday' => $planForm['data.birthday'],
            'place_of_birth' => $planForm['data.place_of_birth'],
            'address' => $planForm['data.address'],
            'country_id' => $planForm['data.country_id'],
            'gender' => $planForm['data.gender'],
            'province_id' => $planForm['data.province_id'],
            'civil_status' => $planForm['data.civil_status'],
            'occupation' => $planForm['data.occupation'],
            'height' => $planForm['data.height'],
            'height_unit_measurement' => $planForm['data.height_unit_measurement'],
            'weight' => $planForm['data.weight'],
            'weight_unit_measurement' => $planForm['data.weight_unit_measurement'],           
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Applicant::where('id', '=',  $policyId)->update($applicantData); 

         $planData = [
            'first_name' => $planForm['data.first_name'],
            'last_name' => $planForm['data.last_name'],
            'identity_document' => $planForm['data.identity_document'],
            'email' => $planForm['data.email'],
            'phone' => $planForm['data.phone'],
            'mobile' => $planForm['data.mobile'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Policy::where('id', '=',  $policyId)->update($planData);

        /*if ($planForm['data.is_renewal'] == true && ($consulta[0]->plan_id !== $planData['plan_id'] || $consulta[0]->deductible_id !== $planData['deductible_id'] || $consulta[0]->coverage_id !== $planData['coverage_id'])){//

            $HistoricalRenovation=HistoricalRenovation::create([
            'id_policy' => $policyId,
            'id_plan'=> $planData['plan_id'],
            'id_deductibles'=>$planData['deductible_id'],
            'id_coverage'=>$planData['coverage_id'],
            'user_id_creation'=>$userupdate->id,

        ]);
        }*/
        $this->calculateFeeAmount($policyId);
        //return response()->success($affectedRows);
        return response()->success($planForm['data.policy_id']);

    }

    public function getPoliciyplan($id){

        $policyplan = DB::table('policies')->where('policies.id', '=', $id)
                                ->join('plans', 'policies.plan_id', '=', 'plans.id')
                                ->select(['policies.plan_id','plans.max_dependent_age'])
                                ->first();
        return response()->success($policyplan);
    }

    public function postActivate(){
         $userupdate = Auth::user();
         $policyId = Input::get('policyId');
          $policyData = [
            'status' => 1,
            'user_id_update' =>  $userupdate->id
        ];
        $affectedRows = Policy::where('id', '=', $policyId)->update($policyData);
        return response()->success('success');
    }

public function calculateCommission($policy_id, $amount){
        $user = Auth::user(); 
        $flag =  false;
        $agents_commission = [];
        $agents_data = array();
        $policy =  Policy::where('id', $policy_id)->select(['agent_id','plan_id','deductible_id','coverage_id'])->first();
        $effective_date = PoliciesEffectiveDate::where('id', $policy_id)->where('status', 1)->select(['start_date_coverage'])->first();
        $agent_principal = Agent::where('id',$policy->agent_id)->first();
        $company = Company::where('id',1)->first();
        $company_commission = $company->commission_percentage;

        $amount_total_commission = ($amount * $company_commission) / 100 ; // calculo de el monto de la comision total a repartir
        //echo "Comision a repartir ".$amount_total_commission. "del monto total ".$amount;
           
        // calculo de comision del agente que ejecuta la venta, se ubica el % de comision en todos los niveles y configuraciones
            $commission = Commission::where('agent_id',$policy->agent_id)->where('plan_id',$policy->plan_id)->first();
            
            if($commission == null || $commission->plan_id == 0 )
            {
                $branch_id = Plan::where('id',$policy->plan_id)->select(['branch_id'])->first();
                $commission = Commission::where('agent_id',$policy->agent_id)->where('branch_id',$branch_id)->first();                
                
                if($commission == null || $commission->branch_id == 0)
                {
                    
                    $commission_agent = $agent_principal->commission;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;        
                                  
                }else
                {
                    $commission_agent = $commission->commission_renewal;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                }                
            }else
            {
                $commission_agent = $commission->commission_renewal;
                $commission_value = ($amount * $commission_agent) / 100;
                $rest_commission =  $amount_total_commission - $commission_value; 
                $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
            }
        //*************************************************************************************************************************

        // calculo de comision de los agentes padres,  se ubica el % de comision en todos los niveles y configuraciones   

            $i = 1;    
            while ($agent_principal->leader  > 0) 
            {    
                $agent_principal =  Agent::where('id',$agent_principal->leader)->where('status',1)->first();
                $commission = Commission::where('agent_id',$policy->agent_id)->where('plan_id',$policy->plan_id)->first();
                
                if($commission == null || $commission->plan_id == 0 )
                {
                    $branch_id = Plan::where('id',$policy->plan_id)->select(['branch_id'])->first();
                    $commission = Commission::where('agent_id',$policy->agent_id)->where('branch_id',$branch_id)->first();  
                    
                    if($commission == null || $commission->branch_id == 0)
                    {
                        $commission_agent = $agent_principal->commission;
                        $commission_value = ($amount * $commission_agent) / 100;
                        $rest_commission =  $amount_total_commission - $commission_value; 
                        $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                                  
                    }else
                    {
                        $commission_agent = $commission->commission_renewal;
                        $commission_value = ($amount * $commission_agent) / 100;
                        $rest_commission =  $amount_total_commission - $commission_value; 
                        $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;    
                    }                                  
                }
                else
                {
                    $commission_agent = $commission->commission_renewal;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                }           
                //$agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                
                if($agent_principal->leader == 0)
                {
                    $flag = true;
                }

                $i++ ;    
            }
         //***************************************************************************************************************************   
        //var_dump($agents_data);
        $long = sizeof($agents_data);    
        for ($i=0; $i <= $long-1 ; $i++) { 
            if($i == 0)
            {
                $commission_agent = (float)$agents_data[$i]['commission'];       
                $amount_commission_agent = ($amount * $commission_agent) / 100;
                $agents_data_final[$i] = array('agent_id' =>  $agents_data[$i]['agent_id'], 'commission' => $agents_data[$i]['commission'],"commission_real" => $commission_agent,  'amount_commission' => $amount_commission_agent);
            }else if($i > 0 )
            {
                $commission_agent = (float)$agents_data[$i]['commission'] -  (float)$agents_data[$i-1]['commission'];    
                $amount_commission_agent = ($amount * $commission_agent) / 100;
                $agents_data_final[$i] = array('agent_id' =>  $agents_data[$i]['agent_id'], 'commission' => $agents_data[$i]['commission'],"commission_real" => $commission_agent, 'amount_commission' => $amount_commission_agent);
            }
        }    

       $company_commission_real = (float)$company_commission - (float)$agents_data[$long-1]['commission'];
       $amount_commission_company = ($amount * $company_commission_real) / 100;
       $company_commission_data =  array('company_id' => $company->id , 'commission' => $company_commission, "commission_real" => $company_commission_real, 'amount_commission' => $amount_commission_company);  
      
        foreach ($agents_data_final as $key => $value) {
        AgentCommision::create([
                'agent_id' => $agents_data_final[$key]['agent_id'],
                'policy_id' => $policy_id,
                'plan_id' => $policy->plan_id,
                'deductible_id' => $policy->deductible_id,
                'coverage_id' => $policy->coverage_id,
                'commision' => $agents_data_final[$key]['commission_real'],
                'amount' => $agents_data_final[$key]['amount_commission'],
                'effective_date' => $effective_date,
                'user_id_creation' => $user->id
                
            ]);
    }

        CompanyCommission::create([
            'company_id' => $company_commission_data['company_id'],
            'agent_id' => $agents_data_final[0]['agent_id'],
            'policy_id' => $policy_id,
            'plan_id' => $policy->plan_id,
            'deductible_id' => $policy->deductible_id,
            'coverage_id' => $policy->coverage_id,
            'commission' => $company_commission_data['commission_real'],
            'amount' => $company_commission_data['amount_commission'],
            'effective_date' => $effective_date,
            'user_id_creation' => $user->id
        ]);
     
    return true;    
    }
}
