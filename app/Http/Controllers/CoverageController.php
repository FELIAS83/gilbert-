<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Coverage;
use Auth;
use Input;
use DB;

class CoverageController extends Controller
{
    //
     //
      //
    public function getIndex()
    {
        $coverages =  DB::table('coverages')
                        ->where('status', '=', '1')
                        ->get();

        return response()->success(compact('coverages'));

    }

    /**
     * Get Coverage details referenced by plan_id.
     *
     * @param int plan ID
     *
     * @return JSON
     */
  public function getCoverage($id)
    {

     $coverage = Coverage::where('plan_id', '=', $id)
                    ->where('status', '=', '1')
                    ->get();
        
        return response()->success($coverage);   

    }
    /**
     * Get coverage details referenced by id.
     *
     * @param int deductible ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $coverage = Coverage::find($id);
        
        return response()->success($coverage);
    }  
     /**
     * Create new Deductible for a Plan.
     *
     * @return JSON
     */
    public function postCoverages()
    {
        $usercreate = Auth::user();
        
        $coverage = Coverage::create([
            'name' => Input::get('name'),
            'plan_id' => Input::get('plan_id'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('coverage');
    }

    /**
     * Update coverage detail data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $coverageForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name'
                
                
            )
        );

        $coverageId = intval($coverageForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $coverageData = [
            'name' => $coverageForm['data.name'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Coverage::where('id', '=', $coverageId)->update($coverageData);

        return response()->success('success');
    }

      /**
     * Delete coverage Data.
     *
     * @return JSON success message
     */
    public function deleteCoverage($id)
    {
        $coverageData = [
            'status' => 0,            
        ];
        $affectedRows = Coverage::where('id', '=', $id)->update($coverageData);
        return response()->success('success');
    }
}
