<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DocumentDiagnostic;

class DocumentDiagnosticController extends Controller
{
    public function getDocuments($id){
	    $documents = DocumentDiagnostic::where('diagnostic_id',$id)->where('status', '=', '1')->with('diagnostics')->get();

	    return response()->success($documents);
	}

}
