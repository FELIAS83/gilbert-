<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;


use App\User;
use App\MedicalInformation;
use Auth;
use Input;

class MedicalInformationController extends Controller
{
    //
    function getShow($id){

        $medicalinformations = MedicalInformation::where('policy_id', '=', $id)->get();
      
        return response()->success($medicalinformations);
    }

    function postMedical()
    {

    	//$answer = 0;
        $repairable_id = 0;
        $observation = '';
        $usercreate = Auth::user();
    	$policy_id = Input::get('policy_id');
        $quiz_id = Input::get('quiz_id');
    	$quiz_details_id = Input::get('quiz_details_id');
   		$answers = Input::get('answer');
    	$repairables_id = Input::get('repairable_id');
    	$observations = Input::get('observation');

    	for ($i = 0; $i < count($quiz_details_id) ; $i++ ){
            if (!array_key_exists($i, $repairables_id) || $repairables_id[$i] == null || $answers[$i] == '0') {
                $repairables_id[$i] = 0;
            }
            if (!array_key_exists($i, $observations) || $observations[$i] == null || $answers[$i] == '0') {
                $observations[$i] = '';
            }
            

            $medicalinformations = MedicalInformation::create([
            'policy_id' => $policy_id,
            'quiz_id' => $quiz_id[$i],
            'quiz_details_id' => $quiz_details_id[$i],
            'answer' => $answers[$i],
            'repairable_id' => $repairables_id[$i],
            'observation' => $observations[$i],
            'user_id_creation' => $usercreate->id
        ]);
            
        }


    	 return response()->success($policy_id);
    }

    public function putShow(Request $request)
    {
       
                
        $data = $request->input('data');
       

        $userupdate = Auth::user();

       for ($i = 0; $i < count($data) ; $i++ ){
            $id = $data[$i]['id'];
            //if($data[$i]['answer'] == true){
                //$answer = 1;
            //}else{
                //$answer = 0;
            //}

            $putMedicalInfo = ([
            'answer' => $data[$i]['answer'],
            'repairable_id' => $data[$i]['repairable_id'],
            'observation' => $data[$i]['observation'],
            'user_id_update' => $userupdate->id,
        ]);
            $affectedRows = MedicalInformation::where('id', '=', $id)    
                                            ->update($putMedicalInfo); 
                                                
        }
       
        

        return response()->success($data[0]['policy_id']);
    }
}
