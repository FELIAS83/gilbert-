<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\DeductibleDetail;
use Auth;
use Input;
use DB;

use App\Coverage;
use App\Configuration;
use App\Fee;

class DeductibleDetailController extends Controller
{
    //
    //
    /**
     * Get all active Agent.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        $deductible_detail = DeductibleDetail::where('status', '1')->get();
        return response()->success(compact('deductible_detail'));
    }

    /**
     * Get Deductible details referenced by plan_id.
     *
     * @param int plan ID
     *
     * @return JSON
     */
  public function getDetails($id)
    {

     $deductible_detail = DeductibleDetail::where('deductible_id', '=', $id)
                    ->where('status', '=', '1')
                    ->where('is_child', '=' , 0)
                    ->get();
        
        return response()->success($deductible_detail);   

    }

     public function getDetail($id)
    {

     $deductible_detail = DeductibleDetail::where('deductible_id', '=', $id)
                    ->where('status', '=', '1')
                    ->where('is_child', '=' , 1)
                    ->get();
        
        return response()->success($deductible_detail);   

    }

   /**
     * Get deductoble details referenced by id.
     *
     * @param int deductible ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $deductible_detail = DeductibleDetail::find($id);
        
        return response()->success($deductible_detail);
    }  

    /**
     * Create new Deductible for a Plan.
     *
     * @return JSON
     */
    public function postDetails()
    {
        $usercreate = Auth::user();
        
        $deductible_detail = DeductibleDetail::create([
            'age_start' => Input::get('age_start'),
            'age_end' => Input::get('age_end'),
            'is_child' => Input::get('is_child'),
            'child_quantity' => Input::get('child_quantity'),
            'amount' => Input::get('amount'),
            'deductible_id' => Input::get('deductible_id'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('deductible_detail');
    }
      /**
     * Update deductible detail data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $detailForm = array_dot(
            app('request')->only(
                'data.id',
                'data.age_start',
                'data.age_end',
                'data.is_child',
                'data.child_quantity',
                'data.amount'
            )
        );

        $deductibleDetailId = intval($detailForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $detailData = [
            'age_start' => $detailForm['data.age_start'],
            'age_end' => $detailForm['data.age_end'],
            'is_child' => $detailForm['data.is_child'],
            'child_quantity' => $detailForm['data.child_quantity'],
            'amount' => $detailForm['data.amount'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = DeductibleDetail::where('id', '=', $deductibleDetailId)->update($detailData);

        return response()->success('success');
    }

      /**
     * Delete deductible detail Data.
     *
     * @return JSON success message
     */
    public function deleteDetail($id)
    {
        $deductibleData = [
            'status' => 0,            
        ];
        $affectedRows = DeductibleDetail::where('id', '=', $id)->update($deductibleData);
        return response()->success('success');
    }

    public function getAmounts($deductible_id,$age,$ageh,$childs,$coverage_id,$policyId){
        if ($age < 18) {
            $age = 18;
        }
        $amount = DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($age." between age_start and age_end")->first();
        $amounth = DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($ageh." between age_start and age_end")->first();
        //return response()->success($amount);
        if (($childs > 0) && ($childs <= 3)){
            $amountchild = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',$childs)->first();
        }
        elseif ($childs > 3){
            $amountchild = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',3)->first();
        }
        else{
            $amountchild = 0;
        }

        $coverage = Coverage::with('detail')->where('id',$coverage_id)->first();
        $configurations = Configuration::all();

        $fees = Fee::where('policy_id',$policyId)->where('paid_fee',0)->where('status',1)->get();


        return response()->success([
            'amounttitular' => (float) $amount->amount,
            'amounth' => (isset($amounth->amount)) ? (float) $amounth->amount : 0 ,
            'amountchild' => (isset($amountchild->amount)) ? (float) $amountchild->amount: 0 ,
            'coverage_name' => (isset($coverage->name)) ? $coverage->name : '',
            'coverage_amount' => (isset($coverage->name)) ? (float) $coverage->detail->amount : 0,
            'taxssc' => (float) $configurations[0]->value,
            'adminfee' => (float) $configurations[1]->value ,
            'iva' => (float) $configurations[2]->value,
            'fees' => $fees
        ]);
    }

    public function getAmountsanual($deductible_id,$age,$ageh,$childs,$coverage_id,$policyId){
        if ($age < 18) {
            $age = 18;
        }
        $amount = DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($age." between age_start and age_end")->first();
        $amounth = DeductibleDetail::where('deductible_id',$deductible_id)->whereRaw($ageh." between age_start and age_end")->first();
        if (($childs > 0) && ($childs <= 3)){
            $amountchild = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',$childs)->first();
        }
        elseif ($childs > 3){
            $amountchild = DeductibleDetail::where('deductible_id',$deductible_id)->where('child_quantity',3)->first();
        }
        else{
            $amountchild = 0;
        }

        $coverage = Coverage::with('detail')->where('id',$coverage_id)->first();
        $configurations = Configuration::all();

        $fees = Fee::where('policy_id',$policyId)->where('paid_fee','=',0)->where('status',1)->get();



        return response()->success([
            'amounttitular' => (float) $amount->amount,
            'amounth' => (isset($amounth->amount)) ? (float) $amounth->amount : 0 ,
            'amountchild' => (isset($amountchild->amount)) ? (float) $amountchild->amount: 0 ,
            'coverage_name' => (isset($coverage->name)) ? $coverage->name : '',
            'coverage_amount' => (isset($coverage->name)) ? (float) $coverage->detail->amount : 0,
            'taxssc' => (float) $configurations[0]->value,
            'adminfee' => (float) $configurations[1]->value ,
            'iva' => (float) $configurations[2]->value,
            'fees' => $fees
        ]);
    }
}
