<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Doctor;
use App\DoctorMedicalSpeciality;
use App\MedicalSpeciality;
use Auth;
use Input;
use DB;


class DoctorController extends Controller
{
    public function getIndex()
    {
     $doctors = Doctor::with('specialities')->where('status', '1')->get();//consulta tabla doctors con specialities

        for ($i = 0; $i < count($doctors) ; $i++ ){//recorro los doctores
            for ($j = 0; $j < count($doctors[$i]->specialities) ; $j++ ){//recorro las especialidades de cada doctor
                $medical_speciality = MedicalSpeciality::find($doctors[$i]->specialities[$j]->speciality_id);//consulto cual es el nombre de cada especialidad
                $doctors[$i]->specialitiesnames.=$medical_speciality->name.',';//creo un elemento del array con los nombres concatenados
            }


            if ($doctors[$i]->specialitiesnames==false){
                $doctors[$i]->specialitiesnames=''    ;
            }
            else{
            $doctors[$i]->specialitiesnames = substr($doctors[$i]->specialitiesnames, 0, -1);//elimino la ultima coma
            }
        }

        return response()->success(compact('doctors'));
    }


    public function postDoctors()
    {
        $usercreate = Auth::user();

        $doctor = Doctor::create([
            'names' => Input::get('names'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city' => Input::get('city_id'),
            'address'=> Input::get('address'),
            'surnames' => Input::get('surnames'),
            'phone' =>Input::get('phone'),
            'identification' => Input::get('identification'),
            'user_id_creation' => $usercreate->id
        ]);

        foreach (Input::get('specialties') as $speciality) {
            DoctorMedicalSpeciality::create(['doctor_id'=>$doctor->id,'speciality_id'=>$speciality]);
        }


        return response()->success('doctor');
    }

    public function getShow($id)
    {

        $data = Doctor::with('specialities')->find($id);//hago una consulta relacionada para traer los datos del doctor y las especialidades que tiene en la tabla
        $data->id = (string)$data->id;

        foreach ($data->specialities as $key => $value) {
            $activespecialities[] = $value->speciality_id;
            $value->name = MedicalSpeciality::where('id',$value->speciality_id)->first()->name;
        }

        if (isset($activespecialities))
        {
            $data->activespecialities = $activespecialities;
        }
        else
        {
            $data->activespecialities = [];
        }
        return response()->success($data);


    }

    public function putShow(Request $request)
    {
        $userForm = array_dot(
            app('request')->all()
        );

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.names' => 'required',
            'data.surnames' => 'required',
            'data.identification' => 'required',
            'data.phone' => 'required'
        ]);

        $data = [
            'names' => $userForm['data.names'],
            'surnames' => $userForm['data.surnames'],
            'identification' => $userForm['data.identification'],
            'country_id' => $userForm['data.country_id'],
            'province_id'=> $userForm['data.province_id'],
            'city'=> $userForm['data.city'],
            'address'=> $userForm['data.address'],
            'phone' => $userForm['data.phone'],
        ];

        $doctor = Doctor::where('id',$userForm['data.id'])->update($data);

        DoctorMedicalSpeciality::where('doctor_id',$userForm['data.id'])->delete();

        foreach (Input::get('data.activespecialities') as $speciality) {
            DoctorMedicalSpeciality::create(['doctor_id'=>$userForm['data.id'],'speciality_id'=>$speciality]);
        }

        return response()->success('success');
    }


    public function deleteDoctors($id)
    {
        $doctorData = [
            'status' => 0,
        ];
        $affectedRows = Doctor::where('id', '=', $id)->update($doctorData);
        return response()->success('success');
    }

    public function getDoctorbyspec($id)//filtrado de doctores por especialidad
    {
        $data = DB::table('doctor_medical_specialities')->where('speciality_id','=',$id)
                    ->select('doctor_medical_specialities.doctor_id','doctors.names','doctors.surnames')
                    ->join('doctors','doctor_medical_specialities.doctor_id','=','doctors.id')
                    ->get();       

        return response()->success(compact('data'));           

    }
}
