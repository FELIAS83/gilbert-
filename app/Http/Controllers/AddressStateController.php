<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddressStateController extends Controller
{
    //

    public function getIndex($country_id)
    {
    	$states = AddressState::find($country_id);
    	return response()->success($states);

    }
}
