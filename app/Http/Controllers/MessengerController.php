<?php

namespace App\Http\Controllers;

use App\User;
use App\Messenger;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessengerController extends Controller
{
   
     /**
     * Get all active Agent.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $messengers = DB::table('messengers')->where('status', '1')->get();
        return response()->success(compact('messengers'));
    }

     /**
     * Create new messenger.
     *
     * @return JSON
     */
    public function postMessengers()
    {
        $usercreate = Auth::user();
        
        $messengers = Messenger::create([
            'name' => Input::get('name'),
            'type' => Input::get('type'),
            'identity_document' => Input::get('identity_document'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('messengers');
    }

    /**
     * Get messenger details referenced by id.
     *
     * @param int messenger ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $messengers = Messenger::find($id);
        
        return response()->success($messengers);
    }

    /**
     * Update messenger data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $messengerForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.type',
                'data.identity_document'
            )
        );

        $messengerId = intval($messengerForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3'
        ]);

        $userupdate = Auth::user();

        $messengerData = [
            'name' => $messengerForm['data.name'],
            'type' => $messengerForm['data.type'],
            'identity_document' => $messengerForm['data.identity_document'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Messenger::where('id', '=', $messengerId)->update($messengerData);

        return response()->success('success');
    }


     /**
     * Delete active Messenger.
     *
     * @return JSON
     */

     public function deleteMessenger($id)
    {
        $messengerData = [
            'status' => 0,            
        ];
        $affectedRows = DB::table('messengers')->where('id', '=', $id)->update($messengerData);
        return response()->success('success');
    }
}
