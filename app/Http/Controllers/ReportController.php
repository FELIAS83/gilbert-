<?php

namespace App\Http\Controllers;

use App\User;
use App\Policy;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;
use App\AgentCommision;
use App\Fee;
use App\Claim;
use App\ClaimDocument;
use App\Diagnostic;
use App\DocumentDiagnostic;
use App\Dependent;
use App\Eob;
use App\EobDocuments;

//use Illuminate\Support\Facades\Input;



class ReportController extends Controller
{
    //
     /**
     * 
     *
     * @param 
     *
     * @return JSON
     */
    public function getReport()
    {

        
        $agent_id = Input::get('agent_id');
        $plan_id = Input::get('plan_id');
        /*$start_date = Input::get('start_date');
        $start_date = substr($start_date,0, 4)."-".substr($start_date,5, 2)."-".substr($start_date, 8, 2);
        $end_date = Input::get('end_date');
        $end_date = substr($end_date,0, 4)."-".substr($end_date,5, 2)."-".substr($end_date, 8, 2);
        */
      
            $reports = DB::table('agent_commisions')->where(function($query) {

                            if(Input::has('agent_id')){
                                    $query->where('agent_commisions.agent_id','=', Input::get('agent_id'))
                                          ->orderBy(Input::get('agent_id'));            
                                                      }

                            if(Input::has('plan_id')){
                                    $query->where('agent_commisions.plan_id','=', Input::get('plan_id'))
                                          ->orderBy(Input::get('plan_id'));            
                                                    }

                             if(Input::has('start_date') && Input::has('end_date')){
                                $start_date = Input::get('start_date');
                                $start_date = substr($start_date,0, 4)."-".substr($start_date,5, 2)."-".substr($start_date, 8, 2);
                                $end_date = Input::get('end_date');
                                $end_date = substr($end_date,0, 4)."-".substr($end_date,5, 2)."-".substr($end_date, 8, 2);
                                    $query->whereBetween('policies.start_date_coverage',[$start_date, $end_date])
                                          ->orderBy('policies.start_date_coverage');            
                                                    } 




                        })
                    
                   //->whereBetween('policies.start_date_coverage', [$start_date, $end_date])
                    ->select(["agent_commisions.*", "agents.id", DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), "policies.number", DB::raw('CONCAT(policies.first_name, " ", policies.last_name) AS applicant'), "policies.start_date_coverage", "plans.name"])
                    ->join('policies', 'policies.id', '=', 'agent_commisions.policy_id')
                    ->join('agents', 'agents.id', '=', 'agent_commisions.agent_id')
                    ->join('plans', 'plans.id', '=', 'agent_commisions.plan_id')
                    ->orderBy('agent_commisions.agent_id')           
                    ->orderBy('agent_commisions.plan_id')             
                    ->orderBy('policies.start_date_coverage')             
                    //->toSQL();
                    ->get();
        return response()->success($reports);
        
       
    }

    function getFeesreport(){

        //$policy_id = Input::get('_id');

        $reports =  Fee::where('fees.status',1)
                     ->where('fees.paid_fee',1)
                     ->where('payments.status', '=', '1')
                     ->join('policies', 'policies.id','=','fees.policy_id')
                     ->join('payments', 'fees.policy_id', '=', 'payments.policy_id')
                     ->join('plans', 'policies.plan_id','=','plans.id')
                     ->join('deductibles', 'policies.deductible_id','=','deductibles.id')
                     ->join('agents', 'agents.id','=','policies.agent_id')
                     ->select([DB::raw('policies.number as number'), DB::raw('CONCAT(policies.first_name, " ", policies.last_name) AS name'), DB::raw("date_format(fees.fee_payment_date, '%d/%m/%Y') as fee_payment_date"), "fees.*", DB::raw("plans.name AS plan"), DB::raw("deductibles.name AS deductible"),DB::raw("concat(deductibles.name, ' | $ ', deductibles.amount_out_usa,' / ',deductibles.amount_in_usa) AS deductible"), "payments.mode", DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent')]) 
                     ->get();

        foreach ($reports as $key => $value) {
           
            if($value->mode == "0" ){
                $value->mode = "Anual";
            }
            elseif($value->mode == "1" ){
                $value->mode = "Semestral"; 
            }
            elseif($value->mode == "2" ){
                $value->mode = "Trimestral"; 
            }else{
              $value->mode = "Mensual"; 
            }
           
            $value->total = "$ ".$value->total;
        }
        

        return response()->success($reports);             

    }

     public function getPolicies()
    {
        
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', '1')
                    ->select([DB::raw('policies.id as policy_id'), "policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(policies.created_at, '%d/%m/%Y') as created"), DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), "policysteps.*"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('agents', 'policies.agent_id', '=', 'agents.id')
                    ->join('policysteps', 'policies.id', '=', 'policysteps.id_policy')
                    ->get();

        return response()->success(compact('policies'));
    }


    public function getClaimhistorical(){
        $claimhistorical =  DB::table('claims')->where('claims.status','=','1')->where('claims.is_closed','=','1')
                        ->join('policies','claims.policy_id','=','policies.id')
                        ->leftjoin('diagnostics','diagnostics.claim_id','=','claims.id')
                        ->select(['claims.id as id','claims.is_closed','claims.step','claims.created_at','diagnostics.description','diagnostics.person_type','diagnostics.id_person',DB::raw('CONCAT(policies.first_name, " ", policies.last_name) AS titular'), 'policies.number as number'])
                        ->get();

         foreach ($claimhistorical as $value) {
            if($value->step == '2.5'){
              $value->resolution = "Devuelto";
            }
            else if($value->step == '3'){
              $value->resolution = "Cerrado";
            }
            else if($value->step == '4'){
              $value->resolution = "Liquidado";
            }
            else if($value->step == '5'){
              $value->resolution = "Liquidado con Devolución";
            }
            else{
              $value->resolution = "Concluido";
            }

            if($value->description == ''){
              $value->description = "Sin Diagnósticos asociados";
            }

           if($value->person_type == "D"){
                $dependent = Dependent::where('id',$value->id_person)->first();
                $value->name = $dependent->first_name.' '.$dependent->last_name;
                //$value->person_type = "Dependiente";
            }else{
                $value->name = $value->titular;
                //$value->person_type = "Titular";
            }

            $eob_data = Eob::where('id_claim','=',$value->id)
                            ->leftjoin('eob_documents', 'eobs.id','=','eob_documents.eob_id')
                            ->select(['eobs.id','eob_documents.directory','eob_documents.filename'])
                            ->get();
             $value->eob = $eob_data;

           }      


        return response()->success(compact('claimhistorical'));                  
    }   



    
}
