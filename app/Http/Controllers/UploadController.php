<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ticket;
use App\TicketDocuments;
use App\Configuration;
use App\Policy;
use DB;

class UploadController extends Controller
{
    /**
     * Store files for policies
     *
     * 
     */
    public function storeFilesNewPolicy(Request $request)
    {
        $file = $request->file('file');
        $policy_id = (isset($request['policy_id'])) ?  $request['policy_id'] : DB::table('policies')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/policies/';
        if (isset($directory[0])){//si hay guardado un directorio en la BD
            $file->move(public_path().'/policies/'.$policy_id, $name);//carpeta de servidor

           /* if (!file_exists($directory[0]->policy_file_directory.'/policies/'.$policy_id)) {
                mkdir($directory[0]->policy_file_directory.'/policies/'.$policy_id, 0777, true);
            }
            \File::copy(public_path().'/policies/'.$policy_id.'/'.$name, $directory[0]->policy_file_directory.'/policies/'.$policy_id.'/'.$name);*/

            //$file->move($directory[0]->policy_file_directory.$folder.$policy_id);

            return response()->success($directory[0]->policy_file_directory.$folder.$policy_id);
        }
        else{
            $file->move(public_path().$folder.$policy_id, $name);
            return response()->success('no hay ruta');
        }
    }

    /**
     * Store files for claims
     *
     * 
     */
    public function storeFilesNewClaim(Request $request)
    {
        $file = $request->file('file');
        $claim_id = (isset($request['claim_id'])) ?  $request['claim_id'] : DB::table('claims')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/claims/';
        if (isset($directory[0])){
            $file->move(public_path().$folder.$claim_id, $name);//carpeta de servidor

            /*if (!file_exists($directory[0]->claim_file_directory.$folder.$claim_id)) {
                mkdir($directory[0]->claim_file_directory.$folder.$claim_id, 0777, true);
            }
            \File::copy(public_path().$folder.$claim_id.'/'.$name, $directory[0]->claim_file_directory.$folder.$claim_id.'/'.$name);//carpeta local*/

            //$file->move($directory[0]->claim_file_directory.$folder.$claim_id, $name);


            return response()->success($directory[0]->claim_file_directory.$folder.$claim_id);
        }
        else{
            $file->move(public_path().$folder.$claim_id, $name);
            return response()->success('no hay ruta');
        }


    }

    public function storeFilesNewTicket(Request $request)
    {
        $file = $request->file('file');
        $ticket = (isset($request['ticket_id'])) ?  $request['ticket_id'] : DB::table('tickets')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/tickets/';
        if (isset($directory[0])){
            $file->move(public_path().$folder.$ticket, $name);//carpeta de servidor

            return response()->success($directory[0]->ticket_file_directory.$folder.$ticket);
        }
        else{
            $file->move(public_path().$folder.$ticket, $name);
            return response()->success('no hay ruta');
        }


    }

    public function storeFilesTicketLetter(Request $request)
    {
        $file = $request->file('file');
        $ticket = (isset($request['ticket_id'])) ?  $request['ticket_id'] : DB::table('tickets')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/tickets/';
            
        if (isset($directory[0])){
            $file->move(public_path().$folder.$ticket, $name);//carpeta de servidor

            return response()->success($directory[0]->ticket_file_directory.$folder.$ticket);
        }
        else{
            $file->move(public_path().$folder.$ticket, $name);
            return response()->success('no hay ruta');
        }


    }

    public function storeFilesPayment(Request $request)
    {
        $file = $request->file('file');
        $policy_id =  $request['policy_id'];
        $name = $file->getClientOriginalName();

        $feeFolder ='/'. $request['feeFolder'];
        $folder = '/payment_record/';

        $file->move(public_path().$folder.$policy_id.$feeFolder, $name);//carpeta de servidor
        return response()->success(public_path().$folder.$policy_id);
    }

    public function storeFilesPaymentRenewal(Request $request)
    {
        $file = $request->file('file');
        $policy_id =  $request['policy_id'];
        $name = $file->getClientOriginalName();
        $folder = '/payment_record/';

        $file->move(public_path().$folder.$policy_id, $name);//carpeta de servidor
        return response()->success(public_path().$folder.$policy_id);
    }

    /***************** Renewals Files with FeeId number in name file ****/
     public function storeRenewalDocs(Request $request)
    {
        $file = $request->file('file');
        $policy_id =  $request['policy_id'];
        $name2 = $request['filename'];
        $name = $file->getClientOriginalName();
        $folder = '/'.$request['folder'];

        $file->move(public_path().$folder.$policy_id, $name2);//carpeta de servidor
        return response()->success(public_path().$folder.$policy_id);
    }


    /**
     * Store files for renewals
     *
     * 
     */
    public function storeFilesRenewal(Request $request)
    {
        $file = $request->file('file');
        $policy_id = (isset($request['policy_id'])) ?  $request['policy_id'] : DB::table('policies')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/renewals/';
        if (isset($directory[0])){//si hay guardado un directorio en la BD
            $file->move(public_path().$folder.$policy_id, $name);//carpeta de servidor

           /* if (!file_exists($directory[0]->policy_file_directory.'/policies/'.$policy_id)) {
                mkdir($directory[0]->policy_file_directory.'/policies/'.$policy_id, 0777, true);
            }
            \File::copy(public_path().'/policies/'.$policy_id.'/'.$name, $directory[0]->policy_file_directory.'/policies/'.$policy_id.'/'.$name);*/

            //$file->move($directory[0]->policy_file_directory.$folder.$policy_id);

            return response()->success($directory[0]->renewal_file_directory.$folder.$policy_id);
        }
        else{
            $file->move(public_path().$folder.$policy_id, $name);
            return response()->success('no hay ruta');
        }
    }

    /**
     * Store files for send of EOB
     *
     * 
     */
    public function storeFilesEOB(Request $request)
    {
        $file = $request->file('file');
        $diagnostic_id = $request['diagnostic_id'];
        //$eobId = DB::table('eobs')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/eob/';
        if (isset($directory[0])){//si hay guardado un directorio en la BD
            $file->move(public_path().$folder.$diagnostic_id, $name);//carpeta de servidor

            return response()->success($directory[0]->eob_file_directory.$folder.$diagnostic_id);
        }
        else{
            $file->move(public_path().$folder.$diagnostic_id, $name);
            return response()->success('no hay ruta');
        }
    }

    public function storeFilesRenewalNotifications(Request $request)////documentos de Aviso de Renovacion
    {
        $file = $request->file('file');
        $policy_id = (isset($request['policy_id'])) ?  $request['policy_id'] : DB::table('policies')->max('id');
        $name = $file->getClientOriginalName();
        $directory = Configuration::where('status', '1')->get();
        $folder = '/renewal_notifications/';
        if (isset($directory[0])){//si hay guardado un directorio en la BD
            $file->move(public_path().$folder.$policy_id, $name);//carpeta de servidor

           /* if (!file_exists($directory[0]->policy_file_directory.'/policies/'.$policy_id)) {
                mkdir($directory[0]->policy_file_directory.'/policies/'.$policy_id, 0777, true);
            }
            \File::copy(public_path().'/policies/'.$policy_id.'/'.$name, $directory[0]->policy_file_directory.'/policies/'.$policy_id.'/'.$name);*/

            //$file->move($directory[0]->policy_file_directory.$folder.$policy_id);

            return response()->success($directory[0]->renewal_file_directory.$folder.$policy_id);
        }
        else{
            $file->move(public_path().$folder.$policy_id, $name);
            return response()->success('no hay ruta');
        }
    }



}
