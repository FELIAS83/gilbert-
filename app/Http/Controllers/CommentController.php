<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Comment;

use Auth;

class CommentController extends Controller
{
    public function getShow($id){
        $comments = Comment::where('status', 1)->where('module', 0)->where('key_field', $id)->with('userInfo')->get();
        foreach($comments as $comment){
            $comment->date = date_format($comment->created_at, 'd/m/Y h:i:s a');
        }
        return response()->success($comments);
    }

    public function deleteComments($id){
        $userdelete = Auth::user();
        $affectedRows = Comment::where('id', '=', $id)->update([
            'status' => 0,
            'user_id_update' => $userdelete->id
        ]);
        return response()->success('success');
    }
}
