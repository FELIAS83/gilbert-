<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddressProvince;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use DB;
class AddressProvinceController extends Controller
{
  
    public function getIndex()
    {        
        $states = AddressProvince::where('status', '1')
                    ->orderby('name')
                    ->get();
        return response()->success(compact('states'));

    }
    
    public function getProvince($country_id)
    {
      
        $states = DB::table('address_provinces')->where('country_id','=',$country_id)->where('status', '1')
                   // ->orderby('name')
                    ->get();
        return response()->success(compact('states'));

    }

}
