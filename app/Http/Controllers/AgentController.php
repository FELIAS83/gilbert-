<?php

namespace App\Http\Controllers;

use App\User;
use App\Agent;
use Auth;
use Input;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;


class AgentController extends Controller
{
    /**
     * Get all active Agent.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        $agents = Agent::where('status', '1')->get();

      
        return response()->success(compact('agents'));
    }


    /**
     * Post  Agent.
     *
     * @return JSON
     */

     public function postAgents()
    {
        $usercreate = Auth::user();
        
        $agents = Agent::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'birthday' => date("Y-m-d h:i:s", strtotime(Input::get('birthday'))),
            'email' => Input::get('email'),
            'skype' => Input::get('skype'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city_id' => Input::get('city_id'),
            'address' => Input::get('address'),
            'commission' => Input::get('commission'),
            'leader' => Input::get('leader'), 
            'user_id_creation' => $usercreate->id
        ]);
        return response()->success('agents');
    }

    /**
     * Get Agents details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $agents = Agent::find($id);
        
        return response()->success($agents);
    }


     /**
     * Update company data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $agentForm = array_dot(
            app('request')->only(
                'data.id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.birthday',
                'data.email',
                'data.skype',
                'data.mobile',
                'data.phone',
                'data.country_id',
                'data.province_id',
                'data.city_id',
                'data.address',
                'data.commission',
                'data.leader'
            )
        );


        $agentId = intval($agentForm['data.id']);
      
        $userupdate = Auth::user();

        $agentData = [
            'first_name' => $agentForm['data.first_name'],
            'last_name' => $agentForm['data.last_name'],
            'identity_document' => $agentForm['data.identity_document'],
            'birthday' => $agentForm['data.birthday'],
            'email' => $agentForm['data.email'],
            'skype' => $agentForm['data.skype'],
            'mobile' => $agentForm['data.mobile'],
            'phone' => $agentForm['data.phone'],
            'country_id' => $agentForm['data.country_id'],
            'province_id' => $agentForm['data.province_id'],
            'city_id' => $agentForm['data.city_id'],
            'address' => $agentForm['data.address'],
            'commission' => $agentForm['data.commission'],
            'leader' => $agentForm['data.leader'],
            'user_id_update' => $userupdate->id,
        ];
        $affectedRows = Agent::where('id', '=',  $agentId)->update($agentData);

        return response()->success('success');
    }


    /**
     * Delete  Agent.
     *
     * @return JSON
     */

     public function deleteAgent($id)
    {
        $agentData = [
            'status' => 0,            
        ];
        $affectedRows = Agent::where('id', '=', $id)->update($agentData);
        return response()->success('success');
    }



   
}
