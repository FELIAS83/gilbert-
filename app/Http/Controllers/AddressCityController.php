<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddressCity;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use DB;

class AddressCityController extends Controller
{
    //get City by province ID

    public function getIndex()    
    {
        
        $cities = AddressCity::where('status', '1')
                    ->orderby('name')
                    ->get();
        return response()->success(compact('cities'));
        
    }

    public function getCity($id_province)
    {

        $cities= DB::table('address_citys')->where('province_id','=',$id_province)->where('status','=','1')
                 ->get();
        return response()->success(compact('cities'));
    }
}
