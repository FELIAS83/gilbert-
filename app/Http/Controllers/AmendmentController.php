<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Amendment;
use App\Applicant;
use App\Dependent;

class AmendmentController extends Controller
{
    public function getAmendments($policy_id)
    {
        $data = Amendment::where('policy_id',$policy_id)->where('status',1)->get();
        foreach ($data as $value){
            $value->affiliate_id = (int) $value->affiliate_id;

            if ($value->role_order == 1){
                $applicant = Applicant::where('id',$value->affiliate_id)->first();
                $value->birthdate = $applicant->birthday;
            }
            else{
                $dependent = Dependent::where('id',$value->affiliate_id)->first();
                $value->birthdate = $dependent->birthday;
            }
        }
        return response()->success($data);
    }
}
