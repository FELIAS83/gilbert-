<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use Input;
use App\User;
use Illuminate\Http\Request;
use App\ClaimDocumentTypeDescriptions;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClaimDocumentTypeDescriptionsController extends Controller
{
     /**
     * Get all claimDocTypesDescirption.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $claimdocumenttypedescriptions = ClaimDocumentTypeDescriptions::all();
        $claimdocumenttypedescriptions = DB::table('claim_document_type_descriptions')->where('status', '=', '1')
                    ->select(["claim_document_type_descriptions.*"])
                    ->get();
       
        return response()->success(compact('claimdocumenttypedescriptions'));
    }

    /**
     * Create new claimDocTypesDescirption.
     *
     * @return JSON
    */ 
    public function postClaimdocumenttypedescriptions()
    {
        $usercreate = Auth::user();
        
        $claimdocumenttypedescriptions = ClaimDocumentTypeDescriptions::create([
            'id_doc_type'=>Input::get('id_doc_type'),
            'description' => Input::get('description'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('claimdocumenttypedescriptions');
    }

    /**
     * Get claimDocTypesDescirption details referenced by id.
     *
     * @param int claimDocTypesDescirption ID
     *
     * @return JSON
    */ 
    public function getShow($id)
    {
        $claimdocumenttypedescriptions = ClaimDocumentTypeDescriptions::find($id);
        
        return response()->success($claimdocumenttypedescriptions);
    }


        public function getClaimdocumenttypedescriptions($id)
    {

     $claimdocumenttypedescriptions= Claimdocumenttypedescriptions::where('id_doc_type', '=', $id)
                    ->where('status', '=', '1')
                    ->get();
        
        return response()->success($claimdocumenttypedescriptions);   
    }

    /**
     * Update claimDocTypesDescirption data.
     *
     * @return JSON success message
    */ 
    public function putShow(Request $request)
    {
        $claimDocTypeDescForm = array_dot(
            app('request')->only(
                'data.id',
                'data.description',
                'data.id_doc_type',
                'data.status'
            )
        );

        $claimDocTypeDescId = intval($claimDocTypeDescForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.id_doc_type'=>'required|integer', 
            'data.description' => 'required|min:3',
        ]);

        $userupdate = Auth::user();

        $claimDocTypeDescData = [
            'description' => $claimDocTypeDescForm['data.description'],
            'id_doc_type' => $claimDocTypeDescForm['data.id_doc_type'],
            'status' => $claimDocTypeDescForm['data.status'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = ClaimDocumentTypeDescriptions::where('id', '=', $claimDocTypeDescId)->update($claimDocTypeDescData);

        return response()->success('success');
    }

    /**
     * Delete claimDocTypesDescirption Data.
     *
     * @return JSON success message
     */
    public function deleteClaimdocumenttypedescriptions($id)
    {
        $claimdoctypeDescData = [
            'status' => 0,            
        ];
        $affectedRows = ClaimDocumentTypeDescriptions::where('id', '=', $id)->update($claimdoctypeDescData);
        return response()->success('success');
    }
}
