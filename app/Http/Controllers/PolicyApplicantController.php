<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PolicyApplicant;


class PolicyApplicantController extends Controller
{
    //
     /**
     * Get all active Policy Applicant.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        $policyApplicant = PolicyApplicant::where('status', '1')->get();
        return response()->success(compact('policyApplicant'));
    }
    //
     /**
     * Get all active Agent.
     *
     * @return JSON
     */
    
        
        public function getShow($id)
        {
      
        $policyApplicant = PolicyApplicant::where('policy_id_old', $id)->get();
        
        return response()->success($policyApplicant);
      
        }
    
}
