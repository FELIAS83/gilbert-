<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\CoverageDetail;
use Auth;
use Input;
use DB;

class CoverageDetailController extends Controller
{
    //
    ////
      //
    public function getIndex()
    {
        $coverage_details =  DB::table('coverage_details')
                        ->where('status', '=', '1')
                        ->get();

        return response()->success(compact('coverage_details'));

    }

     /**
     * Get Coverage details referenced by plan_id.
     *
     * @param int plan ID
     *
     * @return JSON
     */
  public function getDetails($id)
    {

     $coverage_detail = CoverageDetail::where('coverage_id', '=', $id)
                    ->where('status', '=', '1')
                    ->get();
        
        return response()->success($coverage_detail);   

    }
     /**
     * Get coverage details referenced by id.
     *
     * @param int coverage detail ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $coverage_detail = CoverageDetail::find($id);
        
        return response()->success($coverage_detail);
    }  
    /**
     * Get Coverage details referenced by deductible.
     *
     * @param int plan ID
     *
     * @return JSON
     */
  public function getCoverage($id)
    {

    $coverage_detail = DB::table('coverage_details')->where('coverage_details.status', '=', '1')
                            ->where('coverage_details.deductible_id', '=', $id)
                            ->join('coverages', 'coverage_details.coverage_id', '=', 'coverages.id')
                            ->select('coverage_details.*', 'coverages.name')
                            ->get();
    return response()->success($coverage_detail);   

    }
    /**
     * Create new Coverage Details
     *
     * @return JSON
     */
    public function postDetails()
    {
        $usercreate = Auth::user();
        
        $coverage_detail = CoverageDetail::create([
            'amount' => Input::get('amount'),
            'coverage_id' => Input::get('coverageid'),
            'deductible_id' => Input::get('deductible_id'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('coverage_detail');
    }
     /**
     * Update coverage detail data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $coverageForm = array_dot(
            app('request')->only(
                'data.id',
                'data.amount'
            )
        );

        $coverageDetailId = intval($coverageForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $coverageData = [
            'amount' => $coverageForm['data.amount'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = CoverageDetail::where('id', '=', $coverageDetailId)->update($coverageData);

        return response()->success('success');
    }


     /**
     * Delete coverage detail Data.
     *
     * @return JSON success message
     */
    public function deleteCoverage($id)
    {
        $coverageData = [
            'status' => 0,            
        ];
        $affectedRows = CoverageDetail::where('id', '=', $id)->update($coverageData);
        return response()->success('success');
    }
}
