<?php


namespace App\Http\Controllers;
use App\TicketTypes; 
use Illuminate\Http\Request;
use DB;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TicketTypeController extends Controller
{
    public function getIndex(){

    	$tickettypes=DB::table('ticket_types')->where('status','=','1')
    					->get();

    return response()->success(compact('tickettypes'));
    }

}
