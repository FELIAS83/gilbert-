<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assistent;
use App\User;
use Auth;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class AssistentController extends Controller
{
    //
    /**
     * Get all active Assistent.
     *
     * @return JSON
     */
    public function getIndex($id)
    {
        
        $assistent = Assistent::where('status', '1')
                    ->get();  
        return response()->success(compact('assistent'));
    }


    /**
     * Post  Assistent.
     *
     * @return JSON
     */

     public function postAssistents()
    {
        $usercreate = Auth::user();
        
        $assistent = Assistent::create([
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'birthday' => date("Y-m-d h:i:s", strtotime(Input::get('birthday'))),
            'email' => Input::get('email'),
            'skype' => Input::get('skype'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'country_id' => Input::get('country_id'),
            'province_id' => Input::get('province_id'),
            'city_id' => Input::get('city_id'),
            'address' => Input::get('address'),
            'charge' => Input::get('charge'),
            'agent_id' => Input::get('agentId'),
            'user_id_creation' => $usercreate->id
        ]);
        return response()->success('assistent');
    }

    /**
     * Get Assistent details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $assistent = Assistent::find($id);
       
        return response()->success($assistent);
    }

    public function getAssistent($id)
    {

     $assistent = Assistent::where('agent_id', '=', $id)
                    ->where('status', '=', '1')
                    ->get();
        
        return response()->success($assistent);   
    }


     /**
     * Update Assistent data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $assistentForm = array_dot(
            app('request')->only(
                'data.id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.birthday',
                'data.email',
                'data.skype',
                'data.mobile',
                'data.phone',
                'data.country_id',
                'data.province_id',
                'data.city_id',
                'data.address',
                'data.charge'
            )
        );

        $assistentId = intval($assistentForm['data.id']);
        $userupdate = Auth::user();
        $assistentData = [
            'first_name' => $assistentForm['data.first_name'],
            'last_name' => $assistentForm['data.last_name'],
            'identity_document' => $assistentForm['data.identity_document'],
            'birthday' => $assistentForm['data.birthday'],
            'email' => $assistentForm['data.email'],
            'skype' => $assistentForm['data.skype'],
            'mobile' => $assistentForm['data.mobile'],
            'phone' => $assistentForm['data.phone'],
            'country_id' => $assistentForm['data.country_id'],
            'province_id' => $assistentForm['data.province_id'],
            'city_id' => $assistentForm['data.city_id'],
            'address' => $assistentForm['data.address'],
            'charge' => $assistentForm['data.charge'],
            'user_id_update' => $userupdate->id,
        ];
        $affectedRows = Assistent::where('id', '=',  $assistentId)->update($assistentData);

        return response()->success('success');
    }


    /**
     * Delete  Agent.
     *
     * @return JSON
     */

     public function deleteAssistent($id)
    {
        $assistentData = [
            'status' => 0,            
        ];
        $affectedRows = Assistent::where('id', '=', $id)->update($assistentData);
        return response()->success('success');
    }


}
