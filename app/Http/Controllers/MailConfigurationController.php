<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Input;
use DB; 

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MailConfiguration;


class MailConfigurationController extends Controller
{
    
    public function getShow($id)
    {
        $mail_configuration = MailConfiguration::find($id);
        $mail_configuration->protocol = (int)$mail_configuration->protocol;
        return response()->success($mail_configuration);
    }

    public function putShow(Request $request)
    {
        $configurationForm = array_dot(
            app('request')->all()
        );

        $configurationId = intval($configurationForm['data.id']);



        $this->validate($request, [
            'data.id' => 'required',
            'data.host' => 'required',
            'data.username' => 'required',
            'data.name' => 'required',
            'data.password' => 'required',
            'data.protocol' => 'required',
            'data.port' => 'required'
        ]);

        $configurationData = [
            'host' => $configurationForm['data.host'],
            'username' => $configurationForm['data.username'],
            'name' => $configurationForm['data.name'],
            'password' => $configurationForm['data.password'],
            'protocol' => $configurationForm['data.protocol'],
            'port' => $configurationForm['data.port']
        ];

        MailConfiguration::where('id', '=', $configurationId)->update($configurationData);

        return response()->success('success');
	}
}