<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Commission;
use App\Company;
use App\Plan;
use App\Branch;
use App\User;
use App\Agent;
class CommissionController extends Controller
{
    //
     /**
     * Get Agents details referenced by id.
     *
     * @param int agent ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $commission = Commission::where('commissions.agent_id', $id)->where('commissions.status', 1)
                                ->leftjoin('companies', 'commissions.company_id', '=', 'companies.id')
                                ->leftjoin('plans', 'commissions.plan_id', '=', 'plans.id')
                                ->leftjoin('branches', 'commissions.branch_id', '=', 'branches.id')
                                ->select(['commissions.*','companies.name as provider','plans.name as plan','branches.name as branch'])
                                ->get();

     foreach ($commission as $value) {
            /*if($value->provider == "" || $value->provider == null){
                $value->provider = "No seleccionado";
            }*/
            if($value->branch == null){
                $value->branch = "No seleccionado";
            }
            if($value->plan == null){
                $value->plan = "No seleccionado";
            }
        } 
        return response()->success($commission);
    }

    public function postCommission(){

         $usercreate = Auth::user();

         $branch_id = (Input::get('branch') == null) ? 0 : Input::get('branch');
         $plan_id = (Input::get('plans') == null) ? 0 : Input::get('plans');
         
         
         $commission = Commission::create([
            'agent_id' => Input::get('agentId'),
            'company_id' => Input::get('provider'),
            'branch_id' => $branch_id,
            'plan_id' => $plan_id,
            'commission' => Input::get('commissions'),
            'commission_renewal' => Input::get('commission_ren'),
            'user_id_creation' =>  $usercreate->id
         ]);

         return response()->success(Input::get('agentId'));

    }

     /**
     * Delete  Commision.
     *
     * @return JSON
     */

     public function deleteCommission($id)
    {
        $success = true;
        $commission = Commission::find($id);
        $sons = Agent::where('leader', '=', $commission->agent_id)->where('status',1)->count();
        if($sons == 0){

            $commissionData = [
            'status' => 0,            
            ];
            $affectedRows = Commission::where('id', '=', $id)->update($commissionData);
            return response()->success($success);
        }else{
            $success = false;
            return response()->success($success);
        }
    }

    /***
    *
    *Edit Commisions
    *
    **/

    public function getCommission($id){
         $commission = Commission::find($id);
        
        return response()->success($commission);
    }
    /***
    * Edit Commissions
    *
    **/

    public function putCommission(Request $request){
        $userupdate = Auth::user();
        $commissionForm = array_dot(
            app('request')->only(
                'data.id',
                'data.agent_id',
                'data.company_id',
                'data.branch_id',  
                'data.plan_id',
                'data.commission',
                'data.commission_renewal'       
            )
        );

        $commissionId = intval($commissionForm['data.id']);
        $agentId = intval($commissionForm['data.agent_id']);
        $company_id = $commissionForm['data.company_id'];
        $branch_id = ($commissionForm['data.branch_id'] == null) ? 0 : $commissionForm['data.branch_id'];
        $plan_id = ($commissionForm['data.plan_id'] == null) ? 0 : $commissionForm['data.plan_id'];
        //$provider_commission = ($commissionForm['data.commission_company'] == null) ? 0 : $commissionForm['data.commission_company'];
        //$branch_commission = ($commissionForm['data.commission_branch'] == null) ? 0 : $commissionForm['data.commission_branch'];
        $commission = ($commissionForm['data.commission'] == null) ? 0 : $commissionForm['data.commission'];
        $commission_ren = ($commissionForm['data.commission_renewal'] == null) ? 0 : $commissionForm['data.commission_renewal'];

        $commissionData = [
            'company_id' => $commissionForm['data.company_id'],
            'branch_id' => $branch_id,
            'plan_id' => $plan_id, 
            'commission' => $commission, 
            'commission_renewal' => $commission_ren, 
            'user_id_update' => $userupdate->id,

        ];

        $affectedRows = Commission::where('id',$commissionId)->update($commissionData);
        return response()->success($agentId);
      
       
    }

    public function verifyCommission($provider_id,$branch_id,$plan_id,$agentId,$commission,$commission_ren){
        $agent =  Agent::find($agentId);
        $response = [];

        if($agent->leader == 0 || $agent->leader == null){
            $affectedRows = Commission::where('company_id','=',$provider_id)->where('branch_id','=',$branch_id)->where('plan_id','=',$plan_id)->where('agent_id','=',$agentId)->where('status',1)->count();    
            
            if($affectedRows > 0)
            {
                $response = array('response' => true, 'message' => 'Ya existe un comisión igual para este Agente'); 
                return response()->success($response);      
            }else
            {   
                $response = array('response' => false, 'message' => ''); 
                return response()->success($response);      
            }

        }else{
            $agentLeaderId = $agent->leader;
            $leaderConf = Commission::where('company_id','=',$provider_id)->where('branch_id','=',$branch_id)->where('plan_id','=',$plan_id)->where('agent_id','=',$agentLeaderId)->where('status',1)->first();

            if($leaderConf == null){
                $response = array('response' => true, 'message' => 'El Lider de este Agente, no posee una comisión como esta.'); 
                return response()->success($response);       // El padre no posee la misma copnfiguracion de comision  
            }else{

                
                
                    if($commission >  $leaderConf->commission){
                        $response = array('response' => true, 'message' => 'El valor de la comisión para Emisiones NO puede ser mayor o igual a '.$leaderConf->commission.'% correspondiente a su Agente Líder.'); 
                        return response()->success($response);       // La comision del hijo es mayor o igual a la del padre 
                    }else if($commission_ren >  $leaderConf->commission_renewal){
                        $response = array('response' => true, 'message' => 'El valor de la comisión para Renovaciones NO puede ser mayor o igual a '.$leaderConf->commission_renewal.'% correspondiente a su Agente Líder.'); 
                        return response()->success($response);       // La comision del hijo es mayor o igual a la del padre 
                    }
                    else{
                        $response = array('response' => false, 'message' => ''); 
                        return response()->success($response);       // puede guardar
                    }
                    

            }

            

        }

        
        
        
        
    }
}
