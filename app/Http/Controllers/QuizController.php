<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Quiz;
use App\QuizDetail;
use Auth;
use Input;
use DB;

class QuizController extends Controller
{
    //
    public function getIndex()
	{
		$quiz =  DB::table('quiz')->where('status', '=', '1')->get();
		return response()->success(compact('quiz'));

	}

	public function getAnswer()
	{
		$quiz =  DB::table('quiz')->where('quiz.status', '=', '1')
		->select(["quiz.*", "quiz_details.name as answer"])
		->join('quiz_details', 'quiz.id', '=', 'quiz_details.quiz_id')
		->get();
		return response()->success($quiz);

	}

}
