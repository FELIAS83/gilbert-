<?php

namespace App\Http\Controllers;

use App\User;
use App\Applicant;
use App\Diagnostic;
use App\Ticket;
use App\Comment;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class ApplicantController extends Controller
{
    public function getIndex(){

        $applicants = Applicant::where('status', '1')
                        ->select(["applicants.id", "applicants.policy_id", "applicants.first_name", "applicants.last_name", "applicants.address", DB::raw('case applicants.gender when "M" then "Masculino" when "F" then "Femenino" end as gender'), DB::raw('case applicants.civil_status when "C" then "Casado(a)" when "S" then "Soltero(a)" when "D" then "Divorciado(a)" when "V" then "Viudo(a)" when "U" then "Unión Libre" end as civil_status'), "applicants.country_id", "applicants.province_id", "applicants.city_id", "applicants.email", "applicants.phone", "applicants.mobile", "applicants.place_of_birth", "applicants.birthday", "applicants.height", "applicants.height_unit_measurement", "applicants.document_type", "applicants.identity_document", "applicants.weight", "applicants.weight_unit_measurement", "applicants.occupation"])
                        ->get();
        return response()->success(compact('applicants'));

    }

    public function getActiveClients(){

        $applicants =  DB::table('applicants')->where('applicants.status', '=', '1')
                      ->select(["applicants.*"])
                      ->where('policies.status','=','1')
                      ->join('policies','applicants.policy_id','=','policies.id')
                      ->get();

        return response()->success($applicants);
    }

     public function getInactiveClients(){

        $applicants =  DB::table('applicants')->where('applicants.status', '=', '1')
                      ->select(["applicants.*"])
                      ->where('policies.status','=','0')
                      ->where('policies.motive','!=','0')
                      ->join('policies','applicants.policy_id','=','policies.id')
                      ->get();

        return response()->success($applicants);
    }

    /**
     * Get applicant details referenced by policy id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        //$company = Company::find($id);
        $applicant = Applicant::where ('policy_id', $id)->get();

        return response()->success($applicant);
    }

    /**
     * Update applicant data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $applicantForm = array_dot(
            app('request')->only(
                'data.id',
                'data.policy_id',
                'data.first_name',
                'data.last_name',
                'data.address',
                'data.gender',
                'data.civil_status',
                'data.country_id',
                'data.province_id',
                'data.city_id',
                'data.email',
                'data.phone',
                'data.mobile',
                'data.place_of_birth',
                'data.birthday',
                'data.height',
                'data.height_unit_measurement',
                'data.identity_document',
                'data.weight',
                'data.weight_unit_measurement',
                'data.occupation'
            )
        );

        $applicantId = intval($applicantForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.first_name' => 'required|min:3',
            'data.last_name' => 'required|min:3'
        ]);

        $userupdate = Auth::user();

        $applicantData = [
            'first_name' => $applicantForm['data.first_name'],
            'last_name' => $applicantForm['data.last_name'],
            'address' => $applicantForm['data.address'],
            'gender' => $applicantForm['data.gender'],
            'civil_status' => $applicantForm['data.civil_status'],
            'country_id' => $applicantForm['data.country_id'],
            'province_id' => $applicantForm['data.province_id'],
            'city_id' => $applicantForm['data.city_id'],
            'email' => $applicantForm['data.email'],
            'phone' => $applicantForm['data.phone'],
            'mobile' => $applicantForm['data.mobile'],
            'place_of_birth' => $applicantForm['data.place_of_birth'],
            'birthday' => $applicantForm['data.birthday'],
            'height' => $applicantForm['data.height'],
            'height_unit_measurement' => $applicantForm['data.height_unit_measurement'],
            'identity_document' => $applicantForm['data.identity_document'],
            'weight' => $applicantForm['data.weight'],
            'weight_unit_measurement' => $applicantForm['data.weight_unit_measurement'],
            'occupation' => $applicantForm['data.occupation'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Applicant::where('id', '=', $applicantId)->update($applicantData);

        return response()->success($applicantForm['data.policy_id']);
    }

    public function getApplicantData($id){
        $info = [];
        $applicant_data = Applicant::where('status', 1)->where('id', $id)->with('country')->with('province')->with('city')->with('dependents')->first();
        $applicant_data->country_id = (int) $applicant_data->country_id;
        $applicant_data->province_id = (int) $applicant_data->province_id;
        $applicant_data->height_unit_measurement = $applicant_data->height_unit_measurement == 0 ? 'M' : 'CM';
        $applicant_data->weight_unit_measurement = $applicant_data->weight_unit_measurement == 0 ? 'KG' : 'LB';

        foreach($applicant_data->dependents as $value){
            $value->relationship = $value->relationship == 'D' ? $value->relationship = 'Dependiente': 'Cónyuge';
            if ($value->document_type == 0)
                $value->document_type = 'Cédula';
            elseif ($value->document_type == 1)
                $value->document_type = 'Pasaporte';
            else
                $value->document_type = 'RUC';

        }
        $info['info'] = $applicant_data;
        $diagnostics= Diagnostic::where('person_type', 'A')->where('id_person', $id)->where('status', 1)->with('claimDocuments')->get();

        foreach($diagnostics as $diagnostic){
            $amount = 0;
            foreach($diagnostic->claimDocuments as $document){
                if ($document->id_doc_type == 1){
                    $amount =+ $document->amount;
                }
            }
            $info['claims'][]['id'] = $diagnostic->description;
            $info['claims'][count($info['claims'])-1]['amount'] = $amount;
            $info['claims'][count($info['claims'])-1]['description'] = $diagnostic->description;
        }

        $tickets = Ticket::where('status', 1)->where('person_type', 'A')->where('id_person', $id)->with('ticketType')->get();
        $info['tickets'] = $tickets;
         $comments = Comment::where('status', 1)->where('module', 0)->where('key_field', $id)->with('userInfo')->get();
        foreach($comments as $comment){
            $comment->date = date_format($comment->created_at, 'd/m/Y h:i:s a');
        }
        $info['comments'] = $comments;
        $info['current_user']= Auth::user();

        return response()->success($info);
    }

    public function postComments(){
        $usercreate = Auth::user();

        $comment = Comment::create([
            'module' => Input::get('module'),
            'key_field' => Input::get('key_field'),
            'text' => Input::get('text'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('comment');
    }


}
