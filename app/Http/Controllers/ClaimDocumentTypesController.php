<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ClaimDocumentTypes;
use Illuminate\Http\Request;

class ClaimDocumentTypesController extends Controller
{
      /**
     * Get all claimDocumentTypes.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $claimdocumenttypes = ClaimDocumentTypes::all();
        $claimdocumenttypes = DB::table('claim_document_types')->where('status', '=', '1')
                    ->select(["claim_document_types.*"])
                    ->get();
       
        return response()->success(compact('claimdocumenttypes'));
    }

    /**
     * Create new claimDocumentTypes.
     *
     * @return JSON
    */ 
    public function postClaimdocumenttypes()
    {
        $usercreate = Auth::user();
        
        $claimdocumenttype = ClaimDocumentTypes::create([
            'name' => Input::get('name'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('claimdocumenttype');
    }

    /**
     * Get claimDocumentTypes details referenced by id.
     *
     * @param int claimDocumentTypes ID
     *
     * @return JSON
    */ 
    public function getShow($id)
    {
        $claimDocumentTypes = ClaimDocumentTypes::find($id);
        
        return response()->success($claimDocumentTypes);
    }

    /**
     * Update claimDocumentTypes data.
     *
     * @return JSON success message
    */ 
    public function putShow(Request $request)
    {
        $claimDocumentTypesForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.status'
            )
        );

        $claimDocumentTypesId = intval($claimDocumentTypesForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3',
        ]);

        $userupdate = Auth::user();

        $claimDocumentTypesData = [
            'name' => $claimDocumentTypesForm['data.name'],
            'status' => $claimDocumentTypesForm['data.status'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = ClaimDocumentTypes::where('id', '=', $claimDocumentTypesId)->update($claimDocumentTypesData);

        return response()->success('success');
    }

    /**
     * Delete claimDocumentTypes Data.
     *
     * @return JSON success message
     */
    public function deleteClaimdocumenttypes($id)
    {
        $claimdocumenttypesData = [
            'status' => 0,            
        ];
        $affectedRows = ClaimDocumentTypes::where('id', '=', $id)->update($claimdocumenttypesData);
        return response()->success('success');
    }
}
