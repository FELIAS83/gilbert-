<?php

namespace App\Http\Controllers;

use App\User;
use App\Seller;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class SellerController extends Controller
{
    /**
     * Get all sellers.
     *
     * @return JSON
     */
    public function getIndex()
    {
        //$companies = Company::all();
        $sellers = DB::table('sellers')->where('sellers.status', '=', '1')
                    ->select(["sellers.*", DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS name')])
                    ->join('agents', 'sellers.agent_id', '=', 'agents.id')
                    ->get();

        return response()->success(compact('sellers'));
    }

    /**
     * Create new seller.
     *
     * @return JSON
     */
    public function postSellers()
    {
        $usercreate = Auth::user();
        
        $seller = Seller::create([
            'agent_id' => Input::get('agent_id'),
            'company_id' => Input::get('company_id'),
            'type' => Input::get('type'),
            'percentage_vn' => Input::get('percentage_vn'),
            'percentage_rp' => Input::get('percentage_rp'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('seller');
    }

    /**
     * Get seller details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $seller = Seller::find($id);
        
        return response()->success($seller);
    }

    /**
     * Update seller data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $sellerForm = array_dot(
            app('request')->only(
                'data.id',
                'data.company_id',
                'data.agent_id',
                'data.type',
                'data.percentage_vn',
                'data.percentage_rp'
            )
        );

        $sellerId = intval($sellerForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $sellerData = [
            'company_id' => $sellerForm['data.company_id'],
            'agent_id' => $sellerForm['data.agent_id'],
            'type' => $sellerForm['data.type'],
            'percentage_vn' => $sellerForm['data.percentage_vn'],
            'percentage_rp' => $sellerForm['data.percentage_rp'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Seller::where('id', '=', $sellerId)->update($sellerData);

        return response()->success('success');
    }

    /**
     * Delete seller Data.
     *
     * @return JSON success message
     */
    public function deleteSeller($id)
    {
        $sellerData = [
            'status' => 0,            
        ];
        $affectedRows = Seller::where('id', '=', $id)->update($sellerData);
        return response()->success('success');
    }
}
