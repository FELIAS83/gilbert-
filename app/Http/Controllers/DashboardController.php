<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Policy;
use Auth;
use Input;
use DB;

class DashboardController extends Controller
{
    public function getSalespermonth()
    {
        $today = date('Y-m-d');
        $five= date('Y-m-d',strtotime('-5 month',strtotime( $today)));
        $completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', $completed)
                    ->whereDate('policies.start_date_coverage', '>=', [$five])
                    ->whereDate('policies.start_date_coverage', '<=', [$today])
                    ->select([DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), DB::raw('count(*) as num_sales'),DB::raw("MONTH(policies.start_date_coverage) as month")])
                    ->join('fees', 'policies.id', '=', 'fees.policy_id')
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get(); 
               

        return response()->success(compact('policies'));
    }

    public function getSalesreport()
    {
        
        $completed = Input::get('completed'); 
        $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
        $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 

        $completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', $completed)
                    ->whereBetween('policies.start_date_coverage', [$startdate, $finaldate])
                    ->select([DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"),DB::raw("MONTH(policies.start_date_coverage) as month"), DB::raw('count(*) as num_sales')])
                    ->join('fees', 'policies.id', '=', 'fees.policy_id')                    
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get();

        return response()->success(compact('policies'));
    }

    public function getDollarspermonth()
    {
        $today = date('Y-m-d');
        $five= date('Y-m-d',strtotime('-5 month',strtotime( $today)));
        $completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', $completed)
                    ->select([ DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), 
                        DB::raw('case WHEN policies.plan_id = 3 AND coverage_id <> 0 THEN sum(fees.amount_titular+fees.amount_spouse+fees.amount_childs+300+fees.total) ELSE sum(fees.amount_titular+fees.amount_spouse+fees.amount_childs+fees.total)
                        END as total_money'),DB::raw("MONTH(policies.start_date_coverage) as month") ])
                    ->join('fees', 'policies.id', '=', 'fees.policy_id')
                    ->whereDate('policies.start_date_coverage', '>=', [$five])
                    ->whereDate('policies.start_date_coverage', '<=', [$today])
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get();

        return response()->success(compact('policies'));
    }

    public function getDollarsreport()
    {
        
        $completed = Input::get('completed'); 
        $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
        $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 
        $today = date('m');
        //echo $today;
        $completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', $completed)
                    ->select([ DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), 
                        DB::raw('case WHEN policies.plan_id = 3 AND coverage_id <> 0 THEN sum(fees.amount_titular+fees.amount_spouse+fees.amount_childs+300+fees.total) ELSE sum(fees.amount_titular+fees.amount_spouse+fees.amount_childs+fees.total)
                        END as total_money'),DB::raw("MONTH(policies.start_date_coverage) as month") ])
                    ->join('fees', 'policies.id', '=', 'fees.policy_id')
                    //->whereMonth('start_date_coverage', '>=', [$today-5])
                    ->whereBetween('policies.start_date_coverage',[$startdate, $finaldate])
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get();

        return response()->success(compact('policies'));
    }

    public function getCanceledpolicies()
    {
        $today = date('m');
        //echo $today;
        //$completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '0')->where('policies.motive','!=','0')
                    //->where('policies.completed', '=', $completed)
                    ->select([DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), 
                        DB::raw('count(*) as canceled'),
                        DB::raw("MONTH(policies.start_date_coverage) as month")
                        //DB::raw('policies.start_date_coverage IFNULL(COUNT (p2.start_date_coverage), 0) as canceled')
                    ])
                    
                    ->whereMonth('policies.start_date_coverage', '>=', [$today-5])
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get();

        return response()->success(compact('policies'));
    }

    public function getCanceledpoliciesreport()
    {
        $today = date('m');
        $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
        $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 
        //echo $today;
        //$completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '0')->where('policies.motive','!=','0')
                    //->where('policies.completed', '=', $completed)
                    ->select([DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage")
                        , DB::raw('count(*) as canceled'),
                        DB::raw("MONTH(policies.start_date_coverage) as month")])
                    //->whereMonth('policies.start_date_coverage', '>=', [$today-5])
                    ->whereBetween('policies.start_date_coverage',[$startdate, $finaldate])
                    ->groupBy(DB::raw("MONTH(start_date_coverage)"))
                    ->get();

        return response()->success(compact('policies'));
    }

         /**
     * Get all policies expired.
     *
     * @return JSON
     */
    public function getGracepolicies()
    {
      
        $today = date('m');
             
        $policies= DB::table('fees')->where('fees.status', '=', '1')
                    ->where('fees.paid_fee', '=', '0')
                    ->where('payments.status', '=', '1')
                    ->where('policies.status','=', '1')
                   // ->whereBetween('fees.fee_payment_date', [$startdate, $finaldate])
                    ->join('policies', 'fees.policy_id', '=', 'policies.id')
                    ->join('payments', 'fees.policy_id', '=', 'payments.policy_id')
                    ->leftjoin('fee_steps', 'fees.id', '=', 'fee_steps.fee_id')
                    ->select(['policies.id as id', 'policies.plan_id', "fees.id as fee_id","fees.total", "payments.mode",DB::raw("date_format(fees.fee_payment_date, '%m/%d/%Y') as fee_payment_date"),
                        DB::raw('DATEDIFF(CURDATE(), fees.fee_payment_date) as days_passed'),DB::raw('count(*) as num_grace'), 'fees.fee_number', 'payments.mode as modenum','fee_steps.step1','fee_steps.step2','fee_steps.step3','fee_steps.step4','fee_steps.step5','fee_steps.step6',DB::raw("MONTH(fees.fee_payment_date) as month")])
                    ->whereMonth('fees.fee_payment_date', '<=', [$today])
                    ->whereMonth('fees.fee_payment_date', '>=', [$today-5])
                    //->whereBetween('fees.fee_payment_date',[$startdate, $finaldate])
                    ->groupBy(DB::raw("MONTH(fees.fee_payment_date)"))
                    ->get();
            

        return response()->success(compact('policies'));
        
    }

        public function getGracepoliciesreport()
    {
      
        $today = date('m');

        $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
        $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 
             
        $policies = DB::table('fees')->where('fees.status', '=', '1')
                    ->where('fees.paid_fee', '=', '0')
                    ->where('payments.status', '=', '1')
                    ->where('policies.status','=', '1')
                   // ->whereBetween('fees.fee_payment_date', [$startdate, $finaldate])
                    ->join('policies', 'fees.policy_id', '=', 'policies.id')
                    ->join('payments', 'fees.policy_id', '=', 'payments.policy_id')
                    ->leftjoin('fee_steps', 'fees.id', '=', 'fee_steps.fee_id')
                    ->select(['policies.id as id', 'policies.plan_id', "fees.id as fee_id","fees.total", "payments.mode",DB::raw("date_format(fees.fee_payment_date, '%m/%d/%Y') as fee_payment_date"),
                        DB::raw('DATEDIFF(CURDATE(), fees.fee_payment_date) as days_passed'),DB::raw('count(*) as num_grace'), 'fees.fee_number', 'payments.mode as modenum','fee_steps.step1','fee_steps.step2','fee_steps.step3','fee_steps.step4','fee_steps.step5','fee_steps.step6',DB::raw("MONTH(fees.fee_payment_date) as month")])
                    //->whereMonth('fees.fee_payment_date', '<=', [$today])
                    //->whereMonth('fees.fee_payment_date', '>=', [$today-5])
                    ->whereBetween('fees.fee_payment_date',[$startdate, $finaldate])
                    ->groupBy(DB::raw("MONTH(fees.fee_payment_date)"))
                    ->get();
            

        return response()->success(compact('policies'));
        
    }

    public function getClaimspermonth()
    {
        $today = date('Y-m-d');
        $six= date('Y-m-d',strtotime('-6 month',strtotime( $today)));
        
        $claims = DB::table('claims')->where('claims.status', '=', '1')
                    ->select([DB::raw("date_format(eobs.created_at, '%d/%m/%Y') as eob_date"), DB::raw('count(DISTINCT eobs.id_claim) as num_claims'),DB::raw("MONTH(eobs.created_at) as month")])
                    ->join('eobs', 'claims.id', '=', 'eobs.id_claim')
                    ->whereDate('eobs.created_at', '>=', [$six])
                    ->whereDate('eobs.created_at', '<=', [$today])
                    ->groupBy(DB::raw("MONTH(eobs.created_at)"))
                    ->get(); 
               

        return response()->success(compact('claims'));
    }
    public function getClaimsreport()
    {
        
        $startdate = date('Y-m-d',  strtotime(Input::get('start_date')));  
        $finaldate = date('Y-m-d',  strtotime(Input::get('end_date'))); 

        $claims = DB::table('claims')->where('claims.status', '=', '1')
                    ->select([DB::raw("date_format(eobs.created_at, '%d/%m/%Y') as eob_date"), DB::raw('count(DISTINCT eobs.id_claim) as num_claims'),DB::raw("MONTH(eobs.created_at) as month")])
                    ->join('eobs', 'claims.id', '=', 'eobs.id_claim')
                    ->whereDate('eobs.created_at', '>=', [$startdate])
                    ->whereDate('eobs.created_at', '<=', [$finaldate])
                    ->groupBy(DB::raw("MONTH(eobs.created_at)"))
                    ->get(); 
               

        return response()->success(compact('claims'));
    }

}
