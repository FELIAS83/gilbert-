<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Ticket;
use App\User;
use App\Policy;
use App\Dependent;

use App\Applicant;
use Input;
use DB;
use App\TicketDocuments; 
use App\TicketTypes; 
use App\Http\Requests;
use App\Configuration;

class TicketController extends Controller
{
     /**
     * Get all tickets.
     *
     * @return JSON
     */
    public function getIndex()
    {
        //$tickets = Ticket::all();
        $tickets = DB::table('tickets')->where('tickets.status', '=', '1')
                    ->select(["tickets.id AS ticket_id","tickets.id_person","tickets.id_policy","tickets.person_type","dependents.id as dependents_id","applicants.id as applicants_id", "tickets.hour",DB::raw("date_format(tickets.date_start, '%d/%m/%Y') as date_start"),"tickets.date_end","policies.number","ticket_types.name", "ticket_types.id as ticket_types_id","ticket_types.name AS type_name", "tickets.confirmed_date",DB::raw("date_format(tickets.created_at, '%d/%m/%Y') as created"),
                        DB::raw('CONCAT(dependents.first_name," ",dependents.last_name) AS dependents_name'), 
                        DB::raw('CONCAT(applicants.first_name," ",applicants.last_name) AS applicants_name'),DB::raw('CASE person_type
                        when "A" THEN CONCAT(applicants.first_name," ",applicants.last_name)
                        ELSE CONCAT(dependents.first_name," ",dependents.last_name)
                        END AS name'), 'tickets.guarantee_letter', 'tickets.request_medical'])
                    ->join('policies','tickets.id_policy','=','policies.id')
                    ->leftJoin('dependents', 'tickets.id_person','=','dependents.id')
                    ->leftJoin('applicants', 'tickets.id_person','=','applicants.id')
                    ->join('ticket_types','tickets.id_ticket_type','=','ticket_types.id')
                    ->orderby("tickets.id")
                    ->get();
       
        return response()->success(compact('tickets'));
    }

    /**
     * Create new ticket.
     *
     * @return JSON
    */ 
    public function postTickets()
    {
        $usercreate = Auth::user();
            
        
        $doctypes = Input::get('id_doc_types');
        $filenames = Input::get('filenames');
        $policy_id = Input::get('id_policy');
    
        // if (isset($policy_id)){

        $ticket = Ticket::create([
            'id_policy'=>Input::get('id_policy'),
            'id_person'=>Input::get('id_person'),
            'person_type'=>Input::get('person_type'),
            'hour'=>Input::get('hour'),
            'hospital'=>Input::get('hospital'),
            'doctor'=>Input::get('doctor'),
            'medical_speciality'=>Input::get('medical_speciality'),
            'date_start'=>Input::get('date_start'),
            'date_end'=>Input::get('date_end'),
            'city'=>Input::get('city'),
            'id_ticket_type'=>Input::get('ticket_type'),
            'user_id_creation' => $usercreate->id
        ]);
        
        $ticket_id = Input::get('ticket_id');
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->ticket_file_directory)) ? $directory[0]->ticket_file_directory : 0;

        for ($i = 0; $i < count($doctypes) ; $i++ ){//creacion de documentos de tickets de reclamo
        $ticketdocument= TicketDocuments::create([
            'ticket_id'=>$ticket->id,
            'id_doc_type'=>$doctypes[$i],
            'filename'=>$filenames[$i],
            'directory'=>$directory.'/'.'tickets/'.$ticket->id,
            'user_id_creation'=> $usercreate->id    
        ]);
        }

        $modified = Input::get('modified');
        if (isset($modified)){
            foreach ($modified as $value) {
                $affectedRows = TicketDocuments::where('id', '=', $value['id'])->update([
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => $usercreate->id]
                );//update
            }
        }

        $document_ids = Input::get('deleted_ids');
        if (isset($document_ids)){
            foreach ($document_ids as $value) {
                $affectedRows = TicketDocuments::where('id', '=', $value)->update([
                    'status' => 0,
                    'user_id_update' => $usercreate->id,
                ]);//delete
            }
        }

        return response()->success($ticket->id);
        return response()->success($directory);
    }

    /**
     * Get ticket details referenced by id.
     *
     * @param int ticket ID
     *
     * @return JSON
    */ 
    public function getShow($id)
    {
              $ticket= DB::table('tickets')->where('tickets.id','=',$id)->where('tickets.status','=','1')
                        ->select(["tickets.id","tickets.person_type", "tickets.id_person","tickets.id_policy","tickets.hour","tickets.doctor", DB::raw('CONCAT(doctors.names," ",doctors.surnames) as doctor_name'),"tickets.city","tickets.hospital","hospitals.name AS hospital_name","tickets.date_start","tickets.date_end", "tickets.medical_speciality","medical_specialties.name as speciality_name","tickets.person_type","tickets.created_at","ticket_types.name as type_name", "ticket_types.id as type_id",DB::raw('CONCAT(dependents.first_name," ",dependents.last_name) AS dependents_name'), 
                        DB::raw('CONCAT(applicants.first_name," ",applicants.last_name) AS applicants_name'),
                        DB::raw('CASE person_type
                        when "A" THEN CONCAT(applicants.first_name," ",applicants.last_name)
                        ELSE CONCAT(dependents.first_name," ",dependents.last_name)
                        END AS name'),"policies.number as number","applicants.id as applicants_id"])
                        ->join('policies','tickets.id_policy','=','policies.id')
                        ->leftJoin('dependents', 'tickets.id_person','=','dependents.id')
                        ->leftJoin('applicants', 'tickets.id_person','=','applicants.id')
                        ->leftJoin('ticket_types','tickets.id_ticket_type','=','ticket_types.id')
                        ->leftJoin('hospitals','tickets.hospital','=','hospitals.id')
                        ->leftJoin('medical_specialties', 'tickets.medical_speciality','=','medical_specialties.id')
                        ->leftJoin('doctors','tickets.doctor','=','doctors.id')
                        ->distinct()
                        ->get();
        
        return response()->success($ticket);
    }

    /**
     * Get ticket details referenced by id.
     *
     * @param int ticket ID
     *
     * @return JSON
    */ 
    public function getShowticket($id)
    {
              $ticket= DB::table('tickets')->where('tickets.id','=',$id)->where('tickets.status','=','1')
                        ->select(["tickets.id","tickets.person_type", "tickets.id_person","tickets.id_policy","tickets.hour","tickets.doctor", DB::raw('CONCAT(doctors.names," ",doctors.surnames) as doctor_name'),"tickets.city","tickets.hospital","hospitals.name AS hospital_name",DB::raw("date_format(tickets.date_start, '%d/%m/%Y') as date_start"),DB::raw("date_format(tickets.date_end, '%d/%m/%Y') as date_end"), "tickets.medical_speciality","medical_specialties.name as speciality_name","tickets.person_type","tickets.created_at","ticket_types.name as type_name", "ticket_types.id as type_id",DB::raw('CONCAT(dependents.first_name," ",dependents.last_name) AS dependents_name'), 
                        DB::raw('CONCAT(applicants.first_name," ",applicants.last_name) AS applicants_name'),
                        DB::raw('CASE person_type
                        when "A" THEN CONCAT(applicants.first_name," ",applicants.last_name)
                        ELSE CONCAT(dependents.first_name," ",dependents.last_name)
                        END AS name'),"policies.number as number","applicants.id as applicants_id"])
                        ->join('policies','tickets.id_policy','=','policies.id')
                        ->leftJoin('dependents', 'tickets.id_person','=','dependents.id')
                        ->leftJoin('applicants', 'tickets.id_person','=','applicants.id')
                        ->leftJoin('ticket_types','tickets.id_ticket_type','=','ticket_types.id')
                        ->leftJoin('hospitals','tickets.hospital','=','hospitals.id')
                        ->leftJoin('medical_specialties', 'tickets.medical_speciality','=','medical_specialties.id')
                        ->leftJoin('doctors','tickets.doctor','=','doctors.id')
                        ->distinct()
                        ->get();
        
        return response()->success($ticket);
    }

    public function getShowdocuments($id)
    {
            $docs = DB::table('ticket_documents')->where('ticket_documents.status', '=', '1')
                    ->where('ticket_id',$id)
                    //->select('ticket_documents.id_doc_type')
                    ->get();

                for ($i=0; $i <count($docs) ; $i++) { 
             
                    if ($docs[$i]->id_doc_type == 1){

                        $type= "Formulario de Hospital";
                    }
                    else if ($docs[$i]->id_doc_type == 2){
                        $type= "Formulario de BD";
                    }
                    else if ($docs[$i]->id_doc_type == 3){
                        $type= "Informes Medicos";
                    }
                    else if ($docs[$i]->id_doc_type == 4){
                        $type= "Confirmación de Cita";
                    }
                    else if ($docs[$i]->id_doc_type == 5){
                        $type= "Formulario Interconsulta";
                    }
                $docs[$i]->type=$type;
                } 

        //return response()->success($type);             

        return response()->success($docs);
    }

    public function postTicketsdocs(Request $request)
    {

        $user = Auth::user();
        $ticket_id = $request['ticket_id'];
        $id_person= $request['id_person'];
        $person_type=$request['person_type'];
        $medical_speciality = $request['medical_speciality'];
        $hospital= $request['hospital'];
        $doctor = $request['doctor'];
        $city = $request['city'];
        $date_start = $request['date_start'];
        $date_end = $request['date_end'];
        $hour = $request['hour'];
        $filenames= $request['filenames'];
        $doctypes= $request['id_doc_types'];
        $id_ticket_type= $request['id_ticket_type'];
        //$document_ids = $request['ids'];//files to delete
        //$modified = $request['modified'];//files to update
        
        $ticketsdocs = Ticket::where('id','=',$ticket_id)->update([
            'id_ticket_type'=>$id_ticket_type,
            'id_person' => $id_person,
            'person_type'=>$person_type,
            'hospital' => $hospital,
            'doctor' => $doctor,
            'city' => $city,
            'medical_speciality' => $medical_speciality,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'hour' => $hour,
            'user_id_update'=>$user->id
        ]);

        
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->ticket_file_directory)) ? $directory[0]->ticket_file_directory : 0;

        for ($i = 0; $i < count($doctypes) ; $i++ ){//creacion de documentos de tickets de reclamo
        $ticketdocument= TicketDocuments::create([
            'ticket_id'=>$ticket_id,
            'id_doc_type'=>$doctypes[$i],
            'filename'=>$filenames[$i],
            'directory'=>$directory.'/'.'tickets/'.$ticket_id,
            'user_id_creation'=> $user->id  
        ]);
        }

    
       $modified = $request['modified'];
        if (isset($modified)){
            foreach ($modified as $value) {
                $affectedRows = TicketDocuments::where('id',$value['id'])->update([
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => $user->id
                ]);//update
            }
        }

        $document_ids = $request['deleted_ids'];
        if (isset($document_ids)){
            foreach ($document_ids as $value) {
                $affectedRows = TicketDocuments::where('id', $value['id'])->update([
                    'status' => 0,
                    'user_id_update' => $user->id,
                ]);//delete
            }
        }   

        return response()->success(compact('ticketsdocs'));

    }

    public function postTicketdate(Request $request){

        
        $userupdate = Auth::user();
        $ticket_id = $request['ticket_id'];
        $medical_speciality = $request['medical_speciality'];
        $hospital= $request['hospital'];
        $doctor = $request['doctor'];
        $city = $request['city'];
        $date_start = $request['date_start'];
        $date_end = $request['date_end'];
        $hour = $request['hour'];
        $filenames= $request['filenames'];
        $doctypes= $request['id_doc_types'];

        $ticketdate = Ticket::where('id', '=', $ticket_id)->update([
            'date_start'=>$date_start,
            'hour'=>$hour,
            'hospital' => $hospital,
            'doctor' => $doctor,
            'city' => $city,
            'medical_speciality' => $medical_speciality,
            'confirmed_date'=>'1',
            //'user_id_update'=>$userupdate->id
        ]);

        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->ticket_file_directory)) ? $directory[0]->ticket_file_directory : 0;

        $user_id_creation = Auth::user();
        //var_dump($user_id_creation);
              
        for ($i = 0; $i < count($doctypes) ; $i++ ){//creacion de documentos de tickets de reclamo
            $ticketdocument= TicketDocuments::create([
                'ticket_id'=>$ticket_id,
                'id_doc_type'=>$doctypes[$i],
                'filename'=>$filenames[$i],
                'directory'=>$directory.'/'.'tickets/'.$ticket_id,
                'user_id_creation'=> '1' 
            ]);
        }
        
        
        $modified = $request['modified'];
        if (isset($modified)){
            foreach ($modified as $value) {
                $affectedRows = TicketDocuments::where('id',$value['id'])->update([
                'id_doc_type' => $value['id_doc_type'],
                'filename' => $value['filename'],
                'user_id_update' => '1'
                ]);//update
            }
        }

        $document_ids = $request['deleted_ids'];
            if (isset($modified))
            foreach ($document_ids as $value) {
                $affectedRows = TicketDocuments::where('id', $value['id'])->update([
                    'status' => 0,
                    'user_id_update' => '1',
                ]);//delete
            }
        //}   

        return response()->success(compact('ticketdate'));
    }




    /**
     * Update ticket data.
     *
     * @return JSON success message
    */ 
    public function putShow(Request $request)
    {
        $ticketeditForm = array_dot(
            app('request')->only(
                'data.id_person',
                'data.hour',
                'data.city',
                'data.hospital',
                'data.date_start',
                'data.date_end',
                'data.medical_speciality',
                'data.id_doc_type',
                'data.filename'
            )
        );

        $ticketId = intval($ticketeditForm['data.id']);

        $this->validate($request, [
            'data.id_person' => 'required|integer',
            'data.city' => 'required',
            'data.doctor' => 'required',
            'data.date_start' => 'required',
            'data.date_end' => 'required',
        ]);

        $userupdate = Auth::user();

        $ticketData = [
            'id_person' => $ticketeditForm['data.id_person'],
            'city' => $ticketeditForm['data.city'],
            'doctor' => $ticketeditForm['data.doctor'],
            'hospital' => $ticketeditForm['data.hospital'],
            'medical_speciality' => $ticketeditForm['data.medical_speciality'],
            'date_start' => $ticketeditForm['data.date_start'],
            'date_end' => $ticketeditForm['data.date_end'],
            'hour' => $ticketeditForm['data.hour'],            
            'status' => $ticketeditForm['data.status'],
            'user_id_update' => $userupdate->id
        ];

        $affectedRows = Ticket::where('id', '=', $ticketId)->update($ticketData);

        return response()->success('success');

    }

    /**
     * Delete ticket Data.
     *
     * @return JSON success message
     */
    public function deleteTicket($id)
    {
        $ticketData = [
            'status' => 0,            
        ];
        $affectedRows = Ticket::where('id', '=', $id)->update($ticketData);
        return response()->success('success');
    }
}
