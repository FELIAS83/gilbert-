<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Configuration;
use Auth;

use Input;

class ConfigurationController extends Controller
{
    public function getShow(){
        $directory = Configuration::where('status', '1')->get();
        if (isset($directory[0])){
            return response()->success($directory[0]);
        }
        else{

            return response()->success($directory);
        }
    }

    public function putShow(Request $request)
    {
        $directoryForm = array_dot(
            app('request')->only(
                'data.id',
                'data.policy_file_directory'
            )
        );

        $userupdate = Auth::user();

        $directoryData = [
            'policy_file_directory' => $directoryForm['data.policy_file_directory'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Configuration::where('id', '=', '1')->update($directoryData);
        return response()->success('success');
    }

    public function postConfigurations()
    {
        $usercreate = Auth::user();

        $configurations = Configuration::create([
            'id' => 1,
            'policy_file_directory' => Input::get('policy_file_directory'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('configurations');
    }

}
