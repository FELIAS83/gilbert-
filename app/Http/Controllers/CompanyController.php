<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\CompanyBranch;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class CompanyController extends Controller
{
    /**
     * Get all companies.
     *
     * @return JSON
     */
    public function getIndex()
    {
        //$companies = Company::all();
        $companies = DB::table('companies')->where('status', '=', '1')
                    ->select(["companies.*"])
                    ->get();

        return response()->success(compact('companies'));
    }

    /**
     * Create new company.
     *
     * @return JSON
     */
    public function postCompanies()
    {
        $usercreate = Auth::user();
        $branch = Input::get('branch');
        
        $company = Company::create([
            'name' => Input::get('name'),
            //'type' => Input::get('type'),
            'commission_percentage' => Input::get('commission_percentage'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('company');
    }

    /**
     * Get company details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $company = Company::find($id);
        
        return response()->success($company);
    }

    /**
     * Update company data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $companyForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                //'data.type',
                'data.commission_percentage'
            )
        );

        $companyId = intval($companyForm['data.id']);
        $branches = $request['data.branches'];
        
        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3'
        ]);

        $userupdate = Auth::user();

        $companyData = [
            'name' => $companyForm['data.name'],
            //'type' => $companyForm['data.type'],
            'commission_percentage' => $companyForm['data.commission_percentage'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Company::where('id', '=', $companyId)->update($companyData);         
        return response()->success('success');
    }

    /**
     * Delete company Data.
     *
     * @return JSON success message
     */
    public function deleteCompany($id)
    {
        $companyData = [
            'status' => 0,            
        ];
        $affectedRows = Company::where('id', '=', $id)->update($companyData);

        $registers =  CompanyBranch::where('company_id',$id)->get();
            foreach ($registers as  $register) {
                $ids[] = $register->id;
            }
            if(isset($ids)){
                $del = CompanyBranch::destroy($ids);
            }

        return response()->success('success');
    }

    /**
    * Get Company branches
    *
    *
    */
    public function getCompanybranches($id){

        $companybranches = CompanyBranch::where('company_id',$id)->where('status',1)
                                ->get();
        return response()->success($companybranches);                        
    }
}
