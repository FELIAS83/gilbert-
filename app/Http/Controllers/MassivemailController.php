<?php

namespace App\Http\Controllers;


use App\User;
use Auth;
use Input;
use DB; 

use Illuminate\Http\Request;
Use Emailformat;
use App\Http\Requests;
use App\Massivemail;

class MassivemailController extends Controller
{
	
	public function getShow($id)
    {
        $massivemail = Massivemail::find($id);
        return response()->success($massivemail);
         
    }

     /**
     * Create new massivemail.
     *
     * @return JSON
    */ 
    public function postMassivemails()
    {
        $usercreate = Auth::user();
        
        $recipient= Input::get('recipient');
      
        $massivemail = Massivemail::create([
            'format' => Input::get('format'),
            'subject' => Input::get('subject'),
            'title' => Input::get('title'),
            'content' => Input::get('content'),
            'recipient' => Input::get('recipient'),
            'status' => Input::get('status'),
            'user_id_creation' => $usercreate->id
        ]);      
        


        return response()->success($recipient);
    }

    public function putShow(Request $request)
    {
        $massivemailForm = array_dot(
            app('request')->all()
        );

        $massivemailId = intval($massivemailForm['data.id']);



        $this->validate($request, [
            'data.id' => 'required',
            'data.format' => 'required',
            'data.sbuject' => 'required',
            'data.title' => 'required',
            'data.content' => 'required',
            'data.recipient' => 'required'
            
        ]);

        $massivemailData = [
            'format' => $massivemailForm['data.format'],
            'subject' => $massivemailForm['data.subject'],
            'title' => $massivemailForm['data.title'],
            'content' => $massivemailForm['data.content'],
            'recipient' => $massivemailForm['data.recipient']
            
        ];

        Massivemail::where('id', '=', $massivemailId)->update($MassivemailData);

        return response()->success('success');
	}
}
	
