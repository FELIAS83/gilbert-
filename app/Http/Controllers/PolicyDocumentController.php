<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Configuration;
use Auth;
use Input;

use App\PolicyDocument;
use App\Policy;
use App\Policystep;

class PolicyDocumentController extends Controller
{
    public function getShow($id)
    {
        //$policydocument = PolicyDocument::where('status', '1')->where('id_policy',$id)->get();
        $policydocument = DB::table('policy_documents')->where('policy_documents.status', '=', '1')
        			->where('id_policy',$id)
                    ->select(["policy_documents.*", "doc_types.name"])
                    ->join('doc_types', 'policy_documents.id_doc_type', '=', 'doc_types.id')
                    ->get();

        foreach ($policydocument as $key=> $value){
            $policydocument[$key]->id_doc_type = (int)$policydocument[$key]->id_doc_type;
        }

        return response()->success($policydocument);
    }

    public function postDocuments(){
        $userupdate = Auth::user();
        $policy_id = Input::get('policy_id');
        $document_ids = Input::get('ids');//files to delete
        $modified = Input::get('modified');//files to modify
        /*-------new files-------------*/
        $doctypes = Input::get('id_doc_types');
        $descriptions = Input::get('descriptions');
        $filenames = Input::get('filenames');
         /*---------------------------*/

        $documentData = [
            'status' => 0,
            'user_id_update' => $userupdate->id,
        ];
        
        foreach ($document_ids as $value) {
            $affectedRows = PolicyDocument::where('id', '=', $value)->update($documentData);//delete
        }
        
        foreach ($modified as $value) {
            $affectedRows = PolicyDocument::where('id', '=', $value['id'])->update([
            'description' => $value['description'],
            'id_doc_type' => $value['id_doc_type'],
            'filename' => $value['filename'],
            'user_id_update' => $userupdate->id]
            );//modify

        }
        
        $directory = Configuration::where('status', '1')->get();
        $directory = (isset($directory[0]->policy_file_directory)) ? $directory[0]->policy_file_directory : 0;

        for ($i = 0; $i < count($doctypes) ; $i++ ){//Insert
            $policydocument = PolicyDocument::create([
            'id_policy' => $policy_id,
            'id_doc_type' => $doctypes[$i],
            'description' => $descriptions[$i],
            'directory' => 'policies/'.$policy_id,
            'filename' => $filenames[$i],
            'user_id_creation' => $userupdate->id
        ]);
        }

        $dataStep = [
            'step1' => 1,
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Policystep::where('id_policy', '=',  $policy_id)->update($dataStep);
        
        return response()->success('success');

    }
}
