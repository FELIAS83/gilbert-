<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Deductible;
use Auth;
use Input;
use DB;


class DeductibleController extends Controller
{
    //
     /**
     * Get all deductibles
     *
     * @return JSON
     */
    public function getIndex()
    {
        $deductibles = DB::table('deductibles')->where('status', '=', '1')
                    ->get();

        return response()->success(compact('deductibles'));
    }
    /**
     * Get Deductible details referenced by plan_id.
     *
     * @param int plan ID
     *
     * @return JSON
     */
  public function getDeductible($id)
    {

     $deductible = Deductible::where('plan_id', '=', $id)
                    ->where('status', '=', '1')
                    ->get();
        
        return response()->success($deductible);   

    }

   /**
     * Get deductoble details referenced by id.
     *
     * @param int deductible ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $deductible = Deductible::find($id);
        
        return response()->success($deductible);
    }  

    /**
     * Create new Deductible for a Plan.
     *
     * @return JSON
     */
    public function postDeductibles()
    {
        $usercreate = Auth::user();
        
        $deductible = Deductible::create([
            'name' => Input::get('name'),
            'amount_in_usa' => Input::get('amount_in_usa'),
            'amount_out_usa' => Input::get('amount_out_usa'),
            'plan_id' => Input::get('plan_id'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('deductible');
    }

    /**
     * Update deductible detail data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $deductibleForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.amount_in_usa',
                'data.amount_out_usa'
                
            )
        );

        $deductibleId = intval($deductibleForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer'
        ]);

        $userupdate = Auth::user();

        $deductibleData = [
            'name' => $deductibleForm['data.name'],
            'amount_in_usa' => $deductibleForm['data.amount_in_usa'],
            'amount_out_usa' => $deductibleForm['data.amount_out_usa'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Deductible::where('id', '=', $deductibleId)->update($deductibleData);

        return response()->success('success');
    }


      /**
     * Delete deductible Data.
     *
     * @return JSON success message
     */
    public function deleteDeductible($id)
    {
        $deductibleData = [
            'status' => 0,            
        ];
        $affectedRows = Deductible::where('id', '=', $id)->update($deductibleData);
        return response()->success('success');
    }

}
