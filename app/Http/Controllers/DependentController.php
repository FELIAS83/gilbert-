<?php

namespace App\Http\Controllers;

use App\User;
use App\Dependent;
use App\Diagnostic;
use App\Applicant;
use App\Ticket;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class DependentController extends Controller
{
    /**
     * Get dependent details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $dependent = Dependent::where ('applicant_id', $id)->where('status', '1')->get();
        //$dependent[0]->height = (double)($dependent[0]->height);
        foreach ($dependent as $key => $value) {
            $value->height = (double) $value->height;
            $value->weight = (double) $value->weight;
        }

        return response()->success($dependent);


    }
     /**

     * Get dependent details referenced by aplicantId.
     *
     * @param int aplicant ID
     *
     * @return JSON
     */
    public function getDependentsid($id)
    {
        $id_person = $id;    
        $dependents = Dependent::where('id','=', $id_person)->get();

        return response()->success($dependents);


    }



    /**
     * Get dependent details referenced by aplicantId.
     *
     * @param int aplicant ID
     *
     * @return JSON
     */
    public function getDependents($id)
    {
        $dependents = Dependent::where('applicant_id','=', $id)->where('status','=', '1')
        ->select('dependents.*', DB::raw("date_format(dependents.created_at, '%Y-%m-%d') as inclusion_date"))
        ->get();
        return response()->success($dependents);


    }

    /**
     * Update dependent data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $applicantForm = array_dot(
            app('request')->only(
                'data.id',
                'data.first_name',
                'data.last_name',
                'data.gender',
                'data.birthday',
                'data.height',
                'data.height_unit_measurement',
                'data.document_type',
                'data.identity_document',
                'data.weight',
                'data.weight_unit_measurement'
            )
        );

        $applicantId = intval($applicantForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.first_name' => 'required|min:3',
            'data.last_name' => 'required|min:3'
        ]);

        $userupdate = Auth::user();

        $applicantData = [
            'first_name' => $applicantForm['data.first_name'],
            'last_name' => $applicantForm['data.last_name'],            
            'gender' => $applicantForm['data.gender'],            
            'birthday' => $applicantForm['data.birthday'],
            'height' => $applicantForm['data.height'],
            'height_unit_measurement' => $applicantForm['data.height_unit_measurement'],
            'document_type' => $applicantForm['data.document_type'],
            'identity_document' => $applicantForm['data.identity_document'],
            'weight' => $applicantForm['data.weight'],
            'weight_unit_measurement' => $applicantForm['data.weight_unit_measurement'],            
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Dependent::where('id', '=', $applicantId)->update($applicantData);

        return response()->success($applicantForm['data.policy_id']);
    }

    public function postDependents(Request $request)
    {
        $usercreate = Auth::user();
        $aplicantid = $request['aplicantid'];
        $dependents = Input::get('dependents');

        $dependentData = [
            'status' => 0,
        ];
        $affectedRows = Dependent::where('applicant_id', '=', $aplicantid)->update($dependentData);


        /*$this->validate($request, [
            'dependents' => 'required',
        ]);*/

        for ($i = 0; $i < count($dependents); $i++){
            $dependent = Dependent::create([
                'applicant_id' => $aplicantid,
                'first_name' => $dependents[$i]['name'],
                'last_name' => $dependents[$i]['lastname'],
                'document_type' => $dependents[$i]['pid_type'],
                'identity_document' => $dependents[$i]['pid_num'],
                'relationship' => $dependents[$i]['role'],
                'birthday' => $dependents[$i]['dob'],
                'height' => $dependents[$i]['height'],
                'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                'weight' => $dependents[$i]['weight'],
                'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                'gender' => $dependents[$i]['sex'],
                'user_id_creation' => $usercreate->id
            ]);
        }       

        return response()->success($aplicantid);
    }

    public function deleteDependents($id)///elimina los dependientes y los diagnosticos asociados
    {
        $dependentData = [
            'status' => 0,
        ];

        $consulta=DB::table('diagnostics')->where('diagnostics.status','=','1')->where('diagnostics.dispatched','=','0')
                    ->where('id_person','=',$id)->where('person_type','=','D')
                    ->orWhere('id_person','=',$id)->where('person_type','=','C')
                    ->select(['diagnostics.id as diagnostics_id','diagnostics.claim_id','diagnostics.dispatched','claims.step','diagnostics.id as diagnostics_id','diagnostics.status as diagnostics_status','id_person','person_type'])
                    ->JOIN ('claims','diagnostics.claim_id','=','claims.id')
                    ->where('claims.step','=','0')
                    ->where('claims.status','=','1')
                    ->get();        

          //var_dump($consulta);

        if ($consulta!==null){            
        $affectedRows=  Diagnostic::where('id_person','=',$id)
                        ->where('person_type','=','D')
                        ->orWhere('id_person','=',$id)
                        ->where('person_type','=','C')
                        ->where('dispatched','=','0')
                        ->update($dependentData);
        // var_dump($affectedRows);               
        $affectedRows = Dependent::where('id', '=', $id)->update($dependentData);
        }
        else{
         $affectedRows = Dependent::where('id', '=', $id)->update($dependentData);   
        }
        return response()->success('success');
    }


    ///elimina los dependientes, los diagnosticos asociados y agrega el motivo de                                                       la  eliminacion en Renovacion de polizas
  public function postMotivedependents(Request $request)
    {
       
        $id = $request['dependentId'];
        $motive = $request['motive'];

        $dependentData = [
            'status' => 0,
            'motive' => $motive,
        ];
        $statusData = [
            'status' => 0
        ];

        $consulta=DB::table('diagnostics')->where('diagnostics.status','=','1')->where('diagnostics.dispatched','=','0')
                    ->where('id_person','=',$id)->where('person_type','=','D')
                    ->orWhere('id_person','=',$id)->where('person_type','=','C')
                    ->select(['diagnostics.id as diagnostics_id','diagnostics.claim_id','diagnostics.dispatched','claims.step','diagnostics.id as diagnostics_id','diagnostics.status as diagnostics_status','id_person','person_type'])
                    ->JOIN ('claims','diagnostics.claim_id','=','claims.id')
                    ->where('claims.step','=','0')
                    ->where('claims.status','=','1')
                    ->get();        

          //var_dump($consulta);

        if ($consulta!==null){            
        $affectedRows=  Diagnostic::where('id_person','=',$id)
                        ->where('person_type','=','D')
                        ->orWhere('id_person','=',$id)
                        ->where('person_type','=','C')
                        ->where('dispatched','=','0')
                        ->update($statusData);
        // var_dump($affectedRows);               
        $affectedRows = Dependent::where('id', '=', $id)->update($dependentData);
        }
        else{
         $affectedRows = Dependent::where('id', '=', $id)->update($dependentData);   
        }
        return response()->success('success');
    }


     /**
     * Get dependent deleted details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getDeletedependent($id)
    {
        $dependentDel = Dependent::where ('applicant_id', $id)->where('status', '0')->get();
        //$dependent[0]->height = (double)($dependent[0]->height);
        foreach ($dependentDel as $key => $value) {
            $value->height = (double) $value->height;
            $value->weight = (double) $value->weight;
        }

        return response()->success($dependentDel);


    }


    public function postUpdatedependents(Request $request)
    {
        $usercreate = Auth::user();
        $aplicantid = $request['aplicantid'];
        $dependents = Input::get('dependents');

        
        /*$this->validate($request, [
            'dependents' => 'required',
        ]);*/

        for ($i = 0; $i < count($dependents); $i++){
            if(isset($dependents[$i]['id'])){
                $id = $dependents[$i]['id'];
                $dependentData = [
                    'applicant_id' => $aplicantid,
                    'first_name' => $dependents[$i]['name'],
                    'last_name' => $dependents[$i]['lastname'],
                    'document_type' => $dependents[$i]['pid_type'],
                    'identity_document' => $dependents[$i]['pid_num'],
                    'relationship' => $dependents[$i]['role'],
                    'birthday' => $dependents[$i]['dob'],
                    'height' => $dependents[$i]['height'],
                    'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                    'weight' => $dependents[$i]['weight'],
                    'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                    'gender' => $dependents[$i]['sex'],
                    'user_id_creation' => $usercreate->id
                ];    

                //var_dump($dependentData);
                $dependent = Dependent::where('id', '=', $id)->update($dependentData);
            }else{
            $dependent = Dependent::create([
                'applicant_id' => $aplicantid,
                'first_name' => $dependents[$i]['name'],
                'last_name' => $dependents[$i]['lastname'],
                'document_type' => $dependents[$i]['pid_type'],
                'identity_document' => $dependents[$i]['pid_num'],
                'relationship' => $dependents[$i]['role'],
                'birthday' => $dependents[$i]['dob'],
                'height' => $dependents[$i]['height'],
                'height_unit_measurement' => ((isset($dependents[$i]['height_unit_measurement'])) && ($dependents[$i]['height_unit_measurement'] == 'M') ) ? 'M' : 'CM',
                'weight' => $dependents[$i]['weight'],
                'weight_unit_measurement' => ((isset($dependents[$i]['weight_unit_measurement'])) && ($dependents[$i]['weight_unit_measurement'] == 'KG') ) ? 'KG' : 'LB',
                'gender' => $dependents[$i]['sex'],
                'user_id_creation' => $usercreate->id
            ]);
            
        }

        }

        return response()->success($aplicantid);
    }
    public function getDependentsinfo($id)
    {
        $dependents = Dependent::find($id);
        $dependents_info = [];
        $dependents_info['dependents'] = $dependents;

        $diagnostics= Diagnostic::where('person_type', 'D')->where('id_person', $id)->where('status', 1)->with('claimDocuments')->get();

        foreach($diagnostics as $diagnostic){
            $amount = 0;
            foreach($diagnostic->claimDocuments as $document){
                if ($document->id_doc_type == 1){
                    $amount =+ $document->amount;
                }
            }
            $dependents_info['claims'][]['id'] = $diagnostic->description;
            $dependents_info['claims'][count($dependents_info['claims'])-1]['amount'] = $amount;
            $dependents_info['claims'][count($dependents_info['claims'])-1]['description'] = $diagnostic->description;
        }

        $tickets = Ticket::where('status', 1)->where('person_type', 'D')->where('id_person', $id)->with('ticketType')->get();
        $dependents_info['tickets'] = $tickets;

        return response()->success($dependents_info);
    }




}
