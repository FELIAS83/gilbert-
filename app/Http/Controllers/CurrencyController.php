<?php

namespace App\Http\Controllers;

use App\User;
use App\Currency;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class CurrencyController extends Controller
{
    /**
     * Get all currencies.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $currencies = Currency::all();
        $currencies = DB::table('currencies')->where('status', '=', '1')
                    ->select(["currencies.*"])
                    ->get();
       
        return response()->success(compact('currencies'));
    }

    /**
     * Create new currency.
     *
     * @return JSON
    */ 
    public function postCurrencies()
    {
        $usercreate = Auth::user();
        
        $currency = Currency::create([
            'name' => Input::get('name'),
            'iso' => Input::get('iso'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('currency');
    }

    /**
     * Get currency details referenced by id.
     *
     * @param int currency ID
     *
     * @return JSON
    */ 
    public function getShow($id)
    {
        $currency = Currency::find($id);
        
        return response()->success($currency);
    }

    /**
     * Update currency data.
     *
     * @return JSON success message
    */ 
    public function putShow(Request $request)
    {
        $currencyForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name',
                'data.iso',
                'data.status'
            )
        );

        $currencyId = intval($currencyForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3',
            'data.iso' => 'required|max:5'
        ]);

        $userupdate = Auth::user();

        $currencyData = [
            'name' => $currencyForm['data.name'],
            'iso' => $currencyForm['data.iso'],
            'status' => $currencyForm['data.status'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Currency::where('id', '=', $currencyId)->update($currencyData);

        return response()->success('success');
    }

    /**
     * Delete currency Data.
     *
     * @return JSON success message
     */
    public function deleteCurrency($id)
    {
        $currencyData = [
            'status' => 0,            
        ];
        $affectedRows = Currency::where('id', '=', $id)->update($currencyData);
        return response()->success('success');
    }
}
