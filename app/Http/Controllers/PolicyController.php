<?php

namespace App\Http\Controllers;

use App\User;
use App\Policy;
use App\Applicant;
use App\PolicyDocument;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;
use App\Configuration;
use App\Amendment;
use App\Annex;
use App\Fee;
use App\FeeDocuments;
use App\Payment;
use App\Attached;
use App\Policystep;
use App\PolicyApplicant;
use App\AgentCommision;
use App\HistoricalRenovation;
use App\PoliciesEffectiveDate;
use App\FeeStep;
use App\Agent;
use App\Commission;
use App\Plan;
use App\Company;
use App\CompanyCommission;
use App\Active;
use App\Http\Controllers\NotificationController;


class PolicyController extends Controller
{
    /**
     * Get all policies.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', $completed)
                    ->select([DB::raw('policies.id as policy_id'), "policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(policies.created_at, '%d/%m/%Y') as created"), DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), "policysteps.id_policy", "policysteps.step1", "policysteps.step2", "policysteps.step3", "policysteps.step4", "policysteps.step5", "policysteps.step6", "policysteps.step7"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('agents', 'policies.agent_id', '=', 'agents.id')
                    ->join('policysteps', 'policies.id', '=', 'policysteps.id_policy')
                    ->get();

        return response()->success(compact('policies'));
    }

   
    /**
     * Create new policy.
     *
     * @return JSON
     */
    public function postPolicies()
    {
        $usercreate = Auth::user();
        $showhidepregnant = 0;        
        if (Input::get('have_prev_safe') == true) {
            $showhidepregnant = 1;
        }
        
        $policy = Policy::create([
           /* 'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'email' => Input::get('email'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),*/
            'agent_id' => Input::get('agent_id'),
            'plan_id' => Input::get('plan_id'),
            'have_prev_safe' => $showhidepregnant,
            'company_name' => Input::get('company_name'),
            'user_id_creation' => $usercreate->id
        ]);

        if ($policy->id) {
            $applicant = Applicant::create([
            'policy_id' => $policy->id,
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'identity_document' => Input::get('identity_document'),
            'email' => Input::get('email'),
            'mobile' => Input::get('mobile'),
            'phone' => Input::get('phone'),
            'user_id_creation' => $usercreate->id
        ]);
        }

        /***************************************************************************/
        if ($policy->id) {
            $steps = Policystep::create([
            'id_policy' => $policy->id,
            'user_id_creation' => $usercreate->id
        ]);
        }
        /***************************************************************************/        

        $directory = Configuration::where('status', '1')->get();
        if (isset($directory[0]->policy_file_directory)){
            $directory = $directory[0]->policy_file_directory;
        }
        else{
           $directory = 0;
        }

        $doctypes = Input::get('id_doc_types');
        $descriptions = Input::get('descriptions');
        $filenames = Input::get('filenames');
        for ($i = 0; $i < count($doctypes) ; $i++ ){
            $policydocument = PolicyDocument::create([
            'id_policy' => $policy->id,
            'id_doc_type' => $doctypes[$i],
            'description' => $descriptions[$i],
            'directory' => 'policies/'.$policy->id,
            'filename' => $filenames[$i],
            'user_id_creation' => $usercreate->id
        ]);
        }

        /********************** Envio de alerta a emisiones **********************************/
                
                //$sala =  "emisiones";
                //$message = "Se ha ingresado una nueva Póliza ID: POL"+$policy->id;
                //NotificationController::notificar($sala, $message);

        /*************************************************************************/


        return response()->success($policy->id);
        return response()->success($directory);



    }

    public function getPolicyInfo(){
        $policy_id = Input::get('policy_id');
       
        // Nota: condicional, en el siguiente cambio utilizar filtros para $provider_id        
        $getPolicyInfo = DB::table('policies') 
            ->where('policies.id', $policy_id)
            ->join('agents', 'policies.agent_id', '=', 'agents.id')
            ->join('plans', 'policies.plan_id', '=', 'plans.id')
            //->select('policies.*', DB::raw("CONCAT(agents.first_name,' ',agents.last_name) as agent"), 'plans.name as plan')
            ->leftJoin('deductibles', 'policies.deductible_id', '=', 'deductibles.id')
            ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
            ->leftJoin('payments', 'policies.id', '=', 'payments.policy_id')
            ->select('policies.*', DB::raw("CONCAT(agents.first_name,' ',agents.last_name) as agent"), 'plans.name as plan', 'deductibles.name as deductible', 'applicants.id as applicant_id', 'applicants.birthday as birthday','applicants.civil_status','payments.mode','payments.discount','payments.discount_percent','payments.total_value','payments.method','payments.payment_date','payments.bank_name','payments.check_number','payments.transfer_number','payments.titular_name','payments.account_type','payments.account_number','payments.deposit_number','payments.titular_lastnames','payments.card_type','payments.card_mark','payments.card_bank','payments.payment_type','payments.id as payment_id', 'payments.interest', 'payments.month')

            ->get();
        //return response()->success($getPolicyInfo);
        $getPolicyInfo[0]->start_date_coverage = date('d-m-Y',strtotime($getPolicyInfo[0]->start_date_coverage));
        $getPolicyInfo[0]->effective_date = date('Y-m-d',strtotime($getPolicyInfo[0]->start_date_coverage));
        $getPolicyInfo[0]->inclusion_date = date('Y-m-d',strtotime($getPolicyInfo[0]->created_at));
        
        $first_value = Fee::where('policy_id',$policy_id)->where('status',1)->first();
        $getPolicyInfo[0]->first_value = (isset($first_value->total)) ? $first_value->total : 0;

        $getPolicyInfo[0]->method = (int) $getPolicyInfo[0]->method;
        $getPolicyInfo[0]->account_type = (int) $getPolicyInfo[0]->account_type;
        $getPolicyInfo[0]->payment_type = (int) $getPolicyInfo[0]->payment_type;
        $getPolicyInfo[0]->card_mark = (int) $getPolicyInfo[0]->card_mark;
        $getPolicyInfo[0]->card_type= (int) $getPolicyInfo[0]->card_type;
        $getPolicyInfo[0]->interest= (int) $getPolicyInfo[0]->interest;
        $getPolicyInfo[0]->month= (int) $getPolicyInfo[0]->month;


       return response()->success(compact('getPolicyInfo'));
    }
     /**
     * Get all Canceled policies.
     *
     * @return JSON
     */
    public function getCanceledPolicy()
    {
        //$completed = ( null !== Input::get('completed')) ? 1 : 0;
        $policies = DB::table('policies')->where('policies.status', '=', '0')->where('policies.motive', '!=', '0')
                    //->where('policies.completed', '=', $completed)
                    ->select([DB::raw('policies.id as policy_id'), "policies.*", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw("date_format(policies.created_at, '%d/%m/%Y') as created"), DB::raw("date_format(policies.start_date_coverage, '%d/%m/%Y') as start_date_coverage"), DB::raw('CONCAT(agents.first_name, " ", agents.last_name) AS agent'), "policysteps.id_policy", "policysteps.step1", "policysteps.step2", "policysteps.step3", "policysteps.step4", "policysteps.step5", "policysteps.step6", "policysteps.step7"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('agents', 'policies.agent_id', '=', 'agents.id')
                    ->join('policysteps', 'policies.id', '=', 'policysteps.id_policy')
                    ->get();

        return response()->success(compact('policies'));
    }

     /**
     * Get Policy details referenced by id.
     *
     * @param int company ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        //$policy = Policy::find($id);
        $policy = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.id', '=', $id)
                    ->select(["policies.*", "applicants.*"])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->get();
        return response()->success($policy);
    }

    /**
     * Update applicant data plan-deductible-coverage .
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
       
                 

        $planForm = array_dot(
            app('request')->only(
                'data.policy_id',
                'data.first_name',
                'data.last_name',
                'data.identity_document',
                'data.email',
                'data.phone',
                'data.mobile',
                'data.plan_id',
                'data.deductible_id',
                'data.coverage_id',
                'data.is_renewal',
                'data.seller_id'
            )
        );

        $policyId = intval($planForm['data.policy_id']);

        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        $userupdate = Auth::user();

        $planData = [
            'first_name' => $planForm['data.first_name'],
            'last_name' => $planForm['data.last_name'],
            'identity_document' => $planForm['data.identity_document'],
            'email' => $planForm['data.email'],
            'phone' => $planForm['data.phone'],
            'mobile' => $planForm['data.mobile'],
            'plan_id' => $planForm['data.plan_id'],
            'deductible_id' => $planForm['data.deductible_id'],
            'coverage_id' => $planForm['data.coverage_id'],
            'agent_id' => $planForm['data.seller_id'],
            'user_id_update' => $userupdate->id,
        ];



        $consulta=DB::table('policies')->where('status','=','1')->where('id','=',$policyId)
                    ->get();

        if ($consulta[0]->plan_id !== $planData['plan_id']){

            $HistoricalRenovation=HistoricalRenovation::create([
            'id_policy' => $policyId,
            'id_plan'=> $planData['plan_id'],
            'id_deductibles'=>$planData['deductible_id'],
            'id_coverages'=>$planData['coverage_id'],
            'user_id_creation'=>$userupdate->id,

        ]);
        }

        $affectedRows = Policy::where('id', '=',  $policyId)->update($planData);

        

        if ($planForm['data.is_renewal'] == true && ($consulta[0]->plan_id !== $planData['plan_id'] || $consulta[0]->deductible_id !== $planData['deductible_id'] || $consulta[0]->coverage_id !== $planData['coverage_id'])){//

            $HistoricalRenovation=HistoricalRenovation::create([
            'id_policy' => $policyId,
            'id_plan'=> $planData['plan_id'],
            'id_deductibles'=>$planData['deductible_id'],
            'id_coverage'=>$planData['coverage_id'],
            'user_id_creation'=>$userupdate->id,

        ]);
        }

        //return response()->success($affectedRows);
        return response()->success($planForm['data.policy_id']);

    }

    /**
     * Delete  Policy.
     *
     * @return JSON
     */

     public function deletePolicy($id)
    {
        $policyData = [
            'status' => 0,            
        ];
        $affectedRows = Policy::where('id', '=', $id)->update($policyData);
        return response()->success('success');
    }

    public function getDates(){
        $currentdate = date("d-m-Y");
        $startdate= date ( 'd-m-Y' , strtotime ( '-3 month' , strtotime ($currentdate) ) );//tres meses atras
        $startdate[0]='0';
        $startdate[1]='1';

        $finaldate= date ( 'd-m-Y' , strtotime ( '+5 month' , strtotime ($currentdate) ) );//cinco meses despues
        $finaldate[0]='1';
        $finaldate[1]='5';

        $date = $startdate;
        $dates = [];
        $dates[0] = $date;

        while ($date <> $finaldate){
            if (substr($date, 0, 2)=='01'){
                $date= date ( 'd-m-Y' , strtotime ( '+14 days' , strtotime ($date) ) );
            }
            else{
                $date= date ( 'd-m-Y' , strtotime ( '+1 month' , strtotime ($date) ) );
                $date[0]='0';
                $date[1]='1';
            }
            $dates[]=$date;
        }

        return response()->success(compact('dates'));
    }

    public function postCoverage(){
        $userupdate = Auth::user();
        $policyId = Input::get('policy_id');
        $startdate = date('Y-m-d',strtotime(Input::get('start_date_coverage')));

        $dateData = [
            'start_date_coverage' => $startdate,
            'user_id_update' => $userupdate->id,
            'continuity_coverage' => (Input::get('continuity_coverage') == true) ? 1 : 0,
            'have_prev_safe' => Input::get('have_prev_safe'),
            'company_name' => (Input::get('have_prev_safe') == true) ?  Input::get('company_name') : ' ',
            'plan_name' => (Input::get('have_prev_safe') == true) ?  Input::get('plan_name') : ' ',
            'is_international' => (Input::get('is_international') == true) ? 1 : 0,
        ];

        $affectedRows = Policy::where('id', '=',  $policyId)->update($dateData);
        return response()->success($policyId);
    }

    public function updatePolicies(Request $request){
        $userupdate = Auth::user();
        //$policyId = Input::get('policyId');
        $policyId = $request['policyId'];
        $dateData = [
            'customer_response' => $request['customer_response']
        ];

        $affectedRows = Policy::where('id', '=',  $policyId)->update($dateData);

        $dataStep = [
            'step3' => 1,
            //'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Policystep::where('id_policy', '=',  $policyId)->update($dataStep);

        return response()->success(compact('affectedRows'));
        //return response()->success($affectedRows);
    }

    public function postConfirm(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $number = $request['number'];
        $amendments = $request['amendments'];
        $annexes = $request['annexes'];
        $deletedamendments = $request['deletedamendments'];
        $deletedannexes = $request['deletedannexes'];
        $updatedamendments = $request['updatedamendments'];
        $updatedannexes = $request['updatedannexes'];
        $total_value = $request['total_value'];
        $effective_date=$request['effective_date'];

        $confirm = Policy::where('id',$policyId)->update(['number' => $number]);
        foreach ($amendments as $value){
            $amendment = Amendment::create([
                'policy_id' => $policyId,
                'role_order' => $value['role_order'],
                'affiliate_id' => $value['affiliate'],
                'type' => $value['type'],
                'description' => $value['description'],
                'user_id_creation' => $user->id
            ]);
        }

        foreach ($updatedamendments as $value){
            $amendment = Amendment::where('id',$value['id'])->update([
                'role_order' => $value['role_order'],
                'affiliate_id' => $value['affiliate_id'],
                'type' => $value['type'],
                'description' => $value['description'],
                'user_id_update' => $user->id
            ]);
        }

        foreach ($deletedamendments as $value){
            $amendment = Amendment::where('id',$value['id'])->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);
        }

        foreach ($annexes as $value){
            if ($value['effective_date']==null){
                $effec_date=$effective_date;
            }
            else
            {
                $effec_date=$value['effective_date'];
            }

            $annex = Annex::create([
                'policy_id' => $policyId,
                'affiliate_id' => $value['affiliate'],
                'annex' => $value['annex'],
                'effective_date' => $effec_date,
                'user_id_creation' => $user->id
            ]);
        }

        foreach ($updatedannexes as $value){
            $annex = Annex::where('id',$value['id'])->update([
                'affiliate_id' => $value['affiliate_id'],
                'annex' => $value['annex'],
                'effective_date' => $value['effective_date'],
                'user_id_update' => $user->id
            ]);
        }

        foreach ($deletedannexes as $value){
            $annex = Annex::where('id',$value['id'])->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);
        }


        Fee::where('policy_id',$policyId)->update(['status' => 0]);

        $fees = Input::get('fees');
        $mode = Input::get('mode');
        $effective_date = Input::get('effective_date');
        $start_coverage_date = $effective_date;
        $amount_tax_ssc = $fees[0]['taxes'][2]['value'];
        foreach ($fees as $key => $fee){
            if ($mode == 1){
                if ($key <> 0){
                    $effective_date= date ( 'Y-m-d' , strtotime ( '+6 month' , strtotime ($effective_date) ) );
                }
            }
            elseif ($mode == 2){
                if ($key <> 0){
                    $effective_date= date ( 'Y-m-d' , strtotime ( '+3 month' , strtotime ($effective_date) ) );
                }
            }
            elseif ($mode == 3){
                if ($key <> 0){
                    $effective_date= date ( 'Y-m-d' , strtotime ( '+1 month' , strtotime ($effective_date) ) );
                }
            }

            $nfee = Fee::create([
                'policy_id' => $policyId,
                'fee_number' => $fee['fee_id'],
                'adminfee' => ($fee['taxes'][0]['value'] <> '') ? 1 : 0,
                'tax_ssc' => Input::get('tax_ssc') ? 1 : 0,
                'amount_tax_ssc' => $amount_tax_ssc,
                'fee_payment_date' => $effective_date,
                'amount_titular' => $fee['details'][0]['value'],
                'amount_spouse' => isset($fee['details'][1]['value']) ? $fee['details'][1]['value'] : 0,
                'amount_childs' => isset($fee['details'][2]['value']) ? $fee['details'][2]['value'] : 0,
                'amount_coverage' => isset($fee['details'][3]['value']) ? $fee['details'][3]['value'] : 0,
                'total' => $fee['total'],
                'user_id_creation' => $user->id
            ]);
        }

        $dataStep = [
            'step2' => 1,
            'user_id_update' => $user->id,
        ];

        $affectedRows = Policystep::where('id_policy', '=',  $policyId)->update($dataStep);

        $payment = Payment::where('policy_id',$policyId)->update([
            'total_value' => $total_value,
            'user_id_update' => $user->id
        ]);

        //creacion de registro de fecha de inicio de cobertura actual para una poliza nueva
        
        PoliciesEffectiveDate::where('policy_id',$policyId)->update(['status' => 0]);
        $policiesEffectiveDate = PoliciesEffectiveDate::create([
                        'start_date_coverage' =>   $start_coverage_date ,
                        'policy_id' => $policyId,
                        'status' => 1,
                        'user_id_creation' => $user->id
        ]);


        return response()->success(compact('confirm'));
    }

    public function postPayment(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $newfiles = $request['newfiles'];
        $filename = $request['filename'];
        $updatedattached = $request['updatedattached'];
        $deletedattached = $request['deletedattached'];
        $descriptions = $request['descriptions'];

        if ($request['form.method'] == 0){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'check_number' => $request['form.check_number'],
                'bank_name' => $request['form.bank_name'],
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 1){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'transfer_number' => $request['form.transfer_number'],
                'bank_name' => $request['form.bank_name'],
                'titular_name' => $request['form.titular_name'],
                'account_type' => $request['form.account_type'],
                'account_number' => $request['form.account_number'],
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 2){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'bank_name' => $request['form.bank_name'],
                'deposit_number' => $request['form.deposit_number'],
                'account_number' => $request['form.account_number'],
                'user_id_update' => $user->id
            ];
        }
        else{
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'titular_name' => $request['form.titular_name'],
                'titular_lastnames' => $request['form.titular_lastnames'],
                'bank_name' => $request['form.bank_name'],
                'card_type' => $request['form.card_type'],
                'card_mark' => $request['form.card_mark'],
                'card_bank' => $request['form.card_bank'],
                'payment_type' => $request['form.payment_type'],
                'interest' => $request['form.interest'],
                'month' => $request['form.month'],
                'user_id_update' => $user->id
            ];
        }

        $fee = Fee::where('policy_id',$policyId)
                            ->where('fee_number', 1)
                            ->orderBy('created_at', 'DESC')
                            ->first();


       for($i = 0; $i < count($newfiles); $i++){
            FeeDocuments::create([
                'fee_id' => $fee->id,
                'id_policy' => $policyId,
                'payment_id' => $payment_id,
                'filename' => $filename[$i],
                'type_id' => $descriptions[$i],
                'user_id_creation' => $user->id
            ]);
        }
        for($i = 0; $i < count($updatedattached); $i++){
            FeeDocuments::where('id',$updatedattached[$i]['id'])->update([
                'fee_id' => $fee->id,
                'type_id' => $updatedattached[$i]['type_id'],
                'filename' => $updatedattached[$i]['filename'],
                'user_id_update' => $user->id
            ]);
        }
        for($i = 0; $i < count($deletedattached); $i++){
            FeeDocuments::where('id',$deletedattached[$i])->update([
                'status' => 0
            ]);
        }

        Attached::where('payment_id',$payment_id)->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);

        Attached::create([
                'payment_id' => $payment_id,
                /*'description' => $description,
                'filename' => $filename,*/
                'user_id_creation' => $user->id
            ]);

        $dataStep = [
            'step4' => 1,
            'user_id_update' => $user->id,
        ];

        Policystep::where('id_policy', '=',  $policyId)->update($dataStep);

        /************************** ************************************/
        $affectedRows = Fee::where('policy_id',$policyId)
                            ->where('fee_number', 1)
                            ->update([
            'paid_fee' => 1,
            'user_id_update' => $user->id
        ]);
        /******************************************************************************/
        $payment_data = Payment::where('policy_id',  $policyId)->first();
        $policyE = PoliciesEffectiveDate::where('policy_id',$policyId)->where('status',1)->first();
        //var_dump($policyE->start_date_coverage);
        $effective_date = $policyE->start_date_coverage;

        $mode = $payment_data->mode;
         //solo para polizas anuales
        if($mode == 0){
            $effective_date= date ( 'Y-m-d' , strtotime ( '+1 year' , strtotime ($effective_date) ) );
            $nfee = Fee::create([
                'policy_id' => $policyId,
                'fee_number' => 1,
                'fee_payment_date' => $effective_date,
                //'amount_titular' => $fee['details'][0]['value'],
                //'amount_spouse' => isset($fee['details'][1]['value']) ? $fee['details'][1]['value'] : 0,
                //'amount_childs' => isset($fee['details'][2]['value']) ? $fee['details'][2]['value'] : 0,
                //'amount_coverage' => isset($fee['details'][3]['value']) ? $fee['details'][3]['value'] : 0,
                //'total' => $fee['total'],
                'user_id_creation' => $user->id
            ]);   
        }
        /******************************************/



        $affectedRows = Payment::where('policy_id',  $policyId)->update($dateData);
        $this->updatePoliciesEffectiveDate($policyId);
        //Fee::where('policy_id',  $policyId)->where('fee_number','=','1')->update($dateData);
        return response()->success(compact('affectedRows'));
    }

    public function postPaymentregister(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $newfiles = $request['newfiles'];
        $filename = $request['filename'];
        $updatedattached = $request['updatedattached'];
        $deletedattached = $request['deletedattached'];
        $descriptions = $request['descriptions'];
        $feeId = $request['feeId'];


        
            /*$dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'titular_name' => $request['form.titular_name'],
                'titular_lastnames' => $request['form.titular_lastnames'],
                'transfer_number' => $request['form.transfer_number'],
                'bank_name' => $request['form.bank_name'],
                'check_number' => $request['form.check_number'],
                'card_type' => $request['form.card_type'],
                'card_mark' => $request['form.card_mark'],
                'card_bank' => $request['form.card_bank'],
                'payment_type' => $request['form.payment_type'],
                'deposit_number' => $request['form.deposit_number'],
                'account_type' => $request['form.account_type'],
                'account_number' => $request['form.account_number'],
                'interest' => $request['form.interest'],
                'month' => $request['form.month'],
                'user_id_update' => $user->id
            ];*/
            /**********************************************/
            if ($request['form.method'] == 0){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'check_number' => $request['form.check_number'],
                'bank_name' => $request['form.bank_name'],
                'titular_name' => "",
                'titular_lastnames' => "",
                'transfer_number' => "",
                'card_type' => 0,
                'card_mark' => 0,
                'card_bank' => 0,
                'payment_type' => 0,
                'deposit_number' => "",
                'account_type' => 0,
                'account_number' => "",
                'interest' => 0,
                'month' => 0,
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 1){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'transfer_number' => $request['form.transfer_number'],
                'bank_name' => $request['form.bank_name'],
                'titular_name' => $request['form.titular_name'],
                'account_type' => $request['form.account_type'],
                'account_number' => $request['form.account_number'],
                'titular_lastnames' => "",
                'check_number' => "",
                'card_type' => 0,
                'card_mark' => 0,
                'card_bank' => 0,
                'payment_type' => 0,
                'deposit_number' => "",
                'interest' => 0,
                'month' => 0,
                'user_id_update' => $user->id
            ];
        }
        elseif ($request['form.method'] == 2){
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'bank_name' => $request['form.bank_name'],
                'deposit_number' => $request['form.deposit_number'],
                'account_number' => $request['form.account_number'],
                'transfer_number' => "",
                'titular_name' => "",
                'account_type' => 0,
                'titular_lastnames' => "",
                'check_number' => "",
                'card_type' => 0,
                'card_mark' => 0,
                'card_bank' => 0,
                'payment_type' => 0,
                'interest' => 0,
                'month' => 0,
                'user_id_update' => $user->id
            ];
        }
        else{
            $dateData = [
                'method' => $request['form.method'],
                'payment_date' => $request['form.payment_date'],
                'titular_name' => $request['form.titular_name'],
                'titular_lastnames' => $request['form.titular_lastnames'],
                'bank_name' => $request['form.bank_name'],
                'card_type' => $request['form.card_type'],
                'card_mark' => $request['form.card_mark'],
                'card_bank' => $request['form.card_bank'],
                'payment_type' => $request['form.payment_type'],
                'interest' => $request['form.interest'],
                'month' => $request['form.month'],
                'account_type' => 0,
                'check_number' => "",
                'deposit_number' => "",
                'account_number' => "",
                'transfer_number' => "",
                'user_id_update' => $user->id
            ];
        }


           /*******************************************/ 
        
         for($i = 0; $i < count($newfiles); $i++){
            $newDoc = FeeDocuments::create([
                //'fee_id' => $feeId,
                'id_policy' => $policyId,
                'payment_id' => $payment_id,
                'filename' => $filename[$i],
                'type_id' => $descriptions[$i],
                'user_id_creation' => $user->id
            ]);

            FeeDocuments::where('id','=', $newDoc->id)->update(['fee_id' => $feeId]); 
        }
        for($i = 0; $i < count($updatedattached); $i++){
            FeeDocuments::where('id',$updatedattached[$i]['id'])->update([
                'fee_id' => $feeId,
                'type_id' => $updatedattached[$i]['type_id'],
                'filename' => $updatedattached[$i]['filename'],
                'user_id_update' => $user->id
            ]);
        }
        for($i = 0; $i < count($deletedattached); $i++){
            FeeDocuments::where('id',$deletedattached[$i])->update([
                'status' => 0
            ]);
        }

        Attached::where('payment_id',$payment_id)->update([
                'status' => 0,
                'user_id_update' => $user->id
            ]);

        Attached::create([
                'payment_id' => $payment_id,
                /*'description' => $description,
                'filename' => $filename,*/
                'user_id_creation' => $user->id
            ]);
    

       /* $dataStep = [
            'step4' => 1,
            'user_id_update' => $user->id,
        ];

        Policystep::where('id_policy', '=',  $policyId)->update($dataStep);

        /************************** ************************************/
       /* $affectedRows = Fee::where('policy_id',$policyId)
                            ->where('fee_number', 1)
                            ->update([
            'paid_fee' => 1,
            'user_id_update' => $user->id
        ]);
        /******************************************************************************/
        /************ fee steps *****/
        $fee = FeeStep::where('fee_id',$feeId)->first();
        if($fee ==  NULL){
            $fee = FeeStep::create([
                'fee_id' => $feeId,
                'policy_id' => $policyId,
                'user_id_creation' => $user->id
            ]);
        }
        $affectedRows = FeeStep::where('id','=',$fee->id)->update([
            'step4' => 1,
            'user_id_update' => $user->id
        ]);
         /******************************/


        $affectedRows = Payment::where('policy_id',  $policyId)->update($dateData);
        //Fee::where('policy_id',  $policyId)->where('fee_number','=','1')->update($dateData);
        return response()->success(compact('affectedRows'));
    }


    public function postConfirmpayment(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $filename = $request['filename'];

        $affectedRows = Attached::where('payment_id',$payment_id)->update([
            'confirm_filename' => $filename,
            'user_id_update' => $user->id
        ]);

        //$policystep = Policystep::find($policyId);
        $policystep = Policystep::where('id_policy',$policyId)->where('step7', 1)->first();
        //return response()->success($policystep);
        if ($policystep) {
            $affectedRows = Policy::where('id',$policyId)->update([
                'completed' => 1,
                'user_id_update' => $user->id
            ]);

            $policy = Policy::find($policyId);
            $agent_id = $policy->agent_id;
            $plan_id = $policy->plan_id;
            $deductible_id = $policy->deductible_id;
            $coverage_id = $policy->coverage_id;

            $payment = Payment::where('status',1)->where('policy_id',$policyId)->first();
            $amount = $payment->total_value;
            $configuration = ActiveCommission::where('id',1)->first();
            $flag = $configuration->active;

            if($flag == 1) //si está activo el pago de comisiones
            {
                $fee = Fee::where('policy_id','=',$policyId)->where('status',1)->where('fee_number','=',1)->first();
                $amount_neto = $fee->amount_titular+$fee->amount_spouse+$fee->amount_childs+$fee->amount_coverage;
                $this->calculateCommission($policyId,$amount_neto);
            }

        }

        $this->updatePoliciesEffectiveDate($policyId);
        return response()->success(compact('affectedRows'));
    }

    public function postInvoice(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $filename = $request['filename'];

        $affectedRows = Attached::where('payment_id',$payment_id)->update([
            'invoice_filename' => $filename,
            'user_id_update' => $user->id
        ]);
        /*$affectedRows = Policy::where('id',$policyId)->update([
            'completed' => 1,
            'user_id_update' => $user->id
        ]);*/

        //$policystep = Policystep::find($policyId);
        $policystep = Policystep::where('id_policy',$policyId)->where('step6', 1)->first();
        //return response()->success($policystep);
        if ($policystep) {
            $affectedRows = Policy::where('id',$policyId)->update([
                'completed' => 1,
                'user_id_update' => $user->id
            ]);

            $policy = Policy::find($policyId);
            $agent_id = $policy->agent_id;
            $plan_id = $policy->plan_id;
            $deductible_id = $policy->deductible_id;
            $coverage_id = $policy->coverage_id;

            $payment = Payment::where('status',1)->where('policy_id',$policyId)->first();
            $amount = $payment->total_value;

            $configuration = Active::where('id',1)->first();
            $flag = $configuration->active;

            if($flag == 1) //si está activo el pago de comisiones
            {
                $fee = Fee::where('policy_id','=',$policyId)->where('status',1)->where('fee_number','=',1)->first();
                $amount_neto = $fee->amount_titular+$fee->amount_spouse+$fee->amount_childs+$fee->amount_coverage;
                $this->calculateCommission($policyId,$amount_neto);
            }


            
       }
        

        return response()->success(compact('affectedRows'));
    }

    /**
     * Get all policies.
     *
     * @return JSON
     */
    public function getCurrentPolicies()
    {
        $getCurrentPolicies = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', '1')
                    ->where(DB::raw("DATE_ADD(policies_effective_dates.start_date_coverage, INTERVAL 1 YEAR)"), '>', date('Y-m-d'))
                    ->where('policies_effective_dates.status','=','1')
                    //->where(DB::raw("DATE_ADD(policies.start_date_coverage, INTERVAL 1 YEAR)"), '>', date('Y-m-d'))
                    ->select(["policies.id", "policies.number", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name')])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('policies_effective_dates', 'policies.id', '=', 'policies_effective_dates.policy_id')
                    ->get();

        return response()->success(compact('getCurrentPolicies'));
    }


    public function postConfirmpaymentfee(Request $request){
        $user = Auth::user();
        $policyId = $request['policyId'];
        $payment_id = $request['payment_id'];
        $filename = $request['filename'];

        $affectedRows = Attached::where('payment_id',$payment_id)->update([
            'confirm_filename' => $filename,
            'user_id_update' => $user->id
        ]);

        $configuration = Configuration::where('status',1)->where('id',5)->first();
        $commision = $configuration->value;

      

        return response()->success(compact('affectedRows'));
    }

    function getPolicyagent($policy_id){

        //$policy_id = Input::get('policy_id');
        $agent = DB::table('policies') 
            ->where('policies.id', $policy_id)
            ->join('agents', 'policies.agent_id', '=', 'agents.id')
            ->select('agents.*')
            ->get();
         return response()->success($agent);   

    }


    /**
     * Get all policies-affiliate.
     *
     * @return JSON
     */
    public function getCurrentaffiliate()
    {
        $first = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', '1')
                    ->where(DB::raw("DATE_ADD(policies_effective_dates.start_date_coverage, INTERVAL 1 YEAR)"), '>', date('Y-m-d'))
                    ->where('policies_effective_dates.status','=','1')
                    ->select(["policies.id", "policies.number", DB::raw('CONCAT(applicants.first_name, " ", applicants.last_name) AS name'), DB::raw('"Titular" as role')])
                    ->join('applicants', 'policies.id', '=', 'applicants.policy_id')
                    ->join('policies_effective_dates', 'policies.id', '=', 'policies_effective_dates.policy_id');
                    //->get();

        $two = DB::table('policies')->where('policies.status', '=', '1')
                    ->where('policies.completed', '=', '1')
                    ->where(DB::raw("DATE_ADD(policies_effective_dates.start_date_coverage, INTERVAL 1 YEAR)"), '>', date('Y-m-d'))
                    ->where('policies_effective_dates.status','=','1')
                    ->select(["policies.id", "policies.number", DB::raw('CONCAT(dependents.first_name, " ", dependents.last_name) AS name'), DB::raw('"Dependiente" as role')])
                    ->join('dependents', 'policies.id', '=', 'dependents.applicant_id')           
                    ->join('policies_effective_dates', 'policies.id', '=', 'policies_effective_dates.policy_id');            

         $currentaffiliate =  $first->union($two)->get();
        //var_dump($getCurrentAffiliate);            
        return response()->success(compact('currentaffiliate'));
    }

    public function updatePoliciesEffectiveDate($policy_id){
        $userupdate = Auth::user(); 
        //$current_year = date ('Y');
        //$current_month = date ('m');   

        $effective_date = DB::table('policies_effective_dates')->where('policy_id','=',$policy_id)->where('status', '=', '1')
                    ->OrderBy('created_at','DESC')
                    ->select(["policies_effective_dates.start_date_coverage"])
                    ->take(1)
                    ->get();
        
        if($effective_date ==  null){

                $policy = DB::table('policies')->where('status','=','1')->where('id','=',$policy_id)
                    ->first();        
                $start_date_coverage = $policy->start_date_coverage;

                PoliciesEffectiveDate::create([
                    'start_date_coverage'  => $start_date_coverage,
                    'policy_id' => $policy_id,
                    'user_id_creation' => $userupdate->id
                ]); 

      }              
      return true;   
    }

    public function calculateCommission($policy_id, $amount){
        $user = Auth::user(); 
        $flag =  false;
        $agents_commission = [];
        $agents_data = array();
        $policy =  Policy::where('id', $policy_id)->select(['agent_id','plan_id','deductible_id','coverage_id'])->first();
        $effective_date = PoliciesEffectiveDate::where('id', $policy_id)->where('status', 1)->select(['start_date_coverage'])->first();
        $agent_principal = Agent::where('id',$policy->agent_id)->first();
        $company = Company::where('id',1)->first();
        $company_commission = $company->commission_percentage;

        $amount_total_commission = ($amount * $company_commission) / 100 ; // calculo de el monto de la comision total a repartir
        //echo "Comision a repartir ".$amount_total_commission. "del monto total ".$amount;
           
        // calculo de comision del agente que ejecuta la venta, se ubica el % de comision en todos los niveles y configuraciones
            $commission = Commission::where('agent_id',$policy->agent_id)->where('plan_id',$policy->plan_id)->first();
            
            if($commission == null || $commission->plan_id == 0 )
            {
                $branch_id = Plan::where('id',$policy->plan_id)->select(['branch_id'])->first();
                $commission = Commission::where('agent_id',$policy->agent_id)->where('branch_id',$branch_id)->first();                
                
                if($commission == null || $commission->branch_id == 0)
                {
                    
                    $commission_agent = $agent_principal->commission;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;        
                                  
                }else
                {
                    $commission_agent = $commission->commission;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                }                
            }else
            {
                $commission_agent = $commission->commission;
                $commission_value = ($amount * $commission_agent) / 100;
                $rest_commission =  $amount_total_commission - $commission_value; 
                $agents_data[0] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
            }
        //*************************************************************************************************************************

        // calculo de comision de los agentes padres,  se ubica el % de comision en todos los niveles y configuraciones   

            $i = 1;    
            while ($agent_principal->leader  > 0) 
            {    
                $agent_principal =  Agent::where('id',$agent_principal->leader)->where('status',1)->first();
                $commission = Commission::where('agent_id',$policy->agent_id)->where('plan_id',$policy->plan_id)->first();
                
                if($commission == null || $commission->plan_id == 0 )
                {
                    $branch_id = Plan::where('id',$policy->plan_id)->select(['branch_id'])->first();
                    $commission = Commission::where('agent_id',$policy->agent_id)->where('branch_id',$branch_id)->first();  
                    
                    if($commission == null || $commission->branch_id == 0)
                    {
                        $commission_agent = $agent_principal->commission;
                        $commission_value = ($amount * $commission_agent) / 100;
                        $rest_commission =  $amount_total_commission - $commission_value; 
                        $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                                  
                    }else
                    {
                        $commission_agent = $commission->commission;
                        $commission_value = ($amount * $commission_agent) / 100;
                        $rest_commission =  $amount_total_commission - $commission_value; 
                        $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;    
                    }                                  
                }
                else
                {
                    $commission_agent = $commission->commission;
                    $commission_value = ($amount * $commission_agent) / 100;
                    $rest_commission =  $amount_total_commission - $commission_value; 
                    $agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                }           
                //$agents_data[$i] =  array('agent_id' =>  $agent_principal->id, 'commission' => $agent_principal->commission) ;
                
                if($agent_principal->leader == 0)
                {
                    $flag = true;
                }

                $i++ ;    
            }
         //***************************************************************************************************************************   
        //var_dump($agents_data);
        $long = sizeof($agents_data);    
        for ($i=0; $i <= $long-1 ; $i++) { 
            if($i == 0)
            {
                $commission_agent = (float)$agents_data[$i]['commission'];       
                $amount_commission_agent = ($amount * $commission_agent) / 100;
                $agents_data_final[$i] = array('agent_id' =>  $agents_data[$i]['agent_id'], 'commission' => $agents_data[$i]['commission'],"commission_real" => $commission_agent,  'amount_commission' => $amount_commission_agent);
            }else if($i > 0 )
            {
                $commission_agent = (float)$agents_data[$i]['commission'] -  (float)$agents_data[$i-1]['commission'];    
                $amount_commission_agent = ($amount * $commission_agent) / 100;
                $agents_data_final[$i] = array('agent_id' =>  $agents_data[$i]['agent_id'], 'commission' => $agents_data[$i]['commission'],"commission_real" => $commission_agent, 'amount_commission' => $amount_commission_agent);
            }
        }    

       $company_commission_real = (float)$company_commission - (float)$agents_data[$long-1]['commission'];
       $amount_commission_company = ($amount * $company_commission_real) / 100;
       $company_commission_data =  array('company_id' => $company->id , 'commission' => $company_commission, "commission_real" => $company_commission_real, 'amount_commission' => $amount_commission_company);  
      
        foreach ($agents_data_final as $key => $value) {
        AgentCommision::create([
                'agent_id' => $agents_data_final[$key]['agent_id'],
                'policy_id' => $policy_id,
                'plan_id' => $policy->plan_id,
                'deductible_id' => $policy->deductible_id,
                'coverage_id' => $policy->coverage_id,
                'commision' => $agents_data_final[$key]['commission_real'],
                'amount' => $agents_data_final[$key]['amount_commission'],
                'effective_date' => $effective_date,
                'user_id_creation' => $user->id
                
            ]);
    }

        CompanyCommission::create([
            'company_id' => $company_commission_data['company_id'],
            'agent_id' => $agents_data_final[0]['agent_id'],
            'policy_id' => $policy_id,
            'plan_id' => $policy->plan_id,
            'deductible_id' => $policy->deductible_id,
            'coverage_id' => $policy->coverage_id,
            'commission' => $company_commission_data['commission_real'],
            'amount' => $company_commission_data['amount_commission'],
            'effective_date' => $effective_date,
            'user_id_creation' => $user->id
        ]);
     
    return true;    
    }



}
