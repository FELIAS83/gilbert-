<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeeDocuments;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Fee;
use DB;

class FeeDocumentsController extends Controller
{
    public function getIndex(Request $request){
        $payment_id = $request->input('payment_id');

        $feedocuments = FeeDocuments::where('payment_id',$payment_id)->where('status', '1')->get();
        foreach ($feedocuments as $value){
            $value->type_id = (int) $value->type_id;
        }
        return response()->success(compact('feedocuments'));
    }

    public function getFeedocuments(Request $request){
        $feeId = $request->input('feeId');
        $feedocuments = FeeDocuments::where('fee_id',$feeId)->where('status', '1')->get();
        foreach ($feedocuments as  $value) {
        	if($value->type_id == 0){
        		$value->type_name = 'Formulario de Autorización';
        	}else{
        		$value->type_name = 'Documento de Identificación';
        	}
        }
        return response()->success(compact('feedocuments'));
    }  

    public function getFeedocs(Request $request){
        $feeId = $request->input('feeId');
        $feedocs = Fee::where('id',$feeId)->where('status', '1')->select([DB::raw('fees.invoice_filename as invoice'),DB::raw('fees.receipt_filename as receipt') ])->get();
        return response()->success(compact('feedocs'));
    }  
}
