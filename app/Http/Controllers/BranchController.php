<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Branch;
use App\User;

use Auth;
use Input;
use DB;

class BranchController extends Controller
{
    //
     /**
     * Get all branchs.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        $branchs = DB::table('branches')->where('status', '=', '1')
                    ->select(["id","name", "company_id"])
                    ->get();

        return response()->success(compact('branchs'));
    }

     public function getBranches()
    {
        $companyId = Input::get('companyId');
        $branches = DB::table('branches')->where('company_id', '=', $companyId)->where('status', '=', '1')
                    ->select(["id","name", "company_id", "commission_percentage"])
                    ->get();

        return response()->success(compact('branches'));
    }
     /**
     * Create new branch.
     *
     * @return JSON
     */
    public function postBranchs()
    {
        $usercreate = Auth::user();
        $branch = Branch::create([
            'name' => Input::get('name'),
            'company_id' => Input::get('companyId'),
            'commission_percentage' => Input::get('commission_percentage'),
            'user_id_creation' => $usercreate->id
        ]);

        return response()->success('branch');
    }

    /**
     * Get branch details referenced by id.
     *
     * @param int branch ID
     *
     * @return JSON
     */
    public function getShow($id)
    {
        $branch = Branch::find($id);  
        return response()->success($branch);
    }

      /**
     * Update branch data.
     *
     * @return JSON success message
     */
    public function putShow(Request $request)
    {
        $userupdate = Auth::user();
        $branchForm = array_dot(
            app('request')->only(
                'data.id',
                'data.name' ,
                'data.commission_percentage'      
            )
        );

        $branchId = intval($branchForm['data.id']);

        $this->validate($request, [
            'data.id' => 'required|integer',
            'data.name' => 'required|min:3'
        ]);


        $branchData = [
            'name' => $branchForm['data.name'],
            'commission_percentage' => $branchForm['data.commission_percentage'],
            'user_id_update' => $userupdate->id,
        ];

        $affectedRows = Branch::where('id', '=', $branchId)->update($branchData);

        return response()->success('success');
    }
}
