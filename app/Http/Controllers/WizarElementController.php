<?php

namespace App\Http\Controllers;

use App\User;
use App\WizarElement;
use Auth;
use Input;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

class WizarElementController extends Controller
{
    /**
     * Get all active Wizar Elements.
     *
     * @return JSON
     */
    public function getIndex()
    {
        
        //$wizar = DB::table('wizar_elements')
        //			->where('status', '1')
        //			->orderBy('order', 'asc')
        //			->get();
    	$wizar = WizarElement::all();
      
        return response()->success(compact('wizar'));
    }
}
