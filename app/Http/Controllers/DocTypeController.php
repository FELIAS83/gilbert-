<?php

namespace App\Http\Controllers;

use App\User;
use App\DocType;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;

use App\Http\Requests;

class DocTypeController extends Controller
{
    /**
     * Get all doc_types.
     *
     * @return JSON
     */
    public function getIndex()
    {
        $doctypes = DB::table('doc_types')->where('status', '=', '1')
                    ->select(["doc_types.*"])
                    ->get();

        return response()->success(compact('doctypes'));
    }
}
