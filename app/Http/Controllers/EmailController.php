<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Classes\PHPMailer\PHPMailer;
use App\MailConfiguration;
use App\ServiceSteps;
use App\EmailFormat;
use App\User;
use App\Applicant;
use App\Agent;
use App\PolicyDocument;
use App\Policy;
use App\Attached;
use App\Payment;
use App\Policystep;
use App\Diagnostic;
use App\Dependent;
use App\DocumentDiagnostic;
use App\Provider;
use App\ClaimDocument;
use App\Claim;
use App\RenewalDocument;
use App\Assistent;
use App\Ticket;
use App\FeeStep;
use App\FeeDocuments;
use Mail;
use Auth;
use Input;
use Validator;

use Dingo\Api\Exception\ValidationHttpException;
use App\Http\Controllers\NotificationController;


class EmailController extends Controller
{

    /*
     * Send mail 
     *
     *
    */
    public function send(Request $request){
        $user= Auth::user();
        $mail = new PHPMailer(true);
        $mailconfiguration = MailConfiguration::find('1');
        $host = $mailconfiguration->host;
        $username = $mailconfiguration->username;
        //$name = $user->name;
        $name = $mailconfiguration->name;
        $password = $mailconfiguration->password;
        $protocol = $mailconfiguration->protocol;
        $port = $mailconfiguration->port;
        //$mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = $protocol;
        $mail->Port = $port;
        $mail->CharSet = "UTF-8";
        $mail->setFrom($username, $name);

        $format_id = $request->input('format_id');

        if (isset($format_id)){
            if ($format_id==1){// Send mail 4 documents
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                $to = $mailformat->recipient;
                $toCC = $mailformat->cc;
                $policy_id = $request->input('policy_id');
                $type = $request->input('type');

                $policy = Policy::find($policy_id);
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_id",$policy_id,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                $content = str_replace("currency_date",$policy->start_date_coverage,$content);

                $payments = Payment::where('policy_id',$policy_id)->first();
                if ($payments->discount == 1){//si aplica descuento
                    $content.= '<BR>'.'Aplicar descuento del '.$payments->discount_percent.'% .';
                }
                if ($payments->invoice_before == 1){//si aplica factura antes
                    $content.= '<BR>'.'Emitir factura antes del pago.';
                }

                $agents = Agent::find($policy->agent_id);
                //foreach ($agents as $value) {
                if (!empty($agents->email)) {
                    $mail->addCC($agents->email);
                }
                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }
                //}
                if ($type == 'renewal'){
                    $policy_document = RenewalDocument::where('id_policy', $policy_id)->where('status', '1')->get();
                    $folder = 'renewals/';
                }else{
                    $policy_document = PolicyDocument::where('id_policy', $policy_id)->where('status', '1')->get();
                    $folder = 'policies/';
                }
              
                foreach ($policy_document as $value) {
                    //return response()->success($policy_document);
                    try{
                        $mail->addAttachment($folder.$policy_id.'/'.$value['filename']);
                    }
                    catch (Exception $e) {
                        $mail->addAttachment($value['directory'].'/'.$value['filename']);
                        return response()->success($value['filename']);
                    }
                }
            }
            elseif ($format_id==2) {// Send mail 4 customer response
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                $to = $mailformat->recipient;
                $toCC = $mailformat->cc;
                $policy_id = $request->input('policy_id');

                $policy = Policy::find($policy_id);

                $policy_number = $policy->number;
                $mail->addCC($user->email);
                $agents = Agent::find($policy->agent_id);
                if (!empty($agents->email)) {
                    $mail->addCC($agents->email);
                }
                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                if ($request->input('answer')==1) {
                    $content = str_replace("<response>","aceptó",$content);
                    $content = str_replace("<resolution>","proceder con la emisión",$content);
                }else{
                    $content = str_replace("<response>","no aceptó",$content);
                    $content = str_replace("<resolution>","proceder con la baja",$content);
                }

            }
            elseif ($format_id==3) {// Send mail 4 customer response
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;

                $policy_id = $request->input('policy_id');

                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;

                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;
                //$cc = $user->email;
                $mail->addCC($user->email);

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                $policies = Policy::where('id', $policy_id)->update(['send_to_reception' => 1,  'user_id_update' => $user->id]);

                $dataStep = [
                    'step5' => 1,
                    'user_id_update' => $user->id,
                ];
                /********************** Envio de alerta a recepcion **********************************/
                
                //$sala =  "recepcion";
                //$message = "Se ha recibido un nuevo documento en Recepción";
                //NotificationController::notificar($sala, $message);

                /*************************************************************************/

                Policystep::where('id_policy', '=',  $policy_id)->update($dataStep);


                if ($request->input('answer')==1) {
                    $content = str_replace("<response>","aceptó",$content);
                    $content = str_replace("<resolution>","proceder con el envio a recepcion",$content);
                }else{
                    $content = str_replace("<response>","no aceptó",$content);
                    $content = str_replace("<resolution>","proceder con la baja",$content);
                }

            }
            elseif ($format_id == 4) {// Send mail 4 authorization_form // disabled
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                $to = $mailformat->recipient;
                $rnp = $request->input('rnp'); 
                $folder = $request->input('folder');
                $toCC = $mailformat->cc;
                $feeId = $request->input('feeId');
                $mail->addCC($user->email);
                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $policy_id = $request->input('policy_id');
                $payment_method_name = $request->input('payment_method_name');
                $payment_method = $request->input('payment_method');
                $card_mark = $request->input('card_mark');
                $card_type= $request->input('card_type');

                $payment_detail = "<br>";


                $payment = Payment::where('policy_id',$policy_id)->first();
                if ($payment_method == 0){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Cheque: '. $payment->check_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 1){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Transferencia: '. $payment->transfer_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 2){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Depósito: '. $payment->deposit_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 3){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Marca de la Tarjeta: '. $card_mark.'<br>';
                    $payment_detail .= 'Tipo de la Tarjeta: '.$card_type.'<br>';

                }
                $mode = $request->input('mode');
                $policy = Policy::find($policy_id);

                $policy_number = $policy->number;

                $to = $mailformat->recipient;


                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_id",$policy_id,$subject);
                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                $content = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$content);
                $content = str_replace("policy_number",$policy_number,$content);
                $content = str_replace("payment_method",$payment_method_name,$content);

                if ($mode == 0){
                    $fees = 1;
                }
                else if ($mode == 1){
                    $fees = 2;
                }
                else if ($mode == 2){
                    $fees = 4;
                }
                else if ($mode == 3){
                    $fees = 12;
                }
                $content = str_replace("fees",$fees,$content);
                $content = str_replace("payment_detail",$payment_detail,$content);

                if ($rnp == true){
                    $filename = FeeDocuments::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
                    $mail->addAttachment('payment_record/'.$policy_id.'/'.$filename->filename);
                }
                
                else{
                    if(!isset($feeId)){
                        $filenames = FeeDocuments::where('payment_id',$request->input('payment_id'))->where('status',1)->get();
                        foreach ($filenames as $filename){
                        $mail->addAttachment($folder.$policy_id.'/'.$filename->filename);
                        }
                    }else{
                        $filenames = FeeDocuments::where('fee_id',$feeId)->where('status',1)->get();
                        foreach ($filenames as $filename){
                        $mail->addAttachment($folder.$policy_id.'/'.$feeId.'/'.$filename->filename);
                        }
                    }

                }

            }
            elseif ($format_id == 5) {// Send mail 4 confirm payment
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;

                $policy_id = $request->input('policy_id');
                $payment_method_name = $request->input('payment_method_name');
                $payment_method = $request->input('payment_method');
                $card_mark = $request->input('card_mark');
                $card_type= $request->input('card_type');

                $payment_detail = "<br>";

                /*$payment = Payment::where('policy_id',$policy_id)->first();
                if ($payment_method == 0){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Cheque: '. $payment->check_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 1){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Transferencia: '. $payment->transfer_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 2){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Número de Depósito: '. $payment->deposit_number.'<br>';
                    $payment_detail .= 'Banco: '.$payment->bank_name.'<br>';

                }
                elseif ($payment_method == 3){
                    $payment_detail .= 'Fecha de Pago: '.date('d-m-Y',strtotime($payment->payment_date)).'<br>';
                    $payment_detail .= 'Marca de la Tarjeta: '. $card_mark.'<br>';
                    $payment_detail .= 'Tipo de la Tarjeta: '.$card_type.'<br>';

                }
                $mode = $request->input('mode');*/
                $policy = Policy::find($policy_id);

                $policy_number = $policy->number;

                $agents = Agent::find($policy->agent_id);
                //$to = $mailformat->recipient;
                $to = $agents->email;
                $mail->addCC($user->email);

                $toCC = $mailformat->cc;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_id",$policy_id,$subject);
                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                $content = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$content);
                $content = str_replace("policy_number",$policy_number,$content);
                $content = str_replace("payment_method",$payment_method_name,$content);
                /*
                if ($mode == 0){
                    $fees = 1;
                }
                else if ($mode == 1){
                    $fees = 2;
                }
                else if ($mode == 2){
                    $fees = 4;
                }
                else if ($mode == 3){
                    $fees = 12;
                }
                $content = str_replace("fees",$fees,$content);*/
                $content = str_replace("payment_detail",$payment_detail,$content);

                $filename = Attached::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
                $mail->addAttachment('authorization_form/'.$policy_id.'/'.$filename->confirm_filename);

                $dataStep = [
                    'step6' => 1,
                    'user_id_update' => $user->id,
                ];

                Policystep::where('id_policy', '=',  $policy_id)->update($dataStep);
            }
            elseif ($format_id == 6) {// Send mail 4 invoice
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                
                $toCC = $mailformat->cc;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $mail->addCC($user->email);
                $policy_id = $request->input('policy_id');

                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;

                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                $filename = Attached::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
                //return response()->success($filename);
                 if ($filename){
                    $mail->addAttachment('authorization_form/'.$policy_id.'/'.$filename->invoice_filename);
                }
                else{
                    $invoice_filename = $request->input('invoice_filename');
                    $mail->addAttachment('authorization_form/'.$policy_id.'/'.$invoice_filename);
                }


                $dataStep = [
                    'step7' => 1,
                    'user_id_update' => $user->id,
                ];

                Policystep::where('id_policy', '=',  $policy_id)->update($dataStep);
            }
            elseif ($format_id==7) {// Send mail 4 pending docs
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $request->input('content').'<BR>';
                $toCC = $mailformat->cc;

                $policy_id = $request->input('policy_id');
                $claimId = $request->input('claim_id');

                $policy = Policy::find($policy_id);
                //return response()->success($policy_id);
                $policy_number = $policy->number;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;
                //$cc = $user->email;
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                $policies = Policy::where('id', $policy_id)->update(['send_to_reception' => 1,  'user_id_update' => $user->id]);

                $document_diagnostics = $request->input('diagnostics');
                foreach($document_diagnostics as $value){
                    foreach($value['documents'] as $value2){
                        if ($value2['checked'] == true){
                            $content .= $value2['filename'].'<BR>';
                            $pending_doc = 1;
                            $mail->addAttachment('claims/'.$claimId.'/'.$value2['filename']);
                        }
                        else{
                            $pending_doc = 0;
                        }
                        DocumentDiagnostic::where('id',$value2['id'])->update([
                            'pending_doc' => $pending_doc
                        ]);
                    }
                }

                /*if ($request->input('answer')==1) {
                    $content = str_replace("<response>","aceptó",$content);
                    $content = str_replace("<resolution>","proceder con el envio a recepcion",$content);
                }else{
                    $content = str_replace("<response>","no aceptó",$content);
                    $content = str_replace("<resolution>","proceder con la baja",$content);
                }*/

            }
            elseif ($format_id == 8) {// Send mail 4 return docs, cambiar por un 8
                $policy_id = $request->input('policy_id');
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;
                $applicant = Applicant::where ('policy_id', $policy_id)->get();                

                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                $title = $mailformat->title;
                $content = $mailformat->content;
                
                /*$toCC = $mailformat->cc;
                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }
                */

                $claim_id = $request->input('claim_id');
                $returnfiles = $request->input('returnfiles');
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;
                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $content .= '<ol>';

                foreach ($returnfiles as $file){
                    if ($file['return'] == true){
                        $content .= '<li>Tipo de Documento: '.$file['type'].', Descripción:  '.$file['description'].', Archivo: '.$file['filename'].'<br> </li>';
                        $mail->addAttachment('claims/'.$claim_id.'/'.$file['filename']);

                        ClaimDocument::where('id',$file['id'])->update(['returned' => 1]);
                    }
                }
                 //contar si todos los docs estan devueltos para cambiar el step de claim
                $claim_documents = ClaimDocument::where('id_claim',$claim_id)->count();
                $claim_documents_returned = ClaimDocument::where('id_claim',$claim_id)->where('returned',1)->count();

                if ($claim_documents == $claim_documents_returned){
                    //cambiar step
                    Claim::where('id',$claim_id)->update([
                        'step' => '2.5'
                    ]);
                }
                $content .= '</ol>';
            }
            elseif ($format_id==9) {// Send mail 4 pending docs
                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $mailformat->content;
                $toCC = $mailformat->cc;

                $policy_id = $request->input('policy_id');

                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                    }
                }

                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;
                //$cc = $user->email;
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                $content = str_replace("policy_number",$policy_number,$content);
                $content = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$content);
                $policies = Policy::where('id', $policy_id)->update(['user_id_update' => $user->id]);
                $filename = Attached::where('payment_id',$request->input('payment_id'))->where('status',1)->first();
                $mail->addAttachment('authorization_form/'.$policy_id.'/'.$filename->invoice_filename);
            }
            elseif ($format_id == 10) {// Send mail 4 claim dispatch
                $policy_id = $request->input('policy_id');
                $claim_id = $request->input('claim_id');
                $name = $request->input('name');
                $mailformat = EmailFormat::find($format_id);

                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;

                $applicant = Applicant::where ('policy_id', $policy_id)->first();

                $subject = $mailformat->subject;
                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$subject);

                $title = $mailformat->title;
                $content = $mailformat->content;
                $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $title .= "<p align='right'>Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y')."</p>";

                $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                                <thead>
                                    <tr>
                                        <th>Fecha Enviado</th>
                                        <th>#GB</th>
                                        <th>Titular</th>
                                        <th># Póliza</th>
                                        <th>Paciente</th>
                                        <th>Diagnóstico</th>
                                        <th>Monto</th>
                                    </tr>
                                </head>
                                <tbody>";
                $diagnostics = Diagnostic::where('status',1)->where('claim_id',$claim_id)->get();

                foreach($diagnostics as $value){
                    $amount = 0;
                    $document = DocumentDiagnostic::with('diagnostics')->where('status',1)->where('diagnostic_id',$value->id)->get();

                    foreach ($document as $value2){
                        $amount+= $value2->diagnostics->amount;
                    }

                    if ($value->person_type == 'A'){
                        $content.="<tr>
                            <td>".date('d-m-Y')."</td>
                            <td>".$claim_id."</td>
                            <td>".$name."</td>
                            <td>".$policy_id."</td>
                            <td>".$name."</td>
                            <td>".$value->description."</td>
                            <td>".$amount."</td>
                        </tr>";
                    }
                    else{
                        $dependent = Dependent::where('id',$value->id_person)->first();
                        $content.="<tr>
                            <td>".date('d-m-Y')."</td>
                            <td>".$claim_id."</td>
                            <td>".$name."</td>
                            <td>".$policy_id."</td>
                            <td>".$dependent->first_name.' '.$dependent->last_name."</td>
                            <td>".$value->description."</td>
                            <td>".$amount."</td>
                        </tr>";
                    }
                }

                $content.=" </tbody>
                            </table>";

                $content .= '<BR>Detalles de los Documentos<BR>';
                $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                                <thead>
                                    <tr>
                                        <th>Facturas</th>
                                        <th>Proveedor</th>
                                        <th>Valor</th>
                                    </tr>
                                </head>
                                <tbody>";
                $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
                ->whereHas('diagnostics2', function ($query) use ($claim_id) {
                    $query->where('claim_id', $claim_id);
                })
                ->whereHas('diagnostics', function ($query2)  {
                    $query2->where('id_doc_type', 1);
                })->groupBy('document_id')->get();

                $total = 0;
                foreach ($document as $value){
                    $provider = Provider::where('id',$value->diagnostics->provider_id)->first();
                    $content.="<tr>
                            <td>".$value->diagnostics->description."</td>
                            <td>".$provider->name."</td>
                            <td>".$value->diagnostics->amount."</td>
                        </tr>";
                    $total += $value->diagnostics->amount;
                }
                $content.="<tr>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td>$total</td>
                            </tr>
                            </tbody>
                            </table>";

                $content.= "Sin otro particular por el momento,sucribo con un cordial saludo. <BR>";
                $content .= '<BR>Documentos Adicionales<BR>';
                $content .= "<table border='1' cellspacing=0 cellpadding=2 bordercolor='666633'>
                                <thead>
                                    <tr>
                                        <th>Tipo de Documento</th>
                                        <th>Descripción</th>
                                    </tr>
                                </head>
                                <tbody>";
                $document = DocumentDiagnostic::where('status',1)->with('diagnostics')->with('diagnostics2')
                ->whereHas('diagnostics2', function ($query) use ($claim_id) {
                    $query->where('claim_id', $claim_id);
                })
                ->whereHas('diagnostics', function ($query2)  {
                    $query2->where('id_doc_type', '<>', 1);
                })->groupBy('document_id')->get();

                foreach ($document as $value){
                    if ($value->diagnostics->id_doc_type == 1){
                        $doc_type = 'Factura';
                    }
                    elseif ($value->diagnostics->id_doc_type == 2){
                        $doc_type = 'Informe';
                    }
                    elseif ($value->diagnostics->id_doc_type == 3){
                        $doc_type = 'Orden';
                    }
                    else{
                        $doc_type = 'Resultado';
                    }
                    $content.="<tr>
                            <td>".$doc_type."</td>
                            <td>".$value->diagnostics->description."</td>
                        </tr>";
                }
                $content.="</tbody>
                            </table>";

                $content.= '<BR> <BR> Atentamente, <BR> <BR> <BR> <strong>Monica Chiquito </strong>';

                $to = $mailformat->recipient;

                $toCC = $mailformat->cc;
                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }
            }
            elseif ($format_id==11) {// Solicitar carta de garantia Tickets

                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content_form = $request->input('content');
                //$content= $mailformat->content;
                $toCC = $mailformat->cc;
                $file = $request->input('file');
                $filename=$request['filename'];    
                $policy_id = $request->input('policy_id');
                $ticket_id = $request->input('ticket_id');
                $public = public_path();
                $folder='/tickets/';
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;
                $speciality=$request->input('speciality');
                $hospital_name= $request->input('hospital');
                $date_start= $request->input('date_start');
                $date_end = $request->input('date_end');
                $hour= $request->input('hour');
                $doctor= $request->input('doctor');
                $person= $request->input('person');
                $data= $request->input('data');

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $to = $mailformat->recipient;
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                
                $content = $content_form."<br>".$data;

                if ($filename !==null){
                    $mail->addAttachment(public_path().$folder.$ticket_id.'/'.$filename);
                }
                //adjunta documentos almacenados en la creacion del ticket//
                $directorio = opendir("$public$folder$ticket_id"); //ruta actual
                while (($archivo = readdir($directorio))!== false) //obtenemos un archivo y luego otro sucesivamente
                {
                    if (!(is_dir($archivo)))//verificamos si es o no un directorio
                    {
                        $mail->addAttachment(public_path().$folder.$ticket_id.'/'.$archivo);
                    }
    
                }
             
                closedir($directorio);
                Ticket::where('id', $ticket_id)->update([
                    'guarantee_letter' =>  '1'
                ]);
            }
            elseif ($format_id == 12) {// Send mail 4 claim dispatch
                
                $policy_id = $request->input('policy_id');
                $claim_id = $request->input('claim_id');
                $allClaims = $request->input('allClaims');
                $amountDeduc = $request->input('amountDeduc');
                $discount = $request->input('discount');
                $cover = $request->input('cover');
                $dependents_name = $request->input('insured_name');
                $eob = $request->input('eob');
                $observation = $request->input('observation');
                $bank= $request->input('bank');
                $check_number=$request->input('check_number');
                $amount=$request->input('amount');
                $payment_type_id=$request->input('payment_type_id');
                $indice=$request->input('indice');
                $provider = $request->input('provider');
                $file = $request->input('myFile');
                $folder='/eob/';


                $diagnostic_id = $request->input('diagnostic_id');

                if ($payment_type_id == 1)
                {
                    $payment_type='Cheque';
                }
                else{
                    $payment_type='Transferencia';
                }

                if (!empty($observation)) {
                       $observation = "<strong>Observación: </strong>".$observation."<br/><br/>" ;
                }
                $mailformat = EmailFormat::find($format_id);
                $toCC = $mailformat->cc;
                 if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;

                $applicant = Applicant::where ('policy_id', $policy_id)->first();
                $applicant_name = $applicant->first_name.' '.$applicant->last_name;
                $to = $applicant->email;
                                
                 
                $subject = $mailformat->subject;
                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$subject);

                $title =  $mailformat->title; 
                

                $content = $mailformat->content;
                $content = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$content);
                $content = str_replace("policy_number",$policy_number,$content);
                $content = str_replace("dependents_name",$dependents_name,$content);
                $mes = array("Enero","Febrero","Marzo",'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                $current_date = "<p align='right'>Guayaquil, ".date('d')." de ".$mes[date('n')-1]." de ".date('Y')."</p>";
                $title = str_replace("applicant_name",$applicant->first_name.' '.$applicant->last_name,$title);
                $title = str_replace("current_date",$current_date,$title);

                $longI=count($indice);
                $longC=count($allClaims);

                for ($i=0; $i < $longI ; $i++) { 

                    if ($indice[$i] !== null){
             
                        unset($allClaims[$i]);
                        unset($amountDeduc[$i]);
                        unset($discount[$i]);
                        unset($cover[$i]);
                        unset($eob[$i]);
                        unset($provider[$i]);

                    }     
                }
                $allClaims=array_values($allClaims);
                $amountDeduc=array_values($amountDeduc);
                $discount=array_values($discount);
                $cover=array_values($cover);
                $eob=array_values($eob);
                $provider=array_values($provider);


                 $totalCount = 0 ;
                 $body_table = "";
                 for($i = 0; $i < count($allClaims); $i++){
                    $body_table.="<tr>
                            <td style='text-align: center;'>".$eob[$i]['eob']."</td>
                            <td style='text-align: center;'> $".$allClaims[$i]['amount']."</td>
                            <td style='text-align: center;'> $".$amountDeduc[$i]."</td>
                            <td style='text-align: center;'> $".$discount[$i]."</td>
                            <td style='text-align: center;'> $".$cover[$i]."</td>
                            <td style='text-align: center;'> $".$provider[$i]."</td>
                            <td style='text-align: center;'> $".$allClaims[$i]['total']."</td>
                        </tr>";
                     $totalCount += $allClaims[$i]['total'];
                 }
                
                $content = str_replace("body_table",$body_table,$content);
                $content = str_replace("observation",$observation,$content);
                $content = str_replace("total_count",$totalCount,$content);
                if($payment_type_id == 1){
                    $content = str_replace("bank",$bank,$content);
                    $content = str_replace("check_number",$check_number,$content);
                 }else{
                    $content = str_replace("cheque No. check_number, del Banco bank","transferencia bancaria",$content);
                }
                $content = str_replace("amount",$amount,$content);
                $content = str_replace("payment_type",$payment_type,$content);

                foreach ($file as $value) {
                    try{
                        $mail->addAttachment(public_path().$folder.$diagnostic_id.'/'.$value);
                    }
                    catch (Exception $e) {
                        $mail->addAttachment(public_path().$folder.$diagnostic_id.'/'.$value);
                    }
                }

            }
            elseif ($format_id==13) {// Solicitud de Cita Tickets

                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $request->input('content');
                $content = $mailformat->content;
                $toCC = $mailformat->cc;
                $file = $request->input('file');
                $filename=$request['filename'];    
                $policy_id = $request->input('policy_id');
                $ticket_id = $request->input('ticket_id');
                $folder='/tickets/';
                $public = public_path();
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;
                $speciality=$request->input('speciality');
                $hospital_name= $request->input('hospital');
                $date_start= $request->input('date_start');
                $date_end = $request->input('date_end');
                $hour= $request->input('hour');
                $doctor= $request->input('doctor');
                $person= $request->input('person');
                $data= $request->input('data');

                if (!empty($toCC)) {
                    $myArray = explode(',', $toCC);
                    foreach($myArray as $my_Array){
                        $mail->addCC($my_Array);
                    }
                }

                $to = $mailformat->recipient;
                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);
                if ($doctor == null){
                    $doctor = " ";
                }

                if ($hour == null){
                    $hour = " ";
                }

                if ($date_end == null){
                    $date_end = " ";
                }
                else{
                    $date_end =" - ".$date_end; 
                }

                if ($speciality == null){
                    $speciality = " ";
                } 

                $content .= $data;             


                if ($filename !==null){
                    $mail->addAttachment(public_path().$folder.$ticket_id.'/'.$filename);
                }
                //adjunta documentos almacenados en la creacion del ticket//
                $directorio = opendir("$public$folder$ticket_id"); //ruta actual
                while (($archivo = readdir($directorio))!== false) //obtenemos un archivo y luego otro sucesivamente
                {
                    if (!(is_dir($archivo)))//verificamos si es o no un directorio
                    {
                        $mail->addAttachment(public_path().$folder.$ticket_id.'/'.$archivo);
                    }
    
                }
             
                closedir($directorio);
                Ticket::where('id', $ticket_id)->update([
                    'request_medical' =>  '1'
                ]);

            }
            elseif ($format_id==14) {//Aviso de Renovacion

                $mailformat = EmailFormat::find($format_id);
                $subject = $mailformat->subject;
                $title = $mailformat->title;
                $content = $request->input('content');
                //$content = $mailformat->content;
                //$toCC = $mailformat->cc;
                $fee_id= $request->input('fee_id');
                $file = $request->input('filename');
                $filename=$request['filename'];    
                $policy_id = $request->input('policy_id');
                $folder='/renewal_notifications/';
                $public = public_path();
                $policy = Policy::find($policy_id);
                $policy_number = $policy->number;


                $agents = Agent::find($policy->agent_id);
                $to = $agents->email;
                foreach ($agents as $agents) {
                    if (!empty($agents->email)) {
                        $mail->addCC($agents->email);
                        $to = $agents->email;
                    }
                }

                
                $assistents = Assistent::where('agent_id',$policy->agent_id)->get();
                foreach($assistents as $assistent){
                    if (!empty($assistent->email)) {
                        $mail->addCC($assistent->email);
                        $toCC=  $assistent->email;
                    }
                }


                $mail->addCC($user->email);
                $applicant = Applicant::where ('policy_id', $policy_id)->get();

                $subject = str_replace("policy_number",$policy_number,$subject);
                $applicant_name= $applicant[0]->first_name.' '.$applicant[0]->last_name;
                $subject = str_replace("applicant_name",$applicant[0]->first_name.' '.$applicant[0]->last_name,$subject);

                //$content = $content

                if ($file !== null)
                {    

                    foreach ($file as $value) {
                        try{
                            $mail->addAttachment(public_path().$folder.$policy_id.'/'.$value);
                        }
                        catch (Exception $e) {
                            $mail->addAttachment(public_path().$folder.$policy_id.'/'.$value);
                        }
                    }
                }

                /*if ($filename !== null){
                    $mail->addAttachment(public_path().$folder.$policy_id.'/'.$filename);
                }*/

                //adjunta documentos almacenados en la creacion del aviso de renovacion//
                    $directorio = opendir("$public$folder$policy_id"); //ruta actual

                    while (($archivo = readdir($directorio))!== false) //obtenemos un archivo y luego otro sucesivamente
                    {
                        if (!(is_dir($archivo)))//verificamos si es o no un directorio
                        {
                            $mail->addAttachment(public_path().$folder.$policy_id.'/'.$archivo);
                        }
        
                    }
                    
                    closedir($directorio);
                    $userupdate = Auth::user();

                    $dataupdate = [
                        'step1' => 1,
                        'user_id_update' => $userupdate->id,
                    ];

                    $fee_step= FeeStep::where('policy_id', $policy_id)->where('fee_id',$fee_id)->update($dataupdate);
                
            }



        }

        try {
                $mail->addAddress($to);
                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = $title.$content;
                //$mail->Body    = $title.'<br>'.$content_form.'<br>'.$data;
                $mail->send();
                $message = 'Successfully event send';
            }
            catch (Exception $e) {
                $message = 'error to send,'.$mail->ErrorInfo;
            }

        return response()->json(['message' => $message]);
       //return response()->success($applicant);
    }


    public function deleteFiles(Request $request) {///elimina el archivo adjunto en solicitud de carta de garantia, en modulo Tickets
        $folder='/tickets/';
        $ticket_id = $request->input('ticket_id');
        $filename=$request['filename'];

            if(\File::exists(public_path().$folder.$ticket_id.'/'.$filename))
            {
            
            \File::delete(public_path().$folder.$ticket_id.'/'.$filename);
            }
    }  




    public function sendMasive(Request $request){


        $mail = new PHPMailer(true);
        $mailconfiguration = MailConfiguration::find('1');
        $host = $mailconfiguration->host;
        $username = $mailconfiguration->username;
        $name = $mailconfiguration->name;
        $password = $mailconfiguration->password;
        $protocol = $mailconfiguration->protocol;
        $port = $mailconfiguration->port;
        //$mail->SMTPDebug = 2;
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = $protocol;
        $mail->Port = $port;
        $mail->CharSet = "UTF-8";
        $mail->setFrom($username, $name);




        $title = $request->input('title');
        $content = $request->input('content');
        $to = $request->input('to');
        $subject = $request->input('subject');
        

        /*$resultado = str_replace('":true', ",", $to);
        $resultado = str_replace('"', "", $resultado);
        $resultado = str_replace('{', "", $resultado);
        $resultado = str_replace('}', "", $resultado);
        $resultado = str_replace(',,', ",", $resultado);
        $resultado = substr($resultado, 0, -1);

        $tos = explode(',',$resultado);*/
        for ($i=0 ; $i<count($to) ; $i++)
        {

            $mail->addAddress($to[$i]);




        }
        try {

                    $mail->isHTML(true);
                    $mail->Subject = $subject;
                    $mail->Body    = $title.'<BR>'. $content;
                    $mail->send();
                    $message = 'sendmasive successfully';

                }
                catch (Exception $e) {

                    $message .= 'error send masive'.$mail->ErrorInfo;
                }
        return response()->json(['message' => $message]);
    }

    public function getListEvents()
    {
        $eventos = EventosCorreo::all();//nombre del modelo
        return response()->success(compact('eventos'));
    }

    public function showEvent($id)
    {
        $eventos = EventosCorreo::find($id);
        return response()->success(compact('eventos'));
    }

    public function updateStatus($id,Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $evento = EventosCorreo::find($id)->update(['activo'=>$request->input('id')]);
        return response()->success($evento);
    }

    public function updateEvent($id,Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $evento = EventosCorreo::find($id)->update(['subject'=>$request->input('subject'),
                                                   'titulo'=>$request->input('title'),
                                                   'contenido'=>$request->input('content')]);
        return response()->success($evento);
    }

    public function trymail(Request $request){
        $mail = new PHPMailer(true);
        $host = $request->input('host');
        $username = $request->input('username');
        $password = $request->input('password');
        $protocol = $request->input('protocol');
        $port = $request->input('port');
        $name = $request->input('name');
        try {

            //$mail->SMTPDebug = 2;
            $mail->isSMTP();

            $mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

                $mail->Host = $host;
                $mail->SMTPAuth = true;
                $mail->Username = $username;
                $mail->Password = $password;//auiiuwjrkinqqrkq no borrar
                $mail->SMTPSecure = $protocol;
                $mail->Port = $port;
                $mail->CharSet = "UTF-8";

                $mail->setFrom($username, $name);
                $mail->addAddress('elyohan14@gmail.com');

                $mail->isHTML(true);
                $mail->Subject = 'Correo configurado exitosamente';
                $mail->Body    = 'Avenida Legal'.'<BR>'. 'Todo fino';
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                return response()->json(['message' => 'Message send']);
            }
            catch (Exception $e) {
                return response()->json(['message' => 'error' ]);
            }



    }

}


//configuracion recomendada para gmail
//*permitir acceso de aplicaciones menos seguras
//*generar contraseña de aplicacion
//host = smtp.gmail.com
//contraseña = la generada anteriormente
//protocolo = TLS
//puerto = 587

