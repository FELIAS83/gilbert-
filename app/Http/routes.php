<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'AngularController@serveApp');
    Route::get('/unsupported-browser', 'AngularController@unsupported');
    Route::get('user/verify/{verificationCode}', ['uses' => 'Auth\AuthController@verifyUserEmail']);
    Route::get('auth/{provider}', ['uses' => 'Auth\AuthController@redirectToProvider']);
    Route::get('auth/{provider}/callback', ['uses' => 'Auth\AuthController@handleProviderCallback']);
    Route::get('/api/authenticate/user', 'Auth\AuthController@getAuthenticatedUser');
});

$api->group(['middleware' => ['api']], function ($api) {
    $api->controller('auth', 'Auth\AuthController');

    // Password Reset Routes...
    $api->post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    $api->get('auth/password/verify', 'Auth\PasswordResetController@verify');
    $api->post('auth/password/reset', 'Auth\PasswordResetController@reset');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->get('users/me', 'UserController@getMe');
    $api->put('users/me', 'UserController@putMe');
});

$api->group(['middleware' => ['api', 'api.auth', 'role:admin.super|admin.user']], function ($api) {
    $api->controller('users', 'UserController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('companies', 'CompanyController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('sellers', 'SellerController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('agents', 'AgentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('countrys', 'AddressCountryController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('states', 'AddressProvinceController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('cities', 'AddressCityController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('subagents', 'SubagentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('agentsubs', 'AgentSubagentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('plans', 'PlanController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('deductibles', 'DeductibleController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('deductible_details', 'DeductibleDetailController');
    $api->get('amounts/{deductible_id}/{age}/{ageh}/{childs}/{coverage_id}/{PolicyId}', 'DeductibleDetailController@getAmounts');
    $api->get('amountsanual/{deductible_id}/{age}/{ageh}/{childs}/{coverage_id}/{PolicyId}', 'DeductibleDetailController@getAmountsanual');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('coverages', 'CoverageController');
});
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('coverage_details', 'CoverageDetailController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('doctypes', 'DocTypeController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('policies', 'PolicyController');
    $api->get('getPolicyInfo', 'PolicyController@getPolicyInfo');
    $api->get('getCurrentPolicies', 'PolicyController@getCurrentPolicies');
    $api->get('getCanceledPolicy', 'PolicyController@getCanceledPolicy');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('claims', 'ClaimController');
    $api->post('closedClaim', 'ClaimController@closedClaim');    
    $api->get('showdocinfo', 'ClaimController@getShowDocInfo');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('messengers', 'MessengerController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('medicalspecialties', 'MedicalSpecialityController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('assistents', 'AssistentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('wizar', 'WizarElementController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('applicants', 'ApplicantController');
    $api->get('getActiveClients', 'ApplicantController@getActiveClients');
    $api->get('getInactiveClients', 'ApplicantController@getInactiveClients');
    $api->get('applicant_data/{id}', 'ApplicantController@getApplicantData');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('dependents', 'DependentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('quiz', 'QuizController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('quizdet', 'QuizDetailController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('doctors', 'DoctorController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('hospitals', 'HospitalController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('configurations', 'ConfigurationController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('policydocuments', 'PolicyDocumentController');
    $api->post('/send', 'EmailController@send');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('currencies', 'CurrencyController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('providers', 'ProviderController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('medicalinformations', 'MedicalInformationController');
    $api->put('putMedicalInfo', 'MedicalInformationController@putMedicalInfo');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('payments', 'PaymentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('diagnostics', 'DiagnosticController');
    $api->get('reportsorders/{claimId}/{affiliateId}', 'DiagnosticController@getReportsorders');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('mail_configurations', 'MailConfigurationController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('mail_formats', 'MailFormatController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('email_formats', 'EmailFormatController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('massivemails', 'MassivemailController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('amendments', 'AmendmentController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('annexes', 'AnnexController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('attacheds', 'AttachedController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('feedocuments', 'FeeDocumentsController');
});
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('documents_diagnostic', 'DocumentDiagnosticController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('tickets', 'TicketController');
});

/*$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
>>>>>>> dd7fa7877aed45124f8ee7524d65499e8086fe4e
   $api->controller('policyapp', 'PolicyApplicantController');
});*/

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('renewalnotificationdocs', 'RenewalNotificationDocumentsController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('renewals', 'RenewalController');
   $api->get('getExpiredPolicies', 'RenewalController@getExpiredPolicies');
   $api->post('MotiveRenewal', 'RenewalController@MotiveRenewal');
   $api->get('getPolicyInfoRen', 'RenewalController@getPolicyInfoRen');
   $api->get('getPolicyInfoPay', 'RenewalController@getPolicyInfoPay');
   $api->get('getExpired', 'RenewalController@getExpired');
   $api->get('getDependentsforpolicy', 'RenewalController@getDependentsforpolicy');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('historical', 'HistoricalRenovationController');
});


$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('renewaldocuments', 'RenewalDocumentController');
    $api->post('/send', 'EmailController@send');
    $api->post('/exportexcel', 'RenewalController@exportExcel');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('eob', 'EobController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('ticket', 'TicketController');
    $api->post('/deleteFiles', 'EmailController@deleteFiles');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('tickettypes', 'TicketTypeController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('reports', 'ReportController');
   $api->post('getReport', 'ReportController@getReport');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('historical', 'HistoricalRenovationController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('claimdocumenttypes', 'ClaimDocumentTypesController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('claimdocumenttypedescriptions', 'ClaimDocumentTypeDescriptionsController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('registerpolicies', 'HistoricalPoliciesController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   //$api->controller('massivemails', 'MassivemailController');
    $api->post('sendmasive', 'EmailController@sendmasive');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('feedocuments', 'FeeDocumentsController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('comments', 'CommentController');
});
$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
   $api->controller('renewalnotificationdocs', 'RenewalNotificationDocumentsController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('branchs', 'BranchController');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('commissions', 'CommissionController');
    $api->get('verifycommission/{provider_id}/{branch_id}/{plan_id}/{agentId}/{commission}/{commission_ren}', 'CommissionController@verifyCommission');
});

$api->group(['middleware' => ['api', 'api.auth']], function ($api) {
    $api->controller('dashboard', 'DashboardController');
});

Route::post('/uploadpolicies', 'UploadController@storeFilesNewPolicy');
Route::post('/uploadfilespayment', 'UploadController@storeFilesPayment');
Route::post('/uploadfilespayment2', 'UploadController@storeFilesPaymentRenewal');
Route::post('/uploadfilespaymentrenewal', 'UploadController@storeRenewalDocs');
Route::post('/uploadclaims', 'UploadController@storeFilesNewClaim');
Route::post('/uploadtickets', 'UploadController@storeFilesNewTicket');
Route::post('/uploadticketsletter', 'UploadController@storeFilesTicketLetter');
Route::post('/send', 'EmailController@send');
Route::post('/updatePolicies', 'PolicyController@updatePolicies');
Route::post('/notificar', 'NotificationController@notificar');
Route::post('/uploadrenewal', 'UploadController@storeFilesRenewal');
Route::post('/updateTicketdate', 'TicketController@updateTicketdate');
Route::post('/deleteFiles', 'EmailController@deleteFiles');
Route::post('/updateEob', 'ClaimController@updateEob');
Route::post('/updateEob', 'ClaimController@updateEob');
Route::post('/uploadeob', 'UploadController@storeFilesEOB');
Route::post('/uploadFilesRenewalNotifications', 'UploadController@storeFilesRenewalNotifications');
//$api->get('updatePolicies', 'PolicyController@updatePolicies');