<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable=['id', 'name', 'description', 'status', 'user_id_update', 'user_id_creation' ];
}
