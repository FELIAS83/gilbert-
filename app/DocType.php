<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocType extends Model
{
    protected $fillable = [
        'name', 'status', 'created_at', 'updated_at', 'user_id_creation', 'user_id_update',
    ];
}
