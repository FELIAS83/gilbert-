<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $fillable = [
        'policy_id','fee_number','adminfee','tax_ssc','amount_tax_ssc','fee_payment_date','total','amount_titular', 'amount_spouse','amount_childs','amount_coverage','method','user_id_creation','user_id_update'
    ];
}
