<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table = 'doctors';
    protected $fillable = ['names', 'country_id', 'province_id', 'city', 'address','surnames','phone','identification','status','user_id_creation','user_id_update'];

    public function specialities(){//funcion para relacionar la tabla doctors con doctor_medical_specialities
        return $this->hasMany('App\DoctorMedicalSpeciality',//modelo a relacionar
                              'doctor_id',
                              'id');//campos a relacionar
    }
}
