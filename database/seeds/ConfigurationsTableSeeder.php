<?php

use Illuminate\Database\Seeder;

class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            'id' => 1,
            'variable_name' => 'taxssc',
            'variable_name_view' => 'Tax (SSC)',
            'value' => '0.5',
            'description' => '% Tax (SSC) usado en pagos',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ],
        [
            'id' => 2,
            'variable_name' => 'adminfee',
            'variable_name_view' => 'Admin Fee',
            'value' => '75',
            'description' => 'Cantidad de dólares a cobrar de Admin Fee',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ],
        [
            'id' => 3,
            'variable_name' => 'iva',
            'variable_name_view' => 'Tax (IVA)',
            'value' => '12',
            'description' => '% de cobro de iva',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }
}
