<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('first_name');
            $table->string('last_name');
            $table->string('identity_document');
            $table->string('email');
            $table->string('mobile','15');
            $table->string('phone', '15');
            $table->integer('agent_id')->references('id')->on('agents');
            $table->integer('plan_id')->references('id')->on('plans');
            $table->string('company_name');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policies');
    }
}
