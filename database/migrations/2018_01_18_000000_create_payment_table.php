<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');   
            $table->integer('policy_id')->references('id')->on('policies');
            $table->enum('mode', ['0', '1', '2','3'])->default('0');
            $table->enum('method', ['0', '1', '2','3'])->default('0');
            $table->enum('discount', ['0', '1'])->default('0');
            $table->integer('discount_percent');
            $table->enum('invoice_before', ['0', '1'])->default('0');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}