<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoverageId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coverage_details', function (Blueprint $table) {
            $table->integer('coverage_id')->after('amount')->references('id')->on('coverages');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coverage_details', function (Blueprint $table) {
            $table->dropColumn('coverage_id');            
        });
    }
}
