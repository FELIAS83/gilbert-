<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMedicalInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE medical_informations CHANGE COLUMN repairable_id repairable_id ENUM('0','1','2','3')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       // DB::statement('ALTER TABLE `medical_informations` CHANGE `repairable_id` `repairable_id` integer');
    }
}
