<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUisrefrenewalWizarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          DB::statement("ALTER TABLE `wizar_elements` ADD `uisrefrenewal` VARCHAR(255) NOT NULL AFTER `uisref`");;

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         DB::statement("ALTER TABLE `wizar_elements` DROP `uisrefrenewal`;");
    }
}
