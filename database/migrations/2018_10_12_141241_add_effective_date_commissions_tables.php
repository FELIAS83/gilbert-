<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEffectiveDateCommissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `agent_commisions` ADD `effective_date` date  AFTER `amount` ;");
        DB::statement("ALTER TABLE `company_commissions` ADD `effective_date` date  AFTER `amount` ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `agent_commisions` DROP `effective_date` ;");
        DB::statement("ALTER TABLE `company_commissions` DROP `effective_date`;");
    }
}
