<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionPercentageMaxDependentAgePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `plans` ADD `commission_percentage` decimal(8,2) NOT NULL AFTER `branch_id`;");
        DB::statement("ALTER TABLE `plans` ADD `max_dependent_age` int(11) NOT NULL AFTER `commission_percentage`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `plans` DROP `commission_percentage` ;");
        DB::statement("ALTER TABLE `plans` DROP `max_dependent_age` int(11) ;");
    }
}
