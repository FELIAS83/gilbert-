<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annexes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('affiliate_id');
            $table->string('annex');
            $table->date('effective_date');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('annexes');
    }
}
