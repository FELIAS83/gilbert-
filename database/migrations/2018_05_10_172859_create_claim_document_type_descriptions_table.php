<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimDocumentTypeDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('claim_document_type_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_doc_type')->unsigned()->index();
            $table->foreign('id_doc_type')->references('id')->on('claim_document_types');
            $table->string('description');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('claim_document_type_descriptions');
    }
}
