<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->integer('company_id');
            $table->decimal('commission_company',8,2);
            $table->integer('branch_id');
            $table->decimal('commission_branch',8,2);
            $table->integer('plan_id');
            $table->decimal('commission_plan',8,2);
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('commisions');
    }
}
