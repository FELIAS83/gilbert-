<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorMedicalSpecialitiesTable extends Migration
{
    public function up()
    {
        Schema::create('doctor_medical_specialities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id');
            $table->integer('speciality_id');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    public function down()
    {
        Schema::drop('doctor_medical_specialities');
    }
}
