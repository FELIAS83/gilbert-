<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClaimCurrencyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_documents', function (Blueprint $table) { 
            $table->integer('currency_id')->unsigned()->index()->after('id_doc_type');
            $table->foreign('currency_id')->references('id')->on('currencies');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_documents', function (Blueprint $table) {
            $table->dropColumn('currency_id');            
                 
        });
    }
}
