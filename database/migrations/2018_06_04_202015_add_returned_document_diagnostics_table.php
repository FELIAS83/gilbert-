<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReturnedDocumentDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          DB::statement("ALTER TABLE `document_diagnostics` ADD `returned` ENUM('0','1') NOT NULL AFTER `user_id_update`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `document_diagnostics` DROP `returned`");
    }
}
