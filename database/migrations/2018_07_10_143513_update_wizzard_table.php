<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWizzardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         DB::statement("ALTER TABLE `wizar_elements` ADD `uisrefemisionren` VARCHAR(255) NOT NULL AFTER `uisrefrenewal`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          DB::statement("ALTER TABLE `wizar_elements` DROP `uisrefemisionren`;");
    }
}
