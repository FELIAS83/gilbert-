<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableNotRequeriedTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            DB::statement('ALTER TABLE  `tickets` CHANGE  `doctor`  `doctor` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
');
            DB::statement('ALTER TABLE  `tickets` CHANGE  `medical_speciality`  `medical_speciality` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;');
            DB::statement('ALTER TABLE  `tickets` CHANGE  `date_end`  `date_end` DATE NULL ;');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            
            //DB::statement('ALTER TABLE `tickets` CHANGE `hour` `hour` TIME NOT NULL;');
        });
    }
}
