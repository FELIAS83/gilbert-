<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deductible_details', function (Blueprint $table) {
            $table->integer('child_quantity')->after('is_child');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deductible_details', function (Blueprint $table) {
            $table->dropColumn('child_quantity');            
        });
    }
}
