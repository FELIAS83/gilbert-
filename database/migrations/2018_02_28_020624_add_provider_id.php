<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_documents', function (Blueprint $table) {            
            $table->integer('provider_id')->references('id')->on('providers')->after('id_doc_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_documents', function (Blueprint $table) {
            //
             $table->dropColumn('provider_id');
        });
    }
}
