<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policysteps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_policy');
            $table->enum('step1', ['1', '0'])->default('0');
            $table->enum('step2', ['1', '0'])->default('0');
            $table->enum('step3', ['1', '0'])->default('0');
            $table->enum('step4', ['1', '0'])->default('0');
            $table->enum('step5', ['1', '0'])->default('0');
            $table->enum('step6', ['1', '0'])->default('0');
            $table->enum('step7', ['1', '0'])->default('0');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policysteps');
    }
}
