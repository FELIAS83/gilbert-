<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeductibleCoverageId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('policies', function (Blueprint $table) { 
           $table->integer('deductible_id')->after('plan_id')->references('id')->on('deductibles');
           $table->integer('coverage_id')->after('deductible_id')->references('id')->on('coverages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dependents', function (Blueprint $table) {
            $table->dropColumn('deductible_id');            
            $table->dropColumn('coverage_id');            
        });
    }
}
