<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDoctorFieldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('doctor');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('doctor')->unsigned()->index()->after('hour');
            //$table->foreign('doctor')->references('id')->on('doctors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('doctor');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('doctor')->after('hour');
        });
    }
}
