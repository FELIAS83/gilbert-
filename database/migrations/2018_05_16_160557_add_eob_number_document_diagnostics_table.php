<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEobNumberDocumentDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         DB::statement("ALTER TABLE `document_diagnostics` ADD `eob_number` INT(11) NOT NULL AFTER `liquidated`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `document_diagnostics` DROP `eob_number`");
        DB::statement("ALTER TABLE `claim_documents` DROP `eob_number`");
    }
}
