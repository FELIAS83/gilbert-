<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_documents', function (Blueprint $table) {            
            $table->double('amount', 11,2)->nullable()->after('id_doc_type');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_documents', function (Blueprint $table) {
            $table->dropColumn('amount');            
        });
    }
}
