<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `policies` ADD `start_date_coverage` DATE NOT NULL AFTER `coverage_id`, ADD `continuity_coverage` ENUM('1','0') NOT NULL DEFAULT '0' AFTER `start_date_coverage`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement('ALTER TABLE `policies` DROP `start_date_coverage`, DROP `continuity_coverage`;');
    }
}
