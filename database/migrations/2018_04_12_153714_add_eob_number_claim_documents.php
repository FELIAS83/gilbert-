<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEobNumberClaimDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim_documents', function (Blueprint $table) {
            //
            $table->integer('eob_number')->after('currency_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim_documents', function (Blueprint $table) {
            //
            $table->dropColumn('eob_number');
        });
    }
}
