<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      //Schema::table('fees', function (Blueprint $table) {
        DB::statement("ALTER TABLE `fees` ADD `method` ENUM('0', '1', '2', '3') NOT NULL AFTER `paid_fee`");
        DB::statement("ALTER TABLE `fees` ADD `payment_date` DATE NOT NULL AFTER `method`");
        DB::statement("ALTER TABLE `fees` ADD `check_number` VARCHAR(20) NOT NULL AFTER `payment_date`");
        DB::statement("ALTER TABLE `fees` ADD `bank_name` VARCHAR(50) NOT NULL AFTER `check_number`");
        DB::statement("ALTER TABLE `fees` ADD `transfer_number` VARCHAR(20) NOT NULL AFTER `bank_name`");
        DB::statement("ALTER TABLE `fees` ADD `titular_name` VARCHAR(50) NOT NULL AFTER `transfer_number`");
        DB::statement("ALTER TABLE `fees` ADD `titular_lastnames` VARCHAR(50) NOT NULL AFTER `titular_name`");
        DB::statement("ALTER TABLE `fees` ADD `account_type` INT(11) NOT NULL AFTER `titular_lastnames`");
        DB::statement("ALTER TABLE `fees` ADD `account_number` VARCHAR(20) NOT NULL AFTER `account_type`");
        DB::statement("ALTER TABLE `fees` ADD `deposit_number` VARCHAR(20) NOT NULL AFTER `account_number`");
        DB::statement("ALTER TABLE `fees` ADD `card_type` INT(11) NOT NULL AFTER `deposit_number`");
        DB::statement("ALTER TABLE `fees` ADD `card_mark` INT(11) NOT NULL AFTER `card_type`");
        DB::statement("ALTER TABLE `fees` ADD `card_bank` INT(11) NOT NULL AFTER `card_mark`");
        DB::statement("ALTER TABLE `fees` ADD `payment_type` INT(11) NOT NULL AFTER `card_bank`;");
      //)};

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
      //Schema::table('fees', function (Blueprint $table) {
         DB::statement("ALTER TABLE `fees` DROP `method`") ;
         DB::statement("ALTER TABLE `fees` DROP `payment_date`");
         DB::statement("ALTER TABLE `fees` DROP `check_number`");
         DB::statement("ALTER TABLE `fees` DROP `bank_name`");
         DB::statement("ALTER TABLE `fees` DROP `transfer_number`");
         DB::statement("ALTER TABLE `fees` DROP `titular_name`");
         DB::statement("ALTER TABLE `fees` DROP `titular_lastnames`");
         DB::statement("ALTER TABLE `fees` DROP `account_type`");
         DB::statement("ALTER TABLE `fees` DROP `account_number`");
         DB::statement("ALTER TABLE `fees` DROP `deposit_number`");
         DB::statement("ALTER TABLE `fees` DROP `card_type`");
         DB::statement("ALTER TABLE `fees` DROP `card_mark`");
         DB::statement("ALTER TABLE `fees` DROP `card_bank`");
         DB::statement("ALTER TABLE `fees` DROP `payment_type`;");

       //)};
    }
}
