<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->increments('id');   
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address')->nullable();
            $table->enum('gender', ['M', 'F'])->nullable();
            $table->string('civil_status')->nullable();
            $table->integer('country_id')->references('id')->on('address_countrys')->nullable()->unsigned();
            $table->integer('province_id')->references('id')->on('address_provinces')->nullable()->unsigned();
            $table->integer('city_id')->references('id')->on('address_citys')->nullable()->unsigned();
            $table->string('email');
            $table->string('phone');
            $table->string('mobile');
            $table->string('place_of_birth')->nullable();
            $table->date('birthday')->nullable();
            $table->string('height', 5)->nullable();
            $table->enum('height_unit_measurement', ['M', 'CM'])->nullable();
            $table->char('document_type', 1)->nullable();
            $table->string('identity_document')->nullable();
            $table->string('weight', 5)->nullable();
            $table->enum('weight_unit_measurement', ['KG', 'LB'])->nullable();
            $table->string('occupation')->nullable();
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicants');
    }
}