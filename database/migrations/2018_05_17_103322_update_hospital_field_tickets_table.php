<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHospitalFieldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('hospital');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('hospital')->unsigned()->index()->after('medical_speciality');
            $table->foreign('hospital')->references('id')->on('hospitals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('hospital');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('hospital')->after('medical_speciality');
        });
    }
}
