<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWizarElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wizar_elements', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name');
            $table->string('icon');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wizar_elements');
    }
}
