<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMedicalSpecialityFieldTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('medical_speciality');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('medical_speciality')->unsigned()->index()->after('doctor');
            //$table->foreign('medical_speciality')->references('id')->on('medical_specialties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('medical_speciality');
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('medical_speciality')->after('doctor');
        });
    }
}
