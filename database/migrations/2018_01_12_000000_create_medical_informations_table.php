<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicalInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_informations', function (Blueprint $table) {
            $table->increments('id');   
            $table->integer('policy_id')->references('id')->on('policies');
            $table->integer('quiz_id')->references('id')->on('quiz');
            $table->integer('quiz_details_id')->references('id')->on('quiz_details');
            $table->enum('answer', ['0', '1'])->default('0');
             $table->string('observation')->nullable();
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_informations');
    }
}