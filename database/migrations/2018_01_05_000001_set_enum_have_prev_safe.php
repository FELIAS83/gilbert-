<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SetEnumHavePrevSafe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE  `policies` CHANGE  `have_prev_safe`  `have_prev_safe` ENUM(  "1",  "0" ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT  "0";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
