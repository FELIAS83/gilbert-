<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoverageIdHistoricalRenovationsTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('historical_renovations', function (Blueprint $table) {
            $table->integer('id_coverage')->unsigned()->index()->after('id_deductibles');
            //$table->foreign('id_coverage')->references('id')->on('coverages');
         });
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_renovations', function (Blueprint $table) {
            $table->dropColumn('id_coverages');            
        });
    }
}
