<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RmProviderIdDocDiag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_diagnostics', function (Blueprint $table) {
            //
             $table->dropForeign(['provider_id'])->references('id')->on('providers')->onDelete('cascade');
             $table->dropColumn('provider_id');
        });
    }

}
