<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentCommisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_commisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id');
            $table->integer('policy_id');
            $table->integer('plan_id');
            $table->integer('deductible_id');
            $table->integer('coverage_id');
            $table->integer('commision');
            $table->decimal('amount',15,2);
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_commisions');
    }
}
