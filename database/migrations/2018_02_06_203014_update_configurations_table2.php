<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConfigurationsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `configurations` DROP `policy_file_directory`,DROP `claim_file_directory`;");
        DB::statement("ALTER TABLE `configurations` ADD `variable_name` VARCHAR(255) NOT NULL AFTER `id`, ADD `variable_name_view` VARCHAR(255) NOT NULL AFTER `variable_name`, ADD `value` VARCHAR(255) NOT NULL AFTER `variable_name_view`, ADD `description` VARCHAR(255) NOT NULL AFTER `value`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `configurations` ADD `policy_file_directory` VARCHAR(255) NOT NULL AFTER `id`, ADD `claim_file_directory` VARCHAR(255) NOT NULL AFTER `policy_file_directory`;
        ALTER TABLE `configurations` DROP `variable_name`,DROP `variable_name_view`,DROP `value`,DROP `description`");
    }
}
