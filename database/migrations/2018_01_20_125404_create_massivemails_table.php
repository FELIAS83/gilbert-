<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMassivemailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('massivemails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('format');
            $table->string('subject');
            $table->string('title');
            $table->string('content');
            $table->string('recipient');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('massivemails');
    }
}
