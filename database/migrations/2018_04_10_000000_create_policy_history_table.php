<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePolicyHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies_history', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('number');
            $table->integer('applicant_id');
            $table->integer('agent_id');
            $table->integer('plan_id');
            $table->integer('deductible_id');
            $table->integer('coverage_id');
            $table->date('start_date_coverage');            
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policies_history');
    }
}
