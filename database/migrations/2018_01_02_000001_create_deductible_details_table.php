<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeductibleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deductible_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('age_start');
            $table->integer('age_end');
            $table->enum('is_child', ['1', '0'])->default('0');
            $table->integer('amount');
            $table->integer('deductible_id')->references('id')->on('deductibles');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deductible_details');
    }
}
