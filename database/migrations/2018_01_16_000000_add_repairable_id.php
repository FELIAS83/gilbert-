<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRepairableId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_informations', function (Blueprint $table) { 
           $table->integer('repairable_id')->after('answer');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_informations', function (Blueprint $table) {
            $table->dropColumn('repairable_id');            
                 
        });
    }
}
