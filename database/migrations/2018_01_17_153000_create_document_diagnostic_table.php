<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentDiagnosticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_diagnostics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diagnostic_id')->unsigned()->index();
            $table->foreign('diagnostic_id')->references('id')->on('diagnostics');
            $table->integer('document_id')->unsigned()->index();
            $table->foreign('document_id')->references('id')->on('claim_documents');
            $table->integer('provider_id')->unsigned()->index();
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('document_diagnostics');
    }
}
