<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ticket_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_id')->references('tickets')->on('id');
            $table->integer('id_doc_type');
            $table->string('directory');
            $table->string('filename');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
