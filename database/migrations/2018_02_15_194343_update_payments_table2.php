
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `payments` ADD `check_number` VARCHAR(20) NOT NULL AFTER `invoice_before`");
        DB::statement("ALTER TABLE `payments` ADD `bank_name` VARCHAR(50) NOT NULL AFTER `check_number`;");
        DB::statement("ALTER TABLE `payments` ADD `payment_date` DATE NOT NULL AFTER `invoice_before`;"); 
        DB::statement("ALTER TABLE `payments` ADD `transfer_number` VARCHAR(20) NOT NULL AFTER `bank_name`");
        DB::statement("ALTER TABLE `payments` ADD `titular_name` VARCHAR(50) NOT NULL AFTER `transfer_number`");
        DB::statement("ALTER TABLE `payments` ADD `account_type` INT NOT NULL AFTER `titular_name`");
        DB::statement("ALTER TABLE `payments` ADD `account_number` VARCHAR(20) NOT NULL AFTER `account_type`;");
        DB::statement("ALTER TABLE `payments` ADD `deposit_number` VARCHAR(20) NOT NULL AFTER `account_number`;");
        DB::statement("ALTER TABLE `payments` ADD `titular_lastnames` VARCHAR(50) NOT NULL AFTER `deposit_number`");
        DB::statement("ALTER TABLE `payments` ADD `card_type` INT NOT NULL AFTER `titular_lastnames`");
        DB::statement("ALTER TABLE `payments` ADD `card_mark` INT NOT NULL AFTER `card_type`");
        DB::statement("ALTER TABLE `payments` ADD `card_bank` INT NOT NULL AFTER `card_mark`");
        DB::statement("ALTER TABLE `payments` ADD `payment_type` INT NOT NULL AFTER `card_bank`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `fees` ADD `total` DECIMAL(15,2) NOT NULL AFTER `adminfee`;");
    }
}
