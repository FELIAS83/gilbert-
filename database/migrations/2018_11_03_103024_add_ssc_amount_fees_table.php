<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSscAmountFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `fees` ADD `amount_tax_ssc` decimal(15,2)  NOT NULL DEFAULT '0' AFTER `tax_ssc` ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `fees` DROP `amount_tax_ssc`");
    }
}
