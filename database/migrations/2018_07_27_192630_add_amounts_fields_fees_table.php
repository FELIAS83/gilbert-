<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountsFieldsFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `fees` ADD `amount_titular` DECIMAL(15,2) NOT NULL AFTER `total`;");
        DB::statement("ALTER TABLE `fees` ADD `amount_spouse` DECIMAL(15,2) NOT NULL AFTER `amount_titular`;");
        DB::statement("ALTER TABLE `fees` ADD `amount_childs` DECIMAL(15,2) NOT NULL AFTER `amount_spouse`;");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `fees` DROP `amount_titular`;");
        DB::statement("ALTER TABLE `fees` DROP `amount_titular`;");
        DB::statement("ALTER TABLE `fees` DROP `amount_titular`;");
        //
    }
}
