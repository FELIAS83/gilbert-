<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WizarAddUisref extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wizar_elements', function (Blueprint $table) {
            $table->string('uisref')->after('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wizar_elements', function (Blueprint $table) {
            $table->dropColumn('uisref');            
        });
    }
}
