<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableDocumentDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
            DB::statement('ALTER TABLE  claim_documents CHANGE  document_date  document_date DATE NULL ;');        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {            
        //DB::statement('ALTER TABLE `tickets` CHANGE `hour` `hour` TIME NOT NULL;');
    }
}
