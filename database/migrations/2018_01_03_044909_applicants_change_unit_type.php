<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplicantsChangeUnitType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE  `applicants` CHANGE  `height_unit_measurement`  `height_unit_measurement` BOOLEAN NULL DEFAULT NULL ;');
        DB::statement('ALTER TABLE  `applicants` CHANGE  `weight_unit_measurement`  `weight_unit_measurement` BOOLEAN NULL DEFAULT NULL ;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE  `applicants` CHANGE  `height_unit_measurement`  `height_unit_measurement` ENUM(  `CM`,  `M` ) NULL DEFAULT NULL ;');
        DB::statement('ALTER TABLE  `applicants` CHANGE  `weight_unit_measurement`  `weight_unit_measurement` ENUM(  `KG`,  `LB` ) NULL DEFAULT NULL ;');
    }
}
