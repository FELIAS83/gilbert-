<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDirAndPhoneDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //Schema::table('doctors', function (Blueprint $table) {
            //$table->string('address',255 )->nullable()->after('city_id');
        //DB::statement("ALTER TABLE `document_diagnostics` ADD `pending_doc` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `liquidated`;");
           DB::statement("ALTER TABLE  `doctors` ADD `address` VARCHAR(255) NULL DEFAULT NULL AFTER `city_id`");
            DB::statement("ALTER TABLE  `doctors` ADD `phone`  INT(25) NOT NULL AFTER `surnames`");
           // $table->integer('phone', 25)->after('address');
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('doctors', function (Blueprint $table) {
         $table->dropColumn('address');
            $table->dropColumn('phone');
            });
    }
}
