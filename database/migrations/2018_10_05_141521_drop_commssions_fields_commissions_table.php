<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCommssionsFieldsCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `commissions` DROP `commission_company`;");
        DB::statement("ALTER TABLE `commissions` DROP `commission_branch`;");
        DB::statement("ALTER TABLE `commissions` DROP `commission_plan`;");
        DB::statement("ALTER TABLE `commissions` ADD `commission` decimal(8,2) NOT NULL AFTER `plan_id`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
