<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDateTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
        });
         Schema::table('tickets', function (Blueprint $table) {
            $table->timestamp('date_start')->default(DB::raw('CURRENT_TIMESTAMP'))->after('city');
            $table->timestamp('date_end')->nullable()->after('date_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('tickets', function (Blueprint $table) {
            $table->date('date_start');
            $table->date('date_end');
        });
    }
}
