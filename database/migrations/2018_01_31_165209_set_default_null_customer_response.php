<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultNullCustomerResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE  policies CHANGE  customer_response  customer_response ENUM(  '0',  '1' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //DB::statement("ALTER TABLE `configurations` DROP `claim_file_directory`;");
    }
}
