<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_agents', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('first_name');
            $table->string('last_name');
            $table->string('identity_document');
            $table->date('birthday');
            $table->string('email');
            $table->string('skype');
            $table->string('mobile');
            $table->string('phone');
            $table->integer('country_id')->references('id')->on('address_countrys');
            $table->integer('province_id')->references('id')->on('address_provinces');
            $table->integer('city_id')->references('id')->on('address_citys');
            $table->string('address');
            $table->decimal('commission', 8, 2);
            $table->integer('leader');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_agents');
    }
}