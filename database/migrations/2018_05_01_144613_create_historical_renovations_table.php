<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalRenovationsTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('historical_renovations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_policy')->unsigned()->index();
            $table->foreign('id_policy')->references('id')->on('policies');
            $table->integer('id_plan')->unsigned()->index();
            $table->foreign('id_plan')->references('id')->on('plans');
            $table->integer('id_deductibles')->unsigned()->index();
            $table->foreign('id_deductibles')->references('id')->on('deductibles');
            //$table->integer('id_coverages')->unsigned()->index();
            //$table->foreign('id_coverages')->references('id')->on('coverages');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historical_renovations');
    }
}
