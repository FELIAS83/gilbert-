<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDiscountPercentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
            DB::statement('ALTER TABLE `payments` CHANGE `discount_percent` `discount_percent` FLOAT(11.2) NOT NULL;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {            
        //DB::statement('ALTER TABLE `payments` CHANGE `discount_percent` `discount_percent` int NOT NULL;');
    }
}
