<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCityDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
      Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('city_id');
        });
        Schema::table('doctors', function (Blueprint $table) {
            $table->string('city')->after('province_id');
            //$table->foreign('doctor')->references('id')->on('doctors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
