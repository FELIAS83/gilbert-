<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->increments('id');   
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('document_type', ['0', '1', '2'])->default('1');
            $table->string('identity_document')->nullable();
            $table->enum('relationship', ['H', 'D'])->nullable();
            $table->date('birthday')->nullable();            
            $table->string('height', 5)->nullable();
            $table->enum('height_unit_measurement', ['M', 'CM'])->nullable();            
            $table->string('weight', 5)->nullable();
            $table->enum('weight_unit_measurement', ['KG', 'LB'])->nullable();
            $table->enum('gender', ['M', 'F'])->nullable();
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dependents');
    }
}