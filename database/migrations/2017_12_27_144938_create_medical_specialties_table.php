<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('abbreviation');
            $table->string('name');
            $table->enum('status', ['1', '0'])->default('1');
            $table->timestamps();
            $table->integer('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_specialties');
    }
}
