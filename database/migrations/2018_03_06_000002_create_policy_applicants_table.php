<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('policy_id_renewal');
            $table->integer('applicant_id');
            $table->integer('policy_id_old');
            $table->timestamps();
            $table->integer('user_id_creation')->unsigned()->index();
            $table->foreign('user_id_creation')->references('id')->on('users');
            $table->integer('user_id_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policy_applicants');
    }
}

