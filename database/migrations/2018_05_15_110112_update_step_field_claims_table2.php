<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStepFieldClaimsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `claims` CHANGE `step` `step` ENUM('0','0.5','1','2','2.5') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `claims` CHANGE `step` `step` ENUM('0','0.5','1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }
}
