<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeductibleChangeAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE  `deductibles` CHANGE  `amount_in_usa`  `amount_in_usa` FLOAT( 11.2 ) NOT NULL ;
');
        DB::statement('ALTER TABLE  `deductibles` CHANGE  `amount_out_usa`  `amount_out_usa` FLOAT( 11.2 ) NOT NULL ;
');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
