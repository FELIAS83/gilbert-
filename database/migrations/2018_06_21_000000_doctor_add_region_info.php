<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DoctorAddRegionInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->integer('country_id')->after('names')->nullable();
            $table->integer('province_id')->after('country_id')->nullable();
            $table->integer('city_id')->after('province_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('country_id');
            $table->dropColumn('province_id');
            $table->dropColumn('city_id');
        });
    }
}
