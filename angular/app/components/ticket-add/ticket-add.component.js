class TicketAddController{
    constructor($scope, API, $state, $stateParams,$http, $uibModal){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.$http = $http
        this.role = ""
        this.directory = '/tickets/'
        this.lines = []
        this.doc_typeselected = []
        //this.doc_typeselected[0] = 1
        this.value = []
        this.files=[]
        this.lines.push({id : 0, disabled : false, req : ''});
        var ticket_id = $stateParams.ticket_id
        this.ticket_id = ticket_id
        this.processing = false
        this.$uibModal = $uibModal
        this.llave = false
        this.llave2 = false
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        

        if (sessionStorage.getItem('info')){
            let info = JSON.parse(sessionStorage.getItem('info'))
            this.policy_id = info.policy_id
            this.id_person = info.id_person
            this.getAfilliates(this.policy_id)
            //this.systemTicketTypes[0] = info.vm.systemTicketTypes[0]
            this.city = info.city
            this.doctor = info.doctor
            if (this.doctor){
                this.changeDoctor()
            }
            this.hospital = info.hospital
            this.date_start = new Date(info.date_start)
            this.date_end= new Date(info.date_end)
            this.hour= new Date(info.hour)
            this.systemTicketType = info.systemTicketType

            this.medical_speciality = info.medical_speciality
            this.lines = info.lines
            this.doc_typeselected = info.doc_typeselected
            this.myFile = info.myFile
            this.files = info.files
            sessionStorage.removeItem('info')
            //console.log('info',info)
        }

        let Policies = this.API.service('policies')
        Policies.getList({'completed': 1})
        .then((response) => {
            let systemPolicies = []
            let policiesResponse = response.plain()
            angular.forEach(policiesResponse, function (value) {
                systemPolicies.push({id: value.id, policy_id: value.policy_id, name: value.name, number: value.number})
            })
            this.systemPolicies = systemPolicies
            //console.log("systemPolicies"+JSON.stringify(this.systemPolicies))            
        })

        let TicketTypes = this.API.service('tickettypes')
        TicketTypes.getList()
        .then((response)=>{
            let systemTicketTypes = []
            let ticketyperesponse=response.plain()
            angular.forEach(ticketyperesponse, function (value){
                systemTicketTypes.push({id:value.id, name:value.name})
        })
            this.systemTicketTypes = systemTicketTypes
            //console.log("systemTicketTypes"+JSON.stringify(this.systemTicketTypes))
        })

        let systemDocTypes = []
        systemDocTypes.push({id: "1", name: "Formulario de Hospital"})
        systemDocTypes.push({id: "2", name: "Formulario de BD"})
        systemDocTypes.push({id: "3", name: "Informes Médicos"})
        systemDocTypes.push({id: "4", name: "Confirmación de Cita"})
        systemDocTypes.push({id: "5", name: "Formulario Interconsulta"})
        this.systemDocTypes = systemDocTypes
        //console.log(this.systemDocTypes)


        let doctors = this.API.service('doctors')
        doctors.getList({}).then((response) => {
            let dataSet = response.plain()
            this.doctors = dataSet
            //console.log('doctors',this.doctors)
          })

        let hospital = this.API.service('hospitals')
        hospital.getList({}).then((response) => {
            let dataSet = response.plain()
            this.hospitals = dataSet
            //console.log('hospitals',this.hospitals)
          })

        let medicalspecialties = this.API.service('medicalspecialties')
        medicalspecialties.getList({}).then((response) => {
            let dataSet = response.plain()
            this.medicalspecialties = dataSet
            //console.log('medicalspecialties',this.medicalspecialties)
          })

    }

     getAfilliates(policyId){    
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        var relationship = ""
        var orderRole = 0

        //console.log("policy"+policyId)
        let Applicants= this.API.service('show', this.API.all('applicants'))
        Applicants.one(policyId).get() 
        .then((response) => {
           this.applicantsresponse= response.plain() 
           //console.log("ApplicantsResponse"+JSON.stringify(this.applicantsresponse))            
        })

        let Dependents = this.API.service('dependents', this.API.all('dependents'))
        Dependents.one(policyId).get()
        .then((response) => {
            this.dependentsdata = response.plain()
            //console.log("dependentsdata"+JSON.stringify(this.dependentsdata))
 
             angular.forEach(this.dependentsdata.data, function (value) {

                if(value.relationship == "H"){
                        relationship = "Cónyuge"
                        orderRole = 2
                }
                else{
                     relationship = "Dependiente"
                     orderRole = 3
                    }
                systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, relationship: relationship})
                allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, role: value.relationship, order: orderRole, relationship: relationship  })
            })
            allAffiliates.push({id: parseInt(this.applicantsresponse.data[0].id), name: this.applicantsresponse.data[0].first_name+' '+this.applicantsresponse.data[0].last_name, role: "T", order: 1, relationship: 'Titular'})
            //console.log("allAffiliates.role"+JSON.stringify(allAffiliates[0].role))   
      
                allAffiliates.sort(function (a, b) {
                    if (a.order > b.order) {
                        return 1;
                    }
                    if (a.order < b.order) {
                        return -1;
                    }
                    return 0;
                });

                systemDependents.sort(function (a, b){
                  return (b.relationship - a.relationship)
                })
                this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                //console.log("systemDependents"+JSON.stringify(this.systemDependents))
                //console.log("allAffiliates"+JSON.stringify(this.allAffiliates))
                //console.log("allAffiliates.role"+JSON.stringify(this.allAffiliates[0].role))               
            })        
        
     }


     getpersontype(){
        var select=document.getElementById("id_person");   
        var text = select.options[select.selectedIndex].innerText;
        var arrtext =text.split("|")
        var relacion=arrtext[2].trim()

        var person_type=""
        
        if (relacion == 'Titular'){
             person_type = 'A'
         }
            else
            {
                person_type = 'D';
            }
        
        return (person_type);
     }   

    changeDocType(index,type){
        this.indice = index
        if (type == 1){
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
               
        }
        else {
            this.lines[index].disabled = true
            this.lines[index].rep = 'No '
            
            this.value[index] = ''
        }
        }
        else{//anteriores
        if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
        }
        else {
            this.previouslines[index].disabled = true
            this.previouslines[index].rep = 'No '
          
        }
        }

    }

    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});

            //console.log('lineas',this.lines)
        
        //console.log('archivos',this.files)
    }

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    deleteLine(index){//eliminar de los archivos nuevos agregados
        //console.log('antes')
        //console.log('lines',this.lines)
        //console.log('doc_typeselected',this.doc_typeselected)
        //console.log('value',this.value)
        //console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        //console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        //console.log('despues')
        //console.log('lines',this.lines)
        //console.log('doc_typeselected',this.doc_typeselected)
        //console.log('value',this.value)
        //console.log('files',this.files)
    }


     getType(){
       var type= document.getElementById('ticket_type');
       //alert(type.value);
       var tipo=type.value;
       return(tipo);
     }
     changeDoctor(){
        this.llave = true
        let doctorData = this.API.service('show', this.API.all('doctors'))//ojo
        doctorData.one(this.doctor).get()
        .then((response) => {
            $(".js-example-basic-single4").select2({
                placeholder: "Seleccione Espcialidad"
             });
            let dataSet = this.API.copy(response)
            this.doctordata = dataSet.data.specialities
            //console.log("doctordata",this.doctordata)
            
            return true
        })
    }

    ChangeDoctorBySpec(id){
        this.llave2 = true
        //console.log("this.medical_specialty",this.medical_speciality)
        //console.log("id",id)
        let doctorbyspec = this.API.service('doctorbyspec', this.API.all('doctors'))
        doctorbyspec.one(id).get()
        .then((response) => {
            $(".js-example-basic-single2").select2({
                placeholder: "Seleccione Médico"
             });
            
             let systemdoctorbyspec = []
             let doctorbyspecResponse= response.plain()
                        
              angular.forEach(doctorbyspecResponse.data.data, function (value) {
              systemdoctorbyspec.push({id: value.doctor_id, names: value.names+' '+value.surnames})
               })
              this.systemdoctorbyspec=systemdoctorbyspec
            return true
        })
    }



    save (isValid) {                      
            let uploadFile = this.uploadFile
            let filenames = []
            angular.forEach(this.myFile, function (value) {
            filenames.push(value.name)
            })
            this.filenames = filenames
            this.$state.go(this.$state.current, {}, { alerts: 'test' })

            if (isValid) {
                this.processing = true

            let Tickets = this.API.service('tickets', this.API.all('tickets'))
          

            var vm = this
            if (this.hour !== null) {
                let tiempo=this.hour.toString()
                //console.log("tiempo"+tiempo)
                var hora=tiempo.substring(16,24)
                //console.log("hora"+hora)
            }
            
            let $state = this.$state  
            //console.log("ticket_type ", this.systemTicketTypes)

            $state.go($state.current, {}, { reload: $state.current})  
            Tickets.post({
                'ticket_type':this.getType(),
                'id_person':this.id_person,
                'person_type':this.getpersontype(),
                'hour':hora,
                'doctor':this.doctor,    
                'medical_speciality':this.medical_speciality,
                'hospital':this.hospital,
                'city':this.city,
                'date_start':this.date_start,
                'date_end':this.date_end,
                'id_policy': this.policy_id,
                'id_doc_types':this.doc_typeselected,
                'filenames':this.filenames

                }).then(function (response) {
                    vm.uploadFile()
                swal({
                       title: 'El Ticket se ha guardado Correctamente!',
                       type: 'success',
                       confirmButtonText: 'OK',
                       closeOnConfirm: true
                   }, function () {                                                                                                  
                       swal.close()
                       $state.go('app.ticket-lists')
                   })
               , function (response) {
                   swal({
                       title: 'Error Inesperado!',
                       type: 'error',
                       confirmButtonText: 'OK',
                       closeOnConfirm: true
                   })
               }
              })
            } else { 
              this.formSubmitted = true
        }
    }

    uploadFile(){
        var i = 0
            var files = this.files//archivos nuevos
            var pfiles = this.pfiles//archivos que sustituiran a los anteriores
            //var ticket_id = this.ticket_id
            var $http = this.$http
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                //form_data.append('ticket_id', ticket_id);
                $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   })
            })
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                //form_data.append('ticket_id', ticket_id);
                $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })
            this.processing = false

        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Los documentos han sido guardados exitosamente.'}
        this.alerts = [alert]
        //this.$state.reload();
    }


    mostrar(){
        //console.log('lines',this.lines)
        //console.log('doc_typeselected',this.doc_typeselected)
        //console.log('value',this.value)
        //console.log('files',this.files)
    }

    modalDoctor(size) {
        let info = {}
        info.policy_id = this.policy_id
        info.id_person = this.id_person
        //info.systemTicketTypes[0] = this.systemTicketTypes[0]
        info.city = this.city
        info.hospital = this.hospital
        info.medical_speciality = this.medical_speciality
        info.date_start = this.date_start
        info.date_end = this.date_end
        info.hour = this.hour
        info.systemTicketType = this.systemTicketType
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalDoctor',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size
      })
    }
    modalHospital(size) {
        let info = {}
        info.policy_id = this.policy_id
        info.id_person = this.id_person
        info.city = this.city
        info.doctor = this.doctor
        info.medical_speciality = this.medical_speciality
        info.date_start = this.date_start
        info.date_end = this.date_end
        info.hour = this.hour
        info.systemTicketType = this.systemTicketType
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))
        //console.log('modalhospitla', info)

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalHospital',
          controller: this.modalcontroller2,
          controllerAs: 'mvm',
          size: size
      })
    }

    modalcontroller (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state

let Countrys = API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
            //console.log('paises',this.systemCountrys)
        })



        this.getState = function(){
            
            //console.log("pais",this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        
        /*this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }*/


        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist

        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                let doctors = API.service('doctors', API.all('doctors'))
                let $state = this.$state
                let cancel = this.cancel
                doctors.post({
                    'names': this.names,
                    'surnames': this.surnames,
                    'identification' : this.identification,
                    'country_id' : this.country,
                    'province_id' : this.state,
                    'city' : this.city,
                    'phone': this.phone,
                    'address' : this.address,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                    swal({
                                   title: 'Médico guardado Correctamente!',
                                   type: 'success',
                                   confirmButtonText: 'OK',
                                   closeOnConfirm: false
                               }, function () {
                                   cancel()                                                                                                  
                                   swal.close()
                                   $state.reload()
                               })
                           , function (response) {
                               swal({
                                   title: 'Error Inesperado!',
                                   type: 'error',
                                   confirmButtonText: 'OK',
                                   closeOnConfirm: true
                               })
                           }
                })
            } else {
                this.formSubmitted = true
            }
        }
    }

    modalcontroller2 (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state
        this.specialtiesselected = {}

        let Countrys = API.service('countrys')
        Countrys.getList()
        .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
                systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
        })

        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist
        })

        this.getState = function(){
            
            console.log("pais",this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        /*this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }*/

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                this.processing = true
                let hospitals = API.service('hospitals', API.all('hospitals'))
                let $state = this.$state
                let cancel = this.cancel

                hospitals.post({
                    'name': this.name,
                    'country_id' : this.country,
                    'province_id' : this.state,
                    'city' : this.city,
                    'phone': this.phone,
                    'address' : this.address,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                    cancel()
                    $state.reload()
                }, function (response) {})
            } else {
                this.formSubmitted = true
            }
        }
    }

    $onInit(){
    }
}

export const TicketAddComponent = {
    templateUrl: './views/app/components/ticket-add/ticket-add.component.html',
    controller: TicketAddController,
    controllerAs: 'vm',
    bindings: {}
}
