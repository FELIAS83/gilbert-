class ApplicantListsController{
    constructor(API, DTOptionsBuilder, DTColumnBuilder, $compile, $scope){
        'ngInject';

        let applicants = API.service('applicants')
        applicants.getList({}).then((response) => {
            let dataSet = response.plain()
            console.log("la data: ", dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            this.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
                DTColumnBuilder.newColumn(null).withTitle('Nombres').renderWith(actionsHtml),
                DTColumnBuilder.newColumn('identity_document').withTitle('# de Identificación'),
                DTColumnBuilder.newColumn('gender').withTitle('Género').withOption('width', '5%'),
                DTColumnBuilder.newColumn('civil_status').withTitle('Estado Civil').withOption('width', '10%'),
                DTColumnBuilder.newColumn('email').withTitle('Correo'),
                DTColumnBuilder.newColumn('phone').withTitle('Fijo').withOption('width', '5%'),
                DTColumnBuilder.newColumn('mobile').withTitle('Móvil').withOption('width', '5%'),
                //DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(actionsHtml)
            ]

            this.displayTable = true
        })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
            return `<div style="text-align: left;">
                        <a title="Editar" ui-sref=".view({id: ${data.id}})">
                          ${data.first_name} ${data.last_name}
                        </a>
                    </div>`
        }
    }

    $onInit(){
    }
}

export const ApplicantListsComponent = {
    templateUrl: './views/app/components/applicant-lists/applicant-lists.component.html',
    controller: ApplicantListsController,
    controllerAs: 'vm',
    bindings: {}
}
