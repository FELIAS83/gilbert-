class NavSidebarController {
  constructor (AclService, ContextService, $scope) {
    'ngInject'

    var vm = this
    let navSideBar = this
    this.can = AclService.can
    this.isOpen= false;
    this.close  = () => { 
      this.isOpen = !this.isOpen;
      if(!this.isOpen){
        let menu =  document.getElementById('treeview');
        menu.style.display = 'none';
      }    
     };
    ContextService.me(function (data) {
      navSideBar.userData = data
    })
    $scope.$on('save', function(event, data){
      //console.log(data)
      navSideBar.userData = data.data
    })
    this.onError = "this.src='//placeholdit.imgix.net/~text?txtfont=monospace,bold&bg=DD4B39&txtclr=ffffff&txt=A&w=45&h=45&txtsize=16'";

  }

  $onInit () {}
}

export const NavSidebarComponent = {
  templateUrl: './views/app/components/nav-sidebar/nav-sidebar.component.html',
  controller: NavSidebarController,
  controllerAs: 'vm',
  bindings: {}
}
