class RenewalReportController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';
        this.API = API
        this.$state = $state
        var fecha = ""
        var arrfecha=""
                    
        let Historical = this.API.service('historical')
        
        Historical.getList({})
        .then((response) => {
            let dataSet = response.plain()

            angular.forEach(dataSet, function (value) {

              fecha = value.created_at
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.created_at = fecha

            })
            console.log("fecha",fecha)
                
            this.dtOptions =DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
               console.log(dataSet)
            this.dtColumns = [
              
              DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              DTColumnBuilder.newColumn('client').withTitle('Cliente'),           
              DTColumnBuilder.newColumn('plan').withTitle('Plan/Deducible/Cobertura Anterior'),
              DTColumnBuilder.newColumn('planAct').withTitle('Plan/Deducible/Cobertura Actual'),
              DTColumnBuilder.newColumn('created_at').withTitle('Fecha de Cambio'),

                ]

              this.displayTable = true
        })

         let createdRow = (row) => {
         $compile(angular.element(row).contents())($scope)
         }
 
    }

    $onInit(){
    }
}

export const RenewalReportComponent = {
    templateUrl: './views/app/components/renewal-report/renewal-report.component.html',
    controller: RenewalReportController,
    controllerAs: 'vm',
    bindings: {}
}
