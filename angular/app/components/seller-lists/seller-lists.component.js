class SellerListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject'
        this.API = API
        this.$state = $state

        let Sellers = this.API.service('sellers')
        
        Sellers.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()

            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn('type').withTitle('Tipo de Comisión').renderWith(dataComi),
              DTColumnBuilder.newColumn('percentage_vn').withTitle('Porcentaje VN').renderWith(dataPercen),
              DTColumnBuilder.newColumn('percentage_rp').withTitle('Porcentaje RP').renderWith(dataPercen),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let dataComi = (data) => {
          return `
                    <div ng-show='"${data}" == "F"'>
                      Fija 
                    </div>
                    <div ng-show='"${data}" == "V"'>
                      Variable
                    </div>
                  `
        }

        let dataPercen = (data) => {
          return `
                    <div >
                      ${data} %
                    </div>
                  `
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a title="Configuración" class="btn btn-xs btn-info" ui-sref="app.company-edit({sellerId: ${data.id}})">
                        <i class="fa fa-cog"></i>
                    </a>
                    &nbsp
                    <a title="Ventas" class="btn btn-xs btn-primary" ui-sref="app.company-edit({sellerId: ${data.id}})">
                        <i class="fa fa-shopping-cart"></i>
                    </a>
                    &nbsp
                    <a title="Comisión" class="btn btn-xs btn-success" ui-sref="app.company-edit({sellerId: ${data.id}})">
                        <i class="fa fa-money"></i>
                    </a>
                    &nbsp
                    <a title="Editar" class="btn btn-xs btn-warning" ui-sref="app.seller-edit({sellerId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }
    }

    delete (sellerId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el vendedor!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('sellers').one('seller', sellerId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const SellerListsComponent = {
    templateUrl: './views/app/components/seller-lists/seller-lists.component.html',
    controller: SellerListsController,
    controllerAs: 'vm',
    bindings: {}
}
