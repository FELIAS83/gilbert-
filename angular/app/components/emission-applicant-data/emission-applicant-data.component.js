class EmissionApplicantDataController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.current_step = '1'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.applicant_data = []
        this.applicant_data.comments = []
       // this.key_save=false
        //this.key_comment=false
        //vm.getState = getState
        //vm.getCity = getCity
        var country_id = ""
        var province_id = ""
        this.uisrefparameter = ' {policyId: vm.applicanteditdata.data[0].id}'
        this.uisrefback = 'app.emission-policy-lists'
        console.log("console.log(this.policyId)", this.uisrefparameter) 
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }      
        
        let policyId = $stateParams.policyId
        this.completed = $stateParams.completed
        this.applicant_id= policyId

        if (this.completed  == 1){
            this.uisrefparameter = ' {policyId: vm.applicanteditdata.data[0].id, completed: 1}'
            this.uisrefback = 'app.historical-emissions-lists'
        }
        //console.log(policyId)
        
        let ApplicantData = API.service('show', API.all('applicants'))
        ApplicantData.one(policyId).get()
          .then((response) => {
            this.applicanteditdata = API.copy(response)
            console.log("applicanteditdata",this.applicanteditdata.data)
            this.applicanteditdata.data[0].height = parseFloat(this.applicanteditdata.data[0].height, 2)
            this.applicanteditdata.data[0].weight = parseFloat(this.applicanteditdata.data[0].weight, 2)
            this.applicanteditdata.data[0].mobile = parseInt(this.applicanteditdata.data[0].mobile)
            this.applicanteditdata.data[0].phone = parseInt(this.applicanteditdata.data[0].phone)
            //console.log("birthday ", this.applicanteditdata.data[0].birthday)
            if (this.applicanteditdata.data[0].birthday == null) {
                this.applicanteditdata.data[0].birthday =  undefined;//new Date(this.applicanteditdata.data[0].birthday)
            }else{
                this.applicanteditdata.data[0].birthday =  new Date(this.applicanteditdata.data[0].birthday+' ')
            }
            //console.log("birthday ", this.applicanteditdata.data[0].birthday)
            country_id = this.applicanteditdata.data[0].country_id
            province_id = this.applicanteditdata.data[0].province_id
            if(this.applicanteditdata.data[0].height_unit_measurement == '1'){
                this.applicanteditdata.data[0].height_unit_measurement = true
            }else{
                this.applicanteditdata.data[0].height_unit_measurement = false
            }
            if(this.applicanteditdata.data[0].weight_unit_measurement == '1'){
                this.applicanteditdata.data[0].weight_unit_measurement = true
            }else{
                this.applicanteditdata.data[0].weight_unit_measurement = false
            }            


        let States = API.service('index', API.all('states'))
        States.one().get()
          .then((response) => {
            let systemStates = []
            let allStates = []
            let stateResponse = response.plain()
            angular.forEach(stateResponse.data.states, function (value) {
              if(value.country_id == country_id){
                systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
              }
              allStates.push({id: value.id, name: value.name, country_id: value.country_id})
            })
            this.systemStates = systemStates
            this.allStates = allStates
            //console.log("systemStates==> ", systemStates)
            //console.log("allStates==> ", allStates)


            let Cities = API.service('index', API.all('cities'))
            Cities.one().get()
              .then((response) => {
                let systemCities = []
                let allCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                  if(value.province_id == province_id){
                    systemCities.push({id: value.id, name: value.name, province_id: value.province_id})
                  }
                  allCities.push({id: value.id, name: value.name, province_id: value.province_id})
                })
                this.systemCities = systemCities
                this.allCities = allCities
                //console.log("systemCities==> ", systemCities)
                //console.log("allCities==> ", allCities)

                
            })

        })
          let applicant_data = API.one('applicant_data', this.applicant_id)
            applicant_data.get().then((response) => {
            this.applicant_data = response.plain().data
            console.log("applicant_data", this.applicant_data)
            })
        })


        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
                if ($stateParams.completed == 1){
                    if ((value.order == 1) || (value.order == 3) || (value.order == 7)){
                        systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})
                    }
                }
                else{
                    systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})

                }
            })

            this.systemWizar = systemWizar
            //console.log("systemWizar==> ", systemWizar)
        })
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            //console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            //console.log("systemCountrys==> ", systemCountrys)
        })

    }

    getState(){

        console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let States = API.service('index', API.all('states'))
        States.one(vm.applicanteditdata.data[0].country_id).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                 console.log(stateResponse)
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
            this.systemStates = systemStates
            //console.log("systemStates  ==> ", systemStates)          

        })*/
        this.rowStatus = false;
        //console.log("allStates ", this.allStates);
        //console.log(this.systemCanton);
        var country_id = this.applicanteditdata.data[0].country_id
        let systemStates = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allStates, function (value) {
          if(value.country_id == country_id){
            systemStates.push({id: value.id, name: value.name})
          }
        })
        if (country_id !== ""){
          this.rowStatus = true;
        }    
        this.systemStates = systemStates
        this.systemCities = ""
        //console.log("systemStates ", systemStates)
    }

    getCity(){
        console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let Citys = API.service('index', API.all('cities'))
        Citys.one(vm.applicanteditdata.data[0].province_id).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                 console.log(cityResponse)
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
            this.systemCities = systemCities
            //console.log("systemCities  ==> ", systemCities)          

        })*/
        this.rowStatus = false;
        console.log("allCities ", this.allCities);
        //console.log(this.systemCanton);
        var province_id = this.applicanteditdata.data[0].province_id
        let systemCities = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allCities, function (value) {
          if(value.province_id == province_id){
            systemCities.push({id: value.id, name: value.name})
          }
        })
        if (province_id !== ""){
          this.rowStatus = true;
        }    
        this.systemCities = systemCities
        //this.systemCities = ""
        console.log("systemCities ", systemCities)

    }

    save(isValid) {
        //console.log(this.key_save)

        console.log("entro en el save general")
        console.log(isValid)

        
       /* if(this.applicanteditdata.data[0].height_unit_measurement == true){
            this.applicanteditdata.data[0].height_unit_measurement = '1'
        }else{
            this.applicanteditdata.data[0].height_unit_measurement = '0'
        }
        if(this.applicanteditdata.data[0].weight_unit_measurement == true){
            this.applicanteditdata.data[0].weight_unit_measurement = '1'
        }else{
            this.applicanteditdata.data[0].weight_unit_measurement = '0'
        }*/
        //console.log("height_unit_measurement==> ", this.applicanteditdata.data[0].height_unit_measurement)
        //console.log("weight_unit_measurement==> ", this.applicanteditdata.data[0].weight_unit_measurement)
        console.log('xxx', this.applicanteditdata.data[0])
        if (isValid) {
            this.isDisabled = true;
            this.applicanteditdata.data = this.applicanteditdata.data[0]
            //console.log(this.applicanteditdata);
            let $state = this.$state
            this.applicanteditdata.put()
              .then((response) => {
                swal({
                  title: 'Datos del Solicitante Guardados Correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                }, function () {
                      $state.go('app.emission-plan-config',{"policyId": response.data})
                })
            }, (response) => {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                      $state.go($state.current)
                  })
              })
        } else {
          this.formSubmitted = true
        }
    }

    addComment(){
         console.log("entro en addComment")
                   
        if (this.comment != ''){
            var vm = this
            let comment = this.API.service('comments', this.API.all('applicants'))
            comment.post({
                'module' : 0,
                'key_field' : this.applicant_id,
                'text' : this.comment
            }).then(function () {
                vm.comment = ''
                vm.update()
          }, function (response) {
          })
        }
    }
    deleteComment(commentId) {
        this.API.one('comments').one('comments', commentId).remove()
        .then(() => {
            this.update()
        })
    }
    update(){
        let comments = this.API.service('show', this.API.all('comments'))
        comments.one(this.applicant_id).get()
        .then((response) => {
            this.applicant_data.comments = this.API.copy(response).data
        })
    }


    $onInit(){
    }
}

export const EmissionApplicantDataComponent = {
    templateUrl: './views/app/components/emission-applicant-data/emission-applicant-data.component.html',
    controller: EmissionApplicantDataController,
    controllerAs: 'vm',
    bindings: {}
}
