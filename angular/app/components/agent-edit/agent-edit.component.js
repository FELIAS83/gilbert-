class AgentEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

         var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity
        vm.countryRequired = false
        vm.provinceRequired = false
        vm.cityRequired = false
        this.processing = false
        vm.fecha_actual= new Date()
        vm.leader = []
        vm.country_id = []
        vm.province_id = []
        vm.city_id = []

     
       



        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let agentId = $stateParams.agentId

        let Agents = this.API.service('agents')
        Agents.getList()
        .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            angular.forEach(agentsResponse, function (value) {
                systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
        })


        let AgentData = API.service('show', API.all('agents'))
        AgentData.one(agentId).get()
          .then((response) => {
            this.agenteditdata = API.copy(response)
            console.log("data", this.agenteditdata)
            let agentResponse = response.plain(response)
            
            this.agenteditdata.data.commission = parseFloat(this.agenteditdata.data.commission, 2)
            vm.leader = this.agenteditdata.data.leader    
            vm.country_id = {id:this.agenteditdata.data.country_id, name:""}
            vm.province_id = {id: this.agenteditdata.data.province_id, name: ""}
            vm.city_id = {id: this.agenteditdata.data.city_id, name: ""}
            
            this.agenteditdata.data.birthday =  new Date(this.agenteditdata.data.birthday)
            this.agenteditdata.data.mobile = parseInt(this.agenteditdata.data.mobile)
            this.agenteditdata.data.phone = parseInt(this.agenteditdata.data.phone)
            /***********************************************************************/
            let States = API.service('index', API.all('states'))
             States.one().get()
                .then((response) => {
                    let systemStates = []
                    let allStates = []
                    let stateResponse = response.plain()
                    console.log("stateResponse",stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                
                if(value.country_id == vm.country_id.id){
                    systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
                }
                allStates.push({id: value.id, name: value.name, country_id: value.country_id})
                })
                this.systemStates = systemStates
                this.allStates = allStates
            
            let Cities = API.service('index', API.all('cities'))
            Cities.one().get()
              .then((response) => {
                let systemCities = []
                let allCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                  if(value.province_id == vm.province_id.id){
                    systemCities.push({id: value.id, name: value.name, province_id: value.province_id})
                  }
                  allCities.push({id: value.id, name: value.name, province_id: value.province_id})
                })
                this.systemCities = systemCities
                this.allCities = allCities
            
            })

        })
            /************************************************************************/
               
      })
      
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            //console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            //console.log("systemCountrys==> ", systemCountrys)
        })

           

    function  getState(){
        console.log("Entro..", vm.country_id)
       
        this.rowStatus = false;
       
        var country_id = vm.country_id
        let systemStates = []
       
        angular.forEach(this.allStates, function (value) {
          if(value.country_id == vm.country_id.id){
            systemStates.push({id: value.id, name: value.name})
          }
        })
        if (vm.country_id.id !== ""){
          this.rowStatus = true;
        }    
        this.systemStates = systemStates
        this.systemCities = ""
        vm.province_id = {id:0, name:""}
   
    }


    function getCity(){
       // console.log("Entro aquiiiiiiiiiiiiiiiiiii",vm.province_id)
        this.rowStatus = false;      
        var province_id = vm.province_id
        let systemCities = []
      
        angular.forEach(this.allCities, function (value) {
          if(value.province_id == vm.province_id.id){
            systemCities.push({id: value.id, name: value.name})
          }
        })
        if (vm.province_id.id !== ""){
          this.rowStatus = true;
        }    
        this.systemCities = systemCities
      
        //console.log("systemCities ", systemCities)

    }      

           
        //
    }//fin constructor

    

     save (isValid) {
        console.log("entro", this.country_id)
        this.agenteditdata.data.country_id = this.country_id.id
        this.agenteditdata.data.province_id = this.province_id.id
        this.agenteditdata.data.city_id = this.city_id.id
        this.agenteditdata.data.leader = this.leader
                if (isValid) {
                    this.processing = true
                    let vm = this
                    console.log("la data, ", this.agenteditdata);
                    let $state = this.$state
                    this.agenteditdata.put()
                        .then(() => {
                        vm.processing = false
                        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se han actualizado los datos del agente.' }
                        $state.go($state.current, { alerts: alert})
                    }, (response) => {
                        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                        $state.go($state.current, { alerts: alert})
                    })
                } else {
                
                this.formSubmitted = true
            }

        }


    $onInit(){
    }
}

export const AgentEditComponent = {
    templateUrl: './views/app/components/agent-edit/agent-edit.component.html',
    controller: AgentEditController,
    controllerAs: 'vm',
    bindings: {}
}
