class ApplicantTabsController{
    constructor($stateParams, API, DTOptionsBuilder, DTColumnBuilder, $compile, $scope, $state){
        'ngInject';

        this.API = API
        this.active_tab = 1
        this.applicant_id = $stateParams.id
        this.applicant_data = []
        this.applicant_data.comments = []

        let applicant_data = API.one('applicant_data', this.applicant_id)
        applicant_data.get().then((response) => {
            this.applicant_data = response.plain().data
            console.log("applicant_data", this.applicant_data)

            if (this.applicant_data.info.birthday == null) {
                this.applicant_data.info.birthday =  undefined;
            }else{
                this.applicant_data.info.birthday =  new Date(this.applicant_data.info.birthday)
                this.applicant_data.info.birthday.setDate(this.applicant_data.info.birthday.getDate() + 1)
            }
            this.applicant_data.info.height = parseFloat(this.applicant_data.info.height, 2)
            this.applicant_data.info.weight = parseFloat(this.applicant_data.info.weight, 2)

            this.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('data', this.applicant_data.info.dependents)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withBootstrap()
            .withOption('autoWidth', false)

            this.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
                DTColumnBuilder.newColumn('first_name').withTitle('Nombres'),
                DTColumnBuilder.newColumn('last_name').withTitle('Apellidos'),
                DTColumnBuilder.newColumn('gender').withTitle('Género').withOption('width', '5%'),
                DTColumnBuilder.newColumn('identity_document').withTitle('# del documento').withOption('width', '15%'),
                DTColumnBuilder.newColumn('relationship').withTitle('Relación'),
                DTColumnBuilder.newColumn('document_type').withTitle('Tipo de documento').withOption('width', '16%'),
                DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(actionsHtml)
            ]

            this.displayTable = true

            let createdRow = (row) => {
              $compile(angular.element(row).contents())($scope)
            }

            this.dtOptions2 = DTOptionsBuilder.newOptions()
            .withOption('data', this.applicant_data.claims)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withBootstrap()
            .withOption('autoWidth', false)

            this.dtColumns2 = [
                DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
                DTColumnBuilder.newColumn('amount').withTitle('Monto').withOption('width', '10%'),
                DTColumnBuilder.newColumn('description').withTitle('Descripción')
            ]

            this.displayTable2 = true

            let createdRow2 = (row) => {
              $compile(angular.element(row).contents())($scope)
            }

            this.dtOptions3 = DTOptionsBuilder.newOptions()
            .withOption('data', this.applicant_data.tickets)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withBootstrap()
            .withOption('autoWidth', false)

            this.dtColumns3 = [
                DTColumnBuilder.newColumn('id').withTitle('ID Ticket').withOption('width', '10%'),
                DTColumnBuilder.newColumn('ticket_type.name').withTitle('Tipo'),
                DTColumnBuilder.newColumn('date_start').withTitle('Fecha de Cita'),
              DTColumnBuilder.newColumn('created_at').withTitle('Fecha de Creación')
            ]

            this.displayTable3 = true

            let createdRow3 = (row) => {
              $compile(angular.element(row).contents())($scope)
            }
        })

        let actionsHtml = (data) => {
            return `<ul>
                        <a  title="Info Dependiente" class="btn btn-xs btn-primary" href="#/applicant-lists/view/${data.applicant_id}/dependent/${data.id}">
                        <i class="fa fa-eye"></i>
                        </a>
                    </ul>`
        }
    }
    setActive(tab){
        this.active_tab = tab
    }
    addComment(){
        if (this.comment != ''){
            var vm = this
            let comment = this.API.service('comments', this.API.all('applicants'))
            comment.post({
                'module' : 0,
                'key_field' : this.applicant_id,
                'text' : this.comment
            }).then(function () {
                vm.comment = ''
                vm.update()
          }, function (response) {
          })
        }
    }
    deleteComment(commentId) {
        this.API.one('comments').one('comments', commentId).remove()
        .then(() => {
            this.update()
        })
    }
    update(){
        let comments = this.API.service('show', this.API.all('comments'))
        comments.one(this.applicant_id).get()
        .then((response) => {
            this.applicant_data.comments = this.API.copy(response).data
        })
    }

    $onInit(){
    }
}

export const ApplicantTabsComponent = {
    templateUrl: './views/app/components/applicant-tabs/applicant-tabs.component.html',
    controller: ApplicantTabsController,
    controllerAs: 'vm',
    bindings: {}
}
