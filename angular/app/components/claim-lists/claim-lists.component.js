class ClaimListsController{
 constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';

        this.API = API
        this.$state = $state
        let claimStatus = '0'
        this.$scope=$scope
        var fecha = ""
        var arrfecha=""        

        let Policies = this.API.service('claims')
        
        Policies.getList({claimStatus})
          .then((response) => {
            let dataSet = response.plain()
            this.seleccionados=[];

            this.lista=dataSet
            console.log("la data ", dataSet.length)

            angular.forEach(dataSet, function (value) {

              fecha = value.created_at
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.created_at = fecha
            })
            console.log("fecha",fecha)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID Ticket').withOption('width', '10%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Titular').withOption('width', '35%'),
              DTColumnBuilder.newColumn('amount').withTitle('Monto').renderWith(addAmount).withOption('width', '10%'),
              DTColumnBuilder.newColumn('created_at').withTitle('Creación del Trámite').withOption('width', '16%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml).withOption('width', '11%')
            ]

            this.displayTable = true
            this.length = dataSet.length
          })

         
         // console.log(lista);
        

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let addAmount = (data) => {
          return `
                    <div class="">
                      $ {{${data} |  number:2}}
                    </div>
                    `
        }

        let actionsHtml = (data) => {
          return `
                    <a class="btn btn-xs btn-warning" ui-sref="app.claim-edit({claimId: ${data.id}, policyId: ${data.policy_id} })" title="Editar Ticket">
                    <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <input type="checkbox" style="height: 23px;width: 23px;top: 8.5px; position:relative;" ng-change="vm.selectOne(${data.id})" title="Seleccionar Ticket" ng-model="vm.seleccionados[${data.id}]" name="claim" value="${data.id}" required />
                    &nbsp
                    <button style="color: #FE2E2E; background-color: #FFF;" title="Cerrar Reclamo" class="btn btn-xs btn-danger" ng-click="vm.closed(${data.id})">
                        <i class="fa fa-times"></i>
                    </button>
                  `
        }
        //console.log(JSON.stringify(this.seleccionados))
    }

    selectAll (){
      if (this.all) {
        for (var i = 0; i < this.lista.length; i++) {
            this.seleccionados[this.lista[i].id] = true
        }
      }else{
        this.seleccionados=[];
      }      
        
    }

    selectOne (id){
      //console.log("select ", this.seleccionados)
      var tickets = 0
      //this.all = false
      for (var i = 0; i < this.seleccionados.length; i++) {
        if (this.seleccionados[i] !== undefined) {
          tickets ++
          //this.all = true
        }
          
      }
      //console.log(tickets)
      if (tickets == 0) {
        this.seleccionados=[];
      }

      
      if (!this.seleccionados[id]) {
        this.all = false
      }
      if (this.lista.length == tickets){
        this.all = true
      }
    }
  
    dispatch (claimId) {
    let API = this.API
    let $state = this.$state
    //this.seleccionados = this.seleccionados.shift();    
    var claims = ''
    angular.forEach(this.seleccionados, function (value, key) {
        //console.log("value ", key)
        if (value) {
          claims = claims + ", " + key
        }            
    })
    claims = claims.substr(1);
    //console.log("claims ", claims)
    var arregloclaims=claims.split(',');
   

    swal({
      title: 'Está seguro?',
      text: 'Se despacharán los siguientes trámites a Reclamos: </br> <b> ' + claims + '</b>.',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Sí, despachar!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: true
    }, function () {
      for (var i = 0; i < arregloclaims.length; i++) {
        //arregloclaims[i]
      
      API.one('claims').one('claim', arregloclaims[i]).remove()
        .then(() => {
          swal({
            title: 'Despachado!',
            text: 'El(los) trámite(s) '+claims+' ha(n) sido enviado(s) a Reclamos.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })
      }   
    })
  }

  closed (claimId) {
    let API = this.API
    let $state = this.$state
    //this.seleccionados = this.seleccionados.shift();    
    let Claims = this.API.service('closedClaim')

    //console.log("claim ", Claims)   

    swal({
      title: 'Está seguro?',
      text: 'Se cerrará el Ticket.',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Sí, cerrar!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: true
    }, function () {
      console.log("aca")
      //let Claims = this.API.service('closedClaim')
        Claims.post({
        'id_claim' : claimId
      }).then(function (response) {
            swal({
            title: 'Cerrado!',
            text: 'El Ticket ha sido cerrado.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
      }, function (response) {
      })
    })
  }

    $onInit(){
    }
}

export const ClaimListsComponent = {
    templateUrl: './views/app/components/claim-lists/claim-lists.component.html',
    controller: ClaimListsController,
    controllerAs: 'vm',
    bindings: {}
}
