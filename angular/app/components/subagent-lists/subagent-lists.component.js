class SubagentListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
        this.API = API
        this.$state = $state
        this.can = AclService.can


        let Subgents = API.service('subagents')
        Subgents.getList({})
            .then((response) => {

            let dataSet = response.plain()
            console.log(dataSet)
            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()


            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('first_name').withTitle('Nombres'),
              DTColumnBuilder.newColumn('last_name').withTitle('Apellidos'),
              DTColumnBuilder.newColumn('identity_document').withTitle('N° de Identificación'),
              DTColumnBuilder.newColumn('mobile').withTitle('Movil'),              
              DTColumnBuilder.newColumn('phone').withTitle('Telefono'),              
              DTColumnBuilder.newColumn('email').withTitle('E-mail'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

            let createdRow = (row) => {
                $compile(angular.element(row).contents())($scope)
            }

            let actionsHtml = (data) =>  {
                return`<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.subagent-edit({subagentId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
            }


        //
    }//Fin de controlador

     delete (subagentId) {
        let API = this.API
        let $state = this.$state

        swal({
            title: 'Está seguro?',
            text: 'Se eliminará el Sub-Agente!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Sí, elimínalo!',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function () {
            API.one('subagents').one('subagent', subagentId).remove()
            .then(() => {
                swal({
                title: 'Eliminado!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {
                $state.reload()
            })
          })
      })
    }


    $onInit(){
    }
}

export const SubagentListsComponent = {
    templateUrl: './views/app/components/subagent-lists/subagent-lists.component.html',
    controller: SubagentListsController,
    controllerAs: 'vm',
    bindings: {}
}
