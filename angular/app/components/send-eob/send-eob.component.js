class SendEobController{
    constructor($state,$stateParams,API,$http){
        'ngInject';

        let vm = this
        this.$state = $state
        this.alerts = []
        this.$http=$http
        this.API = API
        this.formSubmitted = false
        vm.isDisabled =  false
        vm.allClaims = []
        vm.amountDeduc = []
        vm.cover = []
        vm.discount = []
        vm.changeTotal = changeTotal
        vm.applicant_name = ""
        vm.insured_name = ""
        vm.policyNumber = ""
        vm.policyId = ""
        vm.claimId = ""
        vm.docClaimId=""
        vm.selectInvoice = selectInvoice
        vm.eob = []
        vm.observation = ""
        vm.indice=[]
        vm.diagnosticId = diagnosticId
        vm.provider = []
        vm.myFile = []
        vm.files = []
        vm.myFile2 = []
        vm.files2 = []
        this.lines = []
        this.lines.push({id : 0});
        this.newlines = []
        this.uploadFile = uploadFile
        
    
        vm.amount = 0

         if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        //vm.changeDiscount = changeDiscount
        //vm.changeCover = changeCover
        let disabled
        let checkInvoice

        let claimId = $stateParams.claimId
        let policyId = $stateParams.policyId
        let diagnosticId = $stateParams.diagnosticId
        vm.policyId = policyId
        vm.claimId = claimId
        vm.diagnosticId = diagnosticId

        console.log("Reclamo", claimId)
        console.log("Poliza", policyId)
        console.log("Diagnostico", diagnosticId)


        let systemPayTypes = []
        systemPayTypes.push({id: "1", name: "Cheque"})
        systemPayTypes.push({id: "2", name: "Transferencia"})
        this.systemPayTypes = systemPayTypes
        console.log(this.systemPayTypes)

        let Documents = API.service('eobdata', API.all('claims'))
        Documents.one(diagnosticId).get()
            .then((response) => {
                let dataSet = API.copy(response)
                console.log("dataSet", dataSet)
                console.log(dataSet.data.length)
                vm.long=dataSet.data.length
                console.log("longitud",vm.long)
                
                
                let systemamountDeduc = []
                for (let i = 0 ; i < dataSet.data.length ; i++) {

                    if(dataSet.data[i].docDiagLiquidated == 0){
                        disabled = true
                        checkInvoice = true
                    }else{
                        disabled = false
                        checkInvoice = false
                    }

                    if(dataSet.data[i].eob_number == 0){
                        dataSet.data[i].eob_number = ""
                    }

                    vm.allClaims.push({id: i, diagnostic_doc_id: dataSet.data[i].diagnostic_doc_id, claim_id: dataSet.data[i].id ,eob: dataSet.data[i].eob_number, amount: dataSet.data[i].amount, total: dataSet.data[i].amount, checkInvoice: checkInvoice, disabled : disabled, liquidated: dataSet.data[i].liquidated, returned: dataSet.data[i].returned})
                    vm.eob.push({id: i, eob: dataSet.data[i].eob_number})
                    vm.amount += parseFloat(vm.allClaims[i].amount)


                }
                
            
                 //vm.amountDeduc = systemamountDeduc
                 console.log("REclamos",   vm.allClaims)
                 //console.log("EOBs", vm.eob)
                 //console.log("vm.amountDeduc",   vm.amountDeduc)

                
                    let Policy = API.service('show', API.all('policies'))
                    Policy.one(policyId).get()
                        .then((response) =>{
                             let policydata =  API.copy(response)
                             vm.applicant_name = policydata.data[0].first_name+' '+ policydata.data[0].last_name
                             vm.email = policydata.data[0].email
                             vm.policyNumber = policydata.data[0].number
                             console.log("Poliza", policydata)  
                   
                               if(dataSet.data[0].person_type == "A"){

                                    vm.insured_name =  vm.applicant_name

                                }else{
                                    console.log("id dependentsid", dataSet.data[0].id_person)

                                    let dependentId = dataSet.data[0].id_person
                                    let Dependent = API.service('dependentsid', API.all('dependents'))
                                    Dependent.one(dependentId).get()
                                        .then((response) =>{
                                            let insuredata =  API.copy(response)
                                            console.log("Asegurado", insuredata)  
                                            vm.insured_name = insuredata.data[0].first_name+' '+ insuredata.data[0].last_name
                                            //console.log("Asegurado", insuredata)  
                                        })
                          
                                }   

                        })

                        
                   
              


                 
        })
            let emailformatId = 12
            let EmailFormat = API.service('show', API.all('email_formats'))
            EmailFormat.one(emailformatId).get()
                .then((response) => {
                this.emailformatdata = API.copy(response)
                //console.log("emailformatdata", JSON.stringify(this.emailformatdata))
            })    

        //
    function changeTotal(index){
       let amountTotal = 0 
       if(typeof vm.amountDeduc[index] == "undefined"){
            vm.amountDeduc[index] = 0
        }

        if(typeof vm.discount[index] == "undefined"){
            vm.discount[index] = 0
        }
        if(typeof vm.cover[index] == "undefined"){
            vm.cover[index] = 0
        }
        if(typeof vm.provider[index] == "undefined"){
            vm.provider[index] = 0
        }
       
        vm.allClaims[index].total = vm.allClaims[index].amount - vm.amountDeduc[index] - vm.discount[index] - vm.cover[index] - vm.provider[index]


        for (var i = 0; i < vm.allClaims.length; i++) {
                if(vm.allClaims[i].checkInvoice == true){
                    amountTotal += vm.allClaims[i].total
                }
            }

        /*for (var i = 0; i < vm.allClaims.length ; i++) {
            amountTotal += vm.allClaims[i].total
            
        }*/


        vm.amount = parseFloat(amountTotal)


    }

    function selectInvoice(index){
      let count = 0
      let numOfReg = vm.allClaims.length
      let amountTotal = 0 
      
      if(vm.allClaims[index].checkInvoice)
        {
            vm.allClaims[index].disabled = true    
          
        }else{
            vm.allClaims[index].disabled = false    
            
        }

    for (var i = 0 ; i < vm.allClaims.length; i++) {

        if(vm.allClaims[i].disabled == false){
            count++ 
        }else{
            count--
        }
    }
        if (count == vm.allClaims.length) {
            vm.isDisabled = true
        }else{
            vm.isDisabled = false
        }

        for (var i = 0; i < vm.allClaims.length; i++) {
            if(vm.allClaims[i].checkInvoice == true){
                amountTotal += vm.allClaims[i].total
            }
        }
   
        vm.amount = parseFloat(amountTotal)   

    }

  function uploadFile(){
            var i = 0
            var files = vm.files//archivos nuevos
            var files2 = vm.files2//archivos que sustituiran a los anteriores
            var cantFiles = vm.files2.length
            var uploaded = 0
            if (files.length == 0 && files2.length == 0){
                send()
            }else{    

            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('diagnostic_id', vm.diagnosticId);
                $http.post('uploadeob', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    if (files2.length == 0){
                        send()
                    }else{
                        angular.forEach(files2, function(files2){
                        var form_data = new FormData();
                        angular.forEach(files2, function(file){
                            form_data.append('file', file);
                            })
                        form_data.append('diagnostic_id', vm.diagnosticId);
                        $http.post('uploadeob', form_data,
                            {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                        }).success(function(response){
                            uploaded++
                            if(uploaded == cantFiles){
                                send()       
                            }
                            
                        })
                        
                    })


                    }
                })
            })
          }

            
         }  
function send(){
            API.service('send').post({
                        'policy_id' : vm.policyId,
                        'format_id' : 12,
                        'allClaims' : vm.allClaims,
                        'amountDeduc' : vm.amountDeduc,
                        'discount' :  vm.discount,
                        'cover' : vm.cover,
                        'insured_name':  vm.insured_name, 
                        'eob': vm.eob,
                        'observation': vm.observation,
                        'bank': vm.bank,
                        'check_number':vm.check,
                        'amount':vm.amount,
                        'payment_type_id':vm.systemPayTypes.id,
                        'indice':vm.indice,
                        'provider': vm.provider,
                        'myFile' : vm.filenames,
                        'diagnostic_id': vm.diagnosticId                
                    })
                    .then( ( response ) => {             
                        swal({
                        title: 'Enviado!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                        }, function () {
                        $state.go('app.claim-liquidations', {})
                        })
                    }, function (response) {
                      console.log('error', response.data)
                      swal({
                        title: 'Ha ocurrido un erro al enviar el correo!',
                        type: 'error',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                        }, function () {
                        $state.go('app.claim-liquidations', {})
                        })
                      }   
                    )
        }



    }

    paydata(){
         let amountTotal = 0
        var tipodepago= document.getElementById('payment_type')

            var bank=document.getElementById('bank')
            var check=document.getElementById('check')
            var amount=document.getElementById('amount')
            var thbank=document.getElementById('thbank')
            var thcheck=document.getElementById('thcheck')
            var thamount=document.getElementById('thamount')
            var divamount=document.getElementById('divamount')

        //alert(tipodepago.value)

        
        if (tipodepago.value==0){
           //alert("0-seleccione ")
            bank.style.visibility='hidden';
            check.style.visibility='hidden';
            amount.style.visibility='hidden';
            thbank.style.visibility='hidden';
            thcheck.style.visibility='hidden';
            thamount.style.visibility='hidden';
            divamount.style.visibility='hidden';

        }
        else if (tipodepago.value==1){
            //alert("1-cheque ")
            bank.style.visibility='visible';
            check.style.visibility='visible';
            amount.style.visibility='visible';
            thbank.style.visibility='visible';
            thcheck.style.visibility='visible';
            thamount.style.visibility='visible';
            divamount.style.visibility='visible';


        }
        else{
            //alert("2-transferencia")
            amount.style.visibility='visible';
            bank.style.visibility='hidden';
            check.style.visibility='hidden';
            thamount.style.visibility='visible';
            thbank.style.visibility='hidden';
            thcheck.style.visibility='hidden';
            divamount.style.visibility='visible';
        }

        /* for (var i = 0; i < this.allClaims.length; i++) {
                if(this.allClaims[i].checkInvoice == true){
                    this.amount = this.allClaims[i].amount
                }
            }*/

        for (var i = 0; i < this.allClaims.length; i++) {
            if(this.allClaims[i].checkInvoice == true){
                amountTotal += this.allClaims[i].total
            }
        }
        this.amount = parseFloat(amountTotal)     
    }

    addFile() {
        if (this.newlines.length==0){
            var newid = 0
        }
        else{
            var newid = this.newlines.length
        }
        this.newlines.push({id : newid});
        //this.typeDocNew.push({id : newid, value: 1})
    }

      deleteLine(index){//eliminar de los archivos nuevos agregados
        
        this.newlines.splice(index,1)
        this.files2.splice(index,1)
        this.myFile2.splice(index,1)
       
    }

    save(isValid){
        
        var files = this.files
        var files2 = this.files2

        if (isValid){
            let API = this.API
            let $state = this.$state
            let $http = this.$http
            let vm = this
            let amountDeduc =  vm.amountDeduc
            let allClaims = vm.allClaims
            let eob = vm.eob 
            let Long= vm.long
            let uploadFile = this.uploadFile
            console.log("Archivos", vm.myFile)

            let filenames = []
             angular.forEach(vm.myFile, function (value) {
                      filenames.push(value.name)
                })
                angular.forEach(vm.myFile2, function (value) {
                      filenames.push(value.name)
                })
            vm.filenames = filenames 

        swal({
            title: 'Está seguro?',
            text: 'Desea enviar el EOB al Cliente!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Sí',
            closeOnConfirm: false,
            closeOnCancel:false,
            showLoaderOnConfirm: true,
            html: false
        },function (isConfirm) {
            if (isConfirm){

                for (var i = 0 ; i < vm.allClaims.length; i++) {

                    if(vm.allClaims[i].disabled == false){
                        vm.indice[i]=i;
                    }

                    if( vm.amountDeduc[i] == null){
                       vm.amountDeduc[i] = 0
                    }
                    
                    if( vm.discount[i] == null ){
                      vm.discount[i] = 0
                    }
                    
                    if( vm.cover[i] == null){
                       vm.cover[i] = 0
                    }

                    if( vm.provider[i] == null){
                       vm.provider[i] = 0
                    }
                }

                for ( var i=0; i < vm.allClaims.length; i++){

                    if ( vm.indice[i] == null ){
                        
                        let Docs = API.service('updateEob');
                        var form_data = new FormData();
                        form_data.append('eob_number',eob[i].eob);
                        form_data.append('docDiagnosticId',allClaims[i].diagnostic_doc_id);
                        form_data.append('diagnosticId', vm.diagnosticId);
                    $http.post('updateEob', form_data,
                        {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                        })
                    }
                }             
            

                if (vm.systemPayTypes.id == "2"){


                    let Eob = API.service('eob', API.all('eob'))                   
                Eob.post({
                    'id_claim': vm.claimId,
                    'diagnostic_id': vm.diagnosticId,
                    'observation': vm.observation,
                    'bank': vm.bank,
                    'check_number':vm.check,
                    'payment_type':vm.systemPayTypes.id,
                    'amount':vm.amount,
                    'myFile': vm.filenames
                }).then(function () {
                    //vm.processing = false
                    console.log("mensaje", $state.current);
                    let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha guardado el Eob.' }
                    $state.go($state.current, { alerts: alert})
                    console.log($state.current);
                    }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
                })
                    uploadFile()
                    
                }
                else if (vm.systemPayTypes.id == "1"){                         
                    //console.log("Diagnosticos", diagnosticId)
                    console.log("cheque", vm.check)
                    function printDoc(){
                    let Document = API.service('printdocpdf', API.all('eob'))
                    Document.post({
                        policy_id : vm.policyId,
                        format_id : 12,
                        allClaims : vm.allClaims,
                        amountDeduc : vm.amountDeduc,
                        discount :  vm.discount,
                        cover : vm.cover,
                        insured_name:  vm.insured_name, 
                        eob: vm.eob,
                        observation: vm.observation,
                        bank: vm.bank,
                        check_number:vm.check,
                        amount:vm.amount,
                        payment_type_id:vm.systemPayTypes.id,
                        indice:vm.indice,
                        provider: vm.provider,
                        myFile : vm.filenames,
                        diagnostic_id: vm.diagnosticId
                    })
                    .then((response) => {

                      let contentDoc =  API.copy(response)
                      contentDoc = contentDoc.data
                      console.log(contentDoc)
                      /*let popupWinindow 
                      popupWinindow = window.open('', '_blank','width=800,height=800,scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no'); 
                      popupWinindow.document.open(); 
                      popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" /></head><body onload="window.print();">'+contentDoc.data+'</html>');  
                      popupWinindow.document.close();
                       */
                        var doc = new jsPDF('p', 'mm', 'letter');
                        var size1 = 10
                        var size2 = 14
                        var sizeT = 10

                        doc.setFontSize(size1);
                        let x_ini = 20
                        let x_fin = 210
                        let y_ini = 20 
                        let sizeText = 0  

                        var rect1_width = 0
                        var rect2_width = 0
                        var x1_rect = 0  // posicion inicial x de la celda para el total en el 1er cuadro
                        var x2_rect = 0  // posicion inicial x de la celda para el total en el 2do cuadro
                        var cell_y = 0  //posicion Y de la celda para el total en el 1er cuadro

                        var data = []
                        var data2 = []

                         //
                    sizeText = doc.getStringUnitWidth (contentDoc.date) * size1
                    sizeText = sizeText / 2.81
                    let x = x_fin - sizeText  - 10 
                    doc.text(x, 20, contentDoc.date); // fecha
                  // 
                    doc.text(x_ini, y_ini+20, 'Señores');
                    doc.setFontType("bold");
                    doc.text(x_ini, y_ini+24, contentDoc.applicant);
                    doc.setFontType('normal');
                    doc.text(x_ini, y_ini+28, 'Ciudad.-');
                  //
                    y_ini = y_ini + 30
                    doc.text(x_ini, y_ini+20, 'Estimado Cliente:');
                    doc.text(x_ini, y_ini+30, 'Es un placer informarle que su reclamo ha sido procesado y liquidado a traves de la Compañia Best Doctors S.A.', "justify");
                    doc.text(x_ini, y_ini+40, 'Adjuntamos la Explanation Of Benefits / Explicación de Beneficios con el siguiente detalle: ');
                  //
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+55, 'Asegurado: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+55, contentDoc.affiliate);
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+60, 'Póliza: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+60,  contentDoc.policy_number);  
                  //  
                  // tabla
                   
                    var columns1 = [{title: "N° EOB", dataKey: "eob"}, {title: "Monto del Reclamo", dataKey:"amount"}, {title: "Deducible", dataKey:"deductible"}, {title: "Descuentos", dataKey:"discount"}, {title:"N° Cubierto", dataKey:"cover"}, {title: "Pago a Proveedor", dataKey: "provider"}, {title: "Reembolso al Cliente", dataKey:"total"}];
                    angular.forEach(contentDoc.table, function(value){
                      
                      data.push({eob: value.eob, amount: value.amount, deductible:value.deductible, discount:value.discount, cover: value.cover,  provider:value.provider, total:value.total})

                    })
                    
                    // construccion
                     doc.autoTable(columns1,data,
                                    {theme: 'plain', //plain, grid
                                    bodyStyles: {valign: 'middle', halign: 'center'},
                                    tableWidth : 'wrap',
                                    columnStyles: {
                                        0:{overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                        1: {overflow: 'linebreak', columnWidth:15, halign: 'center',valign: 'middle'},
                                        2:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                        3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'},
                                        4: {overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                        5:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                        6:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'}
                                    },
                                        headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                        },
                                        styles: {fontSize: 9, lineWidth: 0.5,  halign:'center'} ,  
                                        margin:{ top: y_ini+70, right: 40 },
                                        drawCell: function(cell, data){
                                         cell_y = cell.y // conocer el ultimo valor de y al crear la tabla
                                        }

                                })
                     //
                     // observacion
                     console.log("cell_y", cell_y)
                     if(contentDoc.observation.length > 0){
                        doc.setFontType('bold');
                        doc.text(x_ini, cell_y+15, 'Observación: ');
                        doc.setFontType('normal');
                        doc.text(x_ini+30, cell_y+15, contentDoc.observation, "justify");
                        cell_y = cell_y + 10
                     }else{
                        cell_y = cell_y + 10
                     }
                     //
                        doc.text(x_ini, cell_y+20, "Reclamo cancelado con cheque No. "+vm.check+", del Banco "+ vm.bank +" por un monto de $"+ contentDoc.totalCount+".", 'justify')
                        doc.text(x_ini, cell_y+25, "En caso de presentarse alguna inquietud, con relacion a esta EOB, no dude en contactarnos .");                     
                    //
                    // final
                    doc.text(x_ini, cell_y+35, "Aprovechamos esta oportunidad, para agradecerle por elegirnos como sus asesores en un tema tan delicado");                     
                    doc.text(x_ini, cell_y+40, "como su salud y la de sus seres queridos");

                    doc.text(x_ini, cell_y+60, "Atentamente,");
                    doc.setFontType('bold');
                    doc.text(x_ini, cell_y+75, "María Beatriz Rendón");
                    doc.setFontType('normal');
                    doc.text(x_ini, cell_y+80, "Directora de Servicio al Cliente");

                    //
                    if(!!window.chrome && !!window.chrome.webstore){
                     window.open(doc.output('bloburl'), '_blank')
                    }else{
                     doc.output('dataurlnewwindow',{})       //creacion de los archivos
                    }
                    })
                }               
                                //uploadFile() 
                                /*upload*/
                        var files2 = vm.files2 
                        console.log("files2", files2)       
                        console.log("files2,length", files2.length)    
                        angular.forEach(files, function(files){
                        var form_data = new FormData();
                        angular.forEach(files, function(file){
                        form_data.append('file', file);
                        })
                        form_data.append('diagnostic_id', vm.diagnosticId);
                        $http.post('uploadeob', form_data,
                        {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined,'Process-Data': false}
                        }).success(function(response){
                        if (files2.length > 0){
                           angular.forEach(files2, function(files2){
                            var form_data = new FormData();
                            angular.forEach(files2, function(file){
                            form_data.append('file', file);
                            })
                            form_data.append('diagnostic_id', vm.diagnosticId);
                            $http.post('uploadeob', form_data,
                            {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined,'Process-Data': false}
                            }).success(function(response){ 
                            })
                        })    
                        }
                        })
                        })
                                /**/ 
                                printDoc()
                                let Eob = API.service('eob', API.all('eob'))                   
                                Eob.post({
                                    'id_claim': vm.claimId,
                                    'diagnostic_id': vm.diagnosticId,
                                    'observation': vm.observation,
                                    'bank': vm.bank,
                                    'check_number':vm.check,
                                    'payment_type':vm.systemPayTypes.id,
                                    'amount':vm.amount,
                                    'myFile':vm.filenames
                                }).then(function () {
                                    //vm.processing = false
                                       swal({
                                       title: 'EOB enviado Correctamente!',
                                       type: 'success',
                                       confirmButtonText: 'OK',
                                       closeOnConfirm: false
                                   }, function () {                                                                                                  
                                       swal.close()
                                       $state.go('app.claim-liquidations', {})
                                   })
                               , function (response) {
                                   swal({
                                       title: 'Error Inesperado!',
                                       type: 'error',
                                       confirmButtonText: 'OK',
                                       closeOnConfirm: true
                                   }, function () {})
                               }
                          })
                                
                        }  
                    let claimStep = API.service('claimstep', API.all('claims'))  
                    claimStep.post({
                        'claimId': vm.claimId,
                        'diagnostic_id': vm.diagnosticId,
                    }).then(function (response){})
                    }//fin de if confirm
                })
            }                      
        }
    
    $onInit(){
    }
}

export const SendEobComponent = {
    templateUrl: './views/app/components/send-eob/send-eob.component.html',
    controller: SendEobController,
    controllerAs: 'vm',
    bindings: {}
}
