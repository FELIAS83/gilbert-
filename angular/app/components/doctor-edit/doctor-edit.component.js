class DoctorEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.specialties = []
        this.processing = false
        this.getState = getState

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let doctorId = $stateParams.doctorId


        let specialties = API.all('medicalspecialties')
        specialties.getList()
      .then((response) => {
        let specialtieslist = []
        let specialties = response.plain()

        angular.forEach(specialties, function (value) {
          specialtieslist.push({id: value.id, name: value.name})
        })

        this.specialties = specialtieslist
      })


      let doctorData = API.service('show', API.all('doctors'))//ojo
      doctorData.one(doctorId).get()
        .then((response) => {
          this.doctoreditdata = API.copy(response)
          console.log("doctoreditdata",this.doctoreditdata.data)

        })
    

    
    let Countrys = API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
            console.log('paises',this.systemCountrys)
        })

        function getState(){
            console.log("Entro..", this.doctoreditdata.data.country)
            let States = API.service('province', API.all('states'))
            States.one(this.doctoreditdata.data.country_id).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                     console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        systemStates.push({id: value.id, name: value.name})
                    })
                this.systemStates = systemStates
                console.log("systemStates  ==> ", this.systemStates)

                })

        }
        /*function getCity(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.doctoreditdata.data.province_id).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                     console.log(cityResponse)
                    angular.forEach(cityResponse.data.cities, function (value) {
                        systemCities.push({id: value.id, name: value.name})
                    })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", this.systemCities)

                })

        }*/
        }
      
        save(isValid){

        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.doctoreditdata);
          let $state = this.$state
          this.doctoreditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el médico.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const DoctorEditComponent = {
    templateUrl: './views/app/components/doctor-edit/doctor-edit.component.html',
    controller: DoctorEditController,
    controllerAs: 'vm',
    bindings: {}
}
