class DeductibleDetailsEditController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        //vm.blockAge = true
        //vm.blockQuantity = false
        //vm.hideAges = hideAges
        //vm.showAges = showAges
        //vm.child_quantity = 0
        //vm.child_quantity = {id:1, name:1}

          if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleDetailId = $stateParams.deductibleDetailId

        let DeductibleData = API.service('show', API.all('deductible_details'))
        DeductibleData.one(deductibleDetailId).get()
          .then((response) => {
            this.deductibleeditdata = API.copy(response)
            this.deductibleeditdata.data.age_start = parseFloat(this.deductibleeditdata.data.age_start, 2)
            this.deductibleeditdata.data.age_end = parseFloat(this.deductibleeditdata.data.age_end, 2)
            this.deductibleeditdata.data.amount = parseFloat(this.deductibleeditdata.data.amount, 2)
            console.log("Data de deducible", this.deductibleeditdata.data)

            

            let Deductible = API.service('show', API.all('deductibles'))
            Deductible.one(this.deductibleeditdata.data.deductible_id).get()
            .then((response) => {
            this.deductibledata = API.copy(response)
            console.log("data deducible general", this.deductibledata.data)
              
                let PlanData = API.service('show', API.all('plans'))
                PlanData.one(this.deductibledata.data.plan_id).get()
                .then((response) => {
                this.planeditdata = API.copy(response)
                console.log("data del plan", this.planeditdata.data)
                })

            })




        
        })
        

        //
    }
     save (isValid) {
        if (isValid) {
            this.processing = true
            let vm = this
          let $state = this.$state
          this.deductibleeditdata.put()
            .then(() => {
                vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado los datos del deducible.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const DeductibleDetailsEditComponent = {
    templateUrl: './views/app/components/deductible-details-edit/deductible-details-edit.component.html',
    controller: DeductibleDetailsEditController,
    controllerAs: 'vm',
    bindings: {}
}
