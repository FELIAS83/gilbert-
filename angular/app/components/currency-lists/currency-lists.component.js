class CurrencyListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';
        this.API = API
        this.$state = $state

        let Currencies = this.API.service('currencies')
        
        Currencies.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn('iso').withTitle('Iso'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
   })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        

        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.currency-edit({currencyId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

    }

    delete (currencyId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la Moneda!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('currencies').one('currency', currencyId).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const CurrencyListsComponent = {
    templateUrl: './views/app/components/currency-lists/currency-lists.component.html',
    controller: CurrencyListsController,
    controllerAs: 'vm',
    bindings: {}
}
