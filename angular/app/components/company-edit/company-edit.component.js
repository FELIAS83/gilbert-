class CompanyEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false
      

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let companyId = $stateParams.companyId
        //console.log(companyId)
        let CompanyData = API.service('show', API.all('companies'))
        CompanyData.one(companyId).get()
          .then((response) => {
            this.companyeditdata = API.copy(response)
            this.companyeditdata.data.commission_percentage = parseFloat(this.companyeditdata.data.commission_percentage, 2)
          //console.log("data=>", this.companyeditdata);
        })

        
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.companyeditdata);
          let $state = this.$state
          this.companyeditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado la compañía.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const CompanyEditComponent = {
    templateUrl: './views/app/components/company-edit/company-edit.component.html',
    controller: CompanyEditController,
    controllerAs: 'vm',
    bindings: {}
}
