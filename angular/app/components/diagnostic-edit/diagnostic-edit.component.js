class DiagnosticEditController{
    constructor($stateParams,API,$state, $uibModal){
        'ngInject';

        this.title = 'Reclamo'
        this.formSubmitted = false
        this.claimId = $stateParams.claimId
        let diagnosticId = $stateParams.diagnosticId
        this.diagnosticId = diagnosticId
        this.lines = []
        this.API = API
        this.$state = $state
        this.alerts = []
        this.processing = false
        this.directory = 'claims/'
        this.$uibModal = $uibModal

        let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            //console.log("Tipos", docResponse)
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })
        this.systemDocTypes = systemDocTypes

        /*let Documents = API.service('documents', API.all('documents_diagnostic'))
        Documents.one($stateParams.diagnosticId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
            console.log(this.previouslines)
        })*/

        let Document = API.service('diagnosticid', API.all('diagnostics'))
        Document.one(diagnosticId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.diagnostic = dataSet.data
            this.description = this.diagnostic.description
            this.policyId = this.diagnostic.claims.policy_id
        })

        let data = {}
        data.claimId = this.claimId
        data.diagnosticId = diagnosticId
        let Documents = API.service('showdocinfo')
        Documents.getList(data)
        .then((response) => {
            let dataSet = response.plain()
            this.previouslines = dataSet
        })
    }

    $onInit(){
    }
    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});
    }

    save(isValid){
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid){
            this.processing = true
            let $state = this.$state
            let uploadFile = this.uploadFile
            let filenames = []
            angular.forEach(this.myFile, function (value) {
                filenames.push(value.name)
            })
            this.filenames = filenames
            var vm = this
            console.log("policyId... ", vm.policyId)
            console.log("claimId... ", vm.claimId)

            let documents = this.API.service('updatediagnostic', this.API.all('diagnostics'))
            documents.post({
                'policyId': this.policyId,
                'claimId': this.claimId,
                'diagnosticId': this.diagnosticId,
                'description': this.description,
                'files': this.previouslines,
                'files2': JSON.parse(localStorage.getItem('docs'))
          }).then(function (response) {
                vm.processing = false
                //let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el diagnostico.' }
                //vm.alerts = [alert]
                swal({
                  title: 'Diagnóstico editado correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
              }, function () {
                  $state.go('app.ticket-claim-lists', {"claimId": response.data.claimId, "policyId": response.data.policyId})
              })
          }, function (response) {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                 }, function () {
                      $state.go($state.current)
                })
          })
        }
        else{
            this.formSubmitted = true
        }
    }
    openModal (size, claimId, name, last_name) {
      let $uibModal = this.$uibModal
      let modalInstance = $uibModal.open({
          templateUrl: 'myModalAgent',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size,
          resolve: {
              claimId: () => {
                  return claimId
              },
              name: () => {
                  return name
              },
              last_name: () => {
                  return last_name

              },
              diagnosticId: () => {
                  return this.diagnosticId

              }
          }
      })
    }

    modalcontroller ($scope, $uibModalInstance, API,  $http, $stateParams,claimId, name, last_name, diagnosticId) {
        'ngInject'

        let mvm = this
        this.claimId = claimId
        this.name = name
        this.last_name = last_name
        this.processing = false
        localStorage.removeItem('docs')


        let reports_orders = API.all('reportsorders/'+claimId+'/'+diagnosticId)
        //reports_orders.one(claimId).get()
        reports_orders.doGET('',{})
        .then((response) => {
          let dataSet = response.plain()
          this.reports_orders = dataSet.data
          angular.forEach(this.reports_orders, function (value) {
                //value.assigned = false
            })

        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function(){
            console.log('Funcion guardar del modal')
            localStorage.setItem('docs',JSON.stringify(this.reports_orders))
            $uibModalInstance.dismiss('cancel')
        }

    }
}

export const DiagnosticEditComponent = {
    templateUrl: './views/app/components/diagnostic-edit/diagnostic-edit.component.html',
    controller: DiagnosticEditController,
    controllerAs: 'vm',
    bindings: {}
}
