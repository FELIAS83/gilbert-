class PrintGuideController{
    constructor($scope, API, $state, $stateParams,fileUpload,$http){
        'ngInject';

        var vm = this
        this.API = API
        this.$state = $state
        this.$stateParams = $stateParams
        this.alerts = []
        this.agent = ""
        this.agentAddress = ""
        this.agentPhone =""
        this.policyNumber = ""
        this.date = new Date()
        this.date = this.date.getDate()+"/"+(this.date.getMonth()+1)+"/"+this.date.getFullYear()

        let policyId = $stateParams.policyId
        this.policyId = policyId
        console.log(policyId)

        let Messengers = this.API.service('messengers')
        Messengers.getList()
          .then((response) => {

             let messengersData =  response.plain()
             let systemMessenger = []
             console.log("Mensajeros", messengersData)
             angular.forEach(messengersData, function(value) {

               systemMessenger.push({id: value.id, name: value.name})
             })
             this.systemMessenger = systemMessenger

             console.log("systemMessenger", this.systemMessenger)


          })




        let Policy = this.API.service('getPolicyInfo');
        let data = {};
        data.policy_id =   $stateParams.policyId;
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            console.log("data", this.policyinfo)
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.policyNumber = this.policyinfo[0].number
            this.agent = this.policyinfo[0].agent
            let agentId = this.policyinfo[0].agent_id

             let AgentData = API.service('show', API.all('agents'))
            AgentData.one(agentId).get()
                .then((response) => {
                    this.agenteditdata = API.copy(response)
                    console.log(this.agenteditdata)
                    this.agentAddress = this.agenteditdata.data.address
                    this.agentPhone = this.agenteditdata.data.mobile
                })

        })



        //
    }

   

    print(formPrint){

        var combo= document.getElementById("messenger")
        combo.style.display='none'; 
        var div=document.getElementById("messengerHidden")
        div.style.display='table-cell'; 

        var p=document.getElementById("nom")
        p.style.display='table-cell'; 

        let popupWinindow 

        var original=document.getElementById("formPrint");
        
        var nuevo=original.cloneNode(true);

        nuevo.innerHTML =  original.innerHTML;

        
        //let innerContents = document.getElementById(formPrint).innerHTML;
        
        //var sp1 = document.createElement("span"); 

        //sp1.setAttribute("id", "newSpan");

        //var sp1 = document.createElement("span");


        //sp1.setAttribute("id", "newSpan");

       // var sp1_content = document.createTextNode(document.getElementById("messHidden").value);

       // sp1.appendChild(sp1_content);

       // var sp2 = document.getElementById("messenger");

       // var parentDiv = sp2.parentNode;

        //parentDiv.replaceChild(sp1, sp2);
       

        
        
        popupWinindow = window.open('', '_blank','width=800,height=800,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no'); 
        popupWinindow.document.open(); 
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" /></head><body onload="window.print();">' +  nuevo.innerHTML + '</html>');         

        popupWinindow.document.close(); 
        window.location.reload();

        
    }

    $onInit(){
    }
}

export const PrintGuideComponent = {
    templateUrl: './views/app/components/print-guide/print-guide.component.html',
    controller: PrintGuideController,
    controllerAs: 'vm',
    bindings: {}
}
