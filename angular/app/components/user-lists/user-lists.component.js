class UserListsController {
  constructor ($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API) {
    'ngInject'
    this.API = API
    this.$state = $state
    let Users = this.API.service('users')
    Users.getList()
      .then((response) => {
        let dataSet = response.plain()
        this.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('data', dataSet)
          .withOption('createdRow', createdRow)
          .withOption('responsive', true)
          .withBootstrap()
          .withOption('autoWidth', false)
          .withOption('stateSave', true)
         .withOption('stateSaveCallback', function(settings,data) {
            sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
           })
          .withOption('stateLoadCallback', function(settings) {
            return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
           })
        this.dtColumns = [
          DTColumnBuilder.newColumn('id').withTitle('ID'),
          DTColumnBuilder.newColumn('name').withTitle('Name'),
          DTColumnBuilder.newColumn('email').withTitle('Email'),
          DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
            .renderWith(actionsHtml)
        ]
        this.displayTable = true
      })
    let createdRow = (row) => {
      $compile(angular.element(row).contents())($scope)
    }
    let actionsHtml = (data) => {
      return `<a class="btn btn-xs btn-warning" ui-sref="app.user-edit({userId: ${data.id}})">
                <i class="fa fa-edit"></i>
              </a>
              &nbsp
              <button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                <i class="fa fa-trash-o"></i>
              </button>`
    }
  }
  delete (userId) {
    let API = this.API
    let $state = this.$state
    swal({
      title: 'Está seguro?',
      text: 'Se eliminará el usuario!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si, eliminalo!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: false
    }, function () {
      API.one('users').one('user', userId).remove()
        .then(() => {
          swal({
            title: 'Deleted!',
            text: 'El usuario ha sido eliminado.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })
    })
  }

  $onInit () {}
}

export const UserListsComponent = {
  templateUrl: './views/app/components/user-lists/user-lists.component.html',
  controller: UserListsController,
  controllerAs: 'vm',
  bindings: {}
}