class ConfirmRenewalPaymentController{
    constructor($state, API, $stateParams, $http,  $uibModal, $log){
        'ngInject';
        this.$uibModal = $uibModal
        this.$log = $log
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.$http = $http
        this.uploadFile = uploadFile
        this.alerts = []
        this.methods = []
        this.account_types = []
        this.card_types = []
        this.card_marks = []
        this.payment_types = []
        this.lines = []
        this.methods.push({id: 0, name: 'Cheque'},
                         {id: 1, name: 'Transferencia'},
                         {id: 2, name: 'Depósito Bancario'},
                         {id: 3, name: 'Tarjeta de Crédito'})
        this.account_types.push({id: 1, name: 'Ahorros'},
                         {id: 2, name: 'Corriente'})
        this.card_types.push({id: 1, name: 'Corporativa'},
                         {id: 2, name: 'Personal'})
        this.card_marks.push(
            {id: 1, name: 'American Express'},
            {id: 2, name: 'Visa'},
            {id: 3, name: 'Mastercard'},
            {id: 4, name: 'Diners Club'},
            {id: 5, name: 'Otros'})
        this.payment_types.push(
            {id: 1, name: 'Diferido'},
            {id: 2, name: 'Con intereses'},
            {id: 3, name: 'Corriente'},
            {id: 4, name: 'Sin interés'},
            {id: 5, name: '3 meses'},
            {id: 6, name: '6 meses'},
            {id: 7, name: '9 meses'},
            {id: 8, name: '12 meses'})

        let policyId = $stateParams.policyId
        let feeId = $stateParams.feeId
        this.policyId = policyId
        this.feeId = feeId
        console.log("id police aqui", vm.policyId)
        let Policy = this.API.service('getPolicyInfoPay');

        let data = {};
        data.policy_id=   $stateParams.policyId;
        data.fee_id = $stateParams.feeId;



        console.log("Id de la cuota",  data.fee_id)

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            console.log("data", this.policyinfo)
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible
            this.policyinfo[0].first_value = '$'+this.policyinfo[0].first_value
            this.number = this.policyinfo[0].number

           this.form = this.policyinfo[0]
            this.form.payment_date = new Date( this.form.payment_date+' ')
            if(this.form.card_bank == 0){
                    this.form.card_bank = ''
            }
            console.log('vm.form',  this.form)
            this.form.first_value =  this.form.first_value
           
            let attached = {}
            attached.payment_id = this.form.payment_id
            let attacheds = this.API.service('attacheds')
            attacheds.getList(attached).then((response) => {
                this.previous_lines = response.plain()
                this.description = this.previous_lines[0] ? this.previous_lines[0].description : ''
                this.filename = this.previous_lines[0] ? this.previous_lines[0].filename : ''
            })
        })

        function uploadFile(){    
            let confirm = API.service('confirmrenewalpaymentfee', API.all('renewals'))
            var files = vm.files
            let name =  vm.files[0].name.split('.')
            let filename = name[0]+'-'+vm.feeId+'.'+name[1]
            console.log("filename",filename)
              angular.forEach(files, function(files){
              var form_data = new FormData();
              form_data.append('file', files);
              form_data.append('policy_id', vm.policyId);
              form_data.append('folder', 'authorization_form/');
              form_data.append('filename', filename);
              $http.post('uploadfilespaymentrenewal', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).then(function (response){
                   confirm.post({
                'policyId': vm.policyId,
                'payment_id': vm.form.payment_id,
                'filename': filename,
                'feeId' :   vm.feeId,
                'form': vm.form,
                'mode': vm.form.mode,
                'payment_method_name': vm.methods[vm.form.method].name,
                'payment_method': vm.form.method,
                'card_mark': vm.form.method == 3 ? vm.card_marks[vm.form.card_mark-1].name : '',
                'card_type': vm.form.method == 3 ? vm.card_types[vm.form.card_type-1].name : ''

                })
                .then( ( response ) => {
                    swal({
                        title: 'Confirmación de Pago Exitosa!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                        $state.go('app.renewal-lists')
                    })
                }, function (response) {
                    swal({
                        title: 'Error al Confirmar Pago!',
                        type: 'error',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                      $state.go('app.renewal-lists')
                    })
                  }
                )
                })
              })

         }
    }

    modalPayment (size,  policy_id, number) {
      console.log("Entro al modal", policy_id)
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      //console.log("a", name);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalPayment',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
            policy_id: () => {
                return policy_id
          },
          number: () => {
                return number
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

    save(isValid){

        if(isValid){
            this.isDisabled = true;
            if (isValid){
              this.isDisabled = true;  
              let uploadFile = this.uploadFile
              uploadFile()
          }
        else{
            this.formSubmitted = true
        }
           /* let confirm = this.API.service('confirmrenewalpaymentfee', this.API.all('renewals'))
            let $state = this.$state
            let uploadFile = this.uploadFile
            let $http = this.$http
            console.log("vm.files",this.files)
            var policyId = this.policyId
            var files = this.files
            let name =  this.files[0].name.split('.')
            let filename = name[0]+this.feeId+'.'+name[1]
                angular.forEach(files, function(files){
                var form_data = new FormData();
                form_data.append('file', files);
                form_data.append('policy_id', policyId);
                form_data.append('folder', 'authorization_form/');
                form_data.append('filename', filename);
                $http.post('uploadfilespaymentrenewal', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                })
              })
            confirm.post({
                'policyId': this.policyId,
                'payment_id': this.form.payment_id,
                'filename': filename,
                'feeId' :   this.feeId,
                'form': this.form,
                'mode': this.form.mode,
                'payment_method_name': this.methods[this.form.method].name,
                'payment_method': this.form.method,
                'card_mark': this.form.method == 3 ? this.card_marks[this.form.card_mark-1].name : '',
                'card_type': this.form.method == 3 ? this.card_types[this.form.card_type-1].name : ''

            }).then((response ) => {
                //console.log('success')
                //uploadFile()
                  swal({
                            title: 'Confirmación de Pago Exitosa!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.renewal-lists')
                        })
                
            }, function (response) {
                swal({
                    title: 'Error al Confirmar Pago!',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })

            })
        }
        else{
            this.formSubmitted = true
        }*/
    }
    }

     modalcontroller ($scope, $uibModalInstance, API,  $http, $state, $stateParams, policy_id, number) {
    'ngInject'

       this.feeResponse = [] 

      console.log("controller")
      this.API = API
      //this.items = items
      //this.customer = customer
      this.policy_id = policy_id
      this.number =  number
      this.$state = $state
     // this.sendmail = sendmail
      this.diagnosticId = {}
      this.isDisabled = true
      var mvm = this
      this.processing = false
      this.isDisabled = true

      console.log("policy_id", this.policy_id)
      console.log("number", this.number)

      if ($stateParams.alerts) {
        this.alerts.push($stateParams.alerts)
      }
        
     
        let LastFee =   API.service('lastpaymentfee', API.all('renewals'))
        LastFee.one(policy_id).get()
          .then((response) => {

          
            
            let feeData = response.plain(response)
            console.log("Fee", feeData)
            let feeResponse = feeData.data.last_fee[0]
            console.log("Fee", feeResponse)
            this.feeResponse = feeResponse
            if(this.feeResponse.invoice_filename == "" || this.feeResponse.invoice_filename.length == 0){
                this.feeResponse.icon = "fa fa-eye-slash"
                this.feeResponse.invoice_file = "#"
                this.disabledLink = false
                this.disabledLink2 = true
            }else{
               this.feeResponse.icon = "fa fa-file"
               this.feeResponse.invoice_file = "authorization_form/mvm.policy_id/"+this.feeResponse.invoice_filename
               this.disabledLink = true
               this.disabledLink2 = false
            }

      })
      


     this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }
      
     }

    $onInit(){
    }
}

export const ConfirmRenewalPaymentComponent = {
    templateUrl: './views/app/components/confirm-renewal-payment/confirm-renewal-payment.component.html',
    controller: ConfirmRenewalPaymentController,
    controllerAs: 'vm',
    bindings: {}
}
