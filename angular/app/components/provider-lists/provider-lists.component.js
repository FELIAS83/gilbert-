class ProviderListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';
        this.API = API
        this.$state = $state

        let Providers = this.API.service('providers')
        
        Providers.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn('description').withTitle('Descripción'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
   })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        

        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.provider-edit({providerId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

    }

    delete (providerId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Proveedor de Servicio!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('providers').one('provider', providerId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }
    

    $onInit(){
    }
}

export const ProviderListsComponent = {
    templateUrl: './views/app/components/provider-lists/provider-lists.component.html',
    controller: ProviderListsController,
    controllerAs: 'vm',
    bindings: {}
}
