class RenewalCoverageStartController{
    constructor(API,$stateParams, $state){
        'ngInject';

        //
 var vm = this
        this.$state = $state
        this.API = API
        this.current_step = '6'

        let policyId = $stateParams.policyId
        this.policyId = policyId
        let feeId = $stateParams.feeId
        this.feeId = feeId

        let Wizar = API.service('wizar')
        Wizar.getList().then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            angular.forEach(wizarResponse, function (value) {
                systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})
            })
            systemWizar.splice(3,1) 
           //systemWizar.pop()
           this.systemWizar = systemWizar
        })

        let datescoverages = API.service('dates',API.all('policies'));
        datescoverages.getList().then((response) => {
            let dataSet = response.plain()
            this.dates = dataSet
        })


        let Datefees =  API.service('effectivedateactual', API.all('renewals'))
              Datefees.one(policyId).get()
              .then((response) => {
                this.datesEffective = response.plain(response)   
                console.log(this.datesEffective)                
                var datefees = new Date(this.datesEffective.data[0].start_date_coverage)
                this.start_date_coverage = this.datesEffective.data[0].start_date_coverage
                var start_date = this.start_date_coverage
                let values=start_date.split("-")
                let dia = values[2]
                let mes = values[1]
                let ano = values[0]
                this.start_date = dia+"-"+mes+"-"+ano

                console.log("Fecha de effective", this.start_date)

            })

        let data = {};
        data.policy_id=   policyId;
        let Policy = API.service('getPolicyInfo');
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            console.log("fecha policyinfo",dataSet[0].start_date_coverage)
            this.dateselected =  this.start_date != '01-01-1970' ?  this.start_date : this.dates[0];
            this.showhidepregnant = dataSet[0].have_prev_safe == '1' ? true : false
            this.showhidecontinue = dataSet[0].continuity_coverage == '1' ? true : false
            this.company_name = dataSet[0].company_name
            this.plan_name = dataSet[0].plan_name
            this.have_renewal = dataSet[0].have_renewal
            this.is_international = dataSet[0].is_international == '1' ? true : false
        })
    }

    save (isValid) {
        if (isValid) {
            this.isDisabled = true;      
            let $state = this.$state
            let company_name = this.showhidepregnant == true ? this.company_name : 'x'
            let plan_name = this.showhidepregnant == true ? this.plan_name : 'x'
            let datecoverage = this.API.service('coverage', this.API.all('renewals'))

            datecoverage.post({
                'policy_id' : this.policyId,
                'start_date_coverage': this.dateselected,
                'continuity_coverage' : this.showhidecontinue,
                'is_international' : this.is_international,
                'have_prev_safe' : this.showhidepregnant,
                'company_name' : company_name,
                'plan_name' : plan_name,
                'have_renewal':  this.have_renewal
            }).then( (response) => {
                response.feeId = this.feeId
                swal({
                    title: 'Datos de Cobertura Guardados Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.renewal-documents', {"policyId": response.data, "feeId": response.feeId})
                     
                })
            }, function (response) {
                swal({
                    title: 'Error ' + response.data,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current)
                })
          })
        
        }
    

    }
    $onInit(){
    }
}

export const RenewalCoverageStartComponent = {
    templateUrl: './views/app/components/renewal-coverage-start/renewal-coverage-start.component.html',
    controller: RenewalCoverageStartController,
    controllerAs: 'vm',
    bindings: {}
}
