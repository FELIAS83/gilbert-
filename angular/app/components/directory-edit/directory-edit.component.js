class DirectoryEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.API = API
        this.method = 0


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }


        let directoryId = $stateParams.directoryId

        let directoryData = API.service('show', API.all('configurations'))
        directoryData.one(directoryId).get()
          .then((response) => {
            this.directoryeditdata = API.copy(response)
            console.log(this.directoryeditdata.data)
            if (this.directoryeditdata.data.length!=0){
                this.method = 0
            }
            else{
                this.method = 1
            }

        })
    }
    save (isValid) {

        if (isValid) {
            console.log(this.method)
            if (this.method == 1){
                let Config = this.API.service('configurations', this.API.all('configurations'))
                let $state = this.$state

                Config.post({
                'id' : 1,
                'policy_file_directory': this.directoryeditdata.data.policy_file_directory
                }).then(() => {
                    let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el directorio.'}
                    this.$state.go($state.current, { alerts: alert})
                    this.alerts = [alert]
                    this.method = 0
                }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
                })

            }
            else{
                let $state = this.$state
                console.log(this.directoryeditdata)
                this.directoryeditdata.data.id = 1
                this.directoryeditdata.put()
                .then(() => {
                    let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el directorio.' }
                    $state.go($state.current, { alerts: alert})
                    this.alerts = [alert]
                }, (response) => {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                    this.alerts = [alert]
                    })
            }
        } else {
            this.formSubmitted = true
        }
    }
    $onInit(){
    }
}

export const DirectoryEditComponent = {
    templateUrl: './views/app/components/directory-edit/directory-edit.component.html',
    controller: DirectoryEditController,
    controllerAs: 'vm',
    bindings: {}
}


