class BranchEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false
        var vm = this
  

         let branchId = $stateParams.branchId
         let companyId = $stateParams.companyId
         this.companyId = companyId
         console.log("companyId", companyId)
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let Company = API.service('show', API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()
            this.commission_percentage_company = parseFloat(this.companydata.data.commission_percentage,2)
        })  

        let BranchData = API.service('show', API.all('branchs'))
        BranchData.one(branchId).get()
          .then((response) => {
            this.brancheditdata = API.copy(response)
            this.brancheditdata.data.commission_percentage = parseFloat(this.brancheditdata.data.commission_percentage, 2)
            this.commission_percentage  = this.brancheditdata.data.commission_percentage
            if(this.commission_percentage == 0) {
              this.commission_percentage  = this.commission_percentage_company
            }
          
            //this.companyeditdata.data.commission_percentage = parseFloat(this.companyeditdata.data.commission_percentage, 2)
          console.log("data=>", this.brancheditdata);
        })
       


        //
    }
      save (isValid) {
        if (isValid) {
          this.processing = true
          let vm = this
          let $state = this.$state
          console.log("comi", this.commission_percentage)
          if(!this.commission_percentage == null || !this.commission_percentage == undefined || !this.commission_percentage == 0){
             
                this.brancheditdata.data.commission_percentage = this.commission_percentage
             
          }
          this.brancheditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el ramo.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }
    $onInit(){
    }
}

export const BranchEditComponent = {
    templateUrl: './views/app/components/branch_edit/branch_edit.component.html',
    controller: BranchEditController,
    controllerAs: 'vm',
    bindings: {}
}
