class PlanEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.alerts = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let planId = $stateParams.planId
        this.planId =  planId
        let branchId = $stateParams.branchId
        this.branchId = branchId
        let companyId = $stateParams.companyId
        this.companyId = companyId
        console.log(planId)
        
        let Company = API.service('show', API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()

        })

        let Branch = API.service('show', API.all('branchs'))
        Branch.one(branchId).get()
          .then((response) => {
            this.branchdata = response.plain()
            this.commission_percentage_branch = parseFloat(this.branchdata.data.commission_percentage,2)
            console.log(this.branchdata)
        })  

        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(planId).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
            this.planeditdata.data.commission_percentage = parseFloat(this.planeditdata.data.commission_percentage, 2)
            this.commission_percentage  = this.planeditdata.data.commission_percentage
            this.planeditdata.data.max_dependent_age = parseInt(this.planeditdata.data.max_dependent_age)
            if(this.commission_percentage == 0) {
              this.commission_percentage  = this.commission_percentage_branch
            }
            console.log("datos=>", this.planeditdata);
        })

        //
    }
    save (isValid) {
        if (isValid) {
          //console.log(this.messengereditdata);
          let $state = this.$state
          console.log("comi", this.commission_percentage)
          if(!this.commission_percentage == null || !this.commission_percentage == undefined || !this.commission_percentage == 0){       
                this.planeditdata.data.commission_percentage = this.commission_percentage
          }
          this.planeditdata.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el plan.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const PlanEditComponent = {
    templateUrl: './views/app/components/plan-edit/plan-edit.component.html',
    controller: PlanEditController,
    controllerAs: 'vm',
    bindings: {}
}
