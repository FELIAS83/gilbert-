class ConfirmRenewalDataController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
    
        var vm = this
        var relationship = ""
        var orderRole = 0
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.client = ""
        this.role = ""
        this.planDeduc = ""
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        this.lines = []
        this.lines_1 = []
        this.birthdayaffiliate = []
        this.deletedamendments = []
        this.deletedannexes = []
        this.details = []
        this.fees = []
        this.completed = $stateParams.completed
        this.title = 'Confirmar Datos de Póliza'
        this.uisrefback = 'app.renewal-lists'
        if (this.completed == 1){
            this.title = 'Datos de Póliza'
            this.uisrefback = 'app.historical-emissions-lists'
        }
        this.alert =  ""
        var dates = []
        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.amounttitular = ""
        this.amounth = ""
        this.amountchild = ""
        this.coverage_amount = ""
        this.savedfees  = ""
        this.taxssc_percent  = ""
        this.datefees = ""


    
        let Policy = this.API.service('getPolicyInfoRen');
        let data = {};
        data.policy_id=   $stateParams.policyId;
        this.feeId = $stateParams.feeId;




        Policy.getList(data).then((response) => {

            let dataSet = response.plain()
            
            this.policyinfo = dataSet
            console.log("this.policyinfo",this.policyinfo[0])
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible
            this.role =  "Titular"
            this.actual_number = vm.policyinfo[0].number
            this.have_renewal = this.policyinfo[0].have_renewal
            if(this.have_renewal == 1){
               this.alert =  "Número de Póliza debe ser Actualizado !"
            }

            console.log("id de la poliza",  data.policy_id)
              let Datefees =  API.service('effectivedateactual', API.all('renewals'))
              Datefees.one(data.policy_id).get()
              .then((response) => {
                this.dates = response.plain(response)   
                console.log(dates)
                this.datefees = new Date(this.dates.data[0].start_date_coverage)
                this.start_date_coverage = this.dates.data[0].start_date_coverage
                var start_date = this.start_date_coverage
                let values=start_date.split("-")
                let dia = values[2]
                let mes = values[1]
                let ano = values[0]
                start_date = dia+"-"+mes+"-"+ano

                console.log("DateFee", start_date)


            let applicantId = this.policyinfo[0].applicant_id
            let Dependents = API.service('dependents', API.all('dependents'))
            Dependents.one(applicantId).get()
            .then((response) => {
                let dependentsResponse = response.plain()
                console.log("dependent response", dependentsResponse)
                let dependentq = 0
                vm.dependentq = dependentq
                let spouse = 0
                vm.spouse = spouse
                let ageh = 0
                vm.ageh = ageh
                let inclusion_date=""
                console.log(this.policyinfo[0].effective_date)
                angular.forEach(dependentsResponse.data, function (value) {
                    if(value.relationship == "H"){
                        relationship = "Cónyugue"
                        orderRole = 2
                        vm.ageh = vm.calculateAge(value.birthday)
                        vm.spouse = 1
                    }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        vm.dependentq++
                    }
                    inclusion_date = value.inclusion_date
                                        
                    let age = vm.calculateAge(value.birthday)
                    var values = value.birthday.split("-")
                    var dia = values[2]
                    var mes = values[1]
                    var ano = values[0]
                    value.birthday = dia+"-"+mes+"-"+ano

                    systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, relationship: relationship,inclusion_Date: inclusion_date})
                    allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, role: value.relationship, order: orderRole, relationship: relationship, age: age, effective_Date: start_date, inclusion_Date: inclusion_date })
                    
                })
                let values = this.policyinfo[0].birthday.split("-")
                let dia = values[2]
                let mes = values[1]
                let ano = values[0]
                let titularBirth = dia+"-"+mes+"-"+ano
                let agetitular = this.calculateAge(this.policyinfo[0].birthday)
                allAffiliates.push({id: parseInt(this.policyinfo[0].id), name: this.client, role: "T", birthday: titularBirth ,order: 1, relationship: 'Titular' , age: agetitular, effective_Date: start_date,inclusion_Date: this.policyinfo[0].inclusion_date})
                
                allAffiliates.sort(function (a, b) {
                    if (a.order > b.order) {
                        return 1;
                    }
                    if (a.order < b.order) {
                        return -1;
                    }
                    return 0;
                });

                systemDependents.sort(function (a, b){
                  return (b.relationship - a.relationship)
                })
                this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                console.log("allAffiliates",this.allAffiliates)

                if (this.policyinfo[0].mode == 0){
                    this.payment_type = 'Anual'
                    this.amount_fees = 1
                }
                else if (this.policyinfo[0].mode == 1){
                    this.payment_type = 'Semestral'
                    this.amount_fees = 2
                }
                else if (this.policyinfo[0].mode == 2){
                    this.payment_type = 'Trimestral'
                    this.amount_fees = 4
                }
                else if (this.policyinfo[0].mode == 3){
                    this.payment_type = 'Mensual'
                    this.amount_fees = 12
                }
                if (isNaN(this.ageh)){
                    this.ageh = 0
                }


                console.log("mode",this.policyinfo[0].mode)
                if(this.policyinfo[0].mode == 0){
                    console.log("Entro Anual")
                    var amounttitular = API.all('amountsanual/'+this.policyinfo[0].deductible_id+'/'+agetitular+'/'+this.ageh+'/'+this.dependentq+'/'+this.policyinfo[0].coverage_id+'/'+this.policyId)
                }else{
                var amounttitular = API.all('amounts/'+this.policyinfo[0].deductible_id+'/'+agetitular+'/'+this.ageh+'/'+this.dependentq+'/'+this.policyinfo[0].coverage_id+'/'+this.policyId)
                }
                amounttitular.doGET('',{}).then((response) => {
                    dataSet = response.plain()
                    console.log("data",dataSet.data)
                    this.feesData = dataSet.data.fees

                    if(parseFloat(this.feesData[0].amount_titular) == 0){
                      
                            this.amounttitular =  dataSet.data.amounttitular 
                            this.amounth =  dataSet.data.amounth 
                            this.amountchild = dataSet.data.amountchild 
                             if (this.policyinfo[0].mode == 0){
                                 this.nfee = 1
                             }
                             else if(this.policyinfo[0].mode == 1){
                                 this.nfee = 2
                             }
                             else if (this.policyinfo[0].mode == 2){
                                 this.nfee = 4
                             }
                             else {
                                 this.nfee = 12
                             }
                     
                    }else{  

                      if (this.policyinfo[0].mode == 0){
                            this.amounttitular = parseFloat(this.feesData[0].amount_titular) * 1
                            this.amounth = parseFloat(this.feesData[0].amount_spouse) * 1
                            this.amountchild = parseFloat(this.feesData[0].amount_childs) * 1
                            this.nfee = 1
                            this.amount_fees = this.feesData.length
                            

                        }
                    else if (this.policyinfo[0].mode == 1){
                            this.amounttitular = parseFloat(this.feesData[0].amount_titular) * 2
                            this.amounth = parseFloat(this.feesData[0].amount_spouse) * 2
                            this.amountchild = parseFloat(this.feesData[0].amount_childs) * 2
                            this.nfee = 2
                            this.amount_fees = this.feesData.length
                        }
                    else if (this.policyinfo[0].mode == 2){
                            this.amounttitular = parseFloat(this.feesData[0].amount_titular) * 4
                            this.amounth = parseFloat(this.feesData[0].amount_spouse) * 4
                            this.amountchild = parseFloat(this.feesData[0].amount_childs) * 4
                            this.nfee = 4
                            this.amount_fees = this.feesData.length
                        }
                    else if (this.policyinfo[0].mode == 3){
                            this.amounttitular = parseFloat(this.feesData[0].amount_titular) * 12
                            this.amounth = parseFloat(this.feesData[0].amount_spouse) * 12
                            this.amountchild = parseFloat(this.feesData[0].amount_childs) * 12
                            this.nfee = 12
                            this.amount_fees = this.feesData.length
                    }
                }

                    
                    this.coverage_name = dataSet.data.coverage_name
                    this.coverage_amount = dataSet.data.coverage_amount
                    this.taxssc_percent = dataSet.data.taxssc
                    this.adminfee = dataSet.data.adminfee
                    this.iva = dataSet.data.iva
                    this.savedfees = dataSet.data.fees
                    //let datefees = new Date( vm.policyinfo[0].effective_date)
                    
                    console.log(" this.feesData.length", this.feesData.length)
                    this.total = 0
                    for (let i = 0; i < this.amount_fees; i++ ){

                        if(parseFloat(this.feesData[i].amount_titular) == 0){
                            this.feesData[i].amount_titular = this.amounttitular/this.nfee
                        }

                        this.fees.push({fee_id : i+1, id : this.feesData[i].id, fee_number:this.feesData[i].fee_number})
                        this.fees[i].details = []
                        this.fees[i].details.push({id: 1, detail: 'P/H Premium', value: parseFloat(this.feesData[i].amount_titular)})
                        if (this.spouse != 0){
                            if(parseFloat(this.feesData[i].amount_spouse) == 0){
                                this.feesData[i].amount_spouse = this.amounth/this.nfee
                            }   
                            this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: parseFloat(this.feesData[i].amount_spouse)})
                        }else{
                            this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: 0})
                        }
                        if (this.amountchild != 0){
                            if(parseFloat(this.feesData[i].amount_childs) == 0){
                                this.feesData[i].amount_childs = this.amountchild/this.nfee
                            }   
                            let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: parseFloat(this.feesData[i].amount_childs)})
                        }else{
                            let detail = ''
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: 0})
                        }
                        if (this.coverage_amount != 0){
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: this.coverage_name, value: this.coverage_amount/this.amount_fees})
                        }else{
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: this.coverage_name, value: 0})
                        }

                        let sum = 0
                        this.totaldetails = 0
                        for (let j = 0; j < this.fees[i].details.length; j++){
                            sum = sum + this.fees[i].details[j].value
                            this.totaldetails = this.totaldetails + this.fees[i].details[j].value
                        }
                        if (this.policyinfo[0].mode == 1){
                            sum = sum * 0.06
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }
                        if ((this.policyinfo[0].mode == 2) || (this.policyinfo[0].mode == 3) ){
                            sum = sum * 0.1
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }
                         if (this.policyinfo[0].mode == 0) {
                            sum = 0;
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }

                        let totalfee = 0
                        for (let k = 0; k < this.fees[i].details.length; k++){
                            totalfee = totalfee + this.fees[i].details[k].value
                        }
                        //descuento
                        this.fees[i].discounts = []
                        let discount = 0
                        if (this.policyinfo[0].discount == 1 ){
                            discount = ((totalfee-(this.coverage_amount/this.amount_fees))*this.policyinfo[0].discount_percent)/100
                            this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: discount })
                        }else{
                            discount = 0
                            this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: 0 })
                        }
                        //impuesto
                        this.fees[i].taxes = []
                        let adminfeeiva = 0

                        if (i == 0){
                            if (this.savedfees != ''){
                                if (this.savedfees[i].adminfee == 1){
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: this.adminfee, check:true, checkvalue:true })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: this.adminfee*(this.iva/100) })
                                    adminfeeiva = this.adminfee + (this.adminfee*(this.iva/100))
                                }
                                else{
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true, checkvalue:false })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                }
                            }
                            else{
                                this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true, checkvalue:true })
                                this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                            }
                        }else{
                            this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:false, checkvalue:false })
                            this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                        }


                        //Tax SSCtaxssc_percent
                        if (this.savedfees != '') {
                            if (this.savedfees[i].tax_ssc == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                this.tax_ssc = true
                                console.log(this.taxssc)
                            }else{
                              console.log("sum", sum, "discount", discount, "tax percent", taxssc_percent)
                              this.taxssc = 0 //variable configuracion
                              this.tax_ssc = false
                              console.log(this.taxssc)  
                            }                              
                        } else if (this.policyinfo[0].is_international == 0){
                            console.log("es Nacional")
                            this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                            this.tax_ssc = true
                        } else {
                            /*if (this.policyinfo[0].is_international == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                console.log(taxssc)
                                this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                                this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                            } else {*/
                                console.log("else fees para ssc ", this.fees[i])
                                this.tax_ssc = false
                                this.taxssc = 0
                            //}
                            
                        }
                        //Tax SSC
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: this.taxssc })
                        this.fees[i].total =  (totalfee+this.taxssc+adminfeeiva)-discount
                        /*let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                        this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                        this.total = this.total + this.fees[i].total
                        this.fees[i].fee_payment_date = this.feesData[i].fee_payment_date*/
                        this.total = this.total + this.fees[i].total

                        
                        /*

                        if (this.amount_fees == 1){
                            this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                        }
                        else if (this.amount_fees == 2){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = this.datefees.setMonth(this.datefees.getMonth() + 6)
                            }


                        }
                        else if (this.amount_fees == 4){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date =   this.datefees.setMonth(this.datefees.getMonth() + 3)
                            }
                        }
                        else if (this.amount_fees == 12){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = this.datefees.setMonth(this.datefees.getMonth() + 1)
                            }
                        }
                        */
                    }
                    }) // tabla policies_effective_dates
                })
            })
        })

        let amendmentData = API.service('amendments', API.all('amendments'))
        amendmentData.one(policyId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.oldlines_1 = dataSet.data
            console.log('amendments',this.oldlines_1)
        })
        let annexData = API.service('annexes', API.all('annexes'))
        annexData.one(policyId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.oldlines = dataSet.data
            for (let i = 0; i < this.oldlines.length; i++ ){
                this.oldlines[i].effective_date = new Date( this.oldlines[i].effective_date+' ')
            }
        })
    }
    addFile() {
        this.lines.push({id : this.lines.length+1})
    }
    addFile_1() {
        this.lines_1.push({id : this.lines_1.length+1, type: 1})
    }
    deleteLine(index){
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
    }
    deleteLine_1(index){
        var pos = this.lines_1.indexOf(index);
        this.lines_1.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines_1, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines_1 = newlines
    }
    changeAffiliate(affiliate_id,index){
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.birthdayaffiliate[index] = this.allAffiliates[i].birthday
                this.lines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.lines_1',this.lines_1)
            }
        }

    }
    oldchangeAffiliate(affiliate_id,index){
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.oldlines_1[index].birthdate = this.allAffiliates[i].birthday
                this.oldlines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.oldlines_1',this.oldlines_1)
            }
        }
    }
    calculateAge(date){
        if (date != undefined){
            var date = date
            var values=date.split("-")
            var dia = values[2]
            var mes = values[1]
            var ano = values[0]
            var fecha_hoy = new Date()
            var ahora_ano = fecha_hoy.getYear()
            var ahora_mes = fecha_hoy.getMonth()+1
            var ahora_dia = fecha_hoy.getDate()
            var edad = (ahora_ano + 1900) - ano
            if ( ahora_mes < mes ){
                edad--
            }
            if ((mes == ahora_mes) && (ahora_dia < dia)){
                edad--
            }
            if (edad > 1900){
                edad -= 1900
            }
            var meses=0
            if(ahora_mes>mes)
                meses=ahora_mes-mes
            if(ahora_mes<mes)
                meses=12-(mes-ahora_mes)
            if(ahora_mes==mes && dia>ahora_dia)
                meses=11

            var dias=0
            if(ahora_dia>dia)
                dias=ahora_dia-dia
            /*if(ahora_dia<dia)
            {
                console.log('ahora_ano',ahora_ano,'ahora_mes',ahora_mes)
                ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
                console.log('ultimoDiaMes',ultimoDiaMes)
                dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
            }*/
            if (edad < 1){
                if (meses ==1){
                    return meses+' mes'
                }
                else{
                    return meses+' meses'
                }
            }
            else{
                return edad
            }
            }
    }
    olddeleteLine_1(index){
        this.deletedamendments.push({id : index.id})
        let pos = this.oldlines_1.indexOf(index);
        this.oldlines_1.splice(pos,1)
    }
    olddeleteLine(index){
        this.deletedannexes.push({id : index.id})
        let pos = this.oldlines.indexOf(index);
        this.oldlines.splice(pos,1)
    }
    changeAdminFee(fee,tax){
        if (this.fees[fee].taxes[tax].checkvalue == true){
            this.fees[fee].taxes[tax].value = this.adminfee //variable adminfee
        }
        else{
            this.fees[fee].taxes[tax].value = 0
        }
        this.fees[fee].taxes[tax+1].value = this.fees[fee].taxes[tax].value*(this.iva/100) //variable Tax (IVA)
        this.updateTotalFee(fee)
    }
    changeTaxSsc(){
        console.log("cambio ssc ", this.tax_ssc)
        console.log("this.totaldetails",this.totaldetails)
        if (this.tax_ssc) {
            console.log("if cuotas ", this.fees)
            //this.taxssc = 0            
            for (let i = 0; i < this.fees.length; i++ ){
                console.log("los pagos ", this.fees[i])
                
                //this.taxssc = (this.totaldetails)*(this.taxssc_percent/100)
                let sum = this.fees[i].details[4].value
                let discount = this.fees[i].discounts[0].value
                this.fees[i].taxes[2].value = (this.totaldetails+sum-discount)*(this.taxssc_percent/100)
                this.fees[i].total =  this.fees[i].total + this.fees[i].taxes[2].value
                //let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                //this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                //this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                this.total = this.total + this.fees[i].taxes[2].value 
            }
        } else {
            console.log("else cuotas ", this.fees)
            for (let i = 0; i < this.fees.length; i++ ){
                console.log("los pagos ", this.fees[i])
                console.log(i)
                this.fees[i].total = this.fees[i].total - this.fees[i].taxes[2].value
                this.total = this.total - this.fees[i].taxes[2].value
                this.fees[i].taxes[2].value = 0            
            }            
        }
        //this.updateFees()
    }
    updateTotalFee(fee){
        let details = this.fees[fee].details
        let detailsum = 0
        details.forEach(function(element) {
            detailsum = detailsum + element.value
        })

        let discounts = this.fees[fee].discounts
        let discountsum = 0
        discounts.forEach(function(element) {
            discountsum = discountsum + element.value
        })

        let taxes = this.fees[fee].taxes
        let taxsum = 0
        taxes.forEach(function(element) {
            taxsum = taxsum + element.value
        })
        this.fees[fee].total = (detailsum + taxsum) - discountsum

        let fees = this.fees
        let total = 0
        fees.forEach(function(element) {
            total = total + element.total
        })
        this.total = total
    }
    save(isValid){
        console.log('this.lines_1',this.lines_1)
        console.log('Fees',this.fees)
        if (isValid){
            this.isDisabled = true;
            let confirm = this.API.service('confirm', this.API.all('renewals'))
            let $state = this.$state
            confirm.post({
                'actual_number': this.actual_number,
                'number': this.policyinfo[0].number,
                'policyId': this.policyId,                
                'fees': this.fees,
                'amendments': this.lines_1,
                'annexes': this.lines,
                'deletedamendments': this.deletedamendments,
                'deletedannexes': this.deletedannexes,
                'updatedamendments': this.oldlines_1,
                'updatedannexes': this.oldlines,
                'mode': this.policyinfo[0].mode,
                'total_value': this.total,
                'effective_date': this.start_date_coverage,
                'have_renewal': this.policyinfo[0].have_renewal,
                'feeId' : this.feeId,
                'tax_ssc':this.tax_ssc

            }).then(function () {
                swal({
                    title: 'Confirmación de Póliza Exitosa!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.renewal-lists')
                })
            }, function (response) {
                swal({
                    title: 'Error al confirmar póliza',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })
            })
        }
        else{
            this.formSubmitted = true
        }
        console.log(this.lines_1)
    }

        changeTotal(){
                       
        this.fees = []
        this.total = 0
        this.datefees = new Date(this.dates.data[0].start_date_coverage)
                    for (let i = 0; i < this.amount_fees; i++ ){
                        this.fees.push({fee_id : i+1, id : this.feesData[i].id})
                        this.fees[i].details = []
                        this.fees[i].details.push({id: 1, detail: 'P/H Premium', value: this.amounttitular/this.amount_fees})
                        if (this.spouse != 0){
                            this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: this.amounth/this.amount_fees})
                        }
                        if (this.amountchild != 0){
                            let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: this.amountchild/this.amount_fees})
                        }
                        if (this.coverage_amount != 0){
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: this.coverage_name, value: this.coverage_amount/this.amount_fees})
                        }

                        let sum = 0
                        this.totaldetails = 0
                        for (let j = 0; j < this.fees[i].details.length; j++){
                            sum = sum + this.fees[i].details[j].value
                            this.totaldetails = this.totaldetails + this.fees[i].details[j].value
                        }
                        if (this.policyinfo[0].mode == 1){
                            sum = sum * 0.06
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }
                        if ((this.policyinfo[0].mode == 2) || (this.policyinfo[0].mode == 3) ){
                            sum = sum * 0.1
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }

                        let totalfee = 0
                        for (let k = 0; k < this.fees[i].details.length; k++){
                            totalfee = totalfee + this.fees[i].details[k].value
                        }
                        //descuento
                        this.fees[i].discounts = []
                        let discount = 0
                        if (this.policyinfo[0].discount == 1 ){
                            discount = ((totalfee-(this.coverage_amount/this.amount_fees))*this.policyinfo[0].discount_percent)/100
                            this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: discount })
                        }
                        //impuesto
                        this.fees[i].taxes = []
                        let adminfeeiva = 0

                        if (i == 0){
                            if (this.savedfees != ''){
                                if (this.savedfees[i].adminfee == 1){
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: this.adminfee, check:true, checkvalue:true })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: this.adminfee*(this.iva/100) })
                                    adminfeeiva = this.adminfee + (this.adminfee*(this.iva/100))
                                }
                                else{
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                }
                            }
                            else{
                                this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                            }
                        }else{
                            this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:false, checkvalue:false })
                            this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                        }

                         if (this.policyinfo[0].mode == 0) {
                            sum = 0;
                        }

                        
                                                //Tax SSCtaxssc_percent
                        if (this.savedfees != '') {
                            if (this.savedfees[i].tax_ssc == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                this.tax_ssc = true
                                console.log(this.taxssc)
                            }else{
                              this.taxssc = 0 //variable configuracion
                              this.tax_ssc = false
                            }                               
                        } else if (this.policyinfo[0].is_international == 0){
                            console.log("es Nacional")
                            this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                            this.tax_ssc = true
                        } else {
                            /*if (this.policyinfo[0].is_international == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                console.log(taxssc)
                                this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                                this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                            } else {*/
                                console.log("else fees para ssc ", this.fees[i])
                                this.tax_ssc = false
                                this.taxssc = 0
                            //}
                            
                        }
                        //Tax SSC
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: this.taxssc })
                        this.fees[i].total =  (totalfee+this.taxssc+adminfeeiva)-discount
                        /*let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                        this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                        this.total = this.total + this.fees[i].total
                        this.fees[i].fee_payment_date = this.feesData[i].fee_payment_date*/
                        this.total = this.total + this.fees[i].total


                        /*let taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                        this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                        this.total = this.total + this.fees[i].total
                        this.fees[i].fee_payment_date = this.feesData[i].fee_payment_date
                        /*

                        if (this.amount_fees == 1){
                            this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                        }
                        else if (this.amount_fees == 2){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = this.datefees.setMonth(this.datefees.getMonth() + 6)
                            }


                        }
                        else if (this.amount_fees == 4){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date =   this.datefees.setMonth(this.datefees.getMonth() + 3)
                            }
                        }
                        else if (this.amount_fees == 12){
                            if (i == 0){
                                this.fees[i].fee_payment_date = this.datefees.setDate(this.datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = this.datefees.setMonth(this.datefees.getMonth() + 1)
                            }
                        }
                        */
                    }


    }



    $onInit(){
    }
}

export const ConfirmRenewalDataComponent = {
    templateUrl: './views/app/components/confirm-renewal-data/confirm-renewal-data.component.html',
    controller: ConfirmRenewalDataController,
    controllerAs: 'vm',
    bindings: {}
}
