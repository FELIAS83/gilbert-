class TicketEditController
{
    constructor($scope, API, $state, $stateParams,$http, $filter, $compile, AclService, $uibModal){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        //this.filter=$filter
        this.API = API
        this.alerts = []
        this.$http = $http
        this.role = ""
        var ticket_id=$stateParams.ticket_id
        this.ticket_id=ticket_id
        var id_policy=$stateParams.id_policy
        this.id_policy =id_policy
        this.filenames=[]
        this.lines = []
        this.doc_typeselected = []
        this.value = []
        this.files=[]
        this.deletedlines = []
        this.directory='/tickets/'
        var systemDependents = []
        var systemApplicants= []
        var allAffiliates = []
        var allDependents = []
        var relationship = ""
        var orderRole = 0
        this.processing = false
        this.$uibModal = $uibModal
        this.llave = false
        this.llave2 = false
        
          
            if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }


        let systemDocTypes = []
        systemDocTypes.push({id: "1", name: "Formulario de Hospital"})
        systemDocTypes.push({id: "2", name: "Formulario de BD"})
        systemDocTypes.push({id: "3", name: "Informes Médicos"})
        systemDocTypes.push({id: "4", name: "Confirmación de Cita"})
        systemDocTypes.push({id: "5", name: "Formulario Interconsulta"})
        this.systemDocTypes = systemDocTypes
        //console.log(this.systemDocTypes)


        let TicketsData = API.service('show', API.all('tickets'))
        TicketsData.one(ticket_id).get()
        .then((response) => {
            this.ticketseditdata = API.copy(response)
            console.log("date_end ", this.ticketseditdata.data[0])
            this.ticketseditdata.data[0].date_start =  new Date ($filter('date')(this.ticketseditdata.data[0].date_start,'yyyy-MM-dd'))
            var date_start =  new Date(this.ticketseditdata.data[0].date_start)
            date_start = date_start.getTime() + 1000*60*60*24
            date_start = new Date(date_start)

            if (this.ticketseditdata.data[0].date_end !== null) {
                this.ticketseditdata.data[0].date_end =  new Date(this.ticketseditdata.data[0].date_end)
            }
            console.log("date_start ", this.ticketseditdata.data[0].date_start)
            this.ticketseditdata.data[0].hour = new Date("2018-01-01T"+this.ticketseditdata.data[0].hour)
            this.ticketseditdata.data[0].medical_speciality = parseInt (this.ticketseditdata.data[0].medical_speciality)
            console.log("ticketseditdata=>",this.ticketseditdata.data[0])
            //vm.changeDoctor()


        let TicketTypes = this.API.service('tickettypes')
        TicketTypes.getList()
        .then((response)=>{
            let systemTicketTypes = []
            let ticketyperesponse=response.plain()
            angular.forEach(ticketyperesponse, function (value){
                if(value.id==vm.ticketseditdata.data[0].type_id){
                    vm.type={id:value.id, name:value.name}
                //systemTicketTypes.push({id:value.id, name:value.name})
                }
                systemTicketTypes.push({id:value.id, name:value.name})
        })
            this.systemTicketTypes = systemTicketTypes
            console.log("systemTicketTypes"+JSON.stringify(this.systemTicketTypes))
        })    



        let Documents = API.service('showdocuments', API.all('tickets'))
        Documents.one(ticket_id).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
            this.previouslines[0].id_doc_type =String(this.previouslines[0].id_doc_type)
            //console.log("previouslinesdocuments",this.previouslines)            
        })

        let Policies = this.API.service('policies')
        Policies.getList({'completed': 1})
        .then((response) => {
            let systemPolicies = [] 
            let policiesResponse = response.plain()
            //console.log("policiesResponse"+JSON.stringify(policiesResponse))
            angular.forEach(policiesResponse, function (value) {
                if (value.policy_id == vm.ticketseditdata.data[0].id_policy){
                    vm.policyId = {number: value.number, id: value.policy_id, name: value.name}
                    //console.log("policyId"+JSON.stringify(vm.policyId))
                    systemPolicies.push({id: value.id, policy_id: value.policy_id, name: value.name, number: value.number})
                }
            })//foreach
         this.systemPolicies = systemPolicies
         //console.log("systemPolicies"+JSON.stringify(this.systemPolicies))              
        })//Policies
        
        let applicants = this.API.service('show', this.API.all('applicants'))
        applicants.one(id_policy).get()
        .then((response) => {
            this.applicantsdata = response.plain()
            //console.log("applicantsdata"+JSON.stringify(this.applicantsdata))
            angular.forEach(this.applicantsdata.data, function (value) {
                if((value.id == vm.ticketseditdata.data[0].id_person) && (vm.ticketseditdata.data[0].person_type =='A')) {
                    vm.person = {id: value.id, name: value.first_name+' '+value.last_name, person_type:'A'
                }
            //console.log("person"+JSON.stringify(vm.person))
            systemApplicants.push({id: value.id, policy_id: value.policy_id, name: value.first_name+' '+value.last_name, relationship: 'Titular',person_type: 'A'})      
                                    }
                                  
            allAffiliates.push({id: value.id, policy_id: value.policy_id, name: value.first_name+' '+value.last_name, role: value.relationship, order: orderRole, relationship: 'Titular', person_type: 'A'  })
            })
            this.systemApplicants = systemApplicants 
            //console.log("systemApplicants"+JSON.stringify(this.systemApplicants))
            this.allAffiliates = allAffiliates 
            //console.log("allAffiliates"+JSON.stringify(this.allAffiliates)) 
        })//end apaplicants  

        let Dependents = this.API.service('dependents', this.API.all('dependents'))
        Dependents.one(id_policy).get()
        .then((response) => {
            this.dependentsdata = response.plain()
            //console.log("dependentsdata"+JSON.stringify(this.dependentsdata))
            angular.forEach(this.dependentsdata.data, function (value) {
                            
                if(value.relationship == "H"){
                    relationship = "Cónyuge"
                    orderRole = 2
                }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        }           
    
                if((value.id == vm.ticketseditdata.data[0].id_person) && (vm.ticketseditdata.data[0].person_type =='D')){
                    vm.person = {id: value.id, name: value.first_name+' '+value.last_name, person_type:'D'
                }
               // console.log("person"+JSON.stringify(vm.person))                      
                systemDependents.push({id: value.id, policy_id: value.policy_id, name: value.first_name+' '+value.last_name, relationship: relationship, person_type: 'D'})}
                allAffiliates.push({id: value.id, policy_id: value.policy_id, name: value.first_name+' '+value.last_name, role: value.relationship, order: orderRole, relationship: relationship, person_type: 'D'})
            })//foreach dependents

            allAffiliates.sort(function (a, b) {
                if (a.order > b.order) {
                    return 1;
                }
                if (a.order < b.order) {
                    return -1;
                }
                return 0;
            });

            systemDependents.sort(function (a, b){
                return (b.relationship - a.relationship)
            })
            this.systemDependents = systemDependents
            //console.log("systemDependents"+JSON.stringify(this.systemDependents))
            this.allAffiliates = allAffiliates 
            //console.log("allAffiliates"+JSON.stringify(this.allAffiliates))   
        })//end response Dependents
        if (sessionStorage.getItem('info')){
            let info = JSON.parse(sessionStorage.getItem('info'))
            this.policyId = info.policy_id
            this.person = info.id_person
            //this.getAfilliates(this.policy_id)
            this.ticketseditdata.data[0].city = info.city
            this.ticketseditdata.data[0].doctor = info.doctor
            if (this.ticketseditdata.data[0].doctor){
                this.changeDoctor()
            }
            this.ticketseditdata.data[0].hospital = info.hospital
            this.ticketseditdata.data[0].date_start= new Date(info.date_start)
            this.ticketseditdata.data[0].date_end= new Date(info.date_end)
            this.ticketseditdata.data[0].hour = new Date(info.hour)
            this.type = info.systemTicketType

            this.ticketseditdata.data[0].medical_speciality = info.medical_speciality
            this.lines = info.lines
            this.doc_typeselected = info.doc_typeselected
            this.myFile = info.myFile
            this.files = info.files
            sessionStorage.removeItem('info')
            console.log('info',info)
        }
    })//end tickets  
        let doctors = this.API.service('doctors')
        doctors.getList({}).then((response) => {
            let dataSet = response.plain()
            this.doctors = dataSet
            console.log('doctors',this.doctors)
          })
        let hospital = this.API.service('hospitals')
        hospital.getList({}).then((response) => {
            let dataSet = response.plain()
            this.hospitals = dataSet
            console.log('hospitals',this.hospitals)
          })
        let medicalspecialties = this.API.service('medicalspecialties')
        medicalspecialties.getList({})
        .then((response) => {
            let dataSet= response.plain()
            this.medicalspecialties = dataSet
            console.log("medicalspecialties",this.medicalspecialties)
        })

} //constructor

     getpersontype(){
        var select=document.getElementById("id_person").value;   
        var tipo =select.substr(0,1)
        var id=select.substr(1)
        this.id=id
        this.tipo=tipo
     }   

    changeDocType(index,type){
        this.indice = index
        if (type == 1){
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
               
        }
        else{
            this.lines[index].disabled = true
            this.lines[index].rep = 'No '
            
            this.value[index] = ''
            }
        }
        else{//anteriores
        if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
        }
        else{
            this.previouslines[index].disabled = true
            this.previouslines[index].rep = 'No '
          
            }
        }

    }

    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
            }
        this.lines.push({id : newid});
    }

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    deleteLine(index){//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('value',this.value)
        console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('value',this.value)
        console.log('files',this.files)
    }

    getType(){
       var type= document.getElementById('ticket_type');
       //alert(type.value);
       var tipo=type.value;
       return(tipo);        
     }


     modalDoctor(size) {
        let info = {}
        info.policy_id = this.policyId
        info.id_person = this.person
        info.city = this.ticketseditdata.data[0].city
        info.hospital = this.ticketseditdata.data[0].hospital
        info.medical_speciality = this.ticketseditdata.data[0].medical_speciality
        info.date_start = this.ticketseditdata.data[0].date_start
        info.date_end = this.ticketseditdata.data[0].date_end
        info.hour = this.ticketseditdata.data[0].hour
        info.systemTicketType = this.type
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalDoctor',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size
      })
    }

    modalHospital(size) {
        let info = {}
        info.policy_id = this.policyId
        info.id_person = this.person
        info.city = this.ticketseditdata.data[0].city
        info.doctor = this.ticketseditdata.data[0].doctor
        info.medical_speciality = this.ticketseditdata.data[0].medical_speciality
        info.date_start = this.ticketseditdata.data[0].date_start
        info.date_end = this.ticketseditdata.data[0].date_end
        info.hour = this.ticketseditdata.data[0].hour
        info.systemTicketType = this.type
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))
        console.log('modalhospitla', info)

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalHospital',
          controller: this.modalcontroller2,
          controllerAs: 'mvm',
          size: size
      })
    }
    changeDoctor(){
        this.llave=true
        let doctorData = this.API.service('show', this.API.all('doctors'))//ojo
        doctorData.one(this.ticketseditdata.data[0].doctor).get()
        .then((response) => {
            $(".js-example-basic-single4").select2({
                placeholder: "Seleccione Espcialidad"
             });
            let dataSet = this.API.copy(response)
            this.doctordata = dataSet.data.specialities
        })
    }

    ChangeDoctorBySpec(id){
        this.llave2 = true 
        let doctorbyspec = this.API.service('doctorbyspec', this.API.all('doctors'))
        doctorbyspec.one(id).get()
        .then((response) => {

            $(".js-example-basic-single2").select2({
                placeholder: "Seleccione Médico"
             });
             let systemdoctorbyspec = []
             let doctorbyspecResponse= response.plain()
                        
              angular.forEach(doctorbyspecResponse.data.data, function (value) {
              systemdoctorbyspec.push({id: value.doctor_id, names: value.names+' '+value.surnames})
               })
              this.systemdoctorbyspec=systemdoctorbyspec
           
            
            return true
        })
    }

    modalcontroller (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state
        console.log('doctoros')
        let Countrys = API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
            console.log('paises',this.systemCountrys)
        })



        this.getState = function(){
            
            console.log("pais",this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        
        /*this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }*/

        this.getState = function(){
            
            console.log("pais",this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        
        /*this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }*/

        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist
        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                let doctors = API.service('doctors', API.all('doctors'))
                let $state = this.$state
                let cancel = this.cancel
                doctors.post({
                    'names': this.names,
                    'surnames': this.surnames,
                    'identification' : this.identification,
                    'phone': this.phone,
                    'address':this.address,
                    'country_id':this.country,
                    'province_id':this.state,
                    'city':this.city,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                    swal({
                               title: 'Médico guardado Correctamente!',
                               type: 'success',
                               confirmButtonText: 'OK',
                               closeOnConfirm: false
                           }, function () {
                               cancel()                                                                                                  
                               swal.close()
                               $state.reload()
                           })
                       , function (response) {
                           swal({
                               title: 'Error Inesperado!',
                               type: 'error',
                               confirmButtonText: 'OK',
                               closeOnConfirm: true
                           })
                       }
                })
            } else {
                this.formSubmitted = true
            }
        }
    }

    modalcontroller2 (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state
        this.specialtiesselected = {}

        let Countrys = API.service('countrys')
        Countrys.getList()
        .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
                systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
        })

        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist
        })

        this.getState = function(){
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        /*this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }*/

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                this.processing = true
                let hospitals = API.service('hospitals', API.all('hospitals'))
                let $state = this.$state
                let cancel = this.cancel

                hospitals.post({
                    'name': this.name,
                    'country_id' : this.country,
                    'province_id' : this.state,
                    'city' : this.city,
                    'address' : this.address,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                    cancel()
                    $state.reload()
                }, function (response) {})
            } else {
                this.formSubmitted = true
            }
        }
    }


    save (isValid) {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        //console.log("isvalid"+isValid)    
                                        
            if (this.pfiles){
                for (var i = 0; i < this.previouslines.length; i++) {
                    if (this.pfiles[i]) {
                    this.previouslines[i].filename = this.pfiles[i][0].name
                    }
                }
            }
                 
            if (isValid){ 
                this.processing = true
                let uploadFile = this.uploadFile         
                let filenames = []
                angular.forEach(this.myFile, function (value) {
                filenames.push(value.name)
                })
            this.filenames = filenames

            var vm = this
            if (this.ticketseditdata.data[0].hour !== null) {
                let tiempo=this.ticketseditdata.data[0].hour.toString()
                //console.log("tiempo"+tiempo)
                var hora=tiempo.substring(16,21)
                //console.log("hora"+hora)
            }
            let ticketsdocs = this.API.service('ticketsdocs', this.API.all('tickets')) 
            //console.log("ticket_id"+ticket_id)

            let $state = this.$state    
            ticketsdocs.post({
               'ticket_id':this.ticket_id,
               'id_ticket_type':this.getType(),
               'id_person':this.id,
               'person_type':this.tipo,
               'hour':hora,
               'doctor':this.ticketseditdata.data[0].doctor,    
               'medical_speciality':this.ticketseditdata.data[0].medical_speciality,
               'hospital':this.ticketseditdata.data[0].hospital,
               'city':this.ticketseditdata.data[0].city,
               'date_start':this.ticketseditdata.data[0].date_start,
               'date_end':this.ticketseditdata.data[0].date_end,
               'id_policy': this.policy_id,
               'id_doc_types':this.doc_typeselected,
               'filenames':this.filenames,
               'directory':this.directory,
               'modified' : this.previouslines,
               'deleted_ids' : this.deletedlines
            }).then(function () {
                vm.uploadFile()
                //console.log("mensaje", $state.current);
                let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el ticket.' }
                $state.go($state.current, { alerts: alert})
                //console.log($state.current);
                }, function (response) {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                    })
            } 
            else{ 
                this.formSubmitted = true
                }
    }

    uploadFile(){
        var i = 0
        var files = this.files//archivos nuevos
        var pfiles = this.pfiles//archivos que sustituiran a los anteriores
        var ticket_id = this.ticket_id
        var $http = this.$http

        console.log("ticket_id",ticket_id)
        angular.forEach(files, function(files){
            var form_data = new FormData();
            angular.forEach(files, function(file){
                form_data.append('file', file);
            })
            form_data.append('ticket_id', ticket_id);
            $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   })
            })
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                form_data.append('ticket_id', ticket_id);
                $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })
            this.processing = false
        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Los documentos han sido guardados exitosamente.'}
        this.alerts = [alert]
        //this.$state.reload();
    }


    mostrar(){
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('modifies',this.previouslines)
        console.log('value',this.value)
        console.log('files',this.files)
    }
        

    $onInit(){
    }
}

export const TicketEditComponent = {
    templateUrl: './views/app/components/ticket-edit/ticket-edit.component.html',
    controller: TicketEditController,
    controllerAs: 'vm',
    bindings: {}
}
