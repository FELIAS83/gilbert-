class PolicyListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams,$filter){
        'ngInject';

        this.API = API
        this.$state = $state
        var fecha = ""
        var arrfecha=""
        var fecha1 = ""
        var arrfecha1=""
        var app = angular.module('myApp', ['angular-growl']);

        let Policies = this.API.service('policies')
        
        Policies.getList({})
          .then((response) => {
            let dataSet = response.plain()
            for (var i = 0; i < dataSet.length; i++) {
              if(dataSet[i].send_to_reception == 1){
                
                dataSet[i].send_to_reception = false
              
              }else{
              
                 dataSet[i].send_to_reception = true 
              
              }
            }


            angular.forEach(dataSet, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= month+'/'+day+'/'+year  
              value.created = fecha1
            })
            console.log("fecha",fecha)


            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('created').withTitle('Creación del Trámite'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Vigencia del Trámite'),
              //DTColumnBuilder.newColumn('status').withTitle('Estado'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Info. Trámite" class="btn btn-xs btn-primary" ui-sref="app.policy-view({policyId: ${data.id}})">
                      <i class="fa fa-tag"></i>
                    </a>                    
                    &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Póliza al Cliente" class="btn btn-xs btn-primary" style="background-color: #ddd"  ng-disabled="${data.send_to_reception} == 1" ui-sref="app.print-guide({policyId: ${data.id}})">
                        <i class="fa fa-paper-plane"></i>
                    </button>                    
                  </ul>`
        }
    }

    $onInit(){
    }
}

export const PolicyListsComponent = {
    templateUrl: './views/app/components/policy-lists/policy-lists.component.html',
    controller: PolicyListsController,
    controllerAs: 'vm',
    bindings: {}
}
