class PolicyRegisterController{
    constructor($scope, $stateParams, $state, API, $uibModal,  $log, $rootScope){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.$uibModal = $uibModal
        this.$log =  $log
        this.$rootScope = $rootScope
        $scope.form = {};
        vm.hideCoverage = false
        vm.getDeductible = getDeductible
        vm.getFees = getFees
        vm.plan_id = []
        vm.coverage_id = 0
        vm.deductible_id = []
        var systemDeductibles = []
        var allDeductibles = []
        var allCoverages = []
        var allCoveragesDet = []
        var coverAmount = ""
        var nullArray = {id: 0, name: "- Ninguna -", plan_id: 3, amount: 0}
        this.lines = []
        this.dependents = []//falta validar que muestre los checks y que se muestre la informacion de la unidad
        this.heightunit = 'Cms'
        this.methods = []
        this.modes = []
        this.methods.push({id: 0, name: 'Cheque'},
                         {id: 1, name: 'Transferencia'},
                         {id: 2, name: 'Depósito Bancario'},
                         {id: 3, name: 'Tarjeta de Crédito'})
        this.modes.push({id: 0, name: 'Anual'},
                         {id: 1, name: 'Semestral'},
                         {id: 2, name: 'Trimestral'},
                         {id: 3, name: 'Mensual'})
        vm.method =  ""
        vm.mode =  ""
        vm.isDisabled = false
        this.fees = []
        var country_id = ""
        var province_id = ""
        
       // this.modalValidate('')


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        

        let Agents = this.API.service('agents')
        Agents.getList()
        .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            angular.forEach(agentsResponse, function (value) {
                systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
        })

         let Plans = this.API.service('plans')
                Plans.getList()
                  .then((response) => {
                    let systemPlans = []
                    let planResponse = response.plain()
            
                    angular.forEach(planResponse, function (value) {
                      
                    systemPlans.push({id: value.id, name: value.name})
                    })

                    this.systemPlans = systemPlans
                    
                     })

                   let Deductible = this.API.service('deductibles')
                    Deductible.getList()
                        .then((response) => {
                       
                         let deductiblesResponse = response.plain()
                        //console.log("respuesta deducibles", deductiblesResponse)
                        angular.forEach(deductiblesResponse, function (value) {
                            allDeductibles.push({id: value.id, name: value.name, plan_id: value.plan_id, in: value.amount_in_usa, out: value.amount_out_usa})
                        })

                        this.systemDeductibles = systemDeductibles
                        this.allDeductibles = allDeductibles
                       // console.log("allDeductibles==> ", allDeductibles)
                        //console.log("systemDeductibles==> ", allDeductibles)
                        })

                        let CoverageDetails = this.API.service('coverage_details')
                        CoverageDetails.getList()
                        .then((response) => {
                        let systemCoveragesDet = []
                        let coverageDetResponse = response.plain()
                           //console.log("coverageDetResponse ", coverageDetResponse)
                           //console.log("vm.deductible_id.id  ", vm.deductible_id.id )
                        angular.forEach(coverageDetResponse, function (cover) {
                                coverAmount = cover.amount
                                allCoveragesDet.push({id: cover.id, amount: cover.amount, deductible_id:cover.deductible_id, coverage_id: cover.coverage_id})
                            })   

                            
                        })
                        this.allCoveragesDet = allCoveragesDet
                        //console.log("Todas las coberturas",coverAmount)

                    let Coverage = this.API.service('coverages')
                    Coverage.getList()
                        .then((response) => {
                        let systemCoverages = [] 
                        let coveragesResponse = response.plain()
                        //console.log("coveragesResponse", coveragesResponse)
                        //systemCoverages.push({id: 0, name: "- Ninguna -", plan_id: 3, amount: ""})     
                        angular.forEach(coveragesResponse, function (value) {

                            allCoverages.push({id: value.id, name: value.name, plan_id: value.plan_id})
                            //allCoverages = allCoverages.concat(nullArray)
                    })
                    systemCoverages =  systemCoverages.concat(nullArray)        
                    this.systemCoverages =  systemCoverages
               
                    allCoverages = allCoverages.concat(nullArray)    
                    //console.log("all Coverages==> ", allCoverages)
                    //console.log("vm.coverage_id==> ", vm.coverage_id)


                    })

        /******************************** Pais-Provincia-Ciudad *******************************/
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            //console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            //console.log("systemCountrys==> ", systemCountrys)
        })

         let States = API.service('index', API.all('states'))
        States.one().get()
          .then((response) => {
            let systemStates = []
            let allStates = []
            let stateResponse = response.plain()
            angular.forEach(stateResponse.data.states, function (value) {
              if(value.country_id == country_id){
                systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
              }
              allStates.push({id: value.id, name: value.name, country_id: value.country_id})
            })
            this.systemStates = systemStates
            this.allStates = allStates
            //console.log("systemStates==> ", systemStates)
            //console.log("allStates==> ", allStates)


            let Cities = API.service('index', API.all('cities'))
            Cities.one().get()
              .then((response) => {
                let systemCities = []
                let allCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                  if(value.province_id == province_id){
                    systemCities.push({id: value.id, name: value.name, province_id: value.province_id})
                  }
                  allCities.push({id: value.id, name: value.name, province_id: value.province_id})
                })
                this.systemCities = systemCities
                this.allCities = allCities
                //console.log("systemCities==> ", systemCities)
                //console.log("allCities==> ", allCities)

                
            })

        })
    
        /**************************************************************************************/                


        function getDeductible(){
            let flag = false
            let systemDeductibles = []
            vm.coverAmount = "0"
            angular.forEach(allDeductibles, function (value) {
                //console.log("plan id de allDeductibles",  value.plan_id, "plan seleccionado", vm.plan_id)
                if(vm.plan_id.id == value.plan_id){
                     console.log("Entro",vm.plan_id.id)
                    systemDeductibles.push({id: value.id, name: value.name, plan_id: value.plan_id, in: value.in, out: value.out})
                
                }
               
            })
            this.systemDeductibles = systemDeductibles

           if(vm.plan_id.id == 3){
            vm.hideCoverage = true
           }else{
            vm.coverage_id = 0
            vm.hideCoverage = false
           }
        }

        function getFees(){

            let nFee = 0
            let fees = []
            //console.log("modo seleccionado", this.mode)
            if(this.mode == 0){
                nFee = 1
            }else if(this.mode == 1){
                nFee = 2
            }
            else if(this.mode == 2){
                nFee = 4
            }else{
                nFee = 12
            }

            for (var i = 1; i <= nFee; i++) {
                 fees.push({id: i, name: i})

            }
            this.fees = fees
            //console.log("Cuotas", this.fees)
        }


        //
    }
/*******************************************************************************/
getState(){
        console.log("Entro..", this.country_id)
        this.rowStatus = false;
        //console.log("allStates ", this.allStates);
        //console.log(this.systemCanton);
        var country_id = this.country_id
        let systemStates = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allStates, function (value) {
          if(value.country_id == country_id){
            systemStates.push({id: value.id, name: value.name})
          }
        })
        if (country_id !== ""){
          this.rowStatus = true;
        }    
        this.systemStates = systemStates
        this.systemCities = ""
        //console.log("systemStates ", systemStates)
    }

    getCity(){
        console.log("Entro..", this.country_id)
        this.rowStatus = false;
        console.log("allCities ", this.allCities);
        //console.log(this.systemCanton);
        var province_id = this.province_id
        let systemCities = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allCities, function (value) {
          if(value.province_id == province_id){
            systemCities.push({id: value.id, name: value.name})
          }
        })
        if (province_id !== ""){
          this.rowStatus = true;
        }    
        this.systemCities = systemCities
        //this.systemCities = ""
        console.log("systemCities ", systemCities)

    }

/******************************************************************************/
    addLine() {
        this.lines.push({id : this.lines.length+1});
        //console.log(this.lines)
    }

     deleteLine (dependentid,index){
        //console.log(dependentid)
        this.API.one('dependents').one('dependents', dependentid).remove()
          .then(() => {
          })

        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
        //console.log('newlines',this.lines)
        this.dependents.splice(pos,1)
    }

     save (isValid) {
         //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        
        let  plan_id = this.plan_id.id
        let coverage_id = this.coverage_id
        let nfee = 0
        let diffFee = 0
      
        if(this.mode == 0){
                nfee = 1   
        }
        else if(this.mode == 1){
                nfee = 2  
        }
        else if(this.mode == 2){
                nfee = 4        
        }
        else{
                nfee = 12      
            }


         
          if (isValid) { 
               this.isDisabled = true
               let Policies = this.API.service('policies', this.API.all('registerpolicies'))
               let $state = this.$state
               diffFee = nfee -   this.numfee.id
               console.log("$rootScope.number", this.$rootScope.number)
            Policies.post({
                'number': this.$rootScope.number,
                'date_start_coverage': this.date_start_coverage,
                'plan_id': plan_id,
                'deductible_id': this.deductible_id.id,
                'coverage_id': coverage_id,
                'agent_id': this.seller_id,
                'first_name': this.first_name,
                'last_name': this.last_name,
                'identity_document': this.identity_document,
                'email': this.email,
                'birthday': this.birthday,
                'place_of_birth': this.place_of_birth,
                'address': this.address,
                'mobile': this.mobile,
                'phone': this.phone,
                'country_id': this.country_id,
                'province_id': this.province_id,
                'city_id': this.city_id,
                'civil_status': this.civil_status,
                'occupation': this.occupation,
                'gender': this.gender,
                'height': this.height,
                'height_unit_measurement': this.height_unit_measurement,
                'weight': this.weight,
                'weight_unit_measurement' : this.weight_unit_measurement,
                'dependents' : this.dependents,
                'mode': this.mode,
                'method': this.method,
                'payment_date': this.payment_date,
                'amount': this.amount,
                'fees' : this.numfee.id,
                'diffFee': diffFee

               }) 
                .then( ( response ) => {
                        /*swal({
                            title: 'Póliza Registrada!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go($state.current, {}, { reload: $state.current}) 
                        })*/
                        $state.go('app.policy_register_amendment', {"policyId": response.data}) 
                        
                    }, function (response) {
                        swal({
                            title: 'Error al Registrar Póliza!',
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                        })
                    }
                    )

          }else {
            this.formSubmitted = true
        }

     }


      modalValidate (number) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      //console.log("a", number);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalValidate',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'sm',
        resolve: {
         number: () => {
            return number
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }


modalcontroller ($scope, $uibModalInstance, API, $http, $state, $stateParams, number, $rootScope) {
    'ngInject'
        var mvm = this
        this.API = API
        this.$http = $http
        var $state = $state
        this.$state = $state
        this.number = number
        this.processing = true
        this.deleting = false
        this.formSubmitted = false
        this.motive = 0
        this.alert = ""
        $rootScope.color = "#ff0000"
        $rootScope.number = ""

        this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.validate = (isValid) => {

        if (isValid) { 
                this.processing = false
                this.deleting = true
                let PoliciesData = API.service('show', API.all('registerpolicies'))
                PoliciesData.one(this.number).get()
                    .then((response) => {
                        this.policydata = API.copy(response)
                        //console.log(this.policydata)
                        let policyResponse = response.plain(response)
                    if(this.policydata.data == 1){
                        this.alert = "Póliza ya Existe!"
                        this.processing = true
                        this.deleting = false
            }else{
                localStorage.setItem('number',JSON.stringify(this.number))
                $uibModalInstance.dismiss('cancel')
                $rootScope.number = JSON.parse(localStorage.getItem('number'))
                $rootScope.color = "#33cc33"
            }
                })

        }else{ 

            this.formSubmitted = true

            }

      }

    }    


    $onInit(){
    }
}

export const PolicyRegisterComponent = {
    templateUrl: './views/app/components/policy_register/policy_register.component.html',
    controller: PolicyRegisterController,
    controllerAs: 'vm',
    bindings: {}
}
