class EmissionRenewalListController{
    constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http){
        'ngInject';
        this.API = API
        this.$state = $state
        this.$uibModal = $uibModal
        this.$log = $log
        this.animationsEnabled = true
        this.uisrefparameter = ' {policyId: 0}'
        var fecha = ""
        var arrfecha=""
        var fecha1 = ""
        var arrfecha1=""

        let DependentData = API.service('getDependentsforpolicy') 

        DependentData.getList({})
          .then((response) => {
            let dataSet = response.plain()
            var client = dataSet.first_name+' '+dataSet.last_name

            angular.forEach(dataSet, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= month+'/'+day+'/'+year  
              value.created = fecha1
            })
            console.log("fecha",fecha)

               this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('policy_id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('created').withTitle('Fecha de Creación'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true

          })
          let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }
        let actionsHtml = (data) => {
          return `<ul>
                    <a ng-if="${data.step1 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-primary" ng-disabled="${data.step == 1}" ui-sref="app.emmision-renewal-add({policyId: ${data.id}})">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a ng-if="${data.step1 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-default" ng-disabled="${data.step == 1}" ui-sref="app.emmision-renewal-add({policyId: ${data.id}})">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    &nbsp                                 
                    <a ng-if="${data.step2 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza BD" class="btn btn-xs btn-primary" ng-disabled="${data.step == 2}" ui-sref="app.confirm-policy-renewal({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>
                    <a ng-if="${data.step2 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza BD" class="btn btn-xs btn-default" ng-disabled="${data.step == 2}" ui-sref="app.confirm-policy-renewal({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>                  
                     &nbsp
                    <a ng-if="${data.step4 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-primary" ng-disabled="${data.step == 4}" ui-sref="app.register-payment-renewal({policyId: ${data.id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step4 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-default" ng-disabled="${data.step == 4}" ui-sref="app.register-payment-renewal({policyId: ${data.id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                     &nbsp

                    <a ng-if="${data.step7 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-primary" ng-disabled="${data.step == 7}" ui-sref="app.send-invoice-emmision-renewal({policyId: ${data.id}})">
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step7 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-default" ng-disabled="${data.step == 7}" ui-sref="app.send-invoice-emmision-renewal({policyId: ${data.id}})">
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <a ng-if="${data.step6 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago" class="btn btn-xs btn-primary" ui-sref="app.confirm-payment-renewal({policyId: ${data.id}})">
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step6 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago" class="btn btn-xs btn-default" ui-sref="app.confirm-payment-renewal({policyId: ${data.id}})">
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <button style="color: #FE2E2E; background-color: #FFF;" title="Eliminar Póliza" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                    <!--<button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Póliza al Cliente" class="btn btn-xs btn-primary" style="background-color: #ddd"  ng-disabled="${data.send_to_reception} == 1" ui-sref="app.print-guide({policyId: ${data.id}})">
                        <i class="fa fa-paper-plane"></i>
                    </button>-->                    
                  </ul>`
        }


        //
    }

    delete (policyId) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", policyId);
      var modalInstance = $uibModal.open({     
        templateUrl: 'myModalDelete',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'md',
        resolve: {
          policyId: () => {
            return policyId
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
     
    }


modalcontroller ($scope, $uibModalInstance, API, $http, $state, $stateParams, policyId) {
    'ngInject'
        var mvm = this
        this.API = API
        this.$http = $http
        var $state = $state
        this.$state = $state
        this.policyId = policyId
        this.processing = true
        this.deleting = true
        this.formSubmitted = false
        this.motive = 0
        this.alert = ""


      let PolicyData = this.API.service('show', this.API.all('policies'))
      PolicyData.one(policyId).get()
      .then((response)=>{
        this.PolicyData=this.API.copy(response)
      })

        let systemMotive = []
        systemMotive = [{id: 1, name: "Cambio de Compañia"},{id: 2, name: "Temas Económicos"},{id: 3, name: "Cambio de Agencia"},{id: 4, name: "Otros"}] 
        this.systemMotive = systemMotive


      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.changeMotive = () => {

      if(this.motive == 0){
          this.processing = true
          this.deleting = true
        }
        else{
          
          this.processing = false
        }

      }

      
      this.deletePolicy = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log("Dependiente a eliminar", mvm.policyId)
        console.log("Dependiente a eliminar", mvm.motive)
        console.log(isValid)
        if (isValid){
          let $state = this.$state
          let Policy = API.service('MotiveRenewal')

              Policy.post({
                'policyId' : mvm.policyId,
                'motive': mvm.motive
              }).then(function (response) {
                
                $uibModalInstance.dismiss('cancel')
                
             swal({
                    title: 'Póliza eliminada Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current, {}, { reload: $state.current})
                })
                }, function (response) {
                    swal({
                    title: 'Error al guardar motivo' + response.message,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                    })
                  }
                )

            
         
        }
          else
              {
              this.formSubmitted = true
              }
            
         
          }  



    }

    $onInit(){
    }
}

export const EmissionRenewalListComponent = {
    templateUrl: './views/app/components/emission-renewal-list/emission-renewal-list.component.html',
    controller: EmissionRenewalListController,
    controllerAs: 'vm',
    bindings: {}
}
