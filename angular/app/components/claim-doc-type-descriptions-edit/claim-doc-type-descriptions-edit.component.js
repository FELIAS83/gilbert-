class ClaimDocTypeDescriptionsEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false
        //this.$stateParams = $stateParams

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let claimdoctypeId = $stateParams.claimdoctypeId
        let claimdoctypedescId = $stateParams.claimdoctypedescId
        //console.log("claimdoctypedescId",claimdoctypedescId)
        //console.log("claimdoctypeId",claimdoctypeId)

        let ClaimDocTypeData = API.service('show', API.all('claimdocumenttypes'))
        ClaimDocTypeData.one(claimdoctypeId).get()
          .then((response) => {
            this.claimdoctypeditdata = API.copy(response)
            //this.currencyeditdata.data.status
          //console.log("data=>", this.currencyeditdata);
        })
         let claimdoctypedesc = API.service('show', API.all('claimdocumenttypedescriptions'))
        
        claimdoctypedesc.one(claimdoctypedescId).get()
          .then((response) => {
            this.claimdoctypedesceditdata = API.copy(response)
            console.log("claimdoctypedesceditdata",JSON.stringify(this.claimdoctypedesceditdata))
        })  
    }

    save (isValid) {
        
        if (isValid) {
            //this.$state.go(this.$state.current, {}, { alerts: 'test' })
          this.processing = true
            let vm = this
          console.log(this.claimdoctypeditdata);
          let $state = this.$state
          this.claimdoctypedesceditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el tipo de Documento.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }

    }

    $onInit(){
    }
}

export const ClaimDocTypeDescriptionsEditComponent = {
    templateUrl: './views/app/components/claim-doc-type-descriptions-edit/claim-doc-type-descriptions-edit.component.html',
    controller: ClaimDocTypeDescriptionsEditController,
    controllerAs: 'vm',
    bindings: {}
}
