class CoverageAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let planId = $stateParams.planId

        console.log("id del plan", planId)
        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(planId).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
            console.log("data del plan", this.planeditdata.data)
            let planResponse = response.plain(response)
        
      })

        //
    }
      save (isValid) {    
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {          
          let Coverage = this.API.service('coverages', this.API.all('coverages'))
          let $state = this.$state          
          
          Coverage.post({
            'name': this.name,
            'plan_id': this.planeditdata.data.id

          }).then(function () {
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido la cobertura.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const CoverageAddComponent = {
    templateUrl: './views/app/components/coverage-add/coverage-add.component.html',
    controller: CoverageAddController,
    controllerAs: 'vm',
    bindings: {}
}
