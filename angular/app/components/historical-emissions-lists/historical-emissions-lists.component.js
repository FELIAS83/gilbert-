class HistoricalEmissionsListsController{
    constructor(API,DTOptionsBuilder,DTColumnBuilder,$compile,$scope){
        'ngInject';
         var fecha = ""
        var arrfecha=""
        var fecha1 = ""
        var arrfecha1=""

        let Policies = API.service('policies')
        let data = {}
        data.completed = 1

        Policies.getList(data)
          .then((response) => {
            let dataSet = response.plain()

            angular.forEach(dataSet, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= month+'/'+day+'/'+year  
              value.created = fecha1
            })

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza').withOption('width', '8%'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('created').withTitle('Fecha de Creación').withOption('width', '15%'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura').withOption('width', '15%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml).withOption('width', '16%')
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Solicitud" class="btn btn-xs btn-primary" ui-sref="app.emission-applicant-data({policyId: ${data.id},completed:1})">
                      <i class="fa fa-arrow-right"></i>
                    </a>
                    &nbsp
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Datos Póliza BD" class="btn btn-xs btn-primary" ui-sref="app.confirm-policy-data({policyId: ${data.id}, completed: 1})">
                      <i class="fa fa-check"></i>
                    </a>
                    &nbsp
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Documentos" class="btn btn-xs btn-primary" ui-sref="app.policy-documents({policyId: ${data.id}}) ">
                      <i class="fa fa-file"></i>
                    </a>
                    &nbsp
                  </ul>`
        }

    }

    $onInit(){
    }
}

export const HistoricalEmissionsListsComponent = {
    templateUrl: './views/app/components/historical-emissions-lists/historical-emissions-lists.component.html',
    controller: HistoricalEmissionsListsController,
    controllerAs: 'vm',
    bindings: {}
}
