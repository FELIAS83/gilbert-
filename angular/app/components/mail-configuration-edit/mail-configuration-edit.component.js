class MailConfigurationEditController{
    constructor ($scope,$stateParams, $state, API){
        'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.alerts = []
    this.theform = []
    this.API = API
    this.protocols =  [
        {id:0, name:'SSL'},
        {id:1, name:'TLS'},
        {id:2, name:'STARTTLS'},
        {id:3, name:'SMTP'}
    ];

        if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }


    this.id = 1



    if(this.id){

      this.type = 'modificado'

      let data = API.service('show', API.all('mail_configurations'))
      data.one(this.id).get()
        .then((response) => {
          this.theform = API.copy(response);
          console.log(this.theform)

        })
    }
  }

    save (isValid) {
    this.alerts = [];
    if (isValid) {

        //intentar enviar un correo a ver si la configuracion es correcta
      // this.API.all('trymail')
       // .post({ host : this.theform.data.host,
        //        username : this.theform.data.username,
         //       password : this.theform.data.password,
          //      protocol : this.protocols[this.theform.data.protocol].name,
           //     'port' : this.theform.data.port,
            //    'name' : this.theform.data.name} )
             //   .then( ( response ) => {
             //       this.result = this.API.copy(response)
           // this.theform.put()
        //.then(() => {
          console.log(this.data);
           let $state = this.$state;
           this.theform.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado la configuracion de correo.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }

      //    let alert = { type: 'success', 'title': 'Éxito!', msg: 'Configuración modificada exitosamente.' }
       //   $state.go($state.current, { alerts: alert})
        //  this.alerts = [alert]

        //}, (response) => {
        //  let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        //  $state.go($state.current, { alerts: alert})
         //   this.alerts = [alert]
        //})

       // }, (response) => {
        //  let alert = { type: 'error', 'title': 'Error!', msg: 'No se ha podido establecer conexion con el servidor de correo! Compruebe su conexión a internet y los parámetros de conexión. Configuración no guardada' }
         // $state.go($state.current, { alerts: alert})
          //  this.alerts = [alert]
        //})
        //////////////////////

}
    $onInit(){
    }
}

export const MailConfigurationEditComponent = {
    templateUrl: './views/app/components/mail-configuration-edit/mail-configuration-edit.component.html',
    controller: MailConfigurationEditController,
    controllerAs: 'vm',
    bindings: {}
}
