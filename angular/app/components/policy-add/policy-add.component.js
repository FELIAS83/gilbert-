class PolicyAddController{
    constructor ($scope, API, $state, $stateParams,fileUpload,$http) {
        'ngInject';

        this.API = API
        this.$state = $state
        this.fileUpload = fileUpload
        this.uploadFile = uploadFile

        this.formSubmitted = false
        this.showhidepregnant = false;

        this.alerts = []
        this.lines = []
        this.lines.push({id : 0});
        this.lines.push({id : 1});
        this.myFile = [] //array con datos de archivos
        this.systemDocTypes = []
        this.description = []
        this.filenames = []
        this.doc_typeselected = []
        this.doc_typeselected[0] = 1
        this.doc_typeselected[1] = 2
        this.files = []
        this.success = 0

        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let Agents = this.API.service('agents')
        Agents.getList()
        .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            angular.forEach(agentsResponse, function (value) {
                systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
        })

        let Plans = this.API.service('plans')
        Plans.getList()
        .then((response) => {
            let systemPlans = []
            let plansResponse = response.plain()
            angular.forEach(plansResponse, function (value) {
                systemPlans.push({id: value.id, name: value.name})
            })

            this.systemPlans = systemPlans
        })

        let DocTypes = this.API.service('doctypes')
        DocTypes.getList()
        .then((response) => {
            let systemDocTypes = []
            let docTypesResponse = response.plain()
            angular.forEach(docTypesResponse, function (value) {
                systemDocTypes.push({id: value.id, name: value.name})
            })

            this.systemDocTypes = systemDocTypes
            console.log(this.systemDocTypes)
        })

        this.registerFile = function(file,item){
            var index=$scope.registers.indexOf(item);
            if($scope.form.files[index]==undefined){
                $scope.form.files[index]={};
            }
            if($scope.viewprocess==false){
                var ts=Math.floor(Date.now() / 1000);
                $scope.form.files[index]['ts'] = ts;
            }
            $scope.form.files[index]['f']=file;
            $scope.form.files[index]['name']=file.name;
        }

        function uploadFile(){
            var i = 0
            for (i = 0; i < 100; i++) {
               if (this.files[i]){
                    var form_data = new FormData();
                    angular.forEach(this.files[i], function(file){
                        form_data.append('file', file);
                    })
                    $http.post('uploadpolicies', form_data,
                    {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    }).success(function(response){    })
               }
            }
       }

    }

    save (isValid) {
        let filenames = []
        angular.forEach(this.myFile, function (value) {
              filenames.push(value.name)
        })
        this.filenames = filenames
        this.$state.go(this.$state.current, {}, { alerts: 'test' })

        if (isValid) {            
            let Policies = this.API.service('policies', this.API.all('policies'))
            let $state = this.$state
            Policies.post({
                'first_name': this.first_name,
                'last_name': this.last_name,
                'identity_document': this.dni,
                'email': this.email,
                'mobile': this.mobile,
                'phone': this.phone,
                'agent_id': this.seller_id,
                'plan_id': this.plan_type_id,
                'have_prev_safe': this.showhidepregnant,
                'company_name': this.company_name,
                'id_doc_types' : this.doc_typeselected,
                'descriptions' : this.description,
                'filenames' : this.filenames
            }).then(function () {
                    swal({
                        title: 'Póliza creada correctamente!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                        $state.go($state.current, { alerts: alert,succes: 1})
                    })                    
                }, function (response) {
                        swal({
                            title: 'Error al guardar la póliza: ' + response.data.message,
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {})
                    })
                    this.uploadFile()
                    console.log(this.alerts)
        }
        else {
            this.formSubmitted = true
        }
    }

    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
         this.lines.push({id : newid});

        //this.doc_typeselected.push(this.doc_typeselected.length+1);
        //this.description.push(this.description.length+1);
        //this.files.push({id : this.files.length+1});

        //this.newLine = null;
        console.log('lineas',this.lines)
        console.log('seleccionados',this.doc_typeselected)
        console.log('descripciones',this.description)
        console.log('archivos',this.files)

    }


    deleteLine(index){
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines





        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.files.splice(pos,1)

        console.log('lineas',this.lines)
        console.log('seleccionados',this.doc_typeselected)
        console.log('descripciones',this.description)
        console.log('archivos',this.files)
    }

    mostrar(){
     console.log('lineas',this.lines)
        console.log('seleccionados',this.doc_typeselected)
        console.log('descripciones',this.description)
        console.log('archivos',this.files)
    }
    $onInit(){
    }
}

export const PolicyAddComponent = {
    templateUrl: './views/app/components/policy-add/policy-add.component.html',
    controller: PolicyAddController,
    controllerAs: 'vm',
    bindings: {}
}
