class AssistentListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
        this.API = API
        this.$state = $state
        this.can = AclService.can


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let agentId = $stateParams.agentId

        let AgentData = API.service('show', API.all('agents'))
        AgentData.one(agentId).get()
          .then((response) => {
            this.agenteditdata = API.copy(response)
            console.log(this.agenteditdata)
            let agentResponse = response.plain(response)
        
      })

        let Assistent = API.service('assistent', API.all('assistents'))
        Assistent.one(agentId).get()
          .then((response) => {

            let dataSet = response.plain()
            console.log("data", dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()


            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('first_name').withTitle('Nombres'),
              DTColumnBuilder.newColumn('last_name').withTitle('Apellidos'),
              DTColumnBuilder.newColumn('identity_document').withTitle('N° de Identificación'),
              DTColumnBuilder.newColumn('mobile').withTitle('Móvil'),              
              DTColumnBuilder.newColumn('phone').withTitle('Teléfono'),              
              DTColumnBuilder.newColumn('email').withTitle('E-mail'),
              //DTColumnBuilder.newColumn('address').withTitle('Dirección'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        console.log("Id de Agente", agentId)
        let actionsHtml = (data) =>  {
          return`<ul>
                    <a ng-show="vm.can('manage.sales')" title="Editar" class="btn btn-xs btn-warning" ui-sref="app.assistent-edit({assistentId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button ng-show="vm.can('manage.sales')" title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }// constructor
     delete (assistentId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Asistente!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('assistents').one('assistent', assistentId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const AssistentListsComponent = {
    templateUrl: './views/app/components/assistent-lists/assistent-lists.component.html',
    controller: AssistentListsController,
    controllerAs: 'vm',
    bindings: {}
}
