class ClaimDocTypeDescriptionsListsController{
   constructor($scope, API, $state,DTOptionsBuilder,$compile, DTColumnBuilder, $stateParams){
         'ngInject';
             this.API = API
        this.$state = $state
        this.alerts=[]


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let claimdoctypeId = $stateParams.claimdoctypeId
        //let claimdoctypedescId = $stateParams.claimdoctypedescId
        console.log("claimdoctypeId",claimdoctypeId)

        let ClaimDocumentTypes = API.service('show', API.all('claimdocumenttypes'))
        
        ClaimDocumentTypes.one(claimdoctypeId).get()
          .then((response) => {
            this.claimdoctypeditdata = API.copy(response)
            let ClaimDocTypesresponse= response.plain(response)
            console.log("claimdoctypeditdata",this.claimdoctypeditdata)
        })

        let claimdoctypedesc = API.service('claimdocumenttypedescriptions', API.all('claimdocumenttypedescriptions'))
        
        claimdoctypedesc.one(claimdoctypeId).get()
          .then((response) => {
            let dataSet = response.plain()
            console.log("dataset",dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('description').withTitle('Nombre'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
        })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        

        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.claim-doc-type-descriptions-edit({claimdoctypeId: ${claimdoctypeId},claimdoctypedescId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }


    }

    delete (claimdocumenttypeId) {
      let API = this.API
      let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la el Tipo de Documento!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('claimdocumenttypedescriptions').one('claimdocumenttypedescriptions', claimdocumenttypeId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const ClaimDocTypeDescriptionsListsComponent = {
    templateUrl: './views/app/components/claim-doc-type-descriptions-lists/claim-doc-type-descriptions-lists.component.html',
    controller: ClaimDocTypeDescriptionsListsController,
    controllerAs: 'vm',
    bindings: {}
}
