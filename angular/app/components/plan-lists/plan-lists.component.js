class PlanListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';

        this.API = API
        this.$state = $state
        var vm = this

        let branchId = $stateParams.branchId
        let companyId = $stateParams.companyId
        this.branchId = branchId
        this.companyId =  companyId
        console.log("companyId", companyId)

        let Company = API.service('show', API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()
        })

        let Branch = API.service('show', API.all('branchs'))
        Branch.one(branchId).get()
          .then((response) => {
            this.branchdata = response.plain()
        })  

        let Plans = this.API.service('plans', API.all('plans'))
        Plans.getList({branchId})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn('commission_percentage').withTitle('% Comisón').renderWith(dataPercen),
              DTColumnBuilder.newColumn('max_dependent_age').withTitle('Edad Máxima de Dependientes').renderWith(dataAge),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
         let dataPercen = (data) => {
          return `
                    <div >
                      ${data} %
                    </div>
                  `
        }
        let dataAge= (data) => {
          return `
                    <div >
                      ${data} años
                    </div>
                  `
        }
        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Agregar Deducible" class="btn btn-xs btn-info" ui-sref="app.deductible-lists({planId: ${data.id}, branchId: ${this.branchId}, companyId: ${this.companyId}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    &nbsp
                    <a  title="Agregar Cobertura" class="btn btn-xs btn-success" ui-sref="app.coverage-lists({planId: ${data.id}, branchId: ${this.branchId}, companyId: ${this.companyId}})">
                        <i class="fa fa-check-square"></i>
                    </a>
                    &nbsp
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.plan-edit({planId: ${data.id}, branchId: ${this.branchId}, companyId: ${this.companyId}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }//constructor
    delete (planId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el plan!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('plans').one('plan', planId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const PlanListsComponent = {
    templateUrl: './views/app/components/plan-lists/plan-lists.component.html',
    controller: PlanListsController,
    controllerAs: 'vm',
    bindings: {}
}
