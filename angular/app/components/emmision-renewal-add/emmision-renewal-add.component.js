class EmmisionRenewalAddController{
    constructor($scope, $state, API, $stateParams){
        'ngInject';
        var vm = this
        this.current_step = '1'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        var country_id = ""
        var province_id = ""
       // this.uisrefparameter = '0'
        this.uisrefback = 'app.emission-renewal-list'
        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.uisrefparameter = ' {policyId: vm.policyId }'

        this.applicant_data = []
        this.applicant_data.comments = []
        this.applicant_id= policyId
        
        //console.log("console.log(this.policyId)", this.applicanteditdata.data) 
        
        if(this.policyId == 0){
            //this.applicanteditdata.data[0].push({id:0, first_name:"", last_name:"", identity_document:"",email:"",birthday:"",place_of_birth:"",address:"",mobile:"", phone:"",country_id:0,gender:0,province_id:0,civil_status:"",city_id:0,height:0,height_unit_measurement:0,weight:0,weight_unit_measurement:0})
             this.first_name = ""
             this.last_name = ""
             this.identity_document = ""
             this.email = ""
             this.birthday = ""
             this.place_of_birth = ""
             this.address = ""
             this.mobile = ""
             this.phone = ""
             this.country_id = ""
             this.gender = ""
             this.province_id = ""
             this.civil_status = ""
             this.city_id = ""
             this.occupation = ""
             this.height = 0
             this.height_unit_measurement = false
             this.weight = 0
             this.weight_unit_measurement = false
             this.completed = 0
                
        }else{
                console.log("entro aqui")
                let ApplicantData = API.service('show', API.all('applicants'))
                ApplicantData.one(policyId).get()
                  .then((response) => {
                    this.applicanteditdata = API.copy(response)
                    console.log("this.applicanteditdata",this.applicanteditdata)
                    this.applicanteditdata.data[0].height = parseFloat(this.applicanteditdata.data[0].height, 2)
                    this.height = this.applicanteditdata.data[0].height
                    this.applicanteditdata.data[0].weight = parseFloat(this.applicanteditdata.data[0].weight, 2)
                    this.weight = this.applicanteditdata.data[0].weight
                    this.applicanteditdata.data[0].mobile = parseInt(this.applicanteditdata.data[0].mobile)
                    this.mobile = this.applicanteditdata.data[0].mobile
                    this.applicanteditdata.data[0].phone = parseInt(this.applicanteditdata.data[0].phone)
                    this.phone = this.applicanteditdata.data[0].phone 
                    //console.log("birthday ", this.applicanteditdata.data[0].birthday)
                    if (this.applicanteditdata.data[0].birthday == null) {
                        this.applicanteditdata.data[0].birthday =  undefined;//new Date(this.applicanteditdata.data[0].birthday)
                    }else{
                        this.applicanteditdata.data[0].birthday =  new Date(this.applicanteditdata.data[0].birthday+' ')
                    }
                    //console.log("birthday ", this.applicanteditdata.data[0].birthday)
                    country_id = this.applicanteditdata.data[0].country_id
                    this.country_id = country_id
                    province_id = this.applicanteditdata.data[0].province_id
                    this.province_id =  province_id

                    if(this.applicanteditdata.data[0].height_unit_measurement == '1'){
                        this.applicanteditdata.data[0].height_unit_measurement = true
                        this.height_unit_measurement = this.applicanteditdata.data[0].height_unit_measurement
                    }else{
                        this.applicanteditdata.data[0].height_unit_measurement = false
                        this.height_unit_measurement =  false
                    }
                    if(this.applicanteditdata.data[0].weight_unit_measurement == '1'){
                        this.applicanteditdata.data[0].weight_unit_measurement = true
                        this.weight_unit_measurement = this.applicanteditdata.data[0].weight_unit_measurement
                    }else{
                        this.applicanteditdata.data[0].weight_unit_measurement = false
                        this.weight_unit_measurement = false
                    }

                    this.first_name =  this.applicanteditdata.data[0].first_name
                    this.last_name =  this.applicanteditdata.data[0].last_name
                    this.identity_document =  this.applicanteditdata.data[0].identity_document
                    this.email =  this.applicanteditdata.data[0].email
                    this.birthday = this.applicanteditdata.data[0].birthday
                    console.log("birthday", this.birthday)
                    this.place_of_birth =  this.applicanteditdata.data[0].place_of_birth
                    this.address =  this.applicanteditdata.data[0].address
                    
                    this.gender = this.applicanteditdata.data[0].gender
                    this.civil_status =  this.applicanteditdata.data[0].civil_status
                    this.city_id =  this.applicanteditdata.data[0].city_id
                    this.occupation =  this.applicanteditdata.data[0].occupation
                    this.height =  this.applicanteditdata.data[0].height
             
                    this.weight =  this.applicanteditdata.data[0].weight
                    //this.completed = 1
             

             


        
                   }) 


        }

        let States = API.service('index', API.all('states'))
        States.one().get()
          .then((response) => {
            let systemStates = []
            let allStates = []
            let stateResponse = response.plain()
            angular.forEach(stateResponse.data.states, function (value) {
              if(value.country_id == country_id){
                systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
              }
              allStates.push({id: value.id, name: value.name, country_id: value.country_id})
            })
            this.systemStates = systemStates
            this.allStates = allStates
            console.log("systemStates==> ", systemStates)
            console.log("allStates==> ", allStates)


            let Cities = API.service('index', API.all('cities'))
            Cities.one().get()
              .then((response) => {
                let systemCities = []
                let allCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                  if(value.province_id == province_id){
                    systemCities.push({id: value.id, name: value.name, province_id: value.province_id})
                  }
                  allCities.push({id: value.id, name: value.name, province_id: value.province_id})
                })
                this.systemCities = systemCities
                this.allCities = allCities
                //console.log("systemCities==> ", systemCities)
                //console.log("allCities==> ", allCities)

                
            })

        })
      

         


        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
                
                    systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefemisionren, order: value.order})

                
            })

            this.systemWizar = systemWizar
            console.log("systemWizar==> ", this.systemWizar)
        })
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            //console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            //console.log("systemCountrys==> ", systemCountrys)
        })
        //
         let applicant_data = API.one('applicant_data', this.applicant_id)
            applicant_data.get().then((response) => {
            this.applicant_data = response.plain().data
            console.log("applicant_data", this.applicant_data)
            })
    }

     getState(){
        //console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let States = API.service('index', API.all('states'))
        States.one(vm.applicanteditdata.data[0].country_id).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                 console.log(stateResponse)
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
            this.systemStates = systemStates
            //console.log("systemStates  ==> ", systemStates)          

        })*/
        this.rowStatus = false;
       // console.log("allStates ", this.allStates);
        //console.log(this.systemCanton);
        var country_id = this.country_id
        let systemStates = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allStates, function (value) {
          if(value.country_id == country_id){
            systemStates.push({id: value.id, name: value.name})
          }
        })
        if (country_id !== ""){
          this.rowStatus = true;
        }    
        this.systemStates = systemStates
        this.systemCities = ""
        console.log("systemStates ", systemStates)
    }

    getCity(){
        //console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let Citys = API.service('index', API.all('cities'))
        Citys.one(vm.applicanteditdata.data[0].province_id).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                 console.log(cityResponse)
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
            this.systemCities = systemCities
            //console.log("systemCities  ==> ", systemCities)          

        })*/

        this.rowStatus = false;
        console.log("allCities ", this.allCities);
        //console.log(this.systemCanton);
        var province_id = this.province_id
        let systemCities = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allCities, function (value) {
          if(value.province_id == province_id){
            systemCities.push({id: value.id, name: value.name})
          }
        })
        if (province_id !== ""){
          this.rowStatus = true;
        }    
        this.systemCities = systemCities
        //this.systemCities = ""
        console.log("systemCities ", systemCities)

    }

    save (isValid) {
        if(this.height_unit_measurement == true){
            this.height_unit_measurement = '1'
        }else{
            this.height_unit_measurement = '0'
        }
        if(this.weight_unit_measurement == true){
            this.weight_unit_measurement = '1'
        }else{
            this.weight_unit_measurement = '0'
        }
        //console.log("height_unit_measurement==> ", this.applicanteditdata.data[0].height_unit_measurement)
        //console.log("weight_unit_measurement==> ", this.applicanteditdata.data[0].weight_unit_measurement)
        //console.log('xxx', this.applicanteditdata.data[0])
        if (isValid) {
            this.isDisabled = true;
            let Policies = this.API.service('policies2', this.API.all('renewals'))
            let $state = this.$state
            Policies.post({
                'policyId': this.policyId,
                'first_name': this.first_name,
                'last_name': this.last_name,
                'identity_document': this.identity_document,
                'email': this.email,
                'birthday': this.birthday,
                'mobile': this.mobile,
                'phone': this.phone,
                'address':this.address,
                'place_of_birth': this.place_of_birth,
                'country_id': this.country_id,
                'province_id': this.province_id,
                'city_id': this.city_id,
                'gender':this.gender,
                'civil_status': this.civil_status,
                'occupation': this.occupation,
                'height': this.height,
                'height_unit_measurement': this.height_unit_measurement,
                'weight': this.weight,
                'weight_unit_measurement': this.weight_unit_measurement,

            })
              .then((response) => {
                //console.log("ESTADO", $state)
                swal({
                  title: 'Datos del Solicitante Guardados Correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                }, function () {
                      $state.go('app.emission-renewal-plan',{"policyId": response.data})
                })
            }, (response) => {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                      $state.go($state.current)
                  })
              })
        } else {
          this.formSubmitted = true
        }
    }
     addComment(){
         console.log("entro en addComment")
                   
        if (this.comment != ''){
            var vm = this
            let comment = this.API.service('comments', this.API.all('applicants'))
            comment.post({
                'module' : 0,
                'key_field' : this.applicant_id,
                'text' : this.comment
            }).then(function () {
                vm.comment = ''
                vm.update()
          }, function (response) {
          })
        }
    }
    deleteComment(commentId) {
        this.API.one('comments').one('comments', commentId).remove()
        .then(() => {
            this.update()
        })
    }
    update(){
        let comments = this.API.service('show', this.API.all('comments'))
        comments.one(this.applicant_id).get()
        .then((response) => {
            this.applicant_data.comments = this.API.copy(response).data
        })
    }

    $onInit(){
    }
}

export const EmmisionRenewalAddComponent = {
    templateUrl: './views/app/components/emmision-renewal-add/emmision-renewal-add.component.html',
    controller: EmmisionRenewalAddController,
    controllerAs: 'vm',
    bindings: {}
}
