class EmissionCoverageStartController{
    constructor(API,$stateParams, $state){
        'ngInject';
        var vm = this
        this.$state = $state
        this.API = API
        this.current_step = '6'

        let policyId = $stateParams.policyId
        this.policyId = policyId

        let Wizar = API.service('wizar')
        Wizar.getList().then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            angular.forEach(wizarResponse, function (value) {
                systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})
            })
            this.systemWizar = systemWizar
        })

        let datescoverages = API.service('dates',API.all('policies'));
        datescoverages.getList().then((response) => {
            let dataSet = response.plain()
            this.dates = dataSet
        })

        let data = {};
        data.policy_id=   policyId;
        let Policy = API.service('getPolicyInfo');
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.dateselected = dataSet[0].start_date_coverage != '01-01-1970' ? dataSet[0].start_date_coverage : this.dates[0];
            this.showhidepregnant = dataSet[0].have_prev_safe == '1' ? true : false
            this.showhidecontinue = dataSet[0].continuity_coverage == '1' ? true : false
            this.company_name = dataSet[0].company_name
            this.plan_name = dataSet[0].plan_name
            this.is_international = dataSet[0].is_international == '1' ? true : false
        })
    }
    checkEvent(){

    }

    save (isValid) {
        if (isValid) {
            this.isDisabled = true;      
            let $state = this.$state
            let company_name = this.showhidepregnant == true ? this.company_name : 'x'
            let plan_name = this.showhidepregnant == true ? this.plan_name : 'x'
            let datecoverage = this.API.service('coverage', this.API.all('policies'))
            datecoverage.post({
                'policy_id' : this.policyId,
                'start_date_coverage': this.dateselected,
                'continuity_coverage' : this.showhidecontinue,
                'is_international' : this.is_international,
                'have_prev_safe' : this.showhidepregnant,
                'company_name' : company_name,
                'plan_name' : plan_name
            }).then(function (response) {
                swal({
                    title: 'Datos de Cobertura Guardados Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.emission-documents',{"policyId": response.data})
                })
            }, function (response) {
                swal({
                    title: 'Error ' + response.data,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current)
                })
          })
        }
    }
    $onInit(){
    }
}

export const EmissionCoverageStartComponent = {
    templateUrl: './views/app/components/emission-coverage-start/emission-coverage-start.component.html',
    controller: EmissionCoverageStartController,
    controllerAs: 'vm',
    bindings: {}
}

