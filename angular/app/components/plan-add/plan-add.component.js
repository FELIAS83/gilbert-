class PlanAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        var vm = this
        
        let branchId = $stateParams.branchId
        let companyId = $stateParams.companyId
        this.branchId = branchId
        this.companyId =  companyId
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
         let Company = API.service('show', API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()
        })

        let Branch = API.service('show', API.all('branchs'))
        Branch.one(branchId).get()
          .then((response) => {
            this.branchdata = response.plain()
            console.log(this.branchdata)
        })  

        //
    }
    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            this.processing = true
            let vm = this
          let Plans = this.API.service('plan', this.API.all('plans'))
          let $state = this.$state          
          if(this.commission_percentage == "" || this.commission_percentage == undefined || this.commission_percentage == 0){
            this.commission_percentage = parseFloat(this.branchdata.data.commission_percentage,2)
          }
          Plans.post({
            'name': this.name,
            'branchId': this.branchId,
            'commission_percentage': this.commission_percentage,
            'max_age': this.max_age
          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el plan.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const PlanAddComponent = {
    templateUrl: './views/app/components/plan-add/plan-add.component.html',
    controller: PlanAddController,
    controllerAs: 'vm',
    bindings: {}
}
