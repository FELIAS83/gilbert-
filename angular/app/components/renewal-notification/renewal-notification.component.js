class RenewalNotificationController{
    constructor($scope, $state, $compile, API, $stateParams, $http, $uibModal){ 
        'ngInject';
        var vm = this
        //this.max_dependent_age=0
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.$http= $http
        //this.uploadFile = uploadFile
        var relationship = ""
        var orderRole = 0
        this.client = ""
        this.role = ""
        this.directory = 'renewal_notification/'
        this.file = []
        this.files2 = []
        this.alerts = []
        this.form = {}
        this.value = []
        this.methods = []
        this.account_types = []
        this.card_types = []
        this.card_marks = []
        this.payment_types = []
        this.lines = []
        this.fees=[]
        this.details = []
        this.fees = []
        var delete_dependent_array= []
        var change_dependent_array= []
        this.planDeduc = ""
        this.content=""
        this.birthdayaffiliate = []
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        this.doc_typeselected = []
        this.deletedlines = []
        this.descriptions = []
        this.myFile = []
        this.files_type = []
        this.pfiles=[]
        this.dependents=[]
        this.lines.push({id : 0});
        this.payment_date=""
        this.warning=false
        this.message0=""
        this.message1=""
        this.message2=""
        this.message3=""
        this.message4=""
        this.message5=""
        this.standar = false
        this.delete_dependent = false
        this.dependent_change = false
        this.dependentH_change = false
        this.titular_change= false
        this.both_change = false 
        this.agetitular=""
        this.rangetitular=0
        this.new_rangetitular=0
        this.fee_new_value_spouse=0
        this.fee_new_value_titular=0
        this.fee_new_value_both=0
        this.fee_diference=0
        this.rest_titular=0
        this.rest_both=0
        this.rest_spouse=0
        this.age_h=0

        let policyId = $stateParams.policyId
        let feeId = $stateParams.feeId
        let fee_number = $stateParams.fee_number
        this.policyId = policyId
        this.feeId = feeId
        this.fee_number = fee_number
        console.log("id police aqui", vm.policyId)
        console.log("fee_number", fee_number)

        let emailformatId = 14
        //console.log(companyId)
        let EmailFormat = API.service('show', API.all('email_formats'))
        EmailFormat.one(emailformatId).get()
        .then((response) => {
          this.emailformatdata = API.copy(response)
          //console.log("emailformatdata", JSON.stringify(this.emailformatdata))
        })

        let PolicyInfo = API.service('show', API.all('policies'))
        PolicyInfo.one(this.policyId).get()
        .then((response) => {
            this.PolicyInfo = API.copy(response)            
            let Agents= API.service('show', API.all('agents'))
            Agents.one(this.PolicyInfo.data[0].agent_id).get()
            .then((response) => {
                this.AgentsData = API.copy(response)
                this.recipient = this.AgentsData.data.email
            })
        })

        let Agents= API.service('show', API.all('agents'))
        Agents.one(this.policyId).get()
        .then((response) => {
            this.AgentsData = API.copy(response)
            console.log("AgentsData", this.AgentsData)
            console.log("Agentsmail", this.AgentsData.data.email)
            this.recipient = this.AgentsData.data.email

        }) 

        let Policy = this.API.service('getPolicyInfoPay');

        let data = {};
        data.policy_id=   $stateParams.policyId;
        data.fee_id = $stateParams.feeId;
        console.log("Id de la cuota", data.fee_id)





        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            //console.log('dataSet',dataSet)
            this.policyinfo = dataSet
            this.form = this.policyinfo[0]
            this.client = this.form.first_name+" "+this.form.last_name
            this.planDeduc = this.form.plan+" / "+this.form.deductible
            this.total_value = '$'+this.form.total_value
            this.form.payment_date = new Date( this.form.payment_date+' ')
            vm.subject= '['+this.policyinfo[0].number+'] - '+this.client
            console.log("deductible_id",this.form.deductible_id)


           if (this.form.mode == 0){
            this.payfrecuency = 'Anual'
            }
            else if (this.form.mode == 1){
                this.payfrecuency = 'Semestral'
            }
            else if (this.form.mode == 2){
                this.payfrecuency = 'Trimestral'
            }
            else if (this.form.mode == 3){
                this.payfrecuency = 'Mensual'
            }
            this.first_value = parseFloat(this.form.first_value, 2)

        let Plan =this.API.service('show', API.all ('plans'))
        Plan.one(this.policyinfo[0].plan_id).get().then((response) => {
            let PlanData = response.plain()
            //console.log("planData", PlanData.data)
            //console.log("max_dependent_age",PlanData.data.max_dependent_age )
            this.max_dependent_age = PlanData.data.max_dependent_age     
            console.log("this.max_dependent_age" ,this.max_dependent_age)


        this.role =  "Titular"
        let DependentData = API.service('dependents', API.all('dependents'))
        DependentData.one(policyId).get()
          .then((response) => {
            this.DependentData = API.copy(response)

            //console.log("dependent ", this.DependentData.data)
                //let dependentsResponse = response.plain()
                let dependentq = 0
                vm.dependentq = dependentq
                let spouse = 0
                vm.spouse = spouse
                let ageh = 0
                vm.ageh = ageh
                let inclusion_date = ""

                //console.log(this.policyinfo[0].effective_date)
                angular.forEach(this.DependentData.data, function (value) {
                    if(value.relationship == "H"){
                        relationship = "Cónyuge"
                        orderRole = 2
                        vm.ageh = vm.calculateAge(value.birthday)
                        vm.spouse = 1
                    }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        vm.dependentq++
                    }

                    console.log("vm.dependentq",vm.dependentq)
                    inclusion_date = value.inclusion_date
                    //value.birthday = new Date(value.birthday)
                    
                    let age = vm.calculateAge(value.birthday)
                    systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, relationship: relationship, inclusion_Date: inclusion_date})
                    allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, role: value.relationship, order: orderRole, relationship: relationship, age: age, effective_Date: vm.policyinfo[0].effective_date, inclusion_Date: inclusion_date })
                  
                })
                    this.agetitular = this.calculateAge(this.policyinfo[0].birthday)
                    allAffiliates.push({id: parseInt(this.policyinfo[0].id), name: this.client, role: "T", birthday: this.policyinfo[0].birthday ,order: 1, relationship: 'Titular' , age: this.agetitular, effective_Date: this.policyinfo[0].effective_date, inclusion_Date: this.policyinfo[0].inclusion_date})
                    allAffiliates.sort(function (a, b) {
                        if (a.order > b.order) {
                            return 1;
                        }
                        if (a.order < b.order) {
                            return -1;
                        }
                        return 0;
                    });

                    systemDependents.sort(function (a, b){
                      return (b.relationship - a.relationship)
                })
                this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                console.log("this.allAffiliates",this.allAffiliates)
                console.log("this.systemDependents",this.systemDependents)


                    if (this.policyinfo[0].mode == 0){
                    this.payment_type = 'Anual'
                    this.amount_fees = 1
                    this.pay_months=12
                    }
                    else if (this.policyinfo[0].mode == 1){
                        this.payment_type = 'Semestral'
                        this.amount_fees = 2
                        this.pay_months=6
                    }
                    else if (this.policyinfo[0].mode == 2){
                        this.payment_type = 'Trimestral'
                        this.amount_fees = 4
                        this.pay_months=3
                    }
                    else if (this.policyinfo[0].mode == 3){
                        this.payment_type = 'Mensual'
                        this.amount_fees = 12
                        this.pay_months=1
                    }

                     if (isNaN(this.ageh)){
                            this.ageh = 0
                        }
             
        let Deductible = API.service('details', API.all('deductible_details'))
        Deductible.one(this.form.deductible_id).get()
          .then((response) => {

            let dataSet = response.plain()
            this.deductible=dataSet.data
            console.log("deductible_details",this.deductible)

            let today = new Date();
            let birthday = new Date(this.policyinfo[0].birthday)
           
            let m = today.getMonth() - birthday.getMonth();
            this.fee_payment_date= new Date(this.form.fee_payment_date);
            let month= 1+today.getMonth();
            let birthday_month= birthday.getMonth()+1

            
            let fee_paymment_month = this.fee_payment_date.getMonth()+1
            
            let mfp = fee_paymment_month - this.pay_months 
            
            let fee_payment_day = this.fee_payment_date.getDate()+1
            
            let birthday_day= birthday.getDate()+1
            
            let mfb = fee_paymment_month - birthday_month
            
            let dfb = fee_payment_day - birthday_day 
            

            this.intro ="Estimados, a continuación se encuentra el detalle de su próxima renovación:"  
            this.emailformatdata.data.content =this.intro

          /////////////////////chequeo de Dependientes///////////////

            for (var i = 0; i < this.DependentData.data.length; i++){
                
                if(this.DependentData.data[i].relationship == "D")
                {
                    let today = new Date();
                    let birthday = new Date(this.DependentData.data[i].birthday)
                    this.fee_payment_date= new Date(this.form.fee_payment_date);
                    this.ageD = this.calculateAge(this.DependentData.data[i].birthday)
                    let month= 1+today.getMonth();
                    let birthday_month= birthday.getMonth()+1
                    
                    let fee_paymment_month = this.fee_payment_date.getMonth()+1
                    let mfp = fee_paymment_month - this.pay_months 
                    
                    let fee_payment_day = this.fee_payment_date.getDate()+1
                    
                    let birthday_day= birthday.getDate()+1
                    
                    let mfb = fee_paymment_month - birthday_month
                    
                    let dfb = fee_payment_day - birthday_day 

                    let result =  birthday.getDate()-this.fee_payment_date.getDate()
                    //console.log("resultado",result)

                    this.dependent_name= this.DependentData.data[i].first_name+" "+this.DependentData.data[i].last_name
                    console.log("nombre dependiente", this.dependent_name,'cumpleaños del dependiente', this.DependentData.data[i].birthday,"id "+this.DependentData.data[i].id+"de "+this.ageD+" años de edad")

                    ////////////////////CHEQUEO MAXIMA EDAD PERIMITIDA DEPENDIENTES///////////////////////////////
                    if (this.ageD >= this.max_dependent_age)
                    {
                        this.delete_dependent = true
                        
                        this.message1 ="\n\n<br><br>Dependiente Mayor a la edad máxima permitida.\n<br>El dependiente "+this.dependent_name+" tiene "+this.ageD+" años, igual o mayor a la edad permitida: max_dependent_age años."    
                        //document.getElementById("content").value += this.message0;
                        this.message1 = this.message1.replace("dependent_name",this.dependent_name)
                        this.message1 = this.message1.replace("ageD",this.ageD)
                        this.message1 = this.message1.replace("max_dependent_age",this.max_dependent_age)
                        this.emailformatdata.data.content += this.message1
                        
                        
                        delete_dependent_array.push({name:this.dependent_name, ageD:this.ageD})                        

                    }            
                    ////////////////////////CHEQUEO CUMPLE LA MAXIMA EDAD PERMITIDA POR PLAN ANTES DE LA FECHA DE PAGO DEPENDIENTES///////////////////////////////////////////////////

                    if (   (( mfb == 0 && birthday_day <= fee_payment_day ) && (this.ageD == this.max_dependent_age-1 ))  || ( (mfb > 0 && dfb <= 0 && birthday_month >= mfp ) && (this.ageD == this.max_dependent_age-1 ) )  ) 
                    {
                        
                        
                        this.dependent_change=true
                        
                        
                        this.message2 +="\n\n<br><br>Cambio de Edad.\n<br>El Dependiente " +this.dependent_name+" de "+this.ageD+" años de edad, cumplirá max_dependent_age años, antes de la proxima fecha de pago: fee_payment_date." 
                        //document.getElementById("content").value += this.message1;
                        
                        this.message2 = this.message2.replace("dependent_name",this.dependent_name)
                        this.message2 = this.message2.replace("ageD",this.ageD)
                        this.message2 = this.message2.replace("max_dependent_age",this.max_dependent_age)
                        this.message2 = this.message2.replace("fee_payment_date",this.fee_payment_date)
                        this.emailformatdata.data.content += this.message2

                        
                        change_dependent_array.push({name: this.dependent_name, ageD: this.ageD})
                        
                    }
                    //console.log('cumpleaños del dependiente', this.DependentData.data[i].birthday)
                    //console.log("id "+this.DependentData.data[i].id+", edad del dependiente"+this.DependentData.data[i].first_name+ ","+age)
                    

                }
                else if (this.DependentData.data[i].relationship == "H")
                {
                        let today = new Date();
                        let birthday = new Date(this.DependentData.data[i].birthday)
                        this.fee_payment_date= new Date(this.form.fee_payment_date);
                        this.age_h = this.calculateAge(this.DependentData.data[i].birthday)
                        let month= today.getMonth()+1
                        let birthday_month= birthday.getMonth()+1
                        let fee_paymment_month = this.fee_payment_date.getMonth()+1
                        let mfp = fee_paymment_month - this.pay_months 
                        
                        let fee_payment_day = this.fee_payment_date.getDate()+1
                        
                        let birthday_day= birthday.getDate()+1
                        
                        let mfb = fee_paymment_month - birthday_month
                        
                        let dfb = fee_payment_day - birthday_day 
                        
                        this.dependent_nameH= this.DependentData.data[i].first_name+" "+this.DependentData.data[i].last_name
                        

                        if (  ( (this.fee_number == 1) && (mfb == 0) && (birthday_day <= fee_payment_day ) ) 
                           || ( (this.fee_number == 1) && (mfb > 0) && (dfb <= 0) && (birthday_month >= mfp) )  ) 
                        {  
                            //console.log("el Cónyugue "+this.dependent_nameH+"de edad"+this.age_h+" cumple antes de la fecha de pago")
                            var h=j
                            
                            for(var j = 0; j < this.deductible.length; j++)
                            {
                                if ( parseInt(this.age_h) >= parseInt(this.deductible[j].age_start)
                                 && parseInt(this.age_h) <= parseInt(this.deductible[j].age_end) ) 
                                {
                                    //console.log("pertence a este rango de edad"+this.deductible[j].age_start+"-"+this.deductible[j].age_end+"cumpleaños"+birthday)
                                    this.rangeh=j
                                    this.rangeh_age_start=this.deductible[j].age_start
                                    this.rangeh_age_end=this.deductible[j].age_end
                                    this.amountspouse= this.deductible[j].amount
                                    
                                }                            
                                if ( parseInt(this.age_h+1) >= parseInt(this.deductible[j].age_start)
                                 && parseInt(this.age_h+1) <= parseInt(this.deductible[j].age_end) ) 
                                {
                                    //console.log("pertencerá a este rango de edad"+this.deductible[j].age_start+"-"+this.deductible[j].age_end+"cumpleaños"+birthday)
                                    this.new_rangespouse=j
                                    this.new_rangespouse_age_start=this.deductible[j].age_start
                                    this.new_rangespouse_age_end=this.deductible[j].age_end
                                   // console.log(this.new_rangespouse,"new_range")
                                   this.new_amountspouse= this.deductible[j].amount
                                }    
                            }
                            if (this.rangeh==this.new_rangespouse)
                            {
                                console.log("se mantiene en el rango")
                            }
                            else
                            {
                                
                                this.dependentH_change = true
                                
                                this.new_age_h=this.age_h+1

                                //////////////////////calculo de cambio de edad conyuge////////////

                                let amounttitular = API.all('amounts/'+this.policyinfo[0].deductible_id+'/'+this.agetitular+'/'+this.new_age_h+'/'+this.dependentq+'/'+this.policyinfo[0].coverage_id+'/'+this.policyId)
                                amounttitular.doGET('',{}).then((response) => {
                                this.amounttitular = response.plain()
                                let amounttitular = this.amounttitular.data.amounttitular
                                let amounth = this.amounttitular.data.amounth
                                let amountchild = this.amounttitular.data.amountchild
                                let coverage_name = this.amounttitular.data.coverage_name
                                let coverage_amount = this.amounttitular.data.coverage_amount
                                let taxssc_percent = this.amounttitular.data.taxssc
                                this.adminfee = this.amounttitular.data.adminfee
                                this.iva = this.amounttitular.data.iva
                                let savedfees = this.amounttitular.data.fees

                                this.total = 0
                                let datefees = new Date( vm.policyinfo[0].effective_date)
                                for (let i = 0; i < this.amount_fees; i++ ){
                                    this.fees.push({fee_id : i+1})
                                    this.fees[i].details = []
                                    this.fees[i].details.push({id: 1, detail: 'P/H Premium', value: amounttitular/this.amount_fees})
                                    if (this.spouse != 0){
                                        this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: amounth/this.amount_fees})
                                    }
                                    if (amountchild != 0){
                                        let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                                        this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: amountchild/this.amount_fees})
                                    }
                                    if (coverage_amount != 0){
                                        this.fees[i].details.push({id: this.fees[i].details.length+1, detail: coverage_name, value: coverage_amount/this.amount_fees})
                                    }

                                    let sum = 0
                                    let totaldetails = 0
                                    for (let j = 0; j < this.fees[i].details.length; j++){
                                        sum = sum + this.fees[i].details[j].value
                                        totaldetails = totaldetails + this.fees[i].details[j].value
                                    }
                                    if (this.policyinfo[0].mode == 1){
                                        sum = sum * 0.06
                                        this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                                    }
                                    if ((this.policyinfo[0].mode == 2) || (this.policyinfo[0].mode == 3) ){
                                        sum = sum * 0.1
                                        this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                                    }

                                    let totalfee = 0
                                    for (let k = 0; k < this.fees[i].details.length; k++){
                                        totalfee = totalfee + this.fees[i].details[k].value
                                    }
                                    //descuento
                                    this.fees[i].discounts = []
                                    let discount = 0
                                    if (this.policyinfo[0].discount == 1 ){
                                        discount = ((totalfee-(coverage_amount/this.amount_fees))*this.policyinfo[0].discount_percent)/100
                                        this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: discount })
                                    }
                                    //impuesto
                                    this.fees[i].taxes = []
                                    let adminfeeiva = 0

                                    if (i == 0){
                                        if (savedfees != ''){
                                            if (savedfees[i].adminfee == 1){
                                                this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: this.adminfee, check:true, checkvalue:true })
                                                this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: this.adminfee*(this.iva/100) })
                                                adminfeeiva = this.adminfee + (this.adminfee*(this.iva/100))
                                            }
                                            else{
                                                this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                                this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                            }
                                        }
                                        else{
                                            this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                            this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                        }
                                    }

                                    if (this.policyinfo[0].mode == 0) {
                                        sum = 0;
                                    }
                                    let taxssc = (totaldetails+sum-discount)*(taxssc_percent/100) //variable configuracion
                                    this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                                    this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                                    this.total = this.total + this.fees[i].total

                                   
                                    if (this.amount_fees == 1){
                                        this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                    }
                                    else if (this.amount_fees == 2){
                                        if (i == 0){
                                            this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                        }
                                        else{
                                            this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 6)
                                        }


                                    }
                                    else if (this.amount_fees == 4){
                                        if (i == 0){
                                            this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                        }
                                        else{
                                            this.fees[i].fee_payment_date =   datefees.setMonth(datefees.getMonth() + 3)
                                        }


                                    }
                                    else if (this.amount_fees == 12){
                                        if (i == 0){
                                            this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                        }
                                        else{
                                            this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 1)
                                        }

                                    }

                                }///fin del for
                                
                                this.fees[1].total=Math.round(this.fees[1].total*100)/100;
                                this.rest_spouse= Math.round((this.new_amountspouse-this.amountspouse)*100)/100
                                this.fee_new_value_spouse=this.fees[1].total
                                this.fee_diference=this.rest_spouse
                                //console.log("fees.total", this.fees[1].total)
                                

                                if (this.titular_change==false && this.dependentH_change==true)
                                {

                                this.message4 ="\n\n<br><br>Cambio de banda.\n<br>El cónyugue dependent_nameH de ageH años de edad, pertenece a la banda de edad rangeh_age_start - rangeh_age_end años, cuyo valor de couta es de: $ amountspouse y cambiara a la siguiente banda de edad new_rangespouse_age_start - new_rangespouse_age_end años, que tiene una valor de: $ new_amountspouse. El incremento es de: $ rest_spouse. El valor total de la cuota es de: $ fee_new_value_spouse."    
                      
                                this.message4 = this.message4.replace("dependent_nameH",this.dependent_nameH)
                                this.message4 = this.message4.replace("ageH",this.age_h)
                                this.message4 = this.message4.replace("rangeh_age_start",this.rangeh_age_start)
                                this.message4 = this.message4.replace("rangeh_age_end",this.rangeh_age_end)
                                this.message4 = this.message4.replace("new_rangespouse_age_start",this.new_rangespouse_age_start)
                                this.message4 = this.message4.replace("new_rangespouse_age_end",this.new_rangespouse_age_end)
                                this.message4 = this.message4.replace("amountspouse",this.amountspouse)
                                this.message4 = this.message4.replace("new_amountspouse",this.new_amountspouse)
                                this.message4 = this.message4.replace("fee_payment_date",this.fee_payment_date)
                                this.message4 = this.message4.replace("fee_new_value_spouse",this.fee_new_value_spouse)
                                this.message4 = this.message4.replace("rest_spouse",this.rest_spouse)    
                                this.message4 = this.message4.replace("first_value",this.first_value)
                                
                                this.emailformatdata.data.content += this.message4
                                }
                               }) ///fin de amount
                            }//fin del else                                                       
                        }//fin del if
                        //console.log("edad del conyugue", this.age_h)
                        //console.log("fecha de pago", this.form.fee_payment_date)
                        //console.log("id "+this.form.id+", edad del cónyugue"+this.dependent_nameH+ ","+this.age_h+"cumpleaños"+birthday)
                        //console.log("tipo_notificacion_2H",this.notification_type_2H)
                           
                }//else
            }//fin del for        
            this.delete_dependent_array= delete_dependent_array
            this.change_dependent_array= change_dependent_array
            

    /////////////////////////////////CHEQUEO DE CAMBIO DE BANDA DE EDAD TITULAR//////////////////////                    

        for (var i = 0; i < this.policyinfo.length; i++){
       
                 if (  ( (this.fee_number == 1) && (mfb == 0) && (birthday_day <= fee_payment_day ) ) 
                || (  (this.fee_number == 1) && ( mfb > 0) && (dfb <= 0) && (birthday_month >= mfp ) ) ) 
                {
                    //console.log("el titular cumple antes de la fecha de pago")
                    var h=j
                        
                        for(var j = 0; j < this.deductible.length; j++){

                            if ( parseInt(this.agetitular) >= parseInt(this.deductible[j].age_start)
                             && parseInt(this.agetitular) <= parseInt(this.deductible[j].age_end) ) {
                                //console.log("pertence a este rango de edad"+this.deductible[j].age_start+"-"+this.deductible[j].age_end+"cumpleaños"+birthday)
                            this.rangetitular=j
                            this.rangetitular_age_start=this.deductible[j].age_start
                            this.rangetitular_age_end=this.deductible[j].age_end
                            this.amountclient= this.deductible[j].amount
                            //console.log("rango",this.rangetitular)
                            }                            

                            if ( parseInt(this.agetitular+1) >= parseInt(this.deductible[j].age_start)
                             && parseInt(this.agetitular+1) <= parseInt(this.deductible[j].age_end) ) {
                                //console.log("pertencera a este rango de edad"+this.deductible[j].age_start+"-"+this.deductible[j].age_end+"cumpleaños"+birthday)
                            
                            this.new_rangetitular=j
                            this.new_ranget_age_start=this.deductible[j].age_start
                            this.new_ranget_age_end=this.deductible[j].age_end
                            //console.log(this.new_rangetitular,"new_range")
                            this.new_agetitular=this.agetitular+1
                            this.new_amountclient= this.deductible[j].amount
                           
                            }    
                        }

                        if (this.rangetitular==this.new_rangetitular){
                              //  console.log("se mantiene en el rango")
                            }
                            else
                            {
                                //console.log("cambio de rango")
                                this.titular_change=true
                               
                                if (this.dependentH_change==true)
                                {
                                    this.new_ageh=this.age_h+1
                                }
                                else{
                                    this.new_ageh=this.age_h
                                }     

                                ////////////////////////////////calculo de couta titular y/o conyuge/////
                                let amounttitular = API.all('amounts/'+this.policyinfo[0].deductible_id+'/'+this.new_agetitular+'/'+this.new_ageh+'/'+this.dependentq+'/'+this.policyinfo[0].coverage_id+'/'+this.policyId)
                                    amounttitular.doGET('',{}).then((response) => {
                                        this.amounttitular = response.plain()
                                        
                                        let amounttitular = this.amounttitular.data.amounttitular
                                        let amounth = this.amounttitular.data.amounth
                                        let amountchild = this.amounttitular.data.amountchild
                                        let coverage_name = this.amounttitular.data.coverage_name
                                        let coverage_amount = this.amounttitular.data.coverage_amount
                                        let taxssc_percent = this.amounttitular.data.taxssc
                                        this.adminfee = this.amounttitular.data.adminfee
                                        this.iva = this.amounttitular.data.iva
                                        let savedfees = this.amounttitular.data.fees

                                        this.total = 0
                                        let datefees = new Date( vm.policyinfo[0].effective_date)
                                        for (let i = 0; i < this.amount_fees; i++ ){
                                            this.fees.push({fee_id : i+1})
                                            this.fees[i].details = []
                                            this.fees[i].details.push({id: 1, detail: 'P/H Premium', value: amounttitular/this.amount_fees})
                                            if (this.spouse != 0){
                                                this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: amounth/this.amount_fees})
                                            }
                                            if (amountchild != 0){
                                                let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                                                this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: amountchild/this.amount_fees})
                                            }
                                            if (coverage_amount != 0){
                                                this.fees[i].details.push({id: this.fees[i].details.length+1, detail: coverage_name, value: coverage_amount/this.amount_fees})
                                            }

                                            let sum = 0
                                            let totaldetails = 0
                                            for (let j = 0; j < this.fees[i].details.length; j++){
                                                sum = sum + this.fees[i].details[j].value
                                                totaldetails = totaldetails + this.fees[i].details[j].value
                                            }
                                            if (this.policyinfo[0].mode == 1){
                                                sum = sum * 0.06
                                                this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                                            }
                                            if ((this.policyinfo[0].mode == 2) || (this.policyinfo[0].mode == 3) ){
                                                sum = sum * 0.1
                                                this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                                            }

                                            let totalfee = 0
                                            for (let k = 0; k < this.fees[i].details.length; k++){
                                                totalfee = totalfee + this.fees[i].details[k].value
                                            }
                                            //descuento
                                            this.fees[i].discounts = []
                                            let discount = 0
                                            if (this.policyinfo[0].discount == 1 ){
                                                discount = ((totalfee-(coverage_amount/this.amount_fees))*this.policyinfo[0].discount_percent)/100
                                                this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: discount })
                                            }
                                            //impuesto
                                            this.fees[i].taxes = []
                                            let adminfeeiva = 0

                                            if (i == 0){
                                                if (savedfees != ''){
                                                    if (savedfees[i].adminfee == 1){
                                                        this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: this.adminfee, check:true, checkvalue:true })
                                                        this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: this.adminfee*(this.iva/100) })
                                                        adminfeeiva = this.adminfee + (this.adminfee*(this.iva/100))
                                                    }
                                                    else{
                                                        this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                                        this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                                    }
                                                }
                                                else{
                                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                                }
                                            }


                                            if (this.policyinfo[0].mode == 0) {
                                                sum = 0;
                                            }
                                            let taxssc = (totaldetails+sum-discount)*(taxssc_percent/100) //variable configuracion
                                            this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                                            this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                                            this.total = this.total + this.fees[i].total

                                            if (this.amount_fees == 1){
                                                this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                            }
                                            else if (this.amount_fees == 2){
                                                if (i == 0){
                                                    this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                                }
                                                else{
                                                    this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 6)
                                                }


                                            }
                                            else if (this.amount_fees == 4){
                                                if (i == 0){
                                                    this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                                }
                                                else{
                                                    this.fees[i].fee_payment_date =   datefees.setMonth(datefees.getMonth() + 3)
                                                }


                                            }
                                            else if (this.amount_fees == 12){
                                                if (i == 0){
                                                    this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                                                }
                                                else{
                                                    this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 1)
                                                }

                                            }

                                        }///fin del for

                                //console.log("rango",this.rangetitular)
                                //console.log("nuevo rango", this.new_rangetitular)
                                
                                
                                //console.log("diferencia", this.rest)
                                if (this.dependentH_change==true && this.titular_change==true){
                                    
                                    this.both_change=true
                                    this.fees[1].total=Math.round(this.fees[i].total*100)/100;
                                    this.totalboth=this.total
                                    console.log(this.totalclient)
                                    console.log("fees.total", this.fees[1].total)
                                    this.rest_both= Math.round( ( (this.new_amountclient+this.new_amountspouse)-(this.amountclient+this.amountspouse) )*100)/100;
                                    this.fee_new_value_both=this.fees[1].total
                                    this.fee_diference=this.rest_both

                                    this.message5 ='\n\n<br><br>Cambio de banda de Titular y Cónyuge.\n<br>El titular de la póliza tiene agetitular años de edad y pertenece a la banda de edad de rangetitular_age_start - rangetitular_age_end años, cuya cuota tiene un valor de $ amountclient y cambiará a la siguiente banda de edad new_ranget_age_start - new_ranget_age_end años. que tiene un valor de: $ new_amountclient.\n><br>El cónyugue dependent_nameH de ageH años de edad, pertenece a la banda de edad rangeh_age_start - rangeh_age_end años que tiene un valor de: $ amountspouse y cambiará a la siguiente banda de edad new_rangespouse_age_start - new_rangespouse_age_end años, que tiene un valor de : $ new_amountspouse. \n<br> El incremento es de: $ rest_both. El valor total de la cuota es: $ fee_new_value_both.'

                                    this.message5 = this.message5.replace("dependent_nameH",this.dependent_nameH)
                                    this.message5 = this.message5.replace("agetitular",this.agetitular)
                                    this.message5 = this.message5.replace("ageH",this.age_h)
                                    this.message5 = this.message5.replace("rangetitular_age_start",this.rangetitular_age_start)
                                    this.message5 = this.message5.replace("rangetitular_age_end",this.rangetitular_age_end)
                                    this.message5 = this.message5.replace("new_ranget_age_start",this.new_ranget_age_start)
                                    this.message5 = this.message5.replace("new_ranget_age_end",this.new_ranget_age_end)

                                    this.message5 = this.message5.replace("rangeh_age_start",this.rangeh_age_start)
                                    this.message5 = this.message5.replace("rangeh_age_end",this.rangeh_age_end)
                                    this.message5 = this.message5.replace("new_rangespouse_age_start",this.new_rangespouse_age_start)
                                    this.message5 = this.message5.replace("new_rangespouse_age_end",this.new_rangespouse_age_end)
                                    this.message5 = this.message5.replace("amountclient",this.amountclient)
                                    this.message5 = this.message5.replace("new_amountclient",this.new_amountclient)
                                    this.message5 = this.message5.replace("amountspouse",this.amountspouse)
                                    this.message5 = this.message5.replace("new_amountspouse",this.new_amountspouse)

                                    this.message5 = this.message5.replace("fee_payment_date",this.fee_payment_date)
                                    this.message5 = this.message5.replace("fee_new_value_both",this.fee_new_value_both)
                                    this.message5 = this.message5.replace("rest_both",this.rest_both)    
                                    this.message5 = this.message5.replace("first_value",this.first_value)

                                    
                                    //document.getElementById("content").value += this.message2;
                                    this.emailformatdata.data.content += this.message5 
                                    

                                }
                                else if (this.dependentH_change==false && this.titular_change==true) {
                                this.fees[1].total=Math.round(this.fees[1].total*100)/100;
                                console.log("fees.total", this.fees[1].total)
                                this.totalclient=this.total
                                console.log("totalclient",this.totalclient)
                                this.rest_titular=Math.round((this.new_amountclient-this.amountclient)*100)/100;
                                this.fee_new_value_titular=this.fees[1].total
                                
                                

                                this.message3 ="\n\n<br><br>Cambio de banda.\n<br>El Titular de la póliza tiene agetitular años de edad y pertenece a la banda de edad de rangetitular_age_start - rangetitular_age_end años, cuya cuota tiene un valor de $ amountclient  y cambiará a la siguiente banda de edad new_ranget_age_start - new_ranget_age_end años, que tiene un valor de $ new_amountclient. El incremento es de: $ rest_titular. El Valor total de la couta es: $ fee_new_value_titular ."

                                this.message3 = this.message3.replace("agetitular",this.agetitular)
                                this.message3 = this.message3.replace("rangetitular_age_start",this.rangetitular_age_start)
                                this.message3 = this.message3.replace("rangetitular_age_end",this.rangetitular_age_end)
                                this.message3 = this.message3.replace("new_ranget_age_start",this.new_ranget_age_start)
                                this.message3 = this.message3.replace("new_ranget_age_end",this.new_ranget_age_end)
                                this.message3 = this.message3.replace("amountclient",this.amountclient)
                                this.message3 = this.message3.replace("new_amountclient",this.new_amountclient)
                                this.message3 = this.message3.replace("fee_payment_date",this.fee_payment_date)
                                this.message3 = this.message3.replace("fee_new_value_titular",this.fee_new_value_titular)
                                this.message3 = this.message3.replace("rest_titular",this.rest_titular)    
                                this.message3 = this.message3.replace("first_value",this.first_value)
                                                                        
                                //document.getElementById("content").value += this.message2;
                                this.emailformatdata.data.content += "\n"+this.message3+"\n"    
                                }
                                                    
                               })//amount

                            }//end del else    
                }//end del if               
            
                //console.log("edad del titular", this.agetitular)
                //console.log("fecha de pago", this.form.fee_payment_date)
                //console.log("id "+this.form.id+", edad del titular"+this.client+ ","+this.agetitular+"cumpleaños"+birthday)
                //console.log("tipo_notificacion_2",this.notification_type_2)
          }////fin del for    


          if (this.delete_dependent==false && this.dependent_change==false && this.dependentH_change==false && this.titular_change==false){
            
            this.standar = true
            this.message0="\n<br>Estándar<br>"
             //document.getElementById("content").value +=this.message_standar
             this.emailformatdata.data.content += this.message0
          }

          //console.log("this.emailformatdata.data.content",this.emailformatdata.data.content)
    
           })//plan        
          })///deductible
        })////dependent
      })///policy
            
            let attached = {}
           // attached.payment_id = this.form.payment_id
            //attached.feeId =  data.fee_id
            let attacheds = this.API.service('show', this.API.all('renewalnotificationdocs'))
            attacheds.one(policyId).get().then((response) => {
                let dataSet = API.copy(response)
                this.previouslines = dataSet.data
               /* this.description = this.previous_lines[0] ? this.previous_lines[0].description : 'Formulario Autorización'
                this.filename = this.previous_lines[0] ? this.previous_lines[0].filename : ''
                if (this.previous_lines.length == 0){
                    this.lines.push({id: 0})
                    this.descriptions.push(0)
                }*/
                
            
            })
            
    }


    calculateAge(date){
        if (date != undefined){
            var date = date
            var values=date.split("-")
            var dia = values[2]
            var mes = values[1]
            var ano = values[0]
            var fecha_hoy = new Date()
            var ahora_ano = fecha_hoy.getYear()
            var ahora_mes = fecha_hoy.getMonth()+1
            var ahora_dia = fecha_hoy.getDate()+1
            var edad = (ahora_ano + 1900) - ano
            if ( ahora_mes < mes ){
                edad--
            }
            if ((mes == ahora_mes) && (ahora_dia < dia)){
                edad--
            }
            if (edad > 1900){
                edad -= 1900
            }
            var meses=0
            if(ahora_mes>mes)
                meses=ahora_mes-mes
            if(ahora_mes<mes)
                meses=12-(mes-ahora_mes)
            if(ahora_mes==mes && dia>ahora_dia)
                meses=11

            var dias=0
            if(ahora_dia>dia)
                dias=ahora_dia-dia
            /*if(ahora_dia<dia)
            {
                console.log('ahora_ano',ahora_ano,'ahora_mes',ahora_mes)
                ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
                console.log('ultimoDiaMes',ultimoDiaMes)
                dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
            }*/
            if (edad < 1){
                if (meses ==1){
                    return meses+' mes'
                }
                else{
                    return meses+' meses'
                }
            }
            else{
                return edad
            }
            }
    }
      addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});

            //console.log('lineas',this.lines)
        
        //console.log('archivos',this.files)
    }

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    deleteLine(index){//eliminar de los archivos nuevos agregados
        //console.log('antes')
        //console.log('lines',this.lines)
        //console.log('doc_typeselected',this.doc_typeselected)
        //console.log('value',this.value)
        //console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        //console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.value.splice(pos,1)
        this.file.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        //console.log('despues')
        
        //console.log('doc_typeselected',this.doc_typeselected)
        //console.log('value',this.value)
        
    }


    send()
    {
        var vm = this
        var file=vm.file
        console.log("file",file)
        if (file.length == 0)
        {        
          this.API.service('send').post({
              policy_id : this.policyId,
              format_id : 14,
              content: this.emailformatdata.data.content,
              fee_id: this.feeId
              
           }).then( ( response ) => {
                swal({      
                    title: 'Aviso de Renovación Enviado Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true

                }, function () {
                    swal.close() 
                    $state.reload()
                })
            }, function (response) {
                console.log('error')
              })
        }
        else
        {   
            var policyId=this.policyId
            var $http = this.$http
            let filenames = []
            var API =this.API
            var $state =this.$state
            var vm =this

            angular.forEach(vm.myFile, function (value) {
                      filenames.push(value.name)
                })
                vm.filenames = filenames 
                
                

            angular.forEach(file, function(file)
            {
                var form_data = new FormData();
                angular.forEach(file, function (file){
                    form_data.append('policy_id', policyId)
                    form_data.append('file', file)
                    form_data.append('filenames', vm.filenames)
                })
                
                $http.post('uploadFilesRenewalNotifications', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){ 

                    API.service('send').post({
                    policy_id : policyId,
                    format_id : 14,
                    file      : file,
                    filename: vm.filenames,
                    content: vm.emailformatdata.data.content,
                    fee_id : vm.feeId
                    }).then( ( response ) => {
                    swal({      
                        title: 'Aviso de Renovación Enviado Correctamente!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true

                    }, function () {
                        swal.close() 
                        $state.reload()
                    })
                    }, function (response) {
                        console.log('error')
                    })
                })//upload response
            })  
        }//else
    }



    save(isValid){
            
            let uploadFile = this.uploadFile
            var file=this.file
            let filenames = []
            var vm = this

            angular.forEach(this.myFile, function (value) {
            filenames.push(value.name)
            })
            this.filenames = filenames

           /* if (this.pfiles){
                for (var i = 0; i < vm.previouslines.length; i++) {
                    if (this.pfiles[i]) {
                    vm.previouslines[i].filename = this.pfiles[i][0].name
                    }
                }
            }*/

            console.log("vm.file",this.file)
            console.log("pfiles",this.pfiles)
            console.log("file"+JSON.stringify(file))
            console.log("vm.myFile", this.myFile)
            console.log("vm.myFile2", this.myFile2)
            console.log("filenames",this.filenames)
            console.log("notification_type_1", this.notification_type_1)
            console.log("notification_type_2", this.notification_type_2)
            console.log("notification_type_2H", this.notification_type_2H)
            console.log("this.message1",this.message1)
            console.log("this.message2",this.message2)
            console.log("this.message2H",this.message2H)
            this.$state.go(this.$state.current, {}, { alerts: 'test' })

            if (isValid) {
                console.log("isValid",isValid)
                this.send()
                this.processing = true
                
            console.log("vm.file",this.file)
            console.log("pfiles",this.pfiles)
            console.log("file"+JSON.stringify(file))
            console.log("vm.myFile", this.myFile)
            console.log("vm.myFile2", this.myFile2)
            console.log("filenames",this.filenames)
            
            var $http = this.$http
            var policyId= this.policyId
            var myFile = this.myFile
            let cancel = this.cancel
            //this.uploadFile()
            let $state = this.$state


            let documents= this.API.service('renotificationdocs', this.API.all('renewalnotificationdocs'))
            documents.post({
                id_policy : this.policyId,
                file : file,
                filename : this.filenames,
                directory : this.directory,
                id_doc_types : this.doc_typeselected,
                modified : this.previouslines,
                deleted_ids: this.deletedlines
            }).then( ( response ) => {
                            swal({      
                                title: 'Documentos guardados Correctamente!',
                                type: 'success',
                                confirmButtonText: 'OK',
                                closeOnConfirm: true
                            }, function () {
                                swal.close()
                                $state.reload()
                                $state.go("app.renewal-lists")
                               })
                        }, function (response) {
                            console.log('error')
                          })
            } else { 
              this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const RenewalNotificationComponent = {
    templateUrl: './views/app/components/renewal-notification/renewal-notification.component.html',
    controller: RenewalNotificationController,
    controllerAs: 'vm',
    bindings: {}
}
