class PolicyViewController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.doc_type_id1 = 1
        this.directory = '/policies/'
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let policyId = $stateParams.policyId
        this.policyId = policyId
        //console.log("xx")
        let PolicyData = API.service('show', API.all('policies'))
        PolicyData.one(policyId).get()
          .then((response) => {
            this.policydata = API.copy(response)
            console.log("poliza ", this.policydata.data)
        })
          //getPolicyInfo
        let Policy = this.API.service('getPolicyInfo');
        let data = {};
        data.policy_id=   $stateParams.policyId;

        Policy.getList(data).then((response) => {
        let dataSet = response.plain()
        this.extrainfo = dataSet
        this.showhidepregnant = this.extrainfo[0].have_prev_safe == '1' ? true : false

        console.log("data", this.extrainfo)

        })


        //get doctypes
        let DocTypes = this.API.service('doctypes')
        DocTypes.getList()
        .then((response) => {
            let systemDocTypes = []
            let docTypesResponse = response.plain()
            angular.forEach(docTypesResponse, function (value) {
                systemDocTypes.push({id: value.id, name: value.name})
            })

            this.systemDocTypes = systemDocTypes
        })




        //get policy files
        let PolicyfileData = API.service('show', API.all('policydocuments'))
        PolicyfileData.one(policyId).get()
          .then((response) => {
            this.policyfiles = response.plain()
            this.esto = this.policyfiles.data
            console.log("files ", this.esto)
        })



    }

    $onInit(){
    }
}

export const PolicyViewComponent = {
    templateUrl: './views/app/components/policy-view/policy-view.component.html',
    controller: PolicyViewController,
    controllerAs: 'vm',
    bindings: {}
}
