class ClaimDocumentTypesAddController{
    constructor($scope, $compile, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        this.$stateParams = $stateParams
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {    
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this       
          let ClaimDocumentsType = this.API.service('claimdocumenttypes', this.API.all('claimdocumenttypes'))
          let $state = this.$state          
          
          ClaimDocumentsType.post({
            'name': this.name
            }).then(function () {
                vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el tipo de Documento .' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const ClaimDocumentTypesAddComponent = {
    templateUrl: './views/app/components/claim-document-types-add/claim-document-types-add.component.html',
    controller: ClaimDocumentTypesAddController,
    controllerAs: 'vm',
    bindings: {}
}
