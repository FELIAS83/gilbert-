class MassivemailController{
    constructor($scope,API,$state, $stateParams){
        'ngInject';

        this.API = API
        //this.$scope=$scope
        this.procesando = false
        this.$state = $state
        this.theform = []
        this.llave_users = false
        this.llave_all = false
        this.llave_active = false
        this.llave_inactive = false
        this.llave_clients =false
        this.format="none"
        this.seleccionados={};
        this.alerts = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }        

        let listformats = this.API.service('email_formats')
        listformats.getList()
        .then((response) => {
     
            this.formatos = response.plain()
            console.log(this.formatos)
      })

    }    
   
    save (isValid) {
  
    if (isValid) {
        let theform = this.API.service(this.API.all('massivemail'))
        this.procesando = true

        this.sendMail()
            this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {          
          let Massivemail = this.API.service('massivemails', this.API.all('massivemails'))
          let $state = this.$state 
          var destinos=JSON.stringify(this.seleccionados)     
     
        var escogidos = ''
        angular.forEach(this.seleccionados, function (value, key) {

            escogidos = escogidos + "," + key            
        })
        escogidos = escogidos.substr(1);

        //console.log("this.format",this.format)

        if (this.format == "none" || this.format == undefined){
            //console.log("entro en vacio")
            var format1='0'
            var subject1 = this.theform.subject
            var title1 = this.theform.title
            var content1 = this.theform.content
        }
        else{
            var format1 = this.format
            var subject1 = this.formatos[this.format].subject
            var title1 = this.formatos[this.format].title
            var content1= this.formatos[this.format].content
        }

          Massivemail.post({
            'id':this.id,
            'format': format1,
            'subject':subject1, 
            'title':title1,
            'content':content1,
            'recipient':escogidos

            }).then(function () {
            //console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el correo masivo.' }
            $state.go($state.current, { alerts: alert})
            //console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else {
      this.formSubmitted = true
            }
        }
    }
    sendMail(){//envío correos si está habilitado en el sistema
        if (this.llave_users == true){
            //console.log("entro en USUARIOS")
            if (JSON.stringify(this.seleccionados)=='{}'){
                let alert = { type: 'error', 'title': 'Correo sin destinatarios!', msg: 'Debe seleccionar al menos un destinatario para enviar el correo' }
                this.alerts = [alert]
                this.procesando = false
            }
            else{

                var escogidos = ''
            angular.forEach(this.seleccionados, function (value, key) {
                if (value == true){
                escogidos = escogidos + "," + key
                }            
            })
            escogidos = escogidos.substr(1);
            //console.log("escogidos",escogidos)
            //console.log("seleccionados",this.seleccionados)
            var seleccion=[]
            var maildir= []
            let Users= this.API.service('users')
            Users.getList()
                .then((response)=>{
                var data=response.plain()
                //console.log("data",data)
                    for (var i = 0; i < escogidos.length; i++) {
                        if ( escogidos[i] !=',') {
                            
                            seleccion.push({id: escogidos[i]})
                        }
                    }
                    this.seleccion=seleccion
                //console.log("seleccion",this.seleccion)
                
                for (var i = 0; i < seleccion.length; i++) {
                     for (var j = 0; j < data.length; j++) {
                        if (  seleccion[i].id == data[j].id )
                        {
                            maildir[i] = data[j].email
                        }
                    }
                }     
                  
                this.maildir=maildir
                //console.log("maildir",this.maildir)
                //console.log("contenido",this.theform.contenido)
                let $state = this.$state
                    this.API.all('sendmasive')
                    .post({
                    title: this.theform.title,
                    content : this.theform.content,
                    to : this.maildir,
                    format: this.theform.format,
                    subject : this.theform.subject })
                    .then((response)=>{
                        //let alert = { type: 'success', 'title': 'Éxito!', msg: 'El correo ha sido enviado exitosamente al(a los) destinatario(s)' }
                        swal("Éxito!", "'El correo ha sido enviado exitosamente al(a los) destinatario(s)");
                        $state.reload()
                        this.alerts = [alert]
                        this.procesando = false})
               })
            }//else
        }//if
        else if (this.llave_all == true){
            //console.log("entro en TODOS")
            if (JSON.stringify(this.seleccionados)=='{}'){
                let alert = { type: 'error', 'title': 'Correo sin destinatarios!', msg: 'Debe seleccionar al menos un destinatario para enviar el correo' }
                this.alerts = [alert]
                this.procesando = false
            }
            else{

                var escogidos = ''
            angular.forEach(this.seleccionados, function (value, key) {
                if (value == true){
                escogidos = escogidos + "," + key
                }            
            })
            escogidos = escogidos.substr(1);
            //console.log("escogidos",escogidos)
            //console.log("seleccionados",this.seleccionados)
            var seleccion=[]
            var maildir= []
            let Applicants = this.API.service('applicants')
            Applicants.one().get()
                .then((response)=>{
                var dataSet=response.plain()
                //console.log("data",data)
                var sel= escogidos.split(",")
                    for (var i = 0; i < escogidos.length; i++) {
                        if ( sel[i] != undefined) {
                            
                            seleccion.push({id: parseInt(sel[i])})
                        }
                    }
                    this.seleccion=seleccion
                //console.log("seleccion",this.seleccion)
                
                for (var i = 0; i < seleccion.length; i++) {
                     for (var j = 0; j < dataSet.data.applicants.length; j++) {
                        if (  seleccion[i].id == dataSet.data.applicants[j].id )
                        {
                            maildir[i] = dataSet.data.applicants[j].email
                        }
                    }
                }     
                  
                this.maildir=maildir
                //console.log("maildir",this.maildir)
                //console.log("contenido",this.theform.contenido)
                let $state = this.$state
                    this.API.all('sendmasive')
                    .post({
                    title: this.theform.title,
                    content : this.theform.content,
                    to : this.maildir,
                    format: this.theform.format,
                    subject : this.theform.subject })
                    .then((response)=>{
                        //let alert = { type: 'success', 'title': 'Éxito!', msg: 'El correo ha sido enviado exitosamente al(a los) destinatario(s)' }
                        swal("Éxito!", "'El correo ha sido enviado exitosamente al(a los) destinatario(s)");
                        $state.reload()
                        this.alerts = [alert]
                        this.procesando = false})
               })
            }//else
        }//if
        else if (this.llave_active == true){
            //console.log("entro en ACTIVOS")
            if (JSON.stringify(this.seleccionados)=='{}'){
                let alert = { type: 'error', 'title': 'Correo sin destinatarios!', msg: 'Debe seleccionar al menos un destinatario para enviar el correo' }
                this.alerts = [alert]
                this.procesando = false
            }
            else{
                //console.log("seleccionados1",this.seleccionados)
                var escogidos = ''
            angular.forEach(this.seleccionados, function (value, key) {
                if (value == true){
                escogidos = escogidos + "," + key
                }            
            })
            escogidos = escogidos.substr(1);
            //console.log("escogidos",escogidos)
            //console.log("seleccionados",this.seleccionados)
            var seleccion=[]
            var maildir= []
            let Applicants = this.API.service('getActiveClients')
            Applicants.one().get()
                .then((response)=>{
                var dataSet=response.plain()
                //console.log("data",data)
                var sel= escogidos.split(",")
                //console.log("sel",sel)
                    for (var i = 0; i < escogidos.length; i++) {
                        //console.log(sel[i])
                        if ( sel[i] != undefined) {
                            
                            seleccion.push({id: parseInt(sel[i])})
                        }
                    }

                for (var i = 0; i < seleccion.length; i++) {
                     for (var j = 0; j < dataSet.data.length; j++) {
                        if (  seleccion[i].id == dataSet.data[j].id )
                        {
                            maildir[i] = dataSet.data[j].email
                        }
                    }
                }     
                  
                this.maildir=maildir
                //console.log("maildir",this.maildir)
                //console.log("contenido",this.theform.contenido)
                let $state = this.$state
                    this.API.all('sendmasive')
                    .post({
                    title: this.theform.title,
                    content : this.theform.content,
                    to : this.maildir,
                    format: this.theform.format,
                    subject : this.theform.subject })
                    .then((response)=>{
                        //let alert = { type: 'success', 'title': 'Éxito!', msg: 'El correo ha sido enviado exitosamente al(a los) destinatario(s)' }
                        swal("Éxito!", "'El correo ha sido enviado exitosamente al(a los) destinatario(s)");
                        $state.reload()
                        this.alerts = [alert]
                        this.procesando = false})
               })
            }//else
        }//if
        else if (this.llave_inactive == true){
            console.log("entro en INACTIVOS")
            if (JSON.stringify(this.seleccionados)=='{}'){
                //console.log("entro en seleccionados vacio")
                let alert = { type: 'error', 'title': 'Correo sin destinatarios!', msg: 'Debe seleccionar al menos un destinatario para enviar el correo' }
                this.alerts = [alert]
                this.procesando = false
            }
            else{

                var escogidos = ''
            angular.forEach(this.seleccionados, function (value, key) {
                if (value == true){
                escogidos = escogidos + "," + key
                }            
            })
            escogidos = escogidos.substr(1);
            //console.log("escogidos",escogidos)
            //console.log("seleccionados",this.seleccionados)
            var seleccion=[]
            var maildir= []
            let Applicants = this.API.service('getInactiveClients')
            Applicants.one().get()
                .then((response)=>{
                this.dataSet=response.plain()
                //console.log("dataSet",this.dataSet.data)
                //console.log("longitud escogidos", escogidos.length)
                //console.log("longitud escogidos", escogidos.length)
                var sel= escogidos.split(",")
                //console.log("sel",sel)
                    for (var i = 0; i < escogidos.length; i++) {
                        //console.log(sel[i])
                        if ( sel[i] != undefined) {
                            
                            seleccion.push({id: parseInt(sel[i])})
                        }
                    }


                    this.seleccion=seleccion
                //console.log("seleccion",this.seleccion)
                //console.log("longitud seleccion", seleccion.length)
                //console.log("longitud data", this.dataSet.data.length)
                for (var i = 0; i < seleccion.length; i++) {
                     for (var j = 0; j < this.dataSet.data.length; j++) {
                        if (  seleccion[i].id == this.dataSet.data[j].id )
                        {
                            maildir[i] = this.dataSet.data[j].email
                        }
                    }
                }     
                  
                this.maildir=maildir
                console.log("maildir",this.maildir)
                //console.log("contenido",this.theform.contenido)
                let $state = this.$state
                    this.API.all('sendmasive')
                    .post({
                    title: this.theform.title,
                    content : this.theform.content,
                    to : this.maildir,
                    format: this.theform.format,
                    subject : this.theform.subject })
                    .then((response)=>{
                        //let alert = { type: 'success', 'title': 'Éxito!', msg: 'El correo ha sido enviado exitosamente al(a los) destinatario(s)' }
                        swal("Éxito!", "'El correo ha sido enviado exitosamente al(a los) destinatario(s)");
                        $state.reload()
                        this.alerts = [alert]
                        this.procesando = false})
               })
            }//else
        }//if
    }

    changeFormat(i){
        //console.log("format",this.format)
        if (this.format=="none" || this.format== undefined ){
            this.theform.format = ""
            this.theform.subject = ""
            this.theform.title = ""
            this.theform.content = ""
        }
        else{
            this.theform.format = this.formatos[this.format].format
            this.theform.subject = this.formatos[this.format].subject
            this.theform.title = this.formatos[this.format].title
            this.theform.content = this.formatos[this.format].content
            // this.lista.recipient = this.seleccionados.value
        }
    }

      selectRecipients(){
       var select =document.getElementById('chosen')
       var valor = select.options[select.selectedIndex].value;

       if (valor == 0){/////SELECCIONE LIMPIA EL LISTADO
            this.llave_users=false
            this.llave_clients=false
            this.llave_all=false
            this.llave_active=false
            this.llave_inactive=false
            
       } 
 
       if (valor==1){/////////////usuarios
            this.llave_users=true
            this.llave_clients=false
            this.llave_all=false
            this.llave_active=false
            this.llave_inactive=false
            //alert("selecciono 1", valor)
            this.API.all('users').doGET("", {}).
            then((response) =>  {
                let dataSet = response.plain()
                this.lista=dataSet.data.users


                for (var i = 0; i < this.allApplicants.length; i++) {
                this.seleccionados[this.allApplicants[i].id] = undefined
                }

               //console.log("seleccionados", this.seleccionados)
            })
       }
       else if (valor==2){///////////TODOS LOS CLIENTES
            this.llave_clients=true
            this.llave_users=false         
            this.llave_all=true
            this.llave_active=false
            this.llave_inactive=false
             //alert("selecciono 2",valor)
            let allApplicants= this.API.service('applicants')
            allApplicants.one().get().then((response) =>  {
                let dataSet = response.plain()
                this.allApplicants=dataSet.data.applicants
                //console.log("allclientes",this.allApplicants)
              
                for (var i = 0; i < this.allApplicants.length; i++) {
                    this.seleccionados[this.allApplicants[i].id] = true
                }
                //console.log("selecccionados todos", this.seleccionados)       
            })
       }

    }

    changeClients(){
       var select =document.getElementById('clients')
       var valor = select.options[select.selectedIndex].value;
 
       if (valor==1){////////////todos
            this.llave_all=true
            this.llave_active=false
            this.llave_inactive=false
            this.llave_users=false
            let allApplicants= this.API.service('applicants')
            allApplicants.one().get().
            then((response) =>  {
                let dataSet = response.plain()
                this.allApplicants=dataSet.data.applicants
                //console.log("allclientes",this.allApplicants)

                for (var i = 0; i < this.inactiveApplicants.length; i++) {
                   this.seleccionados[this.inactiveApplicants[i].id] = undefined
                }

                for (var i = 0; i < this.activeApplicants.length; i++) {
                this.seleccionados[this.activeApplicants[i].id] = undefined
                }
              
                
                for (var i = 0; i < this.allApplicants.length; i++) {
                    this.seleccionados[this.allApplicants[i].id] = true
                }
                //console.log("seleccionados_all",this.seleccionados)        
            })
       }
       else if (valor==2){//////////activos
            this.llave_users=false
            this.llave_active=true
            this.llave_all=false
            this.llave_inactive=false
             //alert("selecciono 2",valor)
            let activeApplicants= this.API.service('getActiveClients')
            activeApplicants.one().get().
            then((response) =>  {
                let dataSet = response.plain()
                this.activeApplicants = dataSet.data
                //console.log("activeclientes",this.activeApplicants)
                
                for (var i = 0; i < this.allApplicants.length; i++) {
                   this.seleccionados[this.allApplicants[i].id] = undefined
                }    


                for (var i = 0; i < this.activeApplicants.length; i++) {
                this.seleccionados[this.activeApplicants[i].id] = true
                }
                //console.log("seleccionados2", this.seleccionados)        
            })
       }
       else if (valor==3){////////////inactivos
            this.llave_inactive=true
            this.llave_all=false
            this.llave_active=false
             //alert("selecciono 2",valor)
            let inactiveApplicants= this.API.service('getInactiveClients')
            inactiveApplicants.one().get().
            then((response) =>  {
                let dataSet = response.plain()
                this.inactiveApplicants=dataSet.data
                //console.log("inactiveclientes",this.inactiveApplicants)

                for (var i = 0; i < this.allApplicants.length; i++) {
                   this.seleccionados[this.allApplicants[i].id] = undefined
                }

               /* for (var i = 0; i < this.allApplicants.length; i++) {
                   this.seleccionados[this.activepplicants[i].id] = undefined
                }*/

                for (var i = 0; i < this.inactiveApplicants.length; i++) {
                   this.seleccionados[this.inactiveApplicants[i].id] = true
                }
                //console.log("seleccionadosinactivos",this.seleccionados) 
            })
       }
    }

        
    $onInit(){
    }
}

export const MassivemailComponent = {
    templateUrl: './views/app/components/massivemail/massivemail.component.html',
    controller: MassivemailController,
    controllerAs: 'vm',
    bindings: {}
}