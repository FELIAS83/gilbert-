class TicketClaimListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService,$uibModal){
        'ngInject';
        //$state.reload()

        this.API = API
        this.$state = $state
        this.can = AclService.can
        this.$uibModal = $uibModal

        let claimId = $stateParams.claimId

        var vm = this
        var relationship = ""
        var orderRole = 0
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.client = ""
        this.role = ""
        this.planDeduc = ""
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        this.lines = []
        this.lines_1 = []
        this.birthdayaffiliate = []
        this.deletedamendments = []
        this.deletedannexes = []
        this.details = []
        this.fees = []
        
        console.log("allAffiliates ", allAffiliates)


        //let Agents = API.service('diagnostics')
       /* let Coverage = API.service('diagnostic', API.all('diagnostics'))
        Coverage.one(claimId).get()
          .then((response) => {

            let dataSet = response.plain()
            console.log("data ", dataSet)*/

        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.claimId=claimId
          //getPolicyInfo
        let Policy = this.API.service('getPolicyInfo');
        let data = {};
        data.policy_id=   $stateParams.policyId;
        console.log("claimId "+this.claimId)
        console.log("policyId "+this.policyId)
        /*console.log("data.policy_id"+data.policy_id)
        console.log("data"+JSON.stringify(data))*/

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible
            //console.log("client"+this.client)
            //console.log("dataSet"+JSON.stringify(dataSet))
            //this.policyinfo[0].birthday = new Date(this.policyinfo[0].birthday)
            this.role =  "Titular"
            let applicantId = this.policyinfo[0].applicant_id
            let Dependents = API.service('dependents', API.all('dependents'))
            var vm = this
            Dependents.one(applicantId).get()
            .then((response) => {
                //console.log(response.plain(),'dependentes')
                var vm = this
                let dependentsResponse = response.plain()
                let dependentq = 0
                vm.dependentq = dependentq
                let spouse = 0
                vm.spouse = spouse
                angular.forEach(dependentsResponse.data, function (value) {
                    let ageh = 0
                    vm.ageh = ageh

                    /*if(value.relationship == "H"){
                        relationship = "Cónyuge"
                        orderRole = 2
                       // vm.ageh = vm.calculateAge(value.birthday)
                        vm.spouse = 1
                    }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        vm.dependentq++
                    }*/
                    
                    systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, relationship: relationship})

                    //insertar detalles
                    let datas = {}
                    datas.claimId = vm.claimId
                    datas.id = value.id
                    datas.relationship = value.relationship

                    let Documents = API.service('diagnosticdocument', API.all('diagnostics'))
                    Documents.getList(datas)
                    .then((response) => {
                        //let dataSet = API.copy(response)
                        let dataSet = response.plain()
                        let diagnostic = dataSet
                        let statusdispatch = false
                        if (diagnostic.length != 0){
                            statusdispatch = true
                        }
                        if(value.relationship == "H"){
                            relationship = "Cónyuge"
                            orderRole = 2
                            vm.spouse = 1
                        }
                        else{
                            relationship = "Dependiente"
                            orderRole = 3
                            vm.dependentq++
                        }

                        allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, role: value.relationship, order: orderRole, relationship: relationship,detail: diagnostic, statusdispatch: statusdispatch,  })
                        })


                })

               // let agetitular = this.calculateAge(this.policyinfo[0].birthday)

                //insertar detalles
                let datas = {}
                datas.claimId = this.claimId
                datas.id = this.policyinfo[0].id
                datas.relationship = "T"
                let Documentst = API.service('diagnosticdocument', API.all('diagnostics'))
                Documentst.getList(datas)
                .then((response) => {
                    let dataSet = response.plain()
                    let diagnostic = dataSet
                    let statusdispatch = false
                        if (diagnostic.length != 0){
                            statusdispatch = true
                        }
                        //console.log("diagnostic2",diagnostic);
                    allAffiliates.push({id: parseInt(this.policyinfo[0].id), name: this.client, role: "T", order: 1, relationship: 'Titular', detail: diagnostic, statusdispatch: statusdispatch})

                })
                
                //console.log('newafiliate',allAffiliates)
                allAffiliates.sort(function (a, b) {
                    if (a.order > b.order) {
                        return 1;
                    }
                    if (a.order < b.order) {
                        return -1;
                    }
                    return 0;
                });

                /*systemDependents.sort(function (a, b){
                  return (b.relationship - a.relationship)
                })*/
                //this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                //console.log("systemDependents"+JSON.stringify(this.systemDependents))
                //console.log("allAffiliates",this.allAffiliates)
                if (this.policyinfo[0].mode == 0){
                    this.payment_type = 'Anual'
                    this.amount_fees = 1
                }
                else if (this.policyinfo[0].mode == 1){
                    this.payment_type = 'Semestral'
                    this.amount_fees = 2
                }
                else if (this.policyinfo[0].mode == 2){
                    this.payment_type = 'Trimestral'
                    this.amount_fees = 4
                }
                else if (this.policyinfo[0].mode == 3){
                    this.payment_type = 'Mensual'
                    this.amount_fees = 12
                }

                if (isNaN(this.ageh)){
                    this.ageh = 0
                }
          })
        })


        let actionsHtml = (data) =>  {
          return`<ul>
                    <a  title="Atachar Documentos" class="btn btn-xs btn-info" ui-sref="app.assistent-lists({agentId: ${data.id}})">
                        <i class="fa fa-list-ul"></i>
                    </a>
                    &nbsp
                    <!--<a  title="Agregar Sub-Agente" class="btn btn-xs btn-success" ui-sref="app.agent-sublists({agentId: ${data.id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    &nbsp
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.agent-edit({agentId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp-->
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }


        let Documents = API.service('documentswarning', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.warning = dataSet.data
            console.log('warning',dataSet.data)
            this.documents = ''
            let document_type

            this.warning.forEach(function(element) {
                //console.log(element)
                if (element.id_doc_type == "1"){
                    document_type = 'Factura'
                }
                else if (element.id_doc_type == "2"){
                    document_type = 'Informe'
                }
                else if (element.id_doc_type == "3"){
                    document_type = 'Orden'
                }
                else if (element.id_doc_type == "4"){
                    document_type = 'Resultado'
                }
                vm.documents = vm.documents +' '+ document_type + ' ' + element.filename + '; '
            });
        })
    }
    modalcontroller ($uibModalInstance, API, claimId,client,id,relationship) {
        'ngInject'

        this.claimId = claimId
        this.client = client

        /*let Documents = API.service('diagnosticdocument', API.all('diagnostics'))
        Documents.one(claimId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.diagnostic = dataSet.data
        })*/
        let data = {}
        data.claimId = claimId
        data.id = id
        data.relationship = relationship
        let Documents = API.service('diagnosticdocument', API.all('diagnostics'))
        Documents.getList(data)
        .then((response) => {
            //let dataSet = API.copy(response)
            let dataSet = response.plain()
            this.diagnostic = dataSet
        })


        this.ok = () => {
            $uibModalInstance.dismiss('cancel')
        }
    }
    modal (id,relationship,client) {
        let size = 'lg'
        let $uibModal = this.$uibModal
        let claimId = this.claimId
        var modalInstance = $uibModal.open({

        templateUrl: 'myModalDocContent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
            claimId: () => {
                return claimId
            },
            client: () => {
                return client
            },
            id: () => {
                return id
            },
            relationship: () => {
                return relationship
            }
        }
      })
    }
    dispatch(id,role){
        let person_type
        //console.log('id',id,'role',role)
        if(role == 'T'){
            person_type ='A'
        }
        else{
            person_type ='D'
        }

        let Diagnostics = this.API.service('dispatchpatient', this.API.all('diagnostics'))
        Diagnostics.post({
            'id_person': id,
            'person_type': person_type,
            'claimId': this.claimId,
            'dispatch_type': 1
        }).then(function () {
            swal({
                title: 'Diagnósticos Despachados Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
              })
        }, function (response) {
            swal({
                title: 'Error Inesperado!',
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
              })
        })
    }

    dispatchAll(){
        let $state = this.$state
        let Diagnostics = this.API.service('dispatchpatient', this.API.all('diagnostics'))
        Diagnostics.post({
            'claimId': this.claimId,
            'dispatch_type': 2
        }).then(function () {
            swal({
                title: 'Diagnósticos Despachados Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                $state.go('app.claim-claim-lists')
              })
        }, function (response) {
            swal({
                title: 'Error Inesperado!',
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
              })
        })
    }

    $onInit(){
    }
}

export const TicketClaimListsComponent = {
    templateUrl: './views/app/components/ticket-claim-lists/ticket-claim-lists.component.html',
    controller: TicketClaimListsController,
    controllerAs: 'vm',
    bindings: {}
}
