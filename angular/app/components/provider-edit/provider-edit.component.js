class ProviderEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

         this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let providerId = $stateParams.providerId
        //console.log(providerId)
        let ProviderData = API.service('show', API.all('providers'))
        ProviderData.one(providerId).get()
          .then((response) => {
            this.providereditdata = API.copy(response)
           // this.providereditdata.data.commission_percentage = parseFloat(this.providereditdata.data.commission_percentage, 2)
          //console.log("data=>", this.providereditdata);
        })
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.providereditdata);
          let $state = this.$state
          this.providereditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el Proveedor de Servicio.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const ProviderEditComponent = {
    templateUrl: './views/app/components/provider-edit/provider-edit.component.html',
    controller: ProviderEditController,
    controllerAs: 'vm',
    bindings: {}
}
