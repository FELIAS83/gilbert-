class CoverageDetailsAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleId = $stateParams.deductibleId

        
        let DeductibleData = API.service('show', API.all('deductibles'))
        DeductibleData.one(deductibleId).get()
          .then((response) => {
            this.deductibleeditdata = API.copy(response)
            console.log("data del deductible", this.deductibleeditdata.data)

            let PlanData = API.service('show', API.all('plans'))
            PlanData.one(this.deductibleeditdata.data.plan_id).get()
            .then((response) => {
            this.planeditdata = API.copy(response)
            console.log("data del plan", this.planeditdata.data)

                let Coverages = this.API.service('coverages')
                Coverages.getList()
                .then((response) => {
                    let systemCoverages = []
                    let coverageResponse = response.plain() 
                    console.log("cobertura", coverageResponse)
                    angular.forEach(coverageResponse, function (value) {
                        if(value.plan_id == vm.deductibleeditdata.data.plan_id){
                         systemCoverages.push({id: value.id, name: value.name})
                        }
                    })
                    this.systemCoverages = systemCoverages
                    console.log("systemCoverages==> ", systemCoverages)
                })
            })
            


      })
        

        //
    }

    save (isValid) {    
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {          
          let CoverageDetails = this.API.service('details', this.API.all('coverage_details'))
          let $state = this.$state        
          console.log("id de cobertura", this.coverageid)
          CoverageDetails.post({
            'amount': this.amount,
            'coverageid': this.coverageid,
            'deductible_id': this.deductibleeditdata.data.id

          }).then(function () {
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el detalle de la cobertura.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const CoverageDetailsAddComponent = {
    templateUrl: './views/app/components/coverage-details-add/coverage-details-add.component.html',
    controller: CoverageDetailsAddController,
    controllerAs: 'vm',
    bindings: {}
}
