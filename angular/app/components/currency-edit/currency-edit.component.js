class CurrencyEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let currencyId = $stateParams.currencyId
        //console.log(currencyId)
        let CurrencyData = API.service('show', API.all('currencies'))
        CurrencyData.one(currencyId).get()
          .then((response) => {
            this.currencyeditdata = API.copy(response)
            //this.currencyeditdata.data.status
          //console.log("data=>", this.currencyeditdata);
        })
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.currencyeditdata);
          let $state = this.$state
          this.currencyeditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado la Moneda.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const CurrencyEditComponent = {
    templateUrl: './views/app/components/currency-edit/currency-edit.component.html',
    controller: CurrencyEditController,
    controllerAs: 'vm',
    bindings: {}
}
