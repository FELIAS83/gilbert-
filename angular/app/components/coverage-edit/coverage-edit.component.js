class CoverageEditController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';
         this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let coverageId = $stateParams.coverageId

        
        let CoverageData = API.service('show', API.all('coverages'))
        CoverageData.one(coverageId).get()
          .then((response) => {
            this.coverageeditdata = API.copy(response)
        
        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(this.coverageeditdata.data.plan_id).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
        })
        
      })

        //
    }
    save (isValid) {
        if (isValid) {
          let $state = this.$state
          this.coverageeditdata.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado los datos de la cobertura.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const CoverageEditComponent = {
    templateUrl: './views/app/components/coverage-edit/coverage-edit.component.html',
    controller: CoverageEditController,
    controllerAs: 'vm',
    bindings: {}
}
