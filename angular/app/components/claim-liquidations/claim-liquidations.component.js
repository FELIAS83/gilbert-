class ClaimLiquidationsController{
    constructor($state, API,DTOptionsBuilder,DTColumnBuilder,$compile,$scope,$uibModal,$http, $log){
        'ngInject';

        this.API = API
        this.$state = $state
        let step = 1
        let data = {}
        data.claimStatus = 1
        data.step = 1
        var allClaim = []
        this.$uibModal = $uibModal
        this.items = ['item1', 'item2', 'item3']
        let claimStatus = '1'
        this.$log = $log
        var fecha = ""
        var arrfecha=""
        var vm = this


        //let Policies = API.service('claims')
        let Policies = API.service('claimdiagnostics', API.all('claims'))

        Policies.one().get()
          .then((response) => {
            let dataS = response.plain()
            let dataSet = []

            angular.forEach(dataS.data, function(value, index, arr){
              if(value.show == true){
                dataSet.push({id: value.id, claim_id:value.claim_id, created_at:value.created_at, description:value.description, dispatched:value.dispatched, 
                  id_person:value.id_person, name:value.name, number:value.number, person_type:value.person_type, policy_id:value.policy_id, show:value.show,
                  status:value.status, titular:value.titular, amountTotal:value.amountTotal})
              }


            })

             angular.forEach(dataSet, function (value) {

              fecha = value.created_at
              arrfecha= fecha.split("-")
              var day=arrfecha[2]
              var month = arrfecha[1]
              var year= arrfecha[0]

              day = arrfecha[2].split(" ")
              var nday=day[0]

              fecha= month+'/'+day[0]+'/'+year  
              value.created_at = fecha
          
            })

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('order', [[0, 'desc']])
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza').withOption('width', '9%'),
              DTColumnBuilder.newColumn('titular').withTitle('Titular'),
              DTColumnBuilder.newColumn('name').withTitle('Asegurado'),
              DTColumnBuilder.newColumn('description').withTitle('Descripción'),
              DTColumnBuilder.newColumn('amountTotal').withTitle('Monto').withOption('width', '7%'),
              DTColumnBuilder.newColumn('created_at').withTitle('Creación del Trámite').withOption('width', '16%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
            
            this.allClaim = dataSet            
          })
          
        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }



        let actionsHtml = (data) => {

          return `
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Solicitar Documentos Pendientes" class="btn btn-xs btn-primary" ng-click="vm.sendAgent('lg', '${data.name}', ${data.id}, ${data.policy_id}, ${data.claim_id})" ng-disabled="${data.withDiagnostic} == 0">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </button>
                    &nbsp
                    <!--<button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Ver Diagnósticos" class="btn btn-xs btn-primary" ui-sref="app.diagnostics-eob({claimId: ${data.id}, policyId: ${data.policy_id}})" >
                      <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                    </button>  
                    &nbsp  -->
                     <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Subir Docs." class="btn btn-xs btn-primary" ng-click="vm.sendDocument('llg', '${data.name}',${data.id}, ${data.policy_id},${data.claim_id})">
                      <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </button>
                    &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar EOB al Cliente" class="btn btn-xs btn-primary" ng-show="${data.show}" ui-sref="app.send-eob({claimId: ${data.claim_id}, policyId: ${data.policy_id}, diagnosticId: ${data.id}})" >
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </button>
                    &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Devolución de Facturas" class="btn btn-xs btn-primary" ng-click="vm.returnInvoice('lg', '${data.name}', ${data.id}, ${data.policy_id}, ${data.claim_id})">
                      <i class="glyphicon glyphicon-open-file" aria-hidden="true"></i>
                    </button>
                    &nbsp
                     <button  style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Reimprimir Documento de Reclamo" class="btn btn-xs btn-primary"  ng-click="vm.printDocs(${data.id}, ${data.claim_id});">
                      <i class="glyphicon glyphicon-print" aria-hidden="true"></i>
                    </button>

                    `
        }
    }


    printDocs(id, claim_id){
     
      let diagnosticId = []
      diagnosticId.push({id: id, value: id}) 
      let Document = this.API.service('printdocpdf', this.API.all('claims') )
        Document.post({
          'claim_id': claim_id,
          'diagnosticId' : diagnosticId

        })
        .then((response) => {
          let docs = response.plain()
                  docs = docs.data

                  
          //  for (var i = 0 ; i < docs.length; i++) {    

                  var doc = new jsPDF('p', 'mm', 'letter');
                  console.log("doc",docs)
                  var size1 = 11
                  var size2 = 14
                  var sizeT = 10

                  doc.setFontSize(size1);
                  let x_ini = 20
                  let x_fin = 210
                  let y_ini = 20 
                  let sizeText = 0  

                  var rect1_width = 0
                  var rect2_width = 0
                  var x1_rect = 0  // posicion inicial x de la celda para el total en el 1er cuadro
                  var x2_rect = 0  // posicion inicial x de la celda para el total en el 2do cuadro
                  var cell_y = 0  //posicion Y de la celda para el total en el 1er cuadro

                  var data2 = []
                  var data3 = []
                  

                  //
                    sizeText = doc.getStringUnitWidth (docs[0].date) * size1
                    sizeText = sizeText / 2.81
                    let x = x_fin - sizeText  - 10 
                    doc.text(x, 20, docs[0].date); // fecha
                  // 
                    doc.text(x_ini, y_ini+20, 'Señores');
                    doc.setFontType("bold");
                    doc.text(x_ini, y_ini+25, 'BEST DOCTORS');
                    doc.setFontType('normal');
                    doc.text(x_ini, y_ini+30, 'Ciudad.-');
                  //
                    y_ini = y_ini + 30
                    doc.text(x_ini, y_ini+20, 'Atte. Dpto. de Reclamos');
                    doc.text(x_ini, y_ini+30, 'De mis consideraciones:');
                    doc.text(x_ini, y_ini+40, 'Por medio del presente adjunto el siguiente reclamo:');
                  //
                    //console.log("afiliado", docs[i].affiliate)
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+55, 'Asegurado: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+55, docs[0].affiliate);
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+60, 'Póliza: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+60,  docs[0].policy_number);

                    // 1era tabla
                   
                    var columns1 = ["Fecha Enviado", "#GB", "Titular", "# Póliza", "Paciente", "Diágnostico", "Monto"];
                    var data1 = [[docs[0].rowData.date, docs[0].claim_id, docs[0].applicant, docs[0].policy_number, docs[0].affiliate, docs[0].rowData.diagnostic, docs[0].rowData.amount]]
                    //var data1 = [["1","2","3","4","5","6","7"]]
                    //console.log(doc.autoTable(["uno"],[2],{margin:{ top: 25 }}))
                    doc.autoTable(columns1,data1,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                //tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:15, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'},
                                                 4: {overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 5:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 6:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'}
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ top: y_ini+70, right: 20 }
                                              })

                    doc.text(x_ini, y_ini+105, 'Detalles de los Documentos');

                    // 2da tabla - invoices
                    var columns2 = [{title:"Facturas", dataKey:"factura"}, 
                                    {title:"Fecha", dataKey:"fecha_factura"}, 
                                    {title:"Proveedor", dataKey:"provider"}, 
                                    {title:"Valor", dataKey:"monto_factura"}];
                   
                    //console.log("antes del forEach "+i,docs[i])
                    angular.forEach(docs[0].rowData1, function(value){
                      
                      data2.push({factura: value.factura, fecha_factura: value.fecha_factura, provider:value.provider, monto_factura:value.monto_factura})

                    })
                    var lines = data2.length -1
                    var amount_invoices = docs[0].total
                    //console.log("cuadro:", data2)
                  

                     doc.autoTable(columns2,data2,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} , 
                                                startY : y_ini+110,
                                                margin:{ right: 20 },
                                               
                                                drawCell: function(cell, data){
                                                      var index = data.row.index
                                                      var rows = data.table.rows
                                                      //console.log(data)
                                                      
                                                        rect1_width = data.row.cells.factura.width + data.row.cells.fecha_factura.width
                                                        rect2_width = data.row.cells.provider.width + data.row.cells.monto_factura.width
                                                        x1_rect = data.row.cells.factura.x
                                                        x2_rect = data.row.cells.provider.x
                                                        cell_y = cell.y+7.58
                                                      if( index == rows.length - 1){
                                                        doc.rect(x1_rect, cell_y, rect1_width, cell.height,  null)    
                                                        doc.rect(x2_rect, cell_y, rect2_width, cell.height,  null)
                                                      }  
                                                    
                                                    }


                                              })
                     // Amount Total Invoices
                  
                     doc.setFontType('bold');
                     doc.text(x1_rect+20, cell_y+5.58, 'Total');      
                     doc.setFontType('normal');
                     doc.text(x2_rect+23, cell_y+5.58, amount_invoices)    
                    //
                   
                  if(docs[0].rowData2.length > 0){
                      doc.text(x_ini, cell_y+17, "Documentos Adicionales")    

                  //// 2da tabla - invoices
                    var columns3 = [{title:"Tipo de Documento", dataKey:"doc_type"}, 
                                    {title:"Fecha", dataKey:"fecha_doc"}, 
                                    {title:"Descripción", dataKey:"description"}]

                                    
                    angular.forEach(docs[0].rowData2, function(value){
                      
                      data3.push({doc_type: value.doc_type, fecha_doc: value.date, description:value.description_other})

                    })      

                    doc.autoTable(columns3, data3,  {
                                                theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                startY: cell_y+22,
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ right: 20 },
                                                drawCell: function(cell, data){
                                                  cell_y = cell.y
                                                }

                                              })          
                  //
                   //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[0].user)
                  }else{
                  let pageHeight = doc.internal.pageSize.getHeight()
                  console.log("pageHeight", pageHeight)
                  //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[0].user)
                  //  
                  }

                  if(!!window.chrome && !!window.chrome.webstore){
                     window.open(doc.output('bloburl'), '_blank')
                  }else{
                     doc.output('dataurlnewwindow',{})       //creacion de los archivos
                  }
                

        })

    }
    sendAgent (size, name, id, policy_id, claim_id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", id);

      console.log("policy_id", policy_id);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalAgent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          },claim_id: () => {
            return claim_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

sendDocument (size, name, id, policy_id, claim_id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", id);

      console.log("policy_id", policy_id);
      console.log("Asegurado", name);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDocument',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'llg',
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          },claim_id: () => {
            return claim_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

returnInvoice (size, name, id, policy_id, claim_id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", id);

      console.log("policy_id", policy_id);
      console.log("Asegurado", name);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalInvoice',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'lg',
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          },claim_id: () => {
            return claim_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }



//**********************************************//
  modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state, $stateParams, policy_id, claim_id) {
    'ngInject'
    console.log("controller")
      this.API = API
      this.items = items
      this.customer = customer
    
      this.id = id
      console.log("id", this.id)
      this.policy_id = policy_id
      let policyId = this.policy_id
      this.claim_id = claim_id
      console.log("claim id", this.claim_id)
      let claimId = this.claim_id
      this.$state = $state
      this.sendmail = sendmail
      this.diagnosticId = {}
      this.isDisabled = true
      var mvm = this
      this.processing = false
      this.sendbutton = false
      /*************************************/
       this.lines = []
        this.curency_selected = []
        this.curency_selected[0] = 1
        this.value = []
        this.files = []
        this.deletedlines = []
        this.doc_typeselected = []
        this.description = []
        this.provider_id = []
        this.directory = '/claims/'
        this.formSubmitted = false
        this.alerts = []
        this.returnfiles = []        
        this.typeDoc = []
        this.typeDocNew = []
        this.doc_date = []
        this.diagnostic_id = id
       this.$http = $http
/*********************************/
this.invoices = []






      if ($stateParams.alerts) {
        this.alerts.push($stateParams.alerts)
      }
        
      let emailformatId = 7
        //console.log(companyId)
      let EmailFormatData = API.service('show', API.all('email_formats'))
      EmailFormatData.one(emailformatId).get()
        .then((response) => {
          this.emailformateditdata = API.copy(response)
          console.log("b", this.emailformateditdata.data.subject)
          this.emailformateditdata.data.subject = this.emailformateditdata.data.subject.replace('applicant_name', this.customer)

      })

      let Agent =   API.service('policyagent', API.all('policies'))
      Agent.one(policy_id).get()
        .then((response) => {
          this.agentdata = API.copy(response)
          console.log("agente", this.agentdata)
      })


        let diagnostics = API.service('documentdiagnostics', API.all('diagnostics'))
      diagnostics.one(id).get()
        .then((response) => {
          let dataSet = response.plain()
          this.diagnostics = dataSet.data
          console.log("diagnostics", this.diagnostics)
      })

      let invoices = API.service('invoices', API.all('diagnostics'))
        invoices.one(id).get()
        .then((response) => {
          let dataSet = response.plain()
          this.invoices = dataSet.data
          
          angular.forEach(dataSet.data, function(value, index, ar){
            angular.forEach(dataSet.data[index].documents, function(value, index2, ar2){

                  if(dataSet.data[index].documents[index2].id_doc_type != '1'){
                                      ar2.splice(index2,1)
                                    }

            })
          })
          console.log("invoices", this.invoices)
      })



        /*********************************************************/
        this.lines.push({id : 0});
        this.typeDocNew.push({id : 0, value: 1})

        let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            console.log("Tipos", docResponse)
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })
    
       
        this.systemDocTypes = systemDocTypes
        console.log(this.systemDocTypes)

          let systemDescriptionDoc = []
        let systemDescription = []
        let systemDescription2 = []
        let systemDescription3 = []
        let DocDescription = this.API.service('claimdocumenttypedescriptions')
        DocDescription.getList()
        .then((response) => {
            let descriptionResponse = response.plain()
            angular.forEach(descriptionResponse, function(value, index, arr){
                
                if(arr[index].id_doc_type == 2){
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                    systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})

                }else if(arr[index].id_doc_type == 3){
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                    systemDescription2.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})

                }else{
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                    systemDescription3.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})
                }
                
              

            })
            

        })
        
        this.systemDescription = systemDescription
        this.systemDescription2 = systemDescription2
        this.systemDescription3 = systemDescription3
        this.systemDescriptionDoc = systemDescriptionDoc
        
        console.log("Tipo 2",this.systemDescription)
        console.log("Tipo 3",this.systemDescription2)
        console.log("Tipo 4",this.systemDescription3)
        console.log("Tipo Completo",this.systemDescriptionDoc)



        let Policies = API.service('show', API.all('policies'))
        Policies.one(policyId).get()
        .then((response) => {
            this.policy = API.copy(response)
            console.log("r ", this.policy.data)
            this.title = 'Reclamos #'+claimId+' Póliza #'+policyId+' | '+this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
        })

         

        

        let Currencies = API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
            console.log("monedas ", this.systemCurrency)
        })

        let Providers = API.service('providers')
        Providers.getList()
        .then((response) => {
            let systemProviders = []
            let providersResponse = response.plain()
            angular.forEach(providersResponse, function (value) {
                systemProviders.push({id: value.id, name: value.name})
            })

            this.systemProviders = systemProviders
            console.log("systemProviders ", this.systemProviders)
        })

             this.addFile = () => {
          console.log("Entro")
          if (this.lines.length==1){
              var newid = 1
          }
          else{
              var newid = this.lines.length
          }
          this.lines.push({id : newid});

          this.typeDocNew.push({id : newid, value: 1})
      }

      this.changeDocType = (index,type) => {
        this.indice = index
        if (type == 1){
            this.typeDocNew[index].value = this.doc_typeselected[index]
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
                this.curency_selected[index] = 1
                //this.previouslines[index].description = ''
            }
            else {
                this.lines[index].disabled = true
                this.lines[index].rep = 'No '
                this.curency_selected[index] = 1
                this.value[index] = ''
            }
        }
        else{//anteriores
            this.typeDoc[index].value = this.previouslines[index].id_doc_type
            if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
                this.previouslines[index].currency_id = 1
                this.previouslines[index].description = ''
            }else {
                this.previouslines[index].disabled = true
                this.previouslines[index].rep = 'No '
                this.previouslines[index].currency_id = 1
                this.previouslines[index].amount = ''
            }
        }

    }     

        this.save = (isValid) => {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log(isValid)


        if (isValid){
            this.processing = true
            let $state = this.$state
            let uploadFile = this.uploadFile
            let filenames = []
            angular.forEach(this.myFile, function (value) {
                filenames.push(value.name)
            })
            this.filenames = filenames
            
             /***Verifica los tipos de documentos que no sean facturas***/
            for (var i = 0; i < this.description.length; i++) {
                
                if (this.doc_typeselected[i] > 1 ) {
                    
                   this.description[i] = this.systemDescriptionDoc[this.description[i]-1].name    
                    
                    
                }    
            }


            console.log("description", this.description)

            console.log("Reclamo ", this.claim_id, "Diagnostico", this.id)
            let documents = this.API.service('claims2', this.API.all('claims'))
            documents.post({
            'id_claim' : this.claim_id,
            'id_diagnostic' : this.id, 
            'id_doc_types' : this.doc_typeselected,
            'currency_id' : this.curency_selected,
            'provider_id' : this.provider_id,
            'values' : this.value,
            'descriptions' : this.description,
            'filenames' : this.filenames,
            //'modified' : this.previouslines,
            //'deleted_ids' : this.deletedlines,
            //'returned' : this.returnfiles,
            'document_date' : this.doc_date
          }).then(function (response) {
                mvm.uploadFile()
          }, function (response) {
          })
        }else{
            this.formSubmitted = true
        }
    }

    this.uploadFile = () =>{
        let $state = this.$state
        var i = 0
            var files = this.files//archivos nuevos
            //var pfiles = this.pfiles//archivos que sustituiran a los anteriores
            var claimId = this.claim_id
            var diagnosticId = this.diagnostic_id
            var $http = this.$http
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('claim_id', claimId);
                form_data.append('diagnostic_id', diagnosticId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   

                })
            })
            /*
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                form_data.append('claim_id', claimId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })*/
        this.processing = false
        swal({
            title: 'Documentos Guardados Correctamente!',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
        }, function () {
             $state.go($state.current, {}, { reload: $state.current})   
             $uibModalInstance.dismiss('cancel')
             
        })
    }


    this.deleteLine = (index) => {//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
        console.log('providers',this.provider_id)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.curency_selected.splice(pos,1)
        this.provider_id.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)
        this.doc_date.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
        console.log('providers',this.provider_id)
    }


    
    


/***********************************************************/





      function sendmail(answer){
        console.log("id ", id)
        console.log("answer ", answer)
          API.service('send').post({
              policy_id : id,
              format_id : 7,
              answer : answer
          } )
          .then( ( response ) => {
              $state.reload();
          }, function (response) {
                  console.log('error')
              }
          )
          console.log("xxxx")
       }



      $scope.selected = {
        item: items[0]
      }      

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.saveResponse = (answer, id) => {
        var form_data = new FormData();
        let Policies = this.API.service('updatePolicies');
        
        form_data.append('policyId', id);
        form_data.append('customer_response', answer);
        
        let $state = this.$state        
        //Weight.post({
        $http.post('updatePolicies', form_data,
        {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined,'Process-Data': false}
        }).success(function(response){
          $uibModalInstance.dismiss('cancel')
          swal("Éxito!", "Respuesta enviada.");
          sendmail(answer)
          $state.reload()
        })
      }

      this.sendAgent = (answer, id, content, policy_id) => {
        mvm.processing = true
        mvm.sendbutton = false
        console.log("id de Reclamo ", this.claim_id)
        console.log("answer ", answer)
        console.log("content ", content)
        //if (answer == 1) {
          console.log("x el SI")
          API.service('send').post({
              claim_id : this.claim_id,
              policy_id : policy_id,
              format_id : 7,
              answer : 1,
              content : content,
              diagnostics : this.diagnostics

          } )
          .then( ( response ) => {
              mvm.processing = false
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
              $uibModalInstance.dismiss('cancel')
            })
          }, function (response) {
                mvm.processing = false
                console.log('error')
              }
          )
          console.log("xxxx")
       }


this.verifyCheck = function(){
          let check = false

        angular.forEach(this.diagnostics,function(value){
            angular.forEach(value.documents,function(value2){
                if (value2.checked == true)    {
                    check = true
                }
            })
        })
        this.sendbutton = check
      }

    

    this.verifyCheckDoc = function(){
          let check = false

        angular.forEach(this.invoices,function(value){
            angular.forEach(value.documents,function(value2){
                if (value2.return == true)    {
                    check = true
                }
                 console.log(value2.return)
            })
        })
        this.sendbutton = check

       
      }

//************************************************//
this.sendInvoices = (isValid)  => {
  console.log(isValid)
  angular.forEach( this.invoices[0].documents, function (value) {
              console.log(value.return)
            if(value.return == undefined){

                value.return = false              
            }
                
            })
console.log("Facturas", this.invoices[0].documents)

 if (isValid){
            this.processing = true
            //console.log("Reclamo ", this.claim_id, "Diagnostico", this.id)
            let documents = this.API.service('returninvoice', this.API.all('diagnostics'))
            documents.post({
            'documents' : this.invoices[0].documents
          }).then(function (response) {
              mvm.processing = false
            swal({
              title: 'Facturas Devueltas!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
              $uibModalInstance.dismiss('cancel')
            })    
          }, function (response) {
              mvm.processing = false
              console.log('error')

          })
          let claimStep = API.service('claimstep', API.all('claims'))  
            claimStep.post({
                'claimId': this.claim_id,
                'diagnostic_id': this.id,

            }).success(function(response){ 
          })
        }else{
            this.formSubmitted = true
        }
  


}
}
    $onInit(){
    }
}

export const ClaimLiquidationsComponent = {
    templateUrl: './views/app/components/claim-liquidations/claim-liquidations.component.html',
    controller: ClaimLiquidationsController,
    controllerAs: 'vm',
    bindings: {}
}
