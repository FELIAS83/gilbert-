class RenewalDocumentsController{
    constructor(API,$state,$stateParams,$http){
        'ngInject';
        var vm = this
        this.API = API
        var $state = $state
        this.$state = $state
        this.current_step = '7'
        this.lines = []
        this.alerts = []
        this.lines.push({id : 0});
        this.lines.push({id : 1});
        this.newlines = []
        this.deletedlines = []
        this.files = []
        this.files2 = []
        this.myFile = [] 
        this.filenames = []
        this.doc_typeselected = []
        this.uploadFile = uploadFile
        let policyId = $stateParams.policyId
        this.policyId = policyId
        let feeId = $stateParams.feeId
        this.feeId = feeId
        this.uisrefparameter = '{policyId: vm.policyId}'
        this.uisrefback = 'app.renewal-lists'
        this.completed = $stateParams.completed

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        if (this.completed  == 1){
            this.uisrefparameter = ' {policyId: vm.policyId, completed: 1}'
            this.uisrefback = 'app.renewal-dependents-data({policyId: vm.policyId, completed: 1})'
        }

       let Wizar = API.service('wizar')
        Wizar.getList().then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            angular.forEach(wizarResponse, function (value) {
                systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})
            })

           systemWizar.splice(3,1) 
           this.systemWizar = systemWizar
        })
                
        //get policy files
        let RenewalfileData = API.service('show', API.all('renewaldocuments'))
        RenewalfileData.one(policyId).get()
        .then((response) => {
            let dataSet
            dataSet = response.plain()
            this.lines = dataSet.data
            console.log("this.lines",this.lines)
        })
        
        let DocTypes = this.API.service('doctypes')
        DocTypes.getList()
        .then((response) => {
            let systemDocTypes = []
            let docTypesResponse = response.plain()
            angular.forEach(docTypesResponse, function (value) {
                systemDocTypes.push({id: value.id, name: value.name})
            })
            this.systemDocTypes = systemDocTypes
        })
        

        function uploadFile(){
            var i = 0
            var files = vm.files//archivos nuevos
            var files2 = vm.files2//archivos que sustituiran a los anteriores
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('policy_id', vm.policyId);
                $http.post('uploadrenewal', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    /*if (files2.length == 0){
                        send()
                    }*/
                })
            })
            angular.forEach(files2, function(files2){
                var form_data = new FormData();
                angular.forEach(files2, function(file){
                    form_data.append('file', file);
                })
                form_data.append('policy_id', vm.policyId);
                $http.post('uploadrenewal', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    //send()
                })
            })
            /*if (files.length == 0 && files2.length == 0){
                send()
            }*/
        }
        function send(){
            API.service('send').post({
                policy_id : vm.policyId,                
                format_id : 1,
                type: 'renewal'
            })

        }
    }
    deleteLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id, table: index.table})
        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
    }
    addNewLine(){//agregar un nuevo documento
        if (this.newlines.length==0){
            var newid = 0
        }
        else{
            var newid = this.newlines.length
        }
        this.newlines.push({id : newid});  
    }
    deleteNewLine(index){//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log(this.newlines)
        console.log("doc type", this.doc_typeselected)
        console.log(this.description)
        console.log(this.files)


        let pos = this.newlines.indexOf(index);
        this.newlines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.files.splice(pos,1)

        let newvalue = -1
        let nelines = []
        angular.forEach(this.newlines, function (value) {
            nelines.push({id : newvalue+1})
        })
        this.newlines = nelines
    }

    save(isValid){
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        let filenames = []
        angular.forEach(this.myFile, function (value) {
            filenames.push(value.name)
        })
        this.filenames = filenames
        if (this.files2){
        for (var i = 0; i < this.lines.length; i++) {
            if (this.files2[i]) {
                this.lines[i].filename = this.files2[i][0].name
            }
        }}
        
        if (isValid){
            this.isDisabled = true;
            let $state = this.$state
            let API = this.API
            let policyId = this.policyId
            let uploadFile = this.uploadFile
            
            console.log("Documentos", this.doc_typeselected)
            console.log("Descripciones", this.description)
            console.log("Archivos", this.filenames)
            console.log("eliminados", this.deletedlines)

            let documents = API.service('documents', this.API.all('renewaldocuments'))
            documents.post({
            'policy_id' : policyId,
            'ids' : this.deletedlines,
            'id_doc_types' : this.doc_typeselected,
            'descriptions' : this.description,
            'filenames' : this.filenames,
            'modified' : this.lines,
            'feeId' : this.feeId
          }).then(function (response) {
                swal({
                      title: 'Documentos guardados correctamente!',
                      type: 'success',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                  }, function () {
                    $state.go('app.renewal-lists', {})
                      })
                    //console.log($state.current);
                }, function (response) {
                    swal({
                      title: 'Error:' + response.message,
                      type: 'error',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                    }, function () {
                          $state.go($state.current)
                    })
            })
          uploadFile()      
        } 
    }

    $onInit(){
    }
}

export const RenewalDocumentsComponent = {
    templateUrl: './views/app/components/renewal-documents/renewal-documents.component.html',
    controller: RenewalDocumentsController,
    controllerAs: 'vm',
    bindings: {}
}
