class AgentCommissionAddController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getBranches = getBranches
        vm.getPlans = getPlans
        var allBranches = []
        var allPlans = []
        var branch = ""
        var provider = ""
        vm.processing = true
        vm.message = "Guardar"
        this.provider_button = true
        this.branch_button = false
        this.branch_list = false
        this.plan_button = false
        this.branch_button2 = false
        this.plan_button2 = false


          if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
         let agentId = $stateParams.agentId
        this.agentId =  agentId
        console.log("agentId", agentId)

        let AgentData = API.service('show', API.all('agents'))
        AgentData.one(agentId).get()
        .then((response) => {
             this.dataagent = response.plain()

        })  

        let ProviderData = API.service('companies')
        ProviderData.getList({})
        .then((response) =>{

             let dataProvider = response.plain()
             console.log(dataProvider)
             let systemProviders = []
             angular.forEach(dataProvider, function(value){
                systemProviders.push({id:value.id, name:value.name})
             })
             this.systemProviders = systemProviders 
        } )

        let BranchesData = API.service('branchs')
        BranchesData.getList({})
        .then((response) =>{
            let dataBranches = response.plain()
            let systemBranches = []
            console.log("dataBranches",dataBranches)
            
            angular.forEach(dataBranches, function(value){
                if(value.company_id == provider){
                    systemBranches.push({id:value.id, name:value.name, providerId:value.company_id})
                }
                allBranches.push({id:value.id, name:value.name, providerId:value.company_id})
             })
            this.systemBranches = systemBranches
            this.allBranches =  allBranches
            //console.log("this.allBranches",this.allBranches)
        })


        let PlansData = API.service('plans')
        PlansData.getList({})
        .then((response) =>{
            let dataPlans =  response.plain()
            let systemPlans = []
            console.log("dataPlans", dataPlans)
             angular.forEach(dataPlans, function(value){
                if(value.branch_id == branch){
                    systemPlans.push({id:value.id, name:value.name, branchId:value.branch_id})
                }
                allPlans.push({id:value.id, name:value.name, branchId:value.branch_id})
             })
             this.systemPlans = systemPlans
             this.allPlans =  allPlans

        })

        function getBranches(){
            console.log("Entro..", vm.provider)
            this.rowStatus = false
            var provider = vm.provider
            let systemBranches = []
       
            angular.forEach(this.allBranches, function (value) {
                if(value.providerId == provider){
                    systemBranches.push({id: value.id, name: value.name, providerId:value.providerId })
                }
            })

            if(vm.provider != ""){
                console.log("entro false")
                vm.processing = false
            }else{
                console.log("entro true")
                vm.processing = true
                vm.branch_commission = 0
                vm.branch_commission_ren = 0
                vm.plan_commission = 0
                vm.plan_commission_ren = 0
            }

            this.systemBranches = systemBranches
            //this.systemPlans = ""
            }

        function getPlans(){

            console.log("Entro..Branch", vm.branch)
            this.rowStatus = false;
            var branch = vm.branch
            let systemPlans = []
            //this.plans = 0
            this.plan_commission = 0
            this.plan_commission_ren = 0
       
         angular.forEach(this.allPlans, function (value) {
              if(value.branchId == branch){
                systemPlans.push({id: value.id, name: value.name, branchId:value.branchId })
            }
            })
            this.systemPlans = systemPlans  
        
        }

       
    }
    save(isValid){

      var exist = true 
        if(isValid){
            let $state = this.$state
            this.processing = true
            this.message = "Guardando"
            console.log("Comision Prov", this.provider_commission)
            console.log("Comision Prov ren", this.provider_commission_ren)
            console.log("Comision Ramo", this.branch_commission)
            console.log("Comision Ramo ren", this.branch_commission_ren)
            console.log("Comision Plan", this.plan_commission)
            console.log("Comision Plan Ren", this.plan_commission_ren)
            this.branch_commission = this.branch_commission ==  undefined ? 0 : this.branch_commission
            this.branch_commission_ren = this.branch_commission_ren ==  undefined ? 0 : this.branch_commission_ren
            this.plan_commission = this.plan_commission == undefined ? 0 : this.plan_commission
            this.plan_commission_ren = this.plan_commission_ren == undefined ? 0 : this.plan_commission_ren


            this.commission = this.provider_commission + this.branch_commission + this.plan_commission
            this.commission_ren = this.provider_commission_ren + this.branch_commission_ren + this.plan_commission_ren
            console.log("valor de commission", this.commission)
            console.log("valor de commission ren", this.commission_ren)
            if(this.branch ==  undefined || this.branch == ""){
              this.branch = 0
            }

            if(this.plans ==  undefined || this.plans == ""){
              this.plans = 0
            }
            
           var verify = this.API.all('verifycommission/'+this.provider+'/'+this.branch+'/'+this.plans+'/'+this.agentId+'/'+this.commission+'/'+this.commission_ren)
           verify.doGET('',{}).then((response) => {
              let dataSet = response.plain()
              exist = dataSet.data
              console.log("exist",exist)
              if(exist.response){
                this.processing = false
                this.message = "Guardar"
                swal({
                  title: 'Importante!',
                  text: exist.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true,
                  timer: 3000
                }, function () {   
                    $state.go($state.current, {})
                })    

              }else{
                let Commission = this.API.service('commission', this.API.all('commissions'))
            Commission.post({
                'agentId': this.agentId,
                'provider': this.provider,
                'branch': this.branch,
                'plans' : this.plans,
                'commissions': this.commission,
                'commission_ren': this.commission_ren,
            })
            .then((response) => {
                swal({
                  title: 'Comisión Guardada Correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                }, function () {
                      $state.go('app.agent-commission-list',{"agentId": response.data})
                })    
            
            },(response) => {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                      $state.go($state.current)
                  })
              })
              }
              

           })
           /* 
            let Commission = this.API.service('commission', this.API.all('commissions'))
            Commission.post({
                'agentId': this.agentId,
                'provider': this.provider,
                'provider_commission': this.provider_commission,
                'branch': this.branch,
                'branch_commision': this.branch_commission,
                'plans' : this.plans,
                'plan_commission': this.plan_commission
            })
            .then((response) => {
                swal({
                  title: 'Comisiones Guardadas Correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                }, function () {
                      $state.go('app.agent-commission-list',{"agentId": response.data})
                })    
            
            },(response) => {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                      $state.go($state.current)
                  })
              })
            */

        }

    }

    
    showBranches(){
      console.log("entro")

        this.provider_button = false
        this.branch_list = true
        this.branch_button = true
        this.branch_button2 = false
        this.provider_commission = 0
        this.provider_commission_ren = 0

    }
    showPlans(){
      console.log("entro plan")
      this.branch_button = false
      this.branch_list = true
      this.plan_button = true
      this.branch_button2 = false
      this.plan_button2 = true
      this.provider_commission = 0
      this.provider_commission_ren = 0
      this.branch_commission = 0
      this.branch_commission_ren = 0

    }

    hidePlans(){
      this.plan_button = false
      this.plan_button2 = false      
      this.branch_button2 = true
      this.branch_list = true
      this.branch_button = true
      this.plans = 0


    }

    hideBranchs(){
      this.branch_button = false
      this.branch_button2 = false
      this.branch_list = false
      this.provider_button = true
      this.branch = 0
      this.plans = 0
    }
    
    
    $onInit(){
    }
}

export const AgentCommissionAddComponent = {
    templateUrl: './views/app/components/agent-commission-add/agent-commission-add.component.html',
    controller: AgentCommissionAddController,
    controllerAs: 'vm',
    bindings: {}
}
