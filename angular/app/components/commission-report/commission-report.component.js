class CommissionReportController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

    var vm = this
    this.API = API
    this.$state = $state
    this.formSubmitted = false
    this.can = AclService.can
    this.DTOptionsBuilder = DTOptionsBuilder
    this.DTColumnBuilder = DTColumnBuilder

    vm.isDisabled = false
    vm.showMessage = false
    vm.alert = ""
    vm.type = 1
    vm.compareDate = compareDate
    this.systemReport = []
    this.systemReport = [{id:1, name: "Por Agentes"}, {id:2, name: "Por Planes"}, {id:3, name: "Por Fecha de Ventas"}]
    var fecha=""
    var arrfecha=""

       let Agents = API.service('agents')
        Agents.getList()
          .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            console.log("Agentes", agentsResponse)
            angular.forEach(agentsResponse, function (value) {
              systemAgents.push({id: value.id, name: value.first_name+' '+value.last_name })
            })

            this.systemAgents = systemAgents
            //console.log("systemCountrys==> ", systemCountrys)
        })

        let Plans = this.API.service('plans')
        Plans.getList()
          .then((response) => {
            let systemPlans = []
            let plansResponse = response.plain()
            console.log("Plans", plansResponse)
            angular.forEach(plansResponse, function (value) {
              systemPlans.push({id: value.id, name: value.name})
            })

            this.systemPlans = systemPlans
            //console.log("systemCountrys==> ", systemCountrys)
        })

          /*Reporte inicial*/
          let ReportData = this.API.all('getReport')
           ReportData.post({
            'agent_id' : this.agent_id,
            'plan_id' : this.plan_id,
            'start_date' : this.start_date,
            'end_date' : this.end_date

           })
          .then((response) => {
            this.reportdata = response.plain(response)
            console.log("reportdata", this.reportdata)
          
              angular.forEach(this.reportdata.data, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("-")
              var day=arrfecha[2]
              var month = arrfecha[1]
              var year= arrfecha[0]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

           
            })
              console.log("fecha",fecha)
            
            
               
            this.dtOptionsC = this.DTOptionsBuilder.newOptions()
              .withOption('data', this.reportdata.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withOption('paging', false)
              .withOption('searching', false)
              .withOption('info', false)
              .withBootstrap()
                this.showTable = 1
                this.dtColumnsC = [
              this.DTColumnBuilder.newColumn('id').withTitle('ID'),
              this.DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              this.DTColumnBuilder.newColumn('applicant').withTitle('Asegurado'),
              this.DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              this.DTColumnBuilder.newColumn('name').withTitle('Plan'),
              this.DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura'),
              this.DTColumnBuilder.newColumn('commision').withTitle('% Comisión'),
              this.DTColumnBuilder.newColumn('amount').withTitle('Monto'),              
              ]          
              this.displayTableC = true  

             
             let createdRow = (row) => {
             $compile(angular.element(row).contents())($scope)
             }

            })
          /*****************/

          function compareDate(){


            if(this.start_date != null &&  this.end_date != null){
                        let startdate = new Date(this.start_date)
                        let finaldate = new Date(this.end_date)
                       
                        if(startdate <= finaldate){
                             vm.isDisabled = 0 
                             vm.alert1 = ""
                             vm.alert2 = ""
                             vm.showMessage = false
                    
            
                        }else{

                             vm.isDisabled = 1
                             vm.alert1 = "La fecha Inicial debe ser menor a la Final"
                             vm.alert2 = ""
                             vm.showMessage = true
                    
              
                        }
               }else{
                        vm.isDisabled = 0
                        vm.alert1 = ""
                        vm.alert2 = ""
                        vm.showMessage = false
               }
          }

    
          
        
        //
    }

    getReport(){
          //var vm = this
          //let changeType = vm.changeType
         var fecha=""
         var arrfecha="" 
         if(this.verifiedDate()){
          let ReportData = this.API.all('getReport')
           ReportData.post({
            'agent_id' : this.agent_id,
            'plan_id' : this.plan_id,
            'start_date' : this.start_date,
            'end_date' : this.end_date

           })
          .then((response) => {
            this.reportdata = response.plain(response)
            console.log("reportdata", this.reportdata)
         
               angular.forEach(this.reportdata.data, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("-")
              var day=arrfecha[2]
              var month = arrfecha[1]
              var year= arrfecha[0]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

           
            })
              console.log("fecha",fecha)
         

            
               
            this.dtOptionsC = this.DTOptionsBuilder.newOptions()
              .withOption('data', this.reportdata.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withOption('paging', false)
              .withOption('searching', false)
              .withOption('info', false)
              .withBootstrap()
                this.showTable = 1
                this.dtColumnsC = [
              this.DTColumnBuilder.newColumn('id').withTitle('ID'),
              this.DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              this.DTColumnBuilder.newColumn('applicant').withTitle('Asegurado'),
              this.DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              this.DTColumnBuilder.newColumn('name').withTitle('Plan'),
              this.DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura'),
              this.DTColumnBuilder.newColumn('commision').withTitle('% Comisión'),
              this.DTColumnBuilder.newColumn('amount').withTitle('Monto'),              
              ]          
              this.displayTableC = true  

             
             let createdRow = (row) => {
             $compile(angular.element(row).contents())($scope)
             }

            })
        }else{
          console.log("nada")
        }
    
        
    }
    verifiedDate(){
             console.log("Entro")
            if(this.start_date == null &&  this.end_date != null){

                this.isDisabled = 1
                this.alert1 = "Campo requerido"
                this.alert2 = ""
                this.showMessage = true
                return false


            }else if(this.start_date != null &&  this.end_date == null){

                this.isDisabled = 1
                this.alert1 = ""
                this.alert2 = "Campo requerido"
                this.showMessage = true
                return false

            }else if(this.start_date != null &&  this.end_date != null){

                this.isDisabled = 0
                this.alert1 = ""
                this.alert2 = ""
                this.showMessage = false
                return true

            }else{
                this.isDisabled = 0
                this.alert1 = ""
                this.alert2 = ""
                this.showMessage = false
                return true

            }
            
          }

    $onInit(){
    }
}

export const CommissionReportComponent = {
    templateUrl: './views/app/components/commission-report/commission-report.component.html',
    controller: CommissionReportController,
    controllerAs: 'vm',
    bindings: {}
}
