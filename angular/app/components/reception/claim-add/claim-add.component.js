class ClaimAddController {
	
  constructor (API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.service('policy')
      let $state = this.$state

      apiData.post({
        'name': this.name
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, {alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, {alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const ClaimAddComponent = {
  templateUrl: './views/app/components/reception/claim-add/claim-add.component.html',
  controller: ClaimAddController,
  controllerAs: 'vm',
  bindings: {}
}
