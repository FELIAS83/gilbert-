class PolicyAddController {
	
  constructor (API, $state, $stateParams, Upload) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
		this.Upload = Upload;
    this.alerts = []

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }
  }
	
  $onInit () {
		//this.file = '';
	}
	
	uploadFiles(file, errFiles) {
		this.f = file;
		this.errFile = errFiles && errFiles[0];
		if (file) {
			file.upload = this.Upload.upload({
				url: '/upload',
				data: {file: file}
			});

			file.upload.then(function (response) {
				$timeout(function () {
					file.result = response.data;
				});
			}, function (response) {
				if (response.status > 0) {
					this.errorMsg = response.status + ': ' + response.data;
				}
			}, function (evt) {
				file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
		}
	}
	
  save (isValid) {
    this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      let apiData = this.API.all('reception/policy')
      //this.API.service('reception/policy')
			//this.API.all('auth/password').get('verify', {email, token}).then(() => {
      let $state = this.$state

      apiData.post({
        'seller_id': this.seller_id,
				'plan_type_id': this.plan_type_id,
				'first_name': this.first_name,
				'last_name': this.last_name,
				'phone': this.phone,
				'mobile': this.mobile,
				'email': this.email
      }).then(function () {
        let alert = { type: 'success', 'title': 'Listo!', msg: 'El registro ha sido agregado.' }
        $state.go($state.current, {alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, {alerts: alert})
      })
    } else {
      this.formSubmitted = true
    }
  }
	
}

export const PolicyAddComponent = {
  templateUrl: './views/app/components/reception/policy-add/policy-add.component.html',
  controller: PolicyAddController,
  controllerAs: 'vm',
  bindings: {}
}
