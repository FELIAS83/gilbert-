class MailFormatEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

         this.$state = $state
        this.formSubmitted = false
        this.alerts = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let mailformatId = $stateParams.mailformatId
        //console.log(companyId)
        let MailFormatData = API.service('show', API.all('mail_formats'))
        MailFormatData.one(mailformatId).get()
          .then((response) => {
            this.mailformateditdata = API.copy(response)
         //   this.mailformateditdata.data.commission_percentage = parseFloat(this.companyeditdata.data.commission_percentage, 2)
          //console.log("data=>", this.companyeditdata);
        })
    }

    save (isValid) {
        if (isValid) {
          console.log(this.mailformateditdata);
          let $state = this.$state
          this.mailformateditdata.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el Formato de correo.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const MailFormatEditComponent = {
    templateUrl: './views/app/components/mail-format-edit/mail-format-edit.component.html',
    controller: MailFormatEditController,
    controllerAs: 'vm',
    bindings: {}
}
