class CompanyAddController{
    constructor ($scope, API, $state, $stateParams) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        

    }

    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this      
          let Companies = this.API.service('companies', this.API.all('companies'))
          let $state = this.$state          
          
          Companies.post({
            'name': this.name,
            //'type': this.type,
            'commission_percentage': this.commission_percentage
          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido la compañía.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const CompanyAddComponent = {
    templateUrl: './views/app/components/company-add/company-add.component.html',
    controller: CompanyAddController,
    controllerAs: 'vm',
    bindings: {}
}
