class NavHeaderController {
  constructor ($rootScope, ContextService, $scope) {
    'ngInject'

    let navHeader = this

    ContextService.me(function (data) {
      navHeader.userData = data
    })

    $scope.$on('save', function(event, data){
      //console.log(data)
      navHeader.userData = data.data
    })

  }

  $onInit () {}
}

export const NavHeaderComponent = {
  templateUrl: './views/app/components/nav-header/nav-header.component.html',
  controller: NavHeaderController,
  controllerAs: 'vm',
  bindings: {}
}
