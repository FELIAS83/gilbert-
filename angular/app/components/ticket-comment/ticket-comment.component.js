class TicketCommentController{
    constructor($stateParams, API, DTOptionsBuilder, DTColumnBuilder, $compile, $scope, $state){
        'ngInject';

        this.API = API
        this.active_tab = 5
        let policyId = $stateParams.policyId
        this.applicant_id= policyId
        this.applicant_data = []
        this.applicant_data.comments = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        } 

        console.log("policyId",policyId)

        let applicant_data = API.one('applicant_data', this.applicant_id)
        applicant_data.get().then((response) => {
            this.applicant_data = response.plain().data
            console.log("applicant_data", this.applicant_data)
        })


    }
    setActive(tab){
        this.active_tab = tab
    }
    addComment(){
        if (this.comment != ''){
            var vm = this
            let comment = this.API.service('comments', this.API.all('applicants'))
            comment.post({
                'module' : 0,
                'key_field' : this.applicant_id,
                'text' : this.comment
            }).then(function () {
                vm.comment = ''
                vm.update()
          }, function (response) {
          })
        }
    }
    deleteComment(commentId) {
        this.API.one('comments').one('comments', commentId).remove()
        .then(() => {
            this.update()
        })
    }
    update(){
        let comments = this.API.service('show', this.API.all('comments'))
        comments.one(this.applicant_id).get()
        .then((response) => {
            this.applicant_data.comments = this.API.copy(response).data
        })
    }

    $onInit(){
    }
}

export const TicketCommentComponent = {
    templateUrl: './views/app/components/ticket-comment/ticket-comment.component.html',
    controller: TicketCommentController,
    controllerAs: 'vm',
    bindings: {}
}
