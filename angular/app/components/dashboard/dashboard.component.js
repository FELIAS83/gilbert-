class DashboardController {
  constructor ($scope,$state, $compile, API, $stateParams, $http,$filter) {
    'ngInject'
    var vm = this
    this.API = API
    this.$http= $http
    this.$scope=$scope
    this.array_month=[]
    this.month_number=[0,0,0,0,0,0]
    this.new_sales=""
    this.new_sales_money=""
    this.graces_month=""
    var date_today = new Date()
    this.date= new Date()
    this.end_date = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0)
    var date_input= date_today
    this.start_date= new Date (date_input.setMonth(date_input.getMonth()-5))
    this.start_date = new Date(this.start_date.setDate('01'))
    console.log("start_date",this.start_date)
    console.log("end_date",this.end_date)
    this.month = this.date.getMonth()+1
    this.end_month = this.month
    //console.log("end_month",this.end_month)
    //console.log("month",this.month)
    this.year = this.date.getFullYear()
    //console.log("year",this.year)
    this.showMessage = false
    //this.compareDate = compareDate
    this.length=5
    this.isDisabled = false

    var m =this.month
    for (var i = 5 ; i >= 0 ; i--) {
        this.month_number[i]= m
        m--         
    }
    
    for (var i = 0; i <= this.length; i++) {

      switch (this.month_number[i])
      {
        case 1 :  this.array_month[i] = 'Enero'+' '+this.year;
        break;
        case 2 :  this.array_month[i] = 'Febrero'+' '+this.year;
        break;
        case 3 :  this.array_month[i] = 'Marzo'+' '+this.year;
        break;
        case 4 :  this.array_month[i] = 'Abril'+' '+this.year;
        break;
        case 5 :  this.array_month[i] = 'Mayo'+' '+this.year;
        break;
        case 6 :  this.array_month[i] = 'Junio'+' '+this.year;
        break;
        case 7 :  this.array_month[i] = 'Julio'+' '+this.year;
        break;
        case 8 :  this.array_month[i] = 'Agosto'+' '+this.year;
        break;
        case 9 :  this.array_month[i] = 'Septiembre'+' '+this.year;
        break;
        case 10 : this.array_month[i] = 'Octubre'+' '+this.year;
        break;
        case 11 : this.array_month[i] = 'Noviembre'+' '+this.year;
        break;
        case 12 : this.array_month[i] = 'Diciembre'+' '+this.year;
        break;
      }
    }
    
    //console.log("array_month",this.array_month)
    console.log("month_number",this.month_number)


    let Salespermonth = this.API.service('salespermonth', this.API.all('dashboard'))
        
    Salespermonth.one().get({'completed': 1}).then((response) => {
      let Salespermonthdata = API.copy(response)
        
      console.log("Salespermonthdata",Salespermonthdata.data)
      //console.log("long",Salespermonthdata.data.policies.length)

      //console.log("num_sales",Salespermonthdata.data.policies[1].num_sales)
      this.data_number=[0,0,0,0,0,0,0,0,0,0,0,0]
      
      
      var empty_months = 6 - Salespermonthdata.data.policies.length
      var date = new Date()
      var month = date.getMonth()+1
      var start_month = month - 5
      //console.log("month",month) 
      //console.log("start_month",start_month) 
      //console.log("empty_months",empty_months)


        for (var i = 0; i < Salespermonthdata.data.policies.length ; i++) {
          
          switch (Salespermonthdata.data.policies[i].month)
          {
            case 1 : this.data_number[0] = Salespermonthdata.data.policies[i].num_sales
            break; 
            case 2 : this.data_number[1] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 3 : this.data_number[2] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 4 : this.data_number[3] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 5 : this.data_number[4] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 6 : this.data_number[5] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 7 : this.data_number[6] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 8 : this.data_number[7] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 9 : this.data_number[8] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 10 : this.data_number[9] = Salespermonthdata.data.policies[i].num_sales
            break;
            case 11 : this.data_number[10] = Salespermonthdata.data.policiess[i].num_sales
            break;
            case 12 : this.data_number[11] = Salespermonthdata.data.policiess[i].num_sales
            break;

          }
        }
      
      this.data_number= this.data_number.slice(start_month-1, month)
        
      console.log("data_number", this.data_number)
      this.new_sales = this.data_number[5]
      this.total_sales = 0
      
      for (var i = 0; i < this.data_number.length; i++) {
        this.total_sales= this.total_sales+ parseInt(this.data_number[i])
      }
    

      var longitud= Salespermonthdata.data.policies.length

    $scope.data= [this.data_number]        
    //this.array_month=array_month
    console.log("total_sales", this.total_sales)          
    //console.log("month_number", this.month_number) 
    //console.log("array_month",this.array_month) 
    //console.log("data_number",$scope.data)
    
    })
              
    let Totalpermonth = this.API.service('dollarspermonth', this.API.all('dashboard'))
      
   // data.
      
    Totalpermonth.one().get({'completed': 1}).then((response) => {
        let Totalpermonthdata = response.plain()
        console.log("Totalpermonthdata",Totalpermonthdata.data)

        this.data_money=[0,0,0,0,0,0,0,0,0,0,0,0]
        var empty_months = 6 - Totalpermonthdata.data.policies.length

        var date = new Date()
        var month = date.getMonth()+1
        var start_month = month - 5
        //console.log("mes",month) 


        for (var i = 0; i < Totalpermonthdata.data.policies.length ; i++) {
          
          switch (Totalpermonthdata.data.policies[i].month)
          {
            case 1 : this.data_money[0] = Totalpermonthdata.data.policies[i].total_money
            break; 
            case 2 : this.data_money[1] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 3 : this.data_money[2] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 4 : this.data_money[3] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 5 : this.data_money[4] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 6 : this.data_money[5] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 7 : this.data_money[6] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 8 : this.data_money[7] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 9 : this.data_money[8] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 10 : this.data_money[9] = Totalpermonthdata.data.policies[i].total_money
            break;
            case 11 : this.data_money[10] = Totalpermonthdata.data.policiess[i].total_money
            break;
            case 12 : this.data_money[11] = Totalpermonthdata.data.policiess[i].total_money
            break;

          }
        }
      
      this.data_money= this.data_money.slice(start_month-1, month)  
        
        console.log("data_money", this.data_money)
        this.new_sales_money = this.data_money[5]
        this.total_money = 0
        for (var i = 0; i < this.data_money.length; i++) {
          this.total_money= this.total_money+parseFloat(this.data_money[i],2)
          this.total_money= parseFloat(this.total_money.toFixed(2))
        }
        console.log("total_money",this.total_money) 

      $scope.moneyData= [this.data_money]
    })    

    let CanceledPolicies = this.API.service('canceledpolicies', this.API.all('dashboard'))
    CanceledPolicies.one().get({'completed': 1}).then((response) => {
      let CanceledPoliciesdata = response.plain()
      console.log("CanceledPoliciesdata",CanceledPoliciesdata.data)    
      this.data_canceled=[0,0,0,0,0,0,0,0,0,0,0,0]
      //var empty_months = 6 - CanceledPoliciesdata.data.policies.length
            
      var date = new Date()
      var month = date.getMonth()+1
      var start_month = month - 5
      //console.log("mes",month) 
      //console.log("start_month",start_month) 

      for (var i = 0; i < CanceledPoliciesdata.data.policies.length ; i++) {
         
          switch (CanceledPoliciesdata.data.policies[i].month)
          {
            case 1 : this.data_canceled[0] = CanceledPoliciesdata.data.policies[i].canceled
            break; 
            case 2 : this.data_canceled[1] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 3 : this.data_canceled[2] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 4 : this.data_canceled[3] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 5 : this.data_canceled[4] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 6 : this.data_canceled[5] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 7 : this.data_canceled[6] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 8 : this.data_canceled[7] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 9 : this.data_canceled[8] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 10 : this.data_canceled[9] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 11 : this.data_canceled[10] = CanceledPoliciesdata.data.policiess[i].canceled
            break;
            case 12 : this.data_canceled[11] = CanceledPoliciesdata.data.policiess[i].canceled
            break;

          }
        }
      
      this.data_canceled= this.data_canceled.slice(start_month-1, month)   
      console.log("data_canceled", this.data_canceled)
      
      this.total_canceled = 0
      for (var i = 0; i < this.data_canceled.length; i++) {
        this.total_canceled= this.total_canceled + parseInt(this.data_canceled[i])
      }
      
      $scope.cancelData = [this.data_canceled]
      //console.log("cancelData",$scope.cancelData)
    })//QuantityPolicies
       
    let GracePolicies = this.API.service('gracepolicies', this.API.all('dashboard'))
    GracePolicies.one().get({'completed': 1}).then((response) => {
      let GracePoliciesdata = response.plain()
      console.log("GracePlociesdata",GracePoliciesdata.data)    
      this.data_grace=[0,0,0,0,0,0,0,0,0,0,0,0]
             
      var date = new Date()
      var month = date.getMonth()+1
      var start_month = month - 5
      //console.log("month_grace",month) 
      //console.log("start_month",start_month) 
  
      for (var i = 0; i < GracePoliciesdata.data.policies.length ; i++) {
    
          switch (GracePoliciesdata.data.policies[i].month)
          {
            case 1 : this.data_grace[0] = GracePoliciesdata.data.policies[i].num_grace
            break; 
            case 2 : this.data_grace[1] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 3 : this.data_grace[2] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 4 : this.data_grace[3] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 5 : this.data_grace[4] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 6 : this.data_grace[5] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 7 : this.data_grace[6] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 8 : this.data_grace[7] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 9 : this.data_grace[8] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 10 : this.data_grace[9] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 11 : this.data_grace[10] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 12 : this.data_grace[11] = GracePoliciesdata.data.policies[i].num_grace
            break;

          }
        }
      
      this.data_grace= this.data_grace.slice(start_month-1, month)
      console.log("data_grace", this.data_grace)
      this.graces_month = this.data_grace[5]
      //console.log("this.data_grace",this.data_grace[5])  
      this.total_grace = 0
      for (var i = 0; i < this.data_grace.length; i++) {
        this.total_grace= this.total_grace+ parseInt(this.data_grace[i])
      }       
      
   
    $scope.graceData = [this.data_grace]
    
    //console.log("graceData",$scope.graceData)
      
    })//GracePolicies

    let Claimspermonth = this.API.service('claimspermonth', this.API.all('dashboard'))
        
    Claimspermonth.one().get().then((response) => {
      let Claimspermonthdata = API.copy(response)
        
      console.log("Claimspermonthdata",Claimspermonthdata.data)
      console.log("long",Claimspermonthdata.data.claims.length)
      //console.log("num_sales",Salespermonthdata.data.policies[1].num_sales)
      this.data_claims=[0,0,0,0,0,0,0,0,0,0,0,0]
      
      var date = new Date()
      var month = date.getMonth()+1
      var start_month = month - 5
      console.log("month",month) 
      //console.log("start_month",start_month) 
      
        for (var i = 0; i < Claimspermonthdata.data.claims.length ; i++) {

          switch (Claimspermonthdata.data.claims[i].month)
          {
            case 1 : this.data_claims[0] = Claimspermonthdata.data.claims[i].num_claims
            break; 
            case 2 : this.data_claims[1] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 3 : this.data_claims[2] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 4 : this.data_claims[3] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 5 : this.data_claims[4] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 6 : this.data_claims[5] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 7 : this.data_claims[6] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 8 : this.data_claims[7] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 9 : this.data_claims[8] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 10 : this.data_claims[9] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 11 : this.data_claims[10] = Claimspermonthdata.data.claims[i].num_claims
            break;
            case 12 : this.data_claims[11] = Claimspermonthdata.data.claims[i].num_claims
            break;

          }
        }
      
      this.data_claims= this.data_claims.slice(start_month-1, month)
        
      console.log("data_claims", this.data_claims)
      this.claims_payed = this.data_claims[5]
      this.total_claims = 0
      
      for (var i = 0; i < this.data_claims.length; i++) {
        this.total_claims= this.total_claims+ parseInt(this.data_claims[i])
      }
    

      var longitud= Claimspermonthdata.data.claims.length

    $scope.data_claims= [this.data_claims]        
    //this.array_month=array_month
    console.log("total_claims", this.total_claims)          
    //console.log("month_number", this.month_number) 
    //console.log("array_month",this.array_month) 
    //console.log("data_number",$scope.data)
    
    })

    $scope.onClick = function () {}
    $scope.labels = this.array_month
    //console.log("scope.labels",$scope.labels)
    $scope.series = ['Series A']
    $scope.year = this.year
    $scope.pieLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales']
    $scope.pieData = [300, 500, 100]
  }
    compareDate(){
      var vm = this

      if(this.start_date != null &&  this.end_date != null){
        let startdate = new Date(this.start_date)
        let finaldate = new Date(this.end_date)
                 
      if(startdate <= finaldate){
           vm.isDisabled = 0 
           vm.alert1 = ""
           vm.alert2 = ""
           vm.showMessage = false

      }else{

           vm.isDisabled = 1
           vm.alert1 = "La fecha Inicial debe ser menor a la Final"
           vm.alert2 = ""
           vm.showMessage = true

      }
      }else{
              vm.isDisabled = 0
              vm.alert1 = ""
              vm.alert2 = ""
              vm.showMessage = false
      }
  }    
    verifiedDate(){

        if(this.start_date == null &&  this.end_date != null){

            this.isDisabled = 1
            this.alert1 = "requerido"
            this.alert2 = ""
            this.showMessage = true
            return false


        }else if(this.start_date != null &&  this.end_date == null){

            this.isDisabled = 1
            this.alert1 = ""
            this.alert2 = "requerido"
            this.showMessage = true
            return false

        }else if(this.start_date == null &&  this.end_date == null){

            this.isDisabled = 0
            this.alert1 = "requerido"
            this.alert2 = "requerido"
            this.showMessage = true
            return false

        }else if(this.start_date != null &&  this.end_date != null){

            this.isDisabled = 0
            this.alert1 = ""
            this.alert2 = ""
            this.showMessage = false
            return true

        }else{
            this.isDisabled = 0
            this.alert1 = ""
            this.alert2 = ""
            this.showMessage = false
            return true

        }
        
      }
  
  getReport(){
    let $scope = this.$scope
    this.start_month = this.start_date.getMonth()+1 
    this.end_month = this.end_date.getMonth()+1
    this.month_number = []
    this.array_month = []
    this.length = 0
    //console.log("start_month",this.start_month)
    //console.log("end_month",this.end_month) 
    
    var j = 0
    for (var i = this.start_month; i <= this.end_month; i++) {
     this.month_number[j] = i 
     this.length = j
     j++
    }
    
    //console.log("month_number",this.month_number)
    
    for (var i = 0; i <= this.length ; i++) {

      switch (this.month_number[i])
      {
        case 1 :  this.array_month[i] = 'Enero'+' '+this.year;
        break;
        case 2 :  this.array_month[i] = 'Febrero'+' '+this.year;
        break;
        case 3 :  this.array_month[i] = 'Marzo'+' '+this.year;
        break;
        case 4 :  this.array_month[i] = 'Abril'+' '+this.year;
        break;
        case 5 :  this.array_month[i] = 'Mayo'+' '+this.year;
        break;
        case 6 :  this.array_month[i] = 'Junio'+' '+this.year;
        break;
        case 7 :  this.array_month[i] = 'Julio'+' '+this.year;
        break;
        case 8 :  this.array_month[i] = 'Agosto'+' '+this.year;
        break;
        case 9 :  this.array_month[i] = 'Septiembre'+' '+this.year;
        break;
        case 10 : this.array_month[i] = 'Octubre'+' '+this.year;
        break;
        case 11 : this.array_month[i] = 'Noviembre'+' '+this.year;
        break;
        case 12 : this.array_month[i] = 'Diciembre'+' '+this.year;
        break;
      }
    }
    //console.log("this.array_month.length",this.array_month.length)
    //console.log("this.length",this.length)
   

    console.log("array_month",this.array_month) 
   
    let Salesreport = this.API.service('salesreport', this.API.all('dashboard'))
        
    Salesreport.one().get({
      'completed': 1,
      'start_date': this.start_date,
      'end_date': this.end_date}).then((response) => {
      
      let Salesreportdata = response.plain(response) 
      console.log("Salesreportdata", Salesreportdata.data)
      var start_month = this.start_date.getMonth()+1 
      var end_month = this.end_date.getMonth()+1
      this.data_number=[]  
    
      for (var i = 0; i <= 11 ; i++) {      
        this.data_number[i] = 0
      }
      //console.log("this.data_number",this.data_number)

      for (var i = 0; i < Salesreportdata.data.policies.length ; i++) {
        switch (Salesreportdata.data.policies[i].month)
          {
            case 1 : this.data_number[0] = Salesreportdata.data.policies[i].num_sales
            break; 
            case 2 : this.data_number[1] = Salesreportdata.data.policies[i].num_sales
            break;
            case 3 : this.data_number[2] = Salesreportdata.data.policies[i].num_sales
            break;
            case 4 : this.data_number[3] = Salesreportdata.data.policies[i].num_sales
            break;
            case 5 : this.data_number[4] = Salesreportdata.data.policies[i].num_sales
            break;
            case 6 : this.data_number[5] = Salesreportdata.data.policies[i].num_sales
            break;
            case 7 : this.data_number[6] = Salesreportdata.data.policies[i].num_sales
            break;
            case 8 : this.data_number[7] = Salesreportdata.data.policies[i].num_sales
            break;
            case 9 : this.data_number[8] = Salesreportdata.data.policies[i].num_sales
            break;
            case 10 : this.data_number[9] = Salesreportdata.data.policies[i].num_sales
            break;
            case 11 : this.data_number[10] = Salesreportdata.data.policiess[i].num_sales
            break;
            case 12 : this.data_number[11] = Salesreportdata.data.policiess[i].num_sales
            break;

          }
        }
      
      this.data_number= this.data_number.slice(start_month-1, end_month)
      this.total_sales = 0
      
      for (var i = 0; i < this.data_number.length; i++) {
        this.total_sales= this.total_sales+ parseInt(this.data_number[i])
      }
    console.log("data_number",this.data_number)    
    $scope.data= [this.data_number]
    })

    let Totalpermonth = this.API.service('dollarsreport', this.API.all('dashboard'))
    Totalpermonth.one().get({
      'completed': 1,
      'start_date': this.start_date,
      'end_date': this.end_date}).then((response) => {
        let Totalreportdata = response.plain()
        console.log("Totalreportdata",Totalreportdata.data)
        var start_month = this.start_date.getMonth()+1 
        var end_month = this.end_date.getMonth()+1
        this.data_money=[]  
    
        for (var i = 0; i <= 11 ; i++) {      
          this.data_money[i] = 0
        }
        //console.log("this.data_money",this.data_money)

        for (var i = 0; i < Totalreportdata.data.policies.length ; i++) {
          switch (Totalreportdata.data.policies[i].month)
          {
            case 1 : this.data_money[0] = Totalreportdata.data.policies[i].total_money
            break; 
            case 2 : this.data_money[1] = Totalreportdata.data.policies[i].total_money
            break;
            case 3 : this.data_money[2] = Totalreportdata.data.policies[i].total_money
            break;
            case 4 : this.data_money[3] = Totalreportdata.data.policies[i].total_money
            break;
            case 5 : this.data_money[4] = Totalreportdata.data.policies[i].total_money
            break;
            case 6 : this.data_money[5] = Totalreportdata.data.policies[i].total_money
            break;
            case 7 : this.data_money[6] = Totalreportdata.data.policies[i].total_money
            break;
            case 8 : this.data_money[7] = Totalreportdata.data.policies[i].total_money
            break;
            case 9 : this.data_money[8] = Totalreportdata.data.policies[i].total_money
            break;
            case 10 : this.data_money[9] = Totalreportdata.data.policies[i].total_money
            break;
            case 11 : this.data_money[10] = Totalreportdata.data.policiess[i].total_money
            break;
            case 12 : this.data_money[11] = Totalreportdata.data.policies[i].total_money
            break;

          }
        }
      
      this.data_money= this.data_money.slice(start_month-1, end_month)         
      this.total_money = 0
      
        for (var i = 0; i < this.data_money.length; i++) {
          this.total_money = this.total_money+ parseFloat(this.data_money[i],2)
          this.total_money = parseFloat(this.total_money.toFixed(2))
        }
        console.log("data_money",this.data_money)

        $scope.moneyData= [this.data_money]
    })    

    let CanceledPolicies = this.API.service('canceledpoliciesreport', this.API.all('dashboard'))
    CanceledPolicies.one().get({
      'completed': 1,
      'start_date': this.start_date,
      'end_date': this.end_date}).then((response) => {
        let CanceledPoliciesdata = response.plain()
        console.log("CanceledPoliciesdata",CanceledPoliciesdata.data)    
        var start_month = this.start_date.getMonth()+1 
        var end_month = this.end_date.getMonth()+1
        this.data_canceled=[]  
      
        for (var i = 0; i <= 11 ; i++) {      
          this.data_canceled[i] = 0
        }
        //console.log("this.data_canceled",this.data_canceled)

        for (var i = 0; i < CanceledPoliciesdata.data.policies.length ; i++) {

          switch (CanceledPoliciesdata.data.policies[i].month)
          {
            case 1 : this.data_canceled[0] = CanceledPoliciesdata.data.policies[i].canceled
            break; 
            case 2 : this.data_canceled[1] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 3 : this.data_canceled[2] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 4 : this.data_canceled[3] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 5 : this.data_canceled[4] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 6 : this.data_canceled[5] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 7 : this.data_canceled[6] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 8 : this.data_canceled[7] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 9 : this.data_canceled[8] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 10 : this.data_canceled[9] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 11 : this.data_canceled[10] = CanceledPoliciesdata.data.policies[i].canceled
            break;
            case 12 : this.data_canceled[11] = CanceledPoliciesdata.data.policies[i].canceled
            break;

          }
        }
      
      this.data_canceled= this.data_canceled.slice(start_month-1, end_month)   
        this.total_canceled = 0
      
        for (var i = 0; i < this.data_canceled.length; i++) {
          this.total_canceled= this.total_canceled+ parseInt(this.data_canceled[i])
        }
        console.log("data_canceled",this.data_canceled)    
       
      
        $scope.cancelData = [this.data_canceled]
        console.log("cancelData",$scope.cancelData)
    })//QuantityPolicies
       
    let GracePolicies = this.API.service('gracepoliciesreport', this.API.all('dashboard'))
    GracePolicies.one().get({
      'completed': 1,
      'start_date': this.start_date,
      'end_date': this.end_date}).then((response) => {
        let GracePoliciesdata = response.plain()
        console.log("GracePoliciesdata",GracePoliciesdata.data)    
        var start_month = this.start_date.getMonth()+1 
        var end_month = this.end_date.getMonth()+1
        this.data_grace=[]  
      
        for (var i = 0; i <= 11 ; i++) {      
          this.data_grace[i] = 0
        }
        //console.log("this.data_grace",this.data_grace)

        for (var i = 0; i < GracePoliciesdata.data.policies.length ; i++) {

          switch (GracePoliciesdata.data.policies[i].month)
          {
            case 1 : this.data_grace[0] = GracePoliciesdata.data.policies[i].num_grace
            break; 
            case 2 : this.data_grace[1] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 3 : this.data_grace[2] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 4 : this.data_grace[3] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 5 : this.data_grace[4] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 6 : this.data_grace[5] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 7 : this.data_grace[6] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 8 : this.data_grace[7] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 9 : this.data_grace[8] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 10 : this.data_grace[9] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 11 : this.data_grace[10] = GracePoliciesdata.data.policies[i].num_grace
            break;
            case 12 : this.data_grace[11] = GracePoliciesdata.data.policies[i].num_grace
            break;

          }
        }
        this.data_grace= this.data_grace.slice(start_month-1, end_month)   
        this.total_grace = 0
      
        for (var i = 0; i < this.data_grace.length; i++) {
          this.total_grace= this.total_grace+ parseInt(this.data_grace[i])
        }
        console.log("data_grace",this.data_grace)     
        $scope.graceData = [this.data_grace]
    
    })//GracePolicies

    let Claims = this.API.service('claimsreport', this.API.all('dashboard'))
    Claims.one().get({
      
      'start_date': this.start_date,
      'end_date': this.end_date}).then((response) => {
      let Claimsdata = response.plain()
      console.log("Claimsdata",Claimsdata.data)    
      var start_month = this.start_date.getMonth()+1 
      var end_month = this.end_date.getMonth()+1
      this.data_claims=[]  
    
      for (var i = 0; i <= 11 ; i++) {      
        this.data_claims[i] = 0
      }
      //console.log("data_claims",this.data_claims)

      for (var i = 0; i < Claimsdata.data.claims.length ; i++) {

        
        switch (Claimsdata.data.claims[i].month)
        {
          case 1 : this.data_claims[0] = Claimsdata.data.claims[i].num_claims
          break; 
          case 2 : this.data_claims[1] = Claimsdata.data.claims[i].num_claims
          break;
          case 3 : this.data_claims[2] = Claimsdata.data.claims[i].num_claims
          break;
          case 4 : this.data_claims[3] = Claimsdata.data.claims[i].num_claims
          break;
          case 5 : this.data_claims[4] = Claimsdata.data.claims[i].num_claims
          break;
          case 6 : this.data_claims[5] = Claimsdata.data.claims[i].num_claims
          break;
          case 7 : this.data_claims[6] = Claimsdata.data.claims[i].num_claims
          break;
          case 8 : this.data_claims[7] = Claimsdata.data.claims[i].num_claims
          break;
          case 9 : this.data_claims[8] = Claimsdata.data.claims[i].num_claims
          break;
          case 10 : this.data_claims[9] = Claimsdata.data.claims[i].num_claims
          break;
          case 11 : this.data_claims[10] = Claimsdata.data.claims[i].num_claims
          break;
          case 12 : this.data_claims[11] = Claimsdata.data.claims[i].num_claims
          break;

        }
      }
      
      this.data_claims= this.data_claims.slice(start_month-1, end_month)
        this.total_claims = 0
      
        for (var i = 0; i < this.data_claims.length; i++) {
          this.total_claims= this.total_claims+ parseInt(this.data_claims[i])
        }
        console.log("data_claims",this.data_claims)     
        $scope.data_claims = [this.data_claims]
    
    })//Claims
  
  //console.log("this.array_month",this.array_month)
  $scope.labels =this.array_month
  $scope.series = ['Series A']
  $scope.year = this.year
  $scope.pieLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales']
  $scope.pieData = [300, 500, 100]    
    
  }

}

export const DashboardComponent = {
  templateUrl: './views/app/components/dashboard/dashboard.component.html',
  controller: DashboardController,
  controllerAs: 'vm',
  bindings: {}
}
