class RenewalApplicantDataController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';
         var vm = this
        this.current_step = '1'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        var country_id = ""
        var province_id = ""
        this.uisrefback = 'app.renewal-lists'
        var policyId = $stateParams.policyId
        this.policyId = policyId
        var feeId = $stateParams.feeId
        this.feeId = feeId
        this.uisrefparameter = ' {policyId: vm.policyId, feeId: feeId}'
        console.log("parametros", this.feeId)
        this.uisrefback = 'app.renewal-lists'
        console.log("aaaa ", policyId)
        console.log("bbb ", feeId)
        this.applicant_data = []
        this.applicant_data.comments = []
        this.applicant_id= policyId
       
        //console.log("id de poliza actual", policyId)
        
        let ApplicantData = API.service('show', API.all('applicants'))
        ApplicantData.one(policyId).get()
          .then((response) => {
            this.applicanteditdata = API.copy(response)
            console.log("Aplicante", this.applicanteditdata)
            this.applicanteditdata.data[0].height = parseFloat(this.applicanteditdata.data[0].height, 2)
            this.applicanteditdata.data[0].weight = parseFloat(this.applicanteditdata.data[0].weight, 2)
            this.applicanteditdata.data[0].mobile = parseInt(this.applicanteditdata.data[0].mobile)
            this.applicanteditdata.data[0].phone = parseInt(this.applicanteditdata.data[0].phone)
            console.log(this.applicanteditdata.data[0].birthday)
            if (this.applicanteditdata.data[0].birthday == 'null') {
                this.applicanteditdata.data[0].birthday =  ''//new Date(this.applicanteditdata.data[0].birthday)
            }else{
                this.applicanteditdata.data[0].birthday =  new Date(this.applicanteditdata.data[0].birthday+' ')
            }
            country_id = this.applicanteditdata.data[0].country_id
            province_id = this.applicanteditdata.data[0].province_id
            if(this.applicanteditdata.data[0].height_unit_measurement == '1'){
                this.applicanteditdata.data[0].height_unit_measurement = true
            }else{
                this.applicanteditdata.data[0].height_unit_measurement = false
            }
            if(this.applicanteditdata.data[0].weight_unit_measurement == '1'){
                this.applicanteditdata.data[0].weight_unit_measurement = true
            }else{
                this.applicanteditdata.data[0].weight_unit_measurement = false
            }            


        let States = API.service('index', API.all('states'))
        States.one().get()
          .then((response) => {
            let systemStates = []
            let allStates = []
            let stateResponse = response.plain()
            angular.forEach(stateResponse.data.states, function (value) {
              if(value.country_id == country_id){
                systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
              }
              allStates.push({id: value.id, name: value.name, country_id: value.country_id})
            })
            this.systemStates = systemStates
            this.allStates = allStates
            //console.log("systemStates==> ", systemStates)
            //console.log("allStates==> ", allStates)


            let Cities = API.service('index', API.all('cities'))
            Cities.one().get()
              .then((response) => {
                let systemCities = []
                let allCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                  if(value.province_id == province_id){
                    systemCities.push({id: value.id, name: value.name, province_id: value.province_id})
                  }
                  allCities.push({id: value.id, name: value.name, province_id: value.province_id})
                })
                this.systemCities = systemCities
                this.allCities = allCities
                //console.log("systemCities==> ", systemCities)
                //console.log("allCities==> ", allCities)

                
            })

        })
        })


        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
                if ($stateParams.completed == 1){
                    if ((value.order == 1) || (value.order == 3)){
                        systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})
                    }
                }
                else{
                    systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})

                }

            })
            systemWizar.splice(3,1)
            //systemWizar.pop()
            this.systemWizar = systemWizar
            console.log("systemWizar==> ", this.systemWizar)
        })
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            //console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            //console.log("systemCountrys==> ", systemCountrys)
        })

          let applicant_data = API.one('applicant_data', this.applicant_id)
            applicant_data.get().then((response) => {
            this.applicant_data = response.plain().data
            console.log("applicant_data", this.applicant_data)
            })
    }

    getState(){
        console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let States = API.service('index', API.all('states'))
        States.one(vm.applicanteditdata.data[0].country_id).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                 console.log(stateResponse)
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
            this.systemStates = systemStates
            //console.log("systemStates  ==> ", systemStates)          

        })*/
        this.rowStatus = false;
        //console.log("allStates ", this.allStates);
        //console.log(this.systemCanton);
        var country_id = this.applicanteditdata.data[0].country_id
        let systemStates = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allStates, function (value) {
          if(value.country_id == country_id){
            systemStates.push({id: value.id, name: value.name})
          }
        })
        if (country_id !== ""){
          this.rowStatus = true;
        }    
        this.systemStates = systemStates
        this.systemCities = ""
        //console.log("systemStates ", systemStates)
    }

    getCity(){
        console.log("Entro..", this.applicanteditdata.data[0].country_id)
        /*let Citys = API.service('index', API.all('cities'))
        Citys.one(vm.applicanteditdata.data[0].province_id).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                 console.log(cityResponse)
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
            this.systemCities = systemCities
            //console.log("systemCities  ==> ", systemCities)          

        })*/
        this.rowStatus = false;
        console.log("allCities ", this.allCities);
        //console.log(this.systemCanton);
        var province_id = this.applicanteditdata.data[0].province_id
        let systemCities = []
        //console.log("all ", this.allCanton)
        angular.forEach(this.allCities, function (value) {
          if(value.province_id == province_id){
            systemCities.push({id: value.id, name: value.name})
          }
        })
        if (province_id !== ""){
          this.rowStatus = true;
        }    
        this.systemCities = systemCities
        //this.systemCities = ""
        console.log("systemCities ", systemCities)

    }

    save (isValid) {

        if(this.applicanteditdata.data[0].height_unit_measurement == true){
            this.applicanteditdata.data[0].height_unit_measurement = '1'
        }else{
            this.applicanteditdata.data[0].height_unit_measurement = '0'
        }
        if(this.applicanteditdata.data[0].weight_unit_measurement == true){
            this.applicanteditdata.data[0].weight_unit_measurement = '1'
        }else{
            this.applicanteditdata.data[0].weight_unit_measurement = '0'
        }
        console.log('xxx', this.applicanteditdata.data[0])
        if (isValid) {
            console.log("cccc",this.feeId)
            this.isDisabled = true;
            let ApplicantData = this.API.service('applicant', this.API.all('renewals'))
            this.applicanteditdata.data = this.applicanteditdata.data[0]
            //console.log(this.applicanteditdata);
            let $state = this.$state
            ApplicantData.post(this.applicanteditdata)
            //this.applicanteditdata.put()
              .then((response) => {
                response.feeId = this.feeId
                swal({
                  title: 'Datos del Solicitante Guardados Correctamente!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                }, function () {
                      $state.go('app.renewal-plan-config',{"policyId": response.data, "feeId": response.feeId})
                })
            }, (response) => {
                swal({
                  title: 'Error:' + response.data.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                      $state.go($state.current)
                  })
              })
        } else {
          this.formSubmitted = true
        }
    }

    addComment(){
         console.log("entro en addComment")
                   
        if (this.comment != ''){
            var vm = this
            let comment = this.API.service('comments', this.API.all('applicants'))
            comment.post({
                'module' : 0,
                'key_field' : this.applicant_id,
                'text' : this.comment
            }).then(function () {
                vm.comment = ''
                vm.update()
          }, function (response) {
          })
        }
    }
    deleteComment(commentId) {
        this.API.one('comments').one('comments', commentId).remove()
        .then(() => {
            this.update()
        })
    }
    update(){
        let comments = this.API.service('show', this.API.all('comments'))
        comments.one(this.applicant_id).get()
        .then((response) => {
            this.applicant_data.comments = this.API.copy(response).data
        })
    }

    $onInit(){
    }
}

export const RenewalApplicantDataComponent = {
    templateUrl: './views/app/components/renewal-applicant-data/renewal-applicant-data.component.html',
    controller: RenewalApplicantDataController,
    controllerAs: 'vm',
    bindings: {}
}
