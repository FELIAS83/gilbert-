class MedicalSpecialityAddController{
    constructor ($scope, API, $state, $stateParams) {//pendiente por guardar
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.name = ''
        this.processing = false


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
          this.processing = true
          let Medicalspecialties = this.API.service('medicalspecialties', this.API.all('medicalspecialties'))
          let $state = this.$state
          let name = this.name
          let vm = this

          Medicalspecialties.post({
            'abbreviation': "",
            'name': this.name
          }).then(function () {
            vm.processing = false
            //console.log("mensaje" );
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido la especialidad "'+name+'"'}
            $state.go($state.current, { alerts: alert})
            //console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else {
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const MedicalSpecialityAddComponent = {
    templateUrl: './views/app/components/medical-speciality-add/medical-speciality-add.component.html',
    controller: MedicalSpecialityAddController,
    controllerAs: 'vm',
    bindings: {}
}
