class RenewalListsController{
    constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http){
        'ngInject';
        var vm = this
        this.API = API
        this.$state = $state
        this.$scope=$scope  
        this.$uibModal = $uibModal
        this.$log = $log
        this.animationsEnabled = true
        this.items = ['item1', 'item2', 'item3']
        this.systemWizar = []
        this.systemWizar.push({id: 0, icon: "fa fa-paste fa-3x", name: "Cuotas Vencidas", order: 1, uisref: "app.renewal-lists"}, {id: 2, icon: "fa fa-refresh fa-3x", name: "Pólizas a Vencer", order: 2, uisref: "app.renewal-policy-lists"})
        this.current_step = 1
        this.openTab = openTab
        this.isDisabled1 = true
        this.isDisabled2 = false
        this.compareDate = compareDate
        this.isDisabled = false
        this.showMessage = false
        this.alert = ""
        this.DTOptionsBuilder = DTOptionsBuilder
        this.DTColumnBuilder = DTColumnBuilder
        this.$compile = $compile
        var fecha=""
        var arrfecha=""

        /*let Dates = this.API.service('filterdates', API.all('renewals'))
        Dates.one().get()
          .then((response) => {
            let dataS = response.plain()
            //console.log("dates", dataS)
            this.start_date = new Date(dataS.data.start_date.date)
            this.end_date = new Date(dataS.data.end_date.date)
            //console.log("start_date", this.start_date)
            //console.log("end_date", this.end_date)


          })*/

        let Policies = this.API.service('getExpiredPolicies')

         Policies.getList({
          'start_date':null,
          'end_date':null
         })
          .then((response) => {

            let dataSet = response.plain()
            this.start_date = new Date(dataSet[0].start_date.date)
            this.end_date = new Date(dataSet[0].final_date.date)
            //this.have_renewal = dataSet[0].have_renewal == '0' ? true : false
           
            /*********************************/
              angular.forEach(dataSet, function (value) {

              fecha = value.fee_payment_date
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]
              //console.log("fecha",fecha)

              fecha= month+'/'+day+'/'+year  
              value.fee_payment_date = fecha
              //console.log("fecha",fecha)
            })
            //console.log("fecha",fecha)

         
       
     
            /*********************************/

         //console.log("DataSet",dataSet)
         this.dataSet = dataSet
          angular.forEach(dataSet, function(value){

            if(value.step1 == null){
                  value.step1 = 0
              }
              if(value.step2 == null){
                  value.step2 = 0
              }
              if(value.step3 == null){
                  value.step3 = 0
              }
              if(value.step4 == null){
                  value.step4 = 0
              }
              if(value.step5 == null){
                  value.step5 = 0
              }
              if(value.step6 == null){
                  value.step6 = 0
              }
         })

            let id_session = 'DataTables_Renewal'
             if (sessionStorage.getItem('DataTables_Renewal2')){
                 id_session = 'DataTables_Renewal2'
             }
             if (sessionStorage.getItem('data')){
                 dataSet = JSON.parse(sessionStorage.getItem('data'))
            }
            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withOption('stateSave', true)
            .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.removeItem('data')
                sessionStorage.removeItem('DataTables_Renewal2')
                sessionStorage.setItem('DataTables_Renewal', JSON.stringify(data));
            })
            .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem(id_session))
            })
            .withBootstrap()

            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('total').withTitle('Monto a Cancelar'),
              DTColumnBuilder.newColumn('mode').withTitle('Modo de Pago'),
              DTColumnBuilder.newColumn('fee_payment_date').withTitle('Fecha de Pago'),
              DTColumnBuilder.newColumn('days_passed').withTitle('Días de Vencimiento'),
              DTColumnBuilder.newColumn('fee_number').withTitle('Cuota #'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })
          

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        let actionsHtml = (data) =>  {
          return`<ul>
                    <a  ng-if="${data.step1 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Aviso de Renovación" class="btn btn-xs btn-primary" ui-sref="app.renewal-notification({policyId: ${data.id}, feeId: ${data.fee_id}, fee_number: ${data.fee_number}})">
                        <i class="fa fa-envelope"></i>
                    </a>
                    <a  ng-if="${data.step1 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Aviso de Renovación" class="btn btn-xs btn-default" ui-sref="app.renewal-notification({policyId: ${data.id}, feeId: ${data.fee_id}, fee_number: ${data.fee_number}})">
                        <i class="fa fa-envelope"></i>
                    </a>
                    &nbsp 
                    <a  ng-if="${data.step2 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-primary" ui-sref="app.renewal-applicant-data({policyId: ${data.id}, feeId: ${data.fee_id}})">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a  ng-if="${data.step2 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-default" ui-sref="app.renewal-applicant-data({policyId: ${data.id}, feeId: ${data.fee_id}})">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    &nbsp                    
                    <a ng-if="${data.step3 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza-Renovación " class="btn btn-xs btn-primary"  ui-sref="app.confirm-renewal-data({policyId: ${data.id}, feeId: ${data.fee_id}})">
                      <i class="fa fa-check"></i>
                    </a>
                    <a ng-if="${data.step3 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza-Renovación " class="btn btn-xs btn-default"  ui-sref="app.confirm-renewal-data({policyId: ${data.id}, feeId: ${data.fee_id}})">
                      <i class="fa fa-check"></i>
                    </a>
                     &nbsp
                     <a ng-if="${data.step4 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-primary"  ui-sref="app.renewal-payment-register({policyId: ${data.id}, feeId: ${data.fee_id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step4 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-default"  ui-sref="app.renewal-payment-register({policyId: ${data.id}, feeId: ${data.fee_id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <button ng-if="${data.step5 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-primary" ui-sref="app.send-invoice-renewal({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}" >
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </button>
                    <button ng-if="${data.step5 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-default" ui-sref="app.send-invoice-renewal({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}" >
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </button>
                    &nbsp
                     <button ng-if="${data.step6 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago " class="btn btn-xs btn-primary"  ui-sref="app.confirm-renewal-payment({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}">
                      <i class="fa fa-location-arrow"></i>
                    </button>
                    <button ng-if="${data.step6 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago " class="btn btn-xs btn-default"  ui-sref="app.confirm-renewal-payment({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}">
                      <i class="fa fa-location-arrow"></i>
                    </button>
                     &nbsp
                    <button style="color: #d73925; background-color: #FFF;width:24.2px;height:21.98px" title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.motivePolicies({policyId: ${data.id}, feeId: ${data.fee_id}})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

       
        //


      function openTab(value){
             let $state = this.$state
            if(value == "1"){     
                 $state.go('app.renewal-lists')
            }else if(value == "2"){
                $state.go('app.renewal-policy-lists')

            }
        }


      /*****************/

          function compareDate(){


            if(this.start_date != null &&  this.end_date != null){
                        let startdate = new Date(this.start_date)
                        let finaldate = new Date(this.end_date)
                       
                        if(startdate <= finaldate){
                             vm.isDisabled = 0 
                             vm.alert1 = ""
                             vm.alert2 = ""
                             vm.showMessage = false
                    
            
                        }else{

                             vm.isDisabled = 1
                             vm.alert1 = "La fecha Inicial debe ser menor a la Final"
                             vm.alert2 = ""
                             vm.showMessage = true
                    
              
                        }
               }else{
                        vm.isDisabled = 0
                        vm.alert1 = ""
                        vm.alert2 = ""
                        vm.showMessage = false
               }
          }    


    }

    export(){
        let header = [
            'Policy Number', 'Legacy Number', 'Policy Holder', 'Plan Description', 'Option', 'Renewal Date', 'Payment Frequency Name', 'Agent Name', 'Agency Name', 'Frecuencia de Pago', 'Balance de Pago'
        ]
        let dataSet = this.reportdata ? this.reportdata : this.dataSet
        let exportexcel = this.API.all('exportexcel').withHttpConfig({responseType: 'blob'})
        exportexcel.post({
            'header': header,
            'values' : dataSet
        }).then(function (response) {
            let a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = (window.URL || window.webkitURL).createObjectURL(response);
            a.download = 'Renovaciones Pendientes.xls';
            a.click();
            swal({
                title: 'Exportación Exitosa!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                //$state.reload()
              })
              //window.URL.revokeObjectURL(url);
            }, function (response) {
              swal({
                title: 'Error en la Exportación!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                $state.reload()
              })
            })
    }

    /*
  modalInOpen (size, name, id) {
    console.log("Entro", id)
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", name);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalInContent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }
 modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state) {
    'ngInject'

      console.log("controller")
      this.API = API
      this.items = items
      this.customer = customer
      this.id = id
      this.$state = $state
      //this.startRenewal = startRenewal

      this.startRenewal = (id) => {
        console.log("id ", id)
        //console.log("answer ", answer)
        $uibModalInstance.dismiss('cancel')

        //let renewal = this.API.service('renewal', this.API.all('policies'))
        let renewal = this.API.service('renewal', this.API.all('renewals'))
            
          renewal.post({
              'policy_id' : id
          } )
          .then( ( response ) => {             
              

              let params = { "policyId": response.data.policyIdOld, "policyIdNew": response.data.policyId, "applicantId": response.data.applicantId}

              $state.go('app.renewal-applicant-data',{ "policyId": response.data.policyIdOld})
              $uibModalInstance.dismiss('cancel')
            
          }, function (response) {
                  console.log('error')
              }
          )
          
          console.log("xxxx")
       }

    this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }  

    }
*/
delete (policyId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se descartará la renovación de la póliza!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('renewals').one('renewal', policyId).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }


    renewal (policyId) {
         let API = this.API
         let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Se renovará automaticamente esta Póliza!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, renuévala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        let renewal = API.service('renewalpolicy', API.all('renewals'))
        renewal.post({
          'policy_id' : policyId
        })
          .then(() => {
            swal({
              title: 'Renovada!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }


motivePolicies (policyId, feeId) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items
            
      console.log("policyId", policyId);
      let PolicyData = this.API.service('show', this.API.all('policies'))
      PolicyData.one(policyId.policyId).get()
      .then((response)=>{
        this.PolicyData=this.API.copy(response)
        console.log("PolicyData== ", this.PolicyData)
      })

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalPolicy',
        controller: this.modalcontroller,
        controllerAs: 'mvm', 
        size: 'md',
        resolve: {
          policyId: () => {
            return policyId
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    deleteLine (index){
     
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
        console.log('newlines',this.lines)
        this.dependents.splice(pos,1)
        //this.$state.reload()
        
    }
modalcontroller ($scope, $uibModalInstance, API, $http, $state, $stateParams,policyId) {
    'ngInject'
        var mvm = this
        this.API = API
        this.$http = $http
        var $state = $state
        this.$state = $state
        this.processing = true
        this.formSubmitted = false
        this.motive = 0
        this.alert = ""
        this.policyId=policyId
        let systemMotive = []
        systemMotive = [{id: 1, name: "Cambio de Compañia"},{id: 2, name: "Temas Económicos"},{id: 3, name: "Cambio de Agencia"},{id: 4, name: "Otros"}] 
        this.systemMotive = systemMotive
        console.log("policyId",this.policyId)
        


        let PolicyData = this.API.service('show', this.API.all('policies'))
      PolicyData.one(this.policyId.policyId).get()
      .then((response)=>{
        this.PolicyData=this.API.copy(response)
        console.log("PolicyData1", this.PolicyData)
      })

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.changeMotive = () => {
        console.log(mvm.motive)
        if (mvm.motive != 0) {
          this.alert = ""          
          this.processing = false
        }else
          this.processing = true
      }

      
      this.deletePolicy = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log("Póliza a eliminar", mvm.PolicyData)


        if (isValid){
          let $state = this.$state
            
          if(this.motive > 0){
              this.processing = true
              console.log("Motivo", this.motive)
              console.log("policyId", this.policyId.policyId)

              let Policy = this.API.service('MotiveRenewal')
              Policy.post({
                'policyId' : this.policyId.policyId,
                'motive': mvm.motive
              }).then(function (response) {
                
                $uibModalInstance.dismiss('cancel')
                
             swal({
                    title: 'Renovacion eliminada Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current, {}, { reload: $state.current})
                })
                }, function (response) {
                    swal({
                    title: 'Error al guardar motivo' + response.message,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                    })
                  }
                )
        }else{
              this.formSubmitted = true
             }            
      }
    }
  }


/****************************** REPORT *********************************/
getReport(){
          //var vm = this
          //let changeType = vm.changeType
         //if(this.verifiedDate()){
            let Policies = this.API.service('getExpiredPolicies')

            Policies.getList({
              'start_date':this.start_date,
              'end_date':this.end_date
            })
          .then((response) => {
            this.reportdata = response.plain(response)
            sessionStorage.setItem('data', JSON.stringify(this.reportdata));
            //console.log("this.reportdata",this.reportdata)
            
            this.dtOptions = this.DTOptionsBuilder.newOptions()
              .withOption('data', this.reportdata)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withOption('stateSave', true)
            .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.removeItem('DataTables_Renewal')
                sessionStorage.setItem('DataTables_Renewal2', JSON.stringify(data));
            })
            .withOption('stateLoadCallback', function(settings) {
                this.data = sessionStorage.getItem('data' )
                return JSON.parse(sessionStorage.getItem('DataTables_Renewal2' ))
            })
            .withBootstrap()


            this.dtColumns = [
              this.DTColumnBuilder.newColumn('id').withTitle('ID'),
              this.DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              this.DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              this.DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              this.DTColumnBuilder.newColumn('total').withTitle('Monto a Cancelar'),
              this.DTColumnBuilder.newColumn('mode').withTitle('Modo de Pago'),
              this.DTColumnBuilder.newColumn('fee_payment_date').withTitle('Fecha de Pago'),
              this.DTColumnBuilder.newColumn('days_passed').withTitle('Días de Vencimiento'),
              this.DTColumnBuilder.newColumn('fee_number').withTitle('Cuota #'),
              this.DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true

            })
           let createdRow = (row) => {
             this.$compile(angular.element(row).contents())(this.$scope)
             }

              let actionsHtml = (data) =>  {
          return`<ul>
                    <a  style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-info" ui-sref="app.renewal-applicant-data({policyId: ${data.id}, feeId: ${data.fee_id}})">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    &nbsp                    
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza-Renovación " class="btn btn-xs btn-primary"  ui-sref="app.confirm-renewal-data({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>
                    &nbsp
                     <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pagos " class="btn btn-xs btn-primary"  ui-sref="app.renewal-payment-register({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}">
                      <i class="fa fa-usd"></i>
                    </a>
                     &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-primary" ui-sref="app.send-invoice-renewal({policyId: ${data.id}})" ng-disabled="${data.forDisabled}" >
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </button>
                     &nbsp
                     <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pagos " class="btn btn-xs btn-primary"  ui-sref="app.renewal-payment-register({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}">
                      <i class="fa fa-usd"></i>
                    </a>
                    &nbsp
                     <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pagos " class="btn btn-xs btn-primary"  ui-sref="app.confirm-renewal-payment({policyId: ${data.id}, feeId: ${data.fee_id}})" ng-disabled="${data.forDisabled}">
                      <i class="fa fa-location-arrow"></i>
                    </button>
                     &nbsp
                    <button style="color: #d73925; background-color: #FFF;width:24.2px;height:21.98px" title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.motivePolicies({policyId: ${data.id}, feeId: ${data.fee_id}})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }


       /* }else{
          console.log("nada")
        }
    */
        
    }
    verifiedDate(){

            if(this.start_date == null &&  this.end_date != null){

                this.isDisabled = 1
                this.alert1 = "requerido"
                this.alert2 = ""
                this.showMessage = true
                return false


            }else if(this.start_date != null &&  this.end_date == null){

                this.isDisabled = 1
                this.alert1 = ""
                this.alert2 = "requerido"
                this.showMessage = true
                return false

            }else if(this.start_date == null &&  this.end_date == null){

                this.isDisabled = 0
                this.alert1 = "requerido"
                this.alert2 = "requerido"
                this.showMessage = true
                return false

            }else if(this.start_date != null &&  this.end_date != null){

                this.isDisabled = 0
                this.alert1 = ""
                this.alert2 = ""
                this.showMessage = false
                return true

            }else{
                this.isDisabled = 0
                this.alert1 = ""
                this.alert2 = ""
                this.showMessage = false
                return true

            }
            
          }
/***********************************************************************/
    $onInit(){
    }
}

export const RenewalListsComponent = {
    templateUrl: './views/app/components/renewal-lists/renewal-lists.component.html',
    controller: RenewalListsController,
    controllerAs: 'vm',
    bindings: {}
}
