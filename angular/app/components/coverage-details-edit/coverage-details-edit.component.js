class CoverageDetailsEditController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let coverageDetailId = $stateParams.coverageDetailId

        let CoverageData = API.service('show', API.all('coverage_details'))
        CoverageData.one(coverageDetailId).get()
          .then((response) => {
            this.coveragedetaileditdata = API.copy(response)
            //console.log("data de detalles de cobertura", this.coveragedetaileditdata.data)

            let Coverage = API.service('show', API.all('coverages'))
            Coverage.one(this.coveragedetaileditdata.data.coverage_id).get()
            .then((response) => {

                this.coverageeditdata = API.copy(response)
                this.coveragedetaileditdata.data.amount = parseFloat(this.coveragedetaileditdata.data.amount, 2)
                //console.log("data  de cobertura", this.coverageeditdata.data)                

                let DeductibleData = API.service('show', API.all('deductibles'))
                DeductibleData.one(this.coveragedetaileditdata.data.deductible_id).get()
                .then((response) => {

                    this.deductibleeditdata = API.copy(response)
                    console.log("data de deducible", this.deductibleeditdata.data)
                    this.deductibleeditdata.data.amount_in_usa = parseFloat(this.deductibleeditdata.data.amount_in_usa, 2)

                    let PlanData = API.service('show', API.all('plans'))
                    PlanData.one(this.deductibleeditdata.data.plan_id).get()
                    .then((response) => {
                    
                        this.planeditdata = API.copy(response)
                        //console.log("data de plan", this.planeditdata)

                    })

                })

            })        
        
        
      })

        //
    }
    save (isValid) {
        if (isValid) {
          let $state = this.$state
          this.coveragedetaileditdata.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado los datos de la cobertura.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const CoverageDetailsEditComponent = {
    templateUrl: './views/app/components/coverage-details-edit/coverage-details-edit.component.html',
    controller: CoverageDetailsEditController,
    controllerAs: 'vm',
    bindings: {}
}
