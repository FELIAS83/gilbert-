class MessengerListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        this.API = API
        this.$state = $state
        this.can = AclService.can

        let Messengers = API.service('messengers')
        Messengers.getList({})
          .then((response) => {

            let dataSet = response.plain()
            console.log(dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()


            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombres'),
              //DTColumnBuilder.newColumn('type').withTitle('Tipo'),
              DTColumnBuilder.newColumn('identity_document').withTitle('N° de Identificación'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) =>  {
          return`<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.messenger-edit({messengerId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }


        //
    }// constructor

    delete (messengerId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Mensajero!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('messengers').one('messenger', messengerId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }


    $onInit(){
    }
}

export const MessengerListsComponent = {
    templateUrl: './views/app/components/messenger-lists/messenger-lists.component.html',
    controller: MessengerListsController,
    controllerAs: 'vm',
    bindings: {}
}
