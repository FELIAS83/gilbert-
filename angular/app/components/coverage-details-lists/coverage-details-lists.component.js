class CoverageDetailsListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

         this.API = API
        this.$state = $state
        this.can = AclService.can


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleId = $stateParams.deductibleId
        console.log(deductibleId)
        let Coverage = API.service('coverage', API.all('coverage_details'))
        Coverage.one(deductibleId).get()
          .then((response) => {

            let dataSet = response.plain()
            console.log("data", dataSet.data)

            let DeductibleData = API.service('show', API.all('deductibles'))
            DeductibleData.one(deductibleId).get()
            .then((response) => {
                this.deductibleeditdata = API.copy(response)
                console.log("Data de deducible", this.deductibleeditdata.data)

                let PlanData = API.service('show', API.all('plans'))
                PlanData.one(this.deductibleeditdata.data.plan_id).get()
                .then((response) => {
                    this.planeditdata = API.copy(response)
                    console.log("data del plan", this.planeditdata.data)
                    
        
             })
        
            })


            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withOption('paging', false)
              .withOption('searching', false)
              .withOption('info', false)
              .withBootstrap()

           this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Cobertura'),
              DTColumnBuilder.newColumn('amount').withTitle('Monto Anual').renderWith(decimals),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })


           let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
          }

          let decimals = (data) => {
            return `
                          <div class="">
                            $ {{${data} |  number:2}}
                          </div>
                          `
          }
          //$compile(angular.element(row).contents())($scope)

          

          let actionsHtml = (data) =>  {
          return`<ul>
                    
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.coverage-details-edit({coverageDetailId:${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }
    delete (coverageDetailId) {
      let API = this.API
      let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la cobertura!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('coverage_details').one('coverage', coverageDetailId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const CoverageDetailsListsComponent = {
    templateUrl: './views/app/components/coverage-details-lists/coverage-details-lists.component.html',
    controller: CoverageDetailsListsController,
    controllerAs: 'vm',
    bindings: {}
}
