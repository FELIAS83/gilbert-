class ClaimEditController{
    constructor ($scope,API, $state, $stateParams,$http) {
        'ngInject';

        var vm = this
        this.API = API
        this.$state = $state
        let claimStatus = '0'
        this.formSubmitted = false
        this.showhidepregnant = false;
        this.alerts = []
        this.lines = []
        //this.lines.push({id : 0, disabled : false, req : ''});
        this.formSubmitted = false
        this.myFile = [] //array con datos de archivos
        this.systemDocTypes = []
        this.systemCurrency = []
        this.description = []
        this.systemClaims=[]
        this.value = []
        this.filenames=[]
        this.doc_typeselected = []
        this.curency_selected = []
        this.curency_selected[0] = 1
        this.files = []
        let claimId=$stateParams.claimId
        this.claimId = claimId
        this.success = 0
        //vm.changeDocType = changeDocType
        let policyId = $stateParams.policyId
        this.deletedlines = []
        this.processing = false
        this.typeDoc = []
        this.typeDocNew = []
        this.returnfiles = []
        this.psystemDescription = []
        this.systemDescription = []



        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let Policies = API.service('show', API.all('policies'))
        Policies.one(policyId).get()
        .then((response) => {
            this.policy = API.copy(response)
            console.log("x", this.policy.data)
            this.title = 'Reclamo #'+claimId+' Póliza #'+policyId+' | '+this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
        })    

          let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })

        this.systemDocTypes = systemDocTypes
     
        let systemDescriptionDoc = []
        let DocDescription = this.API.service('claimdocumenttypedescriptions')
        DocDescription.getList()
        .then((response) => {
            let descriptionResponse = response.plain()
            angular.forEach(descriptionResponse, function(value, index, arr){
                
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                
            })
            

        })
        
     
        this.systemDescriptionDoc = systemDescriptionDoc


        let Documents = API.service('show', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            let typeDoc = []
            let systemDescription = []
            this.previouslines = dataSet.data
            console.log("DATOS--",this.previouslines)
            for (let i = 0; i < this.previouslines.length; i++){
                //this.previouslines[i].amount = parseFloat(this.previouslines[i].amount)
                //console.log(this.previouslines[i].amount)
                if (this.previouslines[i].amount == 0){
                    this.previouslines[i].amount = ''

                }

                let description = this.previouslines[i].description
                typeDoc.push({id: i, value: this.previouslines[i].id_doc_type})
                let id_doc_type = this.previouslines[i].id_doc_type
                angular.forEach(this.systemDescriptionDoc, function(value){              
                    if(value.id_doc_type == id_doc_type){                    
                       systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 

                       if(description == value.name){
                            description = value.id
                       }
                 }

                })       
                this.previouslines[i].description = description
                this.psystemDescription[i] = systemDescription
            }
            this.typeDoc = typeDoc
            
            console.log("description", this.previouslines[1].description)
            console.log("systemDescription", this.psystemDescription)
            
          

        })

     


       let Currencies = this.API.service('currencies')
       Currencies.getList()
       //Currencies.one(ClaimId).get()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
            console.log("monedas ", this.systemCurrency)
        })

        
    

    /*function changeDocType(index){
        //console.log("aaaa", index)
        //console.log("q ", vm.lines[index].disabled)
        this.indice = index
        if (vm.doc_typeselected[index] == '1') {
            //this.disabled = false
            vm.lines[index].disabled = false
            vm.lines[index].rep = ''
            //this.noreq = ''
            this.curency_selected[index] = 1
        } else {
            //this.disabled = true
            vm.lines[index].disabled = true
            //this.noreq = 'No '
            vm.lines[index].rep = 'No '
           this.curency_selected[index] = 1
        }        
       }
       */
   }///constructor
    

   changeDocType(index,type){
        let systemDescription = []
        this.indice = index
        console.log("Type", type)
        let id_doc_select = this.doc_typeselected[index]
        if (type == 1){
            console.log("zzz", this.doc_typeselected[index])
            this.typeDocNew[index].value = this.doc_typeselected[index]
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
                this.curency_selected[index] = 1
                this.description[index] = ''
             }
             else {
                angular.forEach(this.systemDescriptionDoc, function(value){
                    if(value.id_doc_type == id_doc_select){
                        systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 
                    }

                    })
                console.log("systemDescription", systemDescription)
                this.systemDescription[index] = systemDescription
                this.lines[index].disabled = true
                this.lines[index].rep = 'No '
                this.curency_selected[index] = 1
                this.value[index] = ''
            }
       
        }
        else{//anteriores
            let id_doc_select = this.previouslines[index].id_doc_type
            console.log("seleccionado", this.previouslines[index].id_doc_type)
            this.typeDoc[index].value = this.previouslines[index].id_doc_type


            if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
                this.previouslines[index].currency_id = 1
                this.previouslines[index].description = ''
                }
            else {
                angular.forEach(this.systemDescriptionDoc, function(value){
                    if(value.id_doc_type == id_doc_select){
                        systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 
                    }
                    })

                this.psystemDescription[index] = systemDescription
                console.log("entro", this.psystemDescription[index])
                this.previouslines[index].disabled = true
                this.previouslines[index].rep = 'No '
                this.previouslines[index].currency_id = 1
                this.previouslines[index].amount = ''
            }
            

        }
        

    }       



    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});
        this.typeDocNew.push({id : newid, value: 1})
        this.files.push({name: ""})
        console.log('lineas',this.lines)
        console.log('seleccionados',this.doc_typeselected)
        console.log('descripciones',this.description)
        console.log('archivos',this.files)

    }

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)    
    }
    /*deleteLine(index){
        console.log("aaaa", index)
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.files.splice(pos,1)

        console.log('lineas',this.lines)
        console.log('seleccionados',this.doc_typeselected)
        console.log('descripciones',this.description)
        console.log('archivos',this.files)
    }*/
      deleteLine(index){//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.curency_selected.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
    }


    save (isValid) {
        let name_des = ""
        let id_type_selected = ""

         if (this.pfiles){
        for (var i = 0; i < this.previouslines.length; i++) {
            if (this.pfiles[i]) {
                this.previouslines[i].filename = this.pfiles[i][0].name
            }
        }}
             
        if (isValid) { 
            this.processing = true
            let $state = this.$state
            let filenames = []
        angular.forEach(this.files, function (value) {
              filenames.push(value.name)
        })
        this.filenames = filenames
        console.log("Files", this.filenames)
          
        var vm = this
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        //console.log("descripciones", this.previouslines)
        for (var i = 0; i < this.previouslines.length; i++) {
                
                if (this.previouslines[i].id_doc_type > 1 ) {
                   id_type_selected = this.previouslines[i].description
                   angular.forEach(this.systemDescriptionDoc, function(value){    
                        if(value.id == id_type_selected){
                                 name_des = value.name         
                        }
                   })
                   this.previouslines[i].description = name_des     
                }    
            }

        console.log("Tipo de docu new", this.typeDocNew)    
        console.log("description nuevas", this.description)    
        for (var i = 0; i < this.description.length; i++) {
                
                if (this.doc_typeselected[i] > 1 ) {
                   id_type_selected = this.description[i]
                   angular.forEach(this.systemDescriptionDoc, function(value){    
                        if(value.id == id_type_selected){
                                 name_des = value.name         
                        }
                   })
                   this.description[i] = name_des    
                   console.log(this.description)
                    
                    
                }    
            }
         console.log("descripciones viejos", this.previouslines)
         console.log("descripciones nuevos", this.description)  

            let documents = this.API.service('claims', this.API.all('claims'))
            console.log("previouslines", this.previouslines)
               documents.post({
                'id_claim'    : this.claimId,
                'id_doc_types' : this.doc_typeselected,
                'descriptions' : this.description,
                 'values'      : this.value,
                'currency_id' : this.curency_selected,
                 'modified'   : this.previouslines,
                'deleted_ids' : this.deletedlines,
                'filenames' : this.filenames,
                'returned' :  this.returnfiles


            }).then(function () {
                    vm.processing = false
                    console.log("mensaje", $state.current);
                    let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el reclamo.' }
                    $state.go($state.current, { alerts: alert,succes: 1, reload: $state.current})
                console.log("mensaje", $state.current);



                }, function (response) {
                        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                        $state.go($state.current, { alerts: alert})
                    })
                    //this.uploadFile()
                    console.log(this.alerts)
        }
        else {
            this.formSubmitted = true
        }
    }

    mostrar(){
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
    }
     $onInit(){
    }
}

export const ClaimEditComponent = {
    templateUrl: './views/app/components/claim-edit/claim-edit.component.html',
    controller: ClaimEditController,
    controllerAs: 'vm',
    bindings: {}
}
