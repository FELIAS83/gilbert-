class AssistentEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

         var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity
        var agentId = ""

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }


        let assistentId = $stateParams.assistentId
        let AssistentData = API.service('show', API.all('assistents'))
        AssistentData.one(assistentId).get()
          .then((response) => {
           this.assistenteditdata = API.copy(response)
            this.assistenteditdata.data.birthday =  new Date(this.assistenteditdata.data.birthday)
            agentId =  this.assistenteditdata.data.agent_id           
            console.log("Agente", agentId) 
            let AgentData = API.service('show', API.all('agents'))
            AgentData.one(agentId).get()
            .then((response) => {
            this.agenteditdata = API.copy(response)
            console.log("Datos agente:", this.agenteditdata.data)
            })
      })
        
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              if(value.id == vm.assistenteditdata.data.country_id){
                vm.country_id = {id: value.id, name: value.name}
                console.log("pais seleccionado", vm.country_id.id)
                let States = API.service('index', API.all('states'))
                States.one(value.id).get()
                    .then((response) =>{
                        let systemStates = []
                        let stateResponse = response.plain()
                        console.log(stateResponse)
                        angular.forEach(stateResponse.data.states, function (value){
                            if(value.id == vm.assistenteditdata.data.province_id){
                                vm.state_id = {id: value.id, name: value.name}
                                let Citys = API.service('index', API.all('cities'))
                                console.log("prov seleccionada", vm.state_id.id)
                                Citys.one(value.id).get()
                                    .then((response) => {
                                        let systemCities = []
                                        let cityResponse = response.plain()
                                        if (value.id == vm.assistenteditdata.data.city_id) {
                                            vm.city_id = {id: value.id, name: value.name}
                                            console.log("city seleccionada", vm.city_id)
                                        }                                        
                                        angular.forEach( cityResponse.data.cities, function (value){                                            
                                            if( value.province_id == vm.state_id.id ) {
                                                //vm.city_id = {id: value.id, name: value.name}
                                                //console.log("city seleccionada", vm.city_id)
                                                systemCities.push({id: value.id, name: value.name})
                                            }
                                        })
                                        vm.systemCities = systemCities
                                        console.log("systemCities", systemCities)
                                })                                    
                            }
                        if (vm.country_id.id == value.country_id) {
                            systemStates.push({id: value.id, name: value.name})
                        }                        
                        })                        
                        vm.systemStates = systemStates
                        console.log("systemStates", systemStates)
                        
                    })
              }
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
        })

        function getState(){
            console.log("country ", vm.country_id.id)
            //vm.countryRequired = true
            //console.log("Requerido", vm.countryRequired)
            let States = API.service('index', API.all('states'))
            States.one(vm.country_id.id).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                    //console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        if (vm.country_id.id == value.country_id) {
                            systemStates.push({id: value.id, name: value.name})
                        }
                    })
            this.systemStates = systemStates
            console.log("systemStates  ==> ", systemStates)
            vm.city_id = ""

            })

        }

        function getCity(){
            //vm.provinceRequired = true
            //vm.cityRequired = true
            console.log("state ", vm.state_id.id)
            let Citys = API.service('index', API.all('cities'))
            Citys.one(vm.state_id.id).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                    angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
            this.systemCities = systemCities
            
            })

        }




        

        //
    }//constructor
     save (isValid) {
        this.assistenteditdata.data.country_id = this.country_id.id
        this.assistenteditdata.data.province_id = this.state_id.id
        this.assistenteditdata.data.city_id = this.city_id.id
                if (isValid) {
                    let $state = this.$state
                    this.assistenteditdata.put()
                        .then(() => {
                        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se han actualizado los datos del asistente.' }
                        $state.go($state.current, { alerts: alert})
                    }, (response) => {
                        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                        $state.go($state.current, { alerts: alert})
                    })
                } else {
                
                this.formSubmitted = true
            }

        }

    $onInit(){
    }
}

export const AssistentEditComponent = {
    templateUrl: './views/app/components/assistent-edit/assistent-edit.component.html',
    controller: AssistentEditController,
    controllerAs: 'vm',
    bindings: {}
}
