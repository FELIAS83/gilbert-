class LiquidationsInvoicesController{
    constructor($state,API){
        'ngInject';

        let claimId = $state.params.claimId
        this.claimId = claimId
        let policyId = $state.params.policyId
        this.title = 'Reclamo #'+claimId+' Póliza #'+policyId
        this.directory = '/claims/'
        this.all = false
        this.API = API

        this.$state = $state

        let Currencies = API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, name: value.name, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
        })

        let Documents = API.service('claiminvoices', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.invoices = dataSet.data
            console.log(this.invoices)
            let checkedAll = true
            
            
            for (let i = 0; i < this.invoices.length; i++) {
                if (this.invoices[i].checked == false) {
                    checkedAll = false
                }
                
            }
            
            this.checkedAll = checkedAll
            console.log(this.checkedAll)
        })

    }
    selectAll (){
        if (this.all) {
            for (var i = 0; i < this.invoices.length; i++) {
                this.invoices[i].checked = true
            }
        }
        else{
            for (var i = 0; i < this.invoices.length; i++) {
                this.invoices[i].checked = false
            }

            this.active = false

        }
    }
    checkSelected(){
        let active = false
        angular.forEach(this.invoices, function(value){
            if (value.checked == true){
                active = true
            }
        })
        this.active = active
    }
    liquidate(){
        let $state = this.$state

        let liquidate = this.API.service('liquidate', this.API.all('claims'))

        liquidate.post({
            'invoices' : this.invoices
        }).then(function (response) {
            swal({
                title: 'Facturas Liquidadas correctamente',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go('app.claim-liquidations')
              })
        }, function (response) {
            swal({
                title: 'Error al liquidar facturas!',
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go($state.current)
              })
        
        })
    }

    $onInit(){
    }
}

export const LiquidationsInvoicesComponent = {
    templateUrl: './views/app/components/liquidations-invoices/liquidations-invoices.component.html',
    controller: LiquidationsInvoicesController,
    controllerAs: 'vm',
    bindings: {}
}
