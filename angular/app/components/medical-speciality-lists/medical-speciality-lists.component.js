class MedicalSpecialityListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject'
        this.API = API
        this.$state = $state

        let Medicalspecialties = this.API.service('medicalspecialties')
        Medicalspecialties.getList({}).then((response) => {
            let dataSet = response.plain()
            //console.log("xxx", dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })

            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              //DTColumnBuilder.newColumn('abbreviation').withTitle('Abreviatura'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.medical-speciality-edit({specialityId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

    }

    delete (medicalspecialityID) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la especialidad!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('medicalspecialties').one('medicalspecialties', medicalspecialityID).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const MedicalSpecialityListsComponent = {
    templateUrl: './views/app/components/medical-speciality-lists/medical-speciality-lists.component.html',
    controller: MedicalSpecialityListsController,
    controllerAs: 'vm',
    bindings: {}
}
