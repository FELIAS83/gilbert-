class ConfirmPolicyDataController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
        var vm = this
        var relationship = ""
        var orderRole = 0
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.client = ""
        this.role = ""
        this.planDeduc = ""
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        this.lines = []
        this.lines_1 = []
        this.birthdayaffiliate = []
        this.deletedamendments = []
        this.deletedannexes = []
        this.details = []
        this.fees = []
        this.completed = $stateParams.completed
        this.title = 'Confirmar Datos de Póliza'
        this.uisrefback = 'app.emission-policy-lists'
        if (this.completed == 1){
            this.title = 'Datos de Póliza'
            this.uisrefback = 'app.historical-emissions-lists'
        }

        let policyId = $stateParams.policyId
        this.policyId = policyId
          //getPolicyInfo
        let Policy = this.API.service('getPolicyInfo');
        let data = {};
        data.policy_id=   $stateParams.policyId;

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible
            this.coverage_type = this.policyinfo[0].is_international == 1 ? "Internacional" : "Nacional"            
            

            //this.policyinfo[0].birthday = new Date(this.policyinfo[0].birthday)
            this.role =  "Titular"
            let applicantId = this.policyinfo[0].applicant_id
            let Dependents = API.service('dependents', API.all('dependents'))
            Dependents.one(applicantId).get()
            .then((response) => {
                let dependentsResponse = response.plain()
                let dependentq = 0
                vm.dependentq = dependentq
                let spouse = 0
                vm.spouse = spouse
                let ageh = 0
                vm.ageh = ageh
                let inclusion_date = ""

                console.log(this.policyinfo[0].effective_date)
                angular.forEach(dependentsResponse.data, function (value) {
                    if(value.relationship == "H"){
                        relationship = "Cónyuge"
                        orderRole = 2
                        vm.ageh = vm.calculateAge(value.birthday)
                        vm.spouse = 1
                    }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        vm.dependentq++
                    }
                    inclusion_date = value.inclusion_date
                    //value.birthday = new Date(value.birthday)
                    
                    let age = vm.calculateAge(value.birthday)
                    systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, relationship: relationship, inclusion_Date: inclusion_date})
                    allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, role: value.relationship, order: orderRole, relationship: relationship, age: age, effective_Date: vm.policyinfo[0].effective_date, inclusion_Date: inclusion_date })
                  
                })

                let agetitular = this.calculateAge(this.policyinfo[0].birthday)
                allAffiliates.push({id: parseInt(this.policyinfo[0].id), name: this.client, role: "T", birthday: this.policyinfo[0].birthday ,order: 1, relationship: 'Titular' , age: agetitular, effective_Date: this.policyinfo[0].effective_date, inclusion_Date: this.policyinfo[0].inclusion_date})
                allAffiliates.sort(function (a, b) {
                    if (a.order > b.order) {
                        return 1;
                    }
                    if (a.order < b.order) {
                        return -1;
                    }
                    return 0;
                });

                systemDependents.sort(function (a, b){
                  return (b.relationship - a.relationship)
                })
                this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                console.log(this.allAffiliates)

                if (this.policyinfo[0].mode == 0){
                    this.payment_type = 'Anual'
                    this.amount_fees = 1
                }
                else if (this.policyinfo[0].mode == 1){
                    this.payment_type = 'Semestral'
                    this.amount_fees = 2
                }
                else if (this.policyinfo[0].mode == 2){
                    this.payment_type = 'Trimestral'
                    this.amount_fees = 4
                }
                else if (this.policyinfo[0].mode == 3){
                    this.payment_type = 'Mensual'
                    this.amount_fees = 12
                }

                if (isNaN(this.ageh)){
                    this.ageh = 0
                }

                let amounttitular = API.all('amounts/'+this.policyinfo[0].deductible_id+'/'+agetitular+'/'+this.ageh+'/'+this.dependentq+'/'+this.policyinfo[0].coverage_id+'/'+this.policyId)
                amounttitular.doGET('',{}).then((response) => {
                    dataSet = response.plain()
                    let amounttitular = dataSet.data.amounttitular
                    let amounth = dataSet.data.amounth
                    let amountchild = dataSet.data.amountchild
                    let coverage_name = dataSet.data.coverage_name
                    let coverage_amount = dataSet.data.coverage_amount
                    this.taxssc_percent = dataSet.data.taxssc
                    this.adminfee = dataSet.data.adminfee
                    this.iva = dataSet.data.iva
                    let savedfees = dataSet.data.fees

                    

                    this.total = 0
                    let datefees = new Date( vm.policyinfo[0].effective_date)
                    for (let i = 0; i < this.amount_fees; i++ ){                        
                        this.fees.push({fee_id : i+1})
                        this.fees[i].details = []
                        this.fees[i].details.push({id: 1, detail: 'P/H Premium', value: amounttitular/this.amount_fees})
                        if (this.spouse != 0){
                            this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: amounth/this.amount_fees})
                        }else{
                            this.fees[i].details.push({id: 2, detail: 'Spouse Premium', value: 0})
                        }

                        if (amountchild != 0){
                            let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: amountchild/this.amount_fees})
                        }else{
                            let detail = this.dependentq <=3 ? this.dependentq+' Childs' : '+3 Childs'
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: detail, value: 0})
                        }

                        if (coverage_amount != 0){
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: coverage_name, value: coverage_amount/this.amount_fees})
                        }else{
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: "", value: 0})
                        }

                        let sum = 0
                        this.totaldetails = 0
                        for (let j = 0; j < this.fees[i].details.length; j++){
                            sum = sum + this.fees[i].details[j].value
                            this.totaldetails = this.totaldetails + this.fees[i].details[j].value
                        }
                        if (this.policyinfo[0].mode == 1){
                            sum = sum * 0.06
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }
                        if ((this.policyinfo[0].mode == 2) || (this.policyinfo[0].mode == 3) ){
                            sum = sum * 0.1
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }

                        if (this.policyinfo[0].mode == 0) {
                            sum = 0;
                            this.fees[i].details.push({id: this.fees[i].details.length+1, detail: 'Installment Fee', value: sum})
                        }

                        let totalfee = 0
                        for (let k = 0; k < this.fees[i].details.length; k++){
                            totalfee = totalfee + this.fees[i].details[k].value
                        }
                        //descuento
                        this.fees[i].discounts = []
                        let discount = 0
                        if (this.policyinfo[0].discount == 1 ){
                            discount = ((totalfee-(coverage_amount/this.amount_fees))*this.policyinfo[0].discount_percent)/100
                            this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: discount })
                        }else{
                            discount = 0
                            this.fees[i].discounts.push({id: this.fees[i].discounts.length+1, detail: 'Descuento', value: 0 })
                        }

                        //impuesto
                        this.fees[i].taxes = []
                        let adminfeeiva = 0

                        if (i == 0){
                            if (savedfees != ''){//con cuota pendiente de pago
                                if (savedfees[i].adminfee == 1){
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: this.adminfee, check:true, checkvalue:true })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: this.adminfee*(this.iva/100) })
                                    adminfeeiva = this.adminfee + (this.adminfee*(this.iva/100))
                                }
                                else{
                                    this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                    this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                                }                                
                            }else{//sin couta, no generada o ya pagada
                                this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:true })
                                this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                            }
                        }else{
                            this.fees[i].taxes.push({id: 1, detail: 'Admin Fee', value: '', check:false })
                            this.fees[i].taxes.push({id: 2, detail: 'Tax (IVA)', value: '' })
                        }

                        //console.log("totaldetails ", totaldetails)
                        //console.log("sum ", sum)
                        //console.log("mode ", this.policyinfo[0].mode)
                        
                        //Tax SSC
                        if (savedfees != '') {
                            if (savedfees[i].tax_ssc == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                this.tax_ssc = true
                                console.log(this.taxssc)
                            }else{
                              console.log("sum", sum, "discount", discount, "tax percent", taxssc_percent)
                              this.taxssc = 0 //variable configuracion
                              this.tax_ssc = false
                              console.log(this.taxssc)  
                            }
                        } else if (this.policyinfo[0].is_international == 0){
                            console.log("es Nacional")
                            this.taxssc = (this.totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                            this.tax_ssc = true
                        } else {
                            /*if (this.policyinfo[0].is_international == 1) {
                                console.log("if fees para ssc ", this.fees[i])
                                let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                                console.log(taxssc)
                                this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                                this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                            } else {*/
                                console.log("else fees para ssc ", this.fees[i])
                                this.tax_ssc = false
                                this.taxssc = 0
                            //}
                            
                        }
                        //Tax SSC
                        this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: this.taxssc })
                        this.fees[i].total =  (totalfee+this.taxssc+adminfeeiva)-discount
                        //let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                        //this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                        //this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                        this.total = this.total + this.fees[i].total

                        


                        if (this.amount_fees == 1){
                            this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                        }
                        else if (this.amount_fees == 2){
                            if (i == 0){
                                this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 6)
                            }


                        }
                        else if (this.amount_fees == 4){
                            if (i == 0){
                                this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date =   datefees.setMonth(datefees.getMonth() + 3)
                            }


                        }
                        else if (this.amount_fees == 12){
                            if (i == 0){
                                this.fees[i].fee_payment_date = datefees.setDate(datefees.getDate()+1)
                            }
                            else{
                                this.fees[i].fee_payment_date = datefees.setMonth(datefees.getMonth() + 1)
                            }


                        }




//this.fees[i].fee_payment_date = myDate.setMonth(myDate.getMonth() + 1)

                        //console.log(this.fees[i].fee_payment_date)

                    }
                })
            })
        })

        let amendmentData = API.service('amendments', API.all('amendments'))
        amendmentData.one(policyId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.oldlines_1 = dataSet.data
            console.log('amendments',this.oldlines_1)
        })
        let annexData = API.service('annexes', API.all('annexes'))
        annexData.one(policyId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.oldlines = dataSet.data
            for (let i = 0; i < this.oldlines.length; i++ ){
                this.oldlines[i].effective_date = new Date( this.oldlines[i].effective_date+' ')
            }
        })
    }
    addFile() {
        this.lines.push({id : this.lines.length+1})
    }
    addFile_1() {
        this.lines_1.push({id : this.lines_1.length+1, type: 1})
    }
    deleteLine(index){
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
    }
    deleteLine_1(index){
        var pos = this.lines_1.indexOf(index);
        this.lines_1.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines_1, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines_1 = newlines
    }
    changeAffiliate(affiliate_id,index){
        console.log("id1 ", affiliate_id)
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.birthdayaffiliate[index] = this.allAffiliates[i].birthday
                this.lines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.lines_1',this.lines_1)
            }
        }

    }
    oldchangeAffiliate(affiliate_id,index){
        console.log("id ", affiliate_id)
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.oldlines_1[index].birthdate = this.allAffiliates[i].birthday
                this.oldlines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.oldlines_1',this.oldlines_1)
            }
        }
    }
    calculateAge(date){
        if (date != undefined){
            var date = date
            var values=date.split("-")
            var dia = values[2]
            var mes = values[1]
            var ano = values[0]
            var fecha_hoy = new Date()
            var ahora_ano = fecha_hoy.getYear()
            var ahora_mes = fecha_hoy.getMonth()+1
            var ahora_dia = fecha_hoy.getDate()
            var edad = (ahora_ano + 1900) - ano
            if ( ahora_mes < mes ){
                edad--
            }
            if ((mes == ahora_mes) && (ahora_dia < dia)){
                edad--
            }
            if (edad > 1900){
                edad -= 1900
            }
            var meses=0
            if(ahora_mes>mes)
                meses=ahora_mes-mes
            if(ahora_mes<mes)
                meses=12-(mes-ahora_mes)
            if(ahora_mes==mes && dia>ahora_dia)
                meses=11

            var dias=0
            if(ahora_dia>dia)
                dias=ahora_dia-dia
            /*if(ahora_dia<dia)
            {
                console.log('ahora_ano',ahora_ano,'ahora_mes',ahora_mes)
                ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
                console.log('ultimoDiaMes',ultimoDiaMes)
                dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
            }*/
            if (edad < 1){
                if (meses ==1){
                    return meses+' mes'
                }
                else{
                    return meses+' meses'
                }
            }
            else{
                return edad
            }
            }
    }
    olddeleteLine_1(index){
        this.deletedamendments.push({id : index.id})
        let pos = this.oldlines_1.indexOf(index);
        this.oldlines_1.splice(pos,1)
    }
    olddeleteLine(index){
        this.deletedannexes.push({id : index.id})
        let pos = this.oldlines.indexOf(index);
        this.oldlines.splice(pos,1)
    }
    changeAdminFee(fee,tax){
        if (this.fees[fee].taxes[tax].checkvalue == true){
            this.fees[fee].taxes[tax].value = this.adminfee //variable adminfee
        }
        else{
            this.fees[fee].taxes[tax].value = 0
        }
        this.fees[fee].taxes[tax+1].value = this.fees[fee].taxes[tax].value*(this.iva/100) //variable Tax (IVA)
        this.updateTotalFee(fee)
    }
    changeTaxSsc(){
        console.log("cambio ssc ", this.tax_ssc)
        if (this.tax_ssc) {
            console.log("if cuotas ", this.fees)
            //this.taxssc = 0            
            for (let i = 0; i < this.fees.length; i++ ){
                console.log("los pagos ", this.fees[i])
                console.log(i)
                //this.taxssc = (this.totaldetails)*(this.taxssc_percent/100)
                let sum = this.fees[i].details[4].value
                let discount = this.fees[i].discounts[0].value
                this.fees[i].taxes[2].value = (this.totaldetails+sum-discount)*(this.taxssc_percent/100)
                this.fees[i].total =  this.fees[i].total + this.fees[i].taxes[2].value
                //let taxssc = (totaldetails+sum-discount)*(this.taxssc_percent/100) //variable configuracion
                //this.fees[i].taxes.push({id: 3, detail: 'Tax (SSC)', value: taxssc })
                //this.fees[i].total =  (totalfee+taxssc+adminfeeiva)-discount
                this.total = this.total + this.fees[i].taxes[2].value 
            }
        } else {
            console.log("else cuotas ", this.fees)
            for (let i = 0; i < this.fees.length; i++ ){
                console.log("los pagos ", this.fees[i])
                console.log(i)
                this.fees[i].total = this.fees[i].total - this.fees[i].taxes[2].value
                this.total = this.total - this.fees[i].taxes[2].value
                this.fees[i].taxes[2].value = 0            
            }            
        }
        //this.updateFees()
    }
    updateFees(){
        console.log("pagos ", this.fees)
        //let index = 0
        for (let i = 0; i < this.fees.length; i++ ){
            console.log("los pagos ", this.fees[i])
            console.log(i)
            this.fees[i].total = this.fees[i].total - this.fees[i].taxes[2].value
            this.total = this.total - this.fees[i].taxes[2].value
            this.fees[i].taxes[2].value = 0            
        }
        /*this.fees.forEach(function(element){
            console.log("los pagos ", element)
            console.log("el ssc ", element.taxes[2].value)
            this.fees[index].taxes[2].value = 0
            this.total = this.total - element.taxes[2].value
            index += 1
        })*/
        
    }
    updateTotalFee(fee){
        let details = this.fees[fee].details
        let detailsum = 0
        details.forEach(function(element) {
            detailsum = detailsum + element.value
        })

        let discounts = this.fees[fee].discounts
        let discountsum = 0
        discounts.forEach(function(element) {
            discountsum = discountsum + element.value
        })

        let taxes = this.fees[fee].taxes
        let taxsum = 0
        taxes.forEach(function(element) {
            taxsum = taxsum + element.value
        })
        this.fees[fee].total = (detailsum + taxsum) - discountsum

        let fees = this.fees
        let total = 0
        fees.forEach(function(element) {
            total = total + element.total
        })
        this.total = total
    }

    save(isValid){
        console.log('fees ',this.fees)
        console.log("ssc ", this.tax_ssc)
        if (isValid){
            this.isDisabled = true;
            let confirm = this.API.service('confirm', this.API.all('policies'))
            let $state = this.$state
            confirm.post({
                'policyId': this.policyId,
                'number': this.policyinfo[0].number,
                'amendments': this.lines_1,
                'annexes': this.lines,
                'deletedamendments': this.deletedamendments,
                'deletedannexes': this.deletedannexes,
                'updatedamendments': this.oldlines_1,
                'updatedannexes': this.oldlines,
                'fees': this.fees,
                'tax_ssc': this.tax_ssc,
                'total_value': this.total,
                'mode': this.policyinfo[0].mode,
                'effective_date': this.policyinfo[0].effective_date
            }).then(function () {
                swal({
                    title: 'Confirmación de Póliza Exitosa!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.emission-policy-lists')
                })
            }, function (response) {
                swal({
                    title: 'Error al confirmar póliza',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })
            })
        }
        else{
            this.formSubmitted = true
        }
        console.log(this.lines_1)
    }

    $onInit(){
    }
}

export const ConfirmPolicyDataComponent = {
    templateUrl: './views/app/components/confirm-policy-data/confirm-policy-data.component.html',
    controller: ConfirmPolicyDataController,
    controllerAs: 'vm',
    bindings: {}
}
