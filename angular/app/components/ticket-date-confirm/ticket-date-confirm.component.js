class TicketDateConfirmController{
    constructor($scope, API, $http, $state,$filter,$stateParams,$compile, AclService, $uibModal){
        'ngInject';

      var vm = this
      this.API = API
      this.formSubmitted = false
      this.$state = $state
      var ticket_id=$stateParams.ticket_id
      this.ticket_id=ticket_id
      var id_policy=$stateParams.id_policy
      this.id_policy =id_policy
      this.alerts = []
      this.files=[]
      this.directory = '/tickets/'
      this.lines=[]
      this.$http=$http
      this.value=[]
      this.filenames=[]
      this.eliminados=[]
      this.modificados=[]
      this.doc_typeselected=[]
      this.deletedlines = []
      this.files=[]
      this.processing=false
      this.$uibModal=$uibModal
      this.llave = false
      this.llave2 = false  

       if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        } 

       console.log("ticket_id",this.ticket_id)

       console.log("id_policy",this.id_policy)     
       let TicketsData = API.service('show', API.all('tickets'))
       TicketsData.one(this.ticket_id).get()
        .then((response) => {
            this.ticketseditdata =API.copy(response)

            this.ticketseditdata.data[0].date_start =  new Date ($filter('date')(this.ticketseditdata.data[0].date_start,'yyyy-MM-dd'))      
            this.ticketseditdata.data[0].medical_speciality = parseInt(this.ticketseditdata.data[0].medical_speciality)
            if (this.ticketseditdata.data[0].date_end == null) {
              this.ticketseditdata.data[0].date_end =  undefined;//new Date(this.applicanteditdata.data[0].birthday)
            }else{
                var date_end = new Date(response.data[0].date_end)
                date_end = date_end.getTime() + 1000*60*60*24
                date_end = new Date(date_end)
                this.ticketseditdata.data[0].date_end =  date_end
            }

          
          
          
          this.ticketseditdata.data[0].hour = new Date("2018-01-01T"+this.ticketseditdata.data[0].hour)
          
          console.log("date_end", this.ticketseditdata.data[0].date_end)
          console.log("ticketseditdata=>", this.ticketseditdata.data)
          console.log("date_start ", this.ticketseditdata.data[0].date_start)

      })
      let systemDocTypes = []
      systemDocTypes.push({id: "1", name: "Formulario de Hospital"})
      systemDocTypes.push({id: "2", name: "Formulario de BD"})
      systemDocTypes.push({id: "3", name: "Informes Médicos"})
      systemDocTypes.push({id: "4", name: "Confirmación de Cita"})
      systemDocTypes.push({id: "5", name: "Formulario Interconsulta"})
      this.systemDocTypes = systemDocTypes
      console.log(this.systemDocTypes)


        let Documents = API.service('showdocuments', API.all('tickets'))
        Documents.one(ticket_id).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
            this.previouslines[0].id_doc_type =String(this.previouslines[0].id_doc_type)
            console.log("previouslines"+JSON.stringify(this.previouslines))            
        })          

      $scope.selected = {
        //item: items[0]
      }


        let doctors = this.API.service('doctors')
        doctors.getList({}).then((response) => {
            let dataSet = response.plain()
            this.doctors = dataSet
            console.log('doctors',this.doctors)
          })
        let hospital = this.API.service('hospitals')
        hospital.getList({}).then((response) => {
            let dataSet = response.plain()
            this.hospitals = dataSet
            console.log('hospitals',this.hospitals)
          })
        let medicalspecialties = this.API.service('medicalspecialties')
        medicalspecialties.getList({}).then((response) => {
            let dataSet = response.plain()
            this.medicalspecialties = dataSet
            console.log('medicalspecialties',this.medicalspecialties)
          })

  }

   changeDoctor(){
    console.log("entro en changeDoctor")
        this.llave = true
        let doctorData = this.API.service('show', this.API.all('doctors'))//ojo
        doctorData.one(this.ticketseditdata.data[0].doctor).get()
        .then((response) => {
            $(".js-example-basic-single4").select2({
                placeholder: "Seleccione Espcialidad"
             });
            let dataSet = this.API.copy(response)
            this.doctordata = dataSet.data.specialities
            console.log("doctordata",this.doctordata)
            
            return true
        })
    }

      ChangeDoctorBySpec(id){
        console.log("entro en ChangeDoctorBySpec")
        this.llave2 = true 
        console.log("this.medical_specialty",this.medical_speciality)
        console.log("id",id)
        let doctorbyspec = this.API.service('doctorbyspec', this.API.all('doctors'))
        doctorbyspec.one(id).get()
        .then((response) => {
            $(".js-example-basic-single2").select2({
                placeholder: "Seleccione Médico"
             });
            
             let systemdoctorbyspec = []
             let doctorbyspecResponse= response.plain()
                        
              angular.forEach(doctorbyspecResponse.data.data, function (value) {
              systemdoctorbyspec.push({id: value.doctor_id, names: value.names+' '+value.surnames})
               })
              this.systemdoctorbyspec=systemdoctorbyspec
           
            
            return true
        })
    }

    addFile (){
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});

            console.log('lineas',this.lines)
        
        console.log('archivos',this.files)
    }

    deletePreviousLine (index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    deleteLine (index) {//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('value',this.value)
        console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('value',this.value)
        console.log('files',this.files)
    }



      uploadFile () {
        var i = 0
            var files = this.files//archivos nuevos
            var pfiles = this.pfiles//archivos que sustituiran a los anteriores
            var ticket_id = this.ticket_id
            var $http = this.$http
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('ticket_id', ticket_id);
                $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   })
            })
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                form_data.append('ticket_id', ticket_id);
                $http.post('uploadtickets', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })
            this.processing = false

        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Los documentos han sido guardados exitosamente.'}
        this.alerts = [alert]
        //this.$state.reload();
    }

    save (isValid,ticket_id) {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })


        if (this.pfiles){
                for (var i = 0; i < this.previouslines.length; i++) {
                    if (this.pfiles[i]) {
                    this.previouslines[i].filename = this.pfiles[i][0].name
                    }
                }
            }
        console.log("isValid",isValid)
        if (isValid){

        this.processing = true

          let uploadFile = this.uploadFile
          let filenames = []
          angular.forEach(this.myFile, function (value) {
            filenames.push(value.name)
          })
          this.filenames = filenames
          var vm = this

          var form_data = new FormData();
          console.log("date_start2",this.ticketseditdata.data[0].date_start)
          //var date_start = $filter('date')(this.ticketseditdata.data[0].date_start,'yyyy-MM-dd')
          //var date_end = $filter('date')(this.ticketseditdata.data[0].date_end,'yyyy-MM-dd')
          //var hour = $filter('date')(this.ticketseditdata.data[0].hour,'H:mm')

          let eliminados=[]
          angular.forEach(this.deletedlines, function(value){
            eliminados.push(value.id)
          })
          this.eliminados=eliminados
          console.log("eliminados",this.eliminados)

          let modificados=[]
          angular.forEach(this.previouslines, function(value){
            modificados.push({id:value.id, id_doc_type: value.id_doc_type, filename: value.filename})
          })
          this.modificados=modificados
          console.log("modificados", this.modificados)

          
            if (this.ticketseditdata.data[0].hour !== null) {
                let tiempo=this.ticketseditdata.data[0].hour.toString()
                //console.log("tiempo"+tiempo)
                var hora=tiempo.substring(16,21)
                //console.log("hora"+hora)
            }
          
          console.log("id_doc_types",this.doc_typeselected)     
          console.log("doc_typeselected",this.doc_typeselected)
          console.log("modified",this.previouslines)
          console.log("filenames",this.filenames)
          console.log("deleted_ids",this.deletedlines)       
           
          //let Ticket = this.API.service('updateTicketdate');
          //let $state = this.$state
          //let $http=this.$http    

          let ticketdate = this.API.service('ticketdate', this.API.all('tickets')) 
            //console.log("ticket_id"+ticket_id)

            let $state = this.$state    
            ticketdate.post({
              'ticket_id': this.ticket_id,
              'date_start':this.ticketseditdata.data[0].date_start,
              'directory': this.directory,
              'hour': hora,
              'doctor':this.ticketseditdata.data[0].doctor,    
              'medical_speciality':this.ticketseditdata.data[0].medical_speciality,
              'hospital':this.ticketseditdata.data[0].hospital,
              'city':this.ticketseditdata.data[0].city,               
              'id_doc_types': this.doc_typeselected,
              'filenames':this.filenames,
              'modified': this.previouslines,
              'deleted_ids': this.deletedlines
           
          }).then(function(response){
              vm.uploadFile()
                 let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el ticket.' }
                $state.go($state.current, { alerts: alert})
                //console.log($state.current);
                }, function (response) {
                    let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                    $state.go($state.current, { alerts: alert})
                    })
        }
        else{
          this.formSubmitted = true
        }
      }
    
    modalDoctor(size) {
        let info = {}
        info.policy_id = this.policyId
        info.id_person = this.person
        info.city = this.ticketseditdata.data[0].city
        info.hospital = this.ticketseditdata.data[0].hospital
        info.medical_speciality = this.ticketseditdata.data[0].medical_speciality
        info.date_start = this.ticketseditdata.data[0].date_start
        info.date_end = this.ticketseditdata.data[0].date_end
        info.hour = this.ticketseditdata.data[0].hour
        info.systemTicketType = this.type
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalDoctor',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size
      })
    }

    modalHospital(size) {
        let info = {}
        info.policy_id = this.policyId
        info.id_person = this.person
        info.city = this.ticketseditdata.data[0].city
        info.doctor = this.ticketseditdata.data[0].doctor
        info.medical_speciality = this.ticketseditdata.data[0].medical_speciality
        info.date_start = this.ticketseditdata.data[0].date_start
        info.date_end = this.ticketseditdata.data[0].date_end
        info.hour = this.ticketseditdata.data[0].hour
        info.systemTicketType = this.type
        info.lines = this.lines
        info.doc_typeselected = this.doc_typeselected
        info.myFile = this.myFile
        info.files = this.files
        sessionStorage.setItem('info',JSON.stringify(info))
        console.log('modalhospitla', info)

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalHospital',
          controller: this.modalcontroller2,
          controllerAs: 'mvm',
          size: size
      })
    }
 /*   changeDoctor(){
        let doctorData = this.API.service('show', this.API.all('doctors'))//ojo
        doctorData.one(this.ticketseditdata.data[0].doctor).get()
        .then((response) => {
            let dataSet = this.API.copy(response)
            this.doctordata = dataSet.data.specialities
        })
    }
*/
    modalcontroller (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state
        console.log('doctoros')

        let Countrys = API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
            console.log('paises',this.systemCountrys)
        })



        this.getState = function(){
            
            console.log("pais",this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        
        this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }


        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist
        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                let doctors = API.service('doctors', API.all('doctors'))
                let $state = this.$state
                let cancel = this.cancel
                doctors.post({
                    'names': this.names,
                    'surnames': this.surnames,
                    'identification' : this.identification,
                    'address':this.address,
                    'phone':this.phone,
                    'country_id':this.country,
                    'province_id':this.state,
                    'city_id':this.city,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                     swal({
                             title: 'Médico guardado Correctamente!',
                             type: 'success',
                             confirmButtonText: 'OK',
                             closeOnConfirm: false
                         }, function () {
                             cancel()                                                                                                  
                             swal.close()
                             $state.reload()
                         })
                     , function (response) {
                         swal({
                             title: 'Error Inesperado!',
                             type: 'error',
                             confirmButtonText: 'OK',
                             closeOnConfirm: true
                         })
                     }
                  }) 
            } else {
                this.formSubmitted = true
            }
        }
    }

    modalcontroller2 (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state
        this.specialtiesselected = {}

        let Countrys = API.service('countrys')
        Countrys.getList()
        .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
                systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
        })

        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })

            this.specialties = specialtieslist
        })

        this.getState = function(){
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
            })
        }
        this.getCity = function(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
            })
        }

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                this.processing = true
                let hospitals = API.service('hospitals', API.all('hospitals'))
                let $state = this.$state
                let cancel = this.cancel

                hospitals.post({
                    'name': this.name,
                    'country_id' : this.country,
                    'province_id' : this.state,
                    'city' : this.city,
                    'address' : this.address,
                    'specialties' : this.specialtiesselected
                }).then(function () {
                    cancel()
                    $state.reload()
                }, function (response) {})
            } else {
                this.formSubmitted = true
            }
        }
    }
    

    $onInit(){
    }
}

export const TicketDateConfirmComponent = {
    templateUrl: './views/app/components/ticket-date-confirm/ticket-date-confirm.component.html',
    controller: TicketDateConfirmController,
    controllerAs: 'vm',
    bindings: {}
}
