class BranchListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';

        this.API = API
        this.$state = $state
        let companyId = $stateParams.companyId
        this.companyId =  companyId
        console.log("companyId", companyId)  
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let Company = this.API.service('show', this.API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()
            console.log('companydata',  this.companydata)

          })

        let Branchs = this.API.service('branches', this.API.all('branchs'))
        Branchs.getList({companyId})
            .then((response) =>{
                let dataSet = response.plain()
                this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Ramo'),
              DTColumnBuilder.newColumn('commission_percentage').withTitle('% Comisión').renderWith(dataPercen),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true

            })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
         let dataPercen = (data) => {
          return `
                    <div >
                      ${data} %
                    </div>
                  `
        }
         let actionsHtml = (data) => {
          return `<ul>                    
                    <a  title="Planes" class="btn btn-xs btn-info" ui-sref="app.plan-lists({branchId: ${data.id}, companyId: ${data.company_id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    <!--&nbsp
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.branch-edit({branchId: ${data.id}, companyId: ${data.company_id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>-->
                  </ul>`
        }

        //
    }

    delete (branchId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Ramo!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('branchs').one('branchs', branchId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const BranchListsComponent = {
    templateUrl: './views/app/components/branch_lists/branch_lists.component.html',
    controller: BranchListsController,
    controllerAs: 'vm',
    bindings: {}
}
