class SendInvoiceRenewalController{
    constructor($state,$stateParams,API,$http){
        'ngInject';
let vm = this
        this.$state = $state
        this.uploadFile = uploadFile
        this.API = API
        this.formSubmitted = false

        let policyId = $stateParams.policyId
        let feeId = $stateParams.feeId
        this.policyId = policyId
        this.feeId = feeId
        console.log(this.feeId)

        let Policy = API.service('getPolicyInfo');

        let data = {};
        data.policy_id=   $stateParams.policyId;

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
        })

        function uploadFile(){
            var files = vm.files
            let $state = vm.$state
            let name =  vm.files[0].name.split('.')
            let filename = name[0]+'_'+vm.feeId+'.'+name[1]

                angular.forEach(files, function(files){
                console.log("files",files)
                var form_data = new FormData();
                form_data.append('file', files);
                form_data.append('policy_id', vm.policyId);
                form_data.append('folder', 'authorization_form/');
                form_data.append('filename', filename);
                $http.post('uploadfilespaymentrenewal', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                })  .then( function ( response ) {
                    let confirm = vm.API.service('invoice', vm.API.all('renewals'))
                     confirm.post({
                'policyId': vm.policyId,
                'fee_id': vm.feeId,
                'filename': filename
            }).then( ( response ) => {
                        swal({
                            title: 'Factura Enviada!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.renewal-lists')
                        })
                    }, function (response) {
                        swal({
                            title: 'Error al Enviar Factura!',
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.renewal-lists')
                        })
                    }
                    )
                   
                })
                
            })
        }
    }
    save(isValid){
        console.log("fee_id", this.feeId)
        if (isValid){
            this.isDisabled = true;  
            let uploadFile = this.uploadFile
            uploadFile()
        }
        else{
            this.formSubmitted = true
        }
    }


    $onInit(){
    }
}

export const SendInvoiceRenewalComponent = {
    templateUrl: './views/app/components/send-invoice-renewal/send-invoice-renewal.component.html',
    controller: SendInvoiceRenewalController,
    controllerAs: 'vm',
    bindings: {}
}
