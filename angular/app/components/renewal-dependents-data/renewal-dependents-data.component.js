class RenewalDependentsDataController{
    constructor($scope, $stateParams, $state, API, $log, $http, $uibModal){
        'ngInject';
        var vm = this
        this.current_step = '3'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.lines = []
        this.dependents = []//falta validar que muestre los checks y que se muestre la informacion de la unidad
        this.heightunit = 'Cms'
        this.uisrefparameter = '{policyId: vm.policyId }'
        //this.uisrefback = 'app.emission-plan-config({policyId: vm.policyId})'
        this.outOfRange =  false
        this.$uibModal = $uibModal
        this.$log = $log
        this.dependentsdel = []
        this.linesdel = []
        let policyId = $stateParams.policyId
        this.policyId = policyId
        let feeId = $stateParams.feeId
        this.feeId = feeId
        console.log(policyId)
        this.completed = $stateParams.completed
        //console.log(this.completed)
        this.delDisabled = true
        this.max_dependent_age = 0 //Edad maxima para dependientes segun plan

        if (this.completed  == 1){
            this.uisrefparameter = ' {policyId: vm.policyId, completed: 1}'
            this.uisrefback = 'app.emission-applicant-data({policyId: vm.policyId, completed: 1})'
        }

        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              if ($stateParams.completed == 1){
                    if ((value.order == 1) || (value.order == 3)){
                        systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})
                    }
                }
                else{
                    systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})

                }
            })

            systemWizar.splice(3,1)
            //systemWizar.pop()
            this.systemWizar = systemWizar
        })

        let PolicyData  = API.service('policiyplan', API.all('renewals'))
        PolicyData.one(policyId).get()
          .then((response) => {
             this.PolicyData = API.copy(response)
            console.log("policy data ", this.PolicyData)
            this.max_dependent_age = this.PolicyData.data.max_dependent_age
          })

        let DependentData = API.service('show', API.all('dependents'))
        DependentData.one(policyId).get()
          .then((response) => {
            this.DependentData = API.copy(response)
            console.log("dependent ", this.DependentData.data)

            for (var i = 0; i < this.DependentData.data.length; i++){
                console.log('fechaf',new Date(this.DependentData.data[i].birthday))
                console.log('Relacion', this.DependentData.data[i].relationship)
                let today = new Date();
                let birthday = new Date(this.DependentData.data[i].birthday)
                let age = today.getFullYear() - birthday.getFullYear();
                let m = today.getMonth() - birthday.getMonth();

                if (m < 0 || (m === 0 && today.getDate() < birthday.getDate())) {
                            age--;
                }
                console.log("edad del dependiente", age)
                let alert = ""
                let count = 0
                if(age >= this.max_dependent_age && this.DependentData.data[i].relationship == "D")
                  {
                      alert = "Edad de cobertura excedida, se debe eliminar el Dependiente!!!"
                      count++
                  }
                this.lines.push({id : this.lines.length+1, new: false});
                this.dependents.push( {name : this.DependentData.data[i].first_name,
                                      lastname : this.DependentData.data[i].last_name,
                                      pid_type : this.DependentData.data[i].document_type,
                                      pid_num : this.DependentData.data[i].identity_document,
                                      role : this.DependentData.data[i].relationship,
                                      dob : new Date( this.DependentData.data[i].birthday+' '),
                                      height : parseFloat(this.DependentData.data[i].height, 2),
                                      height_unit_measurement : this.DependentData.data[i].height_unit_measurement,
                                      weight : parseFloat(this.DependentData.data[i].weight, 2),
                                      weight_unit_measurement : this.DependentData.data[i].weight_unit_measurement,
                                      sex : this.DependentData.data[i].gender,
                                      id : this.DependentData.data[i].id,
                                      age: age,
                                      alert: alert
                                    }
                                     
                                      )
            if(count > 0){
              this.isDisabled = true
            }
          }
     
        })

        let DependentDeleted = API.service('deletedependent', API.all('dependents'))
        DependentDeleted.one(policyId).get()
          .then((response) => {
            this.DependentDeleted = API.copy(response)
            console.log("dependent borrados ", this.DependentDeleted.data)

            for (var i = 0; i < this.DependentDeleted.data.length; i++){
                console.log('fechaf',new Date(this.DependentDeleted.data[i].birthday))
                console.log('Relacion', this.DependentDeleted.data[i].relationship)
                let today = new Date();
                let birthday = new Date(this.DependentDeleted.data[i].birthday)
                let age = today.getFullYear() - birthday.getFullYear();
                let m = today.getMonth() - birthday.getMonth();

                if (m < 0 || (m === 0 && today.getDate() < birthday.getDate())) {
                            age--;
                }
                
                this.linesdel.push({id : this.lines.length+1});
                this.dependentsdel.push( {name : this.DependentDeleted.data[i].first_name,
                                      lastname : this.DependentDeleted.data[i].last_name,
                                      pid_type : this.DependentDeleted.data[i].document_type,
                                      pid_num : this.DependentDeleted.data[i].identity_document,
                                      role : this.DependentDeleted.data[i].relationship,
                                      dob : new Date( this.DependentDeleted.data[i].birthday+' '),
                                      height : parseFloat(this.DependentDeleted.data[i].height, 2),
                                      height_unit_measurement : this.DependentDeleted.data[i].height_unit_measurement,
                                      weight : parseFloat(this.DependentDeleted.data[i].weight, 2),
                                      weight_unit_measurement : this.DependentDeleted.data[i].weight_unit_measurement,
                                      sex : this.DependentDeleted.data[i].gender,
                                      id : this.DependentDeleted.data[i].id,
                                      age: age,
                                      alert: alert,
                                      exist: true
                                    }
                                     
                                      )
            /*if(count > 0){
              this.isDisabled = true
            }*/
          }
     
        })





    }
    addLine() {
        this.lines.push({id : this.lines.length+1, new: true});
        console.log("Linea nueva", this.lines)
    }

    mostrar() {
        console.log(this.dependents)
    }

    save (Valid) {
        console.log("Entro a save", Valid)
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (Valid) {
          this.isDisabled = true;
          let Dependents = this.API.service('updatedependents', this.API.all('renewals'))
          let $state = this.$state

//          console.log("xxx ", this.dependents)
          Dependents.post({
            'dependents' : this.dependents,
            'aplicantid' : this.policyId
          }).then((response) =>  {
            response.feeId = this.feeId
            swal({

                title: 'Datos de Dependientes Guardados Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true

              }, function () {

                    $state.go('app.renewal-payment-information',{"policyId": response.data, "feeId": response.feeId})
                    
              })
          }, function (response) {
            swal({
                title: 'Error' + response.data.message,
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go($state.current)
              })
          })
        } else {
          this.formSubmitted = true
        }
    }


     motiveDependent (index,dependentData, max_dependent_age) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", max_dependent_age);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDependent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'md',
        resolve: {
          index: () => {
            return index
          },dependentData: () => {
            return dependentData
          },max_dependent_age: () => {
            return max_dependent_age
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    deleteLine (index){
     
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
        console.log('newlines',this.lines)
        this.dependents.splice(pos,1)
        //this.$state.reload()
        
    }
modalcontroller ($scope, $uibModalInstance, API, $http, $state, $stateParams, dependentData, max_dependent_age) {
    'ngInject'
        var mvm = this
        this.API = API
        this.$http = $http
        var $state = $state
        this.$state = $state
        this.dependentData = dependentData
        this.max_dependent_age = max_dependent_age
        this.processing = true
        this.deleting = true
        this.formSubmitted = false
        this.motive = 0
        this.alert = ""


        let systemMotive = []
        systemMotive = [{id: 1, name: "Salida Edad Excedida"},{id: 2, name: "Fallecimiento"},{id: 3, name: "Cambio de Compañia"},{id: 4, name: "Cambio de Plan"}, {id: 5, name: "Otros"}] 
        this.systemMotive = systemMotive


      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.changeMotive = () => {
        
        if(this.motive == 1 && mvm.dependentData.age < max_dependent_age){
          console.log("Entro al cambio")  
          this.alert = "El Dependiente que intenta eliminar aún no cumple la edad establecida"          
          this.processing = true
          this.deleting = true
        }else if(this.motive == 0){
          this.processing = true
          this.deleting = true
        }
        else{
          this.alert = ""          
          this.processing = false
        }

      }

      
      this.deleteDependent = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log("Dependiente a eliminar", mvm.dependentData)


        if (isValid){
          let $state = this.$state
            
          if(this.motive > 0){
              this.processing = true
              this.deleting = false
              console.log("Motivo", this.motive)

              console.log("no 24")
                let dependents = API.service('motivedependents', API.all('dependents'))
                    dependents.post({
                    'dependentId' : mvm.dependentData.id,
                    'motive': mvm.motive
                  }).then(function (response) {
                    //$state.go($state.current, {}, { reload: $state.current})         
                    $uibModalInstance.dismiss('cancel')
                
                }, function (response) {
              }) 

              if(this.motive == 1){
                swal({
                  title: 'Desea crear una nueva Póliza para '+mvm.dependentData.name+' '+mvm.dependentData.lastname +' ya que salió por tener 24 años?',
                  text: '' ,
                  type: 'warning',
                  showCancelButton: true,
                  cancelButtonText: 'No',
                  confirmButtonColor: '#DD6B55',
                  confirmButtonText: 'Sí, créala!',
                  closeOnConfirm: false,
                  showLoaderOnConfirm: true,
                  html: false
              }, function () {
                let newPolicy = API.service('createpolicydependents', API.all('renewals'))
                newPolicy.post({
                  'dependent': mvm.dependentData

                })
                .then((response) => {
                   swal({
                      title: 'Póliza creada!',
                      //text: 'Se han eliminado los permisos del usuario.',
                      type: 'success',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                }, function () {
                  //$state.reload()
                    console.log("Entro para eliminar")
                    /*let dependents = API.service('motivedependents', API.all('dependents'))
                    dependents.post({
                    'dependentId' : mvm.dependentData.id,
                    'motive': mvm.motive
                  }).then(function (response) {
                    $state.go($state.current, {}, { reload: $state.current})         
                    $uibModalInstance.dismiss('cancel')
                
                }, function (response) {
                   })*/
                })
              })
            })

        }

        $state.go($state.current, {}, { reload: $state.current})         
                  
          
      }
      else
      {
              this.formSubmitted = true
      }
            
         
    }
}
    
}
    $onInit(){
    }
}

export const RenewalDependentsDataComponent = {
    templateUrl: './views/app/components/renewal-dependents-data/renewal-dependents-data.component.html',
    controller: RenewalDependentsDataController,
    controllerAs: 'vm',
    bindings: {}
}
