class FeesPaidReportController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService,$uibModal, $log){
        'ngInject';

    var vm = this
    this.API = API
    this.$state = $state
    this.formSubmitted = false
    this.can = AclService.can
    this.DTOptionsBuilder = DTOptionsBuilder
    this.DTColumnBuilder = DTColumnBuilder
    var fecha = ""
    var arrfecha=""
    this.$uibModal = $uibModal
    this.$log = $log
    
    
    let Policy = API.service('policies', this.API.all('reports'))
        Policy.getList({})
          .then((response) => {
            let systemPolicy = []
            let policyResponse = response.plain()
            //console.log("Polizas", policyResponse)
            angular.forEach(policyResponse, function (value) {
              systemPolicy.push({id: value.id, name: value.first_name+' '+value.last_name, number: value.number })
            })

            this.systemPolicy = systemPolicy
            //console.log("systemCountrys==> ", systemCountrys)
        })
    



     let Report = API.service('feesreport', API.all('reports'))
        Report.one().get({})
        .then((response) => {
            this.report = response.plain()

             angular.forEach(this.report.data, function (value) {

              fecha = value.fee_payment_date
              arrfecha= fecha.split("-")
              var day=arrfecha[2]
              var month = arrfecha[1]
              var year= arrfecha[1]

              fecha= month+'/'+day+'/'+year  
              value.fee_payment_date = fecha
            })
              


            console.log("r ", this.report.data)
            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', this.report.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)              
              .withOption('info', false)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })

            this.dtColumns = [
              DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              DTColumnBuilder.newColumn('plan').withTitle('Plan'),
              DTColumnBuilder.newColumn('deductible').withTitle('Deducible'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('fee_number').withTitle('Cuota'),
              DTColumnBuilder.newColumn('mode').withTitle('Modo de Pago'),
              DTColumnBuilder.newColumn('total').withTitle('Monto Cancelado'),
              DTColumnBuilder.newColumn('fee_payment_date').withTitle('Fecha de Pago'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
              .renderWith(actionsHtml)
              
              
            ]

            this.displayTable = true

        }) 
        let createdRow = (row) => {
             $compile(angular.element(row).contents())($scope)
        }

         let actionsHtml = (data) => {
          return `<ul>
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Documentos Asociados" class="btn btn-xs btn-primary" ng-click="vm.viewDoc('lg', '${data.name}', ${data.id}, ${data.policy_id}, ${data.fee_number})">
                      <i class="fa fa-file" aria-hidden="true"></i>
                    </button>
                    &nbsp
                  </ul>`
        }

        //
    }
    viewDoc (size, name, id, policy_id, fee_number) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      //let items = this.items      

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDocuments',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'md',
        resolve: {
          customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          },fee_number: () => {
            return fee_number
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    //**********************************************//
  modalcontroller ($scope, $uibModalInstance, API, customer, id, $http, $state, $stateParams, policy_id, fee_number) {
    'ngInject'
    //console.log("controller")
      var mvm = this
      this.API = API
      this.customer = customer
      this.policyId = policy_id
      this.feeId = id
      this.fee_number = fee_number
      this.showInvoice = false
      this.showReceipt = false


        let data = {};
        data.policy_id = this.policyId
        let Policy = API.service('getPolicyInfo');
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            //console.log("dataSet", dataSet)
            this.policyinfo = dataSet[0]
            mvm.number = this.policyinfo.number
            //mvm.feeId = this.feeId

            let attached = {}
            attached.feeId =  this.feeId
            let feedocuments = API.service('feedocuments', API.all('feedocuments'))
            feedocuments.getList(attached).then((response) => {
                this.feedocuments = response.plain()
                //console.log('feedocuments',this.feedocuments)

            })

            let attacheds = API.service('feedocs', API.all('feedocuments'))
            attacheds.getList(attached).then((response) => {
                let dataSet2 = response.plain()
                //console.log(dataSet2)
                if(dataSet2[0].invoice.length != 0){
                  this.showInvoice = true
                }
                if(dataSet2[0].receipt.length != 0){
                  this.showReceipt = true
                }
                this.invoice_filename = dataSet2[0].invoice ? dataSet2[0].invoice : 'Sin archivo asociado'
                this.receipt_filename = dataSet2[0].receipt ? dataSet2[0].receipt : 'Sin archivo asociado'
                
            })
        })

    this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

    }
    $onInit(){
    }
}

export const FeesPaidReportComponent = {
    templateUrl: './views/app/components/fees-paid-report/fees-paid-report.component.html',
    controller: FeesPaidReportController,
    controllerAs: 'vm',
    bindings: {}
}
