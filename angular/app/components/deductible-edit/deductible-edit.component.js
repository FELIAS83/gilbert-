class DeductibleEditController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleId = $stateParams.deductibleId

        
        let DeductibleData = API.service('show', API.all('deductibles'))
        DeductibleData.one(deductibleId).get()
          .then((response) => {
            this.deductibleeditdata = API.copy(response)
            console.log("data del deductible", this.deductibleeditdata.data)
            this.deductibleeditdata.data.amount_in_usa = parseFloat(this.deductibleeditdata.data.amount_in_usa, 2)
            this.deductibleeditdata.data.amount_out_usa = parseFloat(this.deductibleeditdata.data.amount_out_usa, 2)
            //let planResponse = response.plain(response)

        
        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(this.deductibleeditdata.data.plan_id).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
        })
        
      })

        //
    }
    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          let $state = this.$state
          this.deductibleeditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado los datos del deducible.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const DeductibleEditComponent = {
    templateUrl: './views/app/components/deductible-edit/deductible-edit.component.html',
    controller: DeductibleEditController,
    controllerAs: 'vm',
    bindings: {}
}
