class EmissionRenewalPaymentController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

         var vm = this
        this.current_step = '5'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        $scope.form = {};
        vm.hideMount = true
        vm.discount = 0
        vm.discount_percent = 3

        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.uisrefparameter = ' {policyId: vm.policyId }'

         let PaymentData = API.service('show', API.all('payments'))
        PaymentData.one(policyId).get()
        .then((response) => {
            this.paymenteditdata = API.copy(response)
            //$scope.form.id = this.applicanteditdata.data.plan_id
            console.log("payment", this.paymenteditdata)
            console.log("payment", this.paymenteditdata.data.length)
            if(this.paymenteditdata.data.length > 0){
                this.mode = this.paymenteditdata.data[0].mode
                this.method =  this.paymenteditdata.data[0].method
                this.discount =  this.paymenteditdata.data[0].discount
                console.log("Descuento", this.discount_percent)
                this.discount_percent =  this.paymenteditdata.data[0].discount_percent == 0 ? vm.discount_percent : parseFloat(this.paymenteditdata.data[0].discount_percent, 2)
                console.log("Descuento", this.discount_percent)
                this.invoice_before = this.paymenteditdata.data[0].invoice_before
                if(this.discount == 1){

                       vm.hideMount =  false 
                }

            }else{
                this.discount = 0
                this.invoice_before = 0
                vm.hideMount =  true

            }

        })

        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefemisionren, order: value.order})
            })

            this.systemWizar = systemWizar
            console.log("systemWizar==> ", systemWizar)
        })


        //
    }

    showMount(){
        console.log("Entro")
        if(this.discount == 0){
            this.hideMount = true
            this.discount_percent = parseFloat("0", 2)
            

        }else{
            this.hideMount = false
            
       }

    }

     save(isValid){
        console.log("invoice_before", this.invoice_before)
        let $state = this.$state
        if (isValid) {      
            this.isDisabled = true;
            if (this.discount == '0') {
                this.discount_percent = 0
            }
            if(this.paymenteditdata.data.length == 0) {
                console.log("X el if")

                console.log("policyId ", this.policyId)
                console.log("mode ", this.mode)
                console.log("method ", this.method)
                console.log("discount", this.discount)
                console.log("discount_percent", this.discount_percent)
                console.log("invoice_before", this.invoice_before)
                let Payments = this.API.service('payments', this.API.all('payments'))
                let $state = this.$state                    
                Payments.post({
                    'policyId' : this.policyId,
                    'mode': this.mode,
                    'method' : this.method,
                    'discount' : this.discount,
                    'discount_percent' : this.discount_percent,
                    'invoice_before' : this.invoice_before
                  }).then(function (response) {
                    swal({
                        title: 'Datos de Pago Guardados Correctamente!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                            $state.go('app.emission-renewal-start-coverage', {"policyId": response.data})
                    })

                  }, function (response) {
                    swal({
                        title: 'Error ' + response.data.message,
                        type: 'error',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                            $state.go($state.current)
                    })
                  })            
              }// if para guardar por primera vez
              else{// para update
                    this.paymenteditdata.data[0].mode = this.mode
                    this.paymenteditdata.data[0].method = this.method
                    this.paymenteditdata.data[0].discount = this.discount
                    this.paymenteditdata.data[0].discount_percent = this.discount_percent
                    this.paymenteditdata.data[0].invoice_before = this.invoice_before
                    
                    this.paymenteditdata.data = this.paymenteditdata.data[0]
                    //console.log("data a actualizar", this.paymenteditdata.data)

                    this.paymenteditdata.put()
                    .then((response) => {
                      swal({
                            title: 'Datos de Pago Guardados Correctamente!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.emission-renewal-start-coverage', {"policyId": response.data})
                        })
                    }, (response) => {
                      swal({
                            title: 'Error ' + response.data.message,
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go($state.current)
                        })

                    })

              }

        } else { 
          this.formSubmitted = true
        } 
    }

    $onInit(){
    }
}

export const EmissionRenewalPaymentComponent = {
    templateUrl: './views/app/components/emission-renewal-payment/emission-renewal-payment.component.html',
    controller: EmissionRenewalPaymentController,
    controllerAs: 'vm',
    bindings: {}
}
