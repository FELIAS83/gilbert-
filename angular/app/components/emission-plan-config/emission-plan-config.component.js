class EmissionPlanConfigController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.current_step = '2'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        $scope.form = {};
        vm.hideCoverage = true
        vm.getDeductible = getDeductible
        //vm.getCoverages = getCoverages
        vm.plan_id = []
        vm.coverage_id = []
        vm.deductible_id = []
        var allDeductibles = []
        var allCoverages = []
        var allCoveragesDet = []
        var coverAmount = ""
        var nullArray = {id: 0, name: "- Ninguna -", plan_id: 0, amount: ""}


        let policyId = $stateParams.policyId
        this.policyId = policyId
        //console.log(policyId)

        let Agents = this.API.service('agents')
        Agents.getList()
        .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            angular.forEach(agentsResponse, function (value) {
                systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
        })


        let ApplicantData = API.service('show', API.all('policies'))
        ApplicantData.one(policyId).get()
          .then((response) => {
            this.applicanteditdata = API.copy(response)
            $scope.form.id = this.applicanteditdata.data[0].plan_id
            console.log("systemApplicant==> ", this.applicanteditdata.data)
            vm.plan_id = {id: this.applicanteditdata.data[0].plan_id }
            vm.deductible_id =  { id: this.applicanteditdata.data[0].deductible_id }       
            vm.coverage_id = {id: this.applicanteditdata.data[0].coverage_id }
            vm.seller_id = this.applicanteditdata.data[0].agent_id
            //console.log("cobertura", vm.coverage_id)
            //console.log("cobertura", vm.coverage_id)
            //console.log("deductible", vm.deductible_id)
                let Plans = this.API.service('plans')
                Plans.getList()
                  .then((response) => {
                    let systemPlans = []
                    let planResponse = response.plain()
            
                    angular.forEach(planResponse, function (value) {
                      
                    systemPlans.push({id: value.id, name: value.name})
                    })

                    this.systemPlans = systemPlans
                    
                     })

                   let Deductible = this.API.service('deductibles')
                    Deductible.getList()
                        .then((response) => {
                        let systemDeductibles = []
                         let deductiblesResponse = response.plain()
                        //console.log("respuesta deducibles", deductiblesResponse)
                        angular.forEach(deductiblesResponse, function (value) {
                            
                            if(value.plan_id == vm.applicanteditdata.data[0].plan_id)
                            {
                    
                                systemDeductibles.push({id: value.id, name: value.name, plan_id: value.plan_id, in: value.amount_in_usa, out: value.amount_out_usa})

                            }
                            allDeductibles.push({id: value.id, name: value.name, plan_id: value.plan_id, in: value.amount_in_usa, out: value.amount_out_usa})
                        })

                        this.systemDeductibles = systemDeductibles
                        this.allDeductibles = allDeductibles
                       // console.log("allDeductibles==> ", allDeductibles)
                        //console.log("systemDeductibles==> ", systemDeductibles)
                        })

                        let CoverageDetails = this.API.service('coverage_details')
                        CoverageDetails.getList()
                        .then((response) => {
                        let systemCoveragesDet = []
                        let coverageDetResponse = response.plain()
                           //console.log("coverageDetResponse ", coverageDetResponse)
                           //console.log("vm.deductible_id.id  ", vm.deductible_id.id )
                        angular.forEach(coverageDetResponse, function (cover) {

                                 if(vm.deductible_id.id == cover.deductible_id){

                                                coverAmount = cover.amount

                                  }else{

                                                coverAmount = cover.amount
                                  }

                                allCoveragesDet.push({id: cover.id, amount: cover.amount, deductible_id:cover.deductible_id, coverage_id: cover.coverage_id})
                            })   

                            
                        })
                        this.allCoveragesDet = allCoveragesDet
                        console.log("Todas las coberturas",coverAmount)

                    let Coverage = this.API.service('coverages')
                    Coverage.getList()
                        .then((response) => {
                        let systemCoverages = [] 
                        let coveragesResponse = response.plain()
                        systemCoverages.push({id: 0, name: "- Ninguna -", plan_id: 0, amount: ""})     
                        angular.forEach(coveragesResponse, function (value) {
                             
                             if(vm.plan_id.id == value.plan_id){

                                systemCoverages.push({id: value.id, name: value.name, plan_id: value.plan_id, amount: " | $ "+coverAmount})  
                                //systemCoverages = systemCoverages.concat(nullArray)   
                                vm.hideCoverage = false
                                //vm.coverage_id = {id: value.id, name: value.name, plan_id: value.plan_id, amount: coverAmount} 

                            }else{

                                vm.hideCoverage = true
                                
                                //vm.coverage_id = {id: 0, name: "", plan_id: value.plan_id, amount: 0} 

                            }

                            allCoverages.push({id: value.id, name: value.name, plan_id: value.plan_id, amount: coverAmount})
                            allCoverages = allCoverages.concat(nullArray)

                    })

                    this.systemCoverages =  systemCoverages
                    this.allCoverages = allCoverages

                    //console.log("systemCoverages==> ", allCoverages)
                    //console.log("vm.coverage_id==> ", vm.coverage_id)


                    })
   
        })


        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})
            })

            this.systemWizar = systemWizar
            //console.log("systemWizar==> ", systemWizar)
        })

        

        function getDeductible(){
            let flag = false
            let systemDeductibles = []
            let systemCoverages =[]
            vm.coverAmount = "0"
            angular.forEach(allDeductibles, function (value) {
                
                if(vm.plan_id.id == value.plan_id){
                     
                    systemDeductibles.push({id: value.id, name: value.name, plan_id: value.plan_id, in: value.in, out: value.out})
                
                }


            })
                        
            angular.forEach(allCoverages, function (coverage) {
               // console.log("allCoverages", allCoverages)
                if(vm.plan_id.id == coverage.plan_id && coverage.plan_id != 0 ){
                   console.log("Entro",  vm.hideCoverage)
                   flag = true
                   systemCoverages.push({id: coverage.id, name: coverage.name, plan_id: coverage.plan_id, amount: "| "+coverage.amount+" $"}) 
                   //systemCoverages = systemCoverages.concat(nullArray)      

                
                }else{
                    
                    systemCoverages = systemCoverages.concat(nullArray) 

                }
                
            })


            if(flag ==  true) {

                   vm.hideCoverage = false
            }else{
                    vm.hideCoverage = true
                    vm.coverage_id = {id: 0}
                    
            }


            this.systemDeductibles =  systemDeductibles
            this.systemCoverages =  systemCoverages 
            
           
            

            //console.log("Deducibles", systemDeductibles)
            console.log("Coberturas", this.systemCoverages)

        }  
        



    }//constructor

    save (isValid) {

        if (isValid) {
            this.isDisabled = true;
          console.log("Plan id", this.plan_id.id)  
          console.log("Deductible Id", this.deductible_id.id)
          console.log("Coverage Id", this.coverage_id.id)  
          console.log("agente Id", this.seller_id)  
          this.applicanteditdata.data = this.applicanteditdata.data[0]
          this.applicanteditdata.data.plan_id = this.plan_id.id
          this.applicanteditdata.data.deductible_id = this.deductible_id.id
          this.applicanteditdata.data.coverage_id = this.coverage_id.id
          this.applicanteditdata.data.seller_id = this.seller_id
          console.log("update", this.applicanteditdata)
          let $state = this.$state
          this.applicanteditdata.put()
            .then((response) => {
              swal({
                title: 'Datos del Plan Guardados Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go('app.emission-dependents-data', {"policyId": response.data})
              })
            }, (response) => {
              swal({
                title: 'Error:' + response.data.message,
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go($state.current)
            })
            })
        } else {
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const EmissionPlanConfigComponent = {
    templateUrl: './views/app/components/emission-plan-config/emission-plan-config.component.html',
    controller: EmissionPlanConfigController,
    controllerAs: 'vm',
    bindings: {}
}
