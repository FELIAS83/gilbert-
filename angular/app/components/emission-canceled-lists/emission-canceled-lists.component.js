class EmissionCanceledListsController{
     constructor(API,DTOptionsBuilder,DTColumnBuilder,$compile,$scope,$state){
        'ngInject';
        this.API = API
        this.$state = $state
        var fecha = ""
        var arrfecha=""
        var fecha1 = ""
        var arrfecha1=""
        let data = {}
        var motivo = ""

        let systemMotive = []
        systemMotive = [{id: 1, name: "Cambio de Compañia"},{id: 2, name: "Temas Económicos"},{id: 3, name: "Cambio de Agencia"},{id: 4, name: "Otros"}] 
        this.systemMotive = systemMotive

        let Policies = API.service('getCanceledPolicy')
        Policies.one().get()
          .then((response) => {
            let dataSet = response.plain()
            console.log("dataSet",dataSet.data)

            angular.forEach(dataSet.data.policies, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= month+'/'+day+'/'+year  
              value.created = fecha1

              if (value.motive == 1){
                motivo = "Cambio de Compañia" 
                value.motive=motivo
              }
              else if (value.motive == 2){
                motivo = "Temas Económicos" 
                value.motive=motivo
              }
              else if (value.motive == 3){
                motivo = "Cambio de Agencia" 
                value.motive=motivo
              }
              else if (value.motive == 4){
                motivo = "Otros"
                value.motive=motivo 
              }
            })

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data.policies)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza').withOption('width', '8%'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('created').withTitle('Fecha de Creación').withOption('width', '15%'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura').withOption('width', '15%'),
              DTColumnBuilder.newColumn('motive').withTitle('Motivo').withOption('width', '15%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(actionsHtml).withOption('width', '16%')
            ]

            this.displayTable = true
          })
        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }
        let actionsHtml = (data) => {
          return `<ul>
                      <button style="color: #23a031; background-color: #FFF;" title="Activar Póliza" class="btn btn-xs btn-success" ng-click="vm.activate( ${data.id})">
                        <i class="fa fa-check-square"></i>
                    </button>
                    &nbsp
                  </ul>`
        }

    }

    activate(policyId){
      let API = this.API
      let $state = this.$state
      console.log("policyId", policyId)
        swal({
        title: 'Está seguro?',
        text: 'Desea activar la póliza ?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, actívala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        let Activate = API.service("activate", API.all("renewals"))
        Activate.post({
          'policyId': policyId
        })
          .then(() => {
            swal({
              title: 'Activada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })

    }

    $onInit(){
    }
}

export const EmissionCanceledListsComponent = {
    templateUrl: './views/app/components/emission-canceled-lists/emission-canceled-lists.component.html',
    controller: EmissionCanceledListsController,
    controllerAs: 'vm',
    bindings: {}
}
