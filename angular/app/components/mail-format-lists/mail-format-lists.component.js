class MailFormatListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API){
        'ngInject';

         this.API = API
    this.$state = $state
    this.modulo = 'Formatos'


    let list = this.API.service('email_formats')
    list.getList()
      .then((response) => {
        let dataSet = response.plain()
console.log(dataSet)
        this.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('data', dataSet)
          .withOption('createdRow', createdRow)
          .withOption('responsive', true)
          .withBootstrap()

        this.dtColumns = [
          DTColumnBuilder.newColumn('id').withTitle('ID'),
          DTColumnBuilder.newColumn('name').withTitle('Nombre'),
          DTColumnBuilder.newColumn('title').withTitle('Título'),
          DTColumnBuilder.newColumn('subject').withTitle('Asunto'),
          DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
            .renderWith(actionsHtml)
        ]

        this.displayTable = true
      })

    let createdRow = (row) => {
      $compile(angular.element(row).contents())($scope)
    }

    let actionsHtml = (data) => {
      return `

                <a class="btn btn-xs btn-warning" ui-sref="app.mail-format-edit({emailformatId: ${data.id}})">
                    <i class="fa fa-edit"></i>
                </a>
                &nbsp
                <button class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                    <i class="fa fa-trash-o"></i>
                </button>`
    }
  }
    delete (userId) {
    let API = this.API
    let $state = this.$state

    swal({
      title: 'Estás seguro?',
      text: 'Se eliminará el formato de correo!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: false
    }, function () {
      API.one('mail_formats').one('format', userId).remove()
        .then(() => {
          swal({
            title: 'Eliminado!',
             text: 'El formato de correo ha sido eliminado.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })
    })
  }
    
    $onInit(){
    }
}

export const MailFormatListsComponent = {
    templateUrl: './views/app/components/mail-format-lists/mail-format-lists.component.html',
    controller: MailFormatListsController,
    controllerAs: 'vm',
    bindings: {}
}
