class TicketListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal){
        'ngInject';

        this.API = API
        this.$state = $state
        this.$scope=$scope
        this.$uibModal = $uibModal 
        this.animationsEnabled = true       
         var systemDependents = []
        var allAffiliates = [] 
        var allDependents = []
        var relationship = ""
        var orderRole = 0
        var fecha = ""
        var arrfecha=[]
        var fecha1 = ""
        var arrfecha1=[]
        let Tickets = this.API.service('tickets')
        
        Tickets.getList({})
          .then((response) => {
            let dataSet = response.plain()
            this.lista=dataSet

             angular.forEach(dataSet, function (value) {

              fecha = value.date_start
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= arrfecha[1]+'/'+arrfecha[0]+'/'+arrfecha[2]  
              value.date_start = fecha

            /*  console.log("date_start", value.date_start)
              console.log("arrfecha",arrfecha[0],arrfecha[1],arrfecha[2])
              console.log("mes, dia ,año",month, day, year)
              console.log("fechainicio",fecha)*/

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= arrfecha1[1]+'/'+arrfecha1[0]+'/'+arrfecha1[2]  
              value.created = fecha1

             
            /*console.log("created", value.created)
            console.log("arrfecha1",arrfecha1[0],arrfecha1[1],arrfecha1[2])
            console.log("mes1 , dia1 ,año1",month, day, year)
            console.log("fechacreada",fecha1)*/
            
           })
            console.log("la data ", dataSet)
            //console.log("lista ", JSON.stringify(this.lista))
             
             
           this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('ticket_id').withTitle('ID Ticket'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Asegurado'),
              DTColumnBuilder.newColumn('type_name').withTitle('Tipo'),
              DTColumnBuilder.newColumn('date_start').withTitle('Fecha de Cita'),
              DTColumnBuilder.newColumn('created').withTitle('Fecha de Creación'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)  
            ]

            this.displayTable = true
          })

          let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        
            let actionsHtml = (data) => {
          return `
                    <a class="btn btn-xs btn-warning" style="color: orange; background-color: #FFF;width:24.2px;height:21.98px" ui-sref="app.ticket-edit({ticket_id: ${data.ticket_id}, id_policy: ${data.id_policy} })" title="Editar Ticket">
                      <i class="fa fa-edit"></i>
                    </a>                    
                    &nbsp
                    <button ng-class="{'btn btn-xs btn-primary' : ${data.request_medical == 0}, 'btn btn-xs btn-default' : ${data.request_medical == 1}  }" ng-style="{'color': ${data.request_medical == 1} ? '#ecf0f5' : '#3c8dbc'}" style="background-color: #FFF;" ng-click="vm.modalDate('lg', ${data.ticket_id}, ${data.id_policy})" title="Solicitar Cita">
                      <i class="fa fa-user-md"></i>
                    </button>
                    &nbsp
                    <button ng-class="{'btn btn-xs btn-primary' : ${data.confirmed_date == 0}, 'btn btn-xs btn-default' : ${data.confirmed_date == 1}  }" ng-style="{'color': ${data.confirmed_date == 1} ? '#ecf0f5' : '#3c8dbc'}" style="background-color: #FFF;width:24.2px;height:21.98px" ui-sref="app.ticket-date-confirm({ ticket_id: ${data.ticket_id}, id_policy: ${data.id_policy}})" title="Confirmar Fecha">
                      <i class="fa fa-calendar"></i>
                    </button>      
                    &nbsp
                     <button ng-class="{'btn btn-xs btn-default' : (${data.confirmed_date} == 0 && ${data.ticket_types_id} == 1) || (${data.guarantee_letter} == 1) ,'btn btn-xs btn-primary' : ((${data.confirmed_date} == '0' && ${data.ticket_types_id} == '1') || (${data.guarantee_letter} == '1')) == false }" ng-style="{'color': (${data.confirmed_date} == 0 && ${data.ticket_types_id} == 1) || (${data.guarantee_letter} == 1) ? '#ecf0f5' : '#3c8dbc'}" style="background-color: #FFF;width:24.2px;height:21.98px"  ng-disabled="((${data.confirmed_date} == '0' && ${data.ticket_types_id} == '1'))" ng-click="vm.modalSend('lg', ${data.ticket_id}, ${data.id_policy})" title="Solicitar Carta de Garantía">
                      <i class="fa fa-send"></i>
                    </button>
                    &nbsp
                    <!--<button title="Cerrar Ticket" style="color: red; background-color: #FFF;width:24.2px;height:21.98px" class="btn btn-xs btn-danger" ng-click="vm.closed(${data.id})">
                        <i class="fa fa-times"></i>
                    </button>
                    &nbsp-->

                    <button title="Eliminar Ticket" style="color:red; background-color: #FFF;width:24.2px;height:21.98px" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.ticket_id})">

                      <i class="fa fa-trash-o"></i>
                    </button>
                    &nbsp
                    <a style="color:#3c8dbc; background-color: #FFF;width:24.2px;height:21.98px"  title="Comentarios"  class="btn btn-xs btn-primary" ui-sref="app.ticket-comment({policyId: ${data.id_policy}})" >
                        <span ><i class="fa fa-comment"></i></span>
                    </a>
                  `
        }
        //console.log(JSON.stringify(this.seleccionados))
    }


    delete (ticketId) {
      let API = this.API
      let $state = this.$state
      console.log(ticketId)

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Ticket!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('tickets').one('ticket', ticketId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    modalInOpen (size, ticket_id, id_policy) {//modal de confirmar fechas
      var vm =this
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items
     
      console.log("ticket_id"+ticket_id)     
      console.log("id_policy"+id_policy)
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModaldate',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          ticket_id: () => {
            return ticket_id
          },id_policy: () => {
            return id_policy
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        //$log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

  modalcontroller ($scope, $uibModalInstance, API, $http, $state,$filter,ticket_id,id_policy,$stateParams) {
    'ngInject'
      console.log("controller")
      var mvm=this      
    }      
    modalSend (size, ticket_id, id_policy) {//modal de solicitud de carta de garantia
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items
     
      console.log("ticket_id"+ticket_id)     
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalSend',
        controller: this.modalsendcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          ticket_id: () => {
            return ticket_id
          },id_policy: () => {
            return id_policy
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        //$log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

  modalsendcontroller ($scope, $uibModalInstance, API, $http, $state,$filter,ticket_id,id_policy,$stateParams) {
    'ngInject'
      console.log("controller")
      var mvm =this
      this.API = API
      this.$state = $state
      this.ticket_id = ticket_id
      this.id_policy = id_policy
      this.alerts = []
      mvm.directory = '/tickets/'
      //this.uploadFile = uploadFile
      
      mvm.processing = false

       if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        } 


      let emailformatId = 11
        //console.log(companyId)
      let EmailFormat = API.service('show', API.all('email_formats'))
      EmailFormat.one(emailformatId).get()
        .then((response) => {
          this.emailformatdata = API.copy(response)
          console.log("emailformatdata1", JSON.stringify(this.emailformatdata))
      })    

      let Ticketdata = API.service('showticket', API.all('tickets'))
      Ticketdata.one(this.ticket_id).get()
      .then((response) => {
        this.ticketdata= API.copy(response)
        this.emailformatdata.data.subject = this.emailformatdata.data.subject.replace("policy_number",this.ticketdata.data[0].number)
        this.emailformatdata.data.subject = this.emailformatdata.data.subject.replace("applicant_name",this.ticketdata.data[0].applicants_name)
        this.emailformatdata.data.content = this.emailformatdata.data.content.replace("applicant_name",this.ticketdata.data[0].applicants_name)
        this.emailformatdata.data.content = this.emailformatdata.data.content.replace("hospital_name",this.ticketdata.data[0].hospital_name)
        this.emailformatdata.data.content = this.emailformatdata.data.content.replace("motive",this.ticketdata.data[0].type_name)
        console.log(response)       
          console.log("ticketdata=>", JSON.stringify(this.ticketdata))
           if (this.ticketdata.data[0].date_end == null){
            this.ticketdata.data[0].date_end = ""
        }
        else{
          this.ticketdata.data[0].date_end = " - "+this.ticketdata.data[0].date_end
        }

        if (this.ticketdata.data[0].hour == null){
            this.ticketdata.data[0].hour = ""
        }
        else{
          this.ticketdata.data[0].hour = this.ticketdata.data[0].hour
        }

        if (this.ticketdata.data[0].doctor_name == null){
            this.ticketdata.data[0].doctor_name = ""
        }
        else{
          this.ticketdata.data[0].doctor_name = this.ticketdata.data[0].doctor_name
        }

        if (this.ticketdata.data[0].speciality_name == null){
            this.ticketdata.data[0].speciality_name = ""
        }
        else{
          this.ticketdata.data[0].speciality_name = this.ticketdata.data[0].speciality_name
        }

                        "<td width='50%' style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Paciente</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].name+"</td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Póliza</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].number+" </td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Hospital</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].hospital_name+" </td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Médico</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].doctor_name+" </td>"+
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Especialidad</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].speciality_name+" </td>"+ 
                      '</tr>'+
                      '<tr>'+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Fecha</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].date_start+" "+this.ticketdata.data[0].date_end+" </td>"+
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Hora</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].hour+"</td>"+ 
                      "</tr>"+
                  "</table>";


       document.getElementById("datosticket").innerHTML = this.tabla
      })

        let Documents = API.service('showdocuments', API.all('tickets'))
        Documents.one(ticket_id).get()
        .then((response) => {
            let dataSet = API.copy(response)
            mvm.previouslines = dataSet.data
            console.log("previouslines"+JSON.stringify(mvm.previouslines))            
        })

              let systemDocTypes = []
        systemDocTypes.push({id: "1", name: "Formulario de Hospital"})
        systemDocTypes.push({id: "2", name: "Formulario de BD"})
        systemDocTypes.push({id: "3", name: "Informes Médicos"})
        systemDocTypes.push({id: "4", name: "Confirmación de Cita"})
        systemDocTypes.push({id: "5", name: "Formulario Interconsulta"})
        this.systemDocTypes = systemDocTypes
        console.log(this.systemDocTypes)


      $scope.selected = {
        //item: items[0]
      }      

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.sendoc = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })              
          if (isValid){
            mvm.processing = true
            let $state = this.$state        
            var file = mvm.file
            var ticket_id = mvm.ticket_id
            if (file==null){
                console.log("entra en null")
                console.log(file)

              console.log("file"+JSON.stringify(file))
              console.log("content",this.emailformatdata.data.content)
                                               
              API.service('send').post({
                policy_id : mvm.id_policy,
                ticket_id: ticket_id,
                format_id : 11,
                content: this.emailformatdata.data.content,
                person : this.ticketdata.data[0].name,
                doctor : this.ticketdata.data[0].doctor_name,
                hospital : this.ticketdata.data[0].hospital_name,
                speciality: this.ticketdata.data[0].speciality_name,
                date_start: this.ticketdata.data[0].date_start,
                date_end: this.ticketdata.data[0].date_end,
                hour: this.ticketdata.data[0].hour,
                data:this.tabla
                })
                  .then( ( response ) => {
                     swal({      
                            title: 'Solicitud de Confirmación Enviada Correctamente!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                          }, function () {
                                $uibModalInstance.dismiss('cancel')
                                $state.reload()
                            })
                          }, function (response) {
                            console.log('error')
                          })
          }
          else{//si sube archivo
              console.log("file"+JSON.stringify(file))
              console.log("entra en file")
              console.log("ticket_id"+ticket_id)
              angular.forEach(file, function(file){
                  var form_data = new FormData();
                  form_data.append('policy_id', mvm.id_policy);
                  form_data.append('ticket_id', ticket_id);
                  form_data.append('file', file)
                  console.log("form_data"+JSON.stringify(form_data))
                  $http.post('uploadticketsletter', form_data,
                  {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                  }).success(function(response){  
                      API.service('send').post({
                          policy_id : mvm.id_policy,
                          ticket_id: ticket_id,
                          format_id : 11,
                          file      : file,
                          filename: mvm.myFile.name,
                          content: this.emailformatdata.data.content,
                          person : this.ticketdata.data[0].name,
                          doctor : this.ticketdata.data[0].doctor_name,
                          hospital : this.ticketdata.data[0].hospital_name,
                          speciality: this.ticketdata.data[0].speciality_name,
                          date_start: this.ticketdata.data[0].date_start,
                          date_end: this.ticketdata.data[0].date_end,
                          hour: this.ticketdata.data[0].hour,
                          data:this.tabla
                                 })
                        .then( ( response ) => {
                           API.service('deleteFiles').post({
                            ticket_id: ticket_id,
                            filename: mvm.myFile.name           
                       }),
                            swal({      
                                title: 'Solicitud de Confirmación Enviada Correctamente!',
                                type: 'success',
                                confirmButtonText: 'OK',
                                closeOnConfirm: true

                            }, function () {
                                $uibModalInstance.dismiss('cancel')
                                $state.reload()
                            })
                        }, function (response) {
                            console.log('error')
                          })
                      })
                  })//cierre de foreach
                }//cierre de else sube archivo
              }//cierre de isvalid
          else{
              this.formSubmitted = true
          }     
      }
  }


  modalDate (size, ticket_id, id_policy) {//modal de solicitud de carta de garantia
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items
     
      console.log("ticket_id"+ticket_id)     
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDate',
        controller: this.modaldatecontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          ticket_id: () => {
            return ticket_id
          },id_policy: () => {
            return id_policy
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        //$log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

  modaldatecontroller ($scope, $uibModalInstance, API, $http, $state,$filter,ticket_id,id_policy,$stateParams) {
    'ngInject'
      console.log("controller")
      this.API = API
      this.$state = $state
      this.ticket_id = ticket_id
      this.id_policy = id_policy
      this.alerts = []
      //this.uploadFile = uploadFile
      var mvm =this
      mvm.processing = false
      this.tabla=""
      this.div=""
      this.Html=" "
      this.MyVar=""

       if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        } 


      let emailformatId = 13
        //console.log(companyId)
      let EmailFormat = API.service('show', API.all('email_formats'))
      EmailFormat.one(emailformatId).get()
        .then((response) => {
          this.emailformatdata = API.copy(response)
          console.log("emailformatdata2", this.emailformatdata.data)
      })    

      let TicketsData = API.service('showticket', API.all('tickets'))
      TicketsData.one(this.ticket_id).get()
      .then((response) => {
        this.ticketdata= API.copy(response)
        this.emailformatdata.data.subject = this.emailformatdata.data.subject.replace("policy_number",this.ticketdata.data[0].number)
        this.emailformatdata.data.subject = this.emailformatdata.data.subject.replace("applicant_name",this.ticketdata.data[0].applicants_name)
        console.log("data ", this.emailformatdata.data.subject)
        //console.log(response)       
        console.log("ticketdata=>", this.ticketdata.data[0])
        if (this.ticketdata.data[0].date_end == null){
            this.ticketdata.data[0].date_end = ""
        }
        else{
          this.ticketdata.data[0].date_end = " - "+this.ticketdata.data[0].date_end
        }

        if (this.ticketdata.data[0].hour == null){
            this.ticketdata.data[0].hour = ""
        }
        else{
          this.ticketdata.data[0].hour = this.ticketdata.data[0].hour
        }

        if (this.ticketdata.data[0].doctor_name == null){
            this.ticketdata.data[0].doctor_name = ""
        }
        else{
          this.ticketdata.data[0].doctor_name = this.ticketdata.data[0].doctor_name
        }

        if (this.ticketdata.data[0].speciality_name == null){
            this.ticketdata.data[0].speciality_name = ""
        }
        else{
          this.ticketdata.data[0].speciality_name = this.ticketdata.data[0].speciality_name
        }

       
                        "<td width='50%' style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Paciente</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].name+"</td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Póliza</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].number+" </td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Hospital</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].hospital_name+" </td>"+ 
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Médico</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].doctor_name+" </td>"+
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Especialidad</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].speciality_name+" </td>"+ 
                      '</tr>'+
                      '<tr>'+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Fecha</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].date_start+" "+this.ticketdata.data[0].date_end+" </td>"+
                      "</tr>"+
                      "<tr>"+
                        "<td style='height:50%;text-align:left;font-weight:bold; font-size:12px;border:1px solid black;padding:2px 2px 2px 2px;'>Hora</td>"+
                        "<td style='height:50%;text-align:left;border:1px solid black;font-size:12px;padding:2px 2px 2px 2px;'>"+this.ticketdata.data[0].hour+"</td>"+ 
                      "</tr>"+
                  "</table>";


       document.getElementById("datosticket").innerHTML = this.tabla

      })

      let Documents = API.service('showdocuments', API.all('tickets'))
      Documents.one(ticket_id).get()
        .then((response) => {
            let dataSet = API.copy(response)
            mvm.previouslines = dataSet.data
            console.log("previouslines"+JSON.stringify(mvm.previouslines))            
        })

        
           

      $scope.selected = {
        //item: items[0]
      }      

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.sendoc = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })              
          if (isValid){
            mvm.processing = true
            let $state = this.$state        
            var file = mvm.file
            var ticket_id = mvm.ticket_id
            if (file==null){
                console.log("entra en null")
                console.log(file)
                console.log("content",this.emailformatdata.data.content)

              console.log("file"+JSON.stringify(file))
              console.log("ticket_id",ticket_id)
              console.log("id_policy",mvm.id_policy) 
                                   
              API.service('send').post({
                policy_id : mvm.id_policy,
                ticket_id: ticket_id,
                format_id : 13,
                person : this.ticketdata.data[0].name,
                doctor : this.ticketdata.data[0].doctor_name,
                hospital : this.ticketdata.data[0].hospital_name,
                speciality: this.ticketdata.data[0].speciality_name,
                date_start: this.ticketdata.data[0].date_start,
                date_end: this.ticketdata.data[0].date_end,
                hour: this.ticketdata.data[0].hour,
                content: this.emailformatdata.data.content,
                data:this.tabla
                
                })
                  .then( ( response ) => {
                     swal({      
                            title: 'Solicitud de Cita Enviada Correctamente!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                          }, function () {
                                $uibModalInstance.dismiss('cancel')
                                $state.reload()
                            })
                          }, function (response) {
                            console.log('error')
                          })
          }
          else{//si sube archivo
              console.log("file"+JSON.stringify(file))
              console.log("entra en file")
              console.log("ticket_id"+ticket_id)
              angular.forEach(file, function(file){
                  var form_data = new FormData();
                  form_data.append('policy_id', mvm.id_policy);
                  form_data.append('ticket_id', ticket_id);
                  form_data.append('file', file)
                  console.log("form_data"+JSON.stringify(form_data))
                  $http.post('uploadticketsletter', form_data,
                  {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                  }).success(function(response){  
                      API.service('send').post({
                          policy_id : mvm.id_policy,
                          ticket_id: ticket_id,
                          format_id : 13,
                          file      : file,
                          filename: mvm.myFile.name,
                          person : this.ticketdata.data[0].name,
                          doctor : this.ticketdata.data[0].doctor_name,
                          hospital : this.ticketdata.data[0].hospital_name,
                          speciality: this.ticketdata.data[0].speciality_name,
                          date_start: this.ticketdata.data[0].date_start,
                          date_end: this.ticketdata.data[0].date_end,
                          hour: this.ticketdata.data[0].hour,
                          content: this.emailformatdata.data.content,
                          data: this.tabla
                       })
                        .then( ( response ) => {
                           API.service('deleteFiles').post({
                            ticket_id: ticket_id,
                            filename: mvm.myFile.name           
                       }),
                            swal({      
                                title: 'Solicitud de Cita Enviada Correctamente!',
                                type: 'success',
                                confirmButtonText: 'OK',
                                closeOnConfirm: true

                            }, function () {
                                $uibModalInstance.dismiss('cancel')
                                $state.reload()
                            })
                        }, function (response) {
                            console.log('error')
                          })
                      })
                  })//cierre de foreach
                }//cierre de else sube archivo
              }//cierre de isvalid
          else{
              this.formSubmitted = true
          }     
      }
  }

    $onInit(){
    }
}

export const TicketListsComponent = {
    templateUrl: './views/app/components/ticket-lists/ticket-lists.component.html',
    controller: TicketListsController,
    controllerAs: 'vm',
    bindings: {}
}
