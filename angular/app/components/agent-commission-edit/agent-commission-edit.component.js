class AgentCommissionEditController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        //vm.getBranches = getBranches
        //vm.getPlans = getPlans
        var allBranches = []
        var allPlans = []
        vm.branch = []
        vm.provider = []
        vm.plans = []
        vm.processing = false
        vm.message = "Guardar"
        vm.provider_button = true
        vm.branch_button = false
        vm.plan_button = false
        vm.branch_list = false

          if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let commissionId = $stateParams.commissionId
        let agentId = $stateParams.agentId
        this.commissionId = commissionId
        this.agentId =  agentId

        console.log("agentId", agentId)
        //
        let CommissionData = API.service('commission', API.all('commissions'))
        CommissionData.one(commissionId).get()
          .then((response) => {
            this.commissioneditdata = API.copy(response)
            console.log("data", this.commissioneditdata)
            let commissionResponse = response.plain(response)
            console.log("response", commissionResponse)
            vm.provider = {id:this.commissioneditdata.data.company_id, name: ""}
            vm.branch = {id:this.commissioneditdata.data.branch_id, name:""}
            vm.plans = {id:this.commissioneditdata.data.plan_id, name:""}
            console.log(typeof(vm.plans.id))
            this.commissioneditdata.data.commission = parseFloat(this.commissioneditdata.data.commission,2)
            this.commissioneditdata.data.commission_renewal = parseFloat(this.commissioneditdata.data.commission_renewal,2)
            //this.commissioneditdata.data.commission_branch = parseFloat(this.commissioneditdata.data.commission_branch,2)
            //this.commissioneditdata.data.commission_plan = parseFloat(this.commissioneditdata.data.commission_plan,2)

            if(this.commissioneditdata.data.branch_id > 0 && this.commissioneditdata.data.plan_id == 0){
              vm.provider_button = false
              vm.branch_list = true
              vm.branch_button = true
              vm.plan_button = false
              
            }else if(this.commissioneditdata.data.plan_id > 0){
                vm.provider_button = false
                vm.branch_button = false
                vm.branch_list = true
                vm.plan_button = true
            }

            

            

       
          let BranchesData = API.service('branchs')
          BranchesData.getList({})
            .then((response) =>{
              let dataBranches = response.plain()
              let systemBranches = []    
                angular.forEach(dataBranches, function(value){
                  if(value.company_id == vm.provider.id){
                      systemBranches.push({id:value.id, name:value.name, providerId:value.company_id})
                  }
                  allBranches.push({id:value.id, name:value.name, providerId:value.company_id})
              })
            
            this.systemBranches = systemBranches
            this.allBranches =  allBranches
          })

          let PlansData = API.service('plans')
          PlansData.getList({})
            .then((response) =>{
              let dataPlans =  response.plain()
              let systemPlans = []
              console.log("dataPlans", dataPlans)
              angular.forEach(dataPlans, function(value){
                if(value.branch_id == vm.branch.id){
                    systemPlans.push({id:value.id, name:value.name, branchId:value.branch_id})
                }
                allPlans.push({id:value.id, name:value.name, branchId:value.branch_id})
             })

             this.systemPlans = systemPlans
             this.allPlans =  allPlans
        })

        })
        
          let AgentData = API.service('show', API.all('agents'))
          AgentData.one(agentId).get()
            .then((response) => {
               this.dataagent = response.plain()
          })  

          let ProviderData = API.service('companies')
          ProviderData.getList({})
            .then((response) =>{
              let dataProvider = response.plain()
              //console.log(dataProvider)
              let systemProviders = []
              angular.forEach(dataProvider, function(value){
                systemProviders.push({id:value.id, name:value.name})
              })
                this.systemProviders = systemProviders 
          })  
    }

    

      getBranches(){
        console.log("Entro..", this.provider)
        this.rowStatus = false
        var provider = this.provider
        let systemBranches = []
        
        if(provider != null){
          angular.forEach(this.allBranches, function (value) {
              if(value.providerId == provider.id){
                systemBranches.push({id: value.id, name: value.name, providerId:value.providerId })
              }
          
          })
            this.systemBranches = systemBranches
            this.systemPlans = ""
            this.processing = false
        }else{
          this.systemBranches = []
          this.systemPlans = []
          this.processing = true
          
        }    
      }

      getPlans(){
        console.log("Entro..Branch", this.allPlans)
        this.rowStatus = false;
        var branch = this.branch
        let systemPlans = []
          if(branch != null){
             angular.forEach(this.allPlans, function (value) {
                  if(value.branchId == branch.id){
                    systemPlans.push({id: value.id, name: value.name, branchId:value.branchId })
                   }
              })
              this.systemPlans = systemPlans  
          }else{
            this.systemPlans = [] 
            
          }
        }

        save(isValid){

          if(isValid){
            this.message = "Guardando"
            this.processing = true
            let $state = this.$state
            this.commissioneditdata.data.company_id = this.provider.id
            var exist = true 
            this.commissioneditdata.data.branch_id = (this.branch == null) ? 0 : this.branch.id 
            this.commissioneditdata.data.plan_id = (this.plans == null)  ? 0 : this.plans.id
            var verify = this.API.all('verifycommission/'+this.provider.id+'/'+this.branch.id+'/'+this.plans.id+'/'+this.agentId+'/'+this.commissioneditdata.data.commission+'/'+this.commissioneditdata.data.commission_renewal)
           verify.doGET('',{}).then((response) => {
              let dataSet = response.plain()
              exist = dataSet.data
              console.log("exist",exist)
              if(exist.response){
                this.processing = false
                this.message = "Guardar"
                swal({
                  title: 'Importante!',
                  text: exist.message,
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true,
                  timer: 3000
                }, function () {   
                    $state.go($state.current, {})
                })    

              }else{
                this.commissioneditdata.put()
                  .then((response) => {
                    this.processing = false
                    swal({
                      title: 'Comisión actualizada con Éxito!',
                      type: 'success',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                    }, function () {
                      $state.go('app.agent-commission-list',{"agentId": response.data})
                  })    
                }, (response)=>{
                    swal({
                      title: 'Error:' + response.data.message,
                      type: 'error',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                    }, function () {
                        $state.go($state.current)
                    })

                })

              }
            })
          }else {
                
                this.formSubmitted = true
            }
        }

    $onInit(){
    }
}

export const AgentCommissionEditComponent = {
    templateUrl: './views/app/components/agent-commission-edit/agent-commission-edit.component.html',
    controller: AgentCommissionEditController,
    controllerAs: 'vm',
    bindings: {}
}
