class DiagnosticsEobController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http, $log){
        'ngInject';

      this.API = API
      this.claimId = $stateParams.claimId
      this.policy_id = $stateParams.policyId
      this.$state = $state
      //this.sendmail = sendmail
      this.diagnosticId = {}
      this.isDisabled = true
      var vm = this
      //this.processing = false
      this.$uibModal = $uibModal
      this.items = ['item1', 'item2', 'item3']
      this.$log = $log
      console.log("claimId", this.claimId)

      let diagnostics = API.service('claimdiagnostics', API.all('diagnostics'))
      diagnostics.one(this.claimId).get()
        .then((response) => {
          let dataSet = response.plain()
          this.diagnostics = dataSet.data
          console.log("diagnostics", this.diagnostics)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', this.diagnostics)
              .withOption('order', [[3, 'desc']])
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('Id'),
              DTColumnBuilder.newColumn('description').withTitle('Descripción'),
              DTColumnBuilder.newColumn('name').withTitle('Asegurado'),
              DTColumnBuilder.newColumn('person_type').withTitle('Tipo'),
              //DTColumnBuilder.newColumn('created_at').withTitle('Creación del Trámite'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
            
            this.allClaim = dataSet


            let Policy = API.service('show', API.all('policies'))
                    Policy.one(this.policy_id).get()
                        .then((response) =>{
                             let policydata =  API.copy(response)
                             vm.policyNumber = policydata.data[0].number
                             console.log("Poliza", policydata)  
                        })



          })
          
        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }



        let actionsHtml = (data) => {

          return `  <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Subir Docs." class="btn btn-xs btn-primary" ng-click="vm.sendDocument('lg', '${data.name}',${this.claimId}, ${ this.policy_id},${data.id})">
                      <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </button>
                    &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar EOB al Cliente" class="btn btn-xs btn-primary" ng-show="${data.show}" ui-sref="app.send-eob({claimId: ${data.claim_id}, policyId: ${this.policy_id}, diagnosticId: ${data.id}})" >
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </button>
                    `
        }

        //
    }

    sendDocument (size, name, id, policy_id, diagnostic_id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", id);

      console.log("policy_id", policy_id);
      console.log("Asegurado", name);

      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDocument',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'lg',
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          },diagnostic_id: () => {
            return diagnostic_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }


modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state, $stateParams, policy_id, diagnostic_id) {
    'ngInject'
        var mvm = this
        this.API = API
        this.$http = $http
        var $state = $state
        this.$state = $state
        let claimId = $stateParams.claimId
        this.claimId = claimId
        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.lines = []
        this.curency_selected = []
        this.curency_selected[0] = 1
        this.value = []
        this.files = []
        this.deletedlines = []
        this.doc_typeselected = []
        this.description = []
        this.provider_id = []
        this.directory = '/claims/'
        this.formSubmitted = false
        this.alerts = []
        this.returnfiles = []
        this.processing = false
        this.typeDoc = []
        this.typeDocNew = []
        this.doc_date = []

        this.customer = customer
        //this.claim_id = id
        this.diagnostic_id =  diagnostic_id
        //this.policy_id = policy_id

        console.log("Poliza", this.policyId)
        console.log("Reclamo", this.claimId)
        console.log("Diagnsotico", this.diagnostic_id)
       

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

         this.lines.push({id : 0});
         this.typeDocNew.push({id : 0, value: 1})

        let systemDocTypes = []
        systemDocTypes.push({id: "1", name: "Factura"})
        systemDocTypes.push({id: "2", name: "Informe"})
        systemDocTypes.push({id: "3", name: "Orden"})
        systemDocTypes.push({id: "4", name: "Resultado"})
        this.systemDocTypes = systemDocTypes
        console.log("a ", this.systemDocTypes)



        let Policies = API.service('show', API.all('policies'))
        Policies.one(policyId).get()
        .then((response) => {
            this.policy = API.copy(response)
            console.log("r ", this.policy.data)
            this.title = 'Reclamos #'+claimId+' Póliza #'+policyId+' | '+this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
        })

        /*
        let Documents = API.service('show', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let typeDoc = []
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
                

            for (let i = 0; i < this.previouslines.length; i++){

                

                if (this.previouslines[i].amount == 0){
                    this.previouslines[i].amount = ''

                }
                //console.log("Tamaño del archivo", this.previouslines[i].filename.length)
                if ( this.previouslines[i].filename == '' || this.previouslines[i].filename.length <= 1 ){
                    this.previouslines[i].isUpload = true

                }else{
                    this.previouslines[i].isUpload = false
                }

                this.previouslines[i].document_date = new Date( this.previouslines[i].document_date)
                this.returnfiles.push({id: this.previouslines[i].id, return: this.previouslines[i].returned == 1 ? true : false, type: systemDocTypes[this.previouslines[i].id_doc_type - 1].name, description: this.previouslines[i].description , filename: this.previouslines[i].filename, isUpload: this.previouslines[i].isUpload})
                typeDoc.push({id: i, value: this.previouslines[i].id_doc_type})

                  // seteo de opcion en informe y resultado
                if(this.previouslines[i].description === "Terapias"){
                    this.previouslines[i].description = "1"
                }
                else if(this.previouslines[i].description === "Fecha Inicio de Síntomas"){
                    this.previouslines[i].description = 2
                    
                }
                else if(this.previouslines[i].description === "Diagnóstico"){
                    this.previouslines[i].description = "3"
                    
                }
                else if(this.previouslines[i].description === "Recetario/Medicamentos"){
                    this.previouslines[i].description = "4"
                    
                }
                else if(this.previouslines[i].description === "Servicios Médicos"){
                    this.previouslines[i].description = "5"
                    
                }
                else if(this.previouslines[i].description === "Historia Clínica y Récord Operativo"){
                    this.previouslines[i].description = "6"
                    
                }
                else if(this.previouslines[i].description === "Exámenes(Procedimientos, Ecos, Resonancias, etc.)"){
                    this.previouslines[i].description = "7"
                    
                }
                else if(this.previouslines[i].description === "Medicamentos"){
                    this.previouslines[i].description = "1"
                    
                }
                else if(this.previouslines[i].description === "Exámenes"){
                    this.previouslines[i].description = "2"
                    
                }

            


        }
               for (let i = 0; i < this.previouslines.length; i++){

                if(this.previouslines[i].documents !== null){
                
                let Dispatched = API.service('dispatcheddoc', API.all('claims'))
                Dispatched.post({
                 'diagnostic_id' : this.previouslines[i].documents.diagnostic_id,
                })
                .then((response) => {
                    let data = API.copy(response)
                    this.dispatched = data.data
                    console.log("despachados", this.dispatched.dispatched)
                    if( this.dispatched.dispatched == '1'){
                               
                        this.previouslines[i].dispatched = 1      
                     
                    }else{
                        this.previouslines[i].dispatched = 0         
                    }


                    })

                }else{
                    console.log("Aqui")
                    this.previouslines[i].dispatched = 0      
                }
            }
            console.log("previuslINES ", this.previouslines)
            
            this.typeDoc = typeDoc

        })
        */
        let systemDescription = []
        systemDescription = [{"id": "1", "name": "Terapias"},{"id": "2", "name": "Fecha Inicio de Síntomas"},{"id": "3", "name": "Diagnóstico"},{"id": "4", "name": "Recetario/Medicamentos"}, {"id": "5", "name": "Servicios Médicos"}, {"id": "6", "name": "Historia Clínica y Récord Operativo"}] //, {"id": "7", "name": "Exámenes(Procedimientos, Ecos, Resonancias, etc.)"}
        this.systemDescription = systemDescription


        let systemDescription2 = []
        systemDescription2.push({"id": "1", "name": "Medicamentos"})
        systemDescription2.push({"id": "2", "name": "Exámenes"})
        this.systemDescription2 = systemDescription2

        

        let Currencies = API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
            console.log("monedas ", this.systemCurrency)
        })

        let Providers = API.service('providers')
        Providers.getList()
        .then((response) => {
            let systemProviders = []
            let providersResponse = response.plain()
            angular.forEach(providersResponse, function (value) {
                systemProviders.push({id: value.id, name: value.name})
            })

            this.systemProviders = systemProviders
            console.log("systemProviders ", this.systemProviders)
        })

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.addFile = () => {
          console.log("Entro")
          if (this.lines.length==1){
              var newid = 1
          }
          else{
              var newid = this.lines.length
          }
          this.lines.push({id : newid});

          this.typeDocNew.push({id : newid, value: 2})
      }

      this.changeDocType = (index,type) => {
        this.indice = index
        if (type == 1){
            this.typeDocNew[index].value = this.doc_typeselected[index]
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
                this.curency_selected[index] = 1
                //this.previouslines[index].description = ''
            }
            else {
                this.lines[index].disabled = true
                this.lines[index].rep = 'No '
                this.curency_selected[index] = 1
                this.value[index] = ''
            }
        }
        else{//anteriores
            this.typeDoc[index].value = this.previouslines[index].id_doc_type
            if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
                this.previouslines[index].currency_id = 1
                this.previouslines[index].description = ''
            }else {
                this.previouslines[index].disabled = true
                this.previouslines[index].rep = 'No '
                this.previouslines[index].currency_id = 1
                this.previouslines[index].amount = ''
            }
        }

    }     



    this.save = (isValid) => {
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log(isValid)


        if (isValid){
            this.processing = true
            let $state = this.$state
            let uploadFile = this.uploadFile
            let filenames = []
            angular.forEach(this.myFile, function (value) {
                filenames.push(value.name)
            })
            this.filenames = filenames
            //var vm = this
            /*
            angular.forEach(this.previouslines, function (value, index, arr) {             
                if(value.id_doc_type == 2 || value.id_doc_type == 4) {   
                    if (value.description == "1") {
                        arr[index].description = "Terapias"
                    }
                    else if(value.description == "2"){
                        arr[index].description = "Fecha Inicio de Síntomas"
                    }
                    else if(value.description == "3"){
                        arr[index].description = "Diagnóstico"
                    }
                    else if(value.description == "4"){
                        arr[index].description = "Recetario/Medicamentos"
                    }
                    else if(value.description == "5"){
                        arr[index].description = "Servicios Médicos"
                    }
                    else if(value.description == "6"){
                        arr[index].description = "Historia Clínica y Récord Operativo"
                    }
                    else if(value.description == "7"){
                        arr[index].description = "Exámenes(Procedimientos, Ecos, Resonancias, etc.)"
                    }

                } else if(value.id_doc_type == 3){
                    if (value.description == "1") {
                        arr[index].description = "Medicamentos"
                    }else {
                        arr[index].description = "Exámenes"
                    }

                }   
                console.log("value description", arr)

            })
            */
            angular.forEach(this.description, function (value, index, arr) {
                
                if (value == "1") {
                    arr[index] = "Terapias"
                }
                else if(value == "2"){
                    arr[index] = "Fecha Inicio de Síntomas"
                }
                else if(value == "3"){
                    arr[index] = "Diagnóstico"
                }
                else if(value == "4"){
                    arr[index] = "Recetario/Medicamentos"
                }
                else if(value == "5"){
                    arr[index] = "Servicios Médicos"
                }
                else if(value == "6"){
                    arr[index] = "Historia Clínica y Récord Operativo"
                }
                else if(value == "7"){
                    arr[index] = "Exámenes(Procedimientos, Ecos, Resonancias, etc.)"
                }
                //console.log("value description", value)

            })

            console.log("lines ", this.lines)
            let documents = this.API.service('claims2', this.API.all('claims'))
            documents.post({
            'id_claim' : this.claimId,
            'id_diagnostic' : this.diagnostic_id, 
            'id_doc_types' : this.doc_typeselected,
            'currency_id' : this.curency_selected,
            'provider_id' : this.provider_id,
            'values' : this.value,
            'descriptions' : this.description,
            'filenames' : this.filenames,
            //'modified' : this.previouslines,
            //'deleted_ids' : this.deletedlines,
            //'returned' : this.returnfiles,
            'document_date' : this.doc_date
          }).then(function (response) {
                mvm.uploadFile()
          }, function (response) {
          })
        }else{
            this.formSubmitted = true
        }
    }

    this.uploadFile = () =>{
        let $state = this.$state
        var i = 0
            var files = this.files//archivos nuevos
            //var pfiles = this.pfiles//archivos que sustituiran a los anteriores
            var claimId = this.claimId
            var diagnosticId = this.diagnostic_id
            var $http = this.$http
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('claim_id', claimId);
                form_data.append('diagnostic_id', diagnosticId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   })
            })
            /*
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                form_data.append('claim_id', claimId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })*/
        this.processing = false
        swal({
            title: 'Documentos Guardados Correctamente!',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
        }, function () {
             $uibModalInstance.dismiss('cancel')
            //$state.go('app.claim-claim-lists', {})
        })
    }


    this.deleteLine = (index) => {//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
        console.log('providers',this.provider_id)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.curency_selected.splice(pos,1)
        this.provider_id.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)
        this.doc_date.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
        console.log('providers',this.provider_id)
    }


    
    }

    

  

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    

        

    retufiles(){
        let sendmail = false
        angular.forEach(this.returnfiles, function(value){
            if (value.return == true){
                sendmail = true
            }
        })
        console.log("a ", sendmail)
        if (sendmail){
            this.API.service('send').post({
            format_id: 8,
            policy_id: this.policyId,
            claim_id : this.claimId,
            returnfiles: this.returnfiles
        } )
        .then( ( response ) => {
            swal({
                title: 'Documentos Devueltos Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {
                this.$state.reload();
            })
        }, function (response) {
            swal({
                title: 'Error al guardar documentos' + response.message,
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {

            })
        })
        }
        else{
            console.log('debe seleccionar')
        }
        console.log('debe enviar')
        this.sendmail = sendmail
    }
    checkSelected(){
        let sendmail = false
        angular.forEach(this.returnfiles, function(value){
            if (value.return == true){
                sendmail = true
            }
        })
        this.sendmail = sendmail
    }

    noPending(id){
        let $state = this.$state
        let pending = this.API.service('pending', this.API.all('claims'))
            pending.post({
            'id' : id
          }).then(function (response) {
                $state.reload()
          }, function (response) {
          })
    }






    $onInit(){
    }
}

export const DiagnosticsEobComponent = {
    templateUrl: './views/app/components/diagnostics-eob/diagnostics-eob.component.html',
    controller: DiagnosticsEobController,
    controllerAs: 'vm',
    bindings: {}
}
