class PolicyRegisterAmendmentController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

          var vm = this
        var relationship = ""
        var orderRole = 0
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.client = ""
        this.role = ""
        this.planDeduc = ""
        var systemDependents = []
        var allAffiliates = []
        var allDependents = []
        this.lines = []
        this.lines_1 = []
        this.birthdayaffiliate = []
        this.deletedamendments = []
        this.deletedannexes = []
        this.details = []
        this.fees = []
        this.completed = $stateParams.completed
        this.title = 'Creación de Póliza - Historicas - Enmiendas y Anexos'
        let policyId = $stateParams.policyId
        this.policyId = policyId
          //getPolicyInfo
        let Policy = this.API.service('getPolicyInfo');
        let data = {};
        data.policy_id=   $stateParams.policyId;
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible

            //this.policyinfo[0].birthday = new Date(this.policyinfo[0].birthday)
            this.role =  "Titular"
            let applicantId = this.policyinfo[0].applicant_id
            let Dependents = API.service('dependents', API.all('dependents'))
            Dependents.one(applicantId).get()
            .then((response) => {
                let dependentsResponse = response.plain()
                let dependentq = 0
                vm.dependentq = dependentq
                let spouse = 0
                vm.spouse = spouse
                let ageh = 0
                vm.ageh = ageh
                console.log(this.policyinfo[0].effective_date)
                angular.forEach(dependentsResponse.data, function (value) {
                    if(value.relationship == "H"){
                        relationship = "Cónyuge"
                        orderRole = 2
                        vm.ageh = vm.calculateAge(value.birthday)
                        vm.spouse = 1
                    }
                    else{
                        relationship = "Dependiente"
                        orderRole = 3
                        vm.dependentq++
                    }
                    //value.birthday = new Date(value.birthday)
                    
                    let age = vm.calculateAge(value.birthday)
                    systemDependents.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, relationship: relationship})
                    allAffiliates.push({id: value.id, name: value.first_name+' '+value.last_name, birthday: value.birthday, role: value.relationship, order: orderRole, relationship: relationship, age: age, effective_Date: vm.policyinfo[0].effective_date })
                    
                })

                let agetitular = this.calculateAge(this.policyinfo[0].birthday)
                allAffiliates.push({id: parseInt(this.policyinfo[0].id), name: this.client, role: "T", birthday: this.policyinfo[0].birthday ,order: 1, relationship: 'Titular' , age: agetitular, effective_Date: this.policyinfo[0].effective_date})
                allAffiliates.sort(function (a, b) {
                    if (a.order > b.order) {
                        return 1;
                    }
                    if (a.order < b.order) {
                        return -1;
                    }
                    return 0;
                });

                systemDependents.sort(function (a, b){
                  return (b.relationship - a.relationship)
                })
                this.systemDependents = systemDependents
                this.allAffiliates = allAffiliates
                console.log(this.allAffiliates)

                if (this.policyinfo[0].mode == 0){
                    this.payment_type = 'Anual'
                    this.amount_fees = 1
                }
                else if (this.policyinfo[0].mode == 1){
                    this.payment_type = 'Semestral'
                    this.amount_fees = 2
                }
                else if (this.policyinfo[0].mode == 2){
                    this.payment_type = 'Trimestral'
                    this.amount_fees = 4
                }
                else if (this.policyinfo[0].mode == 3){
                    this.payment_type = 'Mensual'
                    this.amount_fees = 12
                }

                if (isNaN(this.ageh)){
                    this.ageh = 0
                }
            })
        })

        //

    }

   addFile() {
        this.lines.push({id : this.lines.length+1})
    }
    addFile_1() {
        this.lines_1.push({id : this.lines_1.length+1, type: 1})
    }
    deleteLine(index){
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
    }
    deleteLine_1(index){
        var pos = this.lines_1.indexOf(index);
        this.lines_1.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines_1, function (value) {
              newlines.push({id : newvalue+1})
        })
        this.lines_1 = newlines
    }
    changeAffiliate(affiliate_id,index){
        console.log("id1 ", affiliate_id)
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.birthdayaffiliate[index] = this.allAffiliates[i].birthday
                this.lines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.lines_1',this.lines_1)
            }
        }

    }
    oldchangeAffiliate(affiliate_id,index){
        console.log("id ", affiliate_id)
        for (let i = 0; i < this.allAffiliates.length; i++ ){
            if (this.allAffiliates[i].id == affiliate_id){
                this.oldlines_1[index].birthdate = this.allAffiliates[i].birthday
                this.oldlines_1[index].role_order = this.allAffiliates[i].order
                console.log('this.oldlines_1',this.oldlines_1)
            }
        }
    }
    calculateAge(date){
        if (date != undefined){
            var date = date
            var values=date.split("-")
            var dia = values[2]
            var mes = values[1]
            var ano = values[0]
            var fecha_hoy = new Date()
            var ahora_ano = fecha_hoy.getYear()
            var ahora_mes = fecha_hoy.getMonth()+1
            var ahora_dia = fecha_hoy.getDate()
            var edad = (ahora_ano + 1900) - ano
            if ( ahora_mes < mes ){
                edad--
            }
            if ((mes == ahora_mes) && (ahora_dia < dia)){
                edad--
            }
            if (edad > 1900){
                edad -= 1900
            }
            var meses=0
            if(ahora_mes>mes)
                meses=ahora_mes-mes
            if(ahora_mes<mes)
                meses=12-(mes-ahora_mes)
            if(ahora_mes==mes && dia>ahora_dia)
                meses=11

            var dias=0
            if(ahora_dia>dia)
                dias=ahora_dia-dia
            /*if(ahora_dia<dia)
            {
                console.log('ahora_ano',ahora_ano,'ahora_mes',ahora_mes)
                ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
                console.log('ultimoDiaMes',ultimoDiaMes)
                dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
            }*/
            if (edad < 1){
                if (meses ==1){
                    return meses+' mes'
                }
                else{
                    return meses+' meses'
                }
            }
            else{
                return edad
            }
            }
    }
    olddeleteLine_1(index){
        this.deletedamendments.push({id : index.id})
        let pos = this.oldlines_1.indexOf(index);
        this.oldlines_1.splice(pos,1)
    }
    olddeleteLine(index){
        this.deletedannexes.push({id : index.id})
        let pos = this.oldlines.indexOf(index);
        this.oldlines.splice(pos,1)
    }
    changeAdminFee(fee,tax){
        if (this.fees[fee].taxes[tax].checkvalue == true){
            this.fees[fee].taxes[tax].value = this.adminfee //variable adminfee
        }
        else{
            this.fees[fee].taxes[tax].value = 0
        }
        this.fees[fee].taxes[tax+1].value = this.fees[fee].taxes[tax].value*(this.iva/100) //variable Tax (IVA)
        this.updateTotalFee(fee)
    }
    updateTotalFee(fee){
        let details = this.fees[fee].details
        let detailsum = 0
        details.forEach(function(element) {
            detailsum = detailsum + element.value
        })

        let discounts = this.fees[fee].discounts
        let discountsum = 0
        discounts.forEach(function(element) {
            discountsum = discountsum + element.value
        })

        let taxes = this.fees[fee].taxes
        let taxsum = 0
        taxes.forEach(function(element) {
            taxsum = taxsum + element.value
        })
        this.fees[fee].total = (detailsum + taxsum) - discountsum

        let fees = this.fees
        let total = 0
        fees.forEach(function(element) {
            total = total + element.total
        })
        this.total = total
    }
     save(isValid){
        console.log('this.lines_1',this.lines_1)
        if (isValid){
            this.isDisabled = true;
            let confirm = this.API.service('amendment', this.API.all('registerpolicies'))
            let $state = this.$state
            confirm.post({
                'policyId': this.policyId,
                'number': this.policyinfo[0].number,
                'amendments': this.lines_1,
                'annexes': this.lines,
                'effective_date': this.policyinfo[0].effective_date
            }).then(function () {
                swal({
                    title: 'Póliza Registrada!!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.policy_register')
                })
            }, function (response) {
                swal({
                    title: 'Error al Registrar Datos!',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })
            })
        }
        else{
            this.formSubmitted = true
        }
        console.log(this.lines_1)
    }

//
    $onInit(){
    }
}

export const PolicyRegisterAmendmentComponent = {
    templateUrl: './views/app/components/policy_register_amendment/policy_register_amendment.component.html',
    controller: PolicyRegisterAmendmentController,
    controllerAs: 'vm',
    bindings: {}
}
