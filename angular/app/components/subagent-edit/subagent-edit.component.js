class SubagentEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity
        vm.countryRequired = false
        vm.provinceRequired = false
        vm.cityRequired = false
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let subagentId = $stateParams.subagentId

        let SubagentData = API.service('show', API.all('subagents'))
        SubagentData.one(subagentId).get()
          .then((response) => {
            this.subagenteditdata = API.copy(response)
            console.log(this.subagenteditdata)
            let subagentResponse = response.plain(response)
            this.subagenteditdata.data.commission = parseFloat(this.subagenteditdata.data.commission, 2)
            this.subagenteditdata.data.birthday =  new Date(this.subagenteditdata.data.birthday)
            
        
        })


           function getState(){
                console.log(vm.country_id)
                vm.countryRequired = true
                //console.log("Requerido", vm.countryRequired)
                let States = API.service('index', API.all('states'))
                States.one(vm.country_id).get()
                    .then((response) => {
                        let systemStates = []
                        let stateResponse = response.plain()
                        console.log(stateResponse)
                        angular.forEach(stateResponse.data.states, function (value) {
                        systemStates.push({id: value.id, name: value.name})
                        })
                this.systemStates = systemStates


                })

        }

            function getCity(){
                vm.provinceRequired = true
                vm.cityRequired = true
                let Citys = API.service('index', API.all('cities'))
                Citys.one(vm.province_id).get()
                    .then((response) => {
                        let systemCities = []
                        let cityResponse = response.plain()
                        angular.forEach(cityResponse.data.cities, function (value) {
                        systemCities.push({id: value.id, name: value.name})
                    })
                this.systemCities = systemCities
                
                })

            }


        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              if(value.id == vm.subagenteditdata.data.country_id){
                let States = API.service('index', API.all('states'))
                States.one(value.id).get()
                    .then((response) =>{
                        let systemStates = []
                        let stateResponse = response.plain()
                        console.log(stateResponse)
                        angular.forEach(stateResponse.data.states, function (value){  
                            if(value.id == vm.subagenteditdata.data.province_id){
                                let Citys = API.service('index', API.all('cities'))
                                Citys.one(value.id).get()
                                    .then((response) => {
                                        let systemCities = []
                                        let cityResponse = response.plain()
                                        angular.forEach(cityResponse.data.cities, function (value){
                                            systemCities.push({id: value.id, name: value.name})

                                        })
                                        vm.systemCities = systemCities
                                        console.log("Ciudades", vm.systemCities)
                                    })
                                        
                            }  
                        
                        systemStates.push({id: value.id, name: value.name})
                        

                        })
                        vm.systemStates = systemStates
                        console.log("systemStates", systemStates)                        
                    })
              }
              systemCountrys.push({id: value.id, name: value.name})
              console.log("Paises", systemCountrys)
            })

            this.systemCountrys = systemCountrys
        })


        //
    }//fin de constructor

     save (isValid) {
        console.log("entro")
        this.subagenteditdata.data.country_id = this.country_id
        this.subagenteditdata.data.province_id = this.province_id
        this.subagenteditdata.data.city_id = this.city_id
                if (isValid) {
                    this.processing = true
                    let vm = this
                    //console.log("la data, ", this.subagenteditdata);
                    let $state = this.$state
                    this.subagenteditdata.put()
                        .then(() => {
                        vm.processing = false
                        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se han actualizado los datos del Sub-agente.' }
                        $state.go($state.current, { alerts: alert})
                    }, (response) => {
                        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                        $state.go($state.current, { alerts: alert})
                    })
                } else {
                
                this.formSubmitted = true
            }

        }

    $onInit(){
    }
}

export const SubagentEditComponent = {
    templateUrl: './views/app/components/subagent-edit/subagent-edit.component.html',
    controller: SubagentEditController,
    controllerAs: 'vm',
    bindings: {}
}
