class UserAddController {
  constructor ($scope, API, $state, $stateParams) {
    'ngInject'

    this.$state = $state
    this.formSubmitted = false
    this.API = API
    this.alerts = []
    this.processing = false
    console.log("a")

    let Roles = API.service('roles', API.all('users'))
    Roles.getList()
      .then((response) => {
        let systemRoles = []
        let roleResponse = response.plain()

        angular.forEach(roleResponse, function (value) {
          systemRoles.push({id: value.id, name: value.name})
        })

        this.systemRoles = systemRoles
        console.log("role=>> ", systemRoles);
      }) 


    if ($stateParams.alerts) {
      console.log("=>> ", $stateParams.alerts);
      this.alerts.push($stateParams.alerts)
    }
  }

  save (isValid, form) {
    
    //this.$state.go(this.$state.current, {}, { alerts: 'test' })
    if (isValid) {
      this.processing = true
        let vm = this
      
      let Users = this.API.service('users', this.API.all('users'))
      let $state = this.$state
      
      
      Users.post({
        'name': this.name,
        'email': this.email,
        'password': this.password,
        'role': this.role
      }).then(function () {
        vm.processing = false
        console.log("mensaje");
        let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el usuario.' }
        //console.log();
        $state.go($state.current, { alerts: alert})
      }, function (response) {
        let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
        $state.go($state.current, { alerts: alert})
      })
    } else { 
      this.formSubmitted = true
    }
  }

  $onInit () {}
}

export const UserAddComponent = {
  templateUrl: './views/app/components/user-add/user-add.component.html',
  controller: UserAddController,
  controllerAs: 'vm',
  bindings: {}
}
