class DoctorAddController{
    constructor ($scope, API, $state, $stateParams) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.specialties = []
        this.specialtiesselected = []
        this.getState = getState
        //this.getCity = getCity
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

     let specialties = API.all('medicalspecialties')
        specialties.getList()
      .then((response) => {
        let specialtieslist = []
        let specialties = response.plain()

        angular.forEach(specialties, function (value) {
          specialtieslist.push({id: value.id, name: value.name})
        })

        this.specialties = specialtieslist
      })
      let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })
            this.systemCountrys = systemCountrys
            console.log('paises')
        })

        function getState(){
            console.log("Entro..", this.country)
            let States = API.service('province', API.all('states'))
            States.one(this.country).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                     console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        systemStates.push({id: value.id, name: value.name})
                    })
                this.systemStates = systemStates
                console.log("systemStates  ==> ", systemStates)

                })

        }

        /*function getCity(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.state).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                     console.log(cityResponse)
                    angular.forEach(cityResponse.data.cities, function (value) {
                        systemCities.push({id: value.id, name: value.name})
                    })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", systemCities)

                })

        }*/
    }

    save (isValid) {
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
          this.processing = true
          let doctors = this.API.service('doctors', this.API.all('doctors'))
          let $state = this.$state
          let vm = this

          doctors.post({
            'names': this.names,
            'country_id' : this.country,
            'province_id' : this.state,
            'city' : this.city,
            'address': this.address,
            'surnames': this.surnames,
            'phone':this.phone,
            'identification' : this.identification,
            'specialties' : this.specialtiesselected
          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el doctor.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else {
          this.formSubmitted = true
        }
    }
    $onInit(){
    }
}

export const DoctorAddComponent = {
    templateUrl: './views/app/components/doctor-add/doctor-add.component.html',
    controller: DoctorAddController,
    controllerAs: 'vm',
    bindings: {}
}
