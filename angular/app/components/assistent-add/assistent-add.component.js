class AssistentAddController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

         var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity
        this.processing = false
        
        vm.fecha_actual= new Date()

        vm.fecha_actual= new Date()

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let agentId = $stateParams.agentId
        
        let AgentData = API.service('show', API.all('agents'))
        AgentData.one(agentId).get()
          .then((response) => {
            this.agenteditdata = API.copy(response)
            console.log(this.agenteditdata)
            let agentResponse = response.plain(response)
        
      })

        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            console.log(countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            console.log("systemCountrys==> ", systemCountrys)
        })


    function getState(){
            console.log("Entro..", vm.country)
            let States = API.service('index', API.all('states'))
            States.one(vm.country).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                     console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        if (vm.country == value.country_id) {
                            systemStates.push({id: value.id, name: value.name})
                        }                        
                    })
                this.systemStates = systemStates
                console.log("systemStates  ==> ", systemStates)          

                })

        }


    function getCity(){
        console.log("Entro..", vm.state)
            let Citys = API.service('index', API.all('cities'))
            Citys.one(vm.state).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                     console.log(cityResponse)
                    angular.forEach(cityResponse.data.cities, function (value) {
                        if (vm.state == value.province_id) {
                            systemCities.push({id: value.id, name: value.name})
                        }                        
                    })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", systemCities)          

                })

        }    


        //
    }// fin constructor
    save (isValid) {  
        console.log("Id agente", this.agenteditdata.data.id)
        //let agentId = this.agenteditdata.data.id
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this
          let Assistent = this.API.service('assistents', this.API.all('assistents'))
          let $state = this.$state                    
          Assistent.post({
            'first_name': this.name,
            'last_name' : this.last_name,
            'identity_document' : this.identity,
            'birthday' : this.birthday,
            'email' : this.email,
            'skype' : this.skype,
            'mobile' : this.mobile,
            'phone' : this.phone,
            'country_id' : this.country,
            'province_id' : this.state,
            'city_id' : this.city,
            'address' : this.address,
            'charge' : this.charge,
            'agentId' : this.agenteditdata.data.id

          }).then(function () {
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el asistente.' }
            $state.go($state.current, { alerts: alert})
            //console.log("MENSAJE 2", $state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }


    $onInit(){
    }
}

export const AssistentAddComponent = {
    templateUrl: './views/app/components/assistent-add/assistent-add.component.html',
    controller: AssistentAddController,
    controllerAs: 'vm',
    bindings: {}
}
