class RenewalPaymentInformationController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        //
  var vm = this
        this.current_step = '5'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        $scope.form = {};
        vm.hideMount = true
        vm.discount = 0
        vm.discount_percent = 3

        let policyId = $stateParams.policyId
        this.policyId = policyId
        let feeId = $stateParams.feeId
        this.feeId = feeId

        let PaymentData = API.service('show', API.all('payments'))
        PaymentData.one(policyId).get()
        .then((response) => {
            this.paymenteditdata = API.copy(response)
            //$scope.form.id = this.applicanteditdata.data.plan_id
            console.log("payment", this.paymenteditdata)
            console.log("payment", this.paymenteditdata.data.length)
            if(this.paymenteditdata.data.length > 0){
                this.mode = this.paymenteditdata.data[0].mode
                this.method =  this.paymenteditdata.data[0].method
                this.discount =  this.paymenteditdata.data[0].discount
                console.log("Descuento", this.discount_percent)
                this.discount_percent =  this.paymenteditdata.data[0].discount_percent == 0 ? vm.discount_percent : parseFloat(this.paymenteditdata.data[0].discount_percent, 2)
                console.log("Descuento", this.discount_percent)
                this.invoice_before = this.paymenteditdata.data[0].invoice_before
                if(this.discount == 1){

                       vm.hideMount =  false 
                }
            }else{
                this.discount = 0
                this.invoice_before = 0
                vm.hideMount =  true

            }

        })

        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisrefrenewal, order: value.order})
            })
            systemWizar.splice(3,1)
            //systemWizar.pop()
            this.systemWizar = systemWizar
            console.log("systemWizar==> ", systemWizar)
        })

        //
    }
    showMount(){
        console.log("Entro")
        if(this.discount == 0){
            this.hideMount = true
            this.discount_percent = parseFloat("0", 2)
            

        }else{
            this.hideMount = false
            
       }

    }
    save(isValid){
        console.log("invoice_before", this.invoice_before)
        let $state = this.$state
        if (isValid) {      
            this.isDisabled = true;
            if (this.discount == '0') {
                this.discount_percent = 0
            }                
                console.log("policyId ", this.policyId)
                console.log("mode ", this.mode)
                console.log("method ", this.method)
                console.log("discount", this.discount)
                console.log("discount_percent", this.discount_percent)
                console.log("invoice_before", this.invoice_before)
                let Payments = this.API.service('payments', this.API.all('renewals'))
                let $state = this.$state                    
                Payments.post({
                    'policyId' : this.policyId,
                    'mode': this.mode,
                    'method' : this.method,
                    'discount' : this.discount,
                    'discount_percent' : this.discount_percent,
                    'invoice_before' : this.invoice_before,
                    'feeId' : this.feeId
                    
                  }).then( (response) => {
                    response.feeId = this.feeId
                    swal({
                        title: 'Datos de Pago Guardados Correctamente!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {       
                            $state.go('app.renewal-coverage-start', {"policyId": response.data, "feeId": response.feeId})
                    })

                  }, function (response) {
                    swal({
                        title: 'Error ' + response.data.message,
                        type: 'error',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                            $state.go($state.current)
                    })
                  })    
               

        } else { 
          this.formSubmitted = true
        } 
    }
    $onInit(){
    }
}
export const RenewalPaymentInformationComponent = {
    templateUrl: './views/app/components/renewal-payment-information/renewal-payment-information.component.html',
    controller: RenewalPaymentInformationController,
    controllerAs: 'vm',
    bindings: {}
}
