class EmissionPolicyListsController{
    //constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http, Notificaciones){
    constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http){
        'ngInject';

        this.API = API
        this.$state = $state
        this.$uibModal = $uibModal
        this.$log = $log
        this.animationsEnabled = true
        this.items = ['item1', 'item2', 'item3']
        //this.Notificaciones = Notificaciones
        var fecha = ""
        var arrfecha=""
        var fecha1 = ""
        var arrfecha1=""

        let Policies = this.API.service('policies')
        
        Policies.getList({})
          .then((response) => {
            let dataSet = response.plain()
              angular.forEach(dataSet, function (value) {

              fecha = value.start_date_coverage
              arrfecha= fecha.split("/")
              var day=arrfecha[0]
              var month = arrfecha[1]
              var year= arrfecha[2]

              fecha= month+'/'+day+'/'+year  
              value.start_date_coverage = fecha

              fecha1 = value.created
              arrfecha1= fecha1.split("/")
              var day=arrfecha1[0]
              var month = arrfecha1[1]
              var year= arrfecha1[2]

              fecha1= month+'/'+day+'/'+year  
              value.created = fecha1
            })

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('policy_id').withTitle('ID').withOption('width', '5%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('agent').withTitle('Agente'),
              DTColumnBuilder.newColumn('created').withTitle('Fecha de Creación').withOption('width', '15%'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura').withOption('width', '15%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml).withOption('width', '28%')
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
          return `
                    <a ng-if="${data.step1 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Ingresar Solicitud" class="btn btn-xs btn-primary"  ui-sref="app.emission-applicant-data({policyId: ${data.id}})">
                      <i class="fa fa-arrow-right"></i>
                    </a>
                    <a ng-if="${data.step1 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Ingresar Solicitud" class="btn btn-xs btn-default"  ui-sref="app.emission-applicant-data({policyId: ${data.id}})">
                      <i class="fa fa-arrow-right"></i>
                    </a>
                    &nbsp                    
                    <a ng-if="${data.step2 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza BD" class="btn btn-xs btn-primary" ng-disabled="${data.step == 2}" ui-sref="app.confirm-policy-data({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>
                    <a ng-if="${data.step2 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza BD" class="btn btn-xs btn-default" ng-disabled="${data.step == 2}" ui-sref="app.confirm-policy-data({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>
                    &nbsp
                    <a ng-if="${data.step3 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Respuesta del Cliente" class="btn btn-xs btn-primary" ng-disabled="${data.step == 3}" ng-click="vm.modalInOpen('', '${data.name}', ${data.id})">
                      <span class="glyphicon glyphicon-comment"></span>
                    </a>
                    <a ng-if="${data.step3 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Respuesta del Cliente" class="btn btn-xs btn-default" ng-disabled="${data.step == 3}" ng-click="vm.modalInOpen('', '${data.name}', ${data.id})">
                      <span class="glyphicon glyphicon-comment"></span>
                    </a>
                    &nbsp
                    <a ng-if="${data.step4 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-primary" ng-disabled="${data.step == 4}" ui-sref="app.register-payment({policyId: ${data.id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step4 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Registrar Pago" class="btn btn-xs btn-default" ng-disabled="${data.step == 4}" ui-sref="app.register-payment({policyId: ${data.id}})">
                      <i class="fa fa-usd" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <a ng-if="${data.step5 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Documentos a Recepción" class="btn btn-xs btn-primary" ng-disabled="${data.step == 5}" ng-click="vm.sendDocs('lg', '${data.name}', ${data.id})">
                      <i class="glyphicon glyphicon-print" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step5 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Documentos a Recepción" class="btn btn-xs btn-default" ng-disabled="${data.step == 5}" ng-click="vm.sendDocs('lg', '${data.name}', ${data.id})">
                      <i class="glyphicon glyphicon-print" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <a ng-if="${data.step7 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-primary" ui-sref="app.send-invoice({policyId: ${data.id}})">
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step7 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Factura" class="btn btn-xs btn-default" ui-sref="app.send-invoice({policyId: ${data.id}})">
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <a ng-if="${data.step6 == 0}" style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago" class="btn btn-xs btn-primary" ui-sref="app.confirm-payment({policyId: ${data.id}})">
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </a>
                    <a ng-if="${data.step6 == 1}" style="color: #ecf0f5; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Pago" class="btn btn-xs btn-default" ui-sref="app.confirm-payment({policyId: ${data.id}})">
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </a> 
                    &nbsp
                    <button style="color: #FE2E2E; background-color: #FFF;" title="Eliminar Póliza" class="btn btn-xs btn-danger" ng-click="vm.delete('', '${data.name}', ${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  `

        }

        
    }

    modalInOpen (size, name, id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      //console.log("a", name);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalInContent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    sendDocs (size, name, id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      //console.log("a", id);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDocContent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

  delete (size, name, id) {

      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      var modalInstance = $uibModal.open({     
        templateUrl: 'myModalDelete',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: 'md',
        resolve: {
            items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
     
    }

  //modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state, Notificaciones) {
  modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state) {
    'ngInject'

      //console.log("controller")
      this.API = API
      this.items = items
      this.customer = customer
      this.id = id
      this.$state = $state
      this.sendmail = sendmail
      this.motive = 0
      this.processing = true
      this.deleting = true
      
      var mvm = this

       let systemMotive = []
      systemMotive = [{id: 1, name: "Cambio de Compañia"},{id: 2, name: "Temas Económicos"},{id: 3, name: "Cambio de Agencia"},{id: 4, name: "Otros"}] 
      this.systemMotive = systemMotive

      function sendmail(answer){
        //console.log("id ", id)
        //console.log("answer ", answer)
          API.service('send').post({
              policy_id : id,
              format_id : 2,
              answer : answer
          } )
          .then( ( response ) => {
              $uibModalInstance.dismiss('cancel')
              swal("Éxito!", "Respuesta enviada.");
              $state.reload();
          }, function (response) {
                  console.log('error')
              }
          )
          console.log("xxxx")
       }


      $scope.selected = {
        item: items[0]
      }      

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.saveResponse = (answer, id) => {
        console.log("q")
        $uibModalInstance.dismiss('cancel')
        this.isDisabled = true;
        var form_data = new FormData();
        let Policies = this.API.service('updatePolicies');
        
        form_data.append('policyId', id);
        form_data.append('customer_response', answer);
        
        let $state = this.$state        
        $http.post('updatePolicies', form_data,
        {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined,'Process-Data': false}
        }).success(function(response){
          sendmail(answer)
        })          
      }

      this.saveDocs = (answer, id) => {
        //console.log("id ", id)
        //console.log("answer ", answer)
        $uibModalInstance.dismiss('cancel')
        //if (answer == 1) {
          console.log("x el si")
          API.service('send').post({
              policy_id : id,
              format_id : 3,
              answer : 1
          } )
          .then( ( response ) => {
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
              $uibModalInstance.dismiss('cancel')
            })
          }, function (response) {
                  console.log('error')
              }
          )
          //$uibModalInstance.dismiss('cancel')
        //} else {
          //console.log("x el no")
          //$uibModalInstance.dismiss('cancel')
        //}

          /*API.service('send').post({
              policy_id : id,
              format_id : 2,
              answer : answer
          } )
          .then( ( response ) => {
              $state.reload();
          }, function (response) {
                  console.log('error')
              }
          )*/
          console.log("xxxx")
       }


       this.changeMotive = () => {

        if(this.motive == 0){
            this.processing = true
            this.deleting = true
          }
          else{
            this.processing = false
          }

        }


       this.deletePolicy = (isValid) => {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log("Dependiente a eliminar", mvm.policyId)
        console.log("Dependiente a eliminar", mvm.motive)
        console.log(isValid)
        if (isValid){
          let $state = this.$state
          let Policy = API.service('MotiveRenewal')

              Policy.post({
                'policyId' : mvm.id,
                'motive': mvm.motive
              }).then(function (response) {
                
                $uibModalInstance.dismiss('cancel')
                
             swal({
                    title: 'Póliza eliminada Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current, {}, { reload: $state.current})
                })
                }, function (response) {
                    swal({
                    title: 'Error al guardar motivo' + response.message,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                    })
                  }
                )

            
         
        }
          else
              {
              this.formSubmitted = true
              }
            
         
          }
    }
/*******************************************************************/


    /*delete (policyId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la póliza!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('policies').one('policy', policyId).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }*/

    sendMail (policyId) {
      let API = this.API
      let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Desea enviar los datos de la póliza a Recepción!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        console.log("entro", policyId)
          API.service('send').post({
              policy_id : policyId,
              format_id : 3,
              answer : 1
          } )
          .then( ( response ) => {             
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          }, function (response) {
                  console.log('error')
              }
          )


      })
    }


    $onInit(){
    }
}


export const EmissionPolicyListsComponent = {
    templateUrl: './views/app/components/emission-policy-lists/emission-policy-lists.component.html',
    controller: EmissionPolicyListsController,
    controllerAs: 'vm',
    bindings: {}
}
