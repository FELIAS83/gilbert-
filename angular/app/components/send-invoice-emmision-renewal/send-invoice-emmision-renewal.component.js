class SendInvoiceEmmisionRenewalController{
    constructor($state,$stateParams,API,$http){
        'ngInject';

        let vm = this
        this.$state = $state
        this.uploadFile = uploadFile
        this.API = API
        this.formSubmitted = false

        let policyId = $stateParams.policyId
        this.policyId = policyId
      
        let Policy = API.service('getPolicyInfo');

        let data = {};
        data.policy_id=   $stateParams.policyId;

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            console.log(dataSet)
            this.policyinfo = dataSet
        })

        //
        function uploadFile(){

                var files = vm.files
                angular.forEach(files, function(files){
                console.log("vm.policyinfo[0].payment_id ", vm.policyinfo[0].payment_id)
                console.log('policy_id', vm.policyId)
                 console.log("archvo", files.name)
                var form_data = new FormData();
                form_data.append('file', files);
                form_data.append('policy_id', vm.policyId);
                $http.post('uploadfilespayment', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    API.service('send').post({
                        policy_id : vm.policyId,
                        payment_id: vm.policyinfo[0].payment_id,
                        format_id : 6,
                        invoice_filename: files.name
                    })
                    .then( ( response ) => {
                        swal({
                            title: 'Factura Enviada Correctamente!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.emission-renewal-list')
                        })
                    }, function (response) {
                        console.log('error')
                    }
                    )
                })
            })
        }
    }

     save(isValid){
        if (isValid){
            this.isDisabled = true;
            let confirm = this.API.service('invoiceemissionrenewal', this.API.all('renewals'))
            let $state = this.$state
            let uploadFile = this.uploadFile
            confirm.post({
                'policyId': this.policyId,
                'payment_id': this.policyinfo[0].payment_id,
                'filename': this.myFile.name
            }).then(function () {
                uploadFile()
                
            }, function (response) {
                swal({
                    title: 'Error al Enviar Factura!',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    swal({
                        title: 'Error al Confirmar Pago!',
                        type: 'error',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                    })
                })
            })
        }
        else{
            this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const SendInvoiceEmmisionRenewalComponent = {
    templateUrl: './views/app/components/send-invoice-emmision-renewal/send-invoice-emmision-renewal.component.html',
    controller: SendInvoiceEmmisionRenewalController,
    controllerAs: 'vm',
    bindings: {}
}
