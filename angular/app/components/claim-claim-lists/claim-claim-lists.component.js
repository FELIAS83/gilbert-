class ClaimClaimListsController{
constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http, $window){
        'ngInject';

        this.API = API
        this.$state = $state
        this.$uibModal = $uibModal
        this.$log = $log
        this.animationsEnabled = true
        this.items = ['item1', 'item2', 'item3']
        let claimStatus = '1'
        this.length = 0
        this.seleccionados=[];
       
        let vm = this
        var userAgent = $window.navigator.userAgent;
        console.log("userAgent", userAgent)

        let Policies = this.API.service('claims')
        
        Policies.getList({claimStatus})
          .then((response) => {
            let dataSet = response.plain()
            this.length = dataSet.length
            this.seleccionados=[];
            this.lista=dataSet
            console.log("Datos", dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID Ticket').withOption('width', '9%'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza').withOption('width', '9%'),
              DTColumnBuilder.newColumn('name').withTitle('Titular'),
              DTColumnBuilder.newColumn('amount').withTitle('Monto').withOption('width', '7%'),
              DTColumnBuilder.newColumn('created_at').withTitle('Creación del Trámite').withOption('width', '16%'),
              DTColumnBuilder.newColumn('days_passed').withTitle('Días en Espera').withOption('width', '12%'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true

            //console.log("pending_docs",dataSet[1].pending_docs)
          })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }        

        let actionsHtml = (data) => {
          let alert = ""
          if (data.days_passed > 3 && data.long != 0) {
            alert = "El reclamo tiene documentos por asignar y "+ data.days_passed +" días en espera."
          }else if (data.days_passed > 3) {
            alert = "El reclamo tiene "+ data.days_passed +" días en espera."
          } else {
            alert = "El reclamo tiene documentos por asignar."
          }
          return `
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Subir Escaneados" class="btn btn-xs btn-primary" ui-sref="app.claim-claim-files({claimId: ${data.id}, policyId: ${data.policy_id} })">
                      <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Asignación" class="btn btn-xs btn-primary" ui-sref="app.ticket-claim-lists({claimId: ${data.id}, policyId: ${data.policy_id}})">
                      <i class="glyphicon glyphicon-paste" aria-hidden="true"></i>
                    </a>
                    &nbsp
                    <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Solicitar Documentos Pendientes" class="btn btn-xs btn-primary" ng-click="vm.sendAgent('lg', '${data.name}', ${data.id}, ${data.policy_id})" ng-disabled="${data.withDiagnostic} == 0">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                    </button>
                    &nbsp
                     <button style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Despachar Diagnósticos" class="btn btn-xs btn-primary" ng-click="vm.modalDiagnostic('lg', '${data.name}', ${data.id}, ${data.policy_id})" ng-disabled="${data.withDiagnostic} == 0"> 
                      <i class="fa fa-location-arrow" aria-hidden="true"></i>
                    </button>
                    &nbsp
                    <button style="color: #FE2E2E; background-color: #FFF;" title="Cerrar Reclamo" class="btn btn-xs btn-danger" ng-click="vm.closed(${data.id})">
                        <i class="fa fa-times"></i>
                    </button>
                    &nbsp
                    <button style="color:orange; background-color: #FFF;width:24.2px;height:21.98px" title="${alert}" ng-if="(${data.days_passed} > 3 || ${data.long} != 0 )"  class="btn btn-xs btn-warning">
                        <span ><i class="fa fa-warning"></i></span>
                    </button>
                    &nbsp
                    <input type="checkbox" style="height: 23px;width: 23px;top: 8.5px; position:relative;" ng-change="vm.selectOne(${data.id})" title="Seleccionar Ticket" ng-model="vm.seleccionados[${data.id}]" name="claim" value="${data.id}" required ng-if="${data.withDiagnostic} == 1" />
                    
                  `
        }
    }
    




////ng-click="vm.dispatch(${data.id})"
 selectAll (){
      if (this.all) {
        console.log("entro")
        for (var i = 0; i < this.lista.length; i++) {
            this.seleccionados[this.lista[i].id] = true
        }
      }else{
        this.seleccionados=[];
      }      
        
    }

selectOne (id){
      //console.log("select ", this.seleccionados)
      var tickets = 0
      //this.all = false
      for (var i = 0; i < this.seleccionados.length; i++) {
        if (this.seleccionados[i] !== undefined) {
          tickets ++
        }
          
      }
      if (tickets == 0) {
        this.seleccionados=[];
      }

      
      if (!this.seleccionados[id]) {
        this.all = false
      }
      /*if (this.lista.length == tickets){
        this.all = true
      }*/
      console.log("seleccionados", this.seleccionados)
    }

 dispatch (claimId) {
    let API = this.API
    let $state = this.$state
    var claims = []
    var claimstext = ''
    angular.forEach(this.seleccionados, function (value, key) {
        if (value) {
          claims.push({id:key, value:value})
          claimstext = claimstext + ", " + key
        }            
    })
    claimstext = claimstext.substr(1);
    console.log("claims", claims)
    //claims = claims.substr(1);
    //console.log("claims ", claims)
    //var arregloclaims=claims.split(',');
   

    swal({
      title: 'Está seguro?',
      text: 'Se despacharán los Diagnósticos de los siguientes Reclamos: </br> <b> ' + claimstext + '</b>.',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Sí, despachar!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      html: true
    }, function () {
      console.log("claims", claims)
      
      let Diagnostics = API.service('dispatchdiagnostics', API.all('diagnostics'))
               Diagnostics.post({
                   'claims': claims,
               }).then( (response) => {
                  
                  let docs = response.plain()
                  docs = docs.data
                  console.log("datos", docs)
                  
                  //
                  for (var i = 0 ; i < docs.length; i++) {                  
                    var doc = new jsPDF('p', 'mm', 'letter');
                  console.log("doc",doc)
                  var size1 = 11
                  var size2 = 14
                  var sizeT = 10

                  doc.setFontSize(size1);
                  let x_ini = 20
                  let x_fin = 210
                  let y_ini = 20 
                  let sizeText = 0  

                  var rect1_width = 0
                  var rect2_width = 0
                  var x1_rect = 0  // posicion inicial x de la celda para el total en el 1er cuadro
                  var x2_rect = 0  // posicion inicial x de la celda para el total en el 2do cuadro
                  var cell_y = 0  //posicion Y de la celda para el total en el 1er cuadro

                  var data2 = []
                  var data3 = []
                  

                  //
                    sizeText = doc.getStringUnitWidth (docs[0].date) * size1
                    sizeText = sizeText / 2.81
                    let x = x_fin - sizeText  - 10 
                    doc.text(x, 20, docs[0].date); // fecha
                  // 
                    doc.text(x_ini, y_ini+20, 'Señores');
                    doc.setFontType("bold");
                    doc.text(x_ini, y_ini+25, 'BEST DOCTORS');
                    doc.setFontType('normal');
                    doc.text(x_ini, y_ini+30, 'Ciudad.-');
                  //
                    y_ini = y_ini + 30
                    doc.text(x_ini, y_ini+20, 'Atte. Dpto. de Reclamos');
                    doc.text(x_ini, y_ini+30, 'De mis consideraciones:');
                    doc.text(x_ini, y_ini+40, 'Por medio del presente adjunto el siguiente reclamo:');
                  //
                    //console.log("afiliado", docs[i].affiliate)
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+55, 'Asegurado: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+55, docs[i].affiliate);
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+60, 'Póliza: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+60,  docs[i].policy_number);  

                    // 1era tabla
                   
                    var columns1 = ["Fecha Enviado", "#GB", "Titular", "# Póliza", "Paciente", "Diágnostico", "Monto"];
                    var data1 = [[docs[i].rowData.date, docs[i].claim_id, docs[i].applicant, docs[i].policy_number, docs[i].affiliate, docs[i].rowData.diagnostic, docs[i].rowData.amount]]
                    //var data1 = [["1","2","3","4","5","6","7"]]
                    //console.log(doc.autoTable(["uno"],[2],{margin:{ top: 25 }}))
                    doc.autoTable(columns1,data1,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                //tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:15, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'},
                                                 4: {overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 5:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 6:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'}
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ top: y_ini+70, right: 20 }
                                              })

                    doc.text(x_ini, y_ini+105, 'Detalles de los Documentos');

                    // 2da tabla - invoices
                    var columns2 = [{title:"Facturas", dataKey:"factura"}, 
                                    {title:"Fecha", dataKey:"fecha_factura"}, 
                                    {title:"Proveedor", dataKey:"provider"}, 
                                    {title:"Valor", dataKey:"monto_factura"}];
                   
                    //console.log("antes del forEach "+i,docs[i])
                    angular.forEach(docs[i].rowData1, function(value){
                      
                      data2.push({factura: value.factura, fecha_factura: value.fecha_factura, provider:value.provider, monto_factura:value.monto_factura})

                    })
                    var lines = data2.length -1
                    var amount_invoices = docs[i].total
                    //console.log("cuadro:", data2)
                  

                     doc.autoTable(columns2,data2,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} , 
                                                startY : y_ini+110,
                                                margin:{ right: 20 },
                                               
                                                drawCell: function(cell, data){
                                                      var index = data.row.index
                                                      var rows = data.table.rows
                                                      //console.log(data)
                                                      
                                                        rect1_width = data.row.cells.factura.width + data.row.cells.fecha_factura.width
                                                        rect2_width = data.row.cells.provider.width + data.row.cells.monto_factura.width
                                                        x1_rect = data.row.cells.factura.x
                                                        x2_rect = data.row.cells.provider.x
                                                        cell_y = cell.y+7.58
                                                      if( index == rows.length - 1){
                                                        doc.rect(x1_rect, cell_y, rect1_width, cell.height,  null)    
                                                        doc.rect(x2_rect, cell_y, rect2_width, cell.height,  null)
                                                      }  
                                                    
                                                    }


                                              })
                     // Amount Total Invoices
                  
                     doc.setFontType('bold');
                     doc.text(x1_rect+20, cell_y+5.58, 'Total');      
                     doc.setFontType('normal');
                     doc.text(x2_rect+15, cell_y+5.58, amount_invoices)    
                    //
                   
                  if(docs[i].rowData2.length > 0){
                      doc.text(x_ini, cell_y+17, "Documentos Adicionales")    

                  //// 2da tabla - invoices
                    var columns3 = [{title:"Tipo de Documento", dataKey:"doc_type"}, 
                                    {title:"Fecha", dataKey:"fecha_doc"}, 
                                    {title:"Descripción", dataKey:"description"}]

                                    
                    angular.forEach(docs[i].rowData2, function(value){
                      
                      data3.push({doc_type: value.doc_type, fecha_doc: value.date, description:value.description_other})

                    })      

                    doc.autoTable(columns3, data3,  {
                                                theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                startY: cell_y+22,
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ right: 20 },
                                                drawCell: function(cell, data){
                                                  cell_y = cell.y
                                                }

                                              })          
                  //
                   //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[i].user)   
                  }else{
                  let pageHeight = doc.internal.pageSize.getHeight()
                  console.log("pageHeight", pageHeight)
                  //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[i].user)    
                  //  
                  }

                  if(!!window.chrome && !!window.chrome.webstore){
                     window.open(doc.output('bloburl'), '_blank')
                  }else{
                     doc.output('dataurlnewwindow',{})       //creacion de los archivos
                  }
                }
                  
               }).then(()=> {
                  swal({
                    title: 'Despachado!',
                    text: 'El(los) trámite(s) '+claimstext+' ha(n) sido enviado(s).',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                     $state.reload()
                })

               })
      
       
      
      /*API.one('claims').one('claim', arregloclaims[i]).remove()
        .then(() => {
          swal({
            title: 'Despachado!',
            text: 'El(los) trámite(s) '+claims+' ha(n) sido enviado(s) a Reclamos.',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
          }, function () {
            $state.reload()
          })
        })*/
  
    })
  }    

    sendAgent (size, name, id, policy_id) {
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a ", id);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalAgent',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    modalDiagnostic (size, name, id, policy_id) {
      console.log("Entro al modal")
      let $uibModal = this.$uibModal
      let $scope = this.$scope
      let $log = this.$log
      let items = this.items      
      console.log("a", name);
      var modalInstance = $uibModal.open({
        
        templateUrl: 'myModalDiagnostic',
        controller: this.modalcontroller,
        controllerAs: 'mvm',
        size: size,
        resolve: {
          items: () => {
            return items
          },customer: () => {
            return name
          },id: () => {
            return id
          },policy_id: () => {
            return policy_id
          }
        }
      })

      modalInstance.result.then((selectedItem) => {
        $scope.selected = selectedItem
      }, () => {
        $log.info('Modal dismissed at: ' + new Date())
      })
    }

    toggleModalAnimation () {
      this.animationsEnabled = !this.animationsEnabled
    }

    modalcontroller ($scope, $uibModalInstance, API, items, customer, id, $http, $state, $stateParams, policy_id) {
    'ngInject'

      console.log("controller")
      this.API = API
      this.items = items
      this.customer = customer
      this.id = id
      this.policy_id = policy_id
      this.$state = $state
      this.sendmail = sendmail
      this.diagnosticId = {}
      this.isDisabled = true
      var mvm = this
      this.processing = false
      this.isDisabled = true

      if ($stateParams.alerts) {
        this.alerts.push($stateParams.alerts)
      }
        
      let emailformatId = 7
        //console.log(companyId)
      let EmailFormatData = API.service('show', API.all('email_formats'))
      EmailFormatData.one(emailformatId).get()
        .then((response) => {
          this.emailformateditdata = API.copy(response)
          this.emailformateditdata.data.subject = this.emailformateditdata.data.subject.replace('applicant_name', this.customer)
          //console.log("b", this.emailformateditdata)
      })
        let Agent =   API.service('policyagent', API.all('policies'))
        Agent.one(policy_id).get()
          .then((response) => {
            this.agentdata = API.copy(response)
            console.log("agente", this.agentdata)
        })

      

        let diagnostics = API.service('diagnosticsclaim', API.all('diagnostics'))
      diagnostics.one(id).get()
        .then((response) => {
          let dataSet = response.plain()
          this.diagnostics = dataSet.data
          console.log("diagnostics", this.diagnostics)
      })
    

      let Diagnostic = this.API.service('claimdiagnostics', API.all('diagnostics'))
        Diagnostic.one(id).get()
         .then((response) => {
            let dataSet = response.plain()
            this.diagnostic = dataSet.data
            console.log("Data", this.diagnostic)

            /*for (var i = 0; i < dataSet.data.length ; i++) {
              mvm.diagnosticId.push({dataSet.data[i].id})
            }
            console.log("diagnosticId", mvm.diagnosticId)
            */
      })


      function sendmail(answer){
        console.log("id ", id)
        console.log("answer ", answer)
          API.service('send').post({
              policy_id : id,
              format_id : 7,
              answer : answer
          } )
          .then( ( response ) => {
              $state.reload();
          }, function (response) {
                  console.log('error')
              }
          )
          console.log("xxxx")
       }



      $scope.selected = {
        item: items[0]
      }      

      this.cancel = () => {
        $uibModalInstance.dismiss('cancel')
      }

      this.saveResponse = (answer, id) => {
        var form_data = new FormData();
        let Policies = this.API.service('updatePolicies');
        
        form_data.append('policyId', id);
        form_data.append('customer_response', answer);
        
        let $state = this.$state        
        //Weight.post({
        $http.post('updatePolicies', form_data,
        {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined,'Process-Data': false}
        }).success(function(response){
          $uibModalInstance.dismiss('cancel')
          swal("Éxito!", "Respuesta enviada.");
          sendmail(answer)
          $state.reload()
        })
      }

      this.sendAgent = (answer, id, content, policy_id) => {
        mvm.processing = false
        console.log("id ", id)
        console.log("answer ", answer)
        console.log("content ", content)
        //if (answer == 1) {
          console.log("x el SI ", mvm.processing)
          API.service('send').post({
              claim_id : id,
              policy_id : policy_id,
              format_id : 7,
              answer : 1,
              content : content,
              diagnostics : this.diagnostics

          } )
          .then( ( response ) => {
              mvm.processing = false
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
              $uibModalInstance.dismiss('cancel')
            })
          }, function (response) {
                mvm.processing = false
                console.log('error')
              }
          )
          console.log("xxxx")
       }


this.verifyCheck = function(){
          let check = false

        angular.forEach(this.diagnostics,function(value){
            angular.forEach(value.documents,function(value2){
                if (value2.checked == true)    {
                    check = true
                }
            })
        })
        this.sendbutton = check
      }

        this.modalDiagnostic = (claimId, policyId) => {
        this.processing = true
        console.log("id ", id)
        console.log("answer ", answer)
        console.log("content ", content)
        //if (answer == 1) {
          console.log("x el SI")
          API.service('send').post({
              policy_id : id,
              format_id : 7,
              answer : 1,
              content : content,
              diagnostics : this.diagnostics

          } )
          .then( ( response ) => {
              mvm.processing = false
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
              $uibModalInstance.dismiss('cancel')
            })
          }, function (response) {
                mvm.processing = false
                console.log('error')
              }
          )
          console.log("xxxx")
       }




       /********************************************/
       this.activeDispatch = () => {        
        let diagnosticId = []
        diagnosticId = mvm.diagnosticId
        var countTrue = 0
        var countFalse = 0
        var countTotal = 0
        angular.forEach(mvm.diagnosticId,function(value,index,arr){
            if(value == true){
                countTrue++
            }else{
                //mvm.isDisabled = true
                countFalse++
            }

        })
        countTotal = countTrue + countFalse
          if( countTrue > 0){
              mvm.isDisabled = false
          }else{
              mvm.isDisabled = true
          }

       }
       
     
       this.dispatch = () => {

        let API = this.API
        let $state = this.$state
        this.printDoc = printDoc
        let diagnosticId = []
        //diagnosticId = mvm.diagnosticId
        console.log("A despachar", mvm.diagnosticId)
        angular.forEach(mvm.diagnosticId,function(value, index, arr){
          if(value == true){
            console.log("index", index)
            diagnosticId.push({id: index, value: value}) 
          }

        })
        
        
        console.log("Diagnosticos", diagnosticId)
        console.log("Reclamo", mvm.id)

      function printDoc(diagnosticId, id){
        console.log("Diagnosticos", diagnosticId)
        console.log("Reclamo", id)
        let Document = API.service('printdoc', API.all('claims') )
        Document.post({
          'claim_id': id,
          'diagnosticId' : diagnosticId

        })
        .then((response) => {
          let contentDoc =  API.copy(response)
          //console.log(contentDoc)
          let popupWinindow 
          popupWinindow = window.open('', '_blank','width=800,height=800,scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no'); 
          popupWinindow.document.open(); 
          popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" /></head><body onload="window.print();">'+contentDoc.data+'</html>');  
          popupWinindow.document.close(); 
        })

          
        }

       
           swal({
               title: "Está Seguro?",
               text: "Despachar los Diagnósticos",
               type: "warning",
               showCancelButton: true,
               confirmButtonClass: "btn-danger",
               confirmButtonText: "Sí, despáchalo!",
               closeOnConfirm: false
           },function(){
               this.$state = $state
               dispatchPDF(diagnosticId, id)
               let Diagnostics = API.service('dispatchpatient', API.all('diagnostics'))
               Diagnostics.post({
                   'claimId': id,
                   'diagnosticId' : diagnosticId,                   
                   'dispatch_type': 2
               }).then(function () {
                   swal({
                       title: 'Reclamo Despachado Correctamente!',
                       type: 'success',
                       confirmButtonText: 'OK',
                       closeOnConfirm: true
                   }, function () {
                       $state.reload()
                       $uibModalInstance.dismiss('cancel')
                   })
               }, function (response) {
                   swal({
                       title: 'Error Inesperado!',
                       type: 'error',
                       confirmButtonText: 'OK',
                       closeOnConfirm: true
                   }, function () {})
               })
           })

              /********************************************************************************************************************************************/
       function dispatchPDF (diagnosticId, claim_id ) {
    //let API = this.API
    //let $state = this.$state
    var claims = []
    var claimstext = ''
    
    claimstext = claimstext.substr(1);
    console.log("claims", claims)
    //claims = claims.substr(1);
    //console.log("claims ", claims)
    //var arregloclaims=claims.split(',');
   

  
      console.log("claims", claims)
      
      let Diagnostics = API.service('printdocpdf', API.all('claims') )
        Diagnostics.post({
          'claim_id': claim_id,
          'diagnosticId' : diagnosticId

        }).then( (response) => {
                  
                  let docs = response.plain()
                  docs = docs.data
                  console.log("datos", docs)
                  
                  //
                  for (var i = 0 ; i < docs.length; i++) {                  
                    var doc = new jsPDF('p', 'mm', 'letter');
                  console.log("doc",doc)
                  var size1 = 11
                  var size2 = 14
                  var sizeT = 10

                  doc.setFontSize(size1);
                  let x_ini = 20
                  let x_fin = 210
                  let y_ini = 20 
                  let sizeText = 0  

                  var rect1_width = 0
                  var rect2_width = 0
                  var x1_rect = 0  // posicion inicial x de la celda para el total en el 1er cuadro
                  var x2_rect = 0  // posicion inicial x de la celda para el total en el 2do cuadro
                  var cell_y = 0  //posicion Y de la celda para el total en el 1er cuadro

                  var data2 = []
                  var data3 = []
                  

                  //
                    sizeText = doc.getStringUnitWidth (docs[0].date) * size1
                    sizeText = sizeText / 2.81
                    let x = x_fin - sizeText  - 10 
                    doc.text(x, 20, docs[0].date); // fecha
                  // 
                    doc.text(x_ini, y_ini+20, 'Señores');
                    doc.setFontType("bold");
                    doc.text(x_ini, y_ini+25, 'BEST DOCTORS');
                    doc.setFontType('normal');
                    doc.text(x_ini, y_ini+30, 'Ciudad.-');
                  //
                    y_ini = y_ini + 30
                    doc.text(x_ini, y_ini+20, 'Atte. Dpto. de Reclamos');
                    doc.text(x_ini, y_ini+30, 'De mis consideraciones:');
                    doc.text(x_ini, y_ini+40, 'Por medio del presente adjunto el siguiente reclamo:');
                  //
                    //console.log("afiliado", docs[i].affiliate)
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+55, 'Asegurado: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+55, docs[i].affiliate);
                    doc.setFontType('bold');
                    doc.text(x_ini+20, y_ini+60, 'Póliza: ');
                    doc.setFontType('normal');
                    doc.text(x_ini+50, y_ini+60,  docs[i].policy_number);  

                    // 1era tabla
                   
                    var columns1 = ["Fecha Enviado", "#GB", "Titular", "# Póliza", "Paciente", "Diágnostico", "Monto"];
                    var data1 = [[docs[i].rowData.date, docs[i].claim_id, docs[i].applicant, docs[i].policy_number, docs[i].affiliate, docs[i].rowData.diagnostic, docs[i].rowData.amount]]
                    //var data1 = [["1","2","3","4","5","6","7"]]
                    //console.log(doc.autoTable(["uno"],[2],{margin:{ top: 25 }}))
                    doc.autoTable(columns1,data1,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                //tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:15, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'},
                                                 4: {overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 5:{overflow: 'linebreak', columnWidth:35, halign: 'center',valign: 'middle'},
                                                 6:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'}
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ top: y_ini+70, right: 20 }
                                              })

                    doc.text(x_ini, y_ini+105, 'Detalles de los Documentos');

                    // 2da tabla - invoices
                    var columns2 = [{title:"Facturas", dataKey:"factura"}, 
                                    {title:"Fecha", dataKey:"fecha_factura"}, 
                                    {title:"Proveedor", dataKey:"provider"}, 
                                    {title:"Valor", dataKey:"monto_factura"}];
                   
                    //console.log("antes del forEach "+i,docs[i])
                    angular.forEach(docs[i].rowData1, function(value){
                      
                      data2.push({factura: value.factura, fecha_factura: value.fecha_factura, provider:value.provider, monto_factura:value.monto_factura})

                    })
                    var lines = data2.length -1
                    var amount_invoices = docs[i].total
                    //console.log("cuadro:", data2)
                  

                     doc.autoTable(columns2,data2,
                                                {theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                styles: {fontSize: 10, lineWidth: 0.5} , 
                                                startY : y_ini+110,
                                                margin:{ right: 20 },
                                               
                                                drawCell: function(cell, data){
                                                      var index = data.row.index
                                                      var rows = data.table.rows
                                                      //console.log(data)
                                                      
                                                        rect1_width = data.row.cells.factura.width + data.row.cells.fecha_factura.width
                                                        rect2_width = data.row.cells.provider.width + data.row.cells.monto_factura.width
                                                        x1_rect = data.row.cells.factura.x
                                                        x2_rect = data.row.cells.provider.x
                                                        cell_y = cell.y+7.58
                                                      if( index == rows.length - 1){
                                                        doc.rect(x1_rect, cell_y, rect1_width, cell.height,  null)    
                                                        doc.rect(x2_rect, cell_y, rect2_width, cell.height,  null)
                                                      }  
                                                    
                                                    }


                                              })
                     // Amount Total Invoices
                  
                     doc.setFontType('bold');
                     doc.text(x1_rect+20, cell_y+5.58, 'Total');      
                     doc.setFontType('normal');
                     doc.text(x2_rect+23, cell_y+5.58, amount_invoices)    
                    //
                   
                  if(docs[i].rowData2.length > 0){
                      doc.text(x_ini, cell_y+17, "Documentos Adicionales")    

                  //// 2da tabla - invoices
                    var columns3 = [{title:"Tipo de Documento", dataKey:"doc_type"}, 
                                    {title:"Fecha", dataKey:"fecha_doc"}, 
                                    {title:"Descripción", dataKey:"description"}]

                                    
                    angular.forEach(docs[i].rowData2, function(value){
                      
                      data3.push({doc_type: value.doc_type, fecha_doc: value.date, description:value.description_other})

                    })      

                    doc.autoTable(columns3, data3,  {
                                                theme: 'plain', //plain, grid
                                                bodyStyles: {valign: 'middle'},
                                                tableWidth : 'wrap',
                                                columnStyles: {
                                                 0:{overflow: 'linebreak', columnWidth:20, halign: 'center',valign: 'middle'},
                                                 1: {overflow: 'linebreak', columnWidth:22, halign: 'center',valign: 'middle'},
                                                 2:{overflow: 'linebreak', columnWidth:25, halign: 'center',valign: 'middle'},
                                                 3:{overflow: 'linebreak',columnWidth:25, halign: 'center',valign: 'middle'}
                                                 
                                                  },
                                                headerStyles: {overflow: 'linebreak', halign: 'center', valign: 'middle', text: {columnWidth: 'auto'}
                                                },
                                                startY: cell_y+22,
                                                styles: {fontSize: 10, lineWidth: 0.5} ,  
                                                margin:{ right: 20 },
                                                drawCell: function(cell, data){
                                                  cell_y = cell.y
                                                }

                                              })          
                  //
                   //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[i].user)   
                  }else{
                  let pageHeight = doc.internal.pageSize.getHeight()
                  console.log("pageHeight", pageHeight)
                  //console.log("valor de y", cell_y)
                  // ultimo parrafo
                  doc.text(x_ini, cell_y+20, "Sin otro particular por el momento, sucribo con un cordial saludo.")    
                  doc.text(x_ini, cell_y+30, "Atentamente.")    

                  doc.setFontType('bold')
                  doc.text(x_ini, cell_y+40, docs[i].user)    
                  //  
                  }

                  if(!!window.chrome && !!window.chrome.webstore){
                     window.open(doc.output('bloburl'), '_blank')
                  }else{
                     doc.output('dataurlnewwindow',{})       //creacion de los archivos
                  }
                }
                  
               }).then(()=> {
                  swal({
                    title: 'Despachado!',
                    text: 'El(los) diágnostico(s) ha(n) sido despachado(s).',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                     $state.reload()
                })

               })
      
       
  
  
    
  }    

       /********************************************************************************************************************************************/


    }
       /********************************************/





    }

    sendMail (policyId) {
      let API = this.API
      let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Desea enviar los datos de la póliza a Recepción!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        console.log("entro", policyId)
          API.service('send').post({
              policy_id : policyId,
              format_id : 7,
              answer : 1
          } )
          .then( ( response ) => {             
            swal({
              title: 'Enviado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          }, function (response) {
                  console.log('error')
              }
          )


      })
    }

    closed (claimId) {
      let API = this.API
      let $state = this.$state
      //this.seleccionados = this.seleccionados.shift();    
      let Claims = this.API.service('closedClaim')

      console.log("claim ", Claims)   

      swal({
        title: 'Está seguro?',
        text: 'Se cerrará el Ticket.',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, cerrar!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: true
      }, function () {
        console.log("aca")
        //let Claims = this.API.service('closedClaim')
          Claims.post({
          'id_claim' : claimId
        }).then(function (response) {
              swal({
              title: 'Cerrado!',
              text: 'El Ticket ha sido cerrado.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
        }, function (response) {
        })
      })
    }


  
    

   

    $onInit(){
    }
}

export const ClaimClaimListsComponent = {
    templateUrl: './views/app/components/claim-claim-lists/claim-claim-lists.component.html',
    controller: ClaimClaimListsController,
    controllerAs: 'vm',
    bindings: {}
}
