class ConfirmPaymentController{
    constructor($state, API, $stateParams, $http){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.uploadFile = uploadFile
        this.alerts = []
        this.methods = []
        this.account_types = []
        this.card_types = []
        this.card_marks = []
        this.payment_types = []
        this.lines = []
        this.methods.push({id: 0, name: 'Cheque'},
                         {id: 1, name: 'Transferencia'},
                         {id: 2, name: 'Depósito Bancario'},
                         {id: 3, name: 'Tarjeta de Crédito'})
        this.account_types.push({id: 1, name: 'Ahorros'},
                         {id: 2, name: 'Corriente'})
        this.card_types.push({id: 1, name: 'Corporativa'},
                         {id: 2, name: 'Personal'})
        this.card_marks.push(
            {id: 1, name: 'American Express'},
            {id: 2, name: 'Visa'},
            {id: 3, name: 'Mastercard'},
            {id: 4, name: 'Diners Club'},
            {id: 5, name: 'Otros'})
        this.payment_types.push(
            {id: 1, name: 'Diferido'},
            {id: 2, name: 'Con intereses'},
            {id: 3, name: 'Corriente'},
            {id: 4, name: 'Sin interés'},
            {id: 5, name: '3 meses'},
            {id: 6, name: '6 meses'},
            {id: 7, name: '9 meses'},
            {id: 8, name: '12 meses'})

        let policyId = $stateParams.policyId
        this.policyId = policyId

        let Policy = this.API.service('getPolicyInfo');

        let data = {};
        data.policy_id=   $stateParams.policyId;

        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            console.log("data", this.policyinfo)
            this.client = this.policyinfo[0].first_name+" "+this.policyinfo[0].last_name
            this.planDeduc = this.policyinfo[0].plan+" / "+this.policyinfo[0].deductible
            this.policyinfo[0].first_value = '$'+this.policyinfo[0].first_value
            this.form = this.policyinfo[0]
            this.form.payment_date = new Date( this.form.payment_date+' ')
            console.log('vm.form.method',this.form.method)
            this.form.first_value =  this.form.first_value

            let attached = {}
            attached.payment_id = this.form.payment_id
            let attacheds = this.API.service('attacheds')
            attacheds.getList(attached).then((response) => {
                this.previous_lines = response.plain()
                this.description = this.previous_lines[0] ? this.previous_lines[0].description : ''
                this.filename = this.previous_lines[0] ? this.previous_lines[0].filename : ''
            })
        })

        function uploadFile(){
            var files = vm.files
                angular.forEach(files, function(files){
                var form_data = new FormData();
                form_data.append('file', files);
                form_data.append('policy_id', vm.policyId);
                form_data.append('folder', 'authorization_form/');
                $http.post('uploadfilespayment', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    API.service('send').post({
                        policy_id : vm.policyId,
                        payment_id: vm.form.payment_id,
                        format_id : 5,
                        payment_method_name: vm.methods[vm.form.method].name,
                        payment_method: vm.form.method,
                        mode: vm.form.mode,
                        card_mark: vm.form.method == 3 ? vm.card_marks[vm.form.card_mark-1].name : '',
                        card_type: vm.form.method == 3 ? vm.card_types[vm.form.card_type-1].name : ''
                    })
                    .then( ( response ) => {
                        swal({
                            title: 'Confirmación de Pago Exitosa!',
                            type: 'success',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                            $state.go('app.emission-policy-lists')
                        })
                    }, function (response) {
                        swal({
                            title: 'Error al Confirmar Pago!',
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {
                        })
                    }
                    )
                })
            })
         }
    }

    save(isValid){
        if(isValid){
            this.isDisabled = true;
            let confirm = this.API.service('confirmpayment', this.API.all('policies'))
            let $state = this.$state
            let uploadFile = this.uploadFile
            confirm.post({
                'policyId': this.policyId,
                'payment_id': this.form.payment_id,
                'filename': this.myFile.name
            }).then(function () {
                console.log('success')
                uploadFile()
                
            }, function (response) {
                swal({
                    title: 'Error al Confirmar Pago!',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })

            })
        }
        else{
            this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const ConfirmPaymentComponent = {
    templateUrl: './views/app/components/confirm-payment/confirm-payment.component.html',
    controller: ConfirmPaymentController,
    controllerAs: 'vm',
    bindings: {}
}
