class EmissionMedicalInformationController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.current_step = '4'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        var quiz = []
        var systemQuizDet = []
        var systemQuiz = []
        var answer = []
        var observation = []
        var repairable = []
        var quiz_id = []
        var quiz_detail_id = 0
        var quizDetResponse = []

        var policyId = $stateParams.policyId
        this.policyId = policyId

        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})
            })

            this.systemWizar = systemWizar
            //console.log("systemWizar==> ", systemWizar)
        })


        let Quiz = this.API.service('quiz')
        Quiz.getList()
          .then((response) => {
            //let systemQuiz = []
            let quizResponse = response.plain()
            //console.log("Response", quizResponse)
                angular.forEach(quizResponse, function (value) {
                    systemQuiz.push({id: value.id, name: value.name})
                })//end foreach quizResponse            

                this.systemQuiz = systemQuiz


        })


        let QuizDet = this.API.service('quizdet')
        QuizDet.getList()
          .then((response) => {
            //let systemQuizDet = []
            quizDetResponse = response.plain()
            //console.log("Response", quizResponse)
                angular.forEach(quizDetResponse, function (value) {
                    systemQuizDet.push({id: value.id, name: value.name, quiz_id: value.quiz_id})
                    quiz.push(value.id)
                    quiz_id.push(value.quiz_id)
                })//end foreach quizResponse            

                this.systemQuizDet = systemQuizDet
                console.log("cantidad de preguntas", systemQuizDet)
                this.quiz =  quiz
                this.quiz_id = quiz_id

        })

        let MedicalData = API.service('show', API.all('medicalinformations'))      
        MedicalData.one(policyId).get()
          .then((response) => {
            this.medicaleditdata = API.copy(response)
            
            let medicalResponse = response.plain(response)
            console.log("medicaleditdata ", this.medicaleditdata.data)
            
            if(this.medicaleditdata.data.length > 0){

                for(let i = 0; i < this.medicaleditdata.data.length; i++){
                    let j = this.medicaleditdata.data[i].quiz_details_id
                    
                    if(this.medicaleditdata.data[i].answer == '1'){
                        console.log("la answer x el true", this.medicaleditdata.data[i].answer)
                        answer[j] = true
                    }else{
                        console.log("la answer x el false", this.medicaleditdata.data[i].answer)
                        answer[j] = false
                    }                     
                    repairable[j] = this.medicaleditdata.data[i].repairable_id
                    observation[j] = this.medicaleditdata.data[i].observation
                    //answer.push(this.medicaleditdata.data[i].answer)
                    //repairable.push(this.medicaleditdata.data[i].repairable_id)
                    //observation.push(this.medicaleditdata.data[i].observation)
                    
                }
                this.answer = answer
                //this.answer.shift();
                this.repairable = repairable
                this.observation = observation
                console.log("Preguntas de BD", this.answer)
                console.log("Subsanables de BD", this.repairable)
                //console.log("Observaciones de BD", this.observation)

             
            }else{
                //console.log("ENTRO AQUI PORQUE NO HAY DATOS")
                /*for(let i = 0; i < quizDetResponse.length; i++){
                    answer[i+1] = 0
                    repairable[i+1] = 0
                    observation[i+1] = ""
                }
                */
                this.answer = answer
                this.repairable = repairable
                this.observation = observation
                console.log("Subsanables", this.repairable)

            }
                //console.log("Respuestas", this.answer)
                //console.log("Subsanables", this.repairable)
                //console.log("Obs", this.observation)
    
      })   
          
            



        //
    }

    changeSwitch(repairable, observation, key, answer){        
        console.log("switch", repairable + "/" + observation + "/" + key)
        //repairable[key]vm.answer[item.id] = '0'
        console.log("answer ", answer)        

    }

    save (isValid) {
           console.log("Entro", this.answer)         
        if (isValid) {            
            this.isDisabled = true;
            let $state = this.$state
                
           if(this.medicaleditdata.data.length == 0){
            
            this.answer.shift();
            this.repairable.shift();
            this.observation.shift();
            let answers = []
            for(let i = 0; i < this.quiz.length; i++){
                if (this.answer[i] === true) {
                    answers.push('1')
                }else{
                    answers.push('0')
                }
            }
            this.answers = answers
            console.log("this.policyId ",this.policyId)
            console.log("this.quiz_id ",this.quiz_id)
            console.log("this.quiz ",this.quiz)
            console.log("this.answer ",this.answers)
            console.log("this.repairable ",this.repairable)
            console.log("this.observation ",this.observation)
            let MedicalInfo = this.API.service('medical', this.API.all('medicalinformations'))
            MedicalInfo.post({
                'policy_id': this.policyId,
                'quiz_id': this.quiz_id,
                'quiz_details_id': this.quiz,
                'answer': this.answers,
                'repairable_id': this.repairable,
                'observation': this.observation
            }).then(function (response) {
                swal({
                    title: 'Datos Médicos Guardados Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                        $state.go('app.emission-payment-information', {"policyId": response.data})
                })
            }, function (response) {
                swal({
                    title: 'Error ' + response.data.message,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go($state.current)
                })

            })
            } else{
                
                
                
                for(let i = 0; i < this.medicaleditdata.data.length; i++){
                    if(this.answer[i+1] == true || this.answer[i+1] == 1){
                        this.medicaleditdata.data[i].answer = 1
                        this.medicaleditdata.data[i].repairable_id = this.repairable[i+1]
                        this.medicaleditdata.data[i].observation = this.observation[i+1]
                    }else{
                        this.medicaleditdata.data[i].answer = 0
                        this.medicaleditdata.data[i].repairable_id = 0
                        this.medicaleditdata.data[i].observation = ''
                    }
                    //this.medicaleditdata.data[i].answer = this.answer[i]
                    
                    

                }
               console.log("preguntas para actualizar",  this.medicaleditdata.data)
                let $state = this.$state
                this.medicaleditdata.put()
                .then((response) => {
                    swal({
                    title: 'Datos Médicos Guardados Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                        $state.go('app.emission-payment-information', {"policyId": response.data})
                })
                }, (response) => {
                    swal({
                    title: 'Error ' + response.data.message,
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                        $state.go($state.current, { alerts: alert})
                })
                })

            }

            }//if
            else {
                this.formSubmitted = true
            }
    }

    $onInit(){
    }
}

export const EmissionMedicalInformationComponent = {
    templateUrl: './views/app/components/emission-medical-information/emission-medical-information.component.html',
    controller: EmissionMedicalInformationController,
    controllerAs: 'vm',
    bindings: {}
}
