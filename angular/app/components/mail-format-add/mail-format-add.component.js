var module = 'email_formats'
var sub = 'show'
var _name = 'Servicio'
//var folder = 'servicios-new'

class MailFormatAddController{
    constructor($stateParams, $state, API){
        'ngInject';

        this.$state = $state
    this.formSubmitted = false
    this.alerts = []
    this.userRolesSelected = []
    this.theform = {}
    this.API = API
    this.modulo = 'Formatos'
    this.modulosingular = 'Formato'
    this.type = 'creado';





    let id = $stateParams.id
    this.id = id

    if ($stateParams.alerts) {
      this.alerts.push($stateParams.alerts)
    }

    // let userId = $stateParams.userId

    if(id){

      this.type = 'modificado'

      let data = API.service('show', API.all(module))
      data.one(id).get()
        .then((response) => {
          this.theform = API.copy(response);
          console.log(API.copy(response));
        })

    }
  }

    save (isValid) {
    this.alerts = [];
    if (isValid) {
      let api = this.API.service(sub, this.API.all(module))
      let $state = this.$state

      var method = null

      //this.procesando = true

      if(this.id)
        method = this.theform.put()
      else
        method = api.post(this.theform)

      method
        .then(() => {
          let alert = { type: 'success', 'title': 'Éxito!', msg: _name+' ha sido '+this.type+' exitosamente.' }
          $state.go($state.current, { alerts: alert})
        }, (response) => {
      //    this.procesando = false
          let message = response.data.message;
          // console.log(response.data.errors, response.data.errors.length)
          if(response.data.errors){
            for(var i in response.data.errors){
              message = response.data.errors[i][0]
            }
            //message = response.data.errors[0]
          }
          let alert = { type: 'error', 'title': 'Error!', msg: message }
          this.alerts = [alert];
          // $state.go($state.current, { alerts: alert})
        })
    } else {
      this.formSubmitted = true
    }
  }
    

    $onInit(){
    }
}

export const MailFormatAddComponent = {
    templateUrl: './views/app/components/mail-format-add/mail-format-add.component.html',
    controller: MailFormatAddController,
    controllerAs: 'vm',
    bindings: {}
}
