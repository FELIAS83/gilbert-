class CoverageListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
        this.API = API
        this.$state = $state
        this.can = AclService.can
       


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let planId = $stateParams.planId
        this.planId =  planId
        let branchId = $stateParams.branchId
        this.branchId = branchId
        let companyId = $stateParams.companyId
        this.companyId = companyId
        //console.log("id del plan", planId)
        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(planId).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
            console.log("data del plan", this.planeditdata.data)
            let planResponse = response.plain(response)
            
        
      })

        let Coverage = API.service('coverage', API.all('coverages'))
        Coverage.one(planId).get()
          .then((response) => {

            let dataSet = response.plain()
            console.log("data", dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()

           this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })  

           let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        let actionsHtml = (data) =>  {
          return`<ul>
                    <!--<a  title="Ver/Agregar  Coberturas" class="btn btn-xs btn-success" ui-sref="app.coverage-details-add({planId: ${planId}})">
                        <i class="fa fa-check-square"></i>
                    </a>
                    &nbsp -->
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.coverage-edit({coverageId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }//constructor
    delete (deductibleId) {
      let API = this.API
      let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la cobertura!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('coverage').one('coverages', coverageId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const CoverageListsComponent = {
    templateUrl: './views/app/components/coverage-lists/coverage-lists.component.html',
    controller: CoverageListsController,
    controllerAs: 'vm',
    bindings: {}
}
