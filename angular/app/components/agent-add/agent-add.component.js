class AgentAddController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        vm.fecha_actual= new Date()
        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            console.log("Paises", countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            console.log("systemCountrys==> ", systemCountrys)
        })

          let Agents = this.API.service("agents")
           Agents.getList({})
           .then((response) => {
                let agentData = response.plain()
                console.log("agentData", agentData)
                let systemAgent = []
                angular.forEach(agentData, function(value){
                    systemAgent.push({id:value.id, first_name:value.first_name, last_name:value.last_name})
                })

                this.systemAgent = systemAgent
                console.log("this.systemAgent",this.systemAgent)


          })


    function getState(){
            console.log("Entro..", vm.country)
            let States = API.service('province', API.all('states'))
            States.one(vm.country).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                     console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        systemStates.push({id: value.id, name: value.name})
                    })
                this.systemStates = systemStates
                console.log("systemStates  ==> ", systemStates)          

                })

        }


    function getCity(){
            let Citys = API.service('city', API.all('cities'))
            Citys.one(vm.state).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                     console.log(cityResponse)
                    angular.forEach(cityResponse.data.cities, function (value) {
                        systemCities.push({id: value.id, name: value.name})
                    })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", systemCities)          

                })

        }    
        
    }//fin constructor


    save (isValid) {  

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {          
          let Agents = this.API.service('agents', this.API.all('agents'))
          let $state = this.$state 
          console.log("lider select",  this.leader)
          if(this.leader ==  undefined){
            this.leader = 0
          }      

          Agents.post({
            'first_name': this.name,
            'last_name' : this.last_name,
            'identity_document' : this.identity,
            'birthday' : this.birthday,
            'email' : this.email,
            'skype' : this.skype,
            'mobile' : this.mobile,
            'phone' : this.phone,
            'country_id' : this.country,
            'province_id' : this.state,
            'city_id' : this.city,
            'address' : this.address,
            'commission' : this.commission,
            'leader': this.leader
          }).then(function () {
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el agente.' }
            $state.go($state.current, { alerts: alert})
            console.log("MENSAJE 2", $state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }




    $onInit(){
    }
}//fin class

export const AgentAddComponent = {
    templateUrl: './views/app/components/agent-add/agent-add.component.html',
    controller: AgentAddController,
    controllerAs: 'vm',
    bindings: {}
}
