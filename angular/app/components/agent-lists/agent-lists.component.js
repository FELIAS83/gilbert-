class AgentListsController{
   constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        this.API = API
        this.$state = $state
        this.can = AclService.can

        let Agents = API.service('agents')

        Agents.getList({})
          .then((response) => {

            let dataSet = response.plain()
            console.log(dataSet)

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })

            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('first_name').withTitle('Nombres'),
              DTColumnBuilder.newColumn('last_name').withTitle('Apellidos'),
              DTColumnBuilder.newColumn('identity_document').withTitle('N° de Identificación'),
              DTColumnBuilder.newColumn('mobile').withTitle('Móvil'),              
              DTColumnBuilder.newColumn('phone').withTitle('Teléfono'),              
              DTColumnBuilder.newColumn('email').withTitle('E-mail'),
              //DTColumnBuilder.newColumn('address').withTitle('Dirección'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) =>  {
          return`<ul>
                    <a title="Asistentes" class="btn btn-xs btn-info" ui-sref="app.assistent-lists({agentId: ${data.id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    &nbsp
                    <!--<a  title="Agregar Sub-Agente" class="btn btn-xs btn-success" ui-sref="app.agent-sublists({agentId: ${data.id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    &nbsp-->
                    <a ng-show="vm.can('manage.sales')" title="Editar" class="btn btn-xs btn-warning" ui-sref="app.agent-edit({agentId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <a  title="Configurar Comisiones" class="btn btn-success btn-xs" ui-sref="app.agent-commission-list({agentId: ${data.id}})">
                        <i class="fa fa-percent"></i>
                    </a>
                    &nbsp
                    <button ng-show="vm.can('manage.sales')" title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }




        //
    }// fin contructor

    delete (agentId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el Agente!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('agents').one('agent', agentId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }






    $onInit(){
    }
}

export const AgentListsComponent = {
    templateUrl: './views/app/components/agent-lists/agent-lists.component.html',
    controller: AgentListsController,
    controllerAs: 'vm',
    bindings: {}
}
