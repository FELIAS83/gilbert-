class MessengerEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let messengerId = $stateParams.messengerId
        console.log(messengerId)
        let MessengerData = API.service('show', API.all('messengers'))
        MessengerData.one(messengerId).get()
          .then((response) => {
            this.messengereditdata = API.copy(response)
          console.log("datos=>", this.messengereditdata);
        })
        //
    }

     save (isValid) {
        if (isValid) {
          this.processing = true
          let vm = this
          //console.log(this.messengereditdata);
          let $state = this.$state
          this.messengereditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el mensajero.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const MessengerEditComponent = {
    templateUrl: './views/app/components/messenger-edit/messenger-edit.component.html',
    controller: MessengerEditController,
    controllerAs: 'vm',
    bindings: {}
}
