class ClaimDocTypeDescriptionsAddController{
    constructor($scope, API,$compile, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let claimdoctypeId = $stateParams.claimdoctypeId
        //console.log("claimdoctypeId",claimdoctypeId)

        let ClaimDocumentTypes = API.service('show', API.all('claimdocumenttypes'))
        
        ClaimDocumentTypes.one(claimdoctypeId).get()
          .then((response) => {
            this.claimdoctypeditdata = API.copy(response)
            let ClaimDocTypesresponse= response.plain(response)
            console.log("claimdoctypeditdata",this.claimdoctypeditdata)
        })

    }

    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this       
          let ClaimDocTypeDesc = this.API.service('claimdocumenttypedescriptions', this.API.all('claimdocumenttypedescriptions'))
          let $state = this.$state          
          //console.log("claimdoctypeId",this.claimdoctypeditdata.data.id)
          ClaimDocTypeDesc.post({
            'id_doc_type':this.claimdoctypeditdata.data.id,
            'description': this.description
            }).then(function () {
                vm.processing = false
            //console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el tipo de Documento .' }
            $state.go($state.current, { alerts: alert})
            //console.log("mensaje2",$state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }

    }

    $onInit(){
    }
}

export const ClaimDocTypeDescriptionsAddComponent = {
    templateUrl: './views/app/components/claim-doc-type-descriptions-add/claim-doc-type-descriptions-add.component.html',
    controller: ClaimDocTypeDescriptionsAddController,
    controllerAs: 'vm',
    bindings: {}
}
