class CompanyListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject'
        this.API = API
        this.$state = $state

        let Companies = this.API.service('companies')
        
        Companies.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              //DTColumnBuilder.newColumn('type').withTitle('Tipo de Comisión').renderWith(dataComi),
              DTColumnBuilder.newColumn('commission_percentage').withTitle('% Comisión').renderWith(dataPercen),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let dataComi = (data) => {
          return `
                    <div ng-show='"${data}" == "F"'>
                      Fija 
                    </div>
                    <div ng-show='"${data}" == "V"'>
                      Variable
                    </div>
                  `
        }

        let dataPercen = (data) => {
          return `
                    <div >
                      ${data} %
                    </div>
                  `
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a  title="Ramos" class="btn btn-xs btn-info" ui-sref="app.branch-lists({companyId: ${data.id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    <!--&nbsp
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.company-edit({companyId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>-->
                  </ul>`
        }

    }

    delete (companyId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la compañía!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('companies').one('company', companyId).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const CompanyListsComponent = {
    templateUrl: './views/app/components/company-lists/company-lists.component.html',
    controller: CompanyListsController,
    controllerAs: 'vm',
    bindings: {}
}
