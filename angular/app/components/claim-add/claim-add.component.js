class ClaimAddController{
    constructor ($scope, API, $state, $stateParams,fileUpload,$http) {
        'ngInject';

        var vm = this
        this.API = API
        this.$state = $state
        this.$stateParams = $stateParams
        this.fileUpload = fileUpload
        this.uploadFile = uploadFile

        this.formSubmitted = false
        this.showhidepregnant = false;
        this.processing = false

        this.alerts = []
        this.linesreq = []
        this.linesreq.push({disabled : false});
        this.lines = []
        this.lines.push({id : 0, disabled : false, req : ''});
        //this.lines.push({disabled : false});
        //this.lines.push({id : 1});
        this.myFile = [] //array con datos de archivos
        this.systemDocTypes = []
        this.systemCurrency = []
        this.description = []
        this.value = []
        this.filenames = []
        this.doc_typeselected = []
        this.doc_typeselected[0] = 1
        this.curency_selected = []
        this.curency_selected[0] = 1
        //this.doc_typeselected[1] = 2
        this.files = []
        this.success = 0
        vm.changeDocType = changeDocType
        this.systemDescription = []
        
        this.typeDoc= []
        this.typeDoc.push({id:0 , value : 0});


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            //console.log("Tipos", docResponse)
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })
    
       
        this.systemDocTypes = systemDocTypes
        //console.log(this.systemDocTypes)


        let systemDescriptionDoc = []
        let systemDescription = []
        let systemDescription2 = []
        let systemDescription3 = []
        let DocDescription = this.API.service('claimdocumenttypedescriptions')
        DocDescription.getList()
        .then((response) => {
            let descriptionResponse = response.plain()
            angular.forEach(descriptionResponse, function(value, index, arr){
                
                
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                    
            })
            

        })
        

        this.systemDescriptionDoc = systemDescriptionDoc

       


        let systemCurrency = []
        let Currencies = this.API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
            //console.log("monedas ", this.systemCurrency)
        })

        let Policies = this.API.service('currentaffiliate', this.API.all('policies'))
        Policies.getList()
        .then((response) => {
            let systemPolicies = []
            let policiesResponse = response.plain()
            angular.forEach(policiesResponse, function (value) {
                systemPolicies.push({id: value.number, policy_id: value.id, name: value.name, role: value.role})
            })

            this.systemPolicies = systemPolicies
            //console.log(this.systemPolicies)
        })

        function uploadFile(){
            var i = 0
            for (i = 0; i < 100; i++) {
               if (this.files[i]){
                    var form_data = new FormData();
                    angular.forEach(this.files[i], function(file){
                        form_data.append('file', file);
                    })
                    $http.post('uploadclaims', form_data,
                    {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    }).success(function(response){    })
               }
            }
       }

       function changeDocType(index){
        let systemDescription = []
        this.indice = index
        //console.log("entro  con", index)
        if (vm.doc_typeselected[index] == '1') {
            //this.disabled = false
            vm.lines[index].disabled = false
            vm.lines[index].rep = ''
            vm.typeDoc[index].value = 0
            //this.noreq = ''
            this.curency_selected[index] = 1
        } else {
           angular.forEach(this.systemDescriptionDoc, function(value){
                if(value.id_doc_type == vm.doc_typeselected[index]){
                   systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 
                }

            })
            this.systemDescription[index] = []
            this.systemDescription[index] = systemDescription
            //this.disabled = true
            vm.lines[index].disabled = true
            //this.noreq = 'No '
            vm.lines[index].rep = 'No '
            this.curency_selected[index] = 1
            this.value[index] = ''
            vm.typeDoc[index].value = 1
        }

       }


    }

    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});
        this.typeDoc.push({id : newid, value: 0})

        //console.log('lineas',this.lines)
        //console.log('seleccionados',this.doc_typeselected)
        //console.log('descripciones',this.description)
        //console.log('archivos',this.files)
        //console.log('typeDoc',this.typeDoc)

    }
    deleteLine(index){
        //console.log('lineas',this.lines)
        //console.log('seleccionados',this.doc_typeselected)
        //console.log('descripciones',this.description)
        //console.log('archivos',this.files)
        //console.log("aaaa", index)
        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
              newvalue++
        })
        this.lines = newlines
        this.doc_typeselected.splice(pos,1)
        this.description.splice(pos,1)
        this.curency_selected.splice(pos,1)
        this.value.splice(pos,1)
        //this.files.splice(pos,1)
        this.typeDoc.splice(pos,1)
        //console.log('lineas',this.lines)
        //console.log('seleccionados',this.doc_typeselected)
        //console.log('descripciones',this.description)
        //console.log('archivos',this.files)
    }

    save (isValid) {
        let filenames = []
        let description2 = []
        let name_des = ""
        let id_type_selected = ""
        
        angular.forEach(this.myFile, function (value) {
              filenames.push(value.name)
        })
        this.filenames = filenames
        this.$state.go(this.$state.current, {}, { alerts: 'test' })

        if (isValid) {
            this.processing = true
            //console.log(this.processing,'this.processing')
            let Policies = this.API.service('claims', this.API.all('claims'))
            let $state = this.$state
            let $stateParams = this.$stateParams
            let id_descriptions = []
            //console.log("Descripciones", this.description)
            angular.forEach(this.doc_typeselected, function (value) {
                //console.log("value ", value)
                if (value.values == "") {
                    value.values = "0"
                }


            })

           /***Verifica los tipos de documentos que no sean facturas***/
            for (var i = 0; i < this.description.length; i++) {
                
                if (this.doc_typeselected[i] > 1 ) {
                    
                   id_type_selected = this.description[i]
                   angular.forEach(this.systemDescriptionDoc, function(value){    
                        if(value.id == id_type_selected){
                                 name_des = value.name         
                        }
                   })
                   this.description[i] = name_des    
                   console.log(this.description)
                    
                    
                }    
            }

          
            //console.log("description", this.description)

            let vm = this
            //console.log("this.value ", this.value)
            //console.log("Descripciones ", this.description)
            //$state.go($state.current, {}, { reload: $state.current})
            Policies.post({
                'policy_id': this.policy_id,
                'id_doc_types' : this.doc_typeselected,
                'descriptions' : this.description,
                'currency_id' : this.currency_selected,
                'values' : this.value,
                'currency_id' : this.curency_selected,
                'filenames' : this.filenames
            }).then(function () {
                    vm.processing = false
                    swal({
                        title: 'Reclamo creado correctamente!',
                        type: 'success',
                        confirmButtonText: 'OK',
                        closeOnConfirm: true
                    }, function () {
                        $state.go($state.current, { alerts: alert,succes: 1})
                    })
                }, function (response) {
                        swal({
                            title: 'Error al guardar el reclamo: ' + response.data.message,
                            type: 'error',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        }, function () {})
                    })
                    this.uploadFile()
                    
        }
        else {
            this.formSubmitted = true
        }
    }


    $onInit(){
    }
}

export const ClaimAddComponent = {
    templateUrl: './views/app/components/claim-add/claim-add.component.html',
    controller: ClaimAddController,
    controllerAs: 'vm',
    bindings: {}
}
