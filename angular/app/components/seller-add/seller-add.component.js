class SellerAddController{
    constructor ($scope, API, $state, $stateParams) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let Companies = this.API.service('companies')
        Companies.getList()
          .then((response) => {
            let systemCompanies = []
            let companiesResponse = response.plain()
            //console.log(countrysResponse)
            angular.forEach(companiesResponse, function (value) {
              systemCompanies.push({id: value.id, name: value.name})
            })

            this.systemCompanies = systemCompanies
            //console.log("systemCountrys==> ", systemCountrys)
        })

        let Agents = this.API.service('agents')
        Agents.getList()
          .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            //console.log(countrysResponse)
            angular.forEach(agentsResponse, function (value) {
              systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
            console.log("systemAgents==> ", systemAgents)
        })


    }

    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this
          let Sellers = this.API.service('sellers', this.API.all('sellers'))
          let $state = this.$state          
          
          Sellers.post({
            'agent_id': this.agentid,
            'company_id': this.companyid,
            'type': this.type,
            'percentage_vn': this.vn_percentage,
            'percentage_rp': this.rp_percentage
          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el vendedor.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const SellerAddComponent = {
    templateUrl: './views/app/components/seller-add/seller-add.component.html',
    controller: SellerAddController,
    controllerAs: 'vm',
    bindings: {}
}
