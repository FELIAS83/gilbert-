class ClaimTicketFilesController{
    constructor($scope,$stateParams,API,$http,$state,$uibModal){
        'ngInject';
        var vm = this
        this.API = API
        this.$http = $http
        this.$state = $state
        let claimId = $stateParams.claimId
        this.claimId = claimId
        let policyId = $stateParams.policyId
        this.policyId = policyId
        this.lines = []
        this.curency_selected = []
        this.curency_selected[0] = 1
        this.value = []
        this.files = []
        this.deletedlines = []
        this.doc_typeselected = []
        this.ndescription = []
        this.directory = '/claims/'
        this.formSubmitted = false
        this.alerts = []
        this.returnfiles = []
        this.processing = false
        this.typeDoc = []
        this.typeDocNew = []
        this.doc_date = []
        this.nprovider_id =[]
        this.$uibModal = $uibModal
        this.description = []
        //this.systemDescriptionDoc = []
        this.psystemDescription = []
        this.systemDescription = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        /*
        let systemDocTypes = []
        systemDocTypes.push({id: "1", name: "Factura"})
        systemDocTypes.push({id: "2", name: "Informe"})
        systemDocTypes.push({id: "3", name: "Orden"})
        systemDocTypes.push({id: "4", name: "Resultado"})
        this.systemDocTypes = systemDocTypes
        console.log("a ", this.systemDocTypes)
        */

         let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            //console.log("Tipos", docResponse)
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })
    
       
        this.systemDocTypes = systemDocTypes
        //console.log(this.systemDocTypes)


        let systemDescriptionDoc = []
        //let systemDescription = []
        //let systemDescription2 = []
        //let systemDescription3 = []
        let DocDescription = this.API.service('claimdocumenttypedescriptions')
        DocDescription.getList()
        .then((response) => {
            let descriptionResponse = response.plain()
            angular.forEach(descriptionResponse, function(value, index, arr){
                
                    systemDescriptionDoc.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})     
                    //systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.description})
                    
                
            })

        })
        this.systemDescriptionDoc = systemDescriptionDoc
        //console.log("this.systemDescriptionDoc",this.systemDescriptionDoc)
        //this.systemDescription = systemDescription
        //this.systemDescription2 = systemDescription2
        //this.systemDescription3 = systemDescription3

        //console.log("Tipo 2",this.systemDescription)
        //console.log("Tipo 3",this.systemDescription2)
        //console.log("Tipo 4",this.systemDescription3)


        let Policies = API.service('show', API.all('policies'))
        Policies.one(policyId).get()
        .then((response) => {
            this.policy = API.copy(response)
            //console.log("r ", this.policy.data)
            this.title = 'Reclamos #'+claimId+' Póliza #'+policyId+' | '+this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
        })

        let Documents = API.service('show', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let typeDoc = []
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
            console.log("prev ", this.previouslines)

            for (let i = 0; i < this.previouslines.length; i++){

                if (this.previouslines[i].amount == 0){
                    this.previouslines[i].amount = ''

                }
                //console.log("Tamaño del archivo", this.previouslines[i].filename.length)
                if ( this.previouslines[i].filename == '' || this.previouslines[i].filename.length <= 1 ){
                    this.previouslines[i].isUpload = true

                }else{
                    this.previouslines[i].isUpload = false
                }

                if (this.previouslines[i].document_date == null) {
                    this.previouslines[i].document_date =  undefined;//new Date(this.applicanteditdata.data[0].birthday)
                }else{
                    this.previouslines[i].document_date = new Date(this.previouslines[i].document_date)
                }
                
                this.returnfiles.push({id: this.previouslines[i].id, return: this.previouslines[i].returned == 1 ? true : false, type: systemDocTypes[this.previouslines[i].id_doc_type - 1].name, description: this.previouslines[i].description , filename: this.previouslines[i].filename, isUpload: this.previouslines[i].isUpload})
               
                typeDoc.push({id: i, value: this.previouslines[i].id_doc_type})

                  // seteo de opcion en informe y resultado
                   let description = this.previouslines[i].description 
                let systemDescription = []
                let id_doc_type = this.previouslines[i].id_doc_type
                angular.forEach(this.systemDescriptionDoc, function(value){              
                    if(value.id_doc_type == id_doc_type){                    
                       systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 

                       if(description == value.name){
                            description = value.id
                       }
                 }

                })       
                this.previouslines[i].description = description
                this.psystemDescription[i] = systemDescription
                console.log("this.previouslines[i].description",this.previouslines[i].description)
                  /* if (this.previouslines[i].id_doc_type == 2 ) {

                        
                        for (let j = 0; j < this.systemDescription.length; j++) {
                            if(this.previouslines[i].description == this.systemDescription[j].name){
                                this.previouslines[i].description = this.systemDescription[j].id       
                            }                             
                        }       
                } 
                else if (this.previouslines[i].id_doc_type == 3 ) {
                        
                        for (let j = 0; j < this.systemDescription2.length; j++) {
                             
                            if(this.previouslines[i].description == this.systemDescription2[j].name){
                                this.previouslines[i].description = this.systemDescription2[j].id       
                            } 
                            
                        }
                }    
                else if (this.previouslines[i].id_doc_type == 4 ) {
                        
                        for (let j = 0; j < this.systemDescription3.length; j++) {
                             //console.log("entro des", this.previouslines[i].description)
                             //console.log("entro compa", this.systemDescription3[j])
                            if(this.previouslines[i].description == this.systemDescription3[j].name){
                                this.previouslines[i].description = this.systemDescription3[j].id       
                            } 
                        } 
                }     */
             
        }
               for (let i = 0; i < this.previouslines.length; i++){

                if(this.previouslines[i].documents !== null){
                
                let Dispatched = API.service('dispatcheddoc', API.all('claims'))
                Dispatched.post({
                 'diagnostic_id' : this.previouslines[i].documents.diagnostic_id,
                })
                .then((response) => {
                    let data = API.copy(response)
                    this.dispatched = data.data
                    console.log("despachados", this.dispatched.dispatched)
                    if( this.dispatched.dispatched == '1'){
                               
                        this.previouslines[i].dispatched = 1      
                     
                    }else{
                        this.previouslines[i].dispatched = 0         
                    }


                    })

                }else{
                    console.log("Aqui")
                    this.previouslines[i].dispatched = 0      
                }
            }
            console.log("previuslINES ", this.previouslines)
            
            this.typeDoc = typeDoc

            if (sessionStorage.getItem('info')){
                console.log("entro aquiiiiiiiiiiiiiiiii")
            let info = JSON.parse(sessionStorage.getItem('info'))
            
            this.policy_id = info.policy_id
            this.claimId = info.claimId
            this.previouslines[0].currency_id = info.currency_id
            this.previouslines[0].id = info.id
            this.previouslines[0].document_date = new Date(info.document_date)
            this.previouslines[0].description = info.description
            this.previouslines[0].isUpload = info.isUpload         
            this.previouslines[0].id_doc_type = info.id_doc_type
            this.previouslines[0].provider_id = info.provider_id
            this.previouslines[0].amount= info.amount

            this.curency_selected = info.curency_selected
            for (var i=0; i<this.curency_selected.length;i++){
                this.curency_selected[i] = parseInt(this.curency_selected[i], 10)
                //console.log("tipo de variable",typeof(this.curency_selected[i]))
            }
            this.nprovider_id= info.nprovider_id
            for (var i=0; i<this.nprovider_id.length;i++){
                this.nprovider_id[i] = parseInt(this.nprovider_id[i], 10)
                //console.log("tipo de variable",typeof(this.provider[i]))
            }
            
            this.doc_date =info.doc_date
            for (var i=0; i<this.doc_date.length;i++){
                this.doc_date[i] = new Date (this.doc_date[i])
                console.log("tipo de variable",typeof(this.doc_date[i]))
            }
            
            this.ndescription=info.ndescription
            for (var i=0; i<this.ndescription.length;i++){
                if (this.doc_typeselected[i] != 1  ) {
                this.ndescription[i] =  parseInt(this.ndescription[i], 10)
                //console.log("tipo de variable",typeof(this.description[i]))
                }
            }
            this.doc_typeselected =info.doc_typeselected
            for (var i=0; i<this.doc_typeselected.length;i++){
                this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10)
                //console.log("tipo de variable",typeof(this.doc_typeselected[i]))
            }
            /*this.myfile = info.myfile
            for (var i=0; i<this.myFile.length;i++){
                this.myFile[i] = new File(this.myFile[i])
                //console.log("tipo de variable",typeof(this.doc_typeselected[i]))
            }*/
            //this.noPending(this.id)
            this.arr = info.arr
            this.typeDoc = info.typeDoc
            this.type= info.type
            this.typeDocNew= info.typeDocNew
            this.pmyFile = info.pmyFile
            this.files = info.files
            this.value = info.value
            
            this.filename= info.filename
            this.dispatched= info.dispatched
            this.deletedlines= info.deletedlines
            this.returnfiles=info.returnfiles
            this.lines = info.lines
            this.index=info.index
            sessionStorage.removeItem('info')
            console.log('info',info)
        }

        })

     
        

        let Currencies = API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, iso: value.iso})
            })

            this.systemCurrency = systemCurrency
            console.log("monedas ", this.systemCurrency)
        })

        let Providers = API.service('providers')
        Providers.getList()
        .then((response) => {
            let systemProviders = []
            let providersResponse = response.plain()
            angular.forEach(providersResponse, function (value) {
                systemProviders.push({id: value.id, name: value.name})
            })

            this.systemProviders = systemProviders
            console.log("systemProviders ", this.systemProviders)
        })
    }

    changeDocType(index,type){
        this.indice = index
        let systemDescription = []
        let id_doc_select = this.doc_typeselected[index]
        if (type == 1){
            this.typeDocNew[index].value = this.doc_typeselected[index]
            if (this.doc_typeselected[index] == '1') {
                this.lines[index].disabled = false
                this.lines[index].rep = ''
                this.curency_selected[index] = 1
                this.ndescription[index] = ''
            }
            else {
                angular.forEach(this.systemDescriptionDoc, function(value){
                    if(value.id_doc_type == id_doc_select){
                        systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 
                    }

                    })
                console.log("systemDescription", systemDescription)
                this.systemDescription[index] = systemDescription
                this.lines[index].disabled = true
                this.lines[index].rep = 'No '
                this.curency_selected[index] = 1
                this.value[index] = ''
            }
        }
        else{//anteriores
            let id_doc_select = this.previouslines[index].id_doc_type
            this.typeDoc[index].value = this.previouslines[index].id_doc_type
            if (this.previouslines[index].id_doc_type == '1') {
                this.previouslines[index].disabled = false
                this.previouslines[index].rep = ''
                this.previouslines[index].currency_id = 1
                this.previouslines[index].description = ''
            }else {

                angular.forEach(this.systemDescriptionDoc, function(value){
                    if(value.id_doc_type == id_doc_select){
                        systemDescription.push({id: value.id, id_doc_type: value.id_doc_type, name: value.name}) 
                    }
                    })
                this.psystemDescription[index] = systemDescription
                this.previouslines[index].disabled = true
                this.previouslines[index].rep = 'No '
                this.previouslines[index].currency_id = 1
                this.previouslines[index].amount = ''
            }
        }

    }

    addFile() {
        if (this.lines.length==0){
            var newid = 0
        }
        else{
            var newid = this.lines.length
        }
        this.lines.push({id : newid});
        this.typeDocNew.push({id : newid, value: 1})
    }

    deletePreviousLine(index){//eliminar de la lista de archivos iniciales
        console.log("Lienas", this.previouslines)
        this.deletedlines.push({id : index.id})
        let pos = this.previouslines.indexOf(index);
        this.previouslines.splice(pos,1)
    }

    deleteLine(index){//eliminar de los archivos nuevos agregados
        console.log('antes')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('ndescription',this.ndescription)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)


        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        console.log('pos',pos)
        this.doc_typeselected.splice(pos,1)
        this.ndescription.splice(pos,1)
        this.curency_selected.splice(pos,1)
        this.nprovider_id.splice(pos,1)
        this.value.splice(pos,1)
        this.files.splice(pos,1)
        this.doc_date.splice(pos,1)

        for(var i=0; i<this.doc_typeselected.length;i++) this.doc_typeselected[i] = parseInt(this.doc_typeselected[i], 10);

        let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines

        console.log('despues')
        console.log('lines',this.lines)
        console.log('doc_typeselected',this.doc_typeselected)
        console.log('description',this.description)
        console.log('curency_selected',this.curency_selected)
        console.log('value',this.value)
        console.log('files',this.files)
    }

    save(isValid){
        let name_des = ""
        let id_type_selected = ""

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        console.log(isValid)

        if (this.pfiles){
        for (var i = 0; i < this.previouslines.length; i++) {
            if (this.pfiles[i]) {
                this.previouslines[i].filename = this.pfiles[i][0].name
            }
        }}

        if (isValid){
            this.processing = true
            let $state = this.$state
            let uploadFile = this.uploadFile
            let filenames = []
            angular.forEach(this.myFile, function (value) {
                filenames.push(value.name)
            })
            this.filenames = filenames
            var vm = this
            
            for (var i = 0; i < this.previouslines.length; i++) {
                
                if (this.previouslines[i].id_doc_type > 1 ) {
                   id_type_selected = this.previouslines[i].description
                   angular.forEach(this.systemDescriptionDoc, function(value){    
                        if(value.id == id_type_selected){
                                 name_des = value.name         
                        }
                   })
                   this.previouslines[i].description = name_des     
                }
            }

            console.log("Tipo de docu new", this.typeDocNew)    
            console.log("description nuevas", this.description)   
            console.log("this.ndescription[i]-1",this.ndescription) 
            for (var i = 0; i < this.description.length; i++) {
                 if (this.doc_typeselected[i] > 1 ) {
                   id_type_selected = this.description[i]
                   angular.forEach(this.systemDescriptionDoc, function(value){    
                        if(value.id == id_type_selected){
                                 name_des = value.name         
                        }
                   })
                   this.ndescription[i] = name_des    
                   console.log(this.ndescription)
               
                }    
            } 
             console.log("this.doc_typeselected",this.doc_typeselected)
             console.log("this.curency_selected",this.curency_selected)
             console.log("this.nprovider_id",this.nprovider_id)
             console.log(" this.value",this.value)
            console.log("ndescripciones", this.ndescription) 
            console.log("filenames", this.filenames)  
            console.log("descripciones viejos", this.previouslines)

            console.log("deletedlines", this.deletedlines)
            console.log("returnfiles", this.returnfiles)
            console.log("doc_date", this.doc_date)

            console.log("descripciones nuevos", this.description) 
            
            
            console.log("this.previouslines ", this.previouslines)
            let documents = this.API.service('claims', this.API.all('claims'))
            documents.post({
            'id_claim' : this.claimId,
            'id_doc_types' : this.doc_typeselected,
            'currency_id' : this.curency_selected,
            'provider_id' : this.nprovider_id,
            'values' : this.value,
            'descriptions' : this.ndescription,
            'filenames' : this.filenames,
            'modified' : this.previouslines,
            'deleted_ids' : this.deletedlines,
            'returned' : this.returnfiles,
            'document_date' : this.doc_date
          }).then(function (response) {
                vm.uploadFile()
          }, function (response) {
          })
        }else{
            this.formSubmitted = true
        }
    }

    uploadFile(){
        let $state = this.$state
        var i = 0
            var files = this.files//archivos nuevos
            var pfiles = this.pfiles//archivos que sustituiran a los anteriores
            var claimId = this.claimId
            var $http = this.$http
            angular.forEach(files, function(files){
                var form_data = new FormData();
                angular.forEach(files, function(file){
                    form_data.append('file', file);
                })
                form_data.append('claim_id', claimId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){   })
            })
            angular.forEach(pfiles, function(pfiles){
                var form_data = new FormData();
                angular.forEach(pfiles, function(pfile){
                    form_data.append('file', pfile);
                })
                form_data.append('claim_id', claimId);
                $http.post('uploadclaims', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){     })
            })
        this.processing = false
        swal({
            title: 'Documentos Guardados Correctamente!',
            type: 'success',
            confirmButtonText: 'OK',
            closeOnConfirm: true
        }, function () {
            $state.go('app.claim-claim-lists', {})
        })
    }    

    retufiles(){
        let $state = this.$state
        let sendmail = false
        angular.forEach(this.returnfiles, function(value){
            if (value.return == true){
                sendmail = true
            }
        })
        console.log("a ", sendmail)
        if (sendmail){
            this.API.service('send').post({
            format_id: 8,
            policy_id: this.policyId,
            claim_id : this.claimId,
            returnfiles: this.returnfiles
        } )
        .then( ( response ) => {
            swal({
                title: 'Documentos Devueltos Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {
                $state.reload();
            })
        }, function (response) {
            swal({
                title: 'Error al guardar documentos' + response.message,
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {

            })
        })
        }
        else{
            console.log('debe seleccionar')
        }
        console.log('debe enviar')
        this.sendmail = sendmail
    }
    checkSelected(){
        let sendmail = false
        angular.forEach(this.returnfiles, function(value){
            if (value.return == true){
                sendmail = true
            }
        })
        this.sendmail = sendmail
    }

    noPending(id){
        let $state = this.$state
        let pending = this.API.service('pending', this.API.all('claims'))
            pending.post({
            'id' : id
          }).then(function (response) {
                $state.reload()
          }, function (response) {
          })
    }


    modalProvider(size) {
        let info = {}
        info.policy_id = this.policy_id
        info.claimId = this.claimId
        
        info.id = this.previouslines[0].id
        info.provider_id = this.previouslines[0].provider_id
        info.document_date = this.previouslines[0].document_date
        info.description = this.previouslines[0].description
        info.id_doc_type = this.previouslines[0].id_doc_type
        info.currency_id = this.previouslines[0].currency_id
        info.doc_typeselected = this.doc_typeselected
        info.dispatched =this.previouslines[0].dispatched
        info.filename = this.previouslines[0].filename
        info.amount = this.previouslines[0].amount
        
        info.lines = this.lines
        info.curency_selected = this.curency_selected
        info.doc_date = this.doc_date
        info.pmyfile = this.pmyfile
        info.myFile = this.myFile
        info.files = this.files    
        info.typeDocNew=this.typeDocNew
        info.value = this.value
        info.type= this.type
        info.arr =this.arr
        info.ndescription= this.ndescription        
        info.deletedlines = this.deletedlines
        info.returnfiles=this.returnfiles
        info.nprovider_id=this.nprovider_id
        info.index = this.index
        info.isUpload = this.isUpload
        sessionStorage.setItem('info',JSON.stringify(info))

      let $uibModal = this.$uibModal
      var modalInstance = $uibModal.open({
          templateUrl: 'myModalProvider',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size
      })
    }
    modalcontroller (API, $uibModalInstance, $state) {
        'ngInject'

        var mvm = this
        this.$state = $state


        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function (isValid) {
            if (isValid) {
                this.processing = true
                let providers = API.service('providers', API.all('providers'))
                let $state = this.$state
                let cancel = this.cancel
                providers.post({
                    'name': mvm.name,
                    'description': mvm.description
                }).then(function () {
                    cancel() 
                     $state.reload()
                   }, function (response) {})

            } else {
                this.formSubmitted = true
            }
        }
    }

    $onInit(){
    }
}

export const ClaimTicketFilesComponent = {
    templateUrl: './views/app/components/claim-ticket-files/claim-ticket-files.component.html',
    controller: ClaimTicketFilesController,
    controllerAs: 'vm',
    bindings: {}
}
