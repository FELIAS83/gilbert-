class ProviderAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
    }

    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this
          let Providers = this.API.service('providers', this.API.all('providers'))
          let $state = this.$state          
          
          Providers.post({
            'name': this.name
            }).then(function () {
                vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el Proveedor de Servicio.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }
    
    $onInit(){
    }
}

export const ProviderAddComponent = {
    templateUrl: './views/app/components/provider-add/provider-add.component.html',
    controller: ProviderAddController,
    controllerAs: 'vm',
    bindings: {}
}
