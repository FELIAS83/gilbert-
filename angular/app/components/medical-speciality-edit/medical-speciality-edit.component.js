class MedicalSpecialityEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let specialityId = $stateParams.specialityId
        //console.log(companyId)
        let specialityData = API.service('show', API.all('medicalspecialties'))
        specialityData.one(specialityId).get()
          .then((response) => {
            this.specialityeditdata = API.copy(response)
        })
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.specialityeditdata);
          let $state = this.$state
          this.specialityeditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado la especialidad.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const MedicalSpecialityEditComponent = {
    templateUrl: './views/app/components/medical-speciality-edit/medical-speciality-edit.component.html',
    controller: MedicalSpecialityEditController,
    controllerAs: 'vm',
    bindings: {}
}
