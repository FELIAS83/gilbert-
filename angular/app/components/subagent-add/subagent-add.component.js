class SubagentAddController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';
        

        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.getState = getState
        vm.getCity = getCity
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

         let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()
            console.log(countrysResponse)
            angular.forEach(countrysResponse, function (value) {
              systemCountrys.push({id: value.id, name: value.name})
            })

            this.systemCountrys = systemCountrys
            console.log("systemCountrys==> ", systemCountrys)
        })

        function getState(){
            console.log("Entro..", vm.country)
            let States = API.service('index', API.all('states'))
            States.one(vm.country).get()
                .then((response) => {
                    let systemStates = []
                    let stateResponse = response.plain()
                     console.log(stateResponse)
                    angular.forEach(stateResponse.data.states, function (value) {
                        systemStates.push({id: value.id, name: value.name})
                    })
                this.systemStates = systemStates
                console.log("systemStates  ==> ", systemStates)          

                })

        }

         function getCity(){
            let Citys = API.service('index', API.all('cities'))
            Citys.one(vm.state).get()
                .then((response) => {
                    let systemCities = []
                    let cityResponse = response.plain()
                     console.log(cityResponse)
                    angular.forEach(cityResponse.data.cities, function (value) {
                        systemCities.push({id: value.id, name: value.name})
                    })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", systemCities)          

                })

        }    

        //
    }//fin de constructor


     save (isValid) {  
        console.log("first_name",this.name)
        console.log("last_name",this.last_name)
        console.log("identity_document",this.identity)
        console.log("birthday",this.birthday)
        console.log("email",this.email)
        console.log("skype",this.skype)
        console.log("mobile",this.mobile)
        console.log("phone",this.phone)
        console.log("country_id",this.country)
        console.log("province_id",this.state)
        console.log("city_id",this.city)
        console.log("address",this.address)
        console.log("commission",this.commission)

        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let vm = this
          let Subagents = this.API.service('subagents', this.API.all('subagents'))
          let $state = this.$state                    
          Subagents.post({
            'first_name': this.name,
            'last_name' : this.last_name,
            'identity_document' : this.identity,
            'birthday' : this.birthday,
            'email' : this.email,
            'skype' : this.skype,
            'mobile' : this.mobile,
            'phone' : this.phone,
            'country_id' : this.country,
            'province_id' : this.state,
            'city_id' : this.city,
            'address' : this.address,
            'commission' : this.commission
          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el Sub-agente.' }
            $state.go($state.current, { alerts: alert})
            console.log("MENSAJE 2", $state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
            console.log("Error", $state.current);
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const SubagentAddComponent = {
    templateUrl: './views/app/components/subagent-add/subagent-add.component.html',
    controller: SubagentAddController,
    controllerAs: 'vm',
    bindings: {}
}
