class AgentCommissionListController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        this.API = API
        var vm = this
        this.$state = $state
        this.can = AclService.can

        let agentId = $stateParams.agentId
        this.agentId =  agentId
        console.log("agentId", agentId)

        let AgentData = API.service('show', API.all('agents'))
        AgentData.one(agentId).get()
        .then((response) => {
             this.dataagent = response.plain()

        })  


        let Commission = API.service('show',API.all('commissions'))
        Commission.one(agentId).get()
            .then((response) => {
                let dataSet = response.plain()
                console.log(dataSet)


              this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('provider').withTitle('Proveedor'),
              DTColumnBuilder.newColumn('branch').withTitle('Ramo'),
              DTColumnBuilder.newColumn('plan').withTitle('Plan'),              
              DTColumnBuilder.newColumn('commission').withTitle('Comisión'),
              DTColumnBuilder.newColumn('commission_renewal').withTitle('Comisión para Renovación'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
            .renderWith(actionsHtml)
            ]

            this.displayTable = true

            })
        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        let actionsHtml = (data) =>  {
          return`<ul>
                     <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.agent-commission-edit({commissionId: ${data.id}, agentId: ${data.agent_id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button ng-show="vm.can('manage.sales')" title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }

    delete (id) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminaran las comisiones!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('commissions').one('commission', id).remove()
          .then((response) => {
            let data = response.plain()
            let valid = data.data
            console.log(data)
            
            if(valid)
            {
                swal({
                  title: 'Eliminado!',
                  type: 'success',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true
                  }, function () {
                    $state.reload()
                  })
            }else
            {
              swal({
                  title: 'Importante!',
                  text: 'Aeegúrese de Eliminar primero las comisiones de los agentes hijos !',
                  type: 'error',
                  confirmButtonText: 'OK',
                  closeOnConfirm: true,
                  timer: 3000
                  }, function () {
                    $state.go($state.current, {})
                  })

            }

          })
      })
    }

    $onInit(){
    }
}

export const AgentCommissionListComponent = {
    templateUrl: './views/app/components/agent-commission-list/agent-commission-list.component.html',
    controller: AgentCommissionListController,
    controllerAs: 'vm',
    bindings: {}
}
