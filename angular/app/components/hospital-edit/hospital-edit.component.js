class HospitalEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.specialties = []
        this.API = API
        //vm.getState = getState
        //vm.getCity = getCity
        vm.countryRequired = false
        vm.provinceRequired = false
        vm.cityRequired = false
        this.processing = false


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let hospitalId = $stateParams.hospitalId

        let hospitalData = API.service('show', API.all('hospitals'))//ojo
        hospitalData.one(hospitalId).get()
        .then((response) => {
            this.hospitaleditdata = API.copy(response)
            console.log(this.hospitaleditdata.data)
            this.modelcountry=this.hospitaleditdata.data.country_id
            this.modelstate =this.hospitaleditdata.data.province_id
            //this.modelcity =this.hospitaleditdata.data.city_id
            console.log("pAIS:",this.modelcountry,"Estado",this.modelstate)
                //,"ciudad",this.modelcity)

         

        
     /*    let States = API.service('province', API.all('states'))
        States.one().get()
          .then((response) => {
            console.log("response_province",response)
            let systemStates = []
            let allStates = []
            let stateResponse = response.plain()
            angular.forEach(stateResponse.data.states, function (value) {
              if(value.country_id == this.modelcountry){
                systemStates.push({id: value.id, name: value.name, country_id: value.country_id})
              }
              allStates.push({id: value.id, name: value.name, country_id: value.country_id})
            })
            this.systemStates = systemStates
            this.allStates = allStates
            console.log("systemStates==> ", systemStates)


            })*/


        let specialties = API.all('medicalspecialties')
        specialties.getList()
        .then((response) => {
            let specialtieslist = []
            let specialties = response.plain()

            angular.forEach(specialties, function (value) {
                specialtieslist.push({id: value.id, name: value.name})
            })
            this.specialties = specialtieslist
        })

      

        //console.log(this.hospitaleditdata.data.country_id)
        function getState(){
            console.log("Entro...", vm.modelcountry)
            vm.countryRequired = true
            let States = API.service('index', API.all('states'))
            States.one(this.modelcountry).get()
            .then((response) => {
                let systemStates = []
                let stateResponse = response.plain()
                //console.log(stateResponse)
                angular.forEach(stateResponse.data.states, function (value) {
                    systemStates.push({id: value.id, name: value.name})
                })
                this.systemStates = systemStates
                console.log("Estado", this.systemStates)
            })
        }
        /*function getCity(){
            vm.provinceRequired = true
            vm.cityRequired = true
            let Citys = API.service('city', API.all('cities'))
            Citys.one(this.modelstate).get()
            .then((response) => {
                let systemCities = []
                let cityResponse = response.plain()
                //console.log(cityResponse)
                angular.forEach(cityResponse.data.cities, function (value) {
                    systemCities.push({id: value.id, name: value.name})
                })
                this.systemCities = systemCities
                console.log("systemCities  ==> ", systemCities)
            })
        }*/

        let Countrys = this.API.service('countrys')
        Countrys.getList()
          .then((response) => {
            let systemCountrys = []
            let countrysResponse = response.plain()

            vm.countries = countrysResponse
            console.log(vm.countries)


            angular.forEach(countrysResponse, function (value)
                            {
            console.log("dat del pais", vm.hospitaleditdata.data.country_id)
              if(value.id == vm.hospitaleditdata.data.country_id){//si es el pais seleccionado
                //vm.country_id = value.id
                let States = API.service('index', API.all('states'))
                States.one(value.id).get()
                    .then((response) =>{
                        let systemStates = []
                        let stateResponse = response.plain()
                        vm.estados = stateResponse
                        //console.log(stateResponse)
                        angular.forEach(stateResponse.data.states, function (value){

                            if(value.id == vm.hospitaleditdata.data.province_id){
                                //vm.province_id = value.id
                                let Citys = API.service('index', API.all('cities'))
                                Citys.one(value.id).get()
                                    .then((response) => {
                                        let systemCities = []
                                        let cityResponse = response.plain()
                                        angular.forEach(cityResponse.data.cities, function (value){
                                            systemCities.push({id: value.id, name: value.name})
                                        })
                                        vm.systemCities = systemCities
                                    //console.log("systemCities", systemCities)
                                    })

                            }

                        systemStates.push({id: value.id, name: value.name})


                        })
                        vm.systemStates = systemStates
                        //console.log("systemStates", vm.systemStates)

                    })

              }
              systemCountrys.push({id: value.id, name: value.name})

            })

            vm.systemCountrys = systemCountrys
            //vm.country = {type : vm.systemCountrys[0].value};
            //console.log("systemcountries",vm.systemCountrys)
        })

        }) 
    }

    save (isValid) {
        this.hospitaleditdata.data.country_id = this.modelcountry
        this.hospitaleditdata.data.province_id = this.modelstate
        //this.hospitaleditdata.data.city_id = this.modelcity
        if (isValid) {
            this.processing = true
            let vm = this
          console.log(this.hospitaleditdata);
          let $state = this.$state
          this.hospitaleditdata.put()
            .then(() => {
                vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el hospital.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const HospitalEditComponent = {
    templateUrl: './views/app/components/hospital-edit/hospital-edit.component.html',
    controller: HospitalEditController,
    controllerAs: 'vm',
    bindings: {}
}
