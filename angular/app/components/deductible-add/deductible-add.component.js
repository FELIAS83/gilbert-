class DeductibleAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        //
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let planId = $stateParams.planId

        console.log("id del plan", planId)
        let PlanData = API.service('show', API.all('plans'))
        PlanData.one(planId).get()
          .then((response) => {
            this.planeditdata = API.copy(response)
            console.log("data del plan", this.planeditdata.data)
            let planResponse = response.plain(response)
        
      })
    }// constructor
     save (isValid) {    
         //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
            this.processing = true
            let vm = this
         
          let Deductibles = this.API.service('deductibles', this.API.all('deductibles'))
          let $state = this.$state          
          
          Deductibles.post({
            'name': this.name,
            'amount_in_usa': this.amount_in_usa,
            'amount_out_usa': this.amount_out_usa,
            'plan_id': this.planeditdata.data.id

          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el deducible.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const DeductibleAddComponent = {
    templateUrl: './views/app/components/deductible-add/deductible-add.component.html',
    controller: DeductibleAddController,
    controllerAs: 'vm',
    bindings: {}
}
