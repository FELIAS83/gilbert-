class BranchAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.processing = false
        
        let companyId = $stateParams.companyId
        this.companyId =  companyId
        console.log(this.companyId)
          

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
         let Company = this.API.service('show', this.API.all('companies'))
        Company.one(companyId).get()
          .then((response) => {
            this.companydata = response.plain()
            console.log('companydata',  this.companydata)

          })

       
        //
    }

     save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        

        if (isValid) { 
          
            if(this.commission_percentage == "" || this.commission_percentage == undefined || this.commission_percentage == 0){     
                this.commission_percentage = parseFloat(this.companydata.data.commission_percentage)
            }
            
            this.processing = true
            let vm = this 
            let Branchs = this.API.service('branchs', this.API.all('branchs'))
            let $state = this.$state          
          
             Branchs.post({
                'companyId': vm.companyId,
                'commission_percentage': vm.commission_percentage,
                'name': vm.name
            }).then(function () {
                vm.processing = false
                console.log("mensaje", $state.current);
                let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el Ramo.' }
                $state.go($state.current, { alerts: alert})
                console.log($state.current);
            }, function (response) {
                let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
                $state.go($state.current, { alerts: alert})
            })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const BranchAddComponent = {
    templateUrl: './views/app/components/branch_add/branch_add.component.html',
    controller: BranchAddController,
    controllerAs: 'vm',
    bindings: {}
}
