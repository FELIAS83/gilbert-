class SellerEditController{
    constructor ($scope, $stateParams, $state, API) {
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let sellerId = $stateParams.sellerId
        //console.log(companyId)
        let SellerData = API.service('show', API.all('sellers'))
        SellerData.one(sellerId).get()
          .then((response) => {
            this.sellereditdata = API.copy(response)
            console.log("data=>", this.sellereditdata.data);
            this.sellereditdata.data.percentage_vn = parseFloat(this.sellereditdata.data.percentage_vn, 2)
            this.sellereditdata.data.percentage_rp = parseFloat(this.sellereditdata.data.percentage_rp, 2)
            
        })

        let Companies = this.API.service('companies')
        Companies.getList()
          .then((response) => {
            let systemCompanies = []
            let companiesResponse = response.plain()
            //console.log(countrysResponse)
            angular.forEach(companiesResponse, function (value) {
              systemCompanies.push({id: value.id, name: value.name})
            })

            this.systemCompanies = systemCompanies
            //console.log("systemCountrys==> ", systemCountrys)
        })

        let Agents = this.API.service('agents')
        Agents.getList()
          .then((response) => {
            let systemAgents = []
            let agentsResponse = response.plain()
            //console.log(countrysResponse)
            angular.forEach(agentsResponse, function (value) {
              systemAgents.push({id: value.id, first_name: value.first_name, last_name: value.last_name, identity_document: value.identity_document})
            })

            this.systemAgents = systemAgents
            console.log("systemAgents==> ", systemAgents)
        })
    }

    save (isValid) {
        if (isValid) {
          this.sellereditdata.data.company_id = this.companyid
          console.log("se va ", this.sellereditdata);
          let $state = this.$state
          this.sellereditdata.put()
            .then(() => {
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el vendedor.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
      }

    $onInit(){
    }
}

export const SellerEditComponent = {
    templateUrl: './views/app/components/seller-edit/seller-edit.component.html',
    controller: SellerEditController,
    controllerAs: 'vm',
    bindings: {}
}
