class AgentSublistsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        this.API = API
        this.$state = $state
        this.can = AclService.can

         if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }
        let agentId = $stateParams.agentId
        console.log(agentId)
        


        let Sub_agents = API.service('show', API.all('agentsubs'))
        Sub_agents.one(agentId).get()
                .then((response) =>{
                    let systemSubagents = []
                    let subagentsResponse = response.plain()
                    console.log("Sub agentes completos;", subagentsResponse)
                    angular.foreach(subagentsResponse.data.subagents, function (value) {
                        systemSubagents.push({id: value.id, first_name: value.first_name, last_name: last_name})

                    })
                    this.systemSubagents = systemSubagents

                })


          let AgentData = API.service('show', API.all('agents'))
          AgentData.one(agentId).get()
          .then((response) => {
            this.agentdata = API.copy(response)
            console.log("Agente", this.agentdata)
            let agentResponse = response.plain(response)
            //this.agenteditdata.data.commission = parseFloat(this.agenteditdata.data.commission, 2)
            //this.agenteditdata.data.birthday =  new Date(this.agenteditdata.data.birthday)
            
        
      })





        let Subagents = API.service('index', API.all('agentsubs'))
        Subagents.one(agentId).get({})
            .then((response) => {

            let dataSet = response.plain()
            console.log(dataSet)
            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data.subagents)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()


            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('first_name').withTitle('Nombres'),
              DTColumnBuilder.newColumn('last_name').withTitle('Apellidos'),
              DTColumnBuilder.newColumn('identity_document').withTitle('N° de Identificación'),
              DTColumnBuilder.newColumn('mobile').withTitle('Movil'),              
              DTColumnBuilder.newColumn('phone').withTitle('Telefono'),              
              DTColumnBuilder.newColumn('email').withTitle('E-mail'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

            let createdRow = (row) => {
                $compile(angular.element(row).contents())($scope)
            }

            let actionsHtml = (data) =>  {
                return`<ul>

                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
            }






        //
    }// constructor

     delete (subagentId) {
        let API = this.API
        let $state = this.$state

        swal({
            title: 'Está seguro?',
            text: 'Se eliminará el Sub-Agente!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Sí, elimínalo!',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: false
        }, function () {
            API.one('agentsubs').one('agentsub', subagentId).remove()
            .then(() => {
                swal({
                title: 'Eliminado!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
            }, function () {
                $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const AgentSublistsComponent = {
    templateUrl: './views/app/components/agent-sublists/agent-sublists.component.html',
    controller: AgentSublistsController,
    controllerAs: 'vm',
    bindings: {}
}
