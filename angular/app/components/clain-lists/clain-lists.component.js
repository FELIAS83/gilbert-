class ClainListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';

        this.API = API
        this.$state = $state
console.log("x")
        let Policies = this.API.service('claims')
        
        Policies.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('created_at').withTitle('Creación del Trámite'),
              DTColumnBuilder.newColumn('created_at').withTitle('Vigencia del Trámite'),
              //DTColumnBuilder.newColumn('status').withTitle('Estado'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })

        let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }

        let actionsHtml = (data) => {
          return `<ul>
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Info. Trámite" class="btn btn-xs btn-primary" ui-sref="app.policy-view({policyId: ${data.id}})">
                      <i class="fa fa-tag"></i>
                    </a>                    
                    &nbsp
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Enviar Póliza al Cliente" class="btn btn-xs btn-primary" style="background-color: #ddd"  ng-disabled="true">
                        <i class="fa fa-paper-plane"></i>
                    </a>                    
                  </ul>`
        }
    }

    $onInit(){
    }
}

export const ClainListsComponent = {
    templateUrl: './views/app/components/clain-lists/clain-lists.component.html',
    controller: ClainListsController,
    controllerAs: 'vm',
    bindings: {}
}
