class HistoricalClaimsReportController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';

         this.API = API
        this.$state = $state
                    
       
       let Historical = API.service('claimhistorical', API.all('reports'))
        Historical.getList({})
          .then((response) => {
            let dataSet = response.plain()
            console.log("Polizas", dataSet)
             this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              //.withOption('dataDoc', dataSet.eob)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
              .withOption('autoWidth', false)
              .withOption('stateSave', true)
              .withOption('stateSaveCallback', function(settings,data) {
                sessionStorage.setItem('HistoricalEmissionsLists', JSON.stringify(data));
               })
              .withOption('stateLoadCallback', function(settings) {
                return JSON.parse(sessionStorage.getItem('HistoricalEmissionsLists' ))
               })
            //.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('# Póliza'),
              DTColumnBuilder.newColumn('titular').withTitle('Titular'),
              DTColumnBuilder.newColumn('name').withTitle('Asegurado'),
              DTColumnBuilder.newColumn('description').withTitle('Descripción'),
              DTColumnBuilder.newColumn('created_at').withTitle('Fecha de Creación'),
              DTColumnBuilder.newColumn('resolution').withTitle('Resolución'),
              DTColumnBuilder.newColumn('eob[]').withTitle('Archivos EOB').renderWith(actionsHtml)
             // DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
             //   .renderWith(actionsHtml)
            ]

            this.displayTable = true
        
        })
           let createdRow = (row) => {
            $compile(angular.element(row).contents())($scope)
        }
         let actionsHtml = (data) => {
          var file = ""
          if(data.length > 0){
            file = "<ul>"
          for(var i = 0; i < data.length; i++){     
              //console.log(data[i].directory)
              file +=`
                    <a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px"  target="_blank" title="Solicitud" class="btn btn-xs btn-primary" ng-href="${data[i].directory}/${data[i].filename}")">
                      <i class="fa fa-file"></i>
                    </a>`
            } 
            file += "</ul>"

            //console.log(file) 
            return file           
          }else{
             file = `<ul>
                    
                      <i class="fa fa-eye-slash" style="color: #000000;width:24.2px;height:21.98px"></i>
                    
                  </ul>`
             return file     
          }
        }
        //
    }

    $onInit(){
    }
}

export const HistoricalClaimsReportComponent = {
    templateUrl: './views/app/components/historical-claims-report/historical-claims-report.component.html',
    controller: HistoricalClaimsReportController,
    controllerAs: 'vm',
    bindings: {}
}
