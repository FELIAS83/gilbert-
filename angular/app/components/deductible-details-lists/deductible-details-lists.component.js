class DeductibleDetailsListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, AclService){
        'ngInject';

        this.API = API
        this.$state = $state
        this.can = AclService.can


        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleId = $stateParams.deductibleId

        let Deductible = API.service('details', API.all('deductible_details'))
        Deductible.one(deductibleId).get()
          .then((response) => {

            let dataSet = response.plain()
            //console.log("data", dataSet.data)

            let DeductibleData = API.service('show', API.all('deductibles'))
            DeductibleData.one(deductibleId).get()
            .then((response) => {
                this.deductibleeditdata = API.copy(response)
                console.log("Data de deducible", this.deductibleeditdata.data)

                let PlanData = API.service('show', API.all('plans'))
                PlanData.one(this.deductibleeditdata.data.plan_id).get()
                .then((response) => {
                    this.planeditdata = API.copy(response)
                    console.log("data del plan", this.planeditdata.data)
                    
        
             })
        
            })


            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withOption('paging', false)
              .withOption('searching', false)
              .withOption('info', false)
              .withBootstrap()

           this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('age_start').withTitle('Edad Inicio'),
              DTColumnBuilder.newColumn('age_end').withTitle('Edad Final'),
              DTColumnBuilder.newColumn('amount').withTitle('Monto').renderWith(decimals),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
          })
          /////////////////////////////////////////////////////////////
        let DeductibleChild = API.service('detail', API.all('deductible_details'))
        DeductibleChild.one(deductibleId).get()
          .then((response) => {

            let dataDet = response.plain()
            console.log("data 2", dataDet.data)

            this.dtOptionsD = DTOptionsBuilder.newOptions()
              .withOption('data', dataDet.data)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withOption('paging', false)
              .withOption('searching', false)
              .withOption('info', false)
              .withBootstrap()

           this.dtColumnsD = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('child_quantity').withTitle('Niños').renderWith(dataChild),
              DTColumnBuilder.newColumn('amount').withTitle('Monto').renderWith(decimals),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTableD = true
          })

          /////////////////////////////////////////////////////////////

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let decimals = (data) => {
          return `
                        <div class="">
                          $ {{${data} |  number:2}}
                        </div>
                        `
        }

         let dataChild = (data) => {
          return `
                    <div ng-show='"${data}" == "1"'>
                      1 
                    </div>
                    <div ng-show='"${data}" == "2"'>
                      2 
                    </div>
                    <div ng-show='"${data}" == "3"'>
                      3+ 
                    </div>
                  `
        }


        let actionsHtml = (data) =>  {
          return`<ul>
                    
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.deductible-details-edit({deductibleDetailId:${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  </ul>`
        }

        //
    }
    delete (detailId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará el detalle del deducible!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('deductible_details').one('detail', detailId).remove()
          .then(() => {
            swal({
              title: 'Eliminada!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const DeductibleDetailsListsComponent = {
    templateUrl: './views/app/components/deductible-details-lists/deductible-details-lists.component.html',
    controller: DeductibleDetailsListsController,
    controllerAs: 'vm',
    bindings: {}
}
