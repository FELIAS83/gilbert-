class EmissionDependentsDataController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        var vm = this
        this.current_step = '3'
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        this.lines = []
        this.dependents = []//falta validar que muestre los checks y que se muestre la informacion de la unidad
        this.heightunit = 'Cms'
        this.uisrefparameter = '{policyId: vm.policyId }'
        this.uisrefback = 'app.emission-plan-config({policyId: vm.policyId})'

        let policyId = $stateParams.policyId
        this.policyId = policyId
        console.log(policyId)
        this.completed = $stateParams.completed
        console.log(this.completed)
        if (this.completed  == 1){
            this.uisrefparameter = ' {policyId: vm.policyId, completed: 1}'
            this.uisrefback = 'app.emission-applicant-data({policyId: vm.policyId, completed: 1})'
        }

        let Wizar = this.API.service('wizar')
        Wizar.getList()
          .then((response) => {
            let systemWizar = []
            let wizarResponse = response.plain()
            //console.log(wizarResponse)
            angular.forEach(wizarResponse, function (value) {
              if ($stateParams.completed == 1){
                    if ((value.order == 1) || (value.order == 3) || (value.order == 7)){
                        systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})
                    }
                }
                else{
                    systemWizar.push({id: value.id, name: value.name, icon: value.icon, uisref: value.uisref, order: value.order})

                }
            })

            this.systemWizar = systemWizar
        })

        let DependentData = API.service('show', API.all('dependents'))
        DependentData.one(policyId).get()
          .then((response) => {
            this.DependentData = API.copy(response)
            console.log("dependent ", this.DependentData.data)

            for (var i = 0; i < this.DependentData.data.length; i++){
                console.log('fechaf',new Date(this.DependentData.data[i].birthday))
                this.lines.push({id : this.lines.length+1});
                this.dependents.push( {name : this.DependentData.data[i].first_name,
                                      lastname : this.DependentData.data[i].last_name,
                                      pid_type : this.DependentData.data[i].document_type,
                                      pid_num : this.DependentData.data[i].identity_document,
                                      role : this.DependentData.data[i].relationship,
                                      dob : new Date( this.DependentData.data[i].birthday+' '),
                                      height : parseFloat(this.DependentData.data[i].height, 2),
                                      height_unit_measurement : this.DependentData.data[i].height_unit_measurement,
                                      weight : parseFloat(this.DependentData.data[i].weight, 2),
                                      weight_unit_measurement : this.DependentData.data[i].weight_unit_measurement,
                                      sex : this.DependentData.data[i].gender,
                                      id : this.DependentData.data[i].id})
            }
        })
    }

    addLine() {
        this.lines.push({id : this.lines.length+1});
        console.log(this.lines)
    }

    mostrar() {
        console.log(this.dependents)
    }

    save (isValid) {

        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {
          this.isDisabled = true;
          let Dependents = this.API.service('dependents', this.API.all('dependents'))
          let $state = this.$state

          console.log("xxx ", this.dependents)
          Dependents.post({
            'dependents' : this.dependents,
            'aplicantid' : this.policyId
          }).then(function (response) {
            swal({
                title: 'Datos de Dependientes Guardados Correctamente!',
                type: 'success',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go('app.emission-medical-information',{"policyId": response.data})
              })
          }, function (response) {
            swal({
                title: 'Error' + response.data.message,
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true
              }, function () {
                    $state.go($state.current)
              })
          })
        } else {
          this.formSubmitted = true
        }
    }

    deleteLine (dependentid,index){
        console.log(dependentid)
        this.API.one('dependents').one('dependents', dependentid).remove()
          .then(() => {
          })

        var pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        var newvalue = -1
        var newlines = []
        angular.forEach(this.lines, function (value) {

              newlines.push({id : newvalue+1})
        })
        this.lines = newlines
        console.log('newlines',this.lines)
        this.dependents.splice(pos,1)
    }

    $onInit(){
    }
}

export const EmissionDependentsDataComponent = {
    templateUrl: './views/app/components/emission-dependents-data/emission-dependents-data.component.html',
    controller: EmissionDependentsDataController,
    controllerAs: 'vm',
    bindings: {}
}
