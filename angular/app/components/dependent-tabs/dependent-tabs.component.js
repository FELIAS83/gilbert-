class DependentTabsController{
    constructor(API, $stateParams, DTOptionsBuilder, DTColumnBuilder, $compile, $scope){
        'ngInject';

        this.active_tab = 1

        let dependentData = API.service('dependentsinfo', API.all('dependents'))//ojo
        dependentData.one($stateParams.dependentId).get()
        .then((response) => {
            this.dependent_data = API.copy(response).data

            if (this.dependent_data.dependents.birthday == null) {
                this.dependent_data.dependents.birthday =  undefined;
            }
            else{
                this.dependent_data.dependents.birthday =  new Date(this.dependent_data.dependents.birthday)
                this.dependent_data.dependents.birthday.setDate(this.dependent_data.dependents.birthday.getDate() + 1)
            }
            this.dependent_data.dependents.height = parseFloat(this.dependent_data.dependents.height, 2)
            this.dependent_data.dependents.weight = parseFloat(this.dependent_data.dependents.weight, 2)

            this.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('data', this.dependent_data.claims)
            .withOption('createdRow', createdRow)
            .withOption('responsive', true)
            .withBootstrap()
            .withOption('autoWidth', false)

            this.dtColumns = [
                DTColumnBuilder.newColumn('id').withTitle('ID').withOption('width', '5%'),
                DTColumnBuilder.newColumn('amount').withTitle('Monto').withOption('width', '10%'),
                DTColumnBuilder.newColumn('description').withTitle('Descripción')
            ]

            this.displayTable = true

            let createdRow = (row) => {
              $compile(angular.element(row).contents())($scope)
            }

            this.dtOptions2 = DTOptionsBuilder.newOptions()
            .withOption('data', this.dependent_data.tickets)
            .withOption('createdRow', createdRow2)
            .withOption('responsive', true)
            .withBootstrap()
            .withOption('autoWidth', false)

            this.dtColumns2 = [
                DTColumnBuilder.newColumn('id').withTitle('ID Ticket').withOption('width', '10%'),
                DTColumnBuilder.newColumn('ticket_type.name').withTitle('Tipo'),
                DTColumnBuilder.newColumn('date_start').withTitle('Fecha de Cita'),
              DTColumnBuilder.newColumn('created_at').withTitle('Fecha de Creación')
            ]

            this.displayTable2 = true

            let createdRow2 = (row) => {
              $compile(angular.element(row).contents())($scope)
            }
        })
    }

    setActive(tab){
        this.active_tab = tab
    }

    $onInit(){
    }
}

export const DependentTabsComponent = {
    templateUrl: './views/app/components/dependent-tabs/dependent-tabs.component.html',
    controller: DependentTabsController,
    controllerAs: 'vm',
    bindings: {}
}
