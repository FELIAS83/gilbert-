class EmailFormatEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        this.API=API        
        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false

        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let emailformatId = $stateParams.emailformatId
        //console.log(companyId)
        let EmailFormatData = API.service('show', API.all('email_formats'))
        EmailFormatData.one(emailformatId).get()
          .then((response) => {
            this.emailformateditdata = API.copy(response)
         //   this.mailformateditdata.data.commission_percentage = parseFloat(this.companyeditdata.data.commission_percentage, 2)
          //console.log("data=>", this.companyeditdata);
        })
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
            let vm = this
          console.log(this.emailformateditdata);
          let $state = this.$state
          this.emailformateditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el Formato de correo.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
    }
    

    $onInit(){
    }
}

export const EmailFormatEditComponent = {
    templateUrl: './views/app/components/email-format-edit/email-format-edit.component.html',
    controller: EmailFormatEditController,
    controllerAs: 'vm',
    bindings: {}
}
