class DeductibleDetailsAddController{
    constructor($scope, API, $state, $stateParams){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.alerts = []
        vm.blockAge = true
        vm.blockQuantity = false
        vm.hideAges = hideAges
        vm.showAges = showAges
        vm.child_quantity = 0
        this.processing = false
        //vm.child_quantity = {id:1, name:1}
        
        
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let deductibleId = $stateParams.deductibleId
        console.log("id del deducible", deductibleId)

        let DeductibleData = API.service('show', API.all('deductibles'))
        DeductibleData.one(deductibleId).get()
          .then((response) => {
            this.deductibleeditdata = API.copy(response)
            console.log("Data de deducible", this.deductibleeditdata.data)
            //let agentResponse = response.plain(response)

            let PlanData = API.service('show', API.all('plans'))
            PlanData.one(this.deductibleeditdata.data.plan_id).get()
            .then((response) => {
            this.planeditdata = API.copy(response)
            console.log("data del plan", this.planeditdata.data)
            let planResponse = response.plain(response)
        
        })
        
      })

        function hideAges() {
            
            vm.blockAge = false
            vm.blockQuantity = true
            var age_start = 0
            var age_end = 0
            vm.age_start = age_start
            vm.age_end = age_end
        }

         function showAges() {
            
            vm.blockAge = true
            vm.blockQuantity = false
            var age_start = ""
            var age_end = ""
            vm.age_start = age_start
            vm.age_end = age_end
            vm.child_quantity = 0
        }

        //
    }
    save (isValid) {    
        //this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) {     
            this.processing = true
            let vm = this 
          let DeductibleDetails = this.API.service('details', this.API.all('deductible_details'))
          let $state = this.$state        
          
          DeductibleDetails.post({
            'age_start': this.age_start,
            'age_end': this.age_end,
            'is_child': this.is_child,
            'child_quantity': this.child_quantity,
            'amount': this.amount,
            'deductible_id': this.deductibleeditdata.data.id

          }).then(function () {
            vm.processing = false
            console.log("mensaje", $state.current);
            let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha añadido el detalle del deducible.' }
            $state.go($state.current, { alerts: alert})
            console.log($state.current);
          }, function (response) {
            let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
            $state.go($state.current, { alerts: alert})
          })
        } else { 
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const DeductibleDetailsAddComponent = {
    templateUrl: './views/app/components/deductible-details-add/deductible-details-add.component.html',
    controller: DeductibleDetailsAddController,
    controllerAs: 'vm',
    bindings: {}
}
