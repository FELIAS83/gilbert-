class ClaimDocListsController{
        constructor($stateParams,API,$http,$state,$scope,$uibModal){
        'ngInject';
        var vm = this 
        this.API = API
        var relationship = ""
        this.$http = $http
        this.$state = $state
        var claimId = $stateParams.claimId
        this.claimId = claimId
        let policyId = $stateParams.policyId
        this.policyId =policyId
        let role = $stateParams.role
        this.role =role
        let id= $stateParams.id
        this.id =id
        console.log("rol", this.role, "claimId", this.claimId, "policyId", this.policyId, "id", this.id)
        this.lines = []
        this.value = []
        this.description = []
        this.directory = '/claims/'
        this.formSubmitted = false
        this.alerts = []
        var systemDependents = []
        var allAffiliates = []
        this.processing = false

        this.$uibModal = $uibModal
        this.client = ""
    
       
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)    
        }

        //console.log("role"+role)
        //console.log("claimId"+claimId)
        //console.log("id"+id)
        //console.log("policy"+policyId)
        let Policies = API.service('show', API.all('policies'))
        Policies.one(policyId).get()
        .then((response) => {
            this.policy = API.copy(response)
            //console.log("policy.data", this.policy.data[0].number)
            //console.log("policyId"+policyId)
            this.title = 'Reclamo #'+claimId+' Póliza #'+this.policy.data[0].number+' | '+this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
            this.client = this.policy.data[0].first_name+' '+ this.policy.data[0].last_name
        })

        let Documents = API.service('showdoc', API.all('claims'))
        Documents.one(claimId).get()
        .then((response) => {
            let dataSet = API.copy(response)
            this.previouslines = dataSet.data
            console.log(this.previouslines)
            //console.log("previouslines"+JSON.stringify(this.previouslines))
            for (let i = 0; i < this.previouslines.length; i++){
                //this.previouslines[i].amount = parseFloat(this.previouslines[i].amount)
                //console.log(this.previouslines[i].amount)
                if (this.previouslines[i].amount == 0){
                    this.previouslines[i].amount = ''

                }
            }
        })
 
       let systemDocTypes = []
        let DocType =  this.API.service('claimdocumenttypes')
        DocType.getList()
        .then((response) => {
            
            let docResponse =  response.plain()
            //console.log("Tipos", docResponse)
            angular.forEach(docResponse, function(value){
                systemDocTypes.push({id: value.id, name: value.name})
            })

        })
        this.systemDocTypes = systemDocTypes
        this.seleccionados= {}
        //console.log(this.systemDocTypes)

        let Currencies = API.service('currencies')
        Currencies.getList()
        .then((response) => {
            let systemCurrency = []
            let currencyResponse = response.plain()
            angular.forEach(currencyResponse, function (value) {
                systemCurrency.push({id: value.id, name: value.name})
            })

            this.systemCurrency = systemCurrency
            //console.log("monedas ", this.systemCurrency)
        })

        if(this.role != 'T'){
         let Dependents= API.service('dependentsid', API.all('dependents'))
         Dependents.one(id).get()
         .then((response)=>{
            //this.dependentdata=API.copy(response)
           this.dependentdata=response.plain()
           this.client = this.dependentdata.data[0].first_name+' '+this.dependentdata.data[0].last_name
           //console.log("dependentdata", this.dependentdata.data[0].first_name)
            //console.log("dpendentdata"+JSON.stringify(this.dependentdata))
         })  
        }


    }


    save (isValid) {

        //this.alerts = [];
        //if (isValid) {
        //console.log("xxxxxx ", isValid)
        this.$state.go(this.$state.current, {}, { alerts: 'test' })
        if (isValid) { 
            this.processing = true
            let Diagnostics = this.API.service('diagnostics', this.API.all('diagnostics'))
            let $state = this.$state 
            let API = this.API
            //alert(JSON.stringify(this.seleccionados));     
         
            var docs = []
            angular.forEach(this.seleccionados, function (value, key) {
            //    console.log("value ", key)
            if (value) {
                //docs = docs + "," + key
                docs.push({id: key, related: 0})
                }           
            })

            //k
            angular.forEach(JSON.parse(localStorage.getItem('docs')), function (value, key) {
            //    console.log("value ", key)
            if (value) {
                //docs = docs + "," + key
                docs.push({id: key, related: 1})
                }
            })
            //k

            //docs = docs.substr(1);
            //console.log('seleccionados'+JSON.stringify(this.seleccionados))
            //console.log('docs'+docs)
            //var arreglodocs=docs.split(',')
            var arreglodocs=docs
            //console.log('arreglodocs'+arreglodocs)
            var d = docs.length
            //console.log('doc.length'+d)

            var person_type=''
            if(this.role == 'T'){
                    person_type ='A'
            }
            else{
                person_type ='D'
            }

            console.log("claimId... ", this.claimId)
            console.log("policyId... ", this.policyId)
            let vm = this

            Diagnostics.post({
                'policyId':this.policyId,
                'claim_id':this.claimId,
                'description': this.description,
                'id_person':this.id,
                'person_type':person_type,
                'document_id':arreglodocs        
              }).then(function (response) {
                  vm.processing = false
                  console.log("mensaje", $state.current);
                  //let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha guardado el diagnostico.' }
                  //$state.go($state.current, { alerts: alert})
                  swal({
                      title: 'Diagnóstico creado correctamente!',
                      type: 'success',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                  }, function () {
                      $state.go('app.ticket-claim-lists', {"claimId": response.data.claim_id, "policyId": response.data.policyId})
                  })
                    //console.log($state.current);
                }, function (response) {
                    swal({
                      title: 'Error:' + response.data.message,
                      type: 'error',
                      confirmButtonText: 'OK',
                      closeOnConfirm: true
                    }, function () {
                          $state.go($state.current)
                    })
            })
        } else{
            this.formSubmitted = true
        }
        //}
    }


     

    mostrar(){
        console.log('lines',this.lines)
        console.log('description',this.description)
        console.log('value',this.value)
        console.log('files',this.files)
    }
    
     openModal (size, claimId, name, last_name, affiliateId) {
      let $uibModal = this.$uibModal
      let modalInstance = $uibModal.open({
          templateUrl: 'myModalAgent',
          controller: this.modalcontroller,
          controllerAs: 'mvm',
          size: size,
          resolve: {
              claimId: () => {
                  return claimId
              },
              name: () => {
                  return name
              },
              last_name: () => {
                  return last_name

              },
              affiliateId: () => {
                  return affiliateId

              }
          }
      })
    }
    
    modalcontroller ($scope, $uibModalInstance, API,  $http, $stateParams,claimId, name, last_name,affiliateId) {
        'ngInject'

        let mvm = this
        this.claimId = claimId
        this.name = name
        this.last_name = last_name
        this.affiliateId =  affiliateId
        this.processing = false
        localStorage.removeItem('docs')
        let diagnosticId = 'x'


        /*let reports_orders = API.service('reportsorders', API.all('diagnostics'))
        reports_orders.one(claimId).get()*/
        let reports_orders = API.all('reportsorders/'+claimId+'/'+affiliateId)
        reports_orders.doGET('',{})
        .then((response) => {
          let dataSet = response.plain()
          console.log("datos", dataSet.data)
          
          angular.forEach(dataSet.data, function (value, key) {
            value.related = 0           
            })
          this.reports_orders = dataSet.data
        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function(){
            console.log('Funcion guardar del modal')
            localStorage.setItem('docs',JSON.stringify(this.seleccionados))
            $uibModalInstance.dismiss('cancel')
        }

    }


   /* modalcontroller ($scope, $uibModalInstance, API,  $http, $stateParams,claimId, name, last_name) {
        'ngInject'

        let mvm = this
        this.claimId = claimId
        this.name = name
        this.last_name = last_name
        this.processing = false

        let reports_orders = API.service('reportsorders', API.all('diagnostics'))
        reports_orders.one(claimId).get()
        .then((response) => {
          let dataSet = response.plain()
          this.reports_orders = dataSet.data
        })

        this.cancel = () => {
            $uibModalInstance.dismiss('cancel')
        }

        this.save = function(){
            console.log('Funcion guardar del modal')
            localStorage.setItem('docs',JSON.stringify(this.seleccionados))
            $uibModalInstance.dismiss('cancel')
        }
    }*/

    $onInit(){
    }
}

export const ClaimDocListsComponent = {
    templateUrl: './views/app/components/claim-doc-lists/claim-doc-lists.component.html',
    controller: ClaimDocListsController,
    controllerAs: 'vm',
    bindings: {}
}