class RegisterPaymentController{
    constructor($scope, $state, $compile, API, $stateParams, $http){
        'ngInject';
        var vm = this
        this.$state = $state
        this.formSubmitted = false
        this.API = API
        this.uploadFile = uploadFile
        this.directory = 'authorization_form/'
        this.files = []
        this.files2 = []
        this.descriptions = []
        this.myFile = []
        this.alerts = []
        this.form = {}
        this.methods = []
        this.files_type = []
        this.account_types = []
        this.card_types = []
        this.card_marks = []
        this.payment_types = []
        this.lines = []
        this.deletedfiles = []
        this.interests = []
        this.months = []
        this.methods.push({id: 0, name: 'Cheque'},
                         {id: 1, name: 'Transferencia'},
                         {id: 2, name: 'Depósito Bancario'},
                         {id: 3, name: 'Tarjeta de Crédito'})
        this.files_type.push({id: 0, name: 'Formulario de Autorización'},
                         {id: 1, name: 'Documento de Identificación'})
        this.account_types.push({id: 1, name: 'Ahorros'},
                         {id: 2, name: 'Corriente'})
        this.card_types.push({id: 1, name: 'Corporativa'},
                         {id: 2, name: 'Personal'})
        this.card_marks.push(
            {id: 1, name: 'American Express'},
            {id: 2, name: 'Visa'},
            {id: 3, name: 'Mastercard'},
            {id: 4, name: 'Diners Club'},
            {id: 5, name: 'Otros'})
        /*this.payment_types.push(
            {id: 1, name: 'Diferido'},
            {id: 2, name: 'Con intereses'},
            {id: 3, name: 'Corriente'},
            {id: 4, name: 'Sin interés'},
            {id: 5, name: '3 meses'},
            {id: 6, name: '6 meses'},
            {id: 7, name: '9 meses'},
            {id: 8, name: '12 meses'})*/
            this.payment_types.push(
            {id: 1, name: 'Diferido'},
            {id: 2, name: 'Corriente'})
        this.interests.push(
            {id: 1, name: 'Con intereses'},
            {id: 2, name: 'Sin interés'})
        this.months.push(
            {id: 1, name: '3 meses'},
            {id: 2, name: '6 meses'},
            {id: 3, name: '9 meses'},
            {id: 4, name: '12 meses'})

        let policyId = $stateParams.policyId
        this.policyId = policyId

        let data = {};
        data.policy_id = policyId
        let Policy = this.API.service('getPolicyInfo');
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet
            this.form = this.policyinfo[0]
            this.client = this.form.first_name+" "+this.form.last_name
            this.planDeduc = this.form.plan+" / "+this.form.deductible
            this.total_value = '$'+this.form.total_value
            this.form.payment_date = new Date( this.form.payment_date+' ')

            if (this.form.mode == 0){
                    this.payfrecuency = 'Anual'
            }
            else if (this.form.mode == 1){
                this.payfrecuency = 'Semestral'
            }
            else if (this.form.mode == 2){
                this.payfrecuency = 'Trimestral'
            }
            else if (this.form.mode == 3){
                this.payfrecuency = 'Mensual'
            }
            this.first_value = '$'+ this.form.first_value

            let attached = {}
            attached.payment_id = this.form.payment_id
            let attacheds = this.API.service('feedocuments')
            attacheds.getList(attached).then((response) => {
                this.previous_lines = response.plain()
                this.description = this.previous_lines[0] ? this.previous_lines[0].description : 'Formulario Autorización'
                this.filename = this.previous_lines[0] ? this.previous_lines[0].filename : ''
                if (this.previous_lines.length == 0){
                    //this.lines.push({id: 0, description:'', myFile:[], files:[] })
                    //this.previous = false
                    this.lines.push({id: 0})
                    this.descriptions.push(0)
                }
                else{
                    //this.previous = true
                }
                console.log(this.previous_lines)
            })
        })

        function uploadFile(){
            var files = vm.files
            var files2 = vm.files2

            let filestemp = []
            angular.forEach(files2, function (value) {
                filestemp.push(value)
            })
            files2 = filestemp

            vm.counter = 0;
            vm.fileslength = files.length
            vm.counter2 = 0;
            vm.fileslength2 = files2.length

            angular.forEach(files, function(files){
                let a = vm.a
                var form_data = new FormData();
                form_data.append('file', files[0]);
                form_data.append('policy_id', vm.policyId);
                form_data.append('folder', vm.directory);
                $http.post('uploadfilespayment', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    vm.counter++
                    if (vm.counter == vm.fileslength && files2.length == 0){
                        send()
                    }
                })
            })

            angular.forEach(files2, function(files){
                var form_data = new FormData();
                form_data.append('file', files[0]);
                form_data.append('policy_id', vm.policyId);
                form_data.append('folder', vm.directory);
                $http.post('uploadfilespayment', form_data,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(response){
                    vm.counter2++
                    if (vm.counter2 == vm.fileslength2){
                        send()
                    }
                })
            })

           if (files.length == 0 && files2.length == 0){
               send()
            }
         }
        function send(){
            API.service('send').post({
                policy_id : vm.policyId,
                payment_id: vm.form.payment_id,
                format_id : 4,
                payment_method_name: vm.methods[vm.form.method].name,
                payment_method: vm.form.method,
                mode: vm.form.mode,
                card_mark: vm.form.method == 3 ? vm.card_marks[vm.form.card_mark-1].name : '',
                card_type: vm.form.method == 3 ? vm.card_types[vm.form.card_type-1].name : '',
                folder: vm.directory
            })
            .then( ( response ) => {
            }, function (response) {
                console.log('error')
            })
        }
    }
    addFile(type){
        this.lines.push({id: this.lines.length})
        this.descriptions[this.lines.length-1] = 0
        if (type == 1){
            this.add = true
        }
    }
    deleteFile(index){
        let pos = this.lines.indexOf(index);
        this.lines.splice(pos,1)
        this.descriptions.splice(pos,1)
        this.myFile.splice(pos,1)
        this.files.splice(pos,1)
        /*let newvalue = -1
        let nelines = []
        angular.forEach(this.lines, function (value) {
            nelines.push({id : newvalue+1})
            newvalue++
        })
        this.lines = nelines*/
    }
    deletePreviousFile(index,id){
        let pos = this.previous_lines.indexOf(index);
        this.previous_lines.splice(pos,1)

        /*let newvalue = -1
        let nelines = []
        angular.forEach(this.previous_lines, function (value) {
            nelines.push({id : value.id, description:value.description, filename:value.filename})
            newvalue++
        })
        this.previous_lines = nelines*/
        this.deletedfiles.push(id)
    }
    save(isValid){
       let filenames = []
        angular.forEach(this.myFile, function (value) {
            filenames.push(value.name)
        })
        this.filenames = filenames
/*
        let updatedfilenames = []
        angular.forEach(this.myFile, function (value) {
            updatedfilenames.push(value.name)
        })
        this.updatedfilenames = updatedfilenames
*/
        if (this.myFile2){
        for (var i = 0; i < this.previous_lines.length; i++) {
            if (this.myFile2[i]) {
                this.previous_lines[i].filename = this.myFile2[i].name
            }
        }}

        if(isValid){
            this.isDisabled = true;
            let confirm = this.API.service('payment', this.API.all('policies'))
            let $state = this.$state
            let form = this.form
            let uploadFile = this.uploadFile
            confirm.post({
                'policyId': this.policyId,
                'payment_id': this.form.payment_id,
                'form': this.form,
                'newfiles': this.lines,
                'filename': this.filenames,
                'updatedattached': this.previous_lines,
                'deletedattached': this.deletedfiles,
                'descriptions': this.descriptions
            }).then(function () {
                uploadFile()
                swal({
                    title: 'Método de Pago Registrado Correctamente!',
                    type: 'success',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                    $state.go('app.emission-policy-lists')
                })
            }, function (response) {
                swal({
                    title: 'Error al Registrar Método de Pago!',
                    type: 'error',
                    confirmButtonText: 'OK',
                    closeOnConfirm: true
                }, function () {
                })
            })
        }
        else{
            this.formSubmitted = true
        }
    }
    $onInit(){
    }
}

export const RegisterPaymentComponent = {
    templateUrl: './views/app/components/register-payment/register-payment.component.html',
    controller: RegisterPaymentController,
    controllerAs: 'vm',
    bindings: {}
}
