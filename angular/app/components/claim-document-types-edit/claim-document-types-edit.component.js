class ClaimDocumentTypesEditController{
    constructor($scope, $stateParams, $state, API){
        'ngInject';

        this.$state = $state
        this.formSubmitted = false
        this.alerts = []
        this.processing = false
        if ($stateParams.alerts) {
          this.alerts.push($stateParams.alerts)
        }

        let claimdoctypeId = $stateParams.claimdoctypeId
        //console.log(currencyId)
        let ClaimDocTypeData = API.service('show', API.all('claimdocumenttypes'))
        ClaimDocTypeData.one(claimdoctypeId).get()
          .then((response) => {
            this.claimdoctypeeditdata = API.copy(response)
            //this.currencyeditdata.data.status
          //console.log("data=>", this.currencyeditdata);
        })
    }

    save (isValid) {
        if (isValid) {
          this.processing = true
          let vm = this
          //console.log(this.claimdoctypeeditdata);
          let $state = this.$state
          this.claimdoctypeeditdata.put()
            .then(() => {
              vm.processing = false
              let alert = { type: 'success', 'title': 'Éxito!', msg: 'Se ha actualizado el tipo de Documento.' }
              $state.go($state.current, { alerts: alert})
            }, (response) => {
              let alert = { type: 'error', 'title': 'Error!', msg: response.data.message }
              $state.go($state.current, { alerts: alert})
            })
        } else {
          this.formSubmitted = true
        }
    }

    $onInit(){
    }
}

export const ClaimDocumentTypesEditComponent = {
    templateUrl: './views/app/components/claim-document-types-edit/claim-document-types-edit.component.html',
    controller: ClaimDocumentTypesEditController,
    controllerAs: 'vm',
    bindings: {}
}
