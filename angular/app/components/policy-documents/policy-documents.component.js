class PolicyDocumentsController{
    constructor($stateParams,API){
        'ngInject';

        let policyId = $stateParams.policyId
        this.policyId = policyId

        let data = {};
        data.policy_id = policyId
        let Policy = API.service('getPolicyInfo');
        Policy.getList(data).then((response) => {
            let dataSet = response.plain()
            this.policyinfo = dataSet[0]

            let attached = {}
            attached.payment_id = this.policyinfo.payment_id
            let feedocuments = API.service('feedocuments')
            feedocuments.getList(attached).then((response) => {
                this.feedocuments = response.plain()
                console.log('feedocuments',this.feedocuments)

            })

            let attacheds = API.service('attacheds')
            attacheds.getList(attached).then((response) => {
                let dataSet2 = response.plain()
                console.log(dataSet2)
                this.filename = dataSet2 ? dataSet2[0].filename : ''
                this.confirm_filename = dataSet2 ? dataSet2[0].confirm_filename : ''
                this.invoice_filename = dataSet2 ? dataSet2[0].invoice_filename : ''
            })
        })
    }

    $onInit(){
    }
}

export const PolicyDocumentsComponent = {
    templateUrl: './views/app/components/policy-documents/policy-documents.component.html',
    controller: PolicyDocumentsController,
    controllerAs: 'vm',
    bindings: {}
}
