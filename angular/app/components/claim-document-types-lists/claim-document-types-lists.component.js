class ClaimDocumentTypesListsController{
    constructor($scope, $state, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams){
        'ngInject';
             this.API = API
        this.$state = $state

        let ClaimDocumentTypes = this.API.service('claimdocumenttypes')
        
        ClaimDocumentTypes.getList({})
          .then((response) => {
            let dataSet = response.plain()

            this.dtOptions = DTOptionsBuilder.newOptions()
              .withOption('data', dataSet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()
            console.log(dataSet)
            this.dtColumns = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('name').withTitle('Nombre'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtml)
            ]

            this.displayTable = true
   })

        let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }
        

        let actionsHtml = (data) => {
          return `  <a  title="Agregar Detalle" class="btn btn-xs btn-info" ui-sref="app.claim-doc-type-descriptions-lists({claimdoctypeId: ${data.id}})">
                        <i class="fa fa-plus"></i>
                    </a>
                    &nbsp
                    <a  title="Editar" class="btn btn-xs btn-warning" ui-sref="app.claim-document-types-edit({claimdoctypeId: ${data.id}})">
                        <i class="fa fa-edit"></i>
                    </a>
                    &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button>
                  `
        }


    }

    delete (claimdocumenttypeId) {
      let API = this.API
      let $state = this.$state

      swal({
        title: 'Está seguro?',
        text: 'Se eliminará la el Tipo de Documento!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, elimínalo!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        API.one('claimdocumenttypes').one('claimdocumenttypes', claimdocumenttypeId).remove()
          .then(() => {
            swal({
              title: 'Eliminado!',
              //text: 'Se han eliminado los permisos del usuario.',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }

    $onInit(){
    }
}

export const ClaimDocumentTypesListsComponent = {
    templateUrl: './views/app/components/claim-document-types-lists/claim-document-types-lists.component.html',
    controller: ClaimDocumentTypesListsController,
    controllerAs: 'vm',
    bindings: {}
}
