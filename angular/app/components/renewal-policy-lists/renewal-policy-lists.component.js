class RenewalPolicyListsController{
    constructor($scope, $state, $log, $compile, DTOptionsBuilder, DTColumnBuilder, API, $stateParams, $uibModal, $http){
        'ngInject';

        this.API = API
        this.$state = $state
        this.$scope=$scope  
        this.$uibModal = $uibModal
        this.$log = $log
        this.animationsEnabled = true
        this.items = ['item1', 'item2', 'item3']
        this.systemWizar = []
        this.systemWizar.push({id: 0, icon: "fa fa-paste fa-3x", name: "Cuotas Vencidas", order: 1, uisref: "app.renewal-lists"}, {id: 2, icon: "fa fa-refresh fa-3x", name: "Pólizas a Vencer", order: 2, uisref: "app.renewal-policy-lists"})
        this.current_step = 2
        this.openTab = openTab
        this.isDisabled1 = false
        this.isDisabled2 = true
        this.type = 2

         /***************************************************************************************/
      let Expired = this.API.service('getExpired')

      Expired.getList({})
          .then((response) => {

            let dataDet = response.plain()
            console.log("por Vencerse",dataDet)

            this.dtOptionsD = DTOptionsBuilder.newOptions()
              .withOption('data', dataDet)
              .withOption('createdRow', createdRow)
              .withOption('responsive', true)
              .withBootstrap()

            this.dtColumnsD = [
              DTColumnBuilder.newColumn('id').withTitle('ID'),
              DTColumnBuilder.newColumn('number').withTitle('Póliza'),
              DTColumnBuilder.newColumn('name').withTitle('Cliente'),
              DTColumnBuilder.newColumn('start_date_coverage').withTitle('Inicio de Cobertura Actual'),
              DTColumnBuilder.newColumn('mode').withTitle('Método de Pago'),
              //DTColumnBuilder.newColumn('fee_payment_date').withTitle('Fecha de Pago'),
              //DTColumnBuilder.newColumn('address').withTitle('Dirección'),
              DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable()
                .renderWith(actionsHtmlD)
            ]

            this.displayTableD = true
          
          })
          
      /***************************************************************************************/
      let createdRow = (row) => {
          $compile(angular.element(row).contents())($scope)
        }

        let actionsHtmlD = (data) =>  {
          return`<ul>
          
                    <a  style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Renovar" class="btn btn-xs btn-info" ng-click="vm.renewal(${data.id})">
                        <i class="fa fa-refresh"></i>
                    </a>
                    &nbsp                    
                    <!--<a style="color: #3c8dbc; background-color: #FFF;width:24.2px;height:21.98px" title="Confirmar Datos Póliza-Renovación " class="btn btn-xs btn-primary"  ui-sref="app.confirm-renewal-data({policyId: ${data.id}})">
                      <i class="fa fa-check"></i>
                    </a>
                     &nbsp
                    <button title="Eliminar" class="btn btn-xs btn-danger" ng-click="vm.delete(${data.id})">
                        <i class="fa fa-trash-o"></i>
                    </button> -->
                  </ul>`
        }  

          function openTab(value){
             let $state = this.$state
            if(value == "1"){     
                 $state.go('app.renewal-lists')
            }else if(value == "2"){
                $state.go('app.renewal-policy-lists')

            }
        }

        //
    }

    renewal (policyId) {
         let API = this.API
         let $state = this.$state
      swal({
        title: 'Está seguro?',
        text: 'Se renovará automaticamente esta Póliza!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sí, renuévala!',
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        html: false
      }, function () {
        let renewal = API.service('renewalpolicy', API.all('renewals'))
        renewal.post({
          'policy_id' : policyId
        })
          .then(() => {
            swal({
              title: 'Renovada!',
              type: 'success',
              confirmButtonText: 'OK',
              closeOnConfirm: true
            }, function () {
              $state.reload()
            })
          })
      })
    }


    $onInit(){
    }
}

export const RenewalPolicyListsComponent = {
    templateUrl: './views/app/components/renewal-policy-lists/renewal-policy-lists.component.html',
    controller: RenewalPolicyListsController,
    controllerAs: 'vm',
    bindings: {}
}
