export function RoutesConfig ($stateProvider, $urlRouterProvider) {
  'ngInject'

  var getView = (viewName) => {
    return `./views/app/pages/${viewName}/${viewName}.page.html`
  }

  var getLayout = (layout) => {
    return `./views/app/pages/layout/${layout}.page.html`
  }

  var setList = (url, tmpl) => {
    return { 
      url: url, 
      data: {auth: true},
      views: {'main@app': {template: '<'+tmpl+'-lists></'+tmpl+'-lists>'}}
    }
  }

  var setAdd = (url, tmpl) => {
    return { 
      url: url, 
      data: {auth: true},
      views: {'main@app': {template: '<'+tmpl+'-add></'+tmpl+'-add>'}},
      params: {alerts: null}
    }
  }

  var setEdit = (url, tmpl) => {
    return { 
      url: url, 
      data: {auth: true},
      views: {'main@app': {template: '<'+tmpl+'-edit></'+tmpl+'-edit>'}},
      params: {alerts: null, id: null}
    }
  }

  $urlRouterProvider.otherwise('/')

  $stateProvider
  .state('app', {
    abstract: true,
    views: {
      'layout': {templateUrl: getLayout('layout')},
      'header@app': {templateUrl: getView('header')},
      'footer@app': {templateUrl: getView('footer')},
      main: {}
    },
    data: {bodyClass: 'hold-transition skin-blue sidebar-mini'}
  })
  .state('app.landing', {
    url: '/',
    data: {auth: true},
    views: {
      'main@app': {templateUrl: getView('landing')}
    }
  })
  
  // Admin - User
  //.state('app.user-lists', setList('/admin/users/lists', 'user'))
  //.state('app.user-add', setAdd('/admin/users/add', 'user'))
  //.state('app.user-edit', setEdit('/admin/users/edit/:id', 'user'))

  // Admin - Roles
  //.state('app.role-lists', setList('/admin/roles/lists', 'role'))
  //.state('app.role-add', setAdd('/admin/roles/add', 'role'))
  //.state('app.role-edit', setEdit('/admin/roles/edit/:id', 'role'))

  // Admin - Permissions
  //.state('app.permission-lists', setList('/admin/permissions/lists', 'permission'))
  //.state('app.permission-add', setAdd('/admin/permissions/add', 'permission'))
  //.state('app.permission-edit', setEdit('/admin/permissions/edit/:id', 'permission'))

  // Reception
  //.state('app.policy-add', setAdd('/receptions/policy/add', 'policy'))
  //.state('app.claim-add', setAdd('/receptions/claim/add', 'claim'))

  // Sale - Company
  //.state('app.company-lists', setList('/sales/company/lists', 'company'))
  //.state('app.company-add', setAdd('/sales/company/add', 'company'))
  //.state('app.company-edit', setEdit('/sales/company/edit/:id', 'company'))

  // Sale - Sellers
  //.state('app.seller-lists', setList('/sales/seller/lists', 'seller'))
  //.state('app.seller-add', setAdd('/sales/seller/add', 'seller'))
  //.state('app.seller-edit', setEdit('/sales/seller/edit/:id', 'seller'))

  // Sale - Sales
  .state('app.sale-lists', setList('/sales/lists', 'sale'))
  .state('app.sale-add', setAdd('/sales/add', 'sale'))
  .state('app.sale-edit', setEdit('/sales/edit/:id', 'sale'))

  // Sale - Commission
  .state('app.commission-company-lists', setList('/sales/commission-company/lists', 'commission-company'))
  .state('app.commission-seller-lists', setList('/sales/commission-seller/lists', 'commission-seller'))

  // Service - Hospital
  /*.state('app.hospital-lists', setList('/services/hospital/lists', 'hospital'))
  .state('app.hospital-add', setAdd('/services/hospital/add', 'hospital'))
  .state('app.hospital-edit', setEdit('/services/hospital/edit/:id', 'hospital'))*/

  // Service - Doctor
  /*.state('app.doctor-lists', setList('/services/doctor/lists', 'doctor'))
  .state('app.doctor-add', setAdd('/services/doctor/add', 'doctor'))
  .state('app.doctor-edit', setEdit('/services/doctor/edit/:id', 'doctor'))*/

  // Service - Specialty
  .state('app.specialty-lists', setList('/services/specialty/lists', 'specialty'))
  .state('app.specialty-add', setAdd('/services/specialty/add', 'specialty'))
  .state('app.specialty-edit', setEdit('/services/specialty/edit/:id', 'specialty'))

  // General - Policies
  //.state('app.policies-lists', setList('/general/policies/lists', 'policies'))

  // General - Affiliates
  .state('app.affiliates-lists', setList('/general/affiliates/lists', 'affiliates'))

  // General - Procedure
  .state('app.procedure-lists', setList('/general/procedure/lists', 'procedure'))
  .state('app.procedure-add', setAdd('/general/procedure/add', 'procedure'))
  .state('app.procedure-edit', setEdit('/general/procedure/edit/:id', 'procedure'))
  
  // General - Messenger
  //.state('app.messenger-lists', setList('/general/messenger/lists', 'messenger'))
  //.state('app.messenger-add', setAdd('/general/messenger/add', 'messenger'))
  //.state('app.messenger-edit', setEdit('/general/messenger/edit/:id', 'messenger'))

  // Reception - Policies
  .state('app.policy-lists', {
    url: '/policy-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<policy-lists></policy-lists>'
      }
    },
  })
  .state('app.policy-add', {
    url: '/reception/policy-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<policy-add></policy-add>'
      }
    },
    params: {
      alerts: null
    }
  })
  .state('app.policy-view', {
      url: '/policy-view/:policyId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<policy-view></policy-view>'
        }
      },params: {
        alerts: null,
        policyId: null
      }
    })
   // Reception - Claims
  .state('app.claim-lists', {
    url: '/claim-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-lists></claim-lists>'
      }
    }
  })
  .state('app.claim-add', {
    url: '/reception/claim-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-add></claim-add>'
      }
    },
    params: {
      alerts: null
    }
  })

  // Emission - Policies
  .state('app.emission-policy-lists', {
    url: '/emission-policy-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-policy-lists></emission-policy-lists>'
      }
    },
  })
.state('app.emission-canceled-lists', {
    url: '/emission-canceled-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-canceled-lists></emission-canceled-lists>'
      }
    },
  })

  // Emission Applicant Data
  .state('app.emission-applicant-data', {
    url: '/emission-applicant-data/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-applicant-data></emission-applicant-data>'
      }
    },params: {
        alerts: null,
        policyId: null,
        completed: null
      }
  })
  // Emission Plan Config
  .state('app.emission-plan-config', {
    url: '/emission-plan-config/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-plan-config></emission-plan-config>'
      }
    },params: {
        alerts: null,
        policyId: null
      }
  })
  // Emission Dependents Data
  .state('app.emission-dependents-data', {
    url: '/emission-dependents-data/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-dependents-data></emission-dependents-data>'
      }
    },params: {
        alerts: null,
        policyId: null,
        completed: null
      }
  })
  // Emission Medical Data
  .state('app.emission-medical-information', {
    url: '/emission-medical-information/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-medical-information></emission-medical-information>'
      }
    },params: {
        alerts: null,
        policyId: null
      }
  })
  // Emission Payment Information
  
  .state('app.emission-payment-information', {
    url: '/emission-payment-information/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-payment-information></emission-payment-information>'
      }
    },params: {
        alerts: null,
        policyId: null
      }
  })

    // Emission Coverage Start
  .state('app.emission-start-coverage', {
    url: '/emission-coverage-start/:policyId',
      data: {
      auth: true
    },
    views: {
      'main@app': {
          template: '<emission-coverage-start></emission-coverage-start>'
           }
    },params: {
        alerts: null,
        policyId: null
      }
  })
    // Emission Documents  
    .state('app.emission-documents', {
    url: '/emission-documents/:policyId',
       data: {
      auth: true
    },
    views: {
      'main@app': {
          template: '<emission-documents></emission-documents>'
           }
    },params: {
        alerts: null,
        policyId: null,
        completed: null
      }
  })
  // Emission Confirm Policy Data
  .state('app.confirm-policy-data', {
  url: '/confirm-policy-data/:policyId',
  data: {
    auth: true
  },
  views: {
    'main@app': {
      template: '<confirm-policy-data></confirm-policy-data>'
    }
  },
  params: {
    alerts: null,
    policyId: null,
    completed: null
  }
  })

  // Emission Register Payment
  .state('app.register-payment', {
  url: '/register-payment/:policyId',
  data: {
    auth: true
  },
  views: {
    'main@app': {
      template: '<register-payment></register-payment>'
    }
  },
  params: {
    alerts: null,
    policyId: null
  }
  })

  // Emission Confirm Payment
  .state('app.confirm-payment', {
  url: '/confirm-payment/:policyId',
  data: {
    auth: true
  },
  views: {
    'main@app': {
      template: '<confirm-payment></confirm-payment>'
    }
  },
  params: {
    alerts: null,
    policyId: null
  }
  })

  // Emission print guide
  .state('app.print-guide', {
      url: '/print-guide/:policyId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<print-guide></print-guide>'
        }
      },params: {
        alerts: null,
        policyId: null
      }
    })



  // Claims - Claims
  .state('app.claim-claim-lists', {
    url: '/claim-ticket-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-claim-lists></claim-claim-lists>'
      }
    }
  })

  // Claims edit
  .state('app.claim-edit', {
      url: '/claim-edit/:claimId/:policyId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<claim-edit></claim-edit>'
        }
      },
      params: {
        alerts: null,
        claimId: null
      }
    })

// Claims - Claims Files
  .state('app.claim-claim-files', {
      url: '/claim-ticket-files/:policyId/:claimId',
      data: {
        auth: true
      },
      views: {
          'main@app': {
            template: '<claim-ticket-files></claim-ticket-files>'
          },
          params: {
              alerts: null,
              claimId: null,
              policyId: null
          }
      }
  })
  //historical claims reports
.state('app.historical-claims-report', {
    url: '/historical-claims-report/',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<historical-claims-report></historical-claims-report>'
        }
    },
    params: {
        
    }
  })

  //Policy Renewal
.state('app.renewal-lists', {
    url: '/renewal-lists',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-lists></renewal-lists>'
        }
    },
    params: {
        alerts: null
    }
  })
  .state('app.renewal-notification', {
    url: '/renewal-notification/:policyId/:feeId/:fee_number',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-notification></renewal-notification>'
        }
    },
    params: {
        policyId: null,
        feeId: null,
        fee_number: null
    }
  })

.state('app.renewal-applicant-data', {
    url: '/renewal-applicant-data/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-applicant-data></renewal-applicant-data>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

.state('app.renewal-plan-config', {
    url: '/renewal-plan-config/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-plan-config></renewal-plan-config>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

.state('app.renewal-dependents-data', {
    url: '/renewal-dependents-data/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-dependents-data></renewal-dependents-data>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

.state('app.renewal-medical-information', {
    url: '/renewal-medical-information/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-medical-information></renewal-medical-information>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

.state('app.renewal-payment-information', {
    url: '/renewal-payment-information/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-payment-information></renewal-payment-information>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

  .state('app.renewal-coverage-start', {
    url: '/renewal-coverage-start/:policyId/:feeId',
      data: {
      auth: true
    },
    views: {
      'main@app': {
          template: '<renewal-coverage-start></renewal-coverage-start>'
           }
    },params: {
        alerts: null,
        policyId: null,
        feeId: null
      }
  })

.state('app.renewal-documents', {
    url: '/renewal-documents/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-documents></renewal-documents>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

.state('app.confirm-renewal-data', {
    url: '/confirm-renewal-data/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<confirm-renewal-data></confirm-renewal-data>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })

// policy to renewal list
  .state('app.renewal-policy-lists', {
    url: '/renewal-policy-lists',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-policy-lists></renewal-policy-lists>'
        }
    },
    params: {
        alerts: null
    }
  })

  .state('app.renewal-payment-register', {
    url: '/renewal-payment-register/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-payment-register></renewal-payment-register>'
        }
    },
    params: {
        policyId: null,
        feeId: null,
        alerts: null
    }
  })

.state('app.send-invoice-renewal', {
    url: '/send-invoice-renewal/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<send-invoice-renewal></send-invoice-renewal>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })


// confirm renewal payment
  .state('app.confirm-renewal-payment', {
    url: '/confirm-renewal-payment/:policyId/:feeId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<confirm-renewal-payment></confirm-renewal-payment>'
        }
    },
    params: {
        policyId: null,
        feeId: null
    }
  })  


.state('app.renewal-report', {
    url: '/renewal-report/',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<renewal-report></renewal-report>'
        }
    },
    params: {
        
    }
  })


.state('app.claim-liquidations', {
    url: '/claim-liquidations',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<claim-liquidations></claim-liquidations>'
        }
    },
    params: {

    }
  })

  .state('app.claim-liquidations.invoices', {
    url: '/invoices/:claimId/:policyId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<liquidations-invoices></liquidations-invoices>'
        }
    },
    params: {
        claimId: null,
        policyId: null

    }
  })


  // Service tickets
  .state('app.ticket-lists', {
    url: '/ticket-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-lists></ticket-lists>'
      }
    },params: {
        alerts: null
      }
  })

.state('app.ticket-add', {
    url: '/ticket-add/',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-add></ticket-add>'
      }
    },params: {
        alerts: null
      }
  })
.state('app.ticket-edit', {
    url: '/ticket-edit/:id_policy/:ticket_id',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-edit></ticket-edit>'
      }
    },params: {
        alerts: null
      }
  })

.state('app.ticket-date-confirm', {
    url: '/ticket-date-confirm/:id_policy/:ticket_id',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-date-confirm></ticket-date-confirm>'
      }
    },params: {
        alerts: null
      }
  })

  // Diagnostic Tickets - Claims
  .state('app.ticket-claim-lists', {
    url: '/ticket-claim-lists/:policyId/:claimId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-claim-lists></ticket-claim-lists>'
      }
    },params: {
        alerts: null
      }
  })

  .state('app.diagnostic-edit', {
    url: '/diagnostic-edit/:claimId/:diagnosticId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<diagnostic-edit></diagnostic-edit>'
        }
    },
    params: {
        claimId: null,
        diagnosticId: null,
        description: null
    }
  })

  // Commisions Report
  .state('app.commission-report', {
    url: '/commission-report',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<commission-report></commission-report>'
        }
    },
    params: {
        alerts: null
    }
  })



  .state('app.claim-doc-lists', {
    url: '/claim-doc-lists/:id/:role/:policyId/:claimId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-doc-lists></claim-doc-lists>'
      }
    },params: {
        alerts: null
      }
  })



  // Sales - Agents
  .state('app.agent-lists', {
      url: '/agent-lists',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<agent-lists></agent-lists>'
        }
      }
    })

  .state('app.agent-add', {
      url: '/agent-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<agent-add></agent-add>'
        }
      },params: {
        alerts: null
      }
    })
  .state('app.agent-edit', {
      url: '/agent-edit/:agentId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<agent-edit></agent-edit>'
        }
      },params: {
        alerts: null
      }
    })

  .state('app.agent-sublists', {
    url: '/agent-sublists/:agentId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<agent-sublists></agent-sublists>'
      }
    },params: {
      alerts: null
    }
  })
// Assistent Add
  .state('app.assistent-add', {
    url: '/assistent-add/:agentId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<assistent-add></assistent-add>'
      }
    },
    params: {
      alerts: null,
    }
  })
  .state('app.assistent-lists', {
    url: '/assistent-lists/:agentId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<assistent-lists></assistent-lists>'
      }
    },
    params: {
      alerts: null,
      agentId: null
    }
  })
  .state('app.assistent-edit', {
    url: '/assistent-edit/:assistentId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<assistent-edit></assistent-edit>'
      }
    },
    params: {
      alerts: null,
      assistentId: null

    }
  })
  // Sales - Subagent
  .state('app.subagent-lists', {
      url: '/subagent-lists',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<subagent-lists></subagent-lists>'
        }
      },
    })

  .state('app.subagent-add', {
      url: '/subagent-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<subagent-add></subagent-add>'
        }
      },params: {
        alerts: null
      }
    })

  .state('app.subagent-edit', {
      url: '/subagent-edit/:subagentId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<subagent-edit></subagent-edit>'
        }
      },params: {
        alerts: null
      }
    })
  .state('app.messenger-lists', {
    url: '/messenger-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<messenger-lists></messenger-lists>'
      }
    }
  })
  .state('app.messenger-add', {
    url: '/messenger-add/',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<messenger-add></messenger-add>'
      }
    },
    params: {
      alerts: null,
    }
  })
  .state('app.messenger-edit', {
    url: '/messenger-edit/:messengerId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<messenger-edit></messenger-edit>'
      }
    },
    params: {
      alerts: null
    }
  })
  // Branchs
.state('app.branch-lists', {
    url: '/branch-lists/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<branch-lists></branch-lists>'
      }
      },
    params: {
      companyId: null,
      alerts: null
    }
  })
  .state('app.branch-add', {
    url: '/branch-add/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<branch-add></branch-add>'
      }
    },
    params: {
      alerts: null,
      companyId: null
    }
  })
  .state('app.branch-edit', {
    url: '/branch-edit/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<branch-edit></branch-edit>'
      }
    },
    params: {
      alerts: null,
      branchId: null,
      companyId: null
    }
  })

  // Medical speciality
  .state('app.medical-speciality-lists', {
    url: '/medical-speciality-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<medical-speciality-lists></medical-speciality-lists>'
      }
    },
    params: {
      alerts: null
    }
  })
    .state('app.medical-speciality-add', {
    url: '/medical-speciality-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<medical-speciality-add></medical-speciality-add>'
      }
    },
    params: {
      alerts: null
    }
  })
    .state('app.medical-speciality-edit', {
    url: '/medical-speciality-edit/:specialityId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<medical-speciality-edit></medical-speciality-edit>'
      }
    },
    params: {
      alerts: null,
      companyId: null
    }
  })

    .state('app.doctor-lists', {
    url: '/doctor-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<doctor-lists></doctor-lists>'
      }
    },
    params: {
      alerts: null
    }
  })
 .state('app.doctor-add', {
    url: '/doctor-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<doctor-add></doctor-add>'
      }
    },
    params: {
      alerts: null
    }
  })
    .state('app.doctor-edit', {
    url: '/doctor-edit/:doctorId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<doctor-edit></doctor-edit>'
      }
    },
    params: {
      alerts: null
    }
  })
 .state('app.hospital-lists', {
    url: '/hospital-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<hospital-lists></hospital-lists>'
      }
    },
    params: {
      alerts: null
    }
  })
 .state('app.hospital-add', {
    url: '/hospital-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<hospital-add></hospital-add>'
      }
    },
    params: {
      alerts: null
    }
  })


  .state('app.hospital-edit', {
    url: '/hospital-edit/:hospitalId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<hospital-edit></hospital-edit>'
      }
    },
    params: {
      alerts: null
    }
  })
    .state('app.claim-document-types-lists', {
    url: '/claim-document-types-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-document-types-lists></claim-document-types-lists>'
      }
    }
  })

.state('app.claim-document-types-add', {
    url: '/claim-document-types-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-document-types-add></claim-document-types-add>'
      }
    },
    params: {
      alerts: null
    }
  })

.state('app.claim-document-types-edit', {
    url: '/claim-document-types-edit/:claimdoctypeId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-document-types-edit></claim-document-types-edit>'
      }
    },
    params: {
      alerts: null
    }
  })

.state('app.claim-doc-type-descriptions-lists', {
    url: '/claim-doc-type-descriptions-lists/:claimdoctypeId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-doc-type-descriptions-lists></claim-doc-type-descriptions-lists>'
      }
    },
    params: {
      alerts: null
    }
  })
.state('app.claim-doc-type-descriptions-add', {
    url: '/claim-doc-type-descriptions-add/:claimdoctypeId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-doc-type-descriptions-add></claim-doc-type-descriptions-add>'
      }
    },
    params: {
      alerts: null
    }
  })
.state('app.claim-doc-type-descriptions-edit', {
    url: '/claim-doc-type-descriptions-edit/:claimdoctypedescId/:claimdoctypeId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<claim-doc-type-descriptions-edit></claim-doc-type-descriptions-edit>'
      }
    },
    params: {
      alerts: null
    }
  })


  //Mail configuration
.state('app.mail-configuration-edit', {
      url: '/mail-configuration-edit/:configurationId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<mail-configuration-edit></mail-configuration-edit>'
        }
      },
      params: {
        alerts: null,
        configurationId: null
      }
    })

//Mail formats
.state('app.mail-format-lists', {
      url: '/mail-format-lists',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<mail-format-lists></mail-format-lists>'
        }
      },
      params: {
        alerts: null,
        mailformatId: null
      }
    })
.state('app.mail-format-add', {
      url: '/mail-format-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<mail-format-add></mail-format-add>'
        }
      },
      params: {
        alerts: null,
        mailformatId: null
      }
    })
.state('app.mail-format-edit', {
      url: '/mail-format-edit/:emailformatId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<mail-format-edit></mail-format-edit>'
        }
      },
      params: {
        alerts: null,
        emailformatId: null
      }
    })
//Correo Electronico //Formato de Correo
.state('app.email-format-lists', {
      url: '/email-format-lists',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<email-format-lists></email-format-lists>'
        }
      },
      params: {
        alerts: null,
        mailformatId: null
      }
    })
.state('app.email-format-add', {
      url: '/email-format-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<email-format-add></email-format-add>'
        }
      },
      params: {
        alerts: null,
        mailformatId: null
      }
    })
.state('app.email-format-edit', {
      url: '/email-format-edit/:emailformatId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<email-format-edit></email-format-edit>'
        }
      },
      params: {
        alerts: null,
        emailformatId: null
      }
    })

//Massive mail
.state('app.massivemail', {
      url: '/massivemail',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<massivemail></massivemail>'
        }
      },
      params: {
        alerts: null,
        massivemailId: null
      }
    })


// send EOB to client
  .state('app.send-eob', {
    url: '/send-eob/:claimId/:policyId/:diagnosticId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<send-eob></send-eob>'
        }
    },
    params: {
        policyId: null,
        claimId:null,
        diagnosticId:null
    }
  })  

 //Diagnostics EOB
 .state('app.diagnostics-eob', {
      url: '/diagnostics-eob/:claimId/:policyId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<diagnostics-eob></diagnostics-eob>'
        }
      },
      params: {
        alerts: null,
        claimId: null
      }
    })


  .state('app.profile', {
    url: '/profile',
    data: {auth: true},
    views: {
      'main@app': {template: '<user-profile></user-profile>'}
    },
    params: {alerts: null}
  })
  .state('login', {
    url: '/login',
    views: {
      'layout': {templateUrl: getView('login')},
      'header@app': {},
      'footer@app': {}
    },
    data: {bodyClass: 'hold-transition login-page'},
    params: {registerSuccess: null, successMsg: null}
  })
  .state('loginloader', {
    url: '/login-loader',
    views: {
      'layout': {templateUrl: getView('login-loader')},
      'header@app': {},
      'footer@app': {}
    },
    data: {bodyClass: 'hold-transition login-page'}
  })
  .state('register', {
    url: '/register',
    views: {
      'layout': {templateUrl: getView('register')},
      'header@app': {},
      'footer@app': {}
    },
    data: {bodyClass: 'hold-transition register-page'}
  })
  .state('userverification', {
    url: '/userverification/:status',
    views: {
      'layout': {templateUrl: getView('user-verification')}
    },
    data: {bodyClass: 'hold-transition login-page'},
    params: {status: null}
  })
  .state('forgot_password', {
    url: '/forgot-password',
    views: {
      'layout': {templateUrl: getView('forgot-password')},
      'header@app': {},
      'footer@app': {}
    },
    data: {bodyClass: 'hold-transition login-page'}
  })
  .state('reset_password', {
    url: '/reset-password/:email/:token',
    views: {
      'layout': {templateUrl: getView('reset-password')},
      'header@app': {},
      'footer@app': {}
    },
    data: {bodyClass: 'hold-transition login-page'}
  })
  .state('app.coming-soon', {
      url: '/comingsoon',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<coming-soon></coming-soon>'
        }
      }
    })
  
  .state('app.company-lists', {
    url: '/company-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<company-lists></company-lists>'
      }
    }
  })
  .state('app.company-add', {
    url: '/company-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<company-add></company-add>'
      }
    },
    params: {
      alerts: null
    }
  })
  .state('app.company-edit', {
    url: '/company-edit/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<company-edit></company-edit>'
      }
    },
    params: {
      alerts: null,
      companyId: null
    }
  })
  .state('app.seller-lists', {
    url: '/seller-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<seller-lists></seller-lists>'
      }
    }
  })
  .state('app.seller-add', {
    url: '/seller-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<seller-add></seller-add>'
      }
    },
    params: {
      alerts: null
    }
  })
  .state('app.seller-edit', {
    url: '/seller-edit/:sellerId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<seller-edit></seller-edit>'
      }
    },
    params: {
      alerts: null,
      sellerId: null
    }
  })

  .state('app.plan-lists', {
    url: '/plan-lists/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<plan-lists></plan-lists>'
      }
      },
    params:{
      alerts:null,
      branchId: null,
      companyId: null

    }
  })
  .state('app.plan-add', {
    url: '/plan-add/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<plan-add></plan-add>'
      }
    },
    params: {
      alerts: null,
      branchId: null,
      companyId: null
    }
  })
  .state('app.plan-edit', {
    url: '/plan-edit/:planId/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<plan-edit></plan-edit>'
      }
    },
    params: {
      alerts: null,
      planId: null,
      branchId: null,
      companyId: null
    }
  })
  .state('app.deductible-add', {
    url: '/deductible-add/:planId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-add></deductible-add>'
      }
    },
    params: {
      alerts: null,
      planId: null
    }
  })
  .state('app.deductible-lists', {
    url: '/deductible-lists/:planId/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-lists></deductible-lists>'
      }
    },
    params: {
      alerts: null,
      planId: null,
      branchId: null,
      companyId: null
    }
  })
  .state('app.deductible-edit', {
    url: '/deductible-edit/:deductibleId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-edit></deductible-edit>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null

    }
  })
  .state('app.deductible-details-add', {
    url: '/deductible-details-add/:deductibleId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-details-add></deductible-details-add>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null
    }
  })
  .state('app.deductible-details-lists', {
    url: '/deductible-details-lists/:deductibleId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-details-lists></deductible-details-lists>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null
    }
  })
  .state('app.deductible-details-edit', {
    url: '/deductible-details-edit/:deductibleDetailId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<deductible-details-edit></deductible-details-edit>'
      }
    },
    params: {
      alerts: null,
      deductibleDetailId: null
    }
  })

  //coverage
  .state('app.coverage-add', {
    url: '/coverage-add/:planId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-add></coverage-add>'
      }
    },
    params: {
      alerts: null,
      planId: null
    }
  })
  .state('app.coverage-lists', {
    url: '/coverage-lists/:planId/:branchId/:companyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-lists></coverage-lists>'
      }
    },
    params: {
      alerts: null,
      planId: null,
      branchId: null,
      companyId: null
    }
  })
  .state('app.coverage-edit', {
    url: '/coverage-edit/:coverageId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-edit></coverage-edit>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null

    }
  })

  .state('app.coverage-details-add', {
    url: '/coverage-details-add/:deductibleId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-details-add></coverage-details-add>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null
    }
  })
  .state('app.coverage-details-lists', {
    url: '/coverage-details-lists/:deductibleId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-details-lists></coverage-details-lists>'
      }
    },
    params: {
      alerts: null,
      deductibleId: null
    }
  })
  .state('app.coverage-details-edit', {
    url: '/coverage-details-edit/:coverageDetailId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<coverage-details-edit></coverage-details-edit>'
      }
    },
    params: {
      alerts: null,
      coverageDetailId: null
    }
  })
  
    //General -Currencies
  .state('app.currency-lists', {
    url: '/currency-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<currency-lists></ccurrency-lists>'
      }
    }
  })
  .state('app.currency-add', {
    url: '/currency-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<currency-add></currency-add>'
      }
    },
    params: {
      alerts: null
    }
  })



.state('app.currency-edit', {
      url: '/currency-edit/:currencyId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<currency-edit></currency-edit>'
        }
      },
      params: {
        alerts: null,
        companyId: null
      }
    })
.state('app.provider-list', {
      url: '/doc/provider-list',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<provider-list></provider-list>'
        }
      }
    })
  //General //Proveedor de Servicios
  .state('app.provider-lists', {
      url: '/provider-lists',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<provider-lists></provider-lists>'
        }
      }
    })
    .state('app.provider-add', {
      url: '/provider-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<provider-add></provider-add>'
        }
      },
      params: {
        alerts: null
      }
    })
  .state('app.provider-edit', {
      url: '/provider-edit/:providerId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<provider-edit></provider-edit>'
        }
      },
      params: {
        alerts: null,
        companyId: null
      }
    })



  .state('app.directory-edit', {
    url: '/directory-edit/:directoryId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<directory-edit></directory-edit>'
      }
    },
    params: {
      directoryId: '1'
    }
  })
  //Admin - Users
  .state('app.user-lists', {
    url: '/user-lists',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-lists></user-lists>'
      }
    },
  })
  .state('app.user-edit', {
    url: '/user-edit/:userId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-edit></user-edit>'
      }
    },
    params: {
      alerts: null,
      userId: null

    }
  })
  .state('app.user-add', {
      url: '/user-add',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<user-add></user-add>'
        }
      },params: {
        alerts: null
      }
    })

  .state('app.fees-paid-report', {
    url: '/fees-paid-report/',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<fees-paid-report></fees-paid-report>'
        }
    },
    params: {
        
    }
  })

  // Admin - Roles
  .state('app.user-roles', {
    url: '/user-roles',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-roles></user-roles>'
      }
    },
  })
  .state('app.user-roles-add', {
    url: '/user-roles-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-roles-add></user-roles-add>'
      }
    },params: {
        alerts: null
      }
  })
  .state('app.user-roles-edit', {
      url: '/user-roles-edit/:roleId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<user-roles-edit></user-roles-edit>'
        }
      },params: {
        alerts: null,
        roleId: null
      }
    })

  // Admin - Permissions
  .state('app.user-permissions', {
    url: '/user-permissions',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-permissions></user-permissions>'
      }
    },
  })

  .state('app.user-permissions-add', {
    url: '/user-permissions-add',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<user-permissions-add></user-permissions-add>'
      }
    },params: {
        alerts: null
      }
  })

  .state('app.user-permissions-edit', {
      url: '/user-permissions-edit/:permissionId',
      data: {
        auth: true
      },
      views: {
        'main@app': {
          template: '<user-permissions-edit></user-permissions-edit>'
        }
      },params: {
        alerts: null,
        permissionId: null
      }
    })

  // Emission Send Invoice
  .state('app.send-invoice', {
    url: '/send-invoice/:policyId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<send-invoice></send-invoice>'
        }
    },
    params: {
        alerts: null,
        policyId: null
    }
  })
  // Historical emission list
  .state('app.historical-emissions-lists', {
    url: '/historical-emissions',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<historical-emissions-lists></historical-emissions-lists>'
        }
    },
    params: {
        alerts: null
    }
  })

   .state('app.policy-documents', {
    url: '/policy-documents/:policyId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<policy-documents></policy-documents>'
        }
    },
    params: {
        policyId: null
    }
  })

    // Emmissions  Renewal
.state('app.emission-renewal-list', {
    url: '/emission-renewal-list',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-list></emission-renewal-list>'
      }
    },
  })

.state('app.emmision-renewal-add', {
    url: '/emmision-renewal-add/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emmision-renewal-add></emmision-renewal-add>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-plan', {
    url: '/emission-renewal-plan/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-plan></emission-renewal-plan>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-dependents', {
    url: '/emission-renewal-dependents/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-dependents></emission-renewal-dependents>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-medical-information', {
    url: '/emission-renewal-medical-information/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-medical-information></emission-renewal-medical-information>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-payment', {
    url: '/emission-renewal-payment/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-payment></emission-renewal-payment>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-start-coverage', {
    url: '/emission-renewal-start-coverage/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-start-coverage></emission-renewal-start-coverage>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.emission-renewal-documents', {
    url: '/emission-renewal-documents/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<emission-renewal-documents></emission-renewal-documents>'
      }
    },
    params: {
        policyId: null
    }
  })

.state('app.confirm-policy-renewal', {
    url: '/confirm-policy-renewal/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<confirm-policy-renewal></confirm-policy-renewal>'
      }
    },
    params: {
        policyId: null
    }
  })


.state('app.register-payment-renewal', {
    url: '/register-payment-renewal/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<register-payment-renewal></register-payment-renewal>'
      }
    },
    params: {
        policyId: null
    }
  })


.state('app.confirm-payment-renewal', {
    url: '/confirm-payment-renewal/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<confirm-payment-renewal></confirm-payment-renewal>'
      }
    },
    params: {
        policyId: null
    }
  })
.state('app.applicant-lists', {
        url: '/applicant-lists',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<applicant-lists></applicant-lists>'
            }
        },
        params: {
        }
    })
    .state('app.applicant-lists.view', {
        url: '/view/:id',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<applicant-tabs></applicant-tabs>'
            }
        },
        params: {
            id: null
        }
    })
    .state('app.applicant-lists.view.dependent', {
        url: '/dependent/:dependentId',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<dependent-tabs></dependent-tabs>'
            }
        },
        params: {
            dependentId: null
        }
    })
.state('app.ticket-claim-comment', {
    url: '/ticket-claim-comment/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-claim-comment></ticket-claim-comment>'
      }
    },params: {
        alerts: null,
        policyId: null
      }
  })

.state('app.ticket-comment', {
    url: '/ticket-comment/:policyId',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<ticket-comment></ticket-comment>'
      }
    },params: {
        alerts: null,
        policyId: null
      }
  })

// Registro de Polizas Antiguas
.state('app.policy_register', {
    url: '/policy_register',
    data: {
      auth: true
    },
    views: {
      'main@app': {
        template: '<policy_register></policy_register>'
      }
    },
  })

.state('app.policy_register_amendment', {
    url: '/policy_register_amendment/:policyId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<policy_register_amendment></policy_register_amendment>'
        }
    },
    params: {
        policyId: null
    }
  })


//Envio de Factura Emisiones en Renovacion
.state('app.send-invoice-emmision-renewal', {
    url: '/send-invoice-emmision-renewal/:policyId',
    data: {
        auth: true
    },
    views: {
        'main@app': {
            template: '<send-invoice-emmision-renewal></send-invoice-emmision-renewal>'
        }
    },
    params: {
        policyId: null
    }
  })
// agents commissions
.state('app.agent-commission-list', {
        url: '/agent-commission-list/:agentId',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<agent-commission-list></agent-commission-list>'
            }
        },
        params: {
          agentId: null,
          alerts: null

        }
    })

.state('app.agent-commission-add', {
        url: '/agent-commission-add/:agentId',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<agent-commission-add></agent-commission-add>'
            }
        },
        params: {
          agentId: null,
          alerts: null

        }
    })
.state('app.agent-commission-edit', {
        url: '/agent-commission-edit/:commissionId/:agentId',
        data: {
            auth: true
        },
        views: {
            'main@app': {
                template: '<agent-commission-edit></agent-commission-edit>'
            }
        },
        params: {
          commissionId: null,
          agentId: null,
          alerts: null

        }
    })


  .state('app.logout', {
    url: '/logout',
    views: {
      'main@app': {
        controller: function ($rootScope, $scope, $auth, $state, AclService) {
          $auth.logout().then(function () {
            delete $rootScope.me
            AclService.flushRoles()
            AclService.setAbilities({})
            $state.go('login')
          })
        }
      }
    }
  })
}

