import {AgentCommissionEditComponent} from './app/components/agent-commission-edit/agent-commission-edit.component';
import {AgentCommissionAddComponent} from './app/components/agent-commission-add/agent-commission-add.component';
import {AgentCommissionListComponent} from './app/components/agent-commission-list/agent-commission-list.component';
import {BranchEditComponent} from './app/components/branch_edit/branch_edit.component';
import {BranchAddComponent} from './app/components/branch_add/branch_add.component';
import {BranchListsComponent} from './app/components/branch_lists/branch_lists.component';
import {RenewalNotificationComponent} from './app/components/renewal-notification/renewal-notification.component';
import {TicketCommentComponent} from './app/components/ticket-comment/ticket-comment.component';
import {TicketClaimCommentComponent} from './app/components/ticket-claim-comment/ticket-claim-comment.component';
import {DependentTabsComponent} from './app/components/dependent-tabs/dependent-tabs.component';
import {ApplicantTabsComponent} from './app/components/applicant-tabs/applicant-tabs.component';
import {ApplicantListsComponent} from './app/components/applicant-lists/applicant-lists.component';
import {EmissionCanceledListsComponent} from './app/components/emission-canceled-lists/emission-canceled-lists.component';
import {HistoricalClaimsReportComponent} from './app/components/historical-claims-report/historical-claims-report.component';
import {RenewalPaymentRegisterComponent} from './app/components/renewal-payment-register/renewal-payment-register.component';
import {PolicyRegisterAmendmentComponent} from './app/components/policy_register_amendment/policy_register_amendment.component';
import {SendInvoiceEmmisionRenewalComponent} from './app/components/send-invoice-emmision-renewal/send-invoice-emmision-renewal.component';
import {PolicyRegisterComponent} from './app/components/policy_register/policy_register.component';
import {ConfirmPaymentRenewalComponent} from './app/components/confirm-payment-renewal/confirm-payment-renewal.component';
import {RegisterPaymentRenewalComponent} from './app/components/register-payment-renewal/register-payment-renewal.component';
import {ConfirmPolicyRenewalComponent} from './app/components/confirm-policy-renewal/confirm-policy-renewal.component';
import {EmissionRenewalListComponent} from './app/components/emission-renewal-list/emission-renewal-list.component';
import {EmissionRenewalDocumentsComponent} from './app/components/emission-renewal-documents/emission-renewal-documents.component';
import {EmissionRenewalStartCoverageComponent} from './app/components/emission-renewal-start-coverage/emission-renewal-start-coverage.component';
import {EmissionRenewalPaymentComponent} from './app/components/emission-renewal-payment/emission-renewal-payment.component';
import {EmissionRenewalMedicalInformationComponent} from './app/components/emission-renewal-medical-information/emission-renewal-medical-information.component';
import {EmissionRenewalDependentsComponent} from './app/components/emission-renewal-dependents/emission-renewal-dependents.component';
import {EmissionRenewalPlanComponent} from './app/components/emission-renewal-plan/emission-renewal-plan.component';
import {EmmisionRenewalAddComponent} from './app/components/emmision-renewal-add/emmision-renewal-add.component';
import {TicketDateConfirmComponent} from './app/components/ticket-date-confirm/ticket-date-confirm.component';
import {ClaimDocTypeDescriptionsEditComponent} from './app/components/claim-doc-type-descriptions-edit/claim-doc-type-descriptions-edit.component';
import {ClaimDocTypeDescriptionsAddComponent} from './app/components/claim-doc-type-descriptions-add/claim-doc-type-descriptions-add.component';
import {ClaimDocTypeDescriptionsListsComponent} from './app/components/claim-doc-type-descriptions-lists/claim-doc-type-descriptions-lists.component';
import {ClaimDocumentTypesEditComponent} from './app/components/claim-document-types-edit/claim-document-types-edit.component';
import {ClaimDocumentTypesAddComponent} from './app/components/claim-document-types-add/claim-document-types-add.component';
import {ClaimDocumentTypesListsComponent} from './app/components/claim-document-types-lists/claim-document-types-lists.component';
import {DiagnosticsEobComponent} from './app/components/diagnostics-eob/diagnostics-eob.component';
import {FeesPaidReportComponent} from './app/components/fees-paid-report/fees-paid-report.component';
import {RenewalReportComponent} from './app/components/renewal-report/renewal-report.component';
import {CommissionReportComponent} from './app/components/commission-report/commission-report.component';
import {RenewalPolicyListsComponent} from './app/components/renewal-policy-lists/renewal-policy-lists.component';
import {ConfirmRenewalPaymentComponent} from './app/components/confirm-renewal-payment/confirm-renewal-payment.component';
import {SendEobComponent} from './app/components/send-eob/send-eob.component';
import {SendInvoiceRenewalComponent} from './app/components/send-invoice-renewal/send-invoice-renewal.component';
import {LiquidationsInvoicesComponent} from './app/components/liquidations-invoices/liquidations-invoices.component';
import {ConfirmRenewalDataComponent} from './app/components/confirm-renewal-data/confirm-renewal-data.component';
import {RenewalDocumentsComponent} from './app/components/renewal-documents/renewal-documents.component';
import {RenewalCoverageStartComponent} from './app/components/renewal-coverage-start/renewal-coverage-start.component';
import {RenewalPaymentInformationComponent} from './app/components/renewal-payment-information/renewal-payment-information.component';
import {RenewalMedicalInformationComponent} from './app/components/renewal-medical-information/renewal-medical-information.component';
import {RenewalDependentsDataComponent} from './app/components/renewal-dependents-data/renewal-dependents-data.component';
import {RenewalPlanConfigComponent} from './app/components/renewal-plan-config/renewal-plan-config.component';
import {RenewalApplicantDataComponent} from './app/components/renewal-applicant-data/renewal-applicant-data.component';
import {RenewalListsComponent} from './app/components/renewal-lists/renewal-lists.component';
import {ClaimLiquidationsComponent} from './app/components/claim-liquidations/claim-liquidations.component';
import {TicketEditComponent} from './app/components/ticket-edit/ticket-edit.component';
import {TicketAddComponent} from './app/components/ticket-add/ticket-add.component';
import {TicketListsComponent} from './app/components/ticket-lists/ticket-lists.component';
import {DiagnosticEditComponent} from './app/components/diagnostic-edit/diagnostic-edit.component';
import {PolicyDocumentsComponent} from './app/components/policy-documents/policy-documents.component';
import {HistoricalEmissionsListsComponent} from './app/components/historical-emissions-lists/historical-emissions-lists.component';
import {SendInvoiceComponent} from './app/components/send-invoice/send-invoice.component';
import {ClaimDocListsComponent} from './app/components/claim-doc-lists/claim-doc-lists.component';
import {ConfirmPaymentComponent} from './app/components/confirm-payment/confirm-payment.component';
import {RegisterPaymentComponent} from './app/components/register-payment/register-payment.component';
import {PrintGuideComponent} from './app/components/print-guide/print-guide.component';
import {ClaimEditComponent} from './app/components/claim-edit/claim-edit.component';
import {ConfirmPolicyDataComponent} from './app/components/confirm-policy-data/confirm-policy-data.component';
import {ClaimTicketFilesComponent} from './app/components/claim-ticket-files/claim-ticket-files.component';
import {EmissionDocumentsComponent} from './app/components/emission-documents/emission-documents.component';
import {EmailFormatEditComponent} from './app/components/email-format-edit/email-format-edit.component';
import {EmailFormatAddComponent} from './app/components/email-format-add/email-format-add.component';
import {EmailFormatListsComponent} from './app/components/email-format-lists/email-format-lists.component';
import {MassivemailComponent} from './app/components/massivemail/massivemail.component';
import {EmissionCoverageStartComponent} from './app/components/emission-coverage-start/emission-coverage-start.component';
import {MailFormatEditComponent} from './app/components/mail-format-edit/mail-format-edit.component';
import {MailFormatAddComponent} from './app/components/mail-format-add/mail-format-add.component';
import {MailFormatListsComponent} from './app/components/mail-format-lists/mail-format-lists.component';
import {MailConfigurationEditComponent} from './app/components/mail-configuration-edit/mail-configuration-edit.component';
import {TicketClaimListsComponent} from './app/components/ticket-claim-lists/ticket-claim-lists.component';
import {ClaimClaimListsComponent} from './app/components/claim-claim-lists/claim-claim-lists.component';
import {ProviderEditComponent} from './app/components/provider-edit/provider-edit.component';
import {ProviderAddComponent} from './app/components/provider-add/provider-add.component';
import {ProviderListsComponent} from './app/components/provider-lists/provider-lists.component';
import {CurrencyListsComponent} from './app/components/currency-lists/currency-lists.component';
import {CurrencyEditComponent} from './app/components/currency-edit/currency-edit.component';
import {CurrencyAddComponent} from './app/components/currency-add/currency-add.component';
import {ClaimAddComponent} from './app/components/claim-add/claim-add.component';
import {ClaimListsComponent} from './app/components/claim-lists/claim-lists.component';
import {CoverageDetailsEditComponent} from './app/components/coverage-details-edit/coverage-details-edit.component';
import {CoverageDetailsAddComponent} from './app/components/coverage-details-add/coverage-details-add.component';
import {CoverageDetailsListsComponent} from './app/components/coverage-details-lists/coverage-details-lists.component';
import {CoverageEditComponent} from './app/components/coverage-edit/coverage-edit.component';
import {CoverageAddComponent} from './app/components/coverage-add/coverage-add.component';
import {CoverageListsComponent} from './app/components/coverage-lists/coverage-lists.component';
import {EmissionMedicalInformationComponent} from './app/components/emission-medical-information/emission-medical-information.component';
import {EmissionPaymentInformationComponent} from './app/components/emission-payment-information/emission-payment-information.component';
import {DirectoryEditComponent} from './app/components/directory-edit/directory-edit.component';
import {DeductibleEditComponent} from './app/components/deductible-edit/deductible-edit.component';
import {DeductibleDetailsEditComponent} from './app/components/deductible-details-edit/deductible-details-edit.component';
import {DeductibleDetailsListsComponent} from './app/components/deductible-details-lists/deductible-details-lists.component';
import {DeductibleDetailsAddComponent} from './app/components/deductible-details-add/deductible-details-add.component';
import {DeductibleAddComponent} from './app/components/deductible-add/deductible-add.component';
import {DeductibleListsComponent} from './app/components/deductible-lists/deductible-lists.component';
import {EmissionDependentsDataComponent} from './app/components/emission-dependents-data/emission-dependents-data.component';
import {EmissionPlanConfigComponent} from './app/components/emission-plan-config/emission-plan-config.component';
import {PlanEditComponent} from './app/components/plan-edit/plan-edit.component';
import {PlanAddComponent} from './app/components/plan-add/plan-add.component';
import {PlanListsComponent} from './app/components/plan-lists/plan-lists.component';
import {HospitalListsComponent} from './app/components/hospital-lists/hospital-lists.component';
import {HospitalEditComponent} from './app/components/hospital-edit/hospital-edit.component';
import {HospitalAddComponent} from './app/components/hospital-add/hospital-add.component';
import {DoctorEditComponent} from './app/components/doctor-edit/doctor-edit.component';
import {DoctorAddComponent} from './app/components/doctor-add/doctor-add.component';
import {DoctorListsComponent} from './app/components/doctor-lists/doctor-lists.component';
import {MedicalSpecialityEditComponent} from './app/components/medical-speciality-edit/medical-speciality-edit.component';
import {MedicalSpecialityAddComponent} from './app/components/medical-speciality-add/medical-speciality-add.component';
import {MedicalSpecialityListsComponent} from './app/components/medical-speciality-lists/medical-speciality-lists.component';
import {EmissionApplicantDataComponent} from './app/components/emission-applicant-data/emission-applicant-data.component';
import {PolicyViewComponent} from './app/components/policy-view/policy-view.component';
import {EmissionPolicyListsComponent} from './app/components/emission-policy-lists/emission-policy-lists.component';
import {AssistentAddComponent} from './app/components/assistent-add/assistent-add.component';
import {AssistentEditComponent} from './app/components/assistent-edit/assistent-edit.component';
import {AssistentListsComponent} from './app/components/assistent-lists/assistent-lists.component';
import {PolicyAddComponent} from './app/components/policy-add/policy-add.component';
import {PolicyListsComponent} from './app/components/policy-lists/policy-lists.component';
import {SubagentEditComponent} from './app/components/subagent-edit/subagent-edit.component';
import {SubagentAddComponent} from './app/components/subagent-add/subagent-add.component';
import {SubagentListsComponent} from './app/components/subagent-lists/subagent-lists.component';
import {MessengerEditComponent} from './app/components/messenger-edit/messenger-edit.component';
import {MessengerAddComponent} from './app/components/messenger-add/messenger-add.component';
import {MessengerListsComponent} from './app/components/messenger-lists/messenger-lists.component';
import {SellerEditComponent} from './app/components/seller-edit/seller-edit.component';
import {SellerAddComponent} from './app/components/seller-add/seller-add.component';
import {SellerListsComponent} from './app/components/seller-lists/seller-lists.component';
import {AgentEditComponent} from './app/components/agent-edit/agent-edit.component';
import {AgentListsComponent} from './app/components/agent-lists/agent-lists.component';
import {AgentAddComponent} from './app/components/agent-add/agent-add.component';
import {CompanyEditComponent} from './app/components/company-edit/company-edit.component';
import {CompanyAddComponent} from './app/components/company-add/company-add.component';
import {CompanyListsComponent} from './app/components/company-lists/company-lists.component';
import { TablesSimpleComponent } from './app/components/tables-simple/tables-simple.component'
import { UiModalComponent } from './app/components/ui-modal/ui-modal.component'
import { UiTimelineComponent } from './app/components/ui-timeline/ui-timeline.component'
import { UiButtonsComponent } from './app/components/ui-buttons/ui-buttons.component'
import { UiIconsComponent } from './app/components/ui-icons/ui-icons.component'
import { UiGeneralComponent } from './app/components/ui-general/ui-general.component'
import { FormsGeneralComponent } from './app/components/forms-general/forms-general.component'
import { ChartsChartjsComponent } from './app/components/charts-chartjs/charts-chartjs.component'
import { WidgetsComponent } from './app/components/widgets/widgets.component'
import { UserProfileComponent } from './app/components/user-profile/user-profile.component'
import { UserVerificationComponent } from './app/components/user-verification/user-verification.component'
import { ComingSoonComponent } from './app/components/coming-soon/coming-soon.component'
import { UserEditComponent } from './app/components/user-edit/user-edit.component'
import { UserAddComponent } from './app/components/user-add/user-add.component'
import { UserPermissionsEditComponent } from './app/components/user-permissions-edit/user-permissions-edit.component'
import { UserPermissionsAddComponent } from './app/components/user-permissions-add/user-permissions-add.component'
import { UserPermissionsComponent } from './app/components/user-permissions/user-permissions.component'
import { UserRolesEditComponent } from './app/components/user-roles-edit/user-roles-edit.component'
import { UserRolesAddComponent } from './app/components/user-roles-add/user-roles-add.component'
import { UserRolesComponent } from './app/components/user-roles/user-roles.component'
import { UserListsComponent } from './app/components/user-lists/user-lists.component'
import { DashboardComponent } from './app/components/dashboard/dashboard.component'
import { NavSidebarComponent } from './app/components/nav-sidebar/nav-sidebar.component'
import { NavHeaderComponent } from './app/components/nav-header/nav-header.component'
import { LoginLoaderComponent } from './app/components/login-loader/login-loader.component'
import { ResetPasswordComponent } from './app/components/reset-password/reset-password.component'
import { ForgotPasswordComponent } from './app/components/forgot-password/forgot-password.component'
import { LoginFormComponent } from './app/components/login-form/login-form.component'
import { RegisterFormComponent } from './app/components/register-form/register-form.component'

angular.module('app.components')
	.component('agentCommissionEdit', AgentCommissionEditComponent)
  .component('agentCommissionAdd', AgentCommissionAddComponent)
  .component('agentCommissionList', AgentCommissionListComponent)
  .component('branchEdit', BranchEditComponent)
  .component('branchAdd', BranchAddComponent)
  .component('branchLists', BranchListsComponent)
  .component('renewalNotification', RenewalNotificationComponent)
  .component('ticketComment', TicketCommentComponent)
  .component('ticketClaimComment', TicketClaimCommentComponent)
  .component('dependentTabs', DependentTabsComponent)
  .component('applicantTabs', ApplicantTabsComponent)
  .component('applicantLists', ApplicantListsComponent)
  .component('emissionCanceledLists', EmissionCanceledListsComponent)
  .component('historicalClaimsReport', HistoricalClaimsReportComponent)
  .component('renewalPaymentRegister', RenewalPaymentRegisterComponent)
  .component('policyRegisterAmendment', PolicyRegisterAmendmentComponent)
  .component('sendInvoiceEmmisionRenewal', SendInvoiceEmmisionRenewalComponent)
  .component('policyRegister', PolicyRegisterComponent)
  .component('confirmPaymentRenewal', ConfirmPaymentRenewalComponent)
  .component('registerPaymentRenewal', RegisterPaymentRenewalComponent)
  .component('confirmPolicyRenewal', ConfirmPolicyRenewalComponent)
  .component('emissionRenewalList', EmissionRenewalListComponent)
  .component('emissionRenewalDocuments', EmissionRenewalDocumentsComponent)
  .component('emissionRenewalStartCoverage', EmissionRenewalStartCoverageComponent)
  .component('emissionRenewalPayment', EmissionRenewalPaymentComponent)
  .component('emissionRenewalMedicalInformation', EmissionRenewalMedicalInformationComponent)
  .component('emissionRenewalDependents', EmissionRenewalDependentsComponent)
  .component('emissionRenewalPlan', EmissionRenewalPlanComponent)
  .component('emmisionRenewalAdd', EmmisionRenewalAddComponent)
  .component('ticketDateConfirm', TicketDateConfirmComponent)
  .component('claimDocTypeDescriptionsEdit', ClaimDocTypeDescriptionsEditComponent)
  .component('claimDocTypeDescriptionsAdd', ClaimDocTypeDescriptionsAddComponent)
  .component('claimDocTypeDescriptionsLists', ClaimDocTypeDescriptionsListsComponent)
  .component('claimDocumentTypesEdit', ClaimDocumentTypesEditComponent)
  .component('claimDocumentTypesAdd', ClaimDocumentTypesAddComponent)
  .component('claimDocumentTypesLists', ClaimDocumentTypesListsComponent)
  .component('diagnosticsEob', DiagnosticsEobComponent)
  .component('feesPaidReport', FeesPaidReportComponent)
  .component('renewalReport', RenewalReportComponent)
  .component('commissionReport', CommissionReportComponent)
  .component('renewalPolicyLists', RenewalPolicyListsComponent)
  .component('confirmRenewalPayment', ConfirmRenewalPaymentComponent)
  .component('sendEob', SendEobComponent)
  .component('sendInvoiceRenewal', SendInvoiceRenewalComponent)
  .component('liquidationsInvoices', LiquidationsInvoicesComponent)
  .component('confirmRenewalData', ConfirmRenewalDataComponent)
	.component('renewalDocuments', RenewalDocumentsComponent)
  .component('renewalCoverageStart', RenewalCoverageStartComponent)
  .component('renewalPaymentInformation', RenewalPaymentInformationComponent)
  .component('renewalMedicalInformation', RenewalMedicalInformationComponent)
  .component('renewalDependentsData', RenewalDependentsDataComponent)
  .component('renewalPlanConfig', RenewalPlanConfigComponent)
  .component('renewalApplicantData', RenewalApplicantDataComponent)
  .component('renewalLists', RenewalListsComponent)
  .component('claimLiquidations', ClaimLiquidationsComponent)
  .component('ticketEdit', TicketEditComponent)
  .component('ticketAdd', TicketAddComponent)
  .component('ticketLists', TicketListsComponent)
  .component('diagnosticEdit', DiagnosticEditComponent)
  .component('policyDocuments', PolicyDocumentsComponent)
	.component('historicalEmissionsLists', HistoricalEmissionsListsComponent)
  .component('sendInvoice', SendInvoiceComponent)
  .component('claimDocLists', ClaimDocListsComponent)
  .component('confirmPayment', ConfirmPaymentComponent)
	.component('registerPayment', RegisterPaymentComponent)
  .component('printGuide', PrintGuideComponent)	
	.component('claimEdit', ClaimEditComponent)
  .component('confirmPolicyData', ConfirmPolicyDataComponent)
  .component('claimTicketFiles', ClaimTicketFilesComponent)
  .component('emissionDocuments', EmissionDocumentsComponent)
  .component('emailFormatEdit', EmailFormatEditComponent)
  .component('emailFormatAdd', EmailFormatAddComponent)
  .component('emailFormatLists', EmailFormatListsComponent)
  .component('massivemail', MassivemailComponent)
  .component('emissionCoverageStart', EmissionCoverageStartComponent)
	.component('mailFormatEdit', MailFormatEditComponent)
  .component('mailFormatAdd', MailFormatAddComponent)
  .component('mailFormatLists', MailFormatListsComponent)
  .component('mailConfigurationEdit', MailConfigurationEditComponent)
  .component('ticketClaimLists', TicketClaimListsComponent)
	.component('claimClaimLists', ClaimClaimListsComponent)
  .component('providerEdit', ProviderEditComponent)
  .component('providerAdd', ProviderAddComponent)
  .component('providerLists', ProviderListsComponent)
  .component('currencyLists', CurrencyListsComponent)
  .component('currencyEdit', CurrencyEditComponent)
  .component('currencyAdd', CurrencyAddComponent)
	.component('claimAdd', ClaimAddComponent)
	.component('claimLists', ClaimListsComponent)	
  .component('coverageDetailsEdit', CoverageDetailsEditComponent)
  .component('coverageDetailsAdd', CoverageDetailsAddComponent)
  .component('coverageDetailsLists', CoverageDetailsListsComponent)
  .component('coverageEdit', CoverageEditComponent)
  .component('coverageAdd', CoverageAddComponent)
  .component('coverageLists', CoverageListsComponent)
	.component('emissionMedicalInformation', EmissionMedicalInformationComponent)
  .component('emissionPaymentInformation', EmissionPaymentInformationComponent)
	.component('directoryEdit', DirectoryEditComponent)
  .component('deductibleEdit', DeductibleEditComponent)
  .component('deductibleDetailsEdit', DeductibleDetailsEditComponent)
  .component('deductibleDetailsLists', DeductibleDetailsListsComponent)
  .component('deductibleDetailsAdd', DeductibleDetailsAddComponent)
  .component('deductibleAdd', DeductibleAddComponent)
  .component('deductibleLists', DeductibleListsComponent)
	.component('emissionDependentsData', EmissionDependentsDataComponent)
	.component('emissionPlanConfig', EmissionPlanConfigComponent)
	.component('planEdit', PlanEditComponent)
	.component('planAdd', PlanAddComponent)
	.component('planLists', PlanListsComponent)
	.component('hospitalLists', HospitalListsComponent)
  .component('hospitalEdit', HospitalEditComponent)
  .component('hospitalAdd', HospitalAddComponent)  
  .component('doctorEdit', DoctorEditComponent)
  .component('doctorAdd', DoctorAddComponent)
  .component('doctorLists', DoctorListsComponent)
  .component('medicalSpecialityEdit', MedicalSpecialityEditComponent)
  .component('medicalSpecialityAdd', MedicalSpecialityAddComponent)
  .component('medicalSpecialityLists', MedicalSpecialityListsComponent)
	.component('emissionApplicantData', EmissionApplicantDataComponent)	
	.component('policyView', PolicyViewComponent)
	.component('emissionPolicyLists', EmissionPolicyListsComponent)
  .component('assistentAdd', AssistentAddComponent)
  .component('assistentEdit', AssistentEditComponent)
  .component('assistentLists', AssistentListsComponent)
	.component('policyAdd', PolicyAddComponent)
	.component('policyLists', PolicyListsComponent)
  .component('subagentEdit', SubagentEditComponent)
  .component('subagentAdd', SubagentAddComponent)
  .component('subagentLists', SubagentListsComponent)
  .component('messengerEdit', MessengerEditComponent)
  .component('messengerAdd', MessengerAddComponent)
  .component('messengerLists', MessengerListsComponent)
	.component('sellerEdit', SellerEditComponent)
	.component('sellerAdd', SellerAddComponent)
	.component('sellerLists', SellerListsComponent)
  .component('agentEdit', AgentEditComponent)
	.component('agentLists', AgentListsComponent)
	.component('agentAdd', AgentAddComponent)
	.component('companyEdit', CompanyEditComponent)
	.component('companyAdd', CompanyAddComponent)
	.component('companyLists', CompanyListsComponent)
	.component('tablesSimple', TablesSimpleComponent)
  .component('uiModal', UiModalComponent)
  .component('uiTimeline', UiTimelineComponent)
  .component('uiButtons', UiButtonsComponent)
  .component('uiIcons', UiIconsComponent)
  .component('uiGeneral', UiGeneralComponent)
  .component('formsGeneral', FormsGeneralComponent)
  .component('chartsChartjs', ChartsChartjsComponent)
  .component('widgets', WidgetsComponent)
  .component('userProfile', UserProfileComponent)
  .component('userVerification', UserVerificationComponent)
  .component('comingSoon', ComingSoonComponent)
  .component('userEdit', UserEditComponent)
  .component('userAdd', UserAddComponent)
  .component('userPermissionsEdit', UserPermissionsEditComponent)
  .component('userPermissionsAdd', UserPermissionsAddComponent)
  .component('userPermissions', UserPermissionsComponent)
  .component('userRolesEdit', UserRolesEditComponent)
  .component('userRolesAdd', UserRolesAddComponent)
  .component('userRoles', UserRolesComponent)
  .component('userLists', UserListsComponent)
  .component('dashboard', DashboardComponent)
  .component('navSidebar', NavSidebarComponent)
  .component('navHeader', NavHeaderComponent)
  .component('loginLoader', LoginLoaderComponent)
  .component('resetPassword', ResetPasswordComponent)
  .component('forgotPassword', ForgotPasswordComponent)
  .component('loginForm', LoginFormComponent)
  .component('registerForm', RegisterFormComponent)
