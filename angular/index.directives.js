import { RouteBodyClassComponent } from './directives/route-bodyclass/route-bodyclass.component'
import { PasswordVerifyClassComponent } from './directives/password-verify/password-verify.component'
import { FileModelClassComponent } from './directives/file-model/file-model.component'
import { FileInputClassComponent } from './directives/file-input/file-input.component'

angular.module('app.components')
  .directive('routeBodyclass', RouteBodyClassComponent)
  .directive('passwordVerify', PasswordVerifyClassComponent)
  .directive('fileModel',['$parse', FileModelClassComponent])
  .directive('fileInput', FileInputClassComponent)






.factory('Notificaciones',function(){

    var socket = io.socket

    return {
        connect: function(sistema){

            socket['sistema'] = sistema

            if(socket['suscribed_'+sistema])
                return

            socket.post("/suscribe/"+sistema,function(res,status){
                // console.log('suscrito',res)
                socket['suscribed_'+sistema] = true
                console.log('conectado: ',sistema)
            })

        },
        on: function(sala){
            return {
                then: function(cb){

                    if(!socket['on_'+sala]){

                        socket['on_'+sala] = true

                        socket.on(socket.sistema+'_'+sala,function(res){
                            // console.log(res)
                            // cb(res)
                            cb(res)
                        })

                    }

                }
            }
        }

    }
})





















/*.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])*/

/*.directive("fileInput", function($parse){
      return{
           link: function($scope, element, attrs){
                element.on("change", function(event){
                     var files = event.target.files;
                     //console.log(files[0].name);
                     $parse(attrs.fileInput).assign($scope, element[0].files);
                     $scope.$apply();
                });
           }
      }
 }) */
